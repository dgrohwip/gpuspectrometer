/////////////////////////////////////////////////////////////////////////////////////
//
// Auto-power spectrum kernels (accumulation of |X|^2 after FFT of signal x).
//
// Note that summation error is currently O(N). With Kahan summation it would
// be O(1), with split summation it would be O(log N). Summation error comes
// from rounding when adding a "small" float to a "large" float (adding new value
// to accumulator).
//
// Two variants:
//
//  1) power spectrum entirely calculated in own kernel after FFT is finished, or
//
//  2) parts of the power spectrum computation (Re^2+Im^2: 2 float -> 1 float)
//     is done via cuFFT Callback Store function during the FFT stage, and the
//     accumulation step is done in an own kernel, reducing bandwidth needs by half
//
// Performance on Titan X for 1M channels is ~37 Gs/s complex with Kahan summation
// and similar speed also with naive summation. Variant 1 comes in two versions,
// the *_v2 is good for large FFTs only, while *_v3 is suitable for small and
// large FFTs (with fixed #threads=64 then #blocks=MAX(nchan,maxphysthreads)/#threads).
//
// (C) 2015 Jongsoo Kim, Jan Wagner
//
/////////////////////////////////////////////////////////////////////////////////////

#ifndef CUDA_DEVICE_NR
    #define CUDA_DEVICE_NR 0
#endif
#define CHECK_TIMING 1
#include "cuda_utils.cu"
#include "index_mapping.cu"
#include "arithmetic_autospec_kernels.cu"

#include <cufftXt.h>

__global__ void cu_autospec_1pol_testdata(float2* x, const size_t N, const size_t nchan)
{
        size_t i = blockIdx.x * blockDim.x + threadIdx.x;
        if (i >= N) { return; }
        float2 vx = {(float)(i%nchan + 1), 1.0f};
        x[i] = vx;
}

void autospec_cpu(const float2* __restrict__ x, float* __restrict__ a, const size_t N, const size_t nchan)
{
#if !USE_KAHAN_SUMMATION
    for (size_t n=0; n<N; n++) {
        float2 c = x[n];
        a[n % nchan] += (c.x*c.x + c.y*c.y);
    }
#else
    for (size_t ch=0; ch<nchan; ch++) {
        float comp = 0.0f;
        for (size_t n=ch; n<N; n+=nchan) {
            float2 c = x[n];
            float P = c.x*c.x + c.y*c.y;
            float y = P - comp;
            float t = a[ch] + y;
            comp = (t - a[ch]) - y;
            a[ch] = t;
	}
    }
#endif
}


int main(int argc, char **argv)
{
    cudaDeviceProp cudaDevProp;
    cudaEvent_t tstart, tstop;
    size_t threadsPerBlock, numBlocks;
    size_t maxphysthreads;
    size_t nsamples, nchan, nffts;
    size_t nfree, ntotal, memreq;
    float *d_xx;
    float *d_x;
    float *h_d;

    CUDA_CALL( cudaSetDevice(CUDA_DEVICE_NR) );
    CUDA_CALL( cudaDeviceReset() );
    CUDA_CALL( cudaMemGetInfo(&nfree, &ntotal) );
    CUDA_CALL( cudaGetDeviceProperties(&cudaDevProp, CUDA_DEVICE_NR) );
    maxphysthreads = cudaDevProp.multiProcessorCount * cudaDevProp.maxThreadsPerMultiProcessor;
    CUDA_CALL( cudaEventCreate( &tstart ) );
    CUDA_CALL( cudaEventCreate( &tstop ) );

    // Input
    nchan = 1024*1024;  // 1024k complex values per spectrum
    nffts = 625;  // 625 spectra
    if (argc == 3) {
        nchan = atoi(argv[1]);
        nffts = atoi(argv[2]);
    }
    nsamples = nchan * nffts;
    memreq = nsamples*sizeof(cufftComplex) + nchan*sizeof(float);
    if ( memreq > nfree ) {
        while ( memreq > nfree ) {
            nffts -= 32;
            nsamples = nchan * nffts;
            memreq = nsamples*sizeof(cufftComplex) + nchan*sizeof(float);
        }
        printf("Reduced number of FFTs to %zu fit GPU, now %.2f GB needed, fits into %.2f GB free.\n", nffts, 1e-9*memreq, 1e-9*nfree);
    }
    printf("Data size : %zu complex samples for %zu channels and %zu FFTs\n", nsamples, nchan, nffts);
    CUDA_CALL( cudaMalloc( (void **)&d_xx, nchan*sizeof(float) ) );
    CUDA_CALL( cudaMalloc( (void **)&d_x,  2*nsamples*sizeof(float) ) );
    CUDA_CALL( cudaHostAlloc( (void **)&h_d, 2*nsamples*sizeof(float), cudaHostAllocDefault ) );
    printf("Pointers: d_xx=%p, d_x=%p, h_d=%p\n", d_xx, d_x, h_d);
    CUDA_CALL( cudaMemset( d_xx, 0x00, nchan*sizeof(float) ) );
    CUDA_CALL( cudaMemset( d_x,  0xFF, 2*nsamples*sizeof(float) ) );
    /////////////////////////////////////////////////////////////////////////////////

    //const int lst_threadsPerBlock[11] = { 32, 8, 16, 24, 32, 56, 64, 96, 128, 256, 512 };
    const int lst_threadsPerBlock[3] = {16, 32, 64};
    for (size_t i = 0; i < sizeof(lst_threadsPerBlock)/sizeof(int); i++) {
        printf("Speed in complex samples per second per polarization with %d threads/block:\n", lst_threadsPerBlock[i]);

        // Data generator kernel
        threadsPerBlock = lst_threadsPerBlock[i];
        numBlocks = div2ceil(nsamples,threadsPerBlock);
        CUDA_TIMING_START(tstart, 0);
        cu_autospec_1pol_testdata <<< numBlocks, threadsPerBlock>>> ( (float2*)d_x, nsamples, nchan );
        CUDA_CHECK_ERRORS("cu_autospec_1pol_testdata");
        CUDA_TIMING_STOP(tstop, tstart, 0, "testdata", nsamples); // complex samples per polarization

        // Clear before accumulate
        CUDA_CALL( cudaMemset( d_xx, 0x00, nchan*sizeof(float) ) );

        // Accumulate in Version 2 (Kernel for #threads == 'Nchout')
        numBlocks = div2ceil(nchan,threadsPerBlock);
        CUDA_TIMING_START(tstart, 0);
        autoPowerSpectrum_v2 <<< numBlocks, threadsPerBlock >>> ( (cufftComplex*)d_x, (float*)d_xx, nchan+0, nffts );
        CUDA_CHECK_ERRORS("autoPowerSpectrum_v2");
        CUDA_TIMING_STOP(tstop, tstart, 0, "ac(v2)", nsamples); // complex samples per polarization

        // Clear before -accumulate
        CUDA_CALL( cudaMemset( d_xx, 0x00, nchan*sizeof(float) ) );

        // Accumulate in Version 3 (Kernel for #threads = large)
        CUDA_TIMING_START(tstart, 0);
        numBlocks = div2ceil(max(nchan,maxphysthreads),threadsPerBlock);
        autoPowerSpectrum_v3 <<< numBlocks, threadsPerBlock >>> ( (cufftComplex*)d_x, (float*)d_xx, nchan+0, nffts );
        CUDA_CHECK_ERRORS("autoPowerSpectrum_v3");
        CUDA_TIMING_STOP(tstop, tstart, 0, "ac(v3)", nsamples); // complex samples per polarization
    }

    /////////////////////////////////////////////////////////////////////////////////

    //threadsPerBlock = 64;

    // Some extra info
    printf("Layout: %zd blocks x %zd threads\n", numBlocks, threadsPerBlock);
    printf("CUDA Device #%d : %s, Compute Capability %d.%d, %d threads/block, warpsize %d, %zu threads max.\n",
        CUDA_DEVICE_NR, cudaDevProp.name, cudaDevProp.major, cudaDevProp.minor,
        cudaDevProp.maxThreadsPerBlock, cudaDevProp.warpSize, maxphysthreads
    );
    printf("nsamples = %zd (1-pol.), nffts = %zd, nchan = %zd\n", nsamples, nffts, nchan);

    // Show raw data
    CUDA_CALL( cudaMemcpy(h_d, d_x, 2*nsamples*sizeof(float), cudaMemcpyDeviceToHost) );
    printf("x  : {%.1f %.1f} {%.1f %.1f} ... ", h_d[0],h_d[1],h_d[2],h_d[3]);
    printf("{%.1f %.1f} {%.1f %.1f}\n", h_d[2*nchan-4],h_d[2*nchan-3],h_d[2*nchan-2],h_d[2*nchan-1]);

    // Show output data of 2 x autoPowerSpectrum_v2
    CUDA_CALL( cudaMemset( d_xx, 0x00, nchan*sizeof(float) ) );
    numBlocks = div2ceil(nchan,threadsPerBlock);
    autoPowerSpectrum_v2 <<< numBlocks, threadsPerBlock >>> ( (cufftComplex*)d_x, (float*)d_xx, nchan+0, nffts );
    autoPowerSpectrum_v2 <<< numBlocks, threadsPerBlock >>> ( (cufftComplex*)d_x, (float*)d_xx, nchan+0, nffts );
    printf("Result of two accumulations using Version 2:\n");

    CUDA_CALL( cudaMemcpy(h_d, d_xx, nchan*sizeof(float), cudaMemcpyDeviceToHost) );
    for (size_t n=0; n<nchan; n++) {
        h_d[n] /= (float)2*nffts;
    }
    printf("auto xx : %.1f %.1f %.1f %.1f ... ", h_d[0],h_d[1],h_d[2],h_d[3]);
    printf("%.1f %.1f %.1f %.1f\n" ,  h_d[nchan-4],h_d[nchan-3],h_d[nchan-2],h_d[nchan-1]);

    // Show output data of 2 x autoPowerSpectrum_v3
    CUDA_CALL( cudaMemset( d_xx, 0x00, nchan*sizeof(float) ) );
    numBlocks = div2ceil(max(nchan,maxphysthreads),threadsPerBlock);
    autoPowerSpectrum_v3 <<< numBlocks, threadsPerBlock >>> ( (cufftComplex*)d_x, (float*)d_xx, nchan+0, nffts );
    autoPowerSpectrum_v3 <<< numBlocks, threadsPerBlock >>> ( (cufftComplex*)d_x, (float*)d_xx, nchan+0, nffts );
    printf("Result of two accumulations using Version 3:\n");

    CUDA_CALL( cudaMemcpy(h_d, d_xx, nchan*sizeof(float), cudaMemcpyDeviceToHost) );
    for (size_t n=0; n<nchan; n++) {
        h_d[n] /= (float)2*nffts;
    }
    printf("auto xx : %.1f %.1f %.1f %.1f ... ", h_d[0],h_d[1],h_d[2],h_d[3]);
    printf("%.1f %.1f %.1f %.1f\n" ,  h_d[nchan-4],h_d[nchan-3],h_d[nchan-2],h_d[nchan-1]);

    // Show output data of 2 x CPU autospec
    float* cpu_acc = (float*)malloc(sizeof(float)*(nchan+0));
    CUDA_CALL( cudaMemcpy(h_d, d_x, 2*nsamples*sizeof(float), cudaMemcpyDeviceToHost) );
    memset((void*)cpu_acc, 0, sizeof(float)*(nchan+0));
    autospec_cpu((float2*)h_d, cpu_acc, nsamples, nchan+0);
    autospec_cpu((float2*)h_d, cpu_acc, nsamples, nchan+0);
    for (size_t n=0; n<nchan; n++) {
        cpu_acc[n] /= (float)2*nffts;
    }
    printf("cpu auto xx : %.1f %.1f %.1f %.1f ... ", cpu_acc[0],cpu_acc[1],cpu_acc[2],cpu_acc[3]);
    printf("%.1f %.1f %.1f %.1f\n" ,  cpu_acc[nchan-4],cpu_acc[nchan-3],cpu_acc[nchan-2],cpu_acc[nchan-1]);

    return 0;
}
