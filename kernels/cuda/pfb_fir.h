////////////////////////////////////////////////////////////////////////////////////////////////
//
// Polyphase Filter Bank -- FIR filtering
//
// The FFT step after FIR filtering must be done separately using for example CUFFT.
//
// The PFB layout is hard-coded for a specific Channels x Taps configuration at compile time.
// This allows the generated CUDA binary to be faster. Only the coefficients
// can be dynamically loaded and changed at run time, the PFB structure itself is fixed.
//
// Defaults for the hard-coded PFB layout are specified here. They can be overriden
// by the compiler arguments ('nvcc -DPFB_HC_NCHAN=16 ...' and so on).
//
// (C) 2015 Jan Wagner
//
////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef PFB_FIR_H
#define PFB_FIR_H

#include <stdlib.h> // for 'size_t'

////////////////////////////////////////////////////////////////////////////////////////////////
// Polyphase Filter Bank Layout
////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef PFB_HC_NCHAN
    #define PFB_HC_NCHAN 16     // default number of channels in the PFB
#endif
#ifndef PFB_HC_TAPS
    #define PFB_HC_TAPS  32     // default number of FIR taps in each PFB channel
#endif
#ifndef M_FIR_PER_BLOCK
    #define M_FIR_PER_BLOCK 32  // Shared Memory cache of FIR coeffs, set equal to 1x or 2x warpsize
#endif

#if (PFB_HC_NCHAN*PFB_HC_TAPS*4 < 65536) // K40m, Titan X (output of 'deviceQuery' CUDA SDK example program)
    #define PFB_COEFFS_FIT_CONSTMEM 1
#else
    #define PFB_COEFFS_FIT_CONSTMEM 0
#endif

////////////////////////////////////////////////////////////////////////////////////////////////
// Compute kernels
////////////////////////////////////////////////////////////////////////////////////////////////

// CUDA : cuda/pfb_fir.cu
__global__ void cu_pfb_v1a(const float* __restrict__ src, float* dst, const float* __restrict__ coeffs, size_t Nsamp, size_t dummy);
__global__ void cu_pfb_v1b(const float* __restrict__ src, float* dst, const float* __restrict__ coeffs, size_t Nsamp, size_t Npfbs);
__global__ void cu_pfb_v1c(const float* __restrict__ src, float* dst, const float* __restrict__ coeffs, size_t Nsamp, size_t Npfbs);
__global__ void cu_pfb_complexout_v1b(const float* __restrict__ src, float* dst, const float* __restrict__ coeffs, size_t Nsamp, size_t Npfbs);
__global__ void cu_pfb_complexout_v1c(const float* __restrict__ src, float* dst, const float* __restrict__ coeffs, size_t Nsamp, size_t Npfbs);
__global__ void cu_pfb_v2(const float* __restrict__ src, float* dst, const float* __restrict__ coeffs, size_t Nsamp);

// CPU reference : cpu/pfb_fir.c
void cpu_pfb(const float* __restrict__ src, float* dst, const float* __restrict__ coeffs, size_t Nsamp, float** __restrict__ coeffs_2D);

// helper
#ifndef m_min
#define m_min(x,y) ((x < y) ? x : y)
#endif

#endif // PFB_FIR_H
