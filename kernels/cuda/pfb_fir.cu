////////////////////////////////////////////////////////////////////////////////////////////////
//
// Polyphase Filter Bank -- FIR filtering
//
// The FFT step after FIR filtering must be done separately using for example CUFFT.
//
// The PFB layout is hard-coded for a specific Channels x Taps configuration at compile time.
// This allows the generated CUDA binary to be faster. Only the coefficients
// can be dynamically loaded and changed at run time, the PFB structure itself is fixed.
//
// TODO: make a __constant__ float[PFB_HC_TAPS*PFB_HC_NCHAN] area (for small PFB setups),
//       updateable through cudaGetSymbolAddress()+cudaMemcpyToSymbol(). Might be fast?
//
// (C) 2015 Jan Wagner
//
////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef PFB_FIR_CU
#define PFB_FIR_CU

#include "pfb_fir.h" // for M_FIR_PER_BLOCK, PFB_HC_TAPS, PFB_HC_NCHAN defaults if not defined

#if PFB_COEFFS_FIT_CONSTMEM
    __constant__ float c_pfb_coeffs[PFB_HC_NCHAN*PFB_HC_TAPS];
#endif

////////////////////////////////////////////////////////////////////////////////////////////////
// PFB Version 1 : v1a, v1b, v1c with real-valued output
////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Calculate output of a multi-channel PFB.
 * Must be followed by an (inverse)FFT with cufft (batched plan from cufftPlanMany).
 * FIR Stage:  y_n = b_0 * x[n] + b_1 * x[n-1] + b_2 * x[n-2] + ...
 *
 * Version 1a : every thread processes 1 channel with a dumb serial (non-parallelized) FIR
 * Throughput : 31 Ms/s only for 32 ch x 32 tap
 */
__global__ void cu_pfb_v1a(const float* __restrict__ src, float* dst, const float* __restrict__ coeffs, size_t Nsamp, size_t dummy)
{
    size_t ch = blockIdx.x * blockDim.x + threadIdx.x;
    size_t sub_ch = threadIdx.x;

    const float* __restrict__ my_coeffs = coeffs + (ch*PFB_HC_TAPS);
    const float* __restrict__ src_end = src + Nsamp;

    // Prefetch coefficients, different for each channel!
    __shared__ float sh_coeffs[M_FIR_PER_BLOCK][PFB_HC_TAPS];
    for (size_t i = 0; i < PFB_HC_TAPS; i++) {
        sh_coeffs[sub_ch][i] = my_coeffs[i]; // if already time reversed by CPU
        //sh_coeffs[sub_ch][i] = my_coeffs[PFB_HC_TAPS-i-1]; // time reversed here on GPU
    }
    __syncthreads();

    src += ch;
    dst += ch;

    if (ch < PFB_HC_NCHAN) {
        while (src < src_end) {
            float y_n = 0.0f;
            for (size_t i = 0; i < PFB_HC_TAPS; i++) {
                y_n += sh_coeffs[sub_ch][i] * src[i];
            }
            *dst = y_n;
            src += PFB_HC_NCHAN;
            dst += PFB_HC_NCHAN;
        }
    }

}

/**
 * Version 1b : every thread processes 1 channel with a dumb serial (non-parallelized) FIR,
 *              threads/block = 32 fixed,
 *              total threads = (nchan * Npfb) / 32
 *              with Npfb count of PFBs running in parallel on subsets of the input data
 * Speed for:
 *    16 ch x 16 tap : ~13.2 Gs/s
 *    16 ch x 32 tap :  ~5.6 G/s
 *    32 ch x 32 tap :  ~5.8 Gs/s
 *    32 ch x 64 tap :  ~0.3 Gs/s
 *    64 ch x 64 tap :  ~0.4 Gs/s
 */
__global__ void cu_pfb_v1b(const float* __restrict__ src, float* dst, const float* __restrict__ coeffs, size_t Nsamp, size_t Npfb)
{
    size_t ch = blockIdx.x * blockDim.x + threadIdx.x; // any multiple of PFB_HC_NCHAN
    size_t sub_ch = ch % PFB_HC_NCHAN;                 // channel inside one PFB
    size_t warp_ch = sub_ch % M_FIR_PER_BLOCK;         // channel inside one PFB grouped by warp

    const float* __restrict__ my_coeffs = coeffs + (sub_ch*PFB_HC_TAPS);
    const float* __restrict__ src_end = src + Nsamp;

    // Prefetch coefficients, different for each channel
    __shared__ float sh_coeffs[M_FIR_PER_BLOCK][PFB_HC_TAPS];
    for (size_t i = 0; i < PFB_HC_TAPS; i++) {
        sh_coeffs[warp_ch][i] = my_coeffs[i]; // already time reversed by CPU
    }
    __syncthreads();

    // Calculate y[n] in one channel of one PFB
    src += ch;
    dst += ch;
    while (src < src_end) {
        float y_n = 0.0f;
        for (size_t i = 0; i < PFB_HC_TAPS; i++) {
           y_n += sh_coeffs[warp_ch][i] * src[i]; // sh_coeffs[] three times faster than my_coeffs[]
        }
        *dst = y_n;
        src += PFB_HC_NCHAN*Npfb;
        dst += PFB_HC_NCHAN*Npfb;
    }

}

/*
 * PFB Version 1c : compared to v1b, coefficients are stored in SM in transposed order.
 * Speed for:
 *    16 ch x 16 tap : ~12.9 Gs/s
 *    16 ch x 32 tap :  ~5.4 Gs/s
 *    32 ch x 32 tap :  ~5.7 Gs/s
 *    32 ch x 64 tap :  ~2.4 Gs/s
 *    64 ch x 64 tap :  ~2.4 Gs/s
 */
__global__ void cu_pfb_v1c(const float* __restrict__ src, float* dst, const float* __restrict__ coeffs, size_t Nsamp, size_t Npfb)
{
    size_t ch = blockIdx.x * blockDim.x + threadIdx.x; // any multiple of PFB_HC_NCHAN
    size_t sub_ch = ch % PFB_HC_NCHAN;                 // channel inside one PFB
    size_t warp_ch = sub_ch % M_FIR_PER_BLOCK;         // channel inside one PFB grouped by warp

    const float* __restrict__ my_coeffs = coeffs + (sub_ch*PFB_HC_TAPS);
    const float* __restrict__ src_end = src + Nsamp;

    ch = ch % (PFB_HC_NCHAN*Npfb); // instead of nonexistent "asset(ch<PFB_HC_NCHAN*Npfb)"

    // Prefetch coefficients, different for each channel
    __shared__ float sh_coeffs[PFB_HC_TAPS][M_FIR_PER_BLOCK];
    for (size_t i = 0; i < PFB_HC_TAPS; i++) {
        sh_coeffs[i][warp_ch] = my_coeffs[i]; // already time reversed by CPU
    }
    __syncthreads();

    // Calculate y[n] in one channel of one PFB, output (real,0.0f) pairs
    src += ch;
    dst += ch;
    while (src < src_end) {
        float y_n = 0.0f;
        for (size_t i = 0; i < PFB_HC_TAPS; i++) {
           y_n += sh_coeffs[i][warp_ch] * src[i]; // sh_coeffs[] three times faster than my_coeffs[]
        }
        *dst = y_n;
        src += PFB_HC_NCHAN*Npfb;
        dst += PFB_HC_NCHAN*Npfb;
    }

}


////////////////////////////////////////////////////////////////////////////////////////////////
// Complex PFB Version 1 : complexout_v1b/_v1c with real-to-complex output (pairs of (re,0))
////////////////////////////////////////////////////////////////////////////////////////////////

/*
 * PFB Version complexout_v1b : same as pfb_v1b but output are (real,0.0f)-pairs.
 * Speed for:
 *   16 ch x 16 tap : ~9.6 Gs/s
 *   16 ch x 32 tap : ~7.4 Gs/s
 *   32 ch x 32 tap : ~7.4 Gs/s
 *   32 ch x 64 tap : ~0.4 Gs/s
 *   64 ch x 64 tap : ~0.4 Gs/s
 *   ---------------------------
 *   2048 ch x 4 tap: ~12.8 Gs/s
 */
__global__ void cu_pfb_complexout_v1b(const float* __restrict__ src, float* dst, const float* __restrict__ coeffs, size_t Nsamp, size_t Npfb)
{
    size_t ch = blockIdx.x * blockDim.x + threadIdx.x; // any multiple of PFB_HC_NCHAN
    size_t sub_ch = ch % PFB_HC_NCHAN;                 // channel inside one PFB
    size_t warp_ch = sub_ch % M_FIR_PER_BLOCK;         // channel inside one PFB grouped by warp

    const float* __restrict__ my_coeffs = coeffs + (sub_ch*PFB_HC_TAPS);
    const float* __restrict__ src_end = src + Nsamp;

    ch = ch % (PFB_HC_NCHAN*Npfb); // instead of nonexistent "asset(ch<PFB_HC_NCHAN*Npfb)"

    // Prefetch coefficients, different for each channel
    __shared__ float sh_coeffs[M_FIR_PER_BLOCK][PFB_HC_TAPS];
    for (size_t i = 0; i < PFB_HC_TAPS; i++) {
        sh_coeffs[warp_ch][i] = my_coeffs[i]; // already time reversed by CPU
    }
    __syncthreads();

    // Calculate y[n] in one channel of one PFB, output (real,0.0f) pairs
    src += ch;
    dst += 2*ch; // stride of 2 on output
    while (src < src_end) {
        float y_n = 0.0f;
        for (size_t i = 0; i < PFB_HC_TAPS; i++) {
           y_n += sh_coeffs[warp_ch][i] * src[i]; // sh_coeffs[] three times faster than my_coeffs[]
        }
        *dst = y_n;
        src += PFB_HC_NCHAN*Npfb;
        dst += 2*PFB_HC_NCHAN*Npfb; // stride of 2 on output
    }

}

/*
 * PFB Version complexout_v1c : compared to complexout_v1b, coefficients are stored in SM in transposed order.
 * Speed for:
 *   16 ch x 16 tap : ~10.3 Gs/s
 *   16 ch x 32 tap :  ~7.2 Gs/s
 *   32 ch x 32 tap :  ~7.1 Gs/s
 *   32 ch x 64 tap :  ~2.1 Gs/s
 *   64 ch x 64 tap :  ~2.1 Gs/s
 *   ---------------------------
 *   2048 ch x 4 tap: ~12.8 Gs/s
 */
__global__ void cu_pfb_complexout_v1c(const float* __restrict__ src, float* dst, const float* __restrict__ coeffs, size_t Nsamp, size_t Npfb)
{
    size_t ch = blockIdx.x * blockDim.x + threadIdx.x; // any multiple of PFB_HC_NCHAN
    size_t sub_ch = ch % PFB_HC_NCHAN;                 // channel inside one PFB
    size_t warp_ch = sub_ch % M_FIR_PER_BLOCK;         // channel inside one PFB grouped by warp

    const float* __restrict__ my_coeffs = coeffs + (sub_ch*PFB_HC_TAPS);
    const float* __restrict__ src_end = src + Nsamp;

    ch = ch % (PFB_HC_NCHAN*Npfb); // instead of nonexistent "asset(ch<PFB_HC_NCHAN*Npfb)"

    // Prefetch coefficients, different for each channel
    __shared__ float sh_coeffs[PFB_HC_TAPS][M_FIR_PER_BLOCK];
    for (size_t i = 0; i < PFB_HC_TAPS; i++) {
        sh_coeffs[i][warp_ch] = my_coeffs[i]; // already time reversed by CPU
    }
    __syncthreads();

    // Calculate y[n] in one channel of one PFB, output (real,0.0f) pairs
    src += ch;
    dst += 2*ch;
    while (src < src_end) {
        float y_n = 0.0f;
        for (size_t i = 0; i < PFB_HC_TAPS; i++) {
           y_n += sh_coeffs[i][warp_ch] * src[i]; // sh_coeffs[] three times faster than my_coeffs[]
        }
        *dst = y_n;
        // *(dst+1) = 0.0f;
        src += PFB_HC_NCHAN*Npfb;
        dst += 2*PFB_HC_NCHAN*Npfb;
    }

}

/*
 * PFB Version complexout_v1d : coeffs are loaded from constant memory into shared memory
 *   16 ch x 16 tap :  ~9.5 Gs/s
 *   32 ch x 32 tap :  ~7.3 Gs/s
 *   64 ch x 64 tap :  ~2.1 Gs/s
 *   ---------------------------
 *   2048 ch x 4 tap: ~12.8 Gs/s
 */
#if PFB_COEFFS_FIT_CONSTMEM
__global__ void cu_pfb_complexout_v1d(const float* __restrict__ src, float* dst, const float* __restrict__, size_t Nsamp, size_t Npfb)
{
    size_t ch = blockIdx.x * blockDim.x + threadIdx.x; // any multiple of PFB_HC_NCHAN
    size_t sub_ch = ch % PFB_HC_NCHAN;                 // channel inside one PFB
    size_t warp_ch = sub_ch % M_FIR_PER_BLOCK;         // channel inside one PFB grouped by warp

    const float* __restrict__ my_coeffs = c_pfb_coeffs + (sub_ch*PFB_HC_TAPS);
    const float* __restrict__ src_end = src + Nsamp;

    ch = ch % (PFB_HC_NCHAN*Npfb); // instead of nonexistent "asset(ch<PFB_HC_NCHAN*Npfb)"

    // Prefetch coefficients, different for each channel
    __shared__ float sh_coeffs[PFB_HC_TAPS][M_FIR_PER_BLOCK];
    for (size_t i = 0; i < PFB_HC_TAPS; i++) {
        sh_coeffs[i][warp_ch] = my_coeffs[i]; // already time reversed by CPU
    }
    __syncthreads();

    // Calculate y[n] in one channel of one PFB, output (real,0.0f) pairs
    src += ch;
    dst += 2*ch;
    while (src < src_end) {
        float y_n = 0.0f;
        for (size_t i = 0; i < PFB_HC_TAPS; i++) {
           y_n += sh_coeffs[i][warp_ch] * src[i]; // sh_coeffs[] three times faster than my_coeffs[]
        }
        *dst = y_n;
        // *(dst+1) = 0.0f;
        src += PFB_HC_NCHAN*Npfb;
        dst += 2*PFB_HC_NCHAN*Npfb;
    }
}
#endif // PFB_COEFFS_FIT_CONSTMEM

/*
 * PFB Version complexout_v1e : coeffs are in constant memory, not shared memory.
 * Calculates a series of y[n] in one channel of one PFB, outputs (real,0.0f) pairs.
 * The 0.0f imag part is not actually written, i.e., output array must be pre-zeroed!
 *   16 ch x 16 tap : ~10.0 Gs/s
 *   16 ch x 32 tap :  ~5.9 Gs/s
 *   32 ch x 32 tap :  ~5.9 Gs/s
 *   32 ch x 64 tap :  ~0.2 Gs/s
 *   64 ch x 64 tap :  ~0.2 Gs/s
 *   ---------------------------
 *   2048 ch x 4 tap: ~12.8 Gs/s
 */
#if PFB_COEFFS_FIT_CONSTMEM
__global__ void cu_pfb_complexout_v1e(const float* __restrict__ src, float* dst, const float* __restrict__ g_pfb_coeffs, size_t Nsamp, size_t Npfb)
{
    size_t ch = blockIdx.x * blockDim.x + threadIdx.x; // any multiple of PFB_HC_NCHAN
    size_t sub_ch = ch % PFB_HC_NCHAN;                 // channel inside one PFB

    const float* const my_coeffs = c_pfb_coeffs + (sub_ch*PFB_HC_TAPS); // coeffs, different for each channel, already time reversed by CPU
    const float* __restrict__ src_end = src + Nsamp;

    ch = ch % (PFB_HC_NCHAN*Npfb); // instead of nonexistent "asset(ch<PFB_HC_NCHAN*Npfb)"

    // Calculate series of y[n] in one channel of one PFB, output (real,0.0f) pairs
    src += ch;
    dst += 2*ch;
    while (src < src_end) {
        float y_n = 0.0f;
        for (size_t i = 0; i < PFB_HC_TAPS; i++) {
           y_n += my_coeffs[i] * src[i];
        }
        *dst = y_n;
        // *(dst+1) = 0.0f;
        src += PFB_HC_NCHAN*Npfb;
        dst += 2*PFB_HC_NCHAN*Npfb;
    }

}
#endif // PFB_COEFFS_FIT_CONSTMEM

/*
 * PFB Version complexout_v1f : coeffs are in global or constant memory, not shared memory.
 * Calculates a series of y[n] in one channel of one PFB, outputs (real,0.0f) pairs.
 * The 0.0f imag part is not actually written, i.e., output array must be pre-zeroed!
 *
 * The PFB coeff layout has to be such that coeffs in the flat 1D array are ordered
 * by channel number first, then by tap number. This is the opposite order of the
 * other cu_pfb_complexout functions.
 *
 *    8 ch x  8 tap : ~12.7 Gs/s (const mem), ~13.8 Gs/s (global mem)
 *   16 ch x 16 tap :  ~9.9 Gs/s (const mem),  ~8.8 Gs/s (global mem)
 *   32 ch x 32 tap :  ~6.1 Gs/s (const mem),  ~4.9 Gs/s (global mem)
 *   64 ch x 64 tap :  ~0.2 Gs/s (const mem),  ~2.4 Gs/s (global mem)
 *   ---------------------------------------------------------------
 *   2048 ch x 4 tap: ~12.7 Gs/s
 */
#if !PFB_COEFFS_FIT_CONSTMEM
__global__ void cu_pfb_complexout_v1f(const float* __restrict__ src, float* dst, const float* __restrict__ g_pfb_coeffs, size_t Nsamp, size_t Npfb)
{
    size_t ch = blockIdx.x * blockDim.x + threadIdx.x; // any multiple of PFB_HC_NCHAN
    size_t sub_ch = ch % PFB_HC_NCHAN;                 // channel inside one PFB

    const float* __restrict__ const my_coeffs = g_pfb_coeffs + sub_ch; // coeffs, different for each channel, already time reversed by CPU
    const float* __restrict__ src_end = src + Nsamp;

    ch = ch % (PFB_HC_NCHAN*Npfb); // instead of nonexistent "asset(ch<PFB_HC_NCHAN*Npfb)"

    // Calculate y[n] in one channel of one PFB, output (real,0.0f) pairs
    src += ch;
    dst += 2*ch;
    while (src < src_end) {
        float y_n = 0.0f;
        for (size_t i = 0; i < PFB_HC_TAPS; i++) {
           y_n += my_coeffs[i*PFB_HC_NCHAN] * src[i];
        }
        *dst = y_n;
        // *(dst+1) = 0.0f;
        src += PFB_HC_NCHAN*Npfb;
        dst += 2*PFB_HC_NCHAN*Npfb;
    }

}
#endif // !PFB_COEFFS_FIT_CONSTMEM


////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Calculate output of a multi-channel PFB.
 * Must be followed by an (inverse)FFT with cufft (batched plan from cufftPlanMany).
 * FIR Stage:  y_n = b_0 * x[n] + b_1 * x[n-1] + b_2 * x[n-2] + ...
 *
 * Optimizations:
 * since x[n-1] does not exist at the very start of data stream, we can
 * flip the order of coefficients b_{0..K-1} (for convolution)
 * and start output from y_K rather than y_0
 *
 * Version 2  :
 *   1) one block calculates the FIR structure of one channel
 *   2) each block has as many threads as there are FIR taps
 *   3) every thread in one block calculates one b_i * x[n] of one channel,
 *      the results are summed in transverse reduction to get y_n of that channel
 *   4) if more blocks are available on the GPU than there are channels,
 *      then y_n[ch], y_n+1[ch], y_n+2[ch], ... can be calculated in parallel
 *      since the output y_n+1 of a FIR does not depend on y_n
 *
 * Throughput : <700 Ms/s for 32-tap FIRs in a 16 channel PFB
 *
 *     16 ch x 32 taps : <700 Ms/s
 *   2048 ch x 4 taps  : <780 Ms/s  (same if __constant__ c_pfb_coeffs rather than __global__)
 */
__global__ void cu_pfb_v2(const float* __restrict__ src, float* dst, const float* __restrict__ coeffs, size_t Nsamp)
{
    // threadIdx.x : current tap in this channel
    // blockDim.x  : Ntaps, total number of taps in one channel
    // blockIdx.x  : current channel
    // gridDim.x   : Nchan * K, total number of channels, parallelized (multiples of PFB_HC_NCHAN)
    //printf(" threadIdx=%d blockIdx=%d blockDim=%d gridDim=%d y_n_step=%d\n", threadIdx.x, blockIdx.x, blockDim.x, gridDim.x, y_n_step);

    size_t tap      = threadIdx.x;
    size_t ch       = blockIdx.x; // 0..PFB_HC_NCHAN or multiples of it if parallel/interleaved y_{n,n+1,n+2,..}[ch]
    size_t y_n_step = gridDim.x;

    // Prefetch our coefficient
    float b_n = coeffs[PFB_HC_TAPS*(ch % PFB_HC_NCHAN) + tap];

    // Choose correct input and output sample offsets
    src += ch;
    dst += ch;

    // Partial sums of y_n
    __shared__ float sh_y[PFB_HC_TAPS];

    for (size_t n = 0; n < Nsamp; n += y_n_step) {

        sh_y[tap] = src[n] * b_n;

        // sum across threads (transverse reduction tree) to get partial sums and finally y_n
        for (unsigned int stride = PFB_HC_TAPS/2; stride > 0; stride /= 2) {
            __syncthreads();
            if (tap < stride) {
                sh_y[tap] += sh_y[tap + stride];
            }

        }
        __syncthreads();

        // write y_n
        if (tap == 0) {
            dst[n] = sh_y[0];
        }

    }
}


#endif // PFB_FIR_CU
