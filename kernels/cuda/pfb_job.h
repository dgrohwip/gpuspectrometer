////////////////////////////////////////////////////////////////////////////////////////////////
// Polyphase Filterbank Job Descriptor
////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef PFB_JOB_H
#define PFB_JOB_H

#include <stdlib.h>
#include <cufft.h>

typedef struct pfb_job_tt {

    int Nch;
    int Ntaps;

    float** h_pfb_coeff;
    float*  h_pfb_coeff_1D;
    float*  d_pfb_coeff_1D;
    size_t  Lcoeffs;

    // memory for digitized samples
    unsigned char *h_samples;
    unsigned char *d_samples;
    float *d_idata;
    float *h_idata;

    size_t nrawbytes;      // total amount of raw input data in byte
    size_t nsamples;       // total number of samples (in each band or channel) after raw -> float extraction

    // device capability -dependent
    size_t maxphysthreads; // max. number of threads supported by the device
    size_t nPFB;           // number of Nch-channel filterbanks that can run in parallel on the device

    // memory for PFB and (I)FFT data
    int do_complex_pfb;   // 0 for real output {re}, 1 for complex output {re,0.0f}
    float *d_pfbout;      // output of PFB FIRs; can be in real or in complex format
    float *d_pfbout_td;   // output of PFB FIR post-FFT, channelized time domain
    float *h_pfbout;

    // CUFFT (I)FFT
    cufftHandle ifftplan;
    size_t      ifft_output_samples;

    // memory for re-quantized data
    char *d_pfb_8bit;
    char *h_pfb_8bit;

    // memory for re-quantization
    float *d_stats;
    float *d_stats_chMeans; // == &d_stats[0], length 2*Nch since complex
    float *d_stats_chVars;  // == &d_stats[2*Nch], length 1*Nch since real
    float *h_stats;
    float *h_stats_chMeans;
    float *h_stats_chVars;

} pfb_job_t;

#endif // PFB_JOB_H
