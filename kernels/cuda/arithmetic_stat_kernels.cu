/////////////////////////////////////////////////////////////////////////////////////
//
// Signal statistics kernels
//
// (C) 2015 Jan Wagner
//
// Needs external hard-coded #define's of: PFB_HC_NCHAN
//
/////////////////////////////////////////////////////////////////////////////////////

#ifndef ARITHMETIC_STAT_KERNELS_CU
#define ARITHMETIC_STAT_KERNELS_CU

/** Calculate the mean and the variance of a float array.
 * Uses a one-pass algorithm to compute both results at the same time.
 */
__global__ void cu_stats_v1(const float* __restrict__ src, float* dst_mean, float* dst_var, size_t Nsamp, size_t in_stride)
{
    size_t nPFB = (gridDim.x*blockDim.x)/PFB_HC_NCHAN;
    size_t ch = blockIdx.x * blockDim.x + threadIdx.x; // any multiple of PFB_HC_NCHAN
    size_t sub_ch = ch % PFB_HC_NCHAN;                 // channel inside one PFB

    const float* __restrict__ src_end = src + Nsamp;

    // Online calculation of mean and variance
    // based on http://www.johndcook.com/blog/standard_deviation/
    float S1 = 0.0f;
    float Mk, Sk;
    float M1 = src[ch];
    src += in_stride;

    size_t k = 1;
    while (src < src_end) {
        float x = src[ch];
        Mk = M1 + (x - M1)/k;
        Sk = S1 + (x - M1)*(x - Mk);
        M1 = Mk;
        S1 = Sk;
        src += in_stride;
        k++;
    }
    S1 /= (k-2);

    // Sum all results together. Pre-scale for #of parallel summations (nPFB).
    atomicAdd(&dst_mean[sub_ch], M1 / nPFB);
    atomicAdd(&dst_var[sub_ch], S1 / nPFB);
}

__global__ void cu_stats_complex_v1(const float* __restrict__ src, float* dst_mean, float* dst_var, size_t Nsamp, size_t in_stride)
{
    size_t nPFB = (gridDim.x*blockDim.x)/PFB_HC_NCHAN;
    size_t ch = blockIdx.x * blockDim.x + threadIdx.x; // any multiple of PFB_HC_NCHAN
    size_t scale = 1;

    const float* __restrict__ src_end = src + Nsamp;

#if 1
    // Skip the redundant half of Nch/2-symmetric input data via GPU thread indexing change
    // An example of the method:
    //   N = 8; P = 10; ch = (1:(N*P)) - 1;
    //   got:    ch = [0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 ... K]
    //   remap:  ch = [0 1 2 3         8 9 10 11             16 17 ... 2*K]
    //   --> ch = N*floor(ch/(N/2)) + mod(ch,N/2)
    //   then :  ch = [0 1 2 3 8 9 10 11 16 17 18 19 ... 2*K]
    ch = PFB_HC_NCHAN*(size_t)(ch/(PFB_HC_NCHAN/2)) + (ch%(PFB_HC_NCHAN/2));  // skips 2nd half of IFFT output
    scale = 2;
#endif

    size_t sub_ch = ch % PFB_HC_NCHAN; // channel inside one PFB
    in_stride *= scale;

    // Online calculation of mean and variance
    // based on http://www.johndcook.com/blog/standard_deviation/
    float S1 = 0.0f;
    float Mk_re, Mk_im, Sk;
    float M1_re = src[2*ch];
    float M1_im = src[2*ch+1];
    src += in_stride;

    size_t k = 1;
    while (src < src_end) {
        float x_re = src[2*ch];
        float x_im = src[2*ch+1];
        Mk_re = M1_re + (x_re - M1_re)/k;
        Mk_im = M1_im + (x_im - M1_im)/k;
        // var = sum[conj(x-mean(x)) * (x-mean(x))] / (k-1)
        //   maybe
        //    ~= mean [ ((x_re-M1_re) - 1i*(x_im-M1_im)) * ((x_re-Mk_re) + 1i*(x_im-Mk_im)) ]
        //    ~= mean [ (x_re-M1_re)*(x_re-Mk_re) + (x_im-M1_im)*(x_im-Mk_im)
        //        + 1i * mean [ -(x_im-M1_im)*(x_re-Mk_re) + (x_re-M1_re)*(x_im-Mk_im) ] and this one tends towards zero (maybe)
        Sk = S1 + (x_re - M1_re)*(x_re - Mk_re) + (x_im - M1_im)*(x_im - Mk_im);
        M1_re = Mk_re;
        M1_im = Mk_im;
        S1 = Sk;
        src += in_stride;
        k++;
    }
    S1 /= (k-2);

    // Sum all results together. Scale before summation, to account for
    // redundant samples skipped/not skipped, and #of parallel summations (nPFB)
    atomicAdd(&dst_mean[2*sub_ch+0], M1_re / (scale*nPFB));
    atomicAdd(&dst_mean[2*sub_ch+1], M1_im / (scale*nPFB));
    atomicAdd(&dst_var[sub_ch], S1 / (scale*nPFB));

    // TODO: sum across all threads&blocks more efficiently?
}


#endif // ARITHMETIC_STAT_KERNELS_CU
