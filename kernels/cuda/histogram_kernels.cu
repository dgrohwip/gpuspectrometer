/////////////////////////////////////////////////////////////////////////////////////
//
// Kernels for histogramming 2-bit or 3-bit data with 1 or 2 channels
//
// (C) 2015 Jan Wagner
//
/////////////////////////////////////////////////////////////////////////////////////

#include <assert.h>
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <pthread.h>

#include "histogram_kernels.h"  // hist_t, CPU funcs

/////////////////////////////////////////////////////////////////////////////////////

void cpu_histogram(int nbits, int nch, uint8_t* raw, size_t Nbytes, hist_t* hist_ch1, hist_t* hist_ch2, hist_t* hist_ch3, hist_t* hist_ch4);

void cpu_histogram_8bit_1ch(const uint8_t* __restrict__ src, const size_t Nbyte, hist_t* __restrict__ hist);
void cpu_histogram_8bit_1ch_4S(const uint8_t* __restrict__ src, const size_t Nbyte, hist_t* __restrict__ hist);

void gpu_histogram256_init();
void gpu_histogram256(void* __restrict__ d_Data, size_t byteCount, hist_t* __restrict__ d_Histogram);
void gpu_histogram256_4S(void* __restrict__ d_Data, size_t byteCount, hist_t* __restrict__ d_Histogram);

void cpu_histogramtransform_8bit_to_2bit_1ch(const hist_t* hist8bit, hist_t* hist);
void cpu_histogramtransform_8bit_to_2bit_2ch(const hist_t* hist8bit, hist_t* hist_ch1, hist_t* hist_ch2);
void cpu_histogramtransform_8bit_to_2bit_4ch(const hist_t* hist8bit, hist_t* hist_ch1, hist_t* hist_ch2, hist_t* hist_ch3, hist_t* hist_ch4);
void cpu_histogramtransform_8bit4S_to_2bit_8ch(const hist_t* hist8bit4S, hist_t* hist_multichannel);
void cpu_histogramtransform_8bit4S_to_2bit_16ch(const hist_t* hist8bit4S, hist_t* hist_multichannel);

/** CUDA kernel for histogram of 3-bit 1-channel data (now 108 Gs/s/1channel; tough to optimize the speed any further?) */
__global__ void cu_histogram_3bit_1ch(const uint8_t* __restrict__ src, const size_t Nbyte, hist_t* __restrict__ hist);

/** CUDA kernel for histogram of 3-bit 2-channel data (now 108 G/s/2channel; tough to optimize the speed any further?) */
__global__ void cu_histogram_3bit_2ch(const uint8_t* __restrict__ src, const size_t Nbyte, hist_t* __restrict__ hist_ch1, hist_t* __restrict__ hist_ch2);

/////////////////////////////////////////////////////////////////////////////////////

static void cpu_histogram_2bit_1ch_fast(const uint8_t* __restrict__ src, const size_t Nbyte, hist_t* hist);
static void cpu_histogram_2bit_2ch_fast(const uint8_t* __restrict__ src, const size_t Nbyte, hist_t* hist_ch1, hist_t* hist_ch2);
static void cpu_histogram_2bit_4ch_fast(const uint8_t* __restrict__ src, const size_t Nbyte, hist_t* __restrict__ hist_ch1, hist_t* __restrict__ hist_ch2, hist_t* __restrict__ hist_ch3, hist_t* __restrict__ hist_ch4);
static void cpu_histogram_2bit_8ch_fast(const uint8_t* __restrict__ src, const size_t Nbyte, hist_t* __restrict__ hist_multichannel);
static void cpu_histogram_2bit_16ch_fast(const uint8_t* __restrict__ src, const size_t Nbyte, hist_t* __restrict__ hist_multichannel);

/////////////////////////////////////////////////////////////////////////////////////

/** cpu_histogram_2bit_1ch()
 * For 2-bit data, every 1 byte contains a set of 4 samples.
 * Assume all samples are from the same channel.
 * Speed on CPU: 96000000 samples in 55.225 msec, 1738.3 Ms/s, 434.586 MB/s
 */
void cpu_histogram_2bit_1ch(const uint8_t* __restrict__ src, const size_t Nbyte, hist_t* hist)
{
    // Temporary histograms for each sample in 1 byte allows CPU instruction pipelining (fast)
    hist_t h0[4] = {0}, h1[4] = {0}, h2[4] = {0}, h3[4] = {0};
    uint_fast32_t i;
    for (i=0; i<Nbyte; i++) {
        uint8_t w = src[i];
        h0[(w >> 0) & 3]++;
        h1[(w >> 2) & 3]++;
        h2[(w >> 4) & 3]++;
        h3[(w >> 6) & 3]++;
    }
    for (i=0; i<4; i++) {
        hist[i] = h0[i] + h1[i] + h2[i] + h3[i]; // final histogram
    }
}

/** cpu_histogram_2bit_2ch()
 * For 2-bit data, every 1 byte contains a set of 4 samples.
 * Assume all odd samples are from the first, and even samples from the second channel.
 * Speed on CPU: 96000000 samples in 55.298 msec, 1736.0 Ms/s, 434.012 MB/s
 */
void cpu_histogram_2bit_2ch(const uint8_t* __restrict__ src, const size_t Nbyte, hist_t* hist_ch1, hist_t* hist_ch2)
{
    // Temporary histograms for each sample in 1 byte allows CPU instruction pipelining (fast)
    hist_t h0[4] = {0}, h1[4] = {0}, h2[4] = {0}, h3[4] = {0};
    uint_fast32_t i;
    for (i=0; i<Nbyte; i++) {
        uint8_t w = src[i];
        h0[(w >> 0) & 3]++;
        h1[(w >> 2) & 3]++;
        h2[(w >> 4) & 3]++;
        h3[(w >> 6) & 3]++;
    }
    for (i=0; i<4; i++) {
        hist_ch1[i] = h0[i] + h2[i]; // final histogram
        hist_ch2[i] = h1[i] + h3[i]; // final histogram
    }
}

/** cpu_histogram_2bit_4ch()
 * For 2-bit data, every 1 byte contains a set of 4 samples, each from a different channel.
 * Speed on CPU: 96000000 samples in 51.648 msec, 1858.7 Ms/s, 464.684 MB/s
 */
void cpu_histogram_2bit_4ch(const uint8_t* __restrict__ src, const size_t Nbyte,
    hist_t* hist_ch1, hist_t* hist_ch2, hist_t* hist_ch3, hist_t* hist_ch4
)
{
    uint_fast32_t i;
    for (i=0; i<Nbyte; i++) {
        uint8_t w = src[i];
        hist_ch1[(w >> 0) & 3]++;
        hist_ch2[(w >> 2) & 3]++;
        hist_ch3[(w >> 4) & 3]++;
        hist_ch4[(w >> 6) & 3]++;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

/** cpu_histogram_3bit_1ch()
 * For 3-bit data, every 3 bytes (24 bit) contain a set of 8 samples.
 * Assume all samples are from the same channel.
 * Speed on CPU: 64000000 samples in 44.676 msec, 1432.5 Ms/s, 537.201 MB/s
 */
void cpu_histogram_3bit_1ch(const uint8_t* __restrict__ src, const size_t Nbyte, hist_t* hist)
{
    hist_t h0[8] = {0}, h1[8] = {0}, h2[8] = {0}, h3[8] = {0};
    uint_fast32_t i;

    for (i=0; i<Nbyte/3; i++) {

        // Load pieces of 24-bit word (unaligned!)
        uint8_t b0 = src[3*i+0];
        uint8_t b1 = src[3*i+1];
        uint8_t b2 = src[3*i+2];

        // Generate 24-bit word
        uint_fast32_t w = (((uint_fast32_t)b2) << 16) | (((uint_fast32_t)b1) << 8) | ((uint_fast32_t)b0);

        h0[(w >>  0) & 7]++;
        h1[(w >>  3) & 7]++;
        h2[(w >>  6) & 7]++;
        h3[(w >>  9) & 7]++;
        h0[(w >> 12) & 7]++;
        h1[(w >> 15) & 7]++;
        h2[(w >> 18) & 7]++;
        h3[(w >> 21) & 7]++;
    }

    for (i=0; i<8; i++) {
        hist[i] = h0[i] + h1[i] + h2[i] + h3[i];
    }
}

/* Speed on CPU: 64000000 samples in 39.499 msec, 1620.3 Ms/s, 607.610 MB/s */
void cpu_histogram_3bit_1ch_V2(const uint8_t* __restrict__ src, const size_t Nbyte, hist_t* __restrict__ hist)
{
    hist_t h0[8] = {0}, h1[8] = {0}, h2[8] = {0}, h3[8] = {0};
    uint_fast32_t i;

    for (i=0; i<Nbyte-2; i+=3) {
        // Load pieces of 24-bit word (unaligned!)
        // start bits: 0, 3, 6, 9, 12, 15, 18, 21(,24)
        // bytes     : [aaab.bbcc|cddd.eeef|ffgg.ghhh]
        //             [ccbb.baaa|feee.dddc|hhhg.ggff]
        uint_fast8_t b0 = src[i+0];
        uint_fast8_t b1 = src[i+1];
        uint_fast8_t b2 = src[i+2];

        h0[(b0 >> 0) & 7]++; // aaa
        h1[(b0 >> 3) & 7]++; // bbb
        h2[((b0 >> (6-1)) & 6) | (b1 & 1)]++;  // ccc

        h3[(b1 >> 1) & 7]++; // ddd
        h0[(b1 >> 4) & 7]++; // eee
        h1[((b1 >> (7-2)) & 4) | (b2 & 3)]++;  // fff

        h2[(b2 >> 2) & 7]++; // ggg
        h3[(b2 >> 5) & 7]++; // hhh
    }

    for (i=0; i<8; i++) {
        hist[i] = h0[i] + h1[i] + h2[i] + h3[i];
    }
}

/* Speed on CPU: 64000000 samples in 24.145 msec, 2650.7 Ms/s, 993.995 MB/s */
static void cpu_histogram_3bit_1ch_V3(const uint8_t* __restrict__ src, const size_t Nbyte, hist_t* __restrict__ hist)
{
    hist_t h0[256] = {0}, h1[256] = {0}, h2[256] = {0}, sh0[8] = {0};
    uint_fast32_t i;

    for (i=0; i<Nbyte-2; i+=3) {
        // start bits: 0, 3, 6, 9, 12, 15, 18, 21(,24)
        // bytes     : [aaab.bbcc|cddd.eeef|ffgg.ghhh]
        //             [ccbb.baaa|feee.dddc|hhhg.ggff]
        h0[src[i+0]]++;
        h1[src[i+1]]++;
        h2[src[i+2]]++;
        uint_fast16_t s1 = ((uint_fast16_t)src[i+0]) | (((uint_fast16_t)src[i+1])<<8);
        uint_fast16_t s2 = ((uint_fast16_t)src[i+1]) | (((uint_fast16_t)src[i+2])<<8);
        sh0[(s1 >> 6) & 7]++;
        sh0[(s2 >> 7) & 7]++;
    }
    for (i=0; i<256; i++) {
        uint8_t aaa = i&7;
        uint8_t bbb = (i>>3)&7;
        hist[aaa] += h0[i];
        hist[bbb] += h0[i];
        uint8_t ddd = (i>>1)&7;
        uint8_t eee = (i>>4)&7;
        hist[ddd] += h1[i];
        hist[eee] += h1[i];
        uint8_t ggg = (i>>2)&7;
        uint8_t hhh = (i>>5)&7;
        hist[ggg] += h2[i];
        hist[hhh] += h2[i];
        // for the above only, ignoring sh0[], the speed on CPU: 64000000 samples in 12.737 msec, 5024.7 Ms/s, 1884.274 MB/s
    }
    for (i=0; i<8; i++) {
        hist[i] += sh0[i];
        // for sh0[] calculations included, the speed on CPU: 64000000 samples in 24.145 msec, 2650.7 Ms/s, 993.995 MB/s
    }
}

/** cpu_histogram_3bit_2ch()
 * For 3-bit data, every 3 bytes (24 bit) contain a set of 8 samples.
 * Assume all odd samples are from the first, and even samples from the second channel.
 * Speed on CPU: 64000000 samples in 44.115 msec, 1450.8 Ms/s, 544.033 MB/s
 */
void cpu_histogram_3bit_2ch(const uint8_t* __restrict__ src, const size_t Nbyte, hist_t* hist_ch1, hist_t* hist_ch2)
{
    hist_t h0[8] = {0}, h1[8] = {0}, h2[8] = {0}, h3[8] = {0}, h4[8] = {0}, h5[8] = {0}, h6[8] = {0}, h7[8] = {0};
    uint_fast32_t i;

    for (i=0; i<Nbyte/3; i++) {

        // Load pieces of 24-bit word (unaligned!)
        uint8_t b0 = src[3*i+0];
        uint8_t b1 = src[3*i+1];
        uint8_t b2 = src[3*i+2];

        // Generate 24-bit word : 24/3 = 8 samples total
        uint32_t w = (((uint32_t)b2) << 16) | (((uint32_t)b1) << 8) | ((uint32_t)b0);

        h0[(w >>  0) & 7]++;
        h1[(w >>  3) & 7]++;
        h2[(w >>  6) & 7]++;
        h3[(w >>  9) & 7]++;
        h4[(w >> 12) & 7]++;
        h5[(w >> 15) & 7]++;
        h6[(w >> 18) & 7]++;
        h7[(w >> 21) & 7]++;
    }

    for (i=0; i<8; i++) {
        hist_ch1[i] = h0[i] + h2[i] + h4[i] + h6[i];
        hist_ch2[i] = h1[i] + h3[i] + h5[i] + h7[i];
    }
}

static void cpu_histogram_3bit_2ch_V3(const uint8_t* __restrict__ src, const size_t Nbyte, hist_t* __restrict__ hist_ch1, hist_t* __restrict__ hist_ch2)
{
    hist_t h0[256] = {0}, h1[256] = {0}, h2[256] = {0}, sh0[8] = {0}, sh1[8] = {0};
    uint_fast32_t i;

    for (i=0; i<Nbyte-2; i+=3) {
        // start bits: 0, 3, 6, 9, 12, 15, 18, 21(,24)
        // bytes     : [aaab.bbcc|cddd.eeef|ffgg.ghhh]
        //             [ccbb.baaa|feee.dddc|hhhg.ggff]
        h0[src[i+0]]++;
        h1[src[i+1]]++;
        h2[src[i+2]]++;
        // note: throughput above is ~5 Gs/s, but must add "missing" samples below and that reduces throughput by about half
        uint_fast16_t s1 = ((uint_fast16_t)src[i+0]) | (((uint_fast16_t)src[i+1])<<8);
        uint_fast16_t s2 = ((uint_fast16_t)src[i+1]) | (((uint_fast16_t)src[i+2])<<8);
        sh0[(s1 >> 6) & 7]++;
        sh1[(s2 >> 7) & 7]++;
    }
    for (i=0; i<256; i++) {
        uint8_t aaa = i&7;
        uint8_t bbb = (i>>3)&7;
        hist_ch1[aaa] += h0[i];
        hist_ch2[bbb] += h0[i];
        uint8_t ddd = (i>>1)&7;
        uint8_t eee = (i>>4)&7;
        hist_ch2[ddd] += h1[i];
        hist_ch1[eee] += h1[i];
        uint8_t ggg = (i>>2)&7;
        uint8_t hhh = (i>>5)&7;
        hist_ch1[ggg] += h2[i];
        hist_ch2[hhh] += h2[i];
    }
    for (i=0; i<8; i++) {
        hist_ch1[i] += sh0[i];
        hist_ch2[i] += sh1[i];
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

/** cpu_histogram_8bit_1ch()
 * For 8-bit data, but can be used for 1-bit, 2-bit and 4-bit.
 * Speed on CPU: 24000000 samples in 12.567 msec, 1909.8 Ms/s, 1909.764 MB/s
 * With a post-processing step 2-bit speed near 8 Gs/s!
 */
void cpu_histogram_8bit_1ch(const uint8_t* __restrict__ src, const size_t Nbyte, hist_t* __restrict__ hist)
{
    // Temporary histograms for each sample in 1 byte allows CPU instruction pipelining (fast)
    hist_t h0[256] = {0}, h1[256] = {0}, h2[256] = {0}, h3[256] = {0};
    hist_t h4[256] = {0}, h5[256] = {0}, h6[256] = {0}, h7[256] = {0};
    uint_fast32_t i;
    for (i=0; i<Nbyte-7; i+=8) {
        h0[src[i+0]]++;
        h1[src[i+1]]++;
        h2[src[i+2]]++;
        h3[src[i+3]]++;
        h0[src[i+4]]++;
        h1[src[i+5]]++;
        h2[src[i+6]]++;
        h3[src[i+7]]++;
    }
    for (i=0; i<256; i++) {
        hist[i] = h0[i] + h1[i] + h2[i] + h3[i] + h4[i] + h5[i] + h6[i] + h7[i]; // final histogram
    }
}

void cpu_histogram_8bit_1ch_4S(const uint8_t* __restrict__ src, const size_t Nbyte, hist_t* __restrict__ hist)
{
    // Temporary histograms for each sample in 1 byte allows CPU instruction pipelining (fast)
    hist_t h0[256] = {0}, h1[256] = {0}, h2[256] = {0}, h3[256] = {0};
    hist_t h4[256] = {0}, h5[256] = {0}, h6[256] = {0}, h7[256] = {0};
    uint_fast32_t i;
    for (i=0; i<Nbyte-7; i+=8) {
        h0[src[i+0]]++;
        h1[src[i+1]]++;
        h2[src[i+2]]++;
        h3[src[i+3]]++;
        h0[src[i+4]]++;
        h1[src[i+5]]++;
        h2[src[i+6]]++;
        h3[src[i+7]]++;
    }
    for (i=0; i<256; i++) {
        hist[4*i + 0] = h0[i] + h4[i];
        hist[4*i + 1] = h1[i] + h5[i];
        hist[4*i + 2] = h2[i] + h6[i];
        hist[4*i + 3] = h3[i] + h7[i];
    }
}


////////////////////////////////////////////////////////////////////////////////////////////////////

/** cpu_histogram_24bit_1ch()
 * For 24-bit data, but intended for 3-bit with post processing of histogram.
 * Speed on CPU: 8000000 samples in 128.667 msec, 62.2 Ms/s, 186.528 MB/s
 */
void cpu_histogram_24bit_1ch(const uint8_t* __restrict__ src, const size_t Nbyte, hist_t* hist)
{
    uint_fast32_t i;
    for (i=0; i<Nbyte-2; i+=3) {
        uint_fast32_t w = ((uint_fast32_t)src[i]) | ((uint_fast32_t)src[i+1])<<8 | ((uint_fast32_t)src[i+2])<<16;
        hist[w]++;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// HISTOGRAM TRANSFORM
////////////////////////////////////////////////////////////////////////////////////////////////////

void cpu_histogramtransform_8bit_to_2bit_1ch(const hist_t* hist8bit, hist_t* hist)
{
    static const char s_lut[256][4] = {
         /*0x00*/{4,0,0,0},/*0x01*/{3,1,0,0},/*0x02*/{3,0,1,0},/*0x03*/{3,0,0,1},/*0x04*/{3,1,0,0 },
         /*0x05*/{2,2,0,0},/*0x06*/{2,1,1,0},/*0x07*/{2,1,0,1},/*0x08*/{3,0,1,0},/*0x09*/{2,1,1,0 },
         /*0x0A*/{2,0,2,0},/*0x0B*/{2,0,1,1},/*0x0C*/{3,0,0,1},/*0x0D*/{2,1,0,1},/*0x0E*/{2,0,1,1 },
         /*0x0F*/{2,0,0,2},/*0x10*/{3,1,0,0},/*0x11*/{2,2,0,0},/*0x12*/{2,1,1,0},/*0x13*/{2,1,0,1 },
         /*0x14*/{2,2,0,0},/*0x15*/{1,3,0,0},/*0x16*/{1,2,1,0},/*0x17*/{1,2,0,1},/*0x18*/{2,1,1,0 },
         /*0x19*/{1,2,1,0},/*0x1A*/{1,1,2,0},/*0x1B*/{1,1,1,1},/*0x1C*/{2,1,0,1},/*0x1D*/{1,2,0,1 },
         /*0x1E*/{1,1,1,1},/*0x1F*/{1,1,0,2},/*0x20*/{3,0,1,0},/*0x21*/{2,1,1,0},/*0x22*/{2,0,2,0 },
         /*0x23*/{2,0,1,1},/*0x24*/{2,1,1,0},/*0x25*/{1,2,1,0},/*0x26*/{1,1,2,0},/*0x27*/{1,1,1,1 },
         /*0x28*/{2,0,2,0},/*0x29*/{1,1,2,0},/*0x2A*/{1,0,3,0},/*0x2B*/{1,0,2,1},/*0x2C*/{2,0,1,1 },
         /*0x2D*/{1,1,1,1},/*0x2E*/{1,0,2,1},/*0x2F*/{1,0,1,2},/*0x30*/{3,0,0,1},/*0x31*/{2,1,0,1 },
         /*0x32*/{2,0,1,1},/*0x33*/{2,0,0,2},/*0x34*/{2,1,0,1},/*0x35*/{1,2,0,1},/*0x36*/{1,1,1,1 },
         /*0x37*/{1,1,0,2},/*0x38*/{2,0,1,1},/*0x39*/{1,1,1,1},/*0x3A*/{1,0,2,1},/*0x3B*/{1,0,1,2 },
         /*0x3C*/{2,0,0,2},/*0x3D*/{1,1,0,2},/*0x3E*/{1,0,1,2},/*0x3F*/{1,0,0,3},/*0x40*/{3,1,0,0 },
         /*0x41*/{2,2,0,0},/*0x42*/{2,1,1,0},/*0x43*/{2,1,0,1},/*0x44*/{2,2,0,0},/*0x45*/{1,3,0,0 },
         /*0x46*/{1,2,1,0},/*0x47*/{1,2,0,1},/*0x48*/{2,1,1,0},/*0x49*/{1,2,1,0},/*0x4A*/{1,1,2,0 },
         /*0x4B*/{1,1,1,1},/*0x4C*/{2,1,0,1},/*0x4D*/{1,2,0,1},/*0x4E*/{1,1,1,1},/*0x4F*/{1,1,0,2 },
         /*0x50*/{2,2,0,0},/*0x51*/{1,3,0,0},/*0x52*/{1,2,1,0},/*0x53*/{1,2,0,1},/*0x54*/{1,3,0,0 },
         /*0x55*/{0,4,0,0},/*0x56*/{0,3,1,0},/*0x57*/{0,3,0,1},/*0x58*/{1,2,1,0},/*0x59*/{0,3,1,0 },
         /*0x5A*/{0,2,2,0},/*0x5B*/{0,2,1,1},/*0x5C*/{1,2,0,1},/*0x5D*/{0,3,0,1},/*0x5E*/{0,2,1,1 },
         /*0x5F*/{0,2,0,2},/*0x60*/{2,1,1,0},/*0x61*/{1,2,1,0},/*0x62*/{1,1,2,0},/*0x63*/{1,1,1,1 },
         /*0x64*/{1,2,1,0},/*0x65*/{0,3,1,0},/*0x66*/{0,2,2,0},/*0x67*/{0,2,1,1},/*0x68*/{1,1,2,0 },
         /*0x69*/{0,2,2,0},/*0x6A*/{0,1,3,0},/*0x6B*/{0,1,2,1},/*0x6C*/{1,1,1,1},/*0x6D*/{0,2,1,1 },
         /*0x6E*/{0,1,2,1},/*0x6F*/{0,1,1,2},/*0x70*/{2,1,0,1},/*0x71*/{1,2,0,1},/*0x72*/{1,1,1,1 },
         /*0x73*/{1,1,0,2},/*0x74*/{1,2,0,1},/*0x75*/{0,3,0,1},/*0x76*/{0,2,1,1},/*0x77*/{0,2,0,2 },
         /*0x78*/{1,1,1,1},/*0x79*/{0,2,1,1},/*0x7A*/{0,1,2,1},/*0x7B*/{0,1,1,2},/*0x7C*/{1,1,0,2 },
         /*0x7D*/{0,2,0,2},/*0x7E*/{0,1,1,2},/*0x7F*/{0,1,0,3},/*0x80*/{3,0,1,0},/*0x81*/{2,1,1,0 },
         /*0x82*/{2,0,2,0},/*0x83*/{2,0,1,1},/*0x84*/{2,1,1,0},/*0x85*/{1,2,1,0},/*0x86*/{1,1,2,0 },
         /*0x87*/{1,1,1,1},/*0x88*/{2,0,2,0},/*0x89*/{1,1,2,0},/*0x8A*/{1,0,3,0},/*0x8B*/{1,0,2,1 },
         /*0x8C*/{2,0,1,1},/*0x8D*/{1,1,1,1},/*0x8E*/{1,0,2,1},/*0x8F*/{1,0,1,2},/*0x90*/{2,1,1,0 },
         /*0x91*/{1,2,1,0},/*0x92*/{1,1,2,0},/*0x93*/{1,1,1,1},/*0x94*/{1,2,1,0},/*0x95*/{0,3,1,0 },
         /*0x96*/{0,2,2,0},/*0x97*/{0,2,1,1},/*0x98*/{1,1,2,0},/*0x99*/{0,2,2,0},/*0x9A*/{0,1,3,0 },
         /*0x9B*/{0,1,2,1},/*0x9C*/{1,1,1,1},/*0x9D*/{0,2,1,1},/*0x9E*/{0,1,2,1},/*0x9F*/{0,1,1,2 },
         /*0xA0*/{2,0,2,0},/*0xA1*/{1,1,2,0},/*0xA2*/{1,0,3,0},/*0xA3*/{1,0,2,1},/*0xA4*/{1,1,2,0 },
         /*0xA5*/{0,2,2,0},/*0xA6*/{0,1,3,0},/*0xA7*/{0,1,2,1},/*0xA8*/{1,0,3,0},/*0xA9*/{0,1,3,0 },
         /*0xAA*/{0,0,4,0},/*0xAB*/{0,0,3,1},/*0xAC*/{1,0,2,1},/*0xAD*/{0,1,2,1},/*0xAE*/{0,0,3,1 },
         /*0xAF*/{0,0,2,2},/*0xB0*/{2,0,1,1},/*0xB1*/{1,1,1,1},/*0xB2*/{1,0,2,1},/*0xB3*/{1,0,1,2 },
         /*0xB4*/{1,1,1,1},/*0xB5*/{0,2,1,1},/*0xB6*/{0,1,2,1},/*0xB7*/{0,1,1,2},/*0xB8*/{1,0,2,1 },
         /*0xB9*/{0,1,2,1},/*0xBA*/{0,0,3,1},/*0xBB*/{0,0,2,2},/*0xBC*/{1,0,1,2},/*0xBD*/{0,1,1,2 },
         /*0xBE*/{0,0,2,2},/*0xBF*/{0,0,1,3},/*0xC0*/{3,0,0,1},/*0xC1*/{2,1,0,1},/*0xC2*/{2,0,1,1 },
         /*0xC3*/{2,0,0,2},/*0xC4*/{2,1,0,1},/*0xC5*/{1,2,0,1},/*0xC6*/{1,1,1,1},/*0xC7*/{1,1,0,2 },
         /*0xC8*/{2,0,1,1},/*0xC9*/{1,1,1,1},/*0xCA*/{1,0,2,1},/*0xCB*/{1,0,1,2},/*0xCC*/{2,0,0,2 },
         /*0xCD*/{1,1,0,2},/*0xCE*/{1,0,1,2},/*0xCF*/{1,0,0,3},/*0xD0*/{2,1,0,1},/*0xD1*/{1,2,0,1 },
         /*0xD2*/{1,1,1,1},/*0xD3*/{1,1,0,2},/*0xD4*/{1,2,0,1},/*0xD5*/{0,3,0,1},/*0xD6*/{0,2,1,1 },
         /*0xD7*/{0,2,0,2},/*0xD8*/{1,1,1,1},/*0xD9*/{0,2,1,1},/*0xDA*/{0,1,2,1},/*0xDB*/{0,1,1,2 },
         /*0xDC*/{1,1,0,2},/*0xDD*/{0,2,0,2},/*0xDE*/{0,1,1,2},/*0xDF*/{0,1,0,3},/*0xE0*/{2,0,1,1 },
         /*0xE1*/{1,1,1,1},/*0xE2*/{1,0,2,1},/*0xE3*/{1,0,1,2},/*0xE4*/{1,1,1,1},/*0xE5*/{0,2,1,1 },
         /*0xE6*/{0,1,2,1},/*0xE7*/{0,1,1,2},/*0xE8*/{1,0,2,1},/*0xE9*/{0,1,2,1},/*0xEA*/{0,0,3,1 },
         /*0xEB*/{0,0,2,2},/*0xEC*/{1,0,1,2},/*0xED*/{0,1,1,2},/*0xEE*/{0,0,2,2},/*0xEF*/{0,0,1,3 },
         /*0xF0*/{2,0,0,2},/*0xF1*/{1,1,0,2},/*0xF2*/{1,0,1,2},/*0xF3*/{1,0,0,3},/*0xF4*/{1,1,0,2 },
         /*0xF5*/{0,2,0,2},/*0xF6*/{0,1,1,2},/*0xF7*/{0,1,0,3},/*0xF8*/{1,0,1,2},/*0xF9*/{0,1,1,2 },
         /*0xFA*/{0,0,2,2},/*0xFB*/{0,0,1,3},/*0xFC*/{1,0,0,3},/*0xFD*/{0,1,0,3},/*0xFE*/{0,0,1,3 },
         /*0xFF*/{0,0,0,4 }
    };

    int i;
    for (i=0; i<256; i++) {
        int count = hist8bit[i];
        hist[0] += count * (int)s_lut[i][0];
        hist[1] += count * (int)s_lut[i][1];
        hist[2] += count * (int)s_lut[i][2];
        hist[3] += count * (int)s_lut[i][3];
    }

    if (0) {
        // Generate the LUT above
        int i, j;
        printf("        ");
        for (i=0; i<256; i++) {
                int h[4] = {0};
                int v = i;
                for (j=0; j<4; j++) {
                        h[(v&3)]++;
                        v = v>>2;
                }
                printf("/*0x%02X*/{%d,%d,%d,%d},", i, h[0], h[1], h[2], h[3]);
                if (((i+1)%5) == 0) { printf("\n        "); }
        }
        printf("\n");
    }

}

void cpu_histogramtransform_8bit_to_2bit_2ch(const hist_t* hist8bit, hist_t* hist_ch1, hist_t* hist_ch2)
{
    static const char s_lut_ch1[256][4] = {
        /*0x00*/{2,0,0,0}, /*0x01*/{1,1,0,0}, /*0x02*/{1,0,1,0}, /*0x03*/{1,0,0,1}, /*0x04*/{2,0,0,0}, /*0x05*/{1,1,0,0}, /*0x06*/{1,0,1,0}, /*0x07*/{1,0,0,1}, /*0x08*/{2,0,0,0}, /*0x09*/{1,1,0,0}, 
        /*0x0A*/{1,0,1,0}, /*0x0B*/{1,0,0,1}, /*0x0C*/{2,0,0,0}, /*0x0D*/{1,1,0,0}, /*0x0E*/{1,0,1,0}, /*0x0F*/{1,0,0,1}, /*0x10*/{1,1,0,0}, /*0x11*/{0,2,0,0}, /*0x12*/{0,1,1,0}, /*0x13*/{0,1,0,1}, 
        /*0x14*/{1,1,0,0}, /*0x15*/{0,2,0,0}, /*0x16*/{0,1,1,0}, /*0x17*/{0,1,0,1}, /*0x18*/{1,1,0,0}, /*0x19*/{0,2,0,0}, /*0x1A*/{0,1,1,0}, /*0x1B*/{0,1,0,1}, /*0x1C*/{1,1,0,0}, /*0x1D*/{0,2,0,0}, 
        /*0x1E*/{0,1,1,0}, /*0x1F*/{0,1,0,1}, /*0x20*/{1,0,1,0}, /*0x21*/{0,1,1,0}, /*0x22*/{0,0,2,0}, /*0x23*/{0,0,1,1}, /*0x24*/{1,0,1,0}, /*0x25*/{0,1,1,0}, /*0x26*/{0,0,2,0}, /*0x27*/{0,0,1,1}, 
        /*0x28*/{1,0,1,0}, /*0x29*/{0,1,1,0}, /*0x2A*/{0,0,2,0}, /*0x2B*/{0,0,1,1}, /*0x2C*/{1,0,1,0}, /*0x2D*/{0,1,1,0}, /*0x2E*/{0,0,2,0}, /*0x2F*/{0,0,1,1}, /*0x30*/{1,0,0,1}, /*0x31*/{0,1,0,1}, 
        /*0x32*/{0,0,1,1}, /*0x33*/{0,0,0,2}, /*0x34*/{1,0,0,1}, /*0x35*/{0,1,0,1}, /*0x36*/{0,0,1,1}, /*0x37*/{0,0,0,2}, /*0x38*/{1,0,0,1}, /*0x39*/{0,1,0,1}, /*0x3A*/{0,0,1,1}, /*0x3B*/{0,0,0,2}, 
        /*0x3C*/{1,0,0,1}, /*0x3D*/{0,1,0,1}, /*0x3E*/{0,0,1,1}, /*0x3F*/{0,0,0,2}, /*0x40*/{2,0,0,0}, /*0x41*/{1,1,0,0}, /*0x42*/{1,0,1,0}, /*0x43*/{1,0,0,1}, /*0x44*/{2,0,0,0}, /*0x45*/{1,1,0,0}, 
        /*0x46*/{1,0,1,0}, /*0x47*/{1,0,0,1}, /*0x48*/{2,0,0,0}, /*0x49*/{1,1,0,0}, /*0x4A*/{1,0,1,0}, /*0x4B*/{1,0,0,1}, /*0x4C*/{2,0,0,0}, /*0x4D*/{1,1,0,0}, /*0x4E*/{1,0,1,0}, /*0x4F*/{1,0,0,1}, 
        /*0x50*/{1,1,0,0}, /*0x51*/{0,2,0,0}, /*0x52*/{0,1,1,0}, /*0x53*/{0,1,0,1}, /*0x54*/{1,1,0,0}, /*0x55*/{0,2,0,0}, /*0x56*/{0,1,1,0}, /*0x57*/{0,1,0,1}, /*0x58*/{1,1,0,0}, /*0x59*/{0,2,0,0}, 
        /*0x5A*/{0,1,1,0}, /*0x5B*/{0,1,0,1}, /*0x5C*/{1,1,0,0}, /*0x5D*/{0,2,0,0}, /*0x5E*/{0,1,1,0}, /*0x5F*/{0,1,0,1}, /*0x60*/{1,0,1,0}, /*0x61*/{0,1,1,0}, /*0x62*/{0,0,2,0}, /*0x63*/{0,0,1,1}, 
        /*0x64*/{1,0,1,0}, /*0x65*/{0,1,1,0}, /*0x66*/{0,0,2,0}, /*0x67*/{0,0,1,1}, /*0x68*/{1,0,1,0}, /*0x69*/{0,1,1,0}, /*0x6A*/{0,0,2,0}, /*0x6B*/{0,0,1,1}, /*0x6C*/{1,0,1,0}, /*0x6D*/{0,1,1,0}, 
        /*0x6E*/{0,0,2,0}, /*0x6F*/{0,0,1,1}, /*0x70*/{1,0,0,1}, /*0x71*/{0,1,0,1}, /*0x72*/{0,0,1,1}, /*0x73*/{0,0,0,2}, /*0x74*/{1,0,0,1}, /*0x75*/{0,1,0,1}, /*0x76*/{0,0,1,1}, /*0x77*/{0,0,0,2}, 
        /*0x78*/{1,0,0,1}, /*0x79*/{0,1,0,1}, /*0x7A*/{0,0,1,1}, /*0x7B*/{0,0,0,2}, /*0x7C*/{1,0,0,1}, /*0x7D*/{0,1,0,1}, /*0x7E*/{0,0,1,1}, /*0x7F*/{0,0,0,2}, /*0x80*/{2,0,0,0}, /*0x81*/{1,1,0,0}, 
        /*0x82*/{1,0,1,0}, /*0x83*/{1,0,0,1}, /*0x84*/{2,0,0,0}, /*0x85*/{1,1,0,0}, /*0x86*/{1,0,1,0}, /*0x87*/{1,0,0,1}, /*0x88*/{2,0,0,0}, /*0x89*/{1,1,0,0}, /*0x8A*/{1,0,1,0}, /*0x8B*/{1,0,0,1}, 
        /*0x8C*/{2,0,0,0}, /*0x8D*/{1,1,0,0}, /*0x8E*/{1,0,1,0}, /*0x8F*/{1,0,0,1}, /*0x90*/{1,1,0,0}, /*0x91*/{0,2,0,0}, /*0x92*/{0,1,1,0}, /*0x93*/{0,1,0,1}, /*0x94*/{1,1,0,0}, /*0x95*/{0,2,0,0}, 
        /*0x96*/{0,1,1,0}, /*0x97*/{0,1,0,1}, /*0x98*/{1,1,0,0}, /*0x99*/{0,2,0,0}, /*0x9A*/{0,1,1,0}, /*0x9B*/{0,1,0,1}, /*0x9C*/{1,1,0,0}, /*0x9D*/{0,2,0,0}, /*0x9E*/{0,1,1,0}, /*0x9F*/{0,1,0,1}, 
        /*0xA0*/{1,0,1,0}, /*0xA1*/{0,1,1,0}, /*0xA2*/{0,0,2,0}, /*0xA3*/{0,0,1,1}, /*0xA4*/{1,0,1,0}, /*0xA5*/{0,1,1,0}, /*0xA6*/{0,0,2,0}, /*0xA7*/{0,0,1,1}, /*0xA8*/{1,0,1,0}, /*0xA9*/{0,1,1,0}, 
        /*0xAA*/{0,0,2,0}, /*0xAB*/{0,0,1,1}, /*0xAC*/{1,0,1,0}, /*0xAD*/{0,1,1,0}, /*0xAE*/{0,0,2,0}, /*0xAF*/{0,0,1,1}, /*0xB0*/{1,0,0,1}, /*0xB1*/{0,1,0,1}, /*0xB2*/{0,0,1,1}, /*0xB3*/{0,0,0,2}, 
        /*0xB4*/{1,0,0,1}, /*0xB5*/{0,1,0,1}, /*0xB6*/{0,0,1,1}, /*0xB7*/{0,0,0,2}, /*0xB8*/{1,0,0,1}, /*0xB9*/{0,1,0,1}, /*0xBA*/{0,0,1,1}, /*0xBB*/{0,0,0,2}, /*0xBC*/{1,0,0,1}, /*0xBD*/{0,1,0,1}, 
        /*0xBE*/{0,0,1,1}, /*0xBF*/{0,0,0,2}, /*0xC0*/{2,0,0,0}, /*0xC1*/{1,1,0,0}, /*0xC2*/{1,0,1,0}, /*0xC3*/{1,0,0,1}, /*0xC4*/{2,0,0,0}, /*0xC5*/{1,1,0,0}, /*0xC6*/{1,0,1,0}, /*0xC7*/{1,0,0,1}, 
        /*0xC8*/{2,0,0,0}, /*0xC9*/{1,1,0,0}, /*0xCA*/{1,0,1,0}, /*0xCB*/{1,0,0,1}, /*0xCC*/{2,0,0,0}, /*0xCD*/{1,1,0,0}, /*0xCE*/{1,0,1,0}, /*0xCF*/{1,0,0,1}, /*0xD0*/{1,1,0,0}, /*0xD1*/{0,2,0,0}, 
        /*0xD2*/{0,1,1,0}, /*0xD3*/{0,1,0,1}, /*0xD4*/{1,1,0,0}, /*0xD5*/{0,2,0,0}, /*0xD6*/{0,1,1,0}, /*0xD7*/{0,1,0,1}, /*0xD8*/{1,1,0,0}, /*0xD9*/{0,2,0,0}, /*0xDA*/{0,1,1,0}, /*0xDB*/{0,1,0,1}, 
        /*0xDC*/{1,1,0,0}, /*0xDD*/{0,2,0,0}, /*0xDE*/{0,1,1,0}, /*0xDF*/{0,1,0,1}, /*0xE0*/{1,0,1,0}, /*0xE1*/{0,1,1,0}, /*0xE2*/{0,0,2,0}, /*0xE3*/{0,0,1,1}, /*0xE4*/{1,0,1,0}, /*0xE5*/{0,1,1,0}, 
        /*0xE6*/{0,0,2,0}, /*0xE7*/{0,0,1,1}, /*0xE8*/{1,0,1,0}, /*0xE9*/{0,1,1,0}, /*0xEA*/{0,0,2,0}, /*0xEB*/{0,0,1,1}, /*0xEC*/{1,0,1,0}, /*0xED*/{0,1,1,0}, /*0xEE*/{0,0,2,0}, /*0xEF*/{0,0,1,1}, 
        /*0xF0*/{1,0,0,1}, /*0xF1*/{0,1,0,1}, /*0xF2*/{0,0,1,1}, /*0xF3*/{0,0,0,2}, /*0xF4*/{1,0,0,1}, /*0xF5*/{0,1,0,1}, /*0xF6*/{0,0,1,1}, /*0xF7*/{0,0,0,2}, /*0xF8*/{1,0,0,1}, /*0xF9*/{0,1,0,1}, 
        /*0xFA*/{0,0,1,1}, /*0xFB*/{0,0,0,2}, /*0xFC*/{1,0,0,1}, /*0xFD*/{0,1,0,1}, /*0xFE*/{0,0,1,1}, /*0xFF*/{0,0,0,2}
    };
    static const char s_lut_ch2[256][4] = {
        /*0x00*/{2,0,0,0}, /*0x01*/{2,0,0,0}, /*0x02*/{2,0,0,0}, /*0x03*/{2,0,0,0}, /*0x04*/{1,1,0,0}, /*0x05*/{1,1,0,0}, /*0x06*/{1,1,0,0}, /*0x07*/{1,1,0,0}, /*0x08*/{1,0,1,0}, /*0x09*/{1,0,1,0}, 
        /*0x0A*/{1,0,1,0}, /*0x0B*/{1,0,1,0}, /*0x0C*/{1,0,0,1}, /*0x0D*/{1,0,0,1}, /*0x0E*/{1,0,0,1}, /*0x0F*/{1,0,0,1}, /*0x10*/{2,0,0,0}, /*0x11*/{2,0,0,0}, /*0x12*/{2,0,0,0}, /*0x13*/{2,0,0,0}, 
        /*0x14*/{1,1,0,0}, /*0x15*/{1,1,0,0}, /*0x16*/{1,1,0,0}, /*0x17*/{1,1,0,0}, /*0x18*/{1,0,1,0}, /*0x19*/{1,0,1,0}, /*0x1A*/{1,0,1,0}, /*0x1B*/{1,0,1,0}, /*0x1C*/{1,0,0,1}, /*0x1D*/{1,0,0,1}, 
        /*0x1E*/{1,0,0,1}, /*0x1F*/{1,0,0,1}, /*0x20*/{2,0,0,0}, /*0x21*/{2,0,0,0}, /*0x22*/{2,0,0,0}, /*0x23*/{2,0,0,0}, /*0x24*/{1,1,0,0}, /*0x25*/{1,1,0,0}, /*0x26*/{1,1,0,0}, /*0x27*/{1,1,0,0}, 
        /*0x28*/{1,0,1,0}, /*0x29*/{1,0,1,0}, /*0x2A*/{1,0,1,0}, /*0x2B*/{1,0,1,0}, /*0x2C*/{1,0,0,1}, /*0x2D*/{1,0,0,1}, /*0x2E*/{1,0,0,1}, /*0x2F*/{1,0,0,1}, /*0x30*/{2,0,0,0}, /*0x31*/{2,0,0,0}, 
        /*0x32*/{2,0,0,0}, /*0x33*/{2,0,0,0}, /*0x34*/{1,1,0,0}, /*0x35*/{1,1,0,0}, /*0x36*/{1,1,0,0}, /*0x37*/{1,1,0,0}, /*0x38*/{1,0,1,0}, /*0x39*/{1,0,1,0}, /*0x3A*/{1,0,1,0}, /*0x3B*/{1,0,1,0}, 
        /*0x3C*/{1,0,0,1}, /*0x3D*/{1,0,0,1}, /*0x3E*/{1,0,0,1}, /*0x3F*/{1,0,0,1}, /*0x40*/{1,1,0,0}, /*0x41*/{1,1,0,0}, /*0x42*/{1,1,0,0}, /*0x43*/{1,1,0,0}, /*0x44*/{0,2,0,0}, /*0x45*/{0,2,0,0}, 
        /*0x46*/{0,2,0,0}, /*0x47*/{0,2,0,0}, /*0x48*/{0,1,1,0}, /*0x49*/{0,1,1,0}, /*0x4A*/{0,1,1,0}, /*0x4B*/{0,1,1,0}, /*0x4C*/{0,1,0,1}, /*0x4D*/{0,1,0,1}, /*0x4E*/{0,1,0,1}, /*0x4F*/{0,1,0,1}, 
        /*0x50*/{1,1,0,0}, /*0x51*/{1,1,0,0}, /*0x52*/{1,1,0,0}, /*0x53*/{1,1,0,0}, /*0x54*/{0,2,0,0}, /*0x55*/{0,2,0,0}, /*0x56*/{0,2,0,0}, /*0x57*/{0,2,0,0}, /*0x58*/{0,1,1,0}, /*0x59*/{0,1,1,0}, 
        /*0x5A*/{0,1,1,0}, /*0x5B*/{0,1,1,0}, /*0x5C*/{0,1,0,1}, /*0x5D*/{0,1,0,1}, /*0x5E*/{0,1,0,1}, /*0x5F*/{0,1,0,1}, /*0x60*/{1,1,0,0}, /*0x61*/{1,1,0,0}, /*0x62*/{1,1,0,0}, /*0x63*/{1,1,0,0}, 
        /*0x64*/{0,2,0,0}, /*0x65*/{0,2,0,0}, /*0x66*/{0,2,0,0}, /*0x67*/{0,2,0,0}, /*0x68*/{0,1,1,0}, /*0x69*/{0,1,1,0}, /*0x6A*/{0,1,1,0}, /*0x6B*/{0,1,1,0}, /*0x6C*/{0,1,0,1}, /*0x6D*/{0,1,0,1}, 
        /*0x6E*/{0,1,0,1}, /*0x6F*/{0,1,0,1}, /*0x70*/{1,1,0,0}, /*0x71*/{1,1,0,0}, /*0x72*/{1,1,0,0}, /*0x73*/{1,1,0,0}, /*0x74*/{0,2,0,0}, /*0x75*/{0,2,0,0}, /*0x76*/{0,2,0,0}, /*0x77*/{0,2,0,0}, 
        /*0x78*/{0,1,1,0}, /*0x79*/{0,1,1,0}, /*0x7A*/{0,1,1,0}, /*0x7B*/{0,1,1,0}, /*0x7C*/{0,1,0,1}, /*0x7D*/{0,1,0,1}, /*0x7E*/{0,1,0,1}, /*0x7F*/{0,1,0,1}, /*0x80*/{1,0,1,0}, /*0x81*/{1,0,1,0}, 
        /*0x82*/{1,0,1,0}, /*0x83*/{1,0,1,0}, /*0x84*/{0,1,1,0}, /*0x85*/{0,1,1,0}, /*0x86*/{0,1,1,0}, /*0x87*/{0,1,1,0}, /*0x88*/{0,0,2,0}, /*0x89*/{0,0,2,0}, /*0x8A*/{0,0,2,0}, /*0x8B*/{0,0,2,0}, 
        /*0x8C*/{0,0,1,1}, /*0x8D*/{0,0,1,1}, /*0x8E*/{0,0,1,1}, /*0x8F*/{0,0,1,1}, /*0x90*/{1,0,1,0}, /*0x91*/{1,0,1,0}, /*0x92*/{1,0,1,0}, /*0x93*/{1,0,1,0}, /*0x94*/{0,1,1,0}, /*0x95*/{0,1,1,0}, 
        /*0x96*/{0,1,1,0}, /*0x97*/{0,1,1,0}, /*0x98*/{0,0,2,0}, /*0x99*/{0,0,2,0}, /*0x9A*/{0,0,2,0}, /*0x9B*/{0,0,2,0}, /*0x9C*/{0,0,1,1}, /*0x9D*/{0,0,1,1}, /*0x9E*/{0,0,1,1}, /*0x9F*/{0,0,1,1}, 
        /*0xA0*/{1,0,1,0}, /*0xA1*/{1,0,1,0}, /*0xA2*/{1,0,1,0}, /*0xA3*/{1,0,1,0}, /*0xA4*/{0,1,1,0}, /*0xA5*/{0,1,1,0}, /*0xA6*/{0,1,1,0}, /*0xA7*/{0,1,1,0}, /*0xA8*/{0,0,2,0}, /*0xA9*/{0,0,2,0}, 
        /*0xAA*/{0,0,2,0}, /*0xAB*/{0,0,2,0}, /*0xAC*/{0,0,1,1}, /*0xAD*/{0,0,1,1}, /*0xAE*/{0,0,1,1}, /*0xAF*/{0,0,1,1}, /*0xB0*/{1,0,1,0}, /*0xB1*/{1,0,1,0}, /*0xB2*/{1,0,1,0}, /*0xB3*/{1,0,1,0}, 
        /*0xB4*/{0,1,1,0}, /*0xB5*/{0,1,1,0}, /*0xB6*/{0,1,1,0}, /*0xB7*/{0,1,1,0}, /*0xB8*/{0,0,2,0}, /*0xB9*/{0,0,2,0}, /*0xBA*/{0,0,2,0}, /*0xBB*/{0,0,2,0}, /*0xBC*/{0,0,1,1}, /*0xBD*/{0,0,1,1}, 
        /*0xBE*/{0,0,1,1}, /*0xBF*/{0,0,1,1}, /*0xC0*/{1,0,0,1}, /*0xC1*/{1,0,0,1}, /*0xC2*/{1,0,0,1}, /*0xC3*/{1,0,0,1}, /*0xC4*/{0,1,0,1}, /*0xC5*/{0,1,0,1}, /*0xC6*/{0,1,0,1}, /*0xC7*/{0,1,0,1}, 
        /*0xC8*/{0,0,1,1}, /*0xC9*/{0,0,1,1}, /*0xCA*/{0,0,1,1}, /*0xCB*/{0,0,1,1}, /*0xCC*/{0,0,0,2}, /*0xCD*/{0,0,0,2}, /*0xCE*/{0,0,0,2}, /*0xCF*/{0,0,0,2}, /*0xD0*/{1,0,0,1}, /*0xD1*/{1,0,0,1}, 
        /*0xD2*/{1,0,0,1}, /*0xD3*/{1,0,0,1}, /*0xD4*/{0,1,0,1}, /*0xD5*/{0,1,0,1}, /*0xD6*/{0,1,0,1}, /*0xD7*/{0,1,0,1}, /*0xD8*/{0,0,1,1}, /*0xD9*/{0,0,1,1}, /*0xDA*/{0,0,1,1}, /*0xDB*/{0,0,1,1}, 
        /*0xDC*/{0,0,0,2}, /*0xDD*/{0,0,0,2}, /*0xDE*/{0,0,0,2}, /*0xDF*/{0,0,0,2}, /*0xE0*/{1,0,0,1}, /*0xE1*/{1,0,0,1}, /*0xE2*/{1,0,0,1}, /*0xE3*/{1,0,0,1}, /*0xE4*/{0,1,0,1}, /*0xE5*/{0,1,0,1}, 
        /*0xE6*/{0,1,0,1}, /*0xE7*/{0,1,0,1}, /*0xE8*/{0,0,1,1}, /*0xE9*/{0,0,1,1}, /*0xEA*/{0,0,1,1}, /*0xEB*/{0,0,1,1}, /*0xEC*/{0,0,0,2}, /*0xED*/{0,0,0,2}, /*0xEE*/{0,0,0,2}, /*0xEF*/{0,0,0,2}, 
        /*0xF0*/{1,0,0,1}, /*0xF1*/{1,0,0,1}, /*0xF2*/{1,0,0,1}, /*0xF3*/{1,0,0,1}, /*0xF4*/{0,1,0,1}, /*0xF5*/{0,1,0,1}, /*0xF6*/{0,1,0,1}, /*0xF7*/{0,1,0,1}, /*0xF8*/{0,0,1,1}, /*0xF9*/{0,0,1,1}, 
        /*0xFA*/{0,0,1,1}, /*0xFB*/{0,0,1,1}, /*0xFC*/{0,0,0,2}, /*0xFD*/{0,0,0,2}, /*0xFE*/{0,0,0,2}, /*0xFF*/{0,0,0,2}
    };
    int i;
    for (i=0; i<256; i++) {
        hist_ch1[0] += hist8bit[i] * (int)s_lut_ch1[i][0];
        hist_ch1[1] += hist8bit[i] * (int)s_lut_ch1[i][1];
        hist_ch1[2] += hist8bit[i] * (int)s_lut_ch1[i][2];
        hist_ch1[3] += hist8bit[i] * (int)s_lut_ch1[i][3];
        hist_ch2[0] += hist8bit[i] * (int)s_lut_ch2[i][0];
        hist_ch2[1] += hist8bit[i] * (int)s_lut_ch2[i][1];
        hist_ch2[2] += hist8bit[i] * (int)s_lut_ch2[i][2];
        hist_ch2[3] += hist8bit[i] * (int)s_lut_ch2[i][3];
    }

    if (0) {
        // Generate the two LUTs above
        int i, j;
        printf("        ");
        for (i=0; i<256; i++) {
                int h[4] = {0};
                int v = (i&3) | ((i>>2)&12);        // Channel 1, 8bit **xx**yy -> 4bit xxyy
                //int v = ((i>>2)&3) | ((i>>4)&12); // Channel 2, 8bit xx**yy** -> 4bit xxyy
                for (j=0; j<2; j++) {
                        h[(v&3)]++;
                        v = v>>2;
                }
                printf("/*0x%02X*/{%d,%d,%d,%d}, ", i, h[0], h[1], h[2], h[3]);
                if (((i+1)%10) == 0) { printf("\n        "); }
        }
        printf("\n");
    }
}

void cpu_histogramtransform_8bit_to_2bit_4ch(const hist_t* hist8bit, hist_t* hist_ch1, hist_t* hist_ch2, hist_t* hist_ch3, hist_t* hist_ch4)
{
    int_fast16_t i;
    for (i=0; i<256; i++) {
        int v1 = (i&3), v2 = (i>>2)&3, v3 = (i>>4)&3, v4 = (i>>6)&3;
        hist_ch1[v1] += hist8bit[i];
        hist_ch2[v2] += hist8bit[i];
        hist_ch3[v3] += hist8bit[i];
        hist_ch4[v4] += hist8bit[i];
    }
}

void cpu_histogramtransform_8bit4S_to_2bit_8ch(const hist_t* hist8bit4S, hist_t* hist_multichannel)
{
    const int nlevels = 4;
    for (int bin=0; bin<256; bin++) {
        for (int subbytenr=0; subbytenr<4; subbytenr++) {
            const hist_t count8bit = hist8bit4S[4*bin + subbytenr];
            const int baseCh = (4*subbytenr) % 8; // 4ch per raw byte, modulo 8ch out
            const int v1 = (bin&3), v2 = (bin>>2)&3, v3 = (bin>>4)&3, v4 = (bin>>6)&3;
            hist_multichannel[nlevels*(baseCh+0) + v1] += count8bit;
            hist_multichannel[nlevels*(baseCh+1) + v2] += count8bit;
            hist_multichannel[nlevels*(baseCh+2) + v3] += count8bit;
            hist_multichannel[nlevels*(baseCh+3) + v4] += count8bit;
        }
    }
}

void cpu_histogramtransform_8bit4S_to_2bit_16ch(const hist_t* hist8bit4S, hist_t* hist_multichannel)
{
    const int nlevels = 4;
    for (int bin=0; bin<256; bin++) {
        for (int subbytenr=0; subbytenr<4; subbytenr++) {
            const hist_t count8bit = hist8bit4S[4*bin + subbytenr];
            const int baseCh = (4*subbytenr) % 16; // 4ch per raw byte, modulo 16ch out
            const int v1 = (bin&3), v2 = (bin>>2)&3, v3 = (bin>>4)&3, v4 = (bin>>6)&3;
            hist_multichannel[nlevels*(baseCh+0) + v1] += count8bit;
            hist_multichannel[nlevels*(baseCh+1) + v2] += count8bit;
            hist_multichannel[nlevels*(baseCh+2) + v3] += count8bit;
            hist_multichannel[nlevels*(baseCh+3) + v4] += count8bit;
        }
    }
}

void cpu_histogramtransform_24bit_to_3bit_1ch(const hist_t* hist24bit, hist_t* hist_ch1)
{
    // Note: this is slow, and also the 24-bit histogramming is slow
    int_fast32_t i;
    for (i=0; i<(1<<24); i++) {
        int v1 = (i&7);
        int v2 = (i>>3)&7;
        int v3 = (i>>6)&7;
        int v4 = (i>>9)&7;
        int v5 = (i>>12)&7;
        int v6 = (i>>15)&7;
        int v7 = (i>>18)&7;
        int v8 = (i>>21)&7;
        hist_ch1[v1] += hist24bit[i];
        hist_ch1[v2] += hist24bit[i];
        hist_ch1[v3] += hist24bit[i];
        hist_ch1[v4] += hist24bit[i];
        hist_ch1[v5] += hist24bit[i];
        hist_ch1[v6] += hist24bit[i];
        hist_ch1[v7] += hist24bit[i];
        hist_ch1[v8] += hist24bit[i];
    }
}


////////////////////////////////////////////////////////////////////////////////////////////////////
// CPU-SIDE FAST HISTOGRAMMING WRAPPERS
// These produce a temporary 8-bit histogram (or histograms) and then use
// the transform functions to transition from 8-bit to 2-bit data histograms.
////////////////////////////////////////////////////////////////////////////////////////////////////

/// cpu_histogram_2bit_1ch_fast(). Speed on CPU: 96000000 samples in 12.576 msec, 7633.6 Ms/s, 1908.397 MB/s
static void cpu_histogram_2bit_1ch_fast(const uint8_t* __restrict__ src, const size_t Nbyte, hist_t* hist)
{
    hist_t hist8bit[256] = {0};
    cpu_histogram_8bit_1ch(src, Nbyte, hist8bit);
    cpu_histogramtransform_8bit_to_2bit_1ch((const hist_t*)hist8bit, hist);
}

/// cpu_histogram_2bit_2ch_fast(). Speed on CPU: 96000000 samples in 12.587 msec, 7626.9 Ms/s, 1906.729 MB/s
static void cpu_histogram_2bit_2ch_fast(const uint8_t* __restrict__ src, const size_t Nbyte, hist_t* hist_ch1, hist_t* hist_ch2)
{
    hist_t hist8bit[256] = {0};
    cpu_histogram_8bit_1ch(src, Nbyte, hist8bit);
    cpu_histogramtransform_8bit_to_2bit_2ch((const hist_t*)hist8bit, hist_ch1, hist_ch2);
}

/// cpu_histogram_2bit_4ch_fast(). Speed on CPU: 96000000 samples in 12.562 msec, 7642.1 Ms/s, 1910.524 MB/s
static void cpu_histogram_2bit_4ch_fast(const uint8_t* __restrict__ src, const size_t Nbyte, hist_t* __restrict__ hist_ch1, hist_t* __restrict__ hist_ch2,
                                        hist_t* __restrict__ hist_ch3, hist_t* __restrict__ hist_ch4)
{
    hist_t hist8bit[256] = {0};
    cpu_histogram_8bit_1ch(src, Nbyte, hist8bit);
    cpu_histogramtransform_8bit_to_2bit_4ch((const hist_t*)hist8bit, hist_ch1, hist_ch2, hist_ch3, hist_ch4);
}

/// cpu_histogram_2bit_8ch_fast()
static void cpu_histogram_2bit_8ch_fast(const uint8_t* __restrict__ src, const size_t Nbyte, hist_t* __restrict__ hist_multichannel)
{
    hist_t hist8bit4S[4*256] = {0};
    cpu_histogram_8bit_1ch_4S(src, Nbyte, hist8bit4S);
    cpu_histogramtransform_8bit4S_to_2bit_8ch((const hist_t*)hist8bit4S, hist_multichannel);
}

/// cpu_histogram_2bit_16ch_fast()
static void cpu_histogram_2bit_16ch_fast(const uint8_t* __restrict__ src, const size_t Nbyte, hist_t* __restrict__ hist_multichannel)
{
    hist_t hist8bit4S[4*256] = {0};
    cpu_histogram_8bit_1ch_4S(src, Nbyte, hist8bit4S);
    cpu_histogramtransform_8bit4S_to_2bit_16ch((const hist_t*)hist8bit4S, hist_multichannel);
}

/// Wrapper for CPU histogramming references
void cpu_histogram(int nbits, int nch, uint8_t* raw, size_t Nbytes, /*ch1 or multi-ch:*/hist_t* hist_ch1, hist_t* hist_ch2, hist_t* hist_ch3, hist_t* hist_ch4)
{
    hist_t* hist_multichannel = hist_ch1;
    if ((nbits==2) && (nch==1)) {                                     // -- Perf. on single core of Xeon E5-2667 v3 3.20GHz
        // cpu_histogram_2bit_1ch(raw, Nbytes, hist_ch1);             // 1738.3 Ms/s, 434.586 MB/s
        cpu_histogram_2bit_1ch_fast(raw, Nbytes, hist_ch1);           // 7633.6 Ms/s, 1908.397 MB/s
    } else if ((nbits==2) && (nch==2)) {
        // cpu_histogram_2bit_2ch(raw, Nbytes, hist_ch1, hist_ch2);   // 1736.0 Ms/s, 434.012 MB/s
        cpu_histogram_2bit_2ch_fast(raw, Nbytes, hist_ch1, hist_ch2); // 7617.2 Ms/s, 1904.308 MB/s
    } else if ((nbits==2) && (nch==4)) {
        //cpu_histogram_2bit_4ch(raw, Nbytes, hist_ch1, hist_ch2, hist_ch3, hist_ch4);    // 1852.7 Ms/s, 463.177 MB/s
        cpu_histogram_2bit_4ch_fast(raw, Nbytes, hist_ch1, hist_ch2, hist_ch3, hist_ch4); // 7631.2 Ms/s, 1907.790 MB/s
    } else if ((nbits==2) && (nch==8)) {
        cpu_histogram_2bit_8ch_fast(raw, Nbytes, hist_multichannel);
    } else if ((nbits==2) && (nch==16)) {
        cpu_histogram_2bit_16ch_fast(raw, Nbytes, hist_multichannel);
    } else if ((nbits==3) && (nch==1)) {
        //cpu_histogram_3bit_1ch(raw, Nbytes, hist_ch1);              // 1431.3 Ms/s, 536.721 MB/s
        //cpu_histogram_3bit_1ch_V2(raw, Nbytes, hist_ch1);           // 1618.1 Ms/s, 606.781 MB/s
        cpu_histogram_3bit_1ch_V3(raw, Nbytes, hist_ch1);             // 2650.7 Ms/s, 993.995 MB/s
    } else if ((nbits==3) && (nch==2)) {
        //cpu_histogram_3bit_2ch(raw, Nbytes, hist_ch1, hist_ch2);    // 1447.0 Ms/s, 542.630 MB/s
        cpu_histogram_3bit_2ch_V3(raw, Nbytes, hist_ch1, hist_ch2);   // 2801.7 Ms/s, 1050.650 MB/s
    } else if ((nbits==8) && (nch==1)) {
        cpu_histogram_8bit_1ch(raw, Nbytes, hist_ch1);                // 1907.3 Ms/s, 1907.335 MB/s
    } else if ((nbits==24) && (nch==1)) {
        cpu_histogram_24bit_1ch(raw, Nbytes, hist_ch1);               //  ~45 Ms/s only
    } else {
        printf("Error: no CPU reference histogram func assigned for %d-bit %d-channel layout!\n", nbits, nch);
        exit(1);
    }
}



////////////////////////////////////////////////////////////////////////////////////////////////////
// HISTOGRAM KERNELS GPU/CUDA - NAIVE VERSIONS
////////////////////////////////////////////////////////////////////////////////////////////////////

/** cu_histogram_8bit_1ch()
 * For 8-bit data. Assume all bytes are from the same channel.
 * Assume all samples are from the same channel.
 */
__global__ void cu_histogram_8bit_1ch(const uint8_t* __restrict__ src, const size_t Nbyte, hist_t* __restrict__ hist)
{
    const int N_TH = 32;
    const int N_LEV = 256;
    __shared__ hist_t smem[N_TH*N_LEV];

    int i = blockIdx.x * blockDim.x + threadIdx.x;
    int N = blockDim.x * gridDim.x;
    int si = (i % N_TH) * N_LEV;

    for (int i=0; i<N_LEV; i++) { smem[si+i] = 0; }
    __syncthreads();

    while (i < Nbyte) {
        uint8_t w = src[i];
        atomicAdd(&smem[si + w], 1);
        i += N;
    }
    __syncthreads();

    for (int n=0; n<N_LEV; n++) {
        atomicAdd(&hist[n], smem[si+n]);
    }
}

/** cu_histogram_2bit_1ch()
 * For 2-bit data, every 1 byte contains a set of 4 samples.
 * Assume all samples are from the same channel.
 * Speed on GPU: 96000000 samples in 1.253 msec, 76616.1 Ms/s, 19154.030 MB/s
 */
__global__ void cu_histogram_2bit_1ch(const uint8_t* __restrict__ src, const size_t Nbyte, hist_t* hist)
{
    const int N_TH = 32;
    const int N_LEV = 4;
    __shared__ hist_t smem[N_TH*N_LEV];

    int i = blockIdx.x * blockDim.x + threadIdx.x;
    int N = blockDim.x * gridDim.x;
    int si = (i % N_TH) * N_LEV;

    for (int i=0; i<N_LEV; i++) { smem[si+i] = 0; }
    __syncthreads();

    while (i < Nbyte) {
        uint8_t w = src[i];
        // note that GPU have no deep instruction pipelining and the 4-subhistogram trick in cpu_histogram_2bit_1ch() doesn't help here
        // note2 that shared memory with atomicAdd is about 5x faster than 4 registers (strangely!)
        atomicAdd(&smem[si + (w >> 0) & 3], 1);
        atomicAdd(&smem[si + (w >> 2) & 3], 1);
        atomicAdd(&smem[si + (w >> 4) & 3], 1);
        atomicAdd(&smem[si + (w >> 6) & 3], 1);
        i += N;
    }
    __syncthreads();

    for (int n=0; n<N_LEV; n++) {
        atomicAdd(&hist[n], smem[si+n]);
    }
}

/** cu_histogram_2bit_1ch()
 * For 2-bit data, every 1 byte contains a set of 4 samples.
 * Assume all odd samples are from the first, and even samples from the second channel.
 * Speed on GPU: 96000000 samples in 1.259 msec, 76251.0 Ms/s, 19062.748 MB/s
 */
__global__ void cu_histogram_2bit_2ch(const uint8_t* __restrict__ src, const size_t Nbyte, hist_t* hist_ch1, hist_t* hist_ch2)
{
    const int N_TH = 32;
    const int N_LEV = 4;
    __shared__ hist_t smem1[N_TH*N_LEV];
    __shared__ hist_t smem2[N_TH*N_LEV];

    int i = blockIdx.x * blockDim.x + threadIdx.x;
    int N = blockDim.x * gridDim.x;
    int si = (i % N_TH) * N_LEV;

    for (int i=0; i<N_LEV; i++) {
        smem1[si+i] = 0;
        smem2[si+i] = 0;
    }
    __syncthreads();

    while (i < Nbyte) {
        uint8_t w = src[i];
        atomicAdd(&smem1[si + (w >> 0) & 3], 1);
        atomicAdd(&smem2[si + (w >> 2) & 3], 1);
        atomicAdd(&smem1[si + (w >> 4) & 3], 1);
        atomicAdd(&smem2[si + (w >> 6) & 3], 1);
        i += N;
    }
    __syncthreads();

    for (int n=0; n<N_LEV; n++) {
        atomicAdd(&hist_ch1[n], smem1[si+n]);
        atomicAdd(&hist_ch2[n], smem2[si+n]);
    }
}

/** cu_histogram_3bit_1ch()
 * For 3-bit data, every 3 bytes (24 bit) contain a set of 8 samples.
 * Assume all samples are from the same channel.
 * Speed on GPU: 64000000 samples in 0.594 msec, 107744.1 Ms/s, 40404.040 MB/s
 */
__global__ void cu_histogram_3bit_1ch_old(const uint8_t* __restrict__ src, const size_t Nbyte, hist_t* __restrict__ hist)
{
    const int N_TH = 32;
    const int N_LEV = 8;
    __shared__ hist_t smem[N_TH*N_LEV];

    int i = blockIdx.x * blockDim.x + threadIdx.x;
    int N = blockDim.x * gridDim.x;
    int si = (i % N_TH) * N_LEV;

    for (int i=0; i<N_LEV; i++) { smem[si+i] = 0; }
    __syncthreads();

    while ((3*i+2) < Nbyte) {

        // Load pieces of 24-bit word (unaligned!)
        uint8_t b0 = src[3*i+0];
        uint8_t b1 = src[3*i+1];
        uint8_t b2 = src[3*i+2];

        // Generate 24-bit word
        uint32_t w = (((uint32_t)b2) << 16) | (((uint32_t)b1) << 8) | ((uint32_t)b0);

        atomicAdd(&smem[si + (w >>  0) & 7], 1);
        atomicAdd(&smem[si + (w >>  3) & 7], 1);
        atomicAdd(&smem[si + (w >>  6) & 7], 1);
        atomicAdd(&smem[si + (w >>  9) & 7], 1);
        atomicAdd(&smem[si + (w >> 12) & 7], 1);
        atomicAdd(&smem[si + (w >> 15) & 7], 1);
        atomicAdd(&smem[si + (w >> 18) & 7], 1);
        atomicAdd(&smem[si + (w >> 21) & 7], 1);

        i += N;
    }
    __syncthreads();

    for (int n=0; n<N_LEV; n++) {
        atomicAdd(&hist[n], smem[si+n]);
    }

}

/* Faster version
 * Speed on GPU: 64000000 samples in 0.313 msec, 204472.8 Ms/s, 76677.316 MB/s
 */
__global__ void cu_histogram_3bit_1ch(const uint8_t* __restrict__ src, const size_t Nbyte, hist_t* __restrict__ hist)
{
    __shared__ hist_t smem[8][32];

    const int i = blockIdx.x * blockDim.x + threadIdx.x;
    const int N = blockDim.x * gridDim.x;
    const int si = (i % 32);

    #pragma unroll
    for (int n=0; n<8; n++) {
        smem[n][si] = 0;
    }
    __syncthreads();

    int pos = 3*i;
    while ((pos+2) < Nbyte) {

        // Load pieces of 24-bit word (unaligned!)
        uint8_t b0 = src[pos+0];
        uint8_t b1 = src[pos+1];
        uint8_t b2 = src[pos+2];

        // Generate 24-bit word
        uint32_t w = (((uint32_t)b2) << 16) | (((uint32_t)b1) << 8) | ((uint32_t)b0);

        // Add into shared mem
        smem[(w >>  0) & 7][si]++;
        smem[(w >>  3) & 7][si]++;
        smem[(w >>  6) & 7][si]++;
        smem[(w >>  9) & 7][si]++;
        smem[(w >> 12) & 7][si]++;
        smem[(w >> 15) & 7][si]++;
        smem[(w >> 18) & 7][si]++;
        smem[(w >> 21) & 7][si]++;

        pos += 3*N;
    }
    __syncthreads();

    #pragma unroll
    for (int n=0; n<8; n++) {
        atomicAdd(&hist[n], smem[n][si]);
    }

}

/** cu_histogram_3bit_2ch()
 * For 3-bit data, every 3 bytes (24 bit) contain a set of 8 samples.
 * Assume all odd samples are from the first, and even samples from the second channel.
 * Speed on GPU: 64000000 samples in 0.596 msec, 107382.6 Ms/s, 40268.456 MB/s
 */
__global__ void cu_histogram_3bit_2ch_old(const uint8_t* __restrict__ src, const size_t Nbyte, hist_t* __restrict__ hist_ch1, hist_t* __restrict__ hist_ch2)
{
    const int N_TH = 32;
    const int N_LEV = 8;
    __shared__ hist_t smem1[N_TH*N_LEV];
    __shared__ hist_t smem2[N_TH*N_LEV];

    int i = blockIdx.x * blockDim.x + threadIdx.x;
    int N = blockDim.x * gridDim.x;
    int si = (i % N_TH) * N_LEV;

    #pragma unroll
    for (int i=0; i<N_LEV; i++) {
        smem1[si+i] = 0;
        smem2[si+i] = 0;
    }
    __syncthreads();

    while ((3*i+2) < Nbyte) {

        // Load pieces of 24-bit word (unaligned!)
        uint8_t b0 = src[3*i+0];
        uint8_t b1 = src[3*i+1];
        uint8_t b2 = src[3*i+2];

        // Generate 24-bit word
        uint32_t w = (((uint32_t)b2) << 16) | (((uint32_t)b1) << 8) | ((uint32_t)b0);

        // Add into shared mem
        atomicAdd(&smem1[si + (w >>  0) & 7], 1);
        atomicAdd(&smem2[si + (w >>  3) & 7], 1);
        atomicAdd(&smem1[si + (w >>  6) & 7], 1);
        atomicAdd(&smem2[si + (w >>  9) & 7], 1);
        atomicAdd(&smem1[si + (w >> 12) & 7], 1);
        atomicAdd(&smem2[si + (w >> 15) & 7], 1);
        atomicAdd(&smem1[si + (w >> 18) & 7], 1);
        atomicAdd(&smem2[si + (w >> 21) & 7], 1);

        i += N;
    }
    __syncthreads();

    for (int n=0; n<N_LEV; n++) {
        atomicAdd(&hist_ch1[n], smem1[si+n]);
        atomicAdd(&hist_ch2[n], smem2[si+n]);
    }
}

/*
 * Faster version
 * Speed on GPU: 64000000 samples in 0.325 msec, 196923.1 Ms/s, 73846.154 MB/s
 */
__global__ void cu_histogram_3bit_2ch(const uint8_t* __restrict__ src, const size_t Nbyte, hist_t* __restrict__ hist_ch1, hist_t* __restrict__ hist_ch2)
{
    __shared__ hist_t smem1[8][32];
    __shared__ hist_t smem2[8][32];

    const int i = blockIdx.x * blockDim.x + threadIdx.x;
    const int N = blockDim.x * gridDim.x;
    const int si = (i % 32);

    #pragma unroll
    for (int n=0; n<8; n++) {
        smem1[n][si] = 0;
        smem2[n][si] = 0;
    }
    __syncthreads();

    int pos = 3*i;
    while ((pos+2) < Nbyte) {

        // Load pieces of 24-bit word (unaligned!)
        uint8_t b0 = src[pos+0];
        uint8_t b1 = src[pos+1];
        uint8_t b2 = src[pos+2];

        // Generate 24-bit word
        uint32_t w = (((uint32_t)b2) << 16) | (((uint32_t)b1) << 8) | ((uint32_t)b0);

        // Add into shared mem
        smem1[(w >>  0) & 7][si]++;
        smem2[(w >>  3) & 7][si]++;
        smem1[(w >>  6) & 7][si]++;
        smem2[(w >>  9) & 7][si]++;
        smem1[(w >> 12) & 7][si]++;
        smem2[(w >> 15) & 7][si]++;
        smem1[(w >> 18) & 7][si]++;
        smem2[(w >> 21) & 7][si]++;

        pos += 3*N;
    }
    __syncthreads();

    #pragma unroll
    for (int n=0; n<8; n++) {
        atomicAdd(&hist_ch1[n], smem1[n][si]);
        atomicAdd(&hist_ch2[n], smem2[n][si]);
    }
}



/////////////////////////////////////////////////////////////////////////////////////
// GPU HISTOGRAM - FAST VIA TRICK WITH CUDA SDK histogram256
/////////////////////////////////////////////////////////////////////////////////////

//static const unsigned int PARTIAL_HISTOGRAM256_COUNT = 240;
static unsigned int *m_d_PartialHistograms;
static bool m_gpu_hist256_init_done = false;

#define TAG_MASK 0xFFFFFFFFU
#define HISTOGRAM256_BIN_COUNT 256

#define LOG2_WARP_SIZE 5U
#define WARP_SIZE (1U << LOG2_WARP_SIZE)

// May change on future hardware, so better parametrize the code
#define SHARED_MEMORY_BANKS 16

// Threadblock size: must be a multiple of (4 * SHARED_MEMORY_BANKS)
// because of the bit permutation of threadIdx.x
#define HISTOGRAM64_THREADBLOCK_SIZE (4 * SHARED_MEMORY_BANKS)

// Warps ==subhistograms per threadblock
#define WARP_COUNT 6

// Threadblock size
#define HISTOGRAM256_THREADBLOCK_SIZE (WARP_COUNT * WARP_SIZE)

// Shared memory per threadblock
#define HISTOGRAM256_THREADBLOCK_MEMORY (WARP_COUNT * HISTOGRAM256_BIN_COUNT)


#define UMUL(a, b) ( (a) * (b) )
#define UMAD(a, b, c) ( UMUL((a), (b)) + (c) )

////////////////////////////////////////////////////////////////////////////////
// Shortcut shared memory atomic addition functions
////////////////////////////////////////////////////////////////////////////////
inline __device__ void binByte(hist_t *s_WarpHist, hist_t data, hist_t threadTag)
{
    atomicAdd(s_WarpHist + data, 1);
}

inline __device__ void addWord(hist_t *s_WarpHist, hist_t data, hist_t tag)
{
    binByte(s_WarpHist, (data >>  0) & 0xFFU, tag);
    binByte(s_WarpHist, (data >>  8) & 0xFFU, tag);
    binByte(s_WarpHist, (data >> 16) & 0xFFU, tag);
    binByte(s_WarpHist, (data >> 24) & 0xFFU, tag);
}

inline __device__ void addWord4S(hist_t *s_WarpHist, hist_t data, hist_t tag)
{
    binByte(s_WarpHist, 4*((data >>  0) & 0xFFU), tag);
    binByte(s_WarpHist, 4*((data >>  8) & 0xFFU)+1, tag);
    binByte(s_WarpHist, 4*((data >> 16) & 0xFFU)+2, tag);
    binByte(s_WarpHist, 4*((data >> 24) & 0xFFU)+3, tag);
}

////////////////////////////////////////////////////////////////////////////////
// Histogram 256-bin
// Undocumented by NVIDIA
////////////////////////////////////////////////////////////////////////////////
__global__ void histogram256Kernel(hist_t* __restrict__ m_d_PartialHistograms, hist_t* __restrict__ d_Data, hist_t dataCount)
{
    const uint tag = threadIdx.x << (32 - LOG2_WARP_SIZE);

    //Per-warp subhistogram storage
    __shared__ hist_t s_Hist[HISTOGRAM256_THREADBLOCK_MEMORY];
    hist_t *s_WarpHist= s_Hist + (threadIdx.x >> LOG2_WARP_SIZE) * HISTOGRAM256_BIN_COUNT;

    //Clear shared memory storage for current threadblock before processing
#pragma unroll
    for (uint i = 0; i < (HISTOGRAM256_THREADBLOCK_MEMORY / HISTOGRAM256_THREADBLOCK_SIZE); i++) {
        s_Hist[threadIdx.x + i * HISTOGRAM256_THREADBLOCK_SIZE] = 0;
    }

    //Cycle through the entire data set, update subhistograms for each warp
    __syncthreads();
    for (uint pos = UMAD(blockIdx.x, blockDim.x, threadIdx.x); pos < dataCount; pos += UMUL(blockDim.x, gridDim.x)) {
        hist_t data = d_Data[pos];
        addWord(s_WarpHist, data, tag);
    }

    //Merge per-warp histograms into per-block and write to global memory
    __syncthreads();
    for (uint bin = threadIdx.x; bin < HISTOGRAM256_BIN_COUNT; bin += HISTOGRAM256_THREADBLOCK_SIZE) {
        hist_t sum = 0;
        for (uint i = 0; i < WARP_COUNT; i++) {
            sum += s_Hist[bin + i * HISTOGRAM256_BIN_COUNT] & TAG_MASK;
        }
        m_d_PartialHistograms[blockIdx.x * HISTOGRAM256_BIN_COUNT + bin] = sum;
    }
}

__global__ void histogram256Kernel4S(hist_t* __restrict__ m_d_PartialHistograms, hist_t* __restrict__ d_Data, hist_t dataCount)
{
    const uint tag = threadIdx.x << (32 - LOG2_WARP_SIZE);

    //Per-warp subhistogram storage
    __shared__ hist_t s_Hist[4*HISTOGRAM256_THREADBLOCK_MEMORY];
    hist_t *s_WarpHist= s_Hist + (threadIdx.x >> LOG2_WARP_SIZE) * 4*HISTOGRAM256_BIN_COUNT;

    //Clear shared memory storage for current threadblock before processing
#pragma unroll
    for (uint i = 0; i < (4*HISTOGRAM256_THREADBLOCK_MEMORY / HISTOGRAM256_THREADBLOCK_SIZE); i++) {
        s_Hist[threadIdx.x + i * HISTOGRAM256_THREADBLOCK_SIZE] = 0;
    }

    //Cycle through the entire data set, update subhistograms for each warp
    __syncthreads();
    for (uint pos = UMAD(blockIdx.x, blockDim.x, threadIdx.x); pos < dataCount; pos += UMUL(blockDim.x, gridDim.x)) {
        hist_t data = d_Data[pos];
        addWord4S(s_WarpHist, data, tag);
    }

    //Merge per-warp histograms into per-block and write to global memory
    __syncthreads();
    for (uint bin = threadIdx.x; bin < HISTOGRAM256_BIN_COUNT; bin += HISTOGRAM256_THREADBLOCK_SIZE) {
        hist_t sum = 0, sum1 = 0, sum2 = 0, sum3 = 0;
        for (uint i = 0; i < WARP_COUNT; i++) {
            sum += s_Hist[4*(bin + i * HISTOGRAM256_BIN_COUNT)] & TAG_MASK;
            sum1 += s_Hist[4*(bin + i * HISTOGRAM256_BIN_COUNT) + 1] & TAG_MASK;
            sum2 += s_Hist[4*(bin + i * HISTOGRAM256_BIN_COUNT) + 2] & TAG_MASK;
            sum3 += s_Hist[4*(bin + i * HISTOGRAM256_BIN_COUNT) + 3] & TAG_MASK;
        }
        m_d_PartialHistograms[4*(blockIdx.x * HISTOGRAM256_BIN_COUNT + bin)] = sum;
        m_d_PartialHistograms[4*(blockIdx.x * HISTOGRAM256_BIN_COUNT + bin) + 1] = sum1;
        m_d_PartialHistograms[4*(blockIdx.x * HISTOGRAM256_BIN_COUNT + bin) + 2] = sum2;
        m_d_PartialHistograms[4*(blockIdx.x * HISTOGRAM256_BIN_COUNT + bin) + 3] = sum3;
    }
}

////////////////////////////////////////////////////////////////////////////////
// Merge histogram256() output
// Run one threadblock per bin; each threadblock adds up the same bin counter
// from every partial histogram. Reads are uncoalesced, but mergeHistogram256
// takes only a fraction of total processing time
////////////////////////////////////////////////////////////////////////////////
#define MERGE_THREADBLOCK_SIZE 256
// should one have MERGE_THREADBLOCK_SIZE (256) == HISTOGRAM256_THREADBLOCK_SIZE (2^5 * 6 = 192)?

__global__ void mergeHistogram256Kernel(hist_t* __restrict__ d_Histogram, hist_t* __restrict__ m_d_PartialHistograms, hist_t histogramCount)
{
    __shared__ hist_t data[MERGE_THREADBLOCK_SIZE];
    hist_t sum = 0;

    // Sum the bin 'threadIdx.x' across all former blocks
    for (uint i = threadIdx.x; i < histogramCount; i += MERGE_THREADBLOCK_SIZE) {
        sum += m_d_PartialHistograms[blockIdx.x + i * HISTOGRAM256_BIN_COUNT];
    }
    data[threadIdx.x] = sum;

    // Merge down the block-sums from threads into single bin sum
    for (uint stride = MERGE_THREADBLOCK_SIZE / 2; stride > 0; stride >>= 1) {
        __syncthreads();
        if (threadIdx.x < stride) {
            data[threadIdx.x] += data[threadIdx.x + stride];
        }
    }
    if (threadIdx.x == 0) {
        d_Histogram[blockIdx.x] = data[0];
    }
}

__global__ void mergeHistogram256Kernel4S(hist_t* __restrict__ d_Histogram, hist_t* __restrict__ m_d_PartialHistograms, hist_t histogramCount)
{
    __shared__ hist_t data[4*MERGE_THREADBLOCK_SIZE];
    hist_t sum = 0, sum1 = 0, sum2 = 0, sum3 = 0;

    for (uint i = threadIdx.x; i < histogramCount; i += MERGE_THREADBLOCK_SIZE) {
        sum += m_d_PartialHistograms[4*(blockIdx.x + i * HISTOGRAM256_BIN_COUNT)];
        sum1 += m_d_PartialHistograms[4*(blockIdx.x + i * HISTOGRAM256_BIN_COUNT)+1];
        sum2 += m_d_PartialHistograms[4*(blockIdx.x + i * HISTOGRAM256_BIN_COUNT)+2];
        sum3 += m_d_PartialHistograms[4*(blockIdx.x + i * HISTOGRAM256_BIN_COUNT)+3];
    }
    data[4*threadIdx.x] = sum;
    data[4*threadIdx.x + 1] = sum1;
    data[4*threadIdx.x + 2] = sum2;
    data[4*threadIdx.x + 3] = sum3;

    for (uint stride = MERGE_THREADBLOCK_SIZE / 2; stride > 0; stride >>= 1) {
        __syncthreads();
        if (threadIdx.x < stride) {
            data[4*threadIdx.x] += data[4*(threadIdx.x + stride)];
            data[4*threadIdx.x+1] += data[4*(threadIdx.x + stride) + 1];
            data[4*threadIdx.x+2] += data[4*(threadIdx.x + stride) + 2];
            data[4*threadIdx.x+3] += data[4*(threadIdx.x + stride) + 3];
        }
    }
    if (threadIdx.x == 0) {
        d_Histogram[4*blockIdx.x] = data[0];
        d_Histogram[4*blockIdx.x+1] = data[1];
        d_Histogram[4*blockIdx.x+2] = data[2];
        d_Histogram[4*blockIdx.x+3] = data[3];
    }
}

////////////////////////////////////////////////////////////////////////////////
// Wrappers for NVIDIA 8-bit Histogramming
///////////////////////////////////////////////////////////////////////////////

void gpu_histogram256_init()
{
    cudaMalloc((void **)&m_d_PartialHistograms, 4 * PARTIAL_HISTOGRAM256_COUNT * HISTOGRAM256_BIN_COUNT * sizeof(hist_t));
    m_gpu_hist256_init_done = true;
}

/** gpu_histogram256()
 * Copied from CUDA SDK example file histogram256.cu and adapted a bit.
 */
void gpu_histogram256(void* __restrict__ d_Data, size_t byteCount, hist_t* __restrict__ d_Histogram)
{
    assert(byteCount % sizeof(hist_t) == 0);
    if (!m_gpu_hist256_init_done) {
        gpu_histogram256_init();
    }
    histogram256Kernel<<<PARTIAL_HISTOGRAM256_COUNT, HISTOGRAM256_THREADBLOCK_SIZE>>> (
        m_d_PartialHistograms, (hist_t *)d_Data, byteCount / sizeof(hist_t)
    );
    mergeHistogram256Kernel<<<HISTOGRAM256_BIN_COUNT, MERGE_THREADBLOCK_SIZE>>> (
        d_Histogram, m_d_PartialHistograms, PARTIAL_HISTOGRAM256_COUNT
    );
}

/// Histogramming of 1-byte data in 4 x 1-byte groups. Produces 4 x 256-bin histogram.
void gpu_histogram256_4S(void* __restrict__ d_Data, size_t byteCount, hist_t* __restrict__ d_Histogram)
{
    assert(byteCount % sizeof(hist_t) == 0);
    if (!m_gpu_hist256_init_done) {
        gpu_histogram256_init();
    }
    histogram256Kernel4S<<<PARTIAL_HISTOGRAM256_COUNT, HISTOGRAM256_THREADBLOCK_SIZE>>> (
        m_d_PartialHistograms, (hist_t *)d_Data, byteCount / sizeof(hist_t)
    );
    mergeHistogram256Kernel4S<<<HISTOGRAM256_BIN_COUNT, MERGE_THREADBLOCK_SIZE>>> (
        d_Histogram, m_d_PartialHistograms, PARTIAL_HISTOGRAM256_COUNT
    );
}


/////////////////////////////////////////////////////////////////////////////////////

#ifdef BENCH // standalone compile mode for testing

#ifndef CUDA_DEVICE_NR
    #define CUDA_DEVICE_NR 0
#endif
#define CHECK_TIMING 1
#include "cuda_utils.cu"
#include <sys/time.h>
#include <stdlib.h>

void usage(void)
{
    printf("Usage: histogram_bench [<nbits>] [<nch>]\n");
}

int main(int argc, char **argv)
{
    cudaDeviceProp cudaDevProp;

    cudaEvent_t tstart, tstop;
    struct timeval tvstart, tvstop;
    double dT;

    size_t maxphysthreads;
    size_t nsamples, nrawbytes, sum;
    unsigned char *h_data_uint8;
    unsigned char *d_data_uint8;

    // CPU reference
    hist_t ref_hist_ch1[HIST_MAX_LEVS8] = {0}; // large [HIST_MAX_LEVS24] in order to test 24-bit histogram
    hist_t ref_hist_ch2[HIST_MAX_LEVS8] = {0};
    hist_t ref_hist_ch3[HIST_MAX_LEVS8] = {0};
    hist_t ref_hist_ch4[HIST_MAX_LEVS8] = {0};

    memset(ref_hist_ch1, 0, sizeof(ref_hist_ch1));
    memset(ref_hist_ch2, 0, sizeof(ref_hist_ch2));
    memset(ref_hist_ch3, 0, sizeof(ref_hist_ch3));
    memset(ref_hist_ch4, 0, sizeof(ref_hist_ch4));

    // GPU output; host-side
    hist_t *h_hist_256;
    hist_t *h_hist_256_4S;
    hist_t *h_hist_ch1;
    hist_t *h_hist_ch2;
    hist_t *h_hist_ch3;
    hist_t *h_hist_ch4;
    hist_t *h_hist_multichannel;

    // GPU data; device-side
    hist_t *d_hist_256;
    hist_t *d_hist_256_4S;
    hist_t *d_hist_ch1;
    hist_t *d_hist_ch2;
    hist_t *d_hist_ch3;
    hist_t *d_hist_ch4;

    // Input data format
    int nbits = 3, nch = 1, nlev;
    if (!((argc >= 1) && (argc <= 3)) || ((argc >= 2) && (argv[1][0] == '-'))) {
        usage();
        return 1;
    }
    if (argc > 1) {
        nbits = atoi(argv[1]);
    }
    if (argc > 2) {
        nch = atoi(argv[2]);
    }

    nlev = 1 << nbits;
    nsamples  = 24*8000000/nbits;
    nrawbytes = (nsamples*nbits)/8;
    printf("Testing histogrammer for %d-level %d-bit %d-channel dataset with %zd samples\n", nlev, nbits, nch, nsamples);


    /////////////////////////////////////////////////////////////////////////////////
    // Setup
    /////////////////////////////////////////////////////////////////////////////////

    // Device
    CUDA_CALL( cudaSetDevice(CUDA_DEVICE_NR) );
    CUDA_CALL( cudaGetDeviceProperties(&cudaDevProp, CUDA_DEVICE_NR) );
    maxphysthreads = cudaDevProp.multiProcessorCount * cudaDevProp.maxThreadsPerMultiProcessor;

    CUDA_CALL( cudaEventCreate( &tstart ) );
    CUDA_CALL( cudaEventCreate( &tstop ) );

    // Alloc space
    CUDA_CALL( cudaMalloc( (void **)&d_data_uint8, nrawbytes ) );
    CUDA_CALL( cudaHostAlloc( (void **)&h_data_uint8, nrawbytes, cudaHostAllocDefault ) );

    CUDA_CALL( cudaMalloc( (void **)&d_hist_256, 256*4*sizeof(hist_t) ) );
    CUDA_CALL( cudaMalloc( (void **)&d_hist_256_4S, 4*256*sizeof(hist_t) ) ); // 4-byte groups * max 1-byte sub-histogram size per group
    CUDA_CALL( cudaMalloc( (void **)&d_hist_ch1, HIST_MAX_LEVS24*sizeof(hist_t) ) );
    CUDA_CALL( cudaMalloc( (void **)&d_hist_ch2, HIST_MAX_LEVS24*sizeof(hist_t) ) );
    CUDA_CALL( cudaMalloc( (void **)&d_hist_ch3, HIST_MAX_LEVS24*sizeof(hist_t) ) );
    CUDA_CALL( cudaMalloc( (void **)&d_hist_ch4, HIST_MAX_LEVS24*sizeof(hist_t) ) );

    CUDA_CALL( cudaHostAlloc( (void **)&h_hist_256, 256*4*sizeof(hist_t), cudaHostAllocDefault ) );
    CUDA_CALL( cudaHostAlloc( (void **)&h_hist_256_4S, 4*256*4*sizeof(hist_t), cudaHostAllocDefault ) );
    CUDA_CALL( cudaHostAlloc( (void **)&h_hist_ch1, HIST_MAX_LEVS24*sizeof(hist_t), cudaHostAllocDefault ) );
    CUDA_CALL( cudaHostAlloc( (void **)&h_hist_ch2, HIST_MAX_LEVS24*sizeof(hist_t), cudaHostAllocDefault ) );
    CUDA_CALL( cudaHostAlloc( (void **)&h_hist_ch3, HIST_MAX_LEVS24*sizeof(hist_t), cudaHostAllocDefault ) );
    CUDA_CALL( cudaHostAlloc( (void **)&h_hist_ch4, HIST_MAX_LEVS24*sizeof(hist_t), cudaHostAllocDefault ) );
    CUDA_CALL( cudaHostAlloc( (void **)&h_hist_multichannel, 32*HIST_MAX_LEVS24*sizeof(hist_t), cudaHostAllocDefault ) );

    CUDA_CALL( cudaMemset( d_hist_ch1, 0, HIST_MAX_LEVS24*sizeof(hist_t) ) );
    CUDA_CALL( cudaMemset( d_hist_ch2, 0, HIST_MAX_LEVS24*sizeof(hist_t) ) );
    CUDA_CALL( cudaMemset( d_hist_ch3, 0, HIST_MAX_LEVS24*sizeof(hist_t) ) );
    CUDA_CALL( cudaMemset( d_hist_ch4, 0, HIST_MAX_LEVS24*sizeof(hist_t) ) );
    CUDA_CALL( cudaMemset( d_hist_256, 0, 256*sizeof(hist_t) ) );
    CUDA_CALL( cudaMemset( d_hist_256_4S, 0, 4*256*sizeof(hist_t) ) );

    memset(h_hist_ch1, 0, HIST_MAX_LEVS24*sizeof(hist_t));
    memset(h_hist_ch2, 0, HIST_MAX_LEVS24*sizeof(hist_t));
    memset(h_hist_ch3, 0, HIST_MAX_LEVS24*sizeof(hist_t));
    memset(h_hist_ch4, 0, HIST_MAX_LEVS24*sizeof(hist_t));

    memset(h_hist_256, 0, 256*sizeof(hist_t));
    memset(h_hist_256_4S, 0, 4*256*sizeof(hist_t));


    /////////////////////////////////////////////////////////////////////////////////
    // Random 8-bit data pattern
    /////////////////////////////////////////////////////////////////////////////////

    srand(73128);
    for (size_t i=0; i<nrawbytes; i++) {
        h_data_uint8[i] = rand() % 256;
    }
    CUDA_CALL( cudaMemcpy( d_data_uint8, h_data_uint8, nrawbytes, cudaMemcpyHostToDevice) );


    /////////////////////////////////////////////////////////////////////////////////
    // GPU speed all kernels
    /////////////////////////////////////////////////////////////////////////////////

    // Distribution
    size_t M = cudaDevProp.warpSize;
    size_t N = maxphysthreads;
    dim3 numBlocks(div2ceil(N,M));
    dim3 threadsPerBlock(M);
    printf("GPU exec as <<%d,%d>> grid\n", numBlocks.x, threadsPerBlock.x);

    printf("Individual kernel performance:\n");

    CUDA_CALL( cudaMemset( d_hist_ch1, 0, HIST_MAX_LEVS8*sizeof(hist_t) ) );
    CUDA_TIMING_START(tstart, 0);
    cu_histogram_8bit_1ch<<<numBlocks, threadsPerBlock>>>(d_data_uint8, nrawbytes, d_hist_ch1);
    CUDA_CHECK_ERRORS("");
    CUDA_TIMING_STOP(tstop, tstart, 0, "8bit_1ch", nsamples);

    CUDA_CALL( cudaMemset( d_hist_ch1, 0, HIST_MAX_LEVS8*sizeof(hist_t) ) );
    CUDA_TIMING_START(tstart, 0);
    cu_histogram_2bit_1ch<<<numBlocks, threadsPerBlock>>>(d_data_uint8, nrawbytes, d_hist_ch1);
    CUDA_CHECK_ERRORS("");
    CUDA_TIMING_STOP(tstop, tstart, 0, "2bit_1ch", nsamples);

    CUDA_CALL( cudaMemset( d_hist_ch1, 0, HIST_MAX_LEVS8*sizeof(hist_t) ) );
    CUDA_CALL( cudaMemset( d_hist_ch2, 0, HIST_MAX_LEVS8*sizeof(hist_t) ) );
    CUDA_TIMING_START(tstart, 0);
    cu_histogram_2bit_2ch<<<numBlocks, threadsPerBlock>>>(d_data_uint8, nrawbytes, d_hist_ch1, d_hist_ch2);
    CUDA_CHECK_ERRORS("");
    CUDA_TIMING_STOP(tstop, tstart, 0, "2bit_2ch", nsamples);

    CUDA_CALL( cudaMemset( d_hist_256, 0, 256*sizeof(hist_t) ) );
    gpu_histogram256(d_data_uint8, nrawbytes, d_hist_256); // 1st call will do a one-time alloc, do not time that
    CUDA_TIMING_START(tstart, 0);
    gpu_histogram256(d_data_uint8, nrawbytes, d_hist_256);
    CUDA_CHECK_ERRORS("");
    CUDA_TIMING_STOP(tstop, tstart, 0, "hist256", nsamples);

    CUDA_CALL( cudaMemset( d_hist_256_4S, 0, 4*256*sizeof(hist_t) ) );
    CUDA_TIMING_START(tstart, 0);
    gpu_histogram256_4S(d_data_uint8, nrawbytes, d_hist_256_4S);
    CUDA_CHECK_ERRORS("");
    CUDA_TIMING_STOP(tstop, tstart, 0, "hist256_4S", nsamples);

    // Reset the counts again
    CUDA_CALL( cudaMemset( d_hist_ch1, 0, HIST_MAX_LEVS24*sizeof(hist_t) ) );
    CUDA_CALL( cudaMemset( d_hist_ch2, 0, HIST_MAX_LEVS24*sizeof(hist_t) ) );
    CUDA_CALL( cudaMemset( d_hist_ch3, 0, HIST_MAX_LEVS24*sizeof(hist_t) ) );
    CUDA_CALL( cudaMemset( d_hist_ch4, 0, HIST_MAX_LEVS24*sizeof(hist_t) ) );
    CUDA_CALL( cudaMemset( d_hist_256, 0, 256*sizeof(hist_t) ) );
    CUDA_CALL( cudaMemset( d_hist_256_4S, 0, 4*256*sizeof(hist_t) ) );

    /////////////////////////////////////////////////////////////////////////////////
    // Check 4-byte-group x 256-bin histogrammer
    /////////////////////////////////////////////////////////////////////////////////

    for (size_t i=0; i<nrawbytes; i++) {
        if (i % 4 == 2) {
            h_data_uint8[i] = rand() % 256;
        } else {
            h_data_uint8[i] = i % 4;
        }
    }
    CUDA_CALL( cudaMemcpy( d_data_uint8, h_data_uint8, nrawbytes, cudaMemcpyHostToDevice) );
    CUDA_CALL( cudaMemset( d_hist_256, 0, 256*4*sizeof(hist_t) ) );
    gpu_histogram256_4S(d_data_uint8, nrawbytes, d_hist_256);
    CUDA_CHECK_ERRORS("");
    CUDA_CALL( cudaMemcpy( h_hist_256, d_hist_256, 256*4*sizeof(hist_t), cudaMemcpyDeviceToHost) );

#if 0
    for (int i=0; i<256; i++) {
        printf("bin %3d : ", i);
        for (int k=0; k<4; k++) {
            printf("%-12zu ", (size_t)h_hist_256[4*i + k]);
        }
        printf("\n");
    }
#endif

    const size_t subgroup_nsamples = nrawbytes / 4;
    assert(h_hist_256[4*0 + 0] == subgroup_nsamples); // bin 0, subgroup 0 should have all samples, for test data generator above
    for (int i=0; i<256; i++) {
        assert(h_hist_256[4*i + 2] != 0);             // bin *, subgroup 2 should have N ~ subgroup_nsamples/256 [rand()] samples
    }
    assert(h_hist_256[4*1 + 1] == subgroup_nsamples); // bin 1, subgroup 1 should have all samples, for test data generator above
    assert(h_hist_256[4*3 + 3] == subgroup_nsamples); // bin 3, subgroup 3 should have all samples, for test data generator above

    /////////////////////////////////////////////////////////////////////////////////
    // CPU speed
    /////////////////////////////////////////////////////////////////////////////////

    gettimeofday(&tvstart, NULL);
    cpu_histogram(nbits, nch, h_data_uint8, nrawbytes, ref_hist_ch1, ref_hist_ch2, ref_hist_ch3, ref_hist_ch4);
    gettimeofday(&tvstop, NULL);

    dT = (tvstop.tv_sec - tvstart.tv_sec) + 1e-6*(tvstop.tv_usec - tvstart.tv_usec);
    printf("cpu_histogram(): %zu samples in %.3f msec, %.1f Ms/s, %.3f MB/s\n", nsamples, dT*1e3, (1e-6*nsamples)/dT, (1e-6*nrawbytes)/dT);

    sum = 0;
    for (int i=0; i<nlev; i++) {
        if (nch >= 1) {
            if (i < 32) { printf("   %3d : ch1=%-12zu ", i, (size_t)ref_hist_ch1[i]); }
            sum += ref_hist_ch1[i];
        }
        if (nch >= 2) {
            if (i < 32) { printf("ch2=%-12zu ", (size_t)ref_hist_ch2[i]); }
            sum += ref_hist_ch2[i];
        }
        if (nch >= 4) {
            if (i < 32) { printf("ch3=%-12zu ch4=%-12zu ", (size_t)ref_hist_ch3[i], (size_t)ref_hist_ch4[i]); }
            sum += ref_hist_ch3[i] + ref_hist_ch4[i];
        }
        if (i == 31) { printf("\n   ...\n"); }
        if (i < 32) { printf("\n"); }
    }
    printf("sum = %zu\n", sum);

    /////////////////////////////////////////////////////////////////////////////////
    // GPU speed specific kernels
    /////////////////////////////////////////////////////////////////////////////////

    printf("\n");
    printf("Finding kernel for %d-bit %d-channel layout\n", nbits, nch);

    gettimeofday(&tvstart, NULL);
    CUDA_TIMING_START(tstart, 0);

    if (!(nbits == 8 || nbits == 3 || nbits == 2)) {
        printf("Error: no GPU histogram func assigned for %d-bit data!\n", nbits);
        exit(1);
    }

    // Use CUDA 256-bin kernel and 8-bit samples?

    if ((nbits==8) && (nch==1)) {
        gpu_histogram256(d_data_uint8, nrawbytes, d_hist_ch1);
        CUDA_CALL( cudaMemcpy( h_hist_ch1, d_hist_ch1, 256*sizeof(hist_t), cudaMemcpyDeviceToHost) );
    } else if (nbits==8) {
        printf("Error: no GPU histogram func assigned for %d-bit (8) %d-channel layout!\n", nbits, nch);
        exit(1);
    }

    // Use CUDA 256-bin kernel for binning at 8bit, then untangling to N={1,2,4} x 2-bit histograms via lookup tables

    if ((nbits==2) && (nch==1)) {
        gpu_histogram256(d_data_uint8, nrawbytes, d_hist_256);
        CUDA_CALL( cudaMemcpy( h_hist_256, d_hist_256, 256*sizeof(hist_t), cudaMemcpyDeviceToHost) );
        cpu_histogramtransform_8bit_to_2bit_1ch((const hist_t*)h_hist_256, h_hist_ch1);
    } else if ((nbits==2) && (nch==2)) {
        gpu_histogram256(d_data_uint8, nrawbytes, d_hist_256);
        CUDA_CALL( cudaMemcpy( h_hist_256, d_hist_256, 256*sizeof(hist_t), cudaMemcpyDeviceToHost) );
        cpu_histogramtransform_8bit_to_2bit_2ch((const hist_t*)h_hist_256, h_hist_ch1, h_hist_ch2);
    } else if ((nbits==2) && (nch==4)) {
        gpu_histogram256(d_data_uint8, nrawbytes, d_hist_256);
        CUDA_CALL( cudaMemcpy( h_hist_256, d_hist_256, 256*sizeof(hist_t), cudaMemcpyDeviceToHost) );
        cpu_histogramtransform_8bit_to_2bit_4ch((const hist_t*)h_hist_256, h_hist_ch1, h_hist_ch2, h_hist_ch3, h_hist_ch4);
    } else if ((nbits==2) && (nch==8)) {
        gpu_histogram256_4S(d_data_uint8, nrawbytes, d_hist_256_4S);
        CUDA_CALL( cudaMemcpy( h_hist_256_4S, d_hist_256_4S, 4*256*sizeof(hist_t), cudaMemcpyDeviceToHost) );
        cpu_histogramtransform_8bit4S_to_2bit_8ch((const hist_t*)h_hist_256_4S, h_hist_multichannel);
        memcpy(h_hist_ch1, h_hist_multichannel, 4*sizeof(hist_t));
        memcpy(h_hist_ch2, h_hist_multichannel+1*4, 4*sizeof(hist_t));
        memcpy(h_hist_ch3, h_hist_multichannel+2*4, 4*sizeof(hist_t));
        memcpy(h_hist_ch4, h_hist_multichannel+3*4, 4*sizeof(hist_t));
    } else if ((nbits==2) && (nch==16)) {
        gpu_histogram256_4S(d_data_uint8, nrawbytes, d_hist_256_4S);
        CUDA_CALL( cudaMemcpy( h_hist_256_4S, d_hist_256_4S, 4*256*sizeof(hist_t), cudaMemcpyDeviceToHost) );
        cpu_histogramtransform_8bit4S_to_2bit_16ch((const hist_t*)h_hist_256_4S, h_hist_multichannel);
        memcpy(h_hist_ch1, h_hist_multichannel, 4*sizeof(hist_t));
        memcpy(h_hist_ch2, h_hist_multichannel+1*4, 4*sizeof(hist_t));
        memcpy(h_hist_ch3, h_hist_multichannel+2*4, 4*sizeof(hist_t));
        memcpy(h_hist_ch4, h_hist_multichannel+3*4, 4*sizeof(hist_t));
    } else if (nbits==2) {
        printf("Error: no GPU histogram func assigned for %d-bit %d-channel layout!\n", nbits, nch);
        exit(1);
    }

    // Use custom 3-bit / 24-bit binning

    if ((nbits==3) && (nch==1)) {
        cu_histogram_3bit_1ch<<<numBlocks, threadsPerBlock>>>(d_data_uint8, nrawbytes, d_hist_ch1);
        CUDA_CALL( cudaMemcpy( h_hist_ch1, d_hist_ch1, 16*sizeof(hist_t), cudaMemcpyDeviceToHost) );
    } else if ((nbits==3) && (nch==2)) {
        cu_histogram_3bit_2ch<<<numBlocks, threadsPerBlock>>>(d_data_uint8, nrawbytes, d_hist_ch1, d_hist_ch2);
        CUDA_CALL( cudaMemcpy( h_hist_ch1, d_hist_ch1, 16*sizeof(hist_t), cudaMemcpyDeviceToHost) );
        CUDA_CALL( cudaMemcpy( h_hist_ch2, d_hist_ch2, 16*sizeof(hist_t), cudaMemcpyDeviceToHost) );
    } else if (nbits==3) {
        printf("Error: no GPU histogram func assigned for %d-bit %d-channel layout!\n", nbits, nch);
        exit(1);
    }

    // Overall performance, with CPU-side untangling included

    CUDA_CHECK_ERRORS("cu_histogram");
    CUDA_TIMING_STOP(tstop, tstart, 0, "hist", nsamples);
    gettimeofday(&tvstop, NULL);

    dT = (tvstop.tv_sec - tvstart.tv_sec) + 1e-6*(tvstop.tv_usec - tvstart.tv_usec);
    printf("Throughput: %zu samples in %.3f msec, %.1f Ms/s, %.3f MB/s\n", nsamples, dT*1e3, (1e-6*nsamples)/dT, (1e-6*nrawbytes)/dT);

    sum = 0;
    bool cpu_gpu_mismatch = false;
    for (int i=0; i<nlev; i++) {
        if (nch >= 1) {
            if (i < 32) { printf("   %3d : ch1=%-12zu ", i, (size_t)h_hist_ch1[i]); }
            sum += h_hist_ch1[i];
            cpu_gpu_mismatch |= (h_hist_ch1[i] != ref_hist_ch1[i]);
        }
        if (nch >= 2) {
            if (i < 32) { printf("ch2=%-12zu ", (size_t)h_hist_ch2[i]); }
            sum += h_hist_ch2[i];
            cpu_gpu_mismatch |= (h_hist_ch2[i] != ref_hist_ch2[i]);
        }
        if (nch == 4) {
            if (i < 32) { printf("ch3=%-12zu ch4=%-12zu", (size_t)h_hist_ch3[i], (size_t)h_hist_ch4[i]); }
            sum += h_hist_ch3[i] + h_hist_ch4[i];
            cpu_gpu_mismatch |= (h_hist_ch3[i] != ref_hist_ch3[i]) | (h_hist_ch4[i] != ref_hist_ch4[i]);
        }
        if (i == 31) { printf("\n   ..."); }
        if (i < 32) { printf("\n"); }
    }
    printf("sum = %zu\n", sum);
    if (cpu_gpu_mismatch) {
         printf("-- GPU result does not match CPU result! --\n");
    }

    /////////////////////////////////////////////////////////////////////////////////

    // Some extra info
    printf("Layout: %u blocks x %u threads\n", numBlocks.x, threadsPerBlock.x);

    return 0;
}

#endif
