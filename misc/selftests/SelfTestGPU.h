/**
 * CUDA Hardware Self-Test Class:
 * 1) memory check (https://sourceforge.net/projects/cudagpumemtest/)
 * 2) kernel launch check,
 * 3) read out temperatures(?) and ECC error counts
 *
 * ALMA:
 * - allow up to 15 minutes total for server booting, services start-up, self-test.
 * - self-test has no particular requirements, in some cases sufficient to check hardware
 *   functions without checking software functions
 */

#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/noncopyable.hpp>

class SelfTestGPU : private boost::noncopyable
{

   public:
        enum SelfTestGPUResult { NOT_COMPLETED=0, ONGOING, PASSED, WARNING, FAILED };
        enum SelfTestGPULevel { IMMEDIATE=0, QUICK, THOROUGH }; /// Immediate=check card self-reported data, Quick=check briefly, THOROUGH=longer memtest86

    public:
        SelfTestGPU(SelfTestGPULevel level, int nGPU_expected);
        void start();
        void interrupt();
        void waitStop();
        SelfTestGPUResult getResult() const { return m_result; }

   private:
        void worker(int deviceNr);

   private:
        int m_nGPU;
        int m_nworkers;
        boost::thread* m_workers[32];

        SelfTestGPULevel m_level;
        SelfTestGPUResult m_result;
};
