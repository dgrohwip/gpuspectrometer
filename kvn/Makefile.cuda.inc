## Location of top-level include makefile (this file)
TOPDIR := $(dir $(lastword $(MAKEFILE_LIST)))

## Installation target
INSTALL_PREFIX := /usr/local/

## CUDA location
ifndef CUDA_DIR
        CUDA_DIR := /usr/local/cuda/
endif

## CUDA NVCC
NVCC = $(CUDA_DIR)/bin/nvcc
NVLDFLAGS = -L$(CUDA_DIR)/lib64/
NVLDLIBS = -lcuda -lcudart -lcufft

NV_GEN_OLDARCH := -gencode arch=compute_20,code=sm_20
NV_GEN_KEPLER  := -gencode arch=compute_30,code=sm_30 -gencode arch=compute_35,code=sm_35 -gencode arch=compute_37,code=sm_37
NV_GEN_MAXWELL := -gencode arch=compute_50,code=sm_50 -gencode=arch=compute_52,code=sm_52 -gencode=arch=compute_52,code=compute_52
NV_GEN_PASCAL  := -gencode=arch=compute_61,code=sm_61 -gencode=arch=compute_61,code=compute_61
NV_GEN_VOLTA   := -gencode=arch=compute_70,code=sm_70 -gencode=arch=compute_70,code=compute_70

NV_TARGET_PLATFORMS = $(NV_GEN_PASCAL)
#NV_TARGET_PLATFORMS += $(NV_GEN_VOLTA)
