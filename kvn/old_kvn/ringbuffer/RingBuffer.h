// RingBuffer.h
// Wrote by ROH
// Distributed at 2015/09/17
// 2015/10 JanW added UDP/IP RX-TX

#ifndef RINGBUFFER_H
#define RINGBUFFER_H

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>

// Use __atomic.. builtins instead of __sync.. builtins in gcc 4.8.2 or later versions
#define GCC_VERSION (__GNUC__ * 10000 + __GNUC_MINOR__ * 100 + __GNUC_PATCHLEVEL__)
#if GCC_VERSION < 40802
  #define getAtomic(p)   __sync_add_and_fetch((p),0)
  #define clearAtomic(p) __sync_and_and_fetch((p),0)
  #define setAtomic(p,v) do{__sync_and_and_fetch((p),0);__sync_add_and_fetch((p),(v));}while(0)
  #define addAtomic(p,v) __sync_add_and_fetch((p),(v))
#else
  #define getAtomic(p)   __atomic_load_n((p),__ATOMIC_RELAXED)         // return(*p)
  #define clearAtomic(p) __atomic_store_n((p),0,__ATOMIC_RELAXED)      // (void) *p = 0
  #define setAtomic(p,v) __atomic_store_n((p),(v),__ATOMIC_RELAXED)    // (void) *p = v
  #define addAtomic(p,v) __atomic_add_fetch((p),(v),__ATOMIC_RELAXED)  // return(*p += v)
#endif

#ifdef __cplusplus
extern "C" {
#endif

//------------------------------------------------------------------------------
typedef struct RingBuffer_t {
  void  *addr; // RingBuffer start address
  size_t size; // size of buffer, rounded up by kernel page size
  size_t woff; // offset of append(write to RB) point
  size_t roff; // offset of extract(read from RB) point
  // informative
  char   name[80];  // name of RingBuffer
  size_t full;      // buffer-full counter when append operation
  size_t drop;      // total # of bytes dropped when append operation
  size_t blnk;      // buffer-blank counter when extract operation
  size_t max_exist; // max. exist value
  // network buffer
  void *rxaddr;
} RingBuffer;
//------------------------------------------------------------------------------

//==============================================================================
// create, remove, clear, show status
RingBuffer *RBcreate(size_t buffer_size, const char *buffer_name);
void RBremove(RingBuffer *RB);
void RBclear(RingBuffer *RB);
void RBstatus(RingBuffer *RB, const char *note);

//==============================================================================
// get addr,space for append/extract operation
void *RBappendable(RingBuffer *RB, size_t *empty);
void *RBextractable(RingBuffer *RB, size_t *exist);

// advance the append/extract offset
void RBappended(RingBuffer *RB, size_t n);
void RBextracted(RingBuffer *RB, size_t n);

//==============================================================================
// RB <---> memory
size_t RBput(RingBuffer *RB, void *src, size_t n);	// RB <--- src
size_t RBget(RingBuffer *RB, void *dst, size_t n);	// RB ---> dst

//==============================================================================
// RB <---> FILE stream
size_t RBfread(RingBuffer *RB, FILE *fpr, size_t n);	// RB <--- fpr
size_t RBfwrite(RingBuffer *RB, FILE *fpw, size_t n);	// RB ---> fpw

//==============================================================================
// RB <---> file descriptor
ssize_t RBread(RingBuffer *RB, int fdr, size_t n);	// RB <--- fdr
ssize_t RBwrite(RingBuffer *RB, int fdw, size_t n);	// RB ---> fdw

//==============================================================================
// RB <---> UDP/IP socket or destination ; note <64kB packet size limitation
ssize_t RBsudpread(RingBuffer *RB, int sd, size_t n_max); // RB <-- UDP/IP
ssize_t RBsudppread(RingBuffer *RB, int sd, size_t n_max, off_t offset); // RB <-- UDP/IP
size_t RBsudpwrite(RingBuffer *RB, int sd, size_t n, const struct sockaddr *to, socklen_t tolen); // RB --> UDP/IP

#ifdef __cplusplus
}
#endif

#endif // RINGBUFFER_H
