// RingBuffer.c
// Wrote by ROH
// Distributed at 2015/09/17

#include "RingBuffer.h"

#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <error.h>
#include <malloc.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>

//------------------------------------------------------------------------------

// Create a RingBuffer of specified size and optional name
RingBuffer *RBcreate(size_t buffer_size, const char *buffer_name)
{
  RingBuffer *RB;
  size_t RBsize, VMsize, page_size = sysconf(_SC_PAGESIZE);
  void *VMaddr, *RBaddr, *addr = NULL;
  char path[20];
  int fd;

  // round up buffer_size by page_size
  RBsize = ((buffer_size + (page_size - 1))/page_size) * page_size;

  // total VM space will be "1 page + 2*RBsize"
  VMsize = page_size + (RBsize << 1);

  // create a new ANONYMOUS mapping in the virtual address space for "1page + DOUBLE buffer_size"
  VMaddr = mmap(NULL, VMsize, PROT_NONE, MAP_ANONYMOUS|MAP_PRIVATE, -1, 0);
  if(VMaddr == MAP_FAILED) error(1,errno,"RBcreate: Failed to map full memory.");
  RBaddr = (char*)VMaddr + page_size;

  // create a unique temporary file, with filesize is page_size
  strcpy(path,"/dev/shm/RB-XXXXXX");
  if((fd = mkstemp(path)) < 0) error(1,errno,"RBcreate: Could not get a file descriptor.");
  if(ftruncate(fd,page_size)) error(1,errno,"RBcreate: Could not truncate.");

  // 1page for the RingBuffer struct
  addr = mmap(VMaddr, page_size, PROT_READ|PROT_WRITE, MAP_FIXED|MAP_SHARED, fd, 0);
  if(addr != VMaddr) error(1,errno,"RBcreate: Failed to map the first page.");

  // close the temp file, and remove it (no need hereafter)
  if(close(fd)) error(0,errno,"RBcreate: Could not close file descriptor.");
  if(unlink(path)) error(0,errno,"RBcreate: Could not unlink.");

  // create a unique temporary file, with filesize is RBsize
  strcpy(path,"/dev/shm/RB-XXXXXX");
  if((fd = mkstemp(path)) < 0) error(1,errno,"RBcreate: Could not get a file descriptor.");
  if(ftruncate(fd,RBsize)) error(1,errno,"RBcreate: Could not truncate.");

  // create a new FILE mapping (initial) to the FIRST-HALF of sepcified virtual address space
  addr = mmap(RBaddr, RBsize, PROT_READ|PROT_WRITE, MAP_FIXED|MAP_SHARED, fd, 0);
  if(addr != RBaddr) error(1,errno,"RBcreate: Failed to map initial memory.");
  // create a new FILE mapping (mirror) to the SECOND-HALF of sepcified virtual address space
  addr = mmap((char*)RBaddr+RBsize, RBsize, PROT_READ|PROT_WRITE, MAP_FIXED|MAP_SHARED, fd, 0);
  if(addr != (char*)RBaddr+RBsize) error(1,errno,"RBcreate: Failed to map mirror memory.");

  // close the temp file, and remove it (no need hereafter)
  if(close(fd)) error(0,errno,"RBcreate: Could not close file descriptor.");
  if(unlink(path)) error(0,errno,"RBcreate: Could not unlink.");

  // Now, make sure the physical spaces ready and setup the RingBuffer struct
  RB = (RingBuffer *)memset((void *)VMaddr,0,page_size); // Clear the first page of VMaddr
  RB->addr = memset(RBaddr,0,RBsize); // Clear the actual RB space, to overcome "copy-on-write"
  RB->size = RBsize;
  //RB->woff = RB->roff = 0L;
  strncpy(RB->name,buffer_name?buffer_name:"unknown",sizeof(RB->name)-1);
  //RB->full = RB->drop = RB->blnk = 0L;
  //RB->max_exist = 0L;

  // allocate space for network receive buffer, in case it is used
  RB->rxaddr = memalign(4096,65536);

  RBstatus(RB,"just created");
  return RB;
}

// Remove the RingBuffer
void RBremove(RingBuffer *RB)
{
  size_t exist, page_size = sysconf(_SC_PAGESIZE);

  exist = getAtomic(&RB->woff) - getAtomic(&RB->roff);
  if(exist) error(0,0,"RBremove: abandon %zu bytes in RingBuffer '%s'",exist,RB->name);

  RBstatus(RB,"being removed");
  if(munmap(RB,page_size+(RB->size<<1))) error(1,errno,"RBremove: Could not unmap memory.");

  if(RB->rxaddr) free(RB->rxaddr);
}

// Clear the RingBuffer
void RBclear(RingBuffer *RB)
{
  size_t exist;
  exist = getAtomic(&RB->woff) - getAtomic(&RB->roff);
  if(exist) error(0,0,"RBclear: abandon %zu bytes in RingBuffer '%s'",exist,RB->name);

  clearAtomic(&RB->woff);
  clearAtomic(&RB->roff);
  clearAtomic(&RB->full);
  clearAtomic(&RB->drop);
  clearAtomic(&RB->blnk);
  clearAtomic(&RB->max_exist);
  error(0,0,"RBclear: '%s' cleared",RB->name);
}

//------------------------------------------------------------------------------

// Print the current status of the RingBuffer
void RBstatus(RingBuffer *RB, const char *note)
{
  size_t woff,roff, exist,empty, wcount,rcount, full,drop,blnk, max_exist;
  void *pw,*pr;

  woff = getAtomic(&RB->woff);
  roff = getAtomic(&RB->roff);
  full = getAtomic(&RB->full);
  drop = getAtomic(&RB->drop);
  blnk = getAtomic(&RB->blnk);

  max_exist = getAtomic(&RB->max_exist);

  exist = woff - roff;
  empty = RB->size - exist;
  pw = (char*)(RB->addr) + (woff % RB->size);
  pr = (char*)(RB->addr) + (roff % RB->size);
  wcount = woff / RB->size;
  rcount = roff / RB->size;

  fprintf(stderr,"RingBuffer '%s' at %p (length %zu bytes) : '%s'\n"
    "  appendable %zu bytes to %p (wcount=%zu : total appended %zu)\n"
    "  extractable %zu bytes from %p (rcount=%zu: total extracted %zu)\n",
    RB->name,RB->addr,RB->size, note?note:"???",
    empty,pw,wcount, woff, exist,pr,rcount, roff);

  if(full) fprintf(stderr,"  %zu append-operations failed, %zu bytes dropped.\n",full,drop);
  if(blnk) fprintf(stderr,"  %zu extract-operations failed.\n",blnk);

  if(max_exist) fprintf(stderr,"  Max. usage ratio %.3lf%%\n",(100.0*max_exist)/RB->size);

  if(1) setAtomic(&RB->max_exist,0);
}

//==============================================================================

// Check the addr and spaces for append
void *RBappendable(RingBuffer *RB, size_t *empty)
{
  size_t woff;
  woff = getAtomic(&RB->woff);
  *empty = RB->size - (woff - getAtomic(&RB->roff));
  return( (0 < *empty) ? ((char*)(RB->addr) + (woff % RB->size)) : NULL );
}
// Avdance woff just after append operation
void RBappended(RingBuffer *RB, size_t n)
{
  size_t exist, woff = addAtomic(&RB->woff,n);
  exist = woff - getAtomic(&RB->roff);
  if(exist > getAtomic(&RB->max_exist)) setAtomic(&RB->max_exist,exist);
}

// Check the addr and spaces for extract
void *RBextractable(RingBuffer *RB, size_t *exist)
{
  size_t roff;
  roff = getAtomic(&RB->roff);
  *exist = getAtomic(&RB->woff) - roff;
  return( (0 < *exist) ? ((char*)(RB->addr) + (roff % RB->size)) : NULL );
}

// Avdance roff just after extract operation
void RBextracted(RingBuffer *RB, size_t n)
{
  addAtomic(&RB->roff,n);
}

//==============================================================================
// Memory copy between RB and user memory, similar to 
//   void *memcpy(void *dst, const void *src, size_t n);

// Try to append 'n' bytes of memory 'src' to the tail of RB.
// - returns "# of bytes" actually appended
size_t RBput(RingBuffer *RB, void *src, size_t n)	// RB <--- src
{
  void *dst;
  size_t empty,m;
  dst = RBappendable(RB,&empty);
  m = (n < empty) ? n : empty;
  if(m<n) {
    addAtomic(&RB->full,1L);  // buffer-full counter
    addAtomic(&RB->drop,n-m); // # of bytes dropped
  }
  if(dst) {
    memcpy(dst,src,m);
    if(m<n) fprintf(stderr,"RBput(%zu/%zu)",m,n);
    RBappended(RB,m);
  }
  return m;
}

// Try to extract 'n' bytes from the head of RB to memory 'dst'
// - returns "# of bytes" actually extracted
size_t RBget(RingBuffer *RB, void *dst, size_t n)	// RB ---> dst
{
  void *src;
  size_t exist,m;
  src = RBextractable(RB,&exist);
  m = (n < exist) ? n : exist;
  if(m<n) addAtomic(&RB->blnk,1L); // buffer-empty counter
  if(src) {
    memcpy(dst,src,m);
    if(m<n) fprintf(stderr,"RBget(%zu/%zu)",m,n);
    RBextracted(RB,m);
  }
  return m;
}

//==============================================================================
// Direct fread/fwrite between RB and the specified FILE stream, similar to 
//   size_t fread(void *ptr, size_t size, size_t nmemb, FILE *stream);
//   size_t fwrite(const void *ptr, size_t size, size_t nmemb, FILE *stream);

// Try to append 'n' bytes of FILE 'fpr' to the tail of RB.
// - returns "# of bytes" actually appended (fread)
size_t RBfread(RingBuffer *RB, FILE *fpr, size_t n)	// RB <--- fpr
{
  void *dst;
  size_t empty,m,mr=0;
  dst = RBappendable(RB,&empty);
  m = (n < empty) ? n : empty;
  if(m<n) {
    addAtomic(&RB->full,1L);  // buffer-full counter
    addAtomic(&RB->drop,n-m); // # of bytes dropped
  }
  if(dst) {
    mr = fread(dst,1L,m,fpr);
    if(mr<m) fprintf(stderr,"RBfread(%zu/%zu/%zu:%s)",mr,m,n,strerror(errno));
    else if(m<n) fprintf(stderr,"RBfread(%zu/%zu)",m,n);
    RBappended(RB,mr);
  }
  return mr;
}

// Try to extract 'n' bytes from the head of RB to FILE 'fpw'
// - returns "# of bytes" actually extracted (fwrite)
size_t RBfwrite(RingBuffer *RB, FILE *fpw, size_t n)	// RB ---> fpw
{
  void *src;
  size_t exist,m,mw=0;
  src = RBextractable(RB,&exist);
  m = (n < exist) ? n : exist;
  if(m<n) addAtomic(&RB->blnk,1L); // buffer-empty counter
  if(src) {
    mw = fwrite(src,1L,m,fpw);
    if(mw<m) fprintf(stderr,"RBfwrite(%zu/%zu/%zu:%s)",mw,m,n,strerror(errno));
    else if(m<n) fprintf(stderr,"RBfwrite(%zu/%zu)",m,n);
    RBextracted(RB,mw);
  }
  return mw;
}

//==============================================================================
// Direct read/write between RB and the specified file descriptor, similar to 
//   ssize_t read(int fd, void *buf, size_t count);
//   ssize_t write(int fd, const void *buf, size_t count);

// Try to append 'n' bytes of file descriptor 'fdr' to the tail of RB.
// - returns "# of bytes" actually appended (read)
ssize_t RBread(RingBuffer *RB, int fdr, size_t n)	// RB <--- fdr
{
  void *dst;
  size_t empty,m;
  ssize_t mr=0;
  dst = RBappendable(RB,&empty);
  m = (n < empty) ? n : empty;
  if(m<n) {
    addAtomic(&RB->full,1L);  // buffer-full counter
    addAtomic(&RB->drop,n-m); // # of bytes dropped
  }
  if(dst) {
    mr = read(fdr,dst,m);
    if(mr < (ssize_t)m) {
      fprintf(stderr,"RBread(%zu/%zu/%zu:%s)",mr,m,n,strerror(errno));
      if(mr<0) mr = 0;
    }else if(m<n) fprintf(stderr,"RBread(%zu/%zu)",m,n);
    RBappended(RB,mr);
  }
  return mr;
}

// Try to extract 'n' bytes from the head of RB to file descriptor 'fdw'
// - returns "# of bytes" actually extracted (write)
ssize_t RBwrite(RingBuffer *RB, int fdw, size_t n)	// RB ---> fdw
{
  void *src;
  size_t exist,m;
  ssize_t mw=0;
  src = RBextractable(RB,&exist);
  m = (n < exist) ? n : exist;
  if(m<n) addAtomic(&RB->blnk,1L); // buffer-empty counter
  if(src) {
    mw = write(fdw,src,m);
    if(mw<(ssize_t)m) fprintf(stderr,"RBwrite(%zu/%zu/%zu:%s)",mw,m,n,strerror(errno));
    else if(m<n) fprintf(stderr,"RBwrite(%zu/%zu)",m,n);
    RBextracted(RB,mw);
  }
  return mw;
}

//==============================================================================
// Direct read/write between RB and the specified socket (UDP/IP), similar to
//   ssize_t read(int sd, void *buf, size_t count);
//   ssize_t write(int sd, const void *buf, size_t count);

// Try to append at most 'n_max' bytes received on socket 'sd' to the tail of RB.
// Discard the first 'o' bytes (o >= 0) of the UDP payload.
// These 'o' are not part of the maximum desired read size 'n_max'.
// Is not thread safe.
// - returns "# of bytes" that were actually appended to RB after receive (recv)
ssize_t RBsudppread(RingBuffer *RB, int sd, size_t n_max, off_t o)        // RB <--- UDP/IP
{
  void *dst;
  size_t avail;
  ssize_t mr=0;
  assert(o>=0);
  assert((n_max+o)<=65500);
  dst = RBappendable(RB,&avail);
  if (avail<n_max) {
    // No space in RB right now; recv into own buffer first (and figure out actual UDP size)
    mr = recv(sd, RB->rxaddr, n_max+o, 0);
    if ((mr == -1) && (errno == EAGAIN)) {
      fprintf(stderr,"RBsudpread(-1,No UDP packets received until socket timeout)\n");
      return 0;
    }
    dst = RBappendable(RB,&avail);
    if(avail<(size_t)mr) {
      addAtomic(&RB->full,1L);  // buffer-full counter
      addAtomic(&RB->drop,mr);  // # of bytes dropped i.e. entire UDP packet
      return 0;
    }
    memcpy(dst,((unsigned char*)(RB->rxaddr))+o,mr-o);
    RBappended(RB,mr-o);
  } else {
    // Space available in RB; receive directly into RB
    mr = recv(sd, dst, n_max+o, 0);
    if ((mr == -1) && (errno == EAGAIN)) {
      fprintf(stderr,"RBsudpread(-1,No UDP packets received until socket timeout)\n");
      return 0;
    }
    if (o > 0) {
        memmove(dst,(char*)dst + o,mr-o);
    }
    RBappended(RB,mr-o);
  }
  return mr-o;
}

ssize_t RBsudpread(RingBuffer *RB, int sd, size_t n_max)
{
  return RBsudppread(RB,sd,n_max,0);
}

// Try to send exactly 'n' bytes from the head of RB in a UDP/IP packet.
// Note that UDP/IP is limited to <64kB packets.
// - returns "# of bytes" transmitted (sendto)
size_t RBsudpwrite(RingBuffer *RB, int sd, size_t n,
                   const struct sockaddr *to, socklen_t tolen)	// RB ---> UDP/IP
{
  void *src;
  size_t exist;
  ssize_t mw=0;
  src = RBextractable(RB,&exist);
  if(exist<n) {
     addAtomic(&RB->blnk,1L); // buffer-empty counter
  } else if(src) {
    mw = sendto(sd,src,n,0,to,tolen);
    if(mw<(ssize_t)n) fprintf(stderr,"RBsudpwrite(%zd/%zu/%zu:%s)",mw,n,exist,strerror(errno));
    RBextracted(RB,mw);
  }
  return mw;
}

//==============================================================================
