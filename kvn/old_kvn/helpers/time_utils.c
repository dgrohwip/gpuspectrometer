#ifndef TIME_UTILS_C // in case "include"d from NVCC compile
#define TIME_UTILS_C

#include "time_utils.h"

#include <stddef.h>
#include <stdio.h>
#include <time.h>
#include <sys/time.h>

/**
 * Helper that converts a 'struct timeval' into a string representation.
 * The output format is specified in 'fmt'.
 */
void timeval2YYYYMMDDhhmmss_fmt(const struct timeval *tv, char *dst, const size_t ndst, const char* fmt)
{
    struct tm* tm_val; // gmtime() internal static buffer, does not need free()!
    double     tm_sec;
    tm_val = gmtime((const time_t*)tv);
    tm_sec = tm_val->tm_sec + 1e-6*tv->tv_usec;
    snprintf(dst, ndst, fmt,
             1900 + tm_val->tm_year, tm_val->tm_mon, tm_val->tm_mday,
             tm_val->tm_hour, tm_val->tm_min, tm_sec
    );
}

/**
 * Helper that converts a 'struct timeval' into a string representation.
 * The output format is "%04d/%02d/%02d %02d:%02d:%06.3f".
 */
void timeval2YYYYMMDDhhmmss(const struct timeval *tv, char *dst, const size_t ndst)
{
    return timeval2YYYYMMDDhhmmss_fmt(tv, dst, ndst, "%04d/%02d/%02d %02d:%02d:%06.3f");
}

/**
 * Helper that converts a 'struct timeval' into a string representation, with Day-of-Year.
 * The output format is specified in 'fmt'.
 */
void timeval2YYYYDDDhhmmss_fmt(const struct timeval *tv, char *dst, const size_t ndst, const char* fmt)
{
    struct tm* tm_val; // gmtime() internal static buffer, does not need free()!
    double     tm_sec;
    tm_val = gmtime((const time_t*)tv);
    tm_sec = tm_val->tm_sec + 1e-6*tv->tv_usec;
    snprintf(dst, ndst, fmt,
             1900 + tm_val->tm_year, tm_val->tm_yday + 1,
             tm_val->tm_hour, tm_val->tm_min, tm_sec
    );
}

/**
 * Helper that converts a 'struct timeval' into a string representation, with Day-of-Year.
 * The output format is "%04d/%03d %02d:%02d:%06.3f".
 */
void timeval2YYYYDDDhhmmss(const struct timeval *tv, char *dst, const size_t ndst)
{
    return timeval2YYYYDDDhhmmss_fmt(tv, dst, ndst, "%04d/%03d %02d %02d:%02d:%06.3f");
}

#endif // TIME_UTILS_C
