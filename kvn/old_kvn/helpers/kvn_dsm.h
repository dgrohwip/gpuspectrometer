/////////////////////////////////////////////////////////////////////////////////////
//
// Definition of structures used by the KVN Digital Spectrometer (hardware).
// Follows specifications in 2007KVN-SPC0013_SpectralResultsFile_Spec_ver_110_E.pdf
//
// File format:
// bytes     0 to    83 = 84B long HEADER as in kvn_dsm_dataheader_t below
// bytes    84 to 16467 = 16kB of power spectrum of F1
// bytes 16468 to 32851 = 16kB of power spectrum of F2
// bytes 32852 to 65619 = 16kB Imag + 16 kB real of cross-power spectrum F1 x F2
// followed by any additional pairs (F3,F4,F4xF4, F5,F6,F5xF6, ...)
//
// Documentation errors:
// output_streams field : Section 5.1.1 says it is 2-byte 16-bit,
//                        Section 5.2   says it is 4-byte
//
/////////////////////////////////////////////////////////////////////////////////////

#ifndef KVN_DSM_H
#define KVN_DSM_H

#include <stdint.h>
#include <stddef.h>
#include <sys/time.h>

#define KVN_DSM_MAX_IP_PER_FILE 10000

typedef struct kvn_dsm_dataheader_tt {
    uint32_t    sequence_number;     // start at 0, increment +1 in every Integration Period (IP), max IP is 10,000-1 (then new file)
    char        start_time[7];       // YYYYDDDhhmmss0 with lowest 4 bit of last byte always 0
    char        input_mode[1];       // 1: Narrow Band Mode, 0: Wide Band Mode
    uint16_t    output_streams;      // 16-bit bitmask for "spectrum <x> is present", bit 0 = Auto f1, bit 1 = Auto f2, Bit 2,3 = Re,Im Cross f1x1f2
    uint32_t    valid_samples[8];    // for Narrow Band Mode is num of valid input samples; low 4 bytes zero in Wide Band Mode
    uint32_t    invalid_samples[8];  //  -"- invalid input samples; -"-
    char        integration_length;  // 1: 10.24ms, 2: 51.2ms, 3:102.4ms, 4:512ms, 5:1024ms
    char        output_mode;         // 1: Channel Select mode, 2: Channel Bind mode
    uint16_t    start_channel_index; // "head f-ch index of a spectrum result" (??), except if in Channel Bind mode, then this is 0x0000
} kvn_dsm_dataheader_t;

typedef struct kvn_dsm_writer_tt {
    const char* base_filename; // base file name which later forms "basename.<serial number>.SPC.dat"
    size_t      serialnum;     // starts at 0, increments once every 10,000 IP (KVN_DSM_MAX_IP_PER_FILE)
    size_t      ipnum;         // current Integration Period number, increments "infinitely"
} kvn_dsm_writer_t;

kvn_dsm_writer_t* create_kvn_dsm(const char* base_name);

void append_kvn_dsm(kvn_dsm_writer_t* w,
    const float** dauto,  size_t nauto,
    const float** dcross, size_t ncross,
    size_t nvalid,  size_t ninvalid,
    size_t nchan,
    const struct timeval* tv
);

#endif // KVN_DSM_H
