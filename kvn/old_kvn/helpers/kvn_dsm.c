/////////////////////////////////////////////////////////////////////////////////////
//
// Definition of structures used by the KVN Digital Spectrometer (hardware).
// Follows specifications in 2007KVN-SPC0013_SpectralResultsFile_Spec_ver_110_E.pdf
//
// File format:
// bytes     0 to    83 = 84B long HEADER as in kvn_dsm_dataheader_t below
// bytes    84 to 16467 = 16kB of power spectrum of F1
// bytes 16468 to 32851 = 16kB of power spectrum of F2
// bytes 32852 to 65619 = 16kB Imag + 16 kB real of cross-power spectrum F1 x F2
// followed by any additional pairs (F3,F4,F4xF4, F5,F6,F5xF6, ...)
//
// Documentation errors:
// output_streams field : Section 5.1.1 says it is 2-byte 16-bit,
//                        Section 5.2   says it is 4-byte
//
/////////////////////////////////////////////////////////////////////////////////////

#ifndef KVN_DSM_C // in case file is "include"d e.g. for easier NVCC compilation
#define KVN_DSM_C

#include "kvn_dsm.h"
#include "time_utils.h"

#include <ctype.h>
#include <stdio.h>
#include <stdint.h>
#include <memory.h>

typedef union kvn_dsm_out_streams_tt {
   uint16_t word16;
   struct bit_tt {
   // Byte 0
   uint8_t auto_f1 : 1;
   uint8_t auto_f2 : 1;
   uint8_t cross_f1f2_re : 1;
   uint8_t cross_f1f2_im : 1;
   uint8_t auto_f3 : 1;
   uint8_t auto_f4 : 1;
   uint8_t cross_f3f4_re : 1;
   uint8_t cross_f3f4_im : 1;
   // Byte 1
   uint8_t auto_f5 : 1;
   uint8_t auto_f6 : 1;
   uint8_t cross_f5f6_re : 1;
   uint8_t cross_f5f6_im : 1;
   uint8_t auto_f7 : 1;
   uint8_t auto_f8 : 1;
   uint8_t cross_f7f8_re : 1;
   uint8_t cross_f7f8_im : 1;
   } bit;
} kvn_dsm_out_streams_t;

/////////////////////////////////////////////////////////////////////////////////////

static void str2bcd(char* io)
{
    char* out = io;
    while (1) {
        int v = toupper(*(io++));
        if (v == '\0') { *out = 0x00; out++; break; }
        *out = (v>='0' && v<='9') ? (v-'0')<<4 : (10+v-'A')<<4; // TODO: check for illegal inputs
        v = toupper(*(io++));
        if (v == '\0') { *out &= 0xF0; out++; break; }
        *out |= (v>='0' && v<='9') ? (v-'0')&0x0F : (10+v-'A')&0x0F; // TODO: check for illegal inputs
        out++;
    }
    while (out <= io) {
        *out = '\0';
        out++;
    }
}

#ifdef TEST_BCD
int main(int argc, char** argv)
{
   if (argc > 1) {
       char* s = strdup(argv[1]);
       int i;
       str2bcd(s);
       for (i = 0; i < (strlen(argv[1])+1)/2; i++) { printf("%02x", (unsigned char)s[i]); }
       printf("\n");
   }
   return 0;
}
#endif

/////////////////////////////////////////////////////////////////////////////////////

kvn_dsm_writer_t* create_kvn_dsm(const char* base_name)
{
    kvn_dsm_writer_t* w = (kvn_dsm_writer_t*)malloc(sizeof(kvn_dsm_writer_t));
    memset(w, 0x00, sizeof(kvn_dsm_writer_t));
    w->base_filename = strdup(base_name);
    return w;
}

/////////////////////////////////////////////////////////////////////////////////////

void append_kvn_dsm(kvn_dsm_writer_t* w,
    const float** dauto,  size_t nauto,
    const float** dcross, size_t ncross,
    size_t nvalid,  size_t ninvalid,
    size_t nchan,
    const struct timeval* tv
)
{
    kvn_dsm_out_streams_t streammask;
    kvn_dsm_dataheader_t h;
    char fpath[4096];
    char dtime[20];

    snprintf(fpath, sizeof(fpath), "%s.%zu.SPC.dat", w->base_filename, w->serialnum);
    FILE* f = fopen(fpath, "a");

    timeval2YYYYDDDhhmmss_fmt(tv, dtime, sizeof(dtime), "%04d%03d%02d%02d%02.0f");
    str2bcd(dtime);
    memcpy(h.start_time, dtime, sizeof(h.start_time));

    h.sequence_number = w->ipnum % KVN_DSM_MAX_IP_PER_FILE;
    h.start_time[6] &= 0xF0;
    h.input_mode[0] = 1;
    streammask.word16 = 0;
    streammask.bit.auto_f1 = (nauto >= 1);
    streammask.bit.auto_f2 = (nauto >= 2);
    streammask.bit.auto_f3 = (nauto >= 3);
    streammask.bit.auto_f4 = (nauto >= 4);
    streammask.bit.auto_f5 = (nauto >= 5);
    streammask.bit.auto_f6 = (nauto >= 6);
    streammask.bit.auto_f7 = (nauto >= 7);
    streammask.bit.auto_f8 = (nauto >= 8);
    streammask.bit.cross_f1f2_re = (ncross >= 1);
    streammask.bit.cross_f1f2_im = (ncross >= 1);
    streammask.bit.cross_f3f4_re = (ncross >= 2);
    streammask.bit.cross_f3f4_im = (ncross >= 2);
    streammask.bit.cross_f5f6_re = (ncross >= 3);
    streammask.bit.cross_f5f6_im = (ncross >= 3);
    streammask.bit.cross_f7f8_re = (ncross >= 4);
    streammask.bit.cross_f7f8_im = (ncross >= 4);
    h.output_streams = streammask.word16;
    h.integration_length = 16; //use some undefined value like 16
    h.start_channel_index = 0;
    h.output_mode = 2;

    if (streammask.bit.auto_f1) {
        fwrite(dauto[0], nchan*sizeof(float), 1, f);
    }
    if (streammask.bit.auto_f2) {
        fwrite(dauto[1], nchan*sizeof(float), 1, f);
    }
    if (streammask.bit.cross_f1f2_re) {
        // TODO: what is the input-side format for cross-spectra?
        // Is it {Re Re Re .. Im Im Im ...} or is it {Re Im Re Im Re Im ...}?
        fwrite(dcross[0], nchan*sizeof(float)/2, 1, f);
    }
    if (streammask.bit.cross_f1f2_im) {
        fwrite(dcross[0]+nchan/2, nchan*sizeof(float)/2, 1, f);
    }

    fclose(f);

    w->ipnum++;
    if (w->ipnum >= KVN_DSM_MAX_IP_PER_FILE) {
        w->serialnum++;
    }
}


#endif
