#include "dumpfile_io.h"

/** Write a byte array into binary file */
void write_to_binary_file(const char* fname, const void* d, size_t len)
{
    FILE* fout = fopen(fname, "wb");
    if (NULL == fout) {
        perror("Output file error");
        return;
    }
    fwrite((void*)d, len, 1, fout);
    fclose(fout);
}

/** Write float32 array into text file */
void write_float32_to_text_file(const char* fname, const float* d, size_t len)
{
    size_t i;

    FILE* fout = fopen(fname, "wb");
    if (NULL == fout) {
        perror("Output file error");
        return;
    }

    for (i = 0; i < len; i++) {
        fprintf(fout, "%zu %e\n", i, d[i]);
    }

    fclose(fout);
}

/** Write float32 array into binary file */
void write_float32_to_binary_file(const char* fname, const float* d, size_t len)
{
    write_to_binary_file(fname, (void*)d, sizeof(float)*len);
}

