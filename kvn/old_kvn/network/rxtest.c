#include "udp_receiver.h"

#include <sched.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <malloc.h>
#include <sys/time.h>
#include <unistd.h>

#define N_BUFFERS 4
#define L_BUFFERS (8192+32+8)*800*16 // later should depend on integration time
#define N_SAMP_PER_BYTE 4

void process_data(rx_buffers_t*, float*);
void realtime_init(int cpu);

int main(int argc, char** argv)
{
	const char port[] = "46227"; // default for FILA10G VDIF
	rx_buffers_t* rx;
	float* f32;

	realtime_init(0);

	/* Allocate 2-bit -> 32-bit float decoder output */
	f32 = memalign(16*4096, L_BUFFERS*N_SAMP_PER_BYTE*sizeof(float));

	/* Begin receiving in background */
	rx = udp_receiver_init(port, N_BUFFERS, L_BUFFERS);
	udp_receiver_start(rx);

	/* Process in main process, or in multiple competing threads */
	while (1) {
		process_data(rx, f32);
	}

	return 0;
}

void process_data(rx_buffers_t* rx, float* f32)
{
	size_t i, my_buf = 0;
	double dT, R_Mbps, R_Msps;

	/* Grab a filled but still unprocessed buffer */
	pthread_mutex_lock(&rx->lock);
	while (rx->Nunconsumed <= 0) {
		pthread_cond_wait(&rx->data_available, &rx->lock);
	}
	for (i = 0; i < (rx->Nbuffers + 1); i++) {
		if (i == rx->Nbuffers) {
			printf("Error: pthread_cond_signal() but no new unprocessed buffers!!\n");
			break;
		}
		if (!rx->buffer_is_held[i] && !rx->buffer_is_consumed[i]) {
			rx->buffer_is_held[i] = 1;
			my_buf = i;
			break;
		}
	}
	pthread_mutex_unlock(&rx->lock);

	/* Statistics */
	for (i = 0; i < rx->Nbuffers; i++) {
		printf("Buffer %2zu : held=%d consumed=%d : %zu bytes\n",
			i, rx->buffer_is_held[i], rx->buffer_is_consumed[i],
			rx->buffer_nbytes[i]
		);
	}
	dT = (rx->buffer_stoptimes[my_buf].tv_sec - rx->buffer_starttimes[my_buf].tv_sec)
	     + 1e-6 * (rx->buffer_stoptimes[my_buf].tv_usec - rx->buffer_starttimes[my_buf].tv_usec);
	R_Mbps = 8.0e-6 * rx->buffer_nbytes[my_buf] / dT;
	R_Msps = (R_Mbps/8.0) * (8192.0 / (8192.0+32+8)) * N_SAMP_PER_BYTE;
	printf("Got data with dT=%.3fs at %.3f Mbps (%.3f Ms/s goodput)\n", dT, R_Mbps, R_Msps);

	/* Processing */
	// decode
/*
        decode_2bit_1channel_cpu_blocking(
		(const uint32_t*)rx->buffers[my_buf],
		f32, L_BUFFERS*N_SAMP_PER_BYTE
	);
	// parallel PFB or FFT implementation
*/

	/* Release the buffer */
	pthread_mutex_lock(&rx->lock);
	rx->buffer_is_held[my_buf] = 0;
	rx->buffer_is_consumed[my_buf] = 1;
	rx->buffer_nbytes[my_buf] = 0;
	rx->Nunconsumed--;
	pthread_mutex_unlock(&rx->lock);

	/* Store results of processing */
	// ...
}

void realtime_init(int cpu)
{
	int rc;
	cpu_set_t set;
	CPU_ZERO(&set);
	CPU_SET(cpu, &set);
	rc = sched_setaffinity(0, sizeof(set), &set);
	if (rc < 0) {
		printf("sched_setaffinity: could not set CPU affinity (maybe must run as root)?\n");
	} else {
		printf("Bound to CPU#%d\n", cpu);
	}
	return;
}

