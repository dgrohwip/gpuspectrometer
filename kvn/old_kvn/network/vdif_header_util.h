#ifndef VDIF_HEADER_UTIL_H
#define VDIF_HEADER_UTIL_H

#include <stdint.h>

/* VDIF Header bit-field, based on DiFX correlator 'vdifio' library file vdifio.h */
typedef struct vdif_header_tt {
   uint32_t seconds : 30;
   uint32_t legacymode : 1;
   uint32_t invalid : 1;

   uint32_t frame : 24;
   uint32_t epoch : 6;
   uint32_t unassigned : 2;

   uint32_t framelength8 : 24;  // Frame length (including header) divided by 8
   uint32_t nchan : 5;
   uint32_t version : 3;

   uint32_t stationid : 16;
   uint32_t threadid : 10;
   uint32_t nbits : 5;
   uint32_t iscomplex : 1;

   uint32_t extended1 : 24;
   uint32_t eversion : 8;

   uint32_t extended2;
   uint32_t extended3;
   uint32_t extended4;
} vdif_header_t;

#endif
