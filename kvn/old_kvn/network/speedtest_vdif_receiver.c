#include "vdif_receiver_rb.h"

#include <assert.h>
#include <getopt.h>
#include <malloc.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/time.h>

void usage(void)
{
    fprintf(stderr, "Usage: speedtest_vdif_receiver <source> <VDIF_<payloadlen>-<Mbps>-<nchan>-<nbit>>\n\n");
}

int main(int argc, const char** argv)
{
    const char *rx_source, *raw_format;
    double T_int, T_int_wish = 0.256;

    int rx_cpu = 0, rx_udpoffset = 8;
    int rx_payloadlen, rx_Mbps, rx_nchan, rx_nbit;

    vdif_rx_t *rx;
    unsigned char *rx_buf;
    size_t bufsize, nrd, iter=0, ntotal=0;
    struct timeval tv_mid, tv_start, tv_stop, tv_prev;

    /* Check args */
    if (argc != 3) {
        usage();
        return -1;
    }

    rx_source = strdup(argv[1]);
    raw_format = strdup(argv[2]);

    /* Parse the VDIF_size-Mbps-nchan-nbit format string */
    if (strncasecmp(raw_format, "VDIF_", 5) != 0) {
        usage();
        return -1;
    }
    int nc = sscanf(raw_format+5, "%d-%d-%d-%d", &rx_payloadlen, &rx_Mbps, &rx_nchan, &rx_nbit);
    if ((nc != 4) || (rx_payloadlen < 512) || (rx_Mbps < 64) || (rx_nchan < 1) || (rx_nchan > 1024))
    {
        fprintf(stderr, "%s: format error! Use VDIF_<payloadlen>-<Mbps>-<nchan>-<nbits> with valid values\n", raw_format);
        return -1;
    }

    /* Start VDIF receiver */
    //rx = udp_vdif_receiver(rx_source, rx_payloadlen+32, rx_udpoffset, rx_Mbps);
    rx = filesource_vdif_receiver(rx_source, rx_payloadlen+32, 0, rx_Mbps);
    T_int = vdif_receiver_fit_segmenttime(rx, T_int_wish);
    bufsize = vdif_receiver_get_segmentsize(rx, T_int);
    if (rx_cpu >= 0) {
        vdif_receiver_bind_cpu(rx, rx_cpu);
    }
    vdif_receiver_start(rx);
    rx_buf = memalign(4096, bufsize);

    /* Receive data */
    gettimeofday(&tv_start, NULL);
    tv_prev = tv_start;
    while (iter++ < 512) {

        double R, dT;

        nrd = vdif_receiver_get_segment(rx, rx_buf, T_int, &tv_mid);
        gettimeofday(&tv_stop, NULL);
        ntotal += nrd;

        dT = (tv_stop.tv_sec - tv_start.tv_sec) + 1e-6*(tv_stop.tv_usec - tv_start.tv_usec);
        R = (ntotal*8.0)/dT;

        fprintf(stdout, "segment %3zu : avg. throughput is %.2f Gbps (%.2f Gsps) since program start, ", iter, R*1e-9, R*1e-9/rx_nbit);

        dT = (tv_stop.tv_sec - tv_prev.tv_sec) + 1e-6*(tv_stop.tv_usec - tv_prev.tv_usec);
        R = (nrd*8.0)/dT;
        tv_prev = tv_stop;

        fprintf(stdout, "current %.2f Gbps (%.2f Gsps)\n ", R*1e-9, R*1e-9/rx_nbit);

        if (nrd <= 0) { break; }
    }

    /* Speed */

    return 0;
}
