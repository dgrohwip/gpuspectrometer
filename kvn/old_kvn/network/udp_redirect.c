/* http://www.brokestream.com/udp_redirect.html

  Build: gcc -Wall -O3 -o udp_redirect udp_redirect.c

  udp_redirect.c
  Version 2008-11-09

  Copyright (C) 2007 Ivan Tikhonov

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

  Ivan Tikhonov, kefeer@brokestream.com

  Changes: 03/2016 Jan Wagner, modified to allow UDP port change, and skipping UDP

*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <malloc.h>
#include <netinet/in.h>
#include <arpa/inet.h>


int main(int argc, char *argv[]) {
	void* buf; int nskip=0; int yes=1;
	size_t NSKIP = 1;
	if (argc!=3 && argc!=5 && argc!=6) {
		printf("Usage: %s our-ip our-port send-to-ip send-to-port <send-every-Nth-packet-only>\n",argv[0]);
		printf("Usage: %s our-ip our-port             # echo mode\n",argv[0]);
		exit(1);
	}

	int osrx=socket(PF_INET,SOCK_DGRAM,IPPROTO_IP);
	int ostx=socket(PF_INET,SOCK_DGRAM,IPPROTO_IP);
	if (setsockopt(osrx, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) < 0) { perror("SO_REUSEADDR"); }
	if (setsockopt(osrx, SOL_SOCKET, SO_NO_CHECK, &yes, sizeof(yes)) < 0) { perror("RX SO_NO_CHECK"); }
	if (setsockopt(ostx, SOL_SOCKET, SO_NO_CHECK, &yes, sizeof(yes)) < 0) { perror("TX SO_NO_CHECK"); }

	struct sockaddr_in ai;
	struct sockaddr_in ao;
	ai.sin_family=AF_INET;
	ai.sin_addr.s_addr=inet_addr(argv[1]);
	ai.sin_port=htons(atoi(argv[2]));
	if(bind(osrx,(struct sockaddr *)&ai,sizeof(ai)) == -1) {
		printf("Can't bind our address (%s:%s)\n", argv[1], argv[2]);
		exit(1);
	}

	if(argc>=5) {
		ao.sin_addr.s_addr=inet_addr(argv[3]);
		ao.sin_port=htons(atoi(argv[4]));
		printf("SendTo 3rd  %08X:%d\n", ao.sin_addr.s_addr, ao.sin_port);
	}
	if(argc==6) {
		NSKIP=atoi(argv[5]);
	}

	struct sockaddr_in arx;
	buf = memalign(4096, 65535);
	while(1) {
		socklen_t sn=sizeof(arx);
		ssize_t nw = 0;
		ssize_t nr = recvfrom(osrx,buf,65536,0,(struct sockaddr *)&arx,&sn);
		if(nr < 0) {
			perror("recvfrom");
			continue;
		}
		if (nr <= 0) continue;
		nskip = (nskip+1)%NSKIP;
		if (nskip > 0) continue;
		if(argc==3) {
			// Echo
			nw=sendto(ostx,buf,nr,0,(struct sockaddr *)&arx,sn);
		} else {
			// Forward
			nw=sendto(ostx,buf,nr,0,(struct sockaddr *)&ao,sizeof(ao));
		}
		if (nw < 0) {
			perror("sendto");
		}
	}
}

