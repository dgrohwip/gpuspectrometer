function udpvdifreceiverSynth()

%% Test tone generator

wtone = 2*pi/256;
nsamples_per_frame = (8/2)*10000;
nsamples = 400 * nsamples_per_frame;
sec = 2;
s = single( randn(nsamples, 1) );
s = s + single( 0.125*sin(wtone*(1:nsamples)') );
s = s + single( 0.125*sin((wtone + sec*wtone/8192)*(1:nsamples)') );
sigma = sqrt(1.0^2 + 0.125^2 + 0.125^2);
sq = m_quantize(s,sigma);

%% Spectrometer

if 1,
    Lfft = 32768*2;
    N = floor(numel(s)/Lfft)*Lfft;
    ss = reshape(s(1:N), [Lfft, numel(s(1:N))/Lfft]);
    Fs = sum(abs(fft(ss,[],1)),2);

    ssq = reshape(sq(1:N), [Lfft, numel(sq(1:N))/Lfft]);
    Fs_q = sum(abs(fft(ssq,[],1)),2);
    std(ssq(:))

    figure(), 
    semilogy(Fs(1:(Lfft/2)), 'k-'), hold on;
    semilogy(Fs_q(1:(Lfft/2)), 'g:'), hold on;
    legend('Floating point data','2-bit quantized data');
    
end

end

function sq=m_quantize(s, sigma)
    v0 = 0.982*sigma;
    q0 = 3.3359;
    sq = single(s(:));
    for i=1:numel(s),
        if s(i) < -v0,
            sq(i) = -q0;
        elseif s(i) < 0,
            sq(i) = -1.0;
        elseif s(i) < v0,
            sq(i) = +1.0;
        else
            sq(i) = +q0;
        end
    end
end

