/////////////////////////////////////////////////////////////////////////////////////
//
// Spectrometer config settings and CUDA data buffer holder structure
//
// (C) 2015 Jan Wagner, Jongsoo Kim
//
/////////////////////////////////////////////////////////////////////////////////////

#ifndef KFFTSPEC_CONFIG_H
#define KFFTSPEC_CONFIG_H

#include "vdif_receiver_rb.h"
#include <unistd.h>
#include <stdio.h>
#include <sys/time.h>
#include <cufft.h>

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define FFTSPEC_MAX_GPUS     8   // Max nr of GPUs to use in parallel
#define FFTSPEC_MAX_STREAMS  4   // Max nr of CUDA streams to create on each GPU
#define FFTSPEC_MAX_SUBBANDS 16  // Input data, maximum nr of subbands (aka IFs aka channels) to support

#define OUT_FORMAT_KVNDSM    1
#define OUT_DISCARD          64

#define WINDOW_FUNCTION_NONE 0
#define WINDOW_FUNCTION_HANN 1
#define WINDOW_FUNCTION_HAMMING 2
#define WINDOW_FUNCTION_HFT248D 3

#define SPEC_INVALID      -1.0

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Spectrometer Configuration & CUDA Buffers
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

typedef struct fftspec_config_tt
{
    double T_int;     // Integration time in seconds
    double T_int_wish;// Originally requested integration time in seconds, may not fit framing & data rate
    double bw;        // Bandwidth of one band in Hz
    int    nchan;     // Number of output channels (r2c FFT length is <in>=2*nchan, <out>=nchan+1)
    int    nfft;      // Number of FFTs per subband that fit into T_int
    int    nsubints;  // Number of subintegrations to evenly split 'nfft' into.
    int    do_cross;  // 1 to form cross-power spectra, in addition to power spectra
    int    nspecs;    // total number of averaged spectra to be produced, including cross-power if enabled
    int    do_window; // 1 to apply window function prior to FFT
    int    window_type; // window function (Hann, Hanmming, flat-top, ...)
    float  window_overlap; // amount of overlap between FFTs in percent : TODO

    /* Time division multiplexing */
    int nGPUs;     // How many GPU to use in parallel
    int nstreams;  // How many CUDA Streams per GPU to create

    /* VDIF data receiver object (both file or UDP/IP) */
    vdif_rx_t *rx;
    char *raw_format;
    int raw_nsubbands;
    int raw_nbits;

    /* Raw input sample data (headerless, with a fill pattern for missing frames) */
    struct timeval h_rawbuf_midtimes[FFTSPEC_MAX_GPUS][FFTSPEC_MAX_STREAMS]; // Timestamps (from VDIF) at midpoint of integration period
    unsigned char* h_rawbufs[FFTSPEC_MAX_GPUS][FFTSPEC_MAX_STREAMS]; // Pointers to full integration period -sized data chunks
    unsigned char* d_rawbufs[FFTSPEC_MAX_GPUS][FFTSPEC_MAX_STREAMS]; // -"- on GPU
    size_t         rawbuflen;                                        // Length in bytes for raw input data of one integration period
    unsigned char* h_random;

    /* Stream and FFT setup */
    int            devicemap[FFTSPEC_MAX_GPUS];                // Map own index to GPU device number (default is 0,1,2,3,..)
    cudaStream_t   sid[FFTSPEC_MAX_GPUS][FFTSPEC_MAX_STREAMS]; // IDs of Streams created on the GPUs
    cufftHandle    cufftplans[FFTSPEC_MAX_GPUS][FFTSPEC_MAX_STREAMS];
    cudaDeviceProp devprops[FFTSPEC_MAX_GPUS];

    /* On-GPU arrays */
    float*         d_fft_in[FFTSPEC_MAX_GPUS][FFTSPEC_MAX_STREAMS];  // input to r2c FFT
    float*         d_fft_out[FFTSPEC_MAX_GPUS][FFTSPEC_MAX_STREAMS]; // output of r2c FFT (type is recast cufftComplex)
    cu_window_cb_params_t  h_cufft_userparams;                       // extra user params in cuFFT Callback
    cu_window_cb_params_t* d_cufft_userparams[FFTSPEC_MAX_GPUS];     // device copy of -"-, one copy per GPU is enough

    /* Output arrays */
// FIXME: increase the array dimensionalities to multi-IF via [gpu][stream][fs->raw_nsubbands]
    float*         d_powspecs[FFTSPEC_MAX_GPUS][FFTSPEC_MAX_STREAMS];  // accumulated auto&cross power spectra (interleaved {XX,Re XY,Im XY, YY) of all FFTSPEC_MAX_SUBBANDS side-by-side
    float*         h_powspecs[FFTSPEC_MAX_GPUS][FFTSPEC_MAX_STREAMS];  // -"- power spectra
    float*         h_crosspecs[FFTSPEC_MAX_GPUS][FFTSPEC_MAX_STREAMS]; // -"- cross-power spectra
    double         spec_weight[FFTSPEC_MAX_GPUS][FFTSPEC_MAX_STREAMS]; // weights of each spectrum = valid data / total data; -1 if invalid spectrum

    /* Output storage (formats: SDFITS, KVN DSM, other...?) */
    const char* scanname;
    const char* experiment;
    const char* station;
    const char* observer;
    char* out_filename;

    /* Performance tracking */
    cudaEvent_t process_cuda_starttimes[FFTSPEC_MAX_GPUS][FFTSPEC_MAX_STREAMS]; // Wallclock timestamps when processing of segment started
    cudaEvent_t process_cuda_stoptimes[FFTSPEC_MAX_GPUS][FFTSPEC_MAX_STREAMS];  // -"- finished
    cudaEvent_t process_cuda_inputdata_overwriteable[FFTSPEC_MAX_GPUS][FFTSPEC_MAX_STREAMS];  // Event happens after raw input data area can be freely overwritten
    cudaEvent_t process_cuda_spectrum_available[FFTSPEC_MAX_GPUS][FFTSPEC_MAX_STREAMS];  // Event happens after spectrum completely copied to host
    cudaEvent_t estart[FFTSPEC_MAX_GPUS][FFTSPEC_MAX_STREAMS];  // kernel timing
    cudaEvent_t estop[FFTSPEC_MAX_GPUS][FFTSPEC_MAX_STREAMS];   // kernel timing

} fftspec_config_t;

#endif // KFFTSPEC_CONFIG_H
