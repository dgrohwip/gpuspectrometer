#!/usr/bin/python
"""
stat_kfftspec.py Version 1.0  20160203 Jan Wagner

Usage: stat_kfftspec.py <filename.kfftspec> <start ch> <stop ch>

Shows statistics for each spectrum in kfftspec files produced by
the kfftspec software spectrum.
"""

import numpy, sys, struct
from kfftspecReader import kfftspecReader

# Check args, show usage if necessary
if len(sys.argv)!=4:
    print __doc__
    sys.exit(-1)

def mad(v):
    med = numpy.median(v)
    return numpy.median(numpy.abs(v - med))

def process_kfftspec_file(fname,startch,stopch):

    startch  = max(1,startch)
    kfftspec = kfftspecReader(fname)

    snr_sum = 0
    w_sum = 0

    while True:

        (h,s) = kfftspec.get_next_spectrum()
        if h == None or s == None:
            # print ('EOF')
            break
        N = len(s)
	# print N

        t = ('signal %d x %d, Tint=%.4fs, bw=%.3f kHz, w=%.3f, T=%s'
            % ( h['signal_src_1'],h['signal_src_2'],h['Tint'], 1e-3*h['bandwidth'],
                h['weight'],h['timestamp_str']) )

        x = numpy.linspace(0,N-1,N)
        x = 1024 - 1e-6*x*h['bandwidth']/float(N)
        
        m = numpy.min(s[startch:(stopch+1)])
        M = numpy.max(s[startch:(stopch+1)])
        med = numpy.median(s[startch:(stopch+1)])
        rms = mad(s[startch:(stopch+1)])
        f0 = x[startch]*1e3
        f1 = x[stopch]*1e3

        if numpy.iscomplexobj(s):
            #print ('%s : %.3f-%.3f kHz : median=%s mad=%f min=%s max=%s' % (t,f0,f1,str(med),rms,str(m),str(M)))
            pass
        else:
            #print ('%s : %.3f-%.3f kHz : median=%.3e mad=%.3e min=%.3e max=%.3e (max-mean)/mad=%.3e' % (t,f0,f1,med,rms,m,M,(M-med)/rms))
            snr = (M-med)/rms
            w_sum = w_sum + h['weight']
            snr_sum = snr_sum + h['weight']*snr

    print snr_sum/w_sum

process_kfftspec_file(sys.argv[1],int(sys.argv[2]),int(sys.argv[3]))
