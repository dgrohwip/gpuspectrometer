/////////////////////////////////////////////////////////////////////////////////////
//
// Auto-power spectrum kernels (accumulation of |X|^2 after FFT of signal x).
//
// Note that summation error is currently O(N). With Kahan summation it would
// be O(1), with split summation it would be O(log N). Summation error comes
// from rounding when adding a "small" float to a "large" float (adding new value
// to accumulator).
//
// Two variants:
//
//  1) power spectrum entirely calculated in own kernel after FFT is finished, or
//
//  2) parts of the power spectrum computation (Re^2+Im^2: 2 float -> 1 float)
//     is done via cuFFT Callback Store function during the FFT stage, and the
//     accumulation step is done in an own kernel, reducing bandwidth needs by half
//
// Performance on Titan X for 1M channels is ~37 Gs/s complex with Kahan summation
// and similar speed also with naive summation. Variant 1 comes in two versions,
// the *_v2 is good for large FFTs only, while *_v3 is suitable for small and
// large FFTs (with fixed #threads=64 then #blocks=MAX(nchan,maxphysthreads)/#threads).
//
// (C) 2015 Jongsoo Kim, Jan Wagner
//
/////////////////////////////////////////////////////////////////////////////////////

#ifndef ARITHMETIC_AUTOSPEC_KERNELS_CU
#define ARITHMETIC_AUTOSPEC_KERNELS_CU

#include "index_mapping.cu"

#include <cufftXt.h>

#define USE_KAHAN_SUMMATION 1

/////////////////////////////////////////////////////////////////////////////////////

__global__ void autoPowerSpectrum   (cufftComplex* __restrict__ c, float* __restrict__ a, const size_t output_size, const size_t batch_size);
__global__ void autoPowerSpectrum_v2(const cufftComplex* __restrict__ c, float* __restrict__ a, const size_t Nchout, const size_t Nfft);
__global__ void autoPowerSpectrum_v3(const cufftComplex* __restrict__ c, float* __restrict__ a, const size_t Nchout, const size_t Nfft);
__global__ void autoPowerSpectrum_v3_skipNyquist(const cufftComplex* __restrict__ c, float* __restrict__ a, const size_t Nchout, const size_t Nfft);
__global__ void autoPowerSpectrum_v3_CB(const cufftReal* __restrict__ c, float* __restrict__ a, const size_t Nchout, const size_t Nfft);

/////////////////////////////////////////////////////////////////////////////////////

static __device__ __host__ inline float complexSquare(cufftComplex c)
{
    return c.x*c.x + c.y*c.y;
}

__global__ void autoPowerSpectrum(cufftComplex* __restrict__ c, float* __restrict__ a, const size_t output_size, const size_t batch_size)
{
    const size_t numThreads = blockDim.x * gridDim.x;
    const size_t threadID = blockIdx.x * blockDim.x + threadIdx.x;

    for (size_t i = threadID; i < output_size*batch_size ; i += numThreads)
    {
       size_t j = i % output_size;
       atomicAdd(&a[j], complexSquare(c[i]));
    }

}

// Kernel for #threads == 'Nchout', okay for large FFTs, under-utilizes GPU if small FFTs.
__global__ void autoPowerSpectrum_v2(const cufftComplex* __restrict__ c, float* __restrict__ a, const size_t Nchout, const size_t Nfft)
{
    size_t i_start = blockIdx.x * blockDim.x + threadIdx.x;
    if (i_start < Nchout) // i_start is in range [0...Nchout-1] plus padding
    {
        float acc = a[i_start]; // load current, or set to 0.0f to discard existing accumulator contents
        size_t i = i_start;
        for (size_t j = 0; j < Nfft; j++, i += Nchout)
        {
            acc += complexSquare(c[i]);
            // TODO: Kahan summation here
        }

        // Note: writing once to a definitely not shared location is much faster than atomicAdd
        //acc /= (float)Nfft;
        a[i_start] = acc;
    }
}

// Kernel for #threads=2*warpsize and #blocks = MAX(nchan,maxphysthreads) / 2*warpsize
__global__ void autoPowerSpectrum_v3(const cufftComplex* __restrict__ c, float* __restrict__ a, const size_t Nchout, const size_t Nfft)
{
    size_t nthreads = blockDim.x * gridDim.x;
    size_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    size_t bin = idx % Nchout;
    size_t last_idx = Nchout * Nfft;

#if !USE_KAHAN_SUMMATION
    // Naive summation, error is O(N)
    float acc = 0.0f;
    while (idx < last_idx)
    {
        acc += complexSquare(c[idx]);
        idx += nthreads;
    }
#else
    // Kahan summation, error is O(1)
    // Kahan summation is ~55 Gs/s on nVidia TITAN X,
    // versus ~57 Gs/s achieved with naive summation
    float acc = 0.0f;
    float comp = 0.0f; // a running compensation for lost low-order bits
    while (idx < last_idx)
    {
        float P = complexSquare(c[idx]);
        float y = P - comp;
        float t = acc + y;
        comp = (t - acc) - y; // braces () are critical
        acc = t;
        idx += nthreads;
    }
#endif

    //acc /= (float)Nfft;
    atomicAdd(&a[bin], acc);
}

// Kernel for #threads=2*warpsize and #blocks = MAX(nchan,maxphysthreads) / 2*warpsize
// Nchout : number of spectral output bins without(!) counting the Nyquist bin
__global__ void autoPowerSpectrum_v3_skipNyquist(const cufftComplex* __restrict__ c, float* __restrict__ a, const size_t Nchout, const size_t Nfft)
{
    const size_t Lfftout = Nchout + 1;
    size_t nthreads = blockDim.x * gridDim.x;
    size_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    size_t bin = idx % Nchout;
    size_t last_idx = Nchout * Nfft;

#if !USE_KAHAN_SUMMATION
    // Naive summation, error is O(N)
    float acc = 0.0f;
    while (idx < last_idx)
    {
        size_t memidx = reindex_skip_Nyquist(Lfftout, Nchout, idx);
        acc += complexSquare(c[memidx]);
        idx += nthreads;
    }
#else
    // Kahan summation, error is O(1)
    // Kahan summation is ~55 Gs/s on nVidia TITAN X,
    // versus ~57 Gs/s achieved with naive summation
    float acc = 0.0f;
    float comp = 0.0f; // a running compensation for lost low-order bits
    while (idx < last_idx)
    {
        size_t memidx = reindex_skip_Nyquist(Lfftout, Nchout, idx);
        float P = complexSquare(c[memidx]);
        float y = P - comp;
        float t = acc + y;
        comp = (t - acc) - y; // braces () are critical
        acc = t;
        idx += nthreads;
    }
#endif

    //acc /= (float)Nfft;
    atomicAdd(&a[bin], acc);
}

/////////////////////////////////////////////////////////////////////////////////////
//
// Use cuFFT Store callback (type: CUFFT_CB_ST_COMPLEX)

__device__ void cu_autoPowerSpectrum_cufftCallbackStoreC(void *dataOut, size_t offset, cufftComplex element, void *callerInfo, void *sharedPtr)
{
    cufftReal* P = (cufftReal*)dataOut;
    P[offset] = complexSquare(element);
}
__device__ cufftCallbackStoreC cu_autoPowerSpectrum_cufftCallbackStoreC_ptr = cu_autoPowerSpectrum_cufftCallbackStoreC;

// CUFFT callback kernel ideally for #threads=2*warpsize and #blocks = MAX(nchan,maxphysthreads) / 2*warpsize
// In this kernel the input data are real (power), not complex (FFT output)
__global__ void autoPowerSpectrum_v3_CB(const cufftReal* __restrict__ P, float* __restrict__ a, const size_t Nchout, const size_t Nfft)
{
    size_t nthreads = blockDim.x * gridDim.x;
    size_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    size_t bin = idx % Nchout;
    size_t last_idx = Nchout * Nfft;

#if !USE_KAHAN_SUMMATION
    float acc = 0.0f;
    while (idx < last_idx)
    {
        acc += P[idx];
        idx += nthreads;
    }
#else
    float acc = 0.0f;
    float comp = 0.0f; // a running compensation for lost low-order bits
    while (idx < last_idx)
    {
        float y = P[idx] - comp;
        float t = acc + y;
        comp = (t - acc) - y; // braces () are critical
        acc = t;
        idx += nthreads;
    }
#endif

    //acc /= (float)Nfft;
    atomicAdd(&a[bin], acc);
}

#endif // ARITHMETIC_AUTOSPEC_KERNELS_CU
