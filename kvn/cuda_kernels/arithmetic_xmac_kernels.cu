/////////////////////////////////////////////////////////////////////////////////////
//
// Cross-power spectrum kernels (accumulation of |X|^2 after FFT of signal x).
//
// (C) 2015 Jan Wagner
//
// Performance on Titan X is about 17.0 Gs/s complex per polarization (34 Gs/s total)
// for non-interleaved output, and ~17.1 Gs/s complex per polarization for interleaved.
//
/////////////////////////////////////////////////////////////////////////////////////

#ifndef ARITHMETIC_XMAC_KERNELS_CU
#define ARITHMETIC_XMAC_KERNELS_CU

#include "index_mapping.cu"

#define XMAC_OUTPUT_INTERLEAVED 0   // 1 to store [XX Re{XY} Im{XY} YY],
                                    // 0 to store [XX XX ... YY YY ... Re Im Re Im ...]

/////////////////////////////////////////////////////////////////////////////////////

__global__ void cu_accumulate_2pol(const float2* __restrict__ x, const float2* __restrict__ y, float4* __restrict__ acc, size_t Nchout, size_t Nfft);
__global__ void cu_accumulate_2pol_skipNyquist(const float2* __restrict__ x, const float2* __restrict__ y, float4* __restrict__ acc, size_t Nchout, size_t Nfft);
__global__ void cu_accumulate_2pol_packed(const float4* __restrict__ xy, float4* __restrict__ acc, size_t Nchout, size_t Nfft);
__global__ void cu_accumulate_2pol_autoOnly(const float2* __restrict__ x, const float2* __restrict__ y, float2* __restrict__ acc, size_t Nchout, size_t Nfft);
__global__ void cu_accumulate_2pol_autoOnly_packed(const float4* __restrict__ xy, float2* __restrict__ acc, size_t Nchout, size_t Nfft);

__global__ void cu_accumulate_xpol_only_skipNyquist(const float2* __restrict__ x, const float2* __restrict__ y, float2* __restrict__ acc, size_t Nchout, size_t Nfft);

/////////////////////////////////////////////////////////////////////////////////////

__device__ float4& operator/=(float4 &a, const float f)
{
        a.x /= f; a.y /= f; a.z /= f; a.w /= f;
        return a;
}

__device__ float2& operator/=(float2 &a, const float f)
{
        a.x /= f; a.y /= f;
        return a;
}

/////////////////////////////////////////////////////////////////////////////////////

__global__ void cu_accumulate_2pol(const float2* __restrict__ x, const float2* __restrict__ y,
                                   float4* __restrict__ acc, size_t Nchout, size_t Nfft)
{
        size_t nthreads = blockDim.x * gridDim.x;
        size_t idx = blockIdx.x * blockDim.x + threadIdx.x;
        size_t last_idx = Nchout * Nfft;
        size_t bin = idx % Nchout;

        // Note: X*conj(Y) = (re1 + i im1) (re2 - i im2) = re1*re2 + im1*im2 - i re1*im2 + i re2*im1
        //                 ^= two float2 in              ^= one float4 out

        float4 a = {0.0f, 0.0f, 0.0f, 0.0f};

        while (idx < last_idx) {

             float2 P1 = x[idx]; // {Re,Im} from "X-pol"
             float2 P2 = y[idx]; // {Re,Im} from "Y-pol"

             // TODO: use Kahan summation?

             a.x +=  P1.x*P1.x + P1.y*P1.y; // .x = 1st <XX'>
             a.y +=  P1.x*P2.x + P1.y*P2.y; // .y = 1st Re{<XY'>}
             a.z += -P1.x*P2.y + P1.y*P2.x; // .z = 1st Im{<XY'>}
             a.w +=  P2.x*P2.x + P2.y*P2.y; // .w = 1st <YY'>

             idx += nthreads;
        }

        //a /= (float)Nfft;

#if XMAC_OUTPUT_INTERLEAVED
        // Layout : [XX Re{XY} Im{XY} YY]
        atomicAdd(&(acc[bin].x), a.x);
        atomicAdd(&(acc[bin].y), a.y);
        atomicAdd(&(acc[bin].z), a.z);
        atomicAdd(&(acc[bin].w), a.w);
#else
        // Layout : [XX XX XX ... YY YY YY ... Re{XY} Im{XY} Re{XY} Im{XY} Re{XY} Im{XY} ...]
        float* acc1f = (float*)acc;
        atomicAdd(&(acc1f[bin]),            a.x);
        atomicAdd(&(acc1f[Nchout+bin]),       a.w);
        atomicAdd(&(acc1f[2*Nchout+2*bin+0]), a.y);
        atomicAdd(&(acc1f[2*Nchout+2*bin+1]), a.z);
#endif

        return;
}

// Nchout : number of spectral output bins without(!) counting the Nyquist bin
__global__ void cu_accumulate_2pol_skipNyquist(const float2* __restrict__ x, const float2* __restrict__ y,
                                   float4* __restrict__ acc, size_t Nchout, size_t Nfft)
{

        const size_t Lfftout = Nchout + 1;
        size_t nthreads = blockDim.x * gridDim.x;
        size_t idx = blockIdx.x * blockDim.x + threadIdx.x;
        size_t last_idx = Nchout * Nfft;
        size_t bin = idx % Nchout;

        // Note: X*conj(Y) = (re1 + i im1) (re2 - i im2) = re1*re2 + im1*im2 - i re1*im2 + i re2*im1
        //                 ^= two float2 in              ^= one float4 out

        float4 a = {0.0f, 0.0f, 0.0f, 0.0f};

        while (idx < last_idx) {

             size_t memidx = reindex_skip_Nyquist(Lfftout, Nchout, idx);
             float2 P1 = x[memidx]; // {Re,Im} from "X-pol"
             float2 P2 = y[memidx]; // {Re,Im} from "Y-pol"

             // TODO: use Kahan summation?

             a.x +=  P1.x*P1.x + P1.y*P1.y; // .x = 1st <XX'>
             a.y +=  P1.x*P2.x + P1.y*P2.y; // .y = 1st Re{<XY'>}
             a.z += -P1.x*P2.y + P1.y*P2.x; // .z = 1st Im{<XY'>}
             a.w +=  P2.x*P2.x + P2.y*P2.y; // .w = 1st <YY'>

             idx += nthreads;
        }

        //a /= (float)Nfft;

#if XMAC_OUTPUT_INTERLEAVED
        // Layout : [XX Re{XY} Im{XY} YY]
        atomicAdd(&(acc[bin].x), a.x);
        atomicAdd(&(acc[bin].y), a.y);
        atomicAdd(&(acc[bin].z), a.z);
        atomicAdd(&(acc[bin].w), a.w);
#else
        // Layout : [XX XX XX ... YY YY YY ... Re{XY} Im{XY} Re{XY} Im{XY} Re{XY} Im{XY} ...]
        float* acc1f = (float*)acc;
        atomicAdd(&(acc1f[bin]),            a.x);
        atomicAdd(&(acc1f[Nchout+bin]),       a.w);
        atomicAdd(&(acc1f[2*Nchout+2*bin+0]), a.y);
        atomicAdd(&(acc1f[2*Nchout+2*bin+1]), a.z);
#endif

        return;
}

__global__ void cu_accumulate_xpol_only_skipNyquist(const float2* __restrict__ x, const float2* __restrict__ y, float2* __restrict__ acc, size_t Nchout, size_t Nfft)
{
        const size_t Lfftout = Nchout + 1;
        size_t nthreads = blockDim.x * gridDim.x;
        size_t idx = blockIdx.x * blockDim.x + threadIdx.x;
        size_t last_idx = Nchout * Nfft;
        size_t bin = idx % Nchout;

        // Note: X*conj(Y) = (re1 + i im1) (re2 - i im2)
        //                 = re1*re2 + im1*im2 - i re1*im2 + i re2*im1
        //                   |----real--------|  |----imag-----------|

        float2 a = {0.0f, 0.0f};

        while (idx < last_idx) {

             size_t memidx = reindex_skip_Nyquist(Lfftout, Nchout, idx);
             float2 P1 = x[memidx]; // {Re,Im} from "X-pol"
             float2 P2 = y[memidx]; // {Re,Im} from "Y-pol"

             a.x +=  P1.x*P2.x + P1.y*P2.y; // .y = 1st Re{<XY'>}
             a.y += -P1.x*P2.y + P1.y*P2.x; // .z = 1st Im{<XY'>}

             idx += nthreads;
        }

        //a /= (float)Nfft;

        // Layout : [Re{XY} Im{XY}]
        atomicAdd(&(acc[bin].x), a.x);
        atomicAdd(&(acc[bin].y), a.y);

        return;
}

__global__ void cu_accumulate_2pol_packed(const float4* __restrict__ xy, float4* __restrict__ acc, size_t Nchout, size_t Nfft)
{
        size_t nthreads = blockDim.x * gridDim.x;
        size_t idx = blockIdx.x * blockDim.x + threadIdx.x;
        size_t last_idx = Nchout * Nfft;
        size_t bin = idx % Nchout;

        // Note: X*conj(Y) = (re1 + i im1) (re2 - i im2) = re1*re2 + im1*im2 - i re1*im2 + i re2*im1
        //                 ^= two float2 in              ^= one float4 out

        float4 a = {0.0f, 0.0f, 0.0f, 0.0f};

        while (idx < last_idx) {

             float2 P1 = { xy[idx].x, xy[idx].y }; // {Re,Im} from "X-pol"
             float2 P2 = { xy[idx].z, xy[idx].w }; // {Re,Im} from "Y-pol"

             // TODO: use Kahan summation?

             a.x +=  P1.x*P1.x + P1.y*P1.y; // .x = 1st <XX'>
             a.y +=  P1.x*P2.x + P1.y*P2.y; // .y = 1st Re{<XY'>}
             a.z += -P1.x*P2.y + P1.y*P2.x; // .z = 1st Im{<XY'>}
             a.w +=  P2.x*P2.x + P2.y*P2.y; // .w = 1st <YY'>

             idx += nthreads;
        }

        //a /= (float)Nfft;

#if XMAC_OUTPUT_INTERLEAVED
        // Layout : [XX Re{XY} Im{XY} YY]
        atomicAdd(&(acc[bin].x), a.x);
        atomicAdd(&(acc[bin].y), a.y);
        atomicAdd(&(acc[bin].z), a.z);
        atomicAdd(&(acc[bin].w), a.w);
#else
        // Layout : [XX XX XX ... YY YY YY ... Re{XY} Im{XY} Re{XY} Im{XY} Re{XY} Im{XY} ...]
        float* acc1f = (float*)acc;
        atomicAdd(&(acc1f[bin]),            a.x);
        atomicAdd(&(acc1f[Nchout+bin]),       a.w);
        atomicAdd(&(acc1f[2*Nchout+2*bin+0]), a.y);
        atomicAdd(&(acc1f[2*Nchout+2*bin+1]), a.z);
#endif

        return;

}

///////////////////////////////////////////////////////////////////////

__global__ void cu_accumulate_2pol_autoOnly(const float2* __restrict__ x, const float2* __restrict__ y,
                                   float2* __restrict__ acc, size_t Nchout, size_t Nfft)
{
        size_t nthreads = blockDim.x * gridDim.x;
        size_t idx = blockIdx.x * blockDim.x + threadIdx.x;
        size_t last_idx = Nchout * Nfft;
        size_t bin = idx % Nchout;

        float2 a = {0.0f, 0.0f};

        while (idx < last_idx) {

             float2 P1 = x[idx]; // {Re,Im} from "X-pol"
             float2 P2 = y[idx]; // {Re,Im} from "Y-pol"

             // TODO: use Kahan summation?
             // TODO: potentially bad performance when input data are not interleaved

             a.x +=  P1.x*P1.x + P1.y*P1.y; // .x = 1st <XX'>
             a.y +=  P2.x*P2.x + P2.y*P2.y; // .w = 1st <YY'>

             idx += nthreads;
        }

        //a /= (float)Nfft;

#if XMAC_OUTPUT_INTERLEAVED
        // Layout : [XX YY XX YY XX YY]
        atomicAdd(&(acc[bin].x), a.x);
        atomicAdd(&(acc[bin].y), a.y);
#else
        // Layout : [XX XX XX ... YY YY YY ... ]
        float* acc1f = (float*)acc;
        atomicAdd(&(acc1f[bin]),      a.x);
        atomicAdd(&(acc1f[Nchout+bin]), a.y);
#endif

        return;
}

__global__ void cu_accumulate_2pol_autoOnly_packed(const float4* __restrict__ xy, float2* __restrict__ acc, size_t Nchout, size_t Nfft)
{
        size_t nthreads = blockDim.x * gridDim.x;
        size_t idx = blockIdx.x * blockDim.x + threadIdx.x;
        size_t last_idx = Nchout * Nfft;
        size_t bin = idx % Nchout;

        float2 a = {0.0f, 0.0f};

        while (idx < last_idx) {

             float2 P1 = { xy[idx].x, xy[idx].y }; // {Re,Im} from "X-pol"
             float2 P2 = { xy[idx].z, xy[idx].w }; // {Re,Im} from "Y-pol"

             // TODO: use Kahan summation?

             a.x +=  P1.x*P1.x + P1.y*P1.y; // .x = 1st <XX'>
             a.y +=  P2.x*P2.x + P2.y*P2.y; // .w = 1st <YY'>

             idx += nthreads;
        }

        //a /= (float)Nfft;

#if XMAC_OUTPUT_INTERLEAVED
        // Layout : [XX Re{XY} Im{XY} YY]
        atomicAdd(&(acc[bin].x), a.x);
        atomicAdd(&(acc[bin].y), a.y);
#else
        // Layout : [XX XX XX ... YY YY YY ... ]
        float* acc1f = (float*)acc;
        atomicAdd(&(acc1f[bin]),      a.x);
        atomicAdd(&(acc1f[Nchout+bin]), a.y);
#endif

        return;

}

#endif // ARITHMETIC_XMAC_KERNELS_CU
