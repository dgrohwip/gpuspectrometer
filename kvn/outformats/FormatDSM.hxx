/////////////////////////////////////////////////////////////////////////////////////
//
// Definition of structures used by the KVN Digital Spectrometer (hardware).
// Follows specifications in 2007KVN-SPC0013_SpectralResultsFile_Spec_ver_110_E.pdf
//
// File format:
// bytes     0 to    83 = 84B long HEADER as in kvn_dsm_dataheader_t below
// bytes    84 to 16467 = 16kB of power spectrum of F1
// bytes 16468 to 32851 = 16kB of power spectrum of F2
// bytes 32852 to 65619 = 16kB Imag + 16 kB real of cross-power spectrum F1 x F2
// followed by any additional pairs (F3,F4,F4xF4, F5,F6,F5xF6, ...)
//
// Documentation errors:
// output_streams field : Section 5.1.1 says it is 2-byte 16-bit,
//                        Section 5.2   says it is 4-byte
//
/////////////////////////////////////////////////////////////////////////////////////

#ifndef FORMATDSM_HXX
#define FORMATDSM_HXX

#include <string>
#include <stdint.h>
#include <stddef.h>
#include <sys/time.h>

#define KVN_DSM_MAX_IP_PER_FILE 10000
#define KVN_DSM_NCHANNELS       4096

class SpectrometerConfig;

class FormatDSM {

#pragma pack(push)
#pragma pack(1)
    struct KVN_DSM_Header_t {
        uint32_t    sequence_number;     // Begin at 0, increment +1 in every Integration Period (IP), max IP is 10,000-1 (then new file)
        char        start_time[7];       // Time at acquistion start (not middle of interval); YYYYDDDhhmmss0 with lowest 4 bit of last byte always 0
        char        input_mode[1];       // 1: Narrow Band Mode, 2: Wide Band Mode

        //uint16_t    output_streams;      // 16-bit bitmask for "spectrum <x> is present", bit 0 = Auto f1, bit 1 = Auto f2, Bit 2,3 = Re,Im Cross f1x1f2; bits[8:15]==0 in wideband mode
        uint32_t    output_streams;    // TODO: documentation conflicts here, Section 5.2 says output streams is 32-bit 4-byte, while Section 5.1.1 says 16-bit 2-byte

        uint32_t    valid_samples[8];    // for Narrow Band Mode is num of valid input samples; low 4 bytes zero in Wide Band Mode
        uint32_t    invalid_samples[8];  //  -"- invalid input samples; -"-
        char        integration_length;  // 1: 10.24ms, 2: 51.2ms, 3:102.4ms, 4:512ms, 5:1024ms, others undefined
        char        output_mode;         // 1: Channel Select mode, 2: Channel Bind mode
        uint16_t    start_channel_index; // "head f-ch index of a spectrum result" (??), except if in Channel Bind mode, then this is 0x0000
    };

    union KVN_OutStreams_t {
        struct bit_tt {
            // Byte 0
            uint8_t auto_f1 : 1;
            uint8_t auto_f2 : 1;
            uint8_t cross_f1f2_re : 1;
            uint8_t cross_f1f2_im : 1;
            uint8_t auto_f3 : 1;
            uint8_t auto_f4 : 1;
            uint8_t cross_f3f4_re : 1;
            uint8_t cross_f3f4_im : 1;
            // Byte 1
            uint8_t auto_f5 : 1;
            uint8_t auto_f6 : 1;
            uint8_t cross_f5f6_re : 1;
            uint8_t cross_f5f6_im : 1;
            uint8_t auto_f7 : 1;
            uint8_t auto_f8 : 1;
            uint8_t cross_f7f8_re : 1;
            uint8_t cross_f7f8_im : 1;
        } bit;
        uint16_t word16;
        uint32_t word32;
    };
#pragma pack(pop) // restore previous pack() to avoid BOOST library crashes

    public:
        FormatDSM() { m_serialNum=0; m_ipNum=0; }

        /** Converts a 'struct timeval' into a string representation. The output format is specified in 'fmt'. */
        void timeval2YYYYMMDDhhmmss_fmt(char *dst, const size_t ndst, const struct timeval *tv, const char* fmt);

        /** Converts a 'struct timeval' into a string representation, with Day-of-Year. The output format is specified in 'fmt'. */
        void timeval2YYYYDDDhhmmss_fmt(char *dst, const size_t ndst, const struct timeval *tv, const char* fmt);

        /** Convert GPU spectrometer data into KVN DSM format. Allocates new 'out' buffer */
        int convertData(char** out, size_t& nbyte_out, const char* in, size_t nbyte_in, double w, struct timeval t, const SpectrometerConfig* cfg);

        /** Return current output file name */
        std::string getFilename() { return m_currFilename; }

        /** Return current integration period numer (m_ipNum) */
        uint32_t getIpNum() const { return m_ipNum; }

    private:
        uint32_t m_serialNum;       // starts at 0, increments once every 10,000 IP (KVN_DSM_MAX_IP_PER_FILE)
        uint32_t m_ipNum;           // current Integration Period number, increments "infinitely"
        std::string m_currFilename; // file name which follows naming "basename.<serial number>.SPC.dat"

        static float m_standard_DSM_Tints_s[5];
};

#endif // FORMATDSM_HXX
