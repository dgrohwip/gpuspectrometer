/**
 * \class ResultRecipient
 *
 * An interface invoked by a spectral processor after the completion of
 * a new set of spectra from a single baseband (1-pol, or 2-pol auto, or 2-pol auto and cross).
 *
 */

#ifndef RESULTRECIPIENT_HXX
#define RESULTRECIPIENT_HXX

#include <stdlib.h>
#include <sys/time.h>

class SpectrometerConfig;

class ResultRecipient {
    public:
        ResultRecipient() {}
        virtual void takeResult(void* autoAndCross, size_t nbytes, double w, struct timeval t, const SpectrometerConfig*) = 0;
};

/** Dummy no-op implementation of DataRecipient */
class ResultRecipientNoop : public ResultRecipient {
    public:
        ResultRecipientNoop() {}
        void takeResult(void* autoAndCross, size_t nbytes, double w, struct timeval t, const SpectrometerConfig*) { /* discard data */ }
};

#endif // RESULTRECIPIENT_HXX
