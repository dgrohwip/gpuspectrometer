#ifndef RAWDATABUFFERGROUP_HXX
#define RAWDATABUFFERGROUP_HXX

#include "rawdatabuffer.hxx"

#include <boost/thread/mutex.hpp>
#include <boost/noncopyable.hpp>

#include <stdlib.h>
#include <sys/time.h>
#include <string>

class RawDataBufferGroup : private boost::noncopyable {

    public:
        RawDataBufferGroup(int nbuffers, size_t nbytes, int nchan, int streamId, int verbosity=0);
        ~RawDataBufferGroup();

    public:
        void allocate();
        void deallocate();

    public:
        /** Find and reserve the first buffer slot with given state, starting from slot n. */
        int reserveSlot(const RawDataBuffer::BufState state, int n=0);

        /** Find and reserve the first buffer slot *not* in given state, starting from slot n. */
        int reserveSlotInv(const RawDataBuffer::BufState state, int n=0);

        /** Free up a buffer slot */
        void releaseSlot(int slot);

        /** Mark slot as full (or Overflowed, depending on earlier state) */
        void occupySlot(int slot);

        /** Set slot state */
        void setSlotState(const RawDataBuffer::BufState state, int slot);

        /** Get buffer slot contents */
        RawDataBuffer* getSlot(int slot) { return buffers[slot]; }

        /// Accessors
        int length() const { return nbuffers; }
        size_t capacity() const { return bufcapacity; } // pagesize-padded capacity in byte
        size_t utilized() const { return bufsize; }

    private:
        boost::mutex mutex;
        int verbosity;

        int nbuffers;
        size_t bufsize;
        size_t bufcapacity;
        int nchan;
        int streamId;

        RawDataBuffer** buffers;

};


#endif

