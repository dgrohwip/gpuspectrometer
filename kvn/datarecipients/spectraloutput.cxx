//
// Class SpectralOutput
//
// Implements the ResultRecipient::takeResult() interface function through which
// other classes (esp. BackendCPU) publish new spectral results. Data are accepted
// in a flat array, and a copy is made together with the active settings.
//
// The copy of results is dispatched to an asynchronous data outputter task.
//
// The outputter task reformats the flat array data into the final output format
// using a formatter class (FormatDSM, FormatDSMv2).
//
// Reformatted data are sent to a file, and/or all connected TCP clients.
//
// A simple TCP server/worker in this class accepts and keeps a list of
// connected clients.
//

#include "logger.h"
#include "datarecipients/spectraloutput.hxx"
#include "core/spectrometerconfig.hxx"
#include "outformats/FormatDSM.hxx"

#include <stdint.h>
#include <fstream>
#include <boost/asio.hpp>
#include <boost/asio/deadline_timer.hpp>

#define lockscope

using namespace boost::asio::ip;

//////////////////////////////////////////////////////////////////////////////////////////////////////

SpectralOutput::SpectralOutput(int tcp_port)
{
    m_port = tcp_port;
    m_fileOutputEnabled = true;
    m_clientpool.clear();
    cmthread = new boost::thread(&SpectralOutput::clients_maintainer, this, tcp_port);
}

SpectralOutput::~SpectralOutput()
{
    cmthread->interrupt();
    cmthread->join();
}

bool SpectralOutput::getFileOutputActive()
{
    boost::mutex::scoped_lock cplock(m_clientpoolmutex);
    std::vector<tcp::iostream*>::iterator it = m_clientpool.begin();
    if (it != m_clientpool.end()) {
        // Some TCP clients are connected. No file output.
        return false;
    } else {
        // No TCP clients. File output if enabled.
        return m_fileOutputEnabled;
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

void SpectralOutput::takeResult(void* autoAndCross, size_t nbytes, double w, struct timeval t, const SpectrometerConfig* cfg)
{
    // Copy the settings
    SpectrometerConfig* cfg_copy = new SpectrometerConfig;
    *cfg_copy = *cfg;

    // Copy the results
    char* autoAndCross_copy = new char[nbytes];
    if (autoAndCross != NULL) {
        std::copy((char*)autoAndCross, ((char*)autoAndCross)+nbytes, autoAndCross_copy);
    } else {
        L_(lwarning) << "SpectralOutput::takeResult() got NULL data pointer, using garbage as spectral data for " << cfg->scfg.obsPath << ", current/next IP# " << m_fmtDsm.getIpNum();
    }

    // Store results in the background
    boost::thread store(&SpectralOutput::results_storer, this, autoAndCross_copy, nbytes, w, t, cfg_copy);
    store.detach();
}

void SpectralOutput::results_storer(void* autoAndCross_copy, size_t nbytes, double w, struct timeval t, SpectrometerConfig* cfg)
{
    char* autoAndCross_reformatted = 0;
    size_t nout = 0;

    // Reformat the flat input data into DSMv2 with header and payload
    m_fmtDsm.convertData(&autoAndCross_reformatted, nout, (char*)autoAndCross_copy, nbytes, w, t, cfg);
    delete (char*)autoAndCross_copy; // note: final data are in autoAndCross_reformatted

    // Check if data can go to a file
    bool fileActive = getFileOutputActive();

    // Append into file
    if (fileActive) {

        std::ofstream of;
        std::string outfilename = m_fmtDsm.getFilename();
        of.open(outfilename.c_str(), std::ios_base::out|std::ios_base::binary|std::ios_base::app);
        if (of) {
            L_(linfo) << "Writing spectral file " << outfilename << ", current/next IP# is " << m_fmtDsm.getIpNum();
            of.write(autoAndCross_reformatted, nout);
            of.close();
        } else {
            L_(lerror) << "Failed to write to spectral file " << outfilename << " data of current/next IP# " << m_fmtDsm.getIpNum();
        }
    }

    // Tee into any TCP listeners
    lockscope {
        boost::mutex::scoped_lock cplock(m_clientpoolmutex);
        std::vector<tcp::iostream*>::iterator it = m_clientpool.begin();
        while (it != m_clientpool.end()) {
            tcp::iostream* s = (*it);
            // TODO: how detect a remote disconnect? boost write doesn't have timeout, nor boost::system::error_code return param
            if ( (s->write((const char*)autoAndCross_reformatted, nout)).fail() ) {
                L_(linfo) << "A spectrum client has disconnected";
                s->close();
                it = m_clientpool.erase(it); // erase current, return next
                delete s;
            } else {
                it++;
            }
        }
    }

    // Clean up
    delete cfg;
    free((char*)autoAndCross_reformatted);
}

void SpectralOutput::clients_maintainer(int port)
{
    boost::asio::io_service ios;
    tcp::endpoint endpoint(tcp::v4(), port);
    tcp::acceptor acceptor(ios, endpoint);
    acceptor.set_option(tcp::acceptor::reuse_address(true));

    try {
        while (1) {
            boost::this_thread::interruption_point();
            tcp::iostream* stream = new tcp::iostream();

            // Get a new client
            tcp::endpoint peer;
            boost::system::error_code ec;
            acceptor.accept(*(*stream).rdbuf(), peer, ec);
            if (ec) {
                L_(lerror) << "Could not accept spectrum clients on TCP port " << port << ", " << ec.message();
                delete stream;
                continue;
            } else {
                boost::mutex::scoped_lock cplock(m_clientpoolmutex);
                L_(linfo) << "New spectrum client from " << peer.address().to_string()
                          << " port " << peer.port();
                m_clientpool.push_back(stream);
            }
        }
    } catch (boost::thread_interrupted&) {
        return;
    }

}
