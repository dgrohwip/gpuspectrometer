#ifndef COMMANDRECIPIENT_HXX
#define COMMANDRECIPIENT_HXX

#include <string>

/** Interface definition for linking TCP receiver and any M&C class */
class CommandRecipient
{
     public:
         CommandRecipient() { }
         virtual std::string handleCommand(std::string cmd) = 0;
};

#endif // COMMANDRECIPIENT_HXX
