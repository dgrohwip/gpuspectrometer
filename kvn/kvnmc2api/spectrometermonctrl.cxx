/**
 * \class SpectrometerMonCtrl
 *
 * \brief Class to handle KVN commands. Also implements a command class factory.
 *
 * \author $Author: janw $
 *
 */

#include "logger.h"

#include "kvnmc2api/common.hxx"
#include "core/common.hxx"

#include <iomanip>
#include <iostream>
#include <set>
#include <sstream>
#include <string>

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/** C'stor, initialize list of commands (whether actually implemented or not depends on ./commands/cmdname.cxx) */
SpectrometerMonCtrl::SpectrometerMonCtrl()
{
    m_commandSeqNr = 0;
    m_commandName = CMD_INVALID_CMD;

    // Nominally supported commands
    m_mcCommands.insert("STRT"); // Start Observing   syntax: STRT  (immediate, no time specified)
    m_mcCommands.insert("STOP"); // Stop Observing    syntax: STOP  (immediate, no time specified)
    m_mcCommands.insert("PCMD"); // Procedure Command syntax: PCMD,dev,keyword
    m_mcCommands.insert("CONF"); // Configure Command syntax: CONF,cmdstr
    m_mcCommands.insert("PEND"); // Interrupt Macro   syntax: PEND
    m_mcCommands.insert("DVRC"); // Device Verbosity  syntax: DVRC,dev,ctrl
    m_mcCommands.insert("STTM"); // Set Time          syntax: STTM  (pull UTC time from a server)
    m_mcCommands.insert("OBNM"); // Observation Name  syntax: OBNM,<name> request; cmd=OBNM query; DONE,OBNM,<currName> response
    m_mcCommands.insert("ACQD"); // Spectral File     syntax: ACQD,<SAVE|NOSAVE>   (enables/disables writing of spectra to a file)
    m_mcCommands.insert("LGUI"); // Local GUI lock?   syntax: LGUI,lock
    // device name any from ('*','DSM','DFB','OTX','OTR','ADS_1','ADS_2','ADS_3','ADS_4') and possibly also ('mDSM', 'mPFB')
}

/** \brief Returns true if the specified command name should be supported based on specifications.
 */
bool SpectrometerMonCtrl::isNominallySupported(const std::string &command) const
{
    if (m_isCommand) {
        return (m_mcCommands.find(command) != m_mcCommands.end());
    } else {
        return (m_mcResponses.find(command) != m_mcResponses.end());
    }
}

/** \brief Returns true if the specified command was found in the registered command handlers.
 */
bool SpectrometerMonCtrl::isCommandImplemented(const std::string &command) const
{
    MCMsgCommandResponse *impl = MCMsgCommandResponseFactory::createObject(command);
    if (impl == NULL) {
        return false;
    }
    delete impl;
    return true;
}

void SpectrometerMonCtrl::parse(std::string& s)
{
    m_commandName = CMD_INVALID_CMD;
    m_commandSeqNr = (m_commandSeqNr + 1) % 10000;
    m_commandArgs.clear();

    // Types: "CMD,something?" (query) or "CMD,something;" (statement) or "CMD,something" (statement/query)
    m_commandIsQuery = (s.size() >= 2) && (s[s.size()-1] == '?');
    if (m_commandIsQuery) {
        s.erase(s.size() - 1);
    }
    if (s.size() > 1 && s[s.size()-1] == ';') {
        s.erase(s.size() - 1);
    }

    std::string token;
    std::istringstream ss(s);
    if (!ss) {
        return;
    }
    std::getline(ss, m_commandName, ',');
    while (std::getline(ss, token, ',')) {
        m_commandArgs.push_back(token);
    }
}

void SpectrometerMonCtrl::load(std::istream& cmd_ss)
{
    // Read complete 'istream' then load it
    std::stringstream ss;
    cmd_ss >> ss.rdbuf();
    std::string s = ss.str();
    this->load(s);
}

/** \brief Processes a command string.
 *
 */
void SpectrometerMonCtrl::load(std::string& cmd_s)
{
    this->parse(cmd_s);
}

/**
 * Dynamic handler of a new command. Locates the correct handler class for
 * the given command and executes it on the referenced spectrometer object.
 */
int SpectrometerMonCtrl::handle(const std::vector<std::string> args, std::vector<std::string>& out, SpectrometerInterface& spectrometer)
{
    int rc;
    out.clear();
    MCMsgCommandResponse *impl = MCMsgCommandResponseFactory::createObject(m_commandName);
    if (impl == NULL) {
        //std::cout << "MCMsgCommandResponse: did not find representative handler for command " << m_commandName << ".\n";
        out.push_back(CMD_RET_CMDERROR);
        rc = -1;
    } else {
        impl->setId(m_commandSeqNr);
        rc = impl->handle(args, out, spectrometer, m_commandIsQuery);
        delete impl;
    }
    return rc;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

std::string SpectrometerMonCtrl::dbg_ListAllCommands(bool do_color) const
{
    std::ostringstream ss;
    std::set<std::string>::iterator it;
    const std::string green("\033[32m"), red("\033[31m"), def("\033[39m");

    for (it = m_mcCommands.begin(); it != m_mcCommands.end(); it++) {
        bool implemented = isCommandImplemented(*it);
        if (!do_color) {
            ss << std::left << std::setw(6) << (*it) << (implemented ? " : implemented\n" : " : not yet implemented\n");
        } else {
            ss << std::left << std::setw(6) << (*it) << " : ";
            if (implemented) {
                ss << green << "implemented" << def << " \n";
            } else {
                ss << red << "not yet implemented" << def << " \n";
            }
        }
    }
    return ss.str();
}

