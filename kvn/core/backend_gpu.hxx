#ifndef BACKEND_GPU_HXX
#define BACKEND_GPU_HXX

#include "core/spectrometerconfig.hxx"      // class SpectrometerConfig
#include "core/backendinterface.hxx"        // iface class Backend
#include "datarecipients/rawdatabuffer.hxx" // class RawDataBuffer
#include "datarecipients/datarecipient.hxx" // interface DataRecipient::takeData(void* data, size_t nbytes, double w, struct timeval)
#include "datarecipients/resultrecipient.hxx" // interface ResultRecipient::takeResult(void* autoAndCross, size_t nbytes, double w, struct timeval t, SpectralSettings* fs)

#include <unistd.h>
#include <stdio.h>
#include <sys/time.h>

#include <boost/thread/mutex.hpp>
#include <boost/noncopyable.hpp>

class BackendGPU : public Backend {

    public:
        BackendGPU();
        ~BackendGPU();

    public:
        /**
         * Optimize some processing-related settings in the given configuration, so that
         * these suit the backend better. Determines for example the division of an integration
         * into sub-integrations that better fit the available backend memory (e.g. GPU memory)
         */
        bool optimizeConfiguration(SpectrometerConfig* cfg);

        /**
         * Returns an estimate for memory requirements on the CPU side and device side,
         * for the given configuration. Useful mainly for informative purposes.
         */
        bool estimateMemoryRequirement(const SpectrometerConfig* cfg, size_t* hostbytes, size_t* devicebytes) const;

        /** Return the number of available processing devices (GPU boards, or CPU cores, or other) */
        int getDeviceCount();

        /**
         * Initialize the backend for the provided spectral config.
         *
         * Makes an internal copy of the SpectrometerConfig, so that external
         * changes to the passed SpectrometerConfig have no effect after start().
         *
         * After initialization, data for processing can be pushed into
         * the backend via the takeData() function.
         */
        bool start(SpectrometerConfig* cfg);

        /** Reset the backend */
        bool reset();

        /** Shut down the backend */
        bool stop();

    public:

        /** Interface inherited from DataRecipient. For asynchronous passing of 2-bit data blocks into spectral processing. */
        void takeData(RawDataBuffer* data);

        /** Specify a ResultRecipient that will receive spectral results */
        void setOutput(ResultRecipient* r);

    public:

        /** Register user-provided data (2-bit) buffers in some device-dependend way that may yield better througput. */
        void registerUserDataBuffers(int Nbuffers, void** buffers, size_t buflen);

        /** Un-register the user-provided data (2-bit) buffers */
        void unregisterUserDataBuffers(int Nbuffers, void** buffers, size_t buflen);

    private:

        /**
         * Grab the next idle worker thread i.e. next available processing slot
         * \return Slot handle >=0 on success, -1 on error
         */
        int reserveIdleWorker();

        void showConfig(const SpectrometerConfig* cfg) const;

        bool allocateWorker(const SpectrometerConfig* fs, int slot);
        bool deallocateWorker(int slot);

        /** Non-blocking dispatch of new raw (1-channel or 2-channel) data into the spectral processing pipeline */
        int processDispatch(SpectrometerConfig*, const int gpu, const int stream /*TODO: input array*/);

        /** Blocking wait until procession of previously dispatched data (handle h) has completed */
        int processJoin(int h);

        void workerThread(void* slotcontext);
        int  processSlot(const int slothandle);
        int  makeSpectra(const int slothandle);

    private:

        bool m_init_done;

        void *pimpl; // pointer to cuda_config_t from backend_gpu_impl.hxx

        boost::mutex m_cuCtxMutex;
        size_t m_nGPU_busy_overflows, m_nGPU_completed;
};

#endif // BACKEND_GPU_HXX
