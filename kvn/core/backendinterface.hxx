#ifndef BACKEND_HXX
#define BACKEND_HXX

#include "core/spectrometerconfig.hxx"      // class SpectrometerConfig
#include "datarecipients/rawdatabuffer.hxx" // class RawDataBuffer
#include "datarecipients/datarecipient.hxx" // interface DataRecipient::takeData(void* data, size_t nbytes, double w, struct timeval)
#include "datarecipients/resultrecipient.hxx" // interface ResultRecipient::takeResult(void* autoAndCross, size_t nbytes, double w, struct timeval t, SpectralSettings* fs)

#include <unistd.h>
#include <stdio.h>
#include <sys/time.h>

#include <boost/thread/mutex.hpp>
#include <boost/noncopyable.hpp>

class Backend : public DataRecipient, private boost::noncopyable {

    public:
        Backend() { }
        ~Backend() { }

    public:
        /**
         * Optimize some processing-related settings in the given configuration, so that
         * these suit the backend better. Determines for example the division of an integration
         * into sub-integrations that better fit the available backend memory (e.g. GPU memory)
         */
        virtual bool optimizeConfiguration(SpectrometerConfig* cfg) = 0;

        /**
         * Returns an estimate for memory requirements on the CPU side and device side,
         * for the given configuration. Useful mainly for informative purposes.
         */
        virtual bool estimateMemoryRequirement(const SpectrometerConfig* cfg, size_t* hostbytes, size_t* devicebytes) const = 0;

        /** Return the number of available processing devices (GPU boards, or CPU cores, or other) */
        virtual int getDeviceCount() = 0;

        /**
         * Initialize the backend for the provided spectral config.
         *
         * Makes an internal copy of the SpectrometerConfig, so that external
         * changes to the passed SpectrometerConfig have no effect after start().
         *
         * After initialization, data for processing can be pushed into
         * the backend via the takeData() function.
         */
        virtual bool start(SpectrometerConfig* cfg) = 0;

        /** Reset the backend */
        virtual bool reset() = 0;

        /** Shut down the backend */
        virtual bool stop() = 0;

    public:

        /** Interface inherited from DataRecipient. For asynchronous passing of 2-bit data blocks into spectral processing. */
        virtual void takeData(RawDataBuffer* data) = 0;

        /** Specify a ResultRecipient that will receive spectral results */
        virtual void setOutput(ResultRecipient* r) = 0;

    public:

        /** Register user-provided data (2-bit) buffers in some device-dependend way that may yield better througput. */
        virtual void registerUserDataBuffers(int Nbuffers, void** buffers, size_t buflen) = 0;

        /** Un-register the user-provided data (2-bit) buffers */
        virtual void unregisterUserDataBuffers(int Nbuffers, void** buffers, size_t buflen) = 0;

};

#endif // BACKEND_HXX
