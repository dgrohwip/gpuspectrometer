
#include "core/defines.hxx"
#include "core/spectrometerconfig.hxx"

#include <stdint.h>
#include <cmath>
#include <cassert>

#include <iostream>
#include <iomanip>

static double gcdf(double a, double b)
{
    if (a < b) {
        return gcdf(b, a);
    }
    if (fabs(b) < 1e-9) {
        return a;
    } else {
        return (gcdf(b, a - floor(a / b) * b));
    }
}

static double lcmf(double a, double b)
{
    return (a * b) / gcdf(a, b);
}

double modfloat(double a, double b)
{
    double mod;
    if (a < 0) {
        mod = -a;
    } else {
        mod =  a;
    }
    if (b < 0) {
        b = -b;
    }
    while (mod >= b) {
        mod = mod - b;
    }
    if (a < 0) {
        return -mod;
    }
    return mod;
}

static bool equal_enough(double a, double b)
{
    return (fabs(a-b) < 1e-7);
}

/** Calculate processing settings based on spectral settings and VDIF details */
void SpectrometerConfig::recalculate()
{
    // Calculate VDIF channel bandwidth, frame granularity
    vdifinfo.recalculate();

    // Adjust integration time; try to avoid rounding errors
    pcfg.dfttime_s = (2*scfg.nchan) / (2*vdifinfo.bw); // TODO: overlapped DFTs?
    double lcm_s = lcmf(pcfg.dfttime_s, vdifinfo.frametime_s);
    size_t nlcm = round(scfg.Tint_wish_s / lcm_s);
    size_t nframes = nlcm * round(lcm_s / vdifinfo.frametime_s);
    pcfg.Tint_s = vdifinfo.frametime_s * nframes;

    //std::cout << "Twish=" << std::fixed << std::setprecision(9) << scfg.Tint_wish_s
    //    << " Tdft=" << pcfg.dfttime_s
    //    << " Tframe=" << vdifinfo.frametime_s << " LCM(Tdft,Tframe)=" << lcm_s << "\n";
    //std::cout << " Tdft'=" << nlcm << " x " << lcm_s << " lcm = " << lcm_s*nlcm << "\n"; 
    //std::cout << " Tdft'=" << nframes << " x " << vdifinfo.frametime_s << " Tframe = " << vdifinfo.frametime_s*nframes << "\n";

    // Buffer sizes
    pcfg.rawbufsize = vdifinfo.payloadsize * nframes;

    // Disable non-existing bands
    for (int n=vdifinfo.nbands; n<MAX_SUBBANDS; n++) {
        scfg.enableAutocorr[n] = false;
        scfg.enableCrosscorr[n/2] = false;
    }

    // Determine number of r2c FFTs to accumulate in one integration period per recorded band
    if (scfg.nchan > 0 && vdifinfo.nbits > 0 && vdifinfo.nbands > 0) {
        pcfg.nfft = (8*pcfg.rawbufsize) / (vdifinfo.nbits * vdifinfo.nbands);
        pcfg.nfft /= (2*scfg.nchan);
        int nfft_direct = pcfg.Tint_s / pcfg.dfttime_s;
        if (nfft_direct != pcfg.nfft) {
            // std::cerr << "Error: bufsize nfft=" << pcfg.nfft << " != tint/dft=" << nfft_direct << "\n";
            // TODO: could use calculateIntegrationTimes.cxx type of "testing" of a series of typical integration
            // times to see if any of them fit, rather than landing at obscure e.g. 256.02 millisec or other,
            // or failing the fit altogether...
            pcfg.nfft = 0;
        }
        //assert (nfft_direct == pcfg.nfft);
    } else {
        pcfg.nfft = 0;
    }

    // Update hosted objects
    scfg.recalculate();
    pcfg.recalculate();
}

std::ostream& operator<<(std::ostream& os, const SpectrometerConfig& c)
{
    os << c.scfg;
    os << c.pcfg;
    os << "Input data: VDIF " << c.vdifinfo.nbands << " x " << std::fixed << std::setprecision(1) << (c.vdifinfo.bw*1e-6) << ", MHz "
        << c.vdifinfo.payloadsize << "B payload x " << c.vdifinfo.framespersec << " frames per second\n";
    if (c.scfg.Tint_wish_s != c.pcfg.Tint_s) {
        os << "Config: VDIF and DFT sizes constrained Tint to " << std::fixed << std::setprecision(9) << c.pcfg.Tint_s << "s instead of requested " << c.scfg.Tint_wish_s << "s\n";
    }
    return os;
}
