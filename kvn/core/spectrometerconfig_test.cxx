
#include "core/spectrometerconfig.hxx"

#include <iostream>
#include <cassert>
#include <stdlib.h>
#include <cmath>

using namespace std;

int main(int argc, char** argv)
{
    int tcase = 0; // 0, 1, add others
    if (argc > 1) {
        tcase = atoi(argv[1]);
    }

    SpectrometerConfig cfg;
    cfg.vdifinfo.nbits = 2;
    switch(tcase) {
      case 0:
        // 2 Gbps stream, 8 x 64 MHz x 2-bit
        cfg.vdifinfo.framesize = 8032;
        cfg.vdifinfo.payloadsize = 8000;
        cfg.vdifinfo.framespersec = 32000;
        cfg.vdifinfo.nbands = 8;
        // PI settings
        cfg.scfg.Tint_wish_s = 0.512;
        cfg.scfg.nchan = 8192;
        break;
      case 1:
        // ? Gbps stream, 16 x 32 MHz x 2-bit
        cfg.vdifinfo.framesize = 8224;
        cfg.vdifinfo.payloadsize = 8192;
        cfg.vdifinfo.framespersec = 31250;
        cfg.vdifinfo.nbands = 16;
        // PI settings
        cfg.scfg.Tint_wish_s = 0.250;
        cfg.scfg.nchan = 32768;
        break;
      case 2:
      default:
        // 512 Mbps, 4 x 32 MHz x 2-bit, 8192B, 7812 fps
        // Ought to be 7812.5 fps for the 8129B payload to reach 512M
        // Thus with cropping by half a frame, bandwidth est. as <32MHz per channel
        cfg.vdifinfo.framesize = 8224;
        cfg.vdifinfo.payloadsize = 8192;
        cfg.vdifinfo.framespersec = 7812;
        cfg.vdifinfo.nbands = 4;
        // PI settings
        cfg.scfg.Tint_wish_s = 0.250;
        cfg.scfg.nchan = 1024;
        break;
    }
    if (argc > 2) {
        cfg.scfg.Tint_wish_s = atof(argv[2]);
    }

    // Enable all bands for spectra
    for (int n=0; n<cfg.vdifinfo.nbands; n++) {
        cfg.scfg.enableAutocorr[n] = true;
    }

    // Derive processing settings from VDIF and PI settings
    cfg.recalculate();

    cout << "VDIF " << cfg.vdifinfo.nbands << " channels x " << std::fixed << cfg.vdifinfo.bw*1e-6 << " MHz for rate " << (cfg.vdifinfo.payloadsize*cfg.vdifinfo.framespersec*8*1e-6) << " Mbit/s\n";
    cout << "For VDIF rate " << cfg.vdifinfo.framespersec << " per sec the frame duration is " << cfg.vdifinfo.frametime_s*1e6 << " usec\n";
    cout << "For DFT size 2*" << cfg.scfg.nchan << "-pt the duration is " << std::fixed << cfg.pcfg.dfttime_s*1e6 << " usec\n";
    cout << "Requested Tint of " << cfg.scfg.Tint_wish_s << ", achievable " << cfg.pcfg.Tint_s << " needs " << cfg.pcfg.rawbufsize << " byte raw buffer per Tint\n";
    cout << "Integration has " << cfg.pcfg.nfft << " x 2*" << cfg.scfg.nchan << "-pt DFTs, " << cfg.pcfg.nfft*cfg.pcfg.dfttime_s << " sec\n";

    assert(cfg.vdifinfo.bw > 0 && cfg.vdifinfo.bw < 4096e6);
    assert(cfg.pcfg.Tint_s > 0);
    assert(cfg.pcfg.nfft > 0);
    assert(fabs(cfg.pcfg.nfft*cfg.pcfg.dfttime_s - cfg.pcfg.Tint_s) < 1e-6);

    // Make a copy
    SpectrometerConfig cfg2 = cfg;
    assert(cfg2.pcfg.Tint_s == cfg.pcfg.Tint_s);
    assert(cfg2.scfg.nchan == cfg.scfg.nchan);
    assert(cfg2.vdifinfo.bw == cfg.vdifinfo.bw);

    // Try to-string
    cout << "\nTo-string method outputs:\n";
    cout << cfg2 << "\n";

    cout << "Done. No assert failures.\n";

	cout << (int)cfg.vdifinfo.bw << "\n";

    return 0;
}
