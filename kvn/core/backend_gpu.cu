
#include "logger.h"
#include "core/defines.hxx"
#include "core/common.hxx"
#include "core/backend_gpu.hxx"
#include "core/backend_gpu_impl.hxx"
#include "datarecipients/datarecipient.hxx"
#include "datarecipients/resultrecipient.hxx"

#include <assert.h>
#include <getopt.h>
#include <malloc.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <cuda.h>
#include <cufft.h>
#include <cufftXt.h>
#include <cuda_profiler_api.h>
#include <nvToolsExt.h>

#include "decoder_2b32f_kernels.cu"
#include "arithmetic_xmac_kernels.cu"
#include "arithmetic_autospec_kernels.cu"
#include "arithmetic_winfunc_kernels.cu"
#include "histogram_kernels.cu"

//#define DEBUG  // comment out to disable debug printouts
#undef CHECK_TIMING
#ifndef DEBUG
    #define CHECK_TIMING 0
    #define PROFILING    0
#else
    #define CHECK_TIMING 1
    #define PROFILING    1
#endif
#include "cuda_utils.cu" // defines macros, depending on CHECK_TIMING def
#ifndef DEBUG
    // Disable some performance killers if not in debug mode
    #undef  CUDA_CHECK_ERRORS
    #undef  CUDA_TIMING_START
    #undef  CUDA_TIMING_STOP
    #define CUDA_CHECK_ERRORS(x)
    #define CUDA_TIMING_START(x,y)
    #define CUDA_TIMING_STOP(x,y,a,b,c)
#endif

#define ORDER_STREAM_FIRST       // define to distribute tasks stream-first, undefine to distribute in GPU-first order
#define REPORT_SEGMENT_RESULTS 1 // 1 to report each completed segment

/////////////////////////////////////////////////////////////////////////////////////

static size_t m_gcd(size_t a, size_t b)
{
  size_t t;
  while ( a != 0 ) {
     t = a;
     a = b % a;
     b = t;
  }
  return b;
}

static void inspect_host_buffer(unsigned char* h_buf, size_t nmax, const char *msg)
{
    printf("For %s inspect_dev_buffer(%p) : ", msg, h_buf);
    for (size_t n=0; n<nmax; n++) {
        printf("%02x ", h_buf[n]);
    }
    printf("\n");
}

static void inspect_dev_buffer(void* d_buf, size_t nmax, const char *msg)
{
    unsigned char* h_buf;
    CUDA_CALL( cudaMallocHost((void **)&(h_buf), nmax, cudaHostAllocDefault) );
    CUDA_CALL( cudaMemcpy(h_buf, d_buf, nmax, cudaMemcpyDeviceToHost) );
    inspect_host_buffer(h_buf, nmax, msg);
    CUDA_CALL( cudaFreeHost(h_buf) );
}

/////////////////////////////////////////////////////////////////////////////////////

BackendGPU::BackendGPU()
{
    m_init_done = false;
    pimpl = new cuda_config_pool_t;
    m_nGPU_busy_overflows = 0;
    m_nGPU_completed = 0;
}

BackendGPU::~BackendGPU()
{
    if (pimpl != NULL) {
        stop();
        delete (cuda_config_pool_t*)pimpl;
        pimpl = NULL;
    }
}

/////////////////////////////////////////////////////////////////////////////////////

bool BackendGPU::optimizeConfiguration(SpectrometerConfig* cfg)
{
    assert(cfg != NULL);

    // Disable windowing if "wrong" window function (e.g., boxcar, or not yet implemented HFT248D
    if (cfg->scfg.do_window && (cfg->scfg.window_type == WINDOW_FUNCTION_NONE || cfg->scfg.window_type >=WINDOW_FUNCTION_HFT248D)) {
        cfg->scfg.do_window = 0;
        fprintf(stderr, "Warning: disabling window function since 'boxcar' or not yet implemented.\n");
    }

    // Determine amount of memory present
    size_t min_free = (size_t)(-1);
    for (int g = 0; g < cfg->pcfg.nGPUs; g++) {
        boost::mutex::scoped_lock sharedGPUlock(m_cuCtxMutex);
        size_t nfree = 0, ntotal = 0;
        CUDA_CALL( cudaSetDevice(cfg->pcfg.mapGPUs[g]) );
        CUDA_CALL( cudaMemGetInfo(&nfree, &ntotal) );
        min_free = (nfree < min_free) ? nfree : min_free;
    }

    // Reduce free memory that backend tries to grab; in case of shared use by other GPU-accessing processes
    if (cfg->pcfg.underallocfactor > 1) {
        min_free /= cfg->pcfg.underallocfactor;
    }

    // Estimate how much memory might be needed, and divide in more sub-integrations to reduce memory footprint
    // NOTE: currently integration of one AP always on one GPU, no sharing of AP sub-integrations across GPU
    size_t hostmem, devicemem;
    cfg->pcfg.nsubints = 1; // starting point of iterating is 1 subintegration
    while (true) {

        // Get estimate based in current value of 'nsubints'
        if (!estimateMemoryRequirement(cfg, &hostmem, &devicemem)) {
            break;
        }

        L_(linfo) << "optimizing step; with " << cfg->pcfg.nsubints << " sub-ints x " << cfg->pcfg.nfft_subint << " DFT "
            << std::fixed << std::setprecision(6) << (cfg->pcfg.Tint_s/cfg->pcfg.nsubints)*1e3 << " ms "
            << "need " << std::fixed << std::setprecision(2) << (devicemem/1048576.0) << "GB per GPU "
            << "(min. free " << (min_free/1048576.0) << "GB) "
            << (hostmem/1048576.0) << "GB on host";

        if (devicemem < min_free || cfg->pcfg.nsubints > cfg->pcfg.nfft) {
            break;
        }

        // Increase number of subintegrations 'nsubints' by some prime factor
        const size_t primes = 2*3*5*7*11*13;
        size_t nsubffts = cfg->pcfg.nfft / cfg->pcfg.nsubints;
        size_t R1 = m_gcd(nsubffts, primes);
        size_t R2 = m_gcd(cfg->pcfg.rawbufsize, primes);
        size_t R = m_gcd(R1, R2);
        //L_(linfo) << "optimizing step; R1=" << R1 << " R2=" << R2 << " R=" << R;
        if ((nsubffts % R) != 0 || (cfg->pcfg.rawbufsize % (R*cfg->pcfg.nsubints)) != 0 || (R <= 1)) {
           //fprintf(stderr, "Warning: failed to split into more than %d sub-ints. May run into GPU memory limts.\n", cfg->pcfg.nsubints);
           // Integer split not possible, allow fractional split, such that tailing part of data produces less than 'nfft' FFTs
           R = 2;
        }

        // Next try with:
        cfg->pcfg.nsubints *= R;
        cfg->pcfg.nfft_subint = cfg->pcfg.nfft / cfg->pcfg.nsubints;
    }

    // Final
    cfg->pcfg.nfft_subint = cfg->pcfg.nfft / cfg->pcfg.nsubints;

    // Report final
    estimateMemoryRequirement(cfg, &hostmem, &devicemem);
    L_(linfo) << "final optimized config has " << cfg->pcfg.nsubints << " sub-ints, "
        << "need " << std::fixed << std::setprecision(2) << (devicemem/1048576.0) << "GB per GPU "
        << "(min. free " << (min_free/1048576.0) << "GB) "
        << (hostmem/1048576.0) << "GB on host";

    return true;
}

bool BackendGPU::estimateMemoryRequirement(const SpectrometerConfig* cfg, size_t *hostbytes, size_t *devicebytes) const
{
    size_t mem_cpu = 0, mem_gpu = 0;

    assert(cfg != NULL);

    // Guesstimate host side RAM for one averaging period from all buffers
    mem_cpu += cfg->pcfg.rawbufsize;
    mem_cpu += cfg->scfg.nchan * 2*sizeof(float) * cfg->scfg.nspectra;
    mem_cpu *= cfg->pcfg.nbuffers;

    // Guesstimate the size of a batched FFT plan
    size_t cufft_worksize;
    assert(cfg->pcfg.nfft_subint == (cfg->pcfg.nfft / cfg->pcfg.nsubints));
    CUFFT_CALL( cufftEstimate1d(2*cfg->scfg.nchan, CUFFT_R2C, cfg->pcfg.nfft_subint, &cufft_worksize) );
    mem_gpu += cufft_worksize;

    // Guesstimate the rest of GPU memory needed : on one(!) GPU x streams
    const size_t sampPerByte = 4;
    const size_t samples = (sampPerByte * cfg->pcfg.rawbufsize) / cfg->pcfg.nsubints;
    const size_t samples_selected = 2*cfg->scfg.nchan * cfg->pcfg.nfft_subint * cfg->scfg.nenabledbands;
    mem_gpu += cfg->pcfg.rawbufsize;
    mem_gpu += samples * sizeof(float);             // unpacked float time-domain -- all VDIF bands
    mem_gpu += samples_selected * 2*sizeof(float);  // r2c complex out, ignoring Nyquist bin, no redundant bins -- selected VDIF bands only
    mem_gpu += cfg->scfg.nchan * cfg->scfg.nspectra * 3*sizeof(float);  // accumulated; assuming all are ~3 float/bin average (1 float auto, 4 float cross)
    mem_gpu *= cfg->pcfg.nstreams;

    if (hostbytes != NULL) { *hostbytes = mem_cpu; }
    if (devicebytes != NULL) { *devicebytes = mem_gpu; }

    return true;
}

int BackendGPU::getDeviceCount()
{
    int devicecount;
    CUDA_CALL( cudaGetDeviceCount(&devicecount) );
    return devicecount;
}

/////////////////////////////////////////////////////////////////////////////////////

bool BackendGPU::start(SpectrometerConfig* cfg)
{
    assert(cfg != NULL);
    assert(this->pimpl != NULL);

    cuda_config_pool_t* cp = (cuda_config_pool_t*)this->pimpl;
    boost::mutex::scoped_lock poollock(cp->mutex);

    if (m_init_done) {
        // TODO: de-init first then re-init?
        return false;
    }

    // Prepare workers and map to (stream,GPU) or (GPU,stream) tuple
    cp->nworkers = cfg->pcfg.nGPUs * cfg->pcfg.nstreams;
    assert(cp->nworkers <= CUDA_CFG_POOL_MAXWORKERS);
    int w = 0;
#ifdef ORDER_STREAM_FIRST
    // workers [0..N] by stream-first order
    for (int g = 0; g < cfg->pcfg.nGPUs; g++) {
        for (int s = 0; s < cfg->pcfg.nstreams; s++, w++) {
#else
    // workers [0..N] by GPU-first order
    for (int s = 0; s < cfg->pcfg.nstreams; s++) {
        for (int g = 0; g < cfg->pcfg.nGPUs; g++, w++) {
#endif
            cp->pool[w].g = cfg->pcfg.mapGPUs[g];
            cp->pool[w].s = s;
            cp->pool[w].n = w;
            cp->pool[w].state = cuda_worker_t::Idle;
            cp->pool[w].cfg = *cfg; // make a copy of settings so thread-settings cannot be altered later
        }
    }

#if PROFILING
    {
        boost::mutex::scoped_lock sharedGPUlock(m_cuCtxMutex);
        CUDA_CALL( cudaProfilerStart() );
    }
#endif

    // Launch each worker
    for (int n=0; n < cp->nworkers; n++) {
        void* slotcontext = (void*)&(cp->pool[n]);
        cp->pool[n].recipient = cp->common_recipient;
        cp->pool[n].workerthread = new boost::thread(&BackendGPU::workerThread, this, slotcontext);
    }

    // Reset the statistics
    m_nGPU_busy_overflows = 0;
    m_nGPU_completed = 0;
    m_init_done = true;

    // TODO: some barrier to wait until workers have their allocations done? that could block for a while though...
    L_(linfo) << "Backend started. Worker threads still initializing in background";

    return true;
}

bool BackendGPU::reset()
{
    if (m_init_done) {
        stop();
    }

    /* Stop profiling */
#if PROFILING
    {
        boost::mutex::scoped_lock sharedGPUlock(m_cuCtxMutex);
        CUDA_CALL( cudaProfilerStop() );
    }
#endif

    /* Reset all GPUs (TODO: just those in a given config? rather than all in system?) */
#if 0
    int ngpu;
    CUDA_CALL( cudaGetDeviceCount(&ngpu) );
    for (int g = 0; g < ngpu; g++) {
        boost::mutex::scoped_lock sharedGPUlock(m_cuCtxMutex);
        CUDA_CALL( cudaSetDevice(g) );
        CUDA_CALL( cudaDeviceReset() );
    }
#endif

    return true;
}

bool BackendGPU::stop()
{
    if (!m_init_done) {
        L_(linfo) << "Backend was already stopped, current stop() ineffective.";
        return false;
    }
    assert(this->pimpl != NULL);

    cuda_config_pool_t* cp = (cuda_config_pool_t*)this->pimpl;
    boost::mutex::scoped_lock poollock(cp->mutex);

    // Interrupt the worker threads
    for (int n=0; n < cp->nworkers; n++) {
        cp->pool[n].workerthread->interrupt();
        poollock.unlock();
        cp->pool[n].newdatacond.notify_one(); // wake up if waiting for data
        poollock.lock();
    }

    // Deallocate
    for (int n=0; n < cp->nworkers; n++) {
        cp->pool[n].workerthread->join();
        delete cp->pool[n].workerthread;
    }
    for (int n=0; n < cp->nworkers; n++) {
        deallocateWorker(n);
    }
    m_init_done = false;

    L_(linfo) << "Backend stopped.";
    L_(linfo) << "Backend completed " << m_nGPU_completed << " integrations, lost " << m_nGPU_busy_overflows << " integrations due to busy GPUs";

    return true;
}

/////////////////////////////////////////////////////////////////////////////////////

void BackendGPU::takeData(RawDataBuffer* raw)
{
    if (!m_init_done) {
        L_(lwarning) << "GPU::takeData() invoked but BackendGPU not initialized yet, discarding data";
        return;
    }

#if PROFILING
    nvtxRangeId_t nvRid = nvtxRangeStartA("takeData");
#endif

    // Find and reserve an idle slot (if any)
    int h = reserveIdleWorker();
    if (h < 0) {
        double f = m_nGPU_busy_overflows / ((double)(m_nGPU_busy_overflows + m_nGPU_completed));
        L_(lwarning) << "GPU::takeData() : all GPU slots busy, discarding data, "
                     << m_nGPU_busy_overflows << " discarded so far ("
                     << std::fixed << std::setprecision(1) << 100.0*f << "%)";
        m_nGPU_busy_overflows++;
        return;
    }

    // Prepare copying of input data into our reserved slot
    cuda_config_pool_t* cp = (cuda_config_pool_t*)(this->pimpl);
    cuda_worker_t& cs = cp->pool[h];
    assert(raw->length == cs.rawbuflen);
    lockscope {
        boost::mutex::scoped_lock lock(cs.statemutex);
        cs.rawbuf_midtime = raw->tmid;
        cs.spec_weight = raw->weight;
        cs.h_rawbuf = (unsigned char*)(raw->data);
        cs.state = cuda_worker_t::Reserved; // should've already happened in reserveIdleWorker()
    }

    //inspect_host_buffer((unsigned char*)raw->data, 16, "cpu side new VDIF buf");

    // Initiate copying
    cs.newdatacond.notify_one();
    L_(ldebug2) << "GPU::takeData() start copying to GPU " << (cs.g) << " in stream " << (cs.s);

    // Wait for GPU worker to notify us once copy op completed; state transitions Idle->Reserved->InputCopied->...->Idle
    // TODO: could make ALMA ASM-like mechanism here where the worker thread actually calls back m_udpRx with ::releaseSlot(),
    //      that way there can be multiple concurrent transfers host->GPU!
    lockscope {
        boost::mutex::scoped_lock lock(cs.statemutex);
        while (cs.state == cuda_worker_t::Reserved) {
            cs.newdataonGPUcond.wait(lock);
        }
        m_nGPU_completed++;
    }

#if PROFILING
    nvtxRangeEnd(nvRid);
#endif

    L_(ldebug2) << "GPU::takeData() finished copying to GPU " << (cs.g) << " in stream " << (cs.s);
}

/////////////////////////////////////////////////////////////////////////////////////

void BackendGPU::setOutput(ResultRecipient* r)
{
    cuda_config_pool_t* cp = (cuda_config_pool_t*)this->pimpl;
    boost::mutex::scoped_lock poollock(cp->mutex);

    cp->common_recipient = r;
    for (int n=0; n < cp->nworkers; n++) {
        boost::mutex::scoped_lock workerlock(cp->pool[n].recipientmutex);
        cp->pool[n].recipient = r;
    }
}

/////////////////////////////////////////////////////////////////////////////////////

void BackendGPU::registerUserDataBuffers(int Nbuffers, void** buffers, size_t buflen)
{
    boost::mutex::scoped_lock sharedGPUlock(m_cuCtxMutex);
    if (buffers == NULL) return;
    for (int n =0; n < Nbuffers; n++) {
        if (buffers[n] != NULL) {
            //size_t buflen_ceil = buflen - (buflen % 4096) + 4096;
            CUDA_CALL( cudaHostRegister(buffers[n], buflen, cudaHostRegisterPortable) );
        }
    }
}

void BackendGPU::unregisterUserDataBuffers(int Nbuffers, void** buffers, size_t buflen)
{
    boost::mutex::scoped_lock sharedGPUlock(m_cuCtxMutex);
    if (buffers == NULL) return;
    for (int n =0; n < Nbuffers; n++) {
        if (buffers[n] != NULL) {
            CUDA_CALL( cudaHostUnregister(buffers[n]) );
        }
    }
}


/////////////////////////////////////////////////////////////////////////////////////
// Private member functions
/////////////////////////////////////////////////////////////////////////////////////

/**
 * Reserve a processing slot and return a handle to it
 * \return Slot handle >=0 on success, -1 on error
 */
int BackendGPU::reserveIdleWorker()
{
    assert(this->pimpl != NULL);
    cuda_config_pool_t* cp = (cuda_config_pool_t*)this->pimpl;
    static int n0 = 0;

retry:
    int nuninitialized = 0, nbusy = 0;
    lockscope {
        boost::mutex::scoped_lock poollock(cp->mutex);
        for (int n = 0; n < cp->nworkers; n++) {
            int s = (n + n0) % cp->nworkers;
            boost::mutex::scoped_lock lock(cp->pool[s].statemutex);
            if (!cp->pool[s].initialized) {
                nuninitialized++;
                continue;
            }
            if (cp->pool[s].state == cuda_worker_t::Idle) {
                cp->pool[s].state = cuda_worker_t::Reserved;
                n0 = s + 1; // for round-robin assignment
                //L_(linfo) << "reserved slot " << s << ", nbusy=" << nbusy;
                return s;
            } else {
                nbusy++;
            }
        }
    }
    if (nuninitialized > 0) {
        boost::this_thread::yield();
        goto retry;
    }
    return -1;
}

/////////////////////////////////////////////////////////////////////////////////////

/**
 * Worker thread. Note: since CUDA 4.0(?) one process and all CPU threads of it have
 * a shared device context. That is CPU thread #1 if it switches to GPU#1 can interfere
 * with another CPU thread #2 that is expecting to work on GPU#2 (but gets switched
 * to GPU#1 unknowingly due to thread #1, and all objects and events are invalid).
 * It seems like the only option is to have a "global lock", under which
 * to select the taret GPU and then dispatch any asynchronous tasks, then
 * release the lock (allowing other CPU threads to queue tasks onto the same
 * or another GPU), then acquire the lock periodically to check for task completion.
 */
void BackendGPU::workerThread(void* slotcontext)
{
    struct timeval tv_cpustart, tv_cpustop;

    /* Context */
    cuda_worker_t* cs = (cuda_worker_t*)slotcontext;
    if (cs == NULL) {
        return;
    }
    const SpectrometerConfig* cfg = &cs->cfg;

    /* Prepare our arrays (m_cuCtxMutex is acuired inside the call until it returns) */
    allocateWorker(cfg, cs->n);

    while (1) {
        std::stringstream ssname;
        ssname << "GPU Worker[" << (cs->g) << "," << (cs->s) << "]";
        std::string sname = ssname.str();

        /* Wait for new data (located on CPU-side first; cs->h_rawbuf pointer is changed by CPU) */
        try {
            boost::this_thread::interruption_point();
            boost::mutex::scoped_lock lock(cs->statemutex);
            while (cs->state != cuda_worker_t::Reserved) {
                boost::this_thread::interruption_point();
                cs->newdatacond.wait(lock);
            }
            L_(ldebug2) << sname << ": woke up";
        } catch (boost::thread_interrupted& e) {
            L_(ldebug) << __func__ << " was interrupted and is exiting";
            break;
        }

#if PROFILING
        nvtxRangeId_t nvRid = nvtxRangeStartA("handleData");
#endif

        /* Copy the data and reset spectral accumulators in the meantime */
        gettimeofday(&tv_cpustart, NULL);
        lockscope {
            boost::mutex::scoped_lock sharedGPUlock(m_cuCtxMutex);
            CUDA_CALL( cudaSetDevice(cs->g) );
            // Start Time of entire processing chain
            CUDA_CALL( cudaEventRecord(cs->evt_start, cs->sid) );
            // Clear old accumulators
            CUDA_CALL( cudaMemsetAsync(cs->d_powspecs, 0x00, cs->powspecslen, cs->sid) );
            // Initiate async copying (overlap with other kernels in other threads)
            CUDA_CALL( cudaMemcpyAsync(cs->d_rawbuf, cs->h_rawbuf, cs->rawbuflen, cudaMemcpyHostToDevice, cs->h2d) );

            // Sync up with transfer completion
            CUDA_CALL( cudaEventRecord(cs->evt_sync_with_yield, cs->h2d) );
#if 0
            sharedGPUlock.unlock();
            boost::this_thread::yield();
            sharedGPUlock.lock();
            CUDA_CALL( cudaSetDevice(cs->g) );
#endif
            CUDA_CALL( cudaEventSynchronize(cs->evt_sync_with_yield) ); // not busy-wait, yields, but not sure of fate of sharedGPUlock!
        }

        /* Notify CPU-side that the CPU buffer can be released/re-used */
        lockscope {
            boost::mutex::scoped_lock lock(cs->statemutex);
            cs->state = cuda_worker_t::InputCopied;
        }
        cs->newdataonGPUcond.notify_one();

        /* Process new data */
        processSlot(cs->n);

        /* Finish up on the processing */
        float dT_msec_gpu = -1.0f;
        lockscope {
            boost::mutex::scoped_lock sharedGPUlock(m_cuCtxMutex);
            CUDA_CALL( cudaSetDevice(cs->g) );
            CUDA_CALL( cudaEventRecord(cs->evt_sync_with_yield, cs->sid) );
            CUDA_CALL( cudaEventRecord(cs->evt_stop, cs->sid) );
            CUDA_CALL( cudaEventSynchronize(cs->evt_sync_with_yield) ); // first do non-busy-wait, yields
            CUDA_CALL( cudaEventSynchronize(cs->evt_stop) );            // now do a busy-wait
            CUDA_CALL( cudaEventElapsedTime(&dT_msec_gpu, cs->evt_start, cs->evt_stop) );
        }

#if PROFILING
        nvtxRangeEnd(nvRid);
#endif

        /* Store the spectral results (in background) */
        lockscope {
            boost::mutex::scoped_lock poollock(cs->recipientmutex);
            if (cs->recipient != NULL) {
                cs->recipient->takeResult(cs->h_powspecs, cs->powspecslen, cs->spec_weight, cs->rawbuf_midtime, cfg);
            }
        }

        /* Done */
        gettimeofday(&tv_cpustop, NULL);
        float dT_msec_cpu = 1e3* ((tv_cpustop.tv_sec - tv_cpustart.tv_sec) + 1e-6*(tv_cpustop.tv_usec - tv_cpustart.tv_usec));
        lockscope {
            boost::mutex::scoped_lock lock(cs->statemutex);
            cs->state = cuda_worker_t::Idle;   // or Done
        }
        cs->newdataonGPUcond.notify_one(); // notify again in case waiter missed it earlier
        float Msps = 1e-6 * ((cs->rawbuflen*8.0)/(cfg->vdifinfo.nbits)) / (dT_msec_gpu*1e-3);
        Msps /= cfg->vdifinfo.nbands;
        L_(ldebug2) << sname << ": done, took " << std::fixed << std::setprecision(2)
                    << dT_msec_gpu << " msec on GPU, "
                    << dT_msec_cpu << " msec on host, "
                    << "GPU rate " << std::fixed << (1e-3*Msps) << " Gs/s/subband in "
                    << cfg->vdifinfo.nbands << " subbands";

    }

}

/////////////////////////////////////////////////////////////////////////////////////

/**
 * Wrapper for processing one raw data segment.
 * Processing for single-subband data is easy.
 * Processing for multi-subband data is complicated by data layout.
 */
int BackendGPU::processSlot(const int slothandle)
{
    cuda_config_pool_t* cp = (cuda_config_pool_t*)this->pimpl;
    cuda_worker_t* cs = &(cp->pool[slothandle]);
    const SpectrometerConfig* cfg = &(cs->cfg);
    int rc;

    /* General processing */
    lockscope {
        boost::mutex::scoped_lock sharedGPUlock(m_cuCtxMutex);
        CUDA_CALL( cudaSetDevice(cs->g) );

        if ((cfg->vdifinfo.nbits == 2) || (cfg->vdifinfo.nbits == 8)) {
            /* Make histogram of raw data in CUDA 'd2h' stream */
            // 256-level histogram can be later untangled into 2-bit 1/2/4/8/16/32-channel easily,
            // see cpu_histogramtransform_8bit_to_2bit_4ch and similar()
            CUDA_TIMING_START(cs->evt_kstart, cs->d2h);
            histogram256Kernel<<<PARTIAL_HISTOGRAM256_COUNT, HISTOGRAM256_THREADBLOCK_SIZE, 0, cs->d2h>>> (
                cs->d_partial_histogram256, (hist_t *)cs->d_rawbuf, cs->rawbuflen / sizeof(hist_t)
            );
            mergeHistogram256Kernel<<<HISTOGRAM256_BIN_COUNT, MERGE_THREADBLOCK_SIZE, 0, cs->d2h>>> (
                (hist_t*)cs->d_histogram256, cs->d_partial_histogram256, PARTIAL_HISTOGRAM256_COUNT
            );
            CUDA_CALL( cudaMemcpyAsync(cs->h_histogram256, cs->d_histogram256, 256*sizeof(hist_t), cudaMemcpyDeviceToHost, cs->d2h) );
            CUDA_TIMING_STOP(cs->evt_kstop, cs->evt_kstart, cs->d2h, "hist256", cs->rawbuflen);
        }
    }

    /* Spectral processing in CUDA 'sid' stream */
    boost::this_thread::yield();
    rc = makeSpectra(slothandle);

    /* Copy results off to CPU still in CUDA 'sid' stream linked to FFT/xmac kernels */
    boost::this_thread::yield();
    lockscope {
        boost::mutex::scoped_lock sharedGPUlock(m_cuCtxMutex);
        CUDA_CALL( cudaSetDevice(cs->g) );
        CUDA_CALL( cudaMemcpyAsync(cs->h_powspecs, cs->d_powspecs, cs->powspecslen, cudaMemcpyDeviceToHost, cs->sid) );
        CUDA_CALL( cudaEventRecord(cs->evt_spectrum_avail, cs->sid) );
    }

    return rc;
}

/////////////////////////////////////////////////////////////////////////////////////

/**
 * Multi-channel spectral processing
 */
int BackendGPU::makeSpectra(const int slothandle)
{
    cuda_config_pool_t* cp = (cuda_config_pool_t*)this->pimpl;
    cuda_worker_t* cs = &(cp->pool[slothandle]);
    const SpectrometerConfig* cfg = &(cs->cfg);

    size_t numBlocks, threadsPerBlock;
    unsigned long maxPhysThreads = cs->devprop.multiProcessorCount * cs->devprop.maxThreadsPerMultiProcessor;

    assert((cfg->vdifinfo.nbands >= 1) && (cfg->vdifinfo.nbands <= MAX_SUBBANDS));
    assert(cfg->scfg.nenabledbands >= 1);
    assert(cfg->vdifinfo.nbits == 2);

    /* Split spectral accumulation into subintegration (fits GPU memory size) */
    size_t nfft_done = 0;
    for (size_t subint = 0; subint < (size_t)cfg->pcfg.nsubints; subint++) {

        boost::mutex::scoped_lock sharedGPUlock(m_cuCtxMutex);
        CUDA_CALL( cudaSetDevice(cs->g) );

        /* Unpack as-is without de-interleave */
        CUDA_TIMING_START(cs->evt_kstart, cs->sid);
        const size_t nbytes = cfg->pcfg.rawbufsize / cfg->pcfg.nsubints;
        unsigned char* d_unpack_start = cs->d_rawbuf + nbytes*subint;

        //inspect_dev_buffer((void*)d_unpack_start, 16, "gpu raw inp to subint");

        threadsPerBlock = 32;
        numBlocks = div2ceil(nbytes,threadsPerBlock);
        if (!cfg->scfg.do_window) {
            // Note: failed approach: decode multi-channel as 1-bit
            //    cu_decode_2bit1ch <<< numBlocks, threadsPerBlock, 0, cs->sid >>> ( d_unpack_start, (float4*)cs->d_fft_multiband_in, nbytes );
            // Replacement: do a band-split (divergent I/O, slower...) out into individual positions in a flat array
            decode_2bitNch(numBlocks,threadsPerBlock,cs->sid, d_unpack_start, cs->d_fft_bands_in, nbytes, cfg->vdifinfo.nbands);
            CUDA_CHECK_ERRORS("2bNc");
        } else if (cfg->scfg.window_type == WINDOW_FUNCTION_HANN) {
            // Window by Hann function
            decode_2bitNch_Hann(numBlocks,threadsPerBlock,cs->sid, d_unpack_start, cs->d_fft_bands_in, nbytes, 2*cfg->scfg.nchan, cfg->vdifinfo.nbands);
            CUDA_CHECK_ERRORS("2bNc_Hann");
        } else if (cfg->scfg.window_type == WINDOW_FUNCTION_HAMMING) {
            // Window by Hamming function
            decode_2bitNch_Hamming(numBlocks,threadsPerBlock,cs->sid, d_unpack_start, cs->d_fft_bands_in, nbytes, 2*cfg->scfg.nchan, cfg->vdifinfo.nbands);
            CUDA_CHECK_ERRORS("2bNc_Hamming");
        } else {
            // Window by other (WINDOW_FUNCTION_HFT248D or other custom)
            // TODO: invoke cu_decode_2bitXXch_CustomWindow (..., Lfft, d_weights )
        }
        CUDA_TIMING_STOP(cs->evt_kstop, cs->evt_kstart, cs->sid, "decode", (8*nbytes)/cfg->vdifinfo.nbits);

        /* FFT */
        // Note: not all bands might have been selected, do FFT only of the selected ones
        CUDA_TIMING_START(cs->evt_kstart, cs->sid);
        int nbands_dfted = 0;
        for (int band=0; band<cfg->vdifinfo.nbands; band++) {
            if (cfg->scfg.enableAutocorr[band]) {
                cufftReal* idata = cs->d_fft_bands_in[band];
                cufftComplex* odata = (cufftComplex*)cs->d_fft_out[nbands_dfted];
                // Note: CUFFT docu: "idata and odata pointers are both required to be aligned to **cufftComplex** data type"
                //       Hence cannot use the interleaved samples and decode offset '(cufftReal*)cs->d_fft_multiband_in + band'
                CUFFT_CALL( cufftExecR2C(cs->cufftplan, idata, odata) );
                nbands_dfted++;
            }
        }
        CUDA_TIMING_STOP(cs->evt_kstop, cs->evt_kstart, cs->sid, "r2c FFT", (2*cfg->scfg.nchan)*cfg->pcfg.nfft_subint*nbands_dfted);

        /* Accumulate */
        CUDA_TIMING_START(cs->evt_kstart, cs->sid);
        threadsPerBlock = 64;
        numBlocks = div2ceil(max(maxPhysThreads,(unsigned long)(cfg->scfg.nchan+0)), threadsPerBlock);
        for (int outband = 0; outband < nbands_dfted; outband++) {
            float* d_specout = (float*)cs->d_powspecs + outband*(cfg->scfg.nchan+0);
            autoPowerSpectrum_v3_skipNyquist <<< numBlocks, threadsPerBlock, 0, cs->sid >>> (
                (cufftComplex*)cs->d_fft_out[outband],
                d_specout,
                cfg->scfg.nchan+0,
                cfg->pcfg.nfft_subint
            );
            CUDA_CHECK_ERRORS("autoPowerSpectrum_v3_skipNyquist");
        }
        CUDA_TIMING_STOP(cs->evt_kstop, cs->evt_kstart, cs->sid, "auto(v3)", (cfg->scfg.nchan+0)*cfg->pcfg.nfft_subint*nbands_dfted); // complex input

        if (cfg->scfg.do_cross) {
            // Second pass, compute just the missing Re{XY} Im{XY} since from Autos we already have XX, YY
            // Note: due to arbitrary multi-band and possibly arbitrary band-against-band we cannot really use
            //       pre-interleaved FFT outputs conveniently, so, no generic access pattern to optimize...
            // For now we do not support arbitrariy band-against-band but this can be done in the future,
            // would just need a M&C command to send band pairs and put them in a lookup table for here.
            CUDA_TIMING_START(cs->evt_kstart, cs->sid);
            threadsPerBlock = 64;
            numBlocks = div2ceil(max(maxPhysThreads,(unsigned long)(cfg->scfg.nchan+0)), threadsPerBlock);
            float2* dcrossout = (float2*) ( (float*)cs->d_powspecs + nbands_dfted*(cfg->scfg.nchan+0) ); // append after autocorrs
            int autobandnr = 0;             // counter, current autocorr out of 'nbands_dfted'
            int nbands_crossmultiplied = 0; // counter, current crosscorr
            for (int band=0; band<cfg->vdifinfo.nbands; band += 2) {
                if (!cfg->scfg.enableAutocorr[band] || !cfg->scfg.enableAutocorr[band+1]) {
                    // Band pair incomplete so cannot have cross-corr requested for it
                    if (cfg->scfg.enableAutocorr[band+0]) { autobandnr++; }
                    if (cfg->scfg.enableAutocorr[band+1]) { autobandnr++; }
                } else {
                    // Band pair complete, want cross-product?
                    if (cfg->scfg.enableCrosscorr[band/2]) {
                        float2* x = (float2*)cs->d_fft_out[autobandnr+0];
                        float2* y = (float2*)cs->d_fft_out[autobandnr+1];
                        cu_accumulate_xpol_only_skipNyquist<<<numBlocks,threadsPerBlock>>>(x, y, dcrossout, cfg->scfg.nchan+0, cfg->pcfg.nfft_subint);
                        dcrossout += cfg->scfg.nchan+0;
                        nbands_crossmultiplied++;
                        CUDA_CHECK_ERRORS("cu_accumulate_xpol_only_skipNyquist");
                    }
                    autobandnr += 2;
                }
            }
            CUDA_TIMING_STOP(cs->evt_kstop, cs->evt_kstart, cs->sid, "xpol_reim", (cfg->scfg.nchan+0)*cfg->pcfg.nfft_subint*nbands_crossmultiplied); // complex input
        }

        /* Next sub-integration */
        nfft_done += cfg->pcfg.nfft_subint;

    }//for(subints)

    //inspect_dev_buffer((void*)cs->d_powspecs, 16, "gpu 1st spectrum");

    if (nfft_done != (size_t)cfg->pcfg.nfft) {
        L_(lerror) << "makeSpectra() for some reason did " << nfft_done << " DFTs instead of expected " << cfg->pcfg.nfft;
        exit(1);
    }

    return 0;
}

/////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////////

bool BackendGPU::allocateWorker(const SpectrometerConfig* cfg, int slot)
{
    boost::mutex::scoped_lock sharedGPUlock(m_cuCtxMutex);

    cuda_config_pool_t* cp = (cuda_config_pool_t*)this->pimpl;
    cuda_worker_t* cs = &(cp->pool[slot]);

    /* Select correct GPU */
    L_(linfo) << "Initializing CUDA arrays for GPU " << cs->g << " stream " << cs->s;
    CUDA_CALL( cudaSetDevice(cs->g) );
    CUDA_CALL( cudaGetDeviceProperties(&cs->devprop, cs->g) );
    CUDA_PRINT_MEMORY_INFO("before worker allocs");

    /* Create time and task tracking events */
    CUDA_CALL( cudaEventCreateWithFlags(&cs->evt_start, cudaEventBlockingSync) );
    CUDA_CALL( cudaEventCreateWithFlags(&cs->evt_stop, cudaEventBlockingSync) );
    CUDA_CALL( cudaEventCreateWithFlags(&cs->evt_raw_overwriteable, cudaEventDisableTiming|cudaEventBlockingSync) );
    CUDA_CALL( cudaEventCreateWithFlags(&cs->evt_spectrum_avail, cudaEventDisableTiming|cudaEventBlockingSync) );
    CUDA_CALL( cudaEventCreateWithFlags(&cs->evt_sync_with_yield, cudaEventDisableTiming|cudaEventBlockingSync) );
    CUDA_CALL( cudaEventCreate(&cs->evt_kstart) );
    CUDA_CALL( cudaEventCreate(&cs->evt_kstop) );

    /* Set up stream */
    CUDA_CALL( cudaStreamCreate( &(cs->sid) ) );
    CUDA_CALL( cudaStreamCreate( &(cs->h2d) ) );
    CUDA_CALL( cudaStreamCreate( &(cs->d2h) ) );

    /* Raw input data (GPU side) */
    cs->rawbuflen = cfg->pcfg.rawbufsize;
    CUDA_CALL( cudaMalloc( (void **)&(cs->d_rawbuf), cs->rawbuflen ) );
    CUDA_CALL( cudaMemsetAsync( cs->d_rawbuf, 0xFF, cs->rawbuflen, cs->sid) );
    CUDA_PRINT_MEMORY_INFO("after raw data alloc");

    /* Histogram related array(s) */
    CUDA_CALL( cudaMalloc((void **)&(cs->d_partial_histogram256), PARTIAL_HISTOGRAM256_COUNT * HISTOGRAM256_BIN_COUNT * sizeof(hist_t)) );
    CUDA_CALL( cudaMalloc((void **)&(cs->d_histogram256), 256*sizeof(hist_t)) );
    CUDA_CALL( cudaMallocHost((void **)&(cs->h_histogram256), 256*sizeof(hist_t), cudaHostAllocDefault) );
    CUDA_CALL( cudaMemsetAsync( cs->d_partial_histogram256, 0x00, PARTIAL_HISTOGRAM256_COUNT * HISTOGRAM256_BIN_COUNT * sizeof(hist_t), cs->sid ) );
    CUDA_CALL( cudaMemsetAsync( cs->d_histogram256, 0x00, 256*sizeof(hist_t), cs->sid ) );
    memset(cs->h_histogram256, 0x00, 256*sizeof(hist_t));

    /* FFT-related arrays */
    // TODO: any cfg->scfg.window_overlap != 0.0f would need output buf allocs and FFT input dist 'idist' to be updated!
    size_t unpacked_bytes_per_band = sizeof(float) * 2*cfg->scfg.nchan * cfg->pcfg.nfft_subint;
    if (unpacked_bytes_per_band % sizeof(cufftComplex) != 0) {
        unpacked_bytes_per_band += sizeof(cufftComplex) - unpacked_bytes_per_band % sizeof(cufftComplex);
    }
    // unpack-->FFT input : allocate one large flat array, assign regions of it for splitting out bands into single-band arrays
    CUDA_CALL( cudaMalloc( (void **)&(cs->d_fft_bands_in[0]), unpacked_bytes_per_band*cfg->vdifinfo.nbands) );
    for (int band=1; band<cfg->vdifinfo.nbands; band++) {
        cs->d_fft_bands_in[band] = cs->d_fft_bands_in[band-1] + unpacked_bytes_per_band/sizeof(float);
    }
    // FFT in -- > FFT out : allocate output with Nyquist bin included (otherwise CUFFT corrupts bins)
    const size_t dft_bytes_per_band_out = 2*sizeof(float) * (cfg->scfg.nchan + 1) * cfg->pcfg.nfft_subint;
    memset(cs->d_fft_out, 0, sizeof(cs->d_fft_out));
    for (int band=0; band<cfg->vdifinfo.nbands; band++) {
        CUDA_CALL( cudaMalloc( (void **)&(cs->d_fft_out[band]), dft_bytes_per_band_out ) );
        CUDA_CALL( cudaMemsetAsync( cs->d_fft_out[band],  0x00, dft_bytes_per_band_out, cs->sid ) );
    }
    CUDA_PRINT_MEMORY_INFO("after FFT in&out alloc");

    /* Time integrated output data. Nyquist point N/2+1 is discarded. */
    // Single-pol output layout: [XX   XX ...         ]                             : 1 ch x Lfft
    // Dual-pol output layout  : [XX   XX ... YY   YY ... ]                         : 2 ch x Lfft
    // Dual-pol with cross pwr : [XX Re{XY} Im{XY} YY ... ]                         : 2 ch x Lfft + Lfft (xmac kernel mode 1)
    // Dual-pol with cross pwr : [XX   XX ... YY   YY ...  Re{XY} Im{XY} ...]       : 2 ch x Lfft + Lfft (xmac kernel mode 0)
    // Quad output layout      : [XX   XX ... YY   YY ... ZZ  ZZ ... WW WW ... ]    : 4 ch x Lfft
    // Quad with cross pwr     : [XX Re{XY} Im{XY} YY ...  ZZ Re{ZW} Im{Zw} WW ...] : 4 ch x Lfft + 2 Lfft (xmac kernel mode 1)
    //--- new:
    //  flat output [b1_XX b1_XX b1_XX ... b2_XX b2_XX b2_XX ... bN_XX bN_XX ... bXY_XX Re{XY} Im{XY} YY ...]
    //  over-allocate the memory area for simplicity by assuming that all scfg.nspectra are cross-producs (4 float)
    cs->powspecslen = 4*sizeof(float) * (cfg->scfg.nchan+0) * cfg->scfg.nspectra;
    CUDA_CALL( cudaMalloc( (void **)&(cs->d_powspecs), cs->powspecslen ) );
    CUDA_CALL( cudaMallocHost( (void **)&(cs->h_powspecs), cs->powspecslen, cudaHostAllocPortable ) );
    CUDA_CALL( cudaMemsetAsync( cs->d_powspecs, 0x00, cs->powspecslen, cs->sid) );
    memset(cs->h_powspecs, 0x00, cs->powspecslen);
    cs->spec_weight = SPEC_INVALID;
    CUDA_PRINT_MEMORY_INFO("after output spectrum alloc");

    /* FFT plans */
    // Note: cuFFT API offers a multi-GPU capability (cufftXtSetGPUs(), cufftXtExecDescriptorC2C()) in which
    //       intermediate data will be transferred GPU-to-GPU. This makes most sense for 3D FFT, not 1D FFT.
    // Config that throws away Nyquist bin
    int dimn[1] = {2*cfg->scfg.nchan};  // r2c DFT size
    int inembed[1] = {0};         // ignored for 1D xform
    int onembed[1] = {0};         // ignored for 1D xform
    int istride = 1;              // step between successive in elements
    int ostride = 1;              // step between successive out elements
    int idist = 2*cfg->scfg.nchan;      // step between batches (cufft r2c input = real)
    int odist = cfg->scfg.nchan+1;      // step between batches (cufft r2c output = 1st Nyquist only)
                                    // with +0 rather than +1 could let overwrite the N/2+1 point, but
                                    // cufft appears to corrupt the nearby bins in this case!
    CUFFT_CALL( cufftPlanMany(&(cs->cufftplan), 1, dimn,
        inembed, istride, idist,
        onembed, ostride, odist,
        CUFFT_R2C,
        cfg->pcfg.nfft_subint  // and DFT manually called <=cfg->vdifinfo.nbands times (where scfg.enableAutocorr[<bandnr>] is true)
    ) );
    CUFFT_CALL( cufftSetStream(cs->cufftplan, cs->sid) );
    CUDA_PRINT_MEMORY_INFO("after CuFFT plan");

    CUDA_PRINT_MEMORY_INFO("after GPU worker allocations");

#ifdef DEBUG
    fprintf(stderr, "rawbuf[%d][%d] @ GPU %p\n", cs->g,cs->s,cs->d_rawbuf);
    fprintf(stderr, "fft[%d][%d] in @ GPU %p, out @ GPU %p\n", cs->g,cs->s,cs->d_fft_bands_in[0],cs->d_fft_out[0],cs->d_fft_out[cfg->vdifinfo.nbands]);
#endif

    L_(linfo) << "Finished initializing CUDA arrays for GPU " << cs->g << " stream " << cs->s;
    cs->initialized = true;
    return true;
}

bool BackendGPU::deallocateWorker(int slot)
{
    boost::mutex::scoped_lock sharedGPUlock(m_cuCtxMutex);

    cuda_config_pool_t* cp = (cuda_config_pool_t*)this->pimpl;
    cuda_worker_t* cs = &(cp->pool[slot]);

    /* Select correct GPU */
    L_(linfo) << "De-Initializing CUDA arrays for GPU " << cs->g << " stream " << cs->s;
    CUDA_CALL( cudaSetDevice(cs->g) );
    CUDA_CALL( cudaDeviceSynchronize() );

    /* Destroy time and task tracking events */
    CUDA_CALL( cudaEventDestroy(cs->evt_start) );
    CUDA_CALL( cudaEventDestroy(cs->evt_stop) );
    CUDA_CALL( cudaEventDestroy(cs->evt_raw_overwriteable) );
    CUDA_CALL( cudaEventDestroy(cs->evt_spectrum_avail) );
    CUDA_CALL( cudaEventDestroy(cs->evt_sync_with_yield) );
    CUDA_CALL( cudaEventDestroy(cs->evt_kstart) );
    CUDA_CALL( cudaEventDestroy(cs->evt_kstop) );

    /* Destroy stream */
    CUDA_CALL( cudaStreamDestroy(cs->sid) );
    CUDA_CALL( cudaStreamDestroy(cs->h2d) );
    CUDA_CALL( cudaStreamDestroy(cs->d2h) );

    /* Free raw input data (GPU side) */
    CUDA_CALL( cudaFree(cs->d_rawbuf) );

    /* Free histogram related array(s) */
    CUDA_CALL( cudaFree(cs->d_partial_histogram256) );
    CUDA_CALL( cudaFree(cs->d_histogram256) );
    CUDA_CALL( cudaFreeHost(cs->h_histogram256) );

    /* Free FFT-related arrays */
    CUDA_CALL( cudaFree(cs->d_fft_bands_in[0]) ); // rest [1..n] are ptrs to other offsets in same flat array
    for (int n=0; n<MAX_SUBBANDS; n++) {
        if (cs->d_fft_out[n] != NULL) {
            CUDA_CALL( cudaFree(cs->d_fft_out[n]) );
        }
        cs->d_fft_out[n] = NULL;
    }

    /* Free time integrated output data */
    CUDA_CALL( cudaFree(cs->d_powspecs) );
    CUDA_CALL( cudaFreeHost(cs->h_powspecs) );

    /* FFT plans */
    CUFFT_CALL( cufftDestroy(cs->cufftplan) );

    CUDA_PRINT_MEMORY_INFO("after GPU stream deallocations");

    L_(linfo) << "Finished de-initializing CUDA arrays for GPU " << cs->g << " stream " << cs->s;
    cs->initialized = false;
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////

/**
 * Print out the spectral processing setup, and memory usage estimate.
 */
void BackendGPU::showConfig(const SpectrometerConfig* cfg) const
{
    assert(cfg != NULL);

// TODO: replace fprintf by an ostream?

    const double to_GB = 1/(1024*1024*1024.0);
    size_t mem_gpu, mem_cpu;

    estimateMemoryRequirement(cfg, &mem_cpu, &mem_gpu);

    fprintf(stderr, "Computational setup  : %d GPUs x %d streams x %d sub-integrations, "
            "without cuFFT Callbacks.\n",
            cfg->pcfg.nGPUs, cfg->pcfg.nstreams, cfg->pcfg.nsubints
    );
    fprintf(stderr, "Estimated memory req.: %.2f GB per GPU, %.2f GB on host\n", mem_gpu*to_GB, mem_cpu*to_GB);
    fprintf(stderr, "Spectral setup       : %d spectral points, %d subbands, %.3fs integration (wish was %.3fs) of %d spectra in %d sub-ints\n",
            cfg->scfg.nchan, cfg->vdifinfo.nbands, cfg->pcfg.Tint_s, cfg->scfg.Tint_wish_s, cfg->pcfg.nfft, cfg->pcfg.nsubints);
    fprintf(stderr, "Time domain taper    : %s time domain windowing with %.1f%% overlap\n",
            (SpectralSettings::windowType(cfg->scfg.window_type)).c_str(), 100.0*cfg->scfg.window_overlap);
    fprintf(stderr, "Spectral output      : %d spectra per integration, %s cross-power spectra\n",
            cfg->scfg.nspectra, (cfg->scfg.do_cross) ? "including" : "with no");
    if (CHECK_TIMING) {
        fprintf(stderr, "Special/debug        : ");
        if (CHECK_TIMING) { fprintf(stderr, "kernel timing  "); }
        fprintf(stderr, "\n");
    }

    for (int g = 0; g < cfg->pcfg.nGPUs; g++) {
        cudaDeviceProp dp;
        CUDA_CALL( cudaGetDeviceProperties(&dp, cfg->pcfg.mapGPUs[g]) );
        fprintf(stderr, "Card %d: CUDA device %d: %s, CC %d.%d, %d threads/block, warpsize %d\n",
                g, cfg->pcfg.mapGPUs[g], dp.name, dp.major, dp.minor,
                dp.maxThreadsPerBlock, dp.warpSize
        );
    }

    return;
}

/////////////////////////////////////////////////////////////////////////////////////
