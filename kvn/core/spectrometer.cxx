#include "logger.h"

#include "version_gittag.hxx"
#include "core/common.hxx"
#include "core/spectrometer.hxx"
#include "core/backend_gpu.hxx"
#include "core/backend_cpu.hxx"
#include "kvnmc2api/kvnmc_defs.hxx"
#include "network/udpvdifreceiver.hxx"

#include <iostream>
#include <iterator>
#include <vector>

/** C'stor with TCP port numbers to listen on */
Spectrometer::Spectrometer(int cmdPort, int monPort, int outputPort, int vdifPort)
  : m_ctrlListener(*this,cmdPort), m_monitorListener(*this,monPort), m_outputHandler(outputPort), m_udpPort(vdifPort)
{
    m_rx_started = false;
    m_vdifOffset = 0;
    m_outputHandler.enableFileOutput(m_cfg.scfg.writespectra); // propagate default value downstream
#ifdef HAVE_GPU
    m_backend = new BackendGPU();
#else
    m_backend = new BackendCPU();
#endif
}

/** Direct read/write accessor to spectral settings */
SpectralSettings& Spectrometer::spectralCfg()
{
    return m_cfg.scfg;
}

/** Direct read/write accessor to spectral settings */
ProcessingSettings& Spectrometer::processingCfg()
{
    return m_cfg.pcfg;
}

/** Handle a command string and return a response string. Invoked from SpectrometerTCPServer listener only. */
std::string Spectrometer::handleCommand(std::string cmd)
{
    boost::mutex::scoped_lock cmdlock(m_cmdmutex);

    // Let the M&C class parse the command
    m_mciface.load(cmd);
    L_(linfo) << m_mciface.getCommandName() << " : handling command";

    // Let the M&C class now execute the command (factory method)
    std::vector<std::string> respItems;
    int rc = m_mciface.handle(respItems, *this);

    // Collect the M&C response to send back
    std::stringstream respstream;
    std::copy(respItems.begin(), respItems.end()-1, std::ostream_iterator<std::string>(respstream,","));
    respstream << respItems.back();

    std::string response = respstream.str();
    L_(linfo) << m_mciface.getCommandName() << " : handler returned rc=" << rc << ", response=" << response;

    return response;
}

/** Method that starts the spectrometer M&C listener loop (default: non-blocking, background) */
void Spectrometer::startMC(bool blocking)
{
    L_(linfo) << "Started version " << KVNMC2API_VERSION_STR << " (" << GIT_REVISION << ")";

    L_(linfo) << "Control acceptor is listening on TCP/IPv4 port " << m_ctrlListener.getPort();
    m_ctrlListener.run(blocking);

    L_(linfo) << "Monitor acceptor is listening on TCP/IPv4 port " << m_monitorListener.getPort();
    m_monitorListener.run(blocking);

    L_(linfo) << "Spectraloutput acceptor is listening on TCP/IPv4 port " << m_outputHandler.getPort();
    // no .run() necessary
}

/** Start background VDIF data receiption pump */
bool Spectrometer::startRX(void)
{
    if (m_rx_started) {
        L_(lwarning) << "StartRX(): Reception was already running! Restarting...";
        stopRX();
    }

    if (!m_cfg.scfg.validate()) {
        L_(lerror) << "Cannot start because spectral settings (integration time, #channels) are invalid";
        return false;
    }
    if (!m_cfg.pcfg.validate()) {
        L_(lerror) << "Cannot start because processing settings (observation details, GPU infos) are invalid";
        return false;
    }

    if (!m_vdifRx.open(m_udpPort, m_vdifOffset)) {
        L_(lerror) << "Failed to open VDIF UDP port " << m_udpPort;
        return false;
    }

    // Check details of incoming VDIF
    if (!m_vdifRx.analyze()) {
        L_(lerror) << "Error determing UDP VDIF information from UDP stream";
        m_vdifRx.close();
        return false;
    }
    m_cfg.vdifinfo = m_vdifRx.getVDIFDetails();

    // Apply VDIF & DFT details to derive an actual integration time of integer DFT-multiples and Frame-multiples
    m_cfg.recalculate();
    if (m_cfg.pcfg.Tint_s < 10e-3) {
        L_(lerror) << "Cannot start, requested Tint=" << std::setprecision(3) << m_cfg.scfg.Tint_wish_s << "s "
                   << "under " << m_cfg.vdifinfo.framespersec << " fps results in integer-frame "
                   << "Tint=" << std::setprecision(3) << m_cfg.pcfg.Tint_s << "s. "
                   << "The minimum Tint is 0.010s.";
        m_vdifRx.close();
        return false;
    }
    L_(linfo) << "From VDIF properties and DFT size determined best-fit Tint=" << std::setprecision(6) << std::fixed << m_cfg.pcfg.Tint_s << "s, "
        << m_cfg.pcfg.rawbufsize << "-byte buffer, " << m_cfg.pcfg.rawbufsize/(double)m_cfg.vdifinfo.payloadsize << " frames/buf";

    // Allocate buffers for VDIF reception
    m_cfg.pcfg.nbuffers = 2 * m_cfg.pcfg.nGPUs * m_cfg.pcfg.nstreams;
    if (m_cfg.pcfg.nbuffers < 4) {
        m_cfg.pcfg.nbuffers = 4; // at least 4-fold buffering
    }
    if (!m_vdifRx.allocate(m_cfg.pcfg.nbuffers, m_cfg.pcfg.rawbufsize)) {
        L_(lerror) << "Error allocating " << std::setprecision(1)
                   << (m_cfg.pcfg.Tint_s*1e3) << "msec receive buffers "
                   << "(" << m_cfg.pcfg.rawbufsize << " byte)";
        m_vdifRx.close();
        return false;
    }

    // Start reception
    m_rx_started = m_vdifRx.startRx();

    return m_rx_started;
}

/** Stop background VDIF data receiption pump */
bool Spectrometer::stopRX()
{
    if (!m_rx_started) {
        return true;
    }
    m_vdifRx.stopRx();
    m_vdifRx.deallocate();
    m_vdifRx.close();
    m_rx_started = false;
    return true;
}

/**
 * Start GPU processing.
 * The current spectral settings are copied into "active" settings
 * and spectral processing is initialized and started with that copy.
 * Note that active settings can not be changed on the fly, i.e.,
 * settings changed while GPU processing is already ongoing will
 * become activated only after stopRX() followed by startRX().
 */
bool Spectrometer::startGPU()
{
    if (!m_cfg.scfg.validate()) {
        L_(lerror) << "Cannot start GPU because spectral settings are invalid";
        return false;
    }

    // Stop current processing
    m_backend->reset();
    m_backend->setOutput(&m_outputHandler);

    // Copy new settings
    lockscope {
        boost::mutex::scoped_lock cmdlock(m_cfgmutex);
        m_cfg_active = m_cfg;
    }

    // Device/system dependend limits
    int devicecount = m_backend->getDeviceCount();
    for (int n=0; n<m_cfg_active.pcfg.nGPUs; n++) {
        m_cfg_active.pcfg.mapGPUs[n] = n % devicecount;
    }
    if ((m_cfg_active.pcfg.nGPUs < 1) || (m_cfg_active.pcfg.nGPUs > MAX_GPUS)) {
        L_(lwarning) << "number of requested devices of " << m_cfg_active.pcfg.nGPUs << " not between 1 and " << MAX_GPUS << "! "
            << "Defaulting to " << devicecount << " (installed).";
        m_cfg_active.pcfg.nGPUs = devicecount;
    }
    if (m_cfg_active.pcfg.nGPUs > devicecount) {
        L_(lwarning) << "number of requested devices of " << m_cfg_active.pcfg.nGPUs << " exceeds " << devicecount << " devices actually detected. "
            << "Defaulting to " << devicecount << ".";
        m_cfg_active.pcfg.nGPUs = devicecount;
    }
    if ((m_cfg_active.pcfg.nstreams < 1) || (m_cfg_active.pcfg.nstreams > MAX_STREAMS)) {
        L_(lwarning) << "number of requested streams per device of " << m_cfg_active.pcfg.nstreams << " not between 1 and " << MAX_STREAMS << "! "
            << "Defaulting to 2";
        m_cfg_active.pcfg.nstreams = 2; // default 2, or is there a better choice?
    }
    for (int i = 0; i < m_cfg_active.pcfg.nGPUs; i++) {
        if ((m_cfg_active.pcfg.mapGPUs[i] < 0) || (m_cfg_active.pcfg.mapGPUs[i] >= devicecount)) {
            int newentry = i % devicecount;
            L_(lwarning) << "entry " << (i+1) << "/" << m_cfg_active.pcfg.nGPUs << " of GPU mapping is nonexistend GPU nr " << m_cfg_active.pcfg.mapGPUs[i] << " (" << devicecount << " installed). "
                << "Setting it to " << newentry << ".";
            m_cfg_active.pcfg.mapGPUs[i] = newentry;
        }
    }
    if (m_cfg_active.pcfg.nGPUs <= 0) {
        L_(lerror) << "Found " << devicecount << " GPU installed. No GPUs were specified in configuration!";
        return false;
    }
    // TODO: assert() that m_cfg_active.vdifinfo.nbands is a power of two?

    // Let the backend optimize some settings (memory footprint)
    m_backend->optimizeConfiguration(&m_cfg_active);

    // Register VDIF integration period buffers into the backend
    mempinVDIFBuffers(true);

    // Initialize the backend with the activated processing configuration
    bool ok = m_backend->start(&m_cfg_active);
    if (!ok) {
        L_(lerror) << "Failed to initialize backend";
        return false;
    }

    // Assign backend to VDIF UDP receiver so the latter pumps new data into backend::takeData()
    m_vdifRx.registerRecipient(m_backend);

    return true;
}

/** Stop GPU processing */
bool Spectrometer::stopGPU()
{
    //m_vdifRx.unregisterRecipient(&m_gpu); // unregister just m_gpu
    m_vdifRx.unregisterRecipient(NULL); // unregister all
    bool ok = m_backend->stop();
    if (!ok) {
        L_(lerror) << "Failed to de-initialize backend";
    }

    mempinVDIFBuffers(false);

    return ok;
}


/** Enable or disable spectral output to a file. Output to TCP clients is not affected. */
void Spectrometer::enableFileOutput(bool enable)
{
    m_outputHandler.enableFileOutput(enable);
}



/** Pin or un-pin memory buffers of the UDPVDIFReceiver backend (GPU) */
void Spectrometer::mempinVDIFBuffers(bool pin)
{
    int Nbuffers;
    size_t buflen;
    void** buffers;

    buffers = m_vdifRx.getBufferPtrs(Nbuffers, buflen);

    if (buffers != NULL) {
        if (pin) {
            m_backend->registerUserDataBuffers(Nbuffers, buffers, buflen);
        } else {
            m_backend->unregisterUserDataBuffers(Nbuffers, buffers, buflen);
        }
        free(buffers);
    }
}
