#include "logger.h"
#include "commands/common.hxx"

#include <string>
#include <vector>

#include <boost/filesystem.hpp>

KVNMC2API_CREATE_AND_REGISTER_CMD(OBNM);

/**
 * Set observation name, create /home/DAS_LOC/OBS/<Obs_Name> directory ('OBS_ROOT_DIR'/<Obs_Name>) and populate it
 */
int MCMsgCommandResponse_OBNM::handle(const std::vector<std::string>& args, std::vector<std::string>& out, SpectrometerInterface& k, bool isQuery)
{
    if (args.size() > 1) {
        out.push_back(CMD_RET_PARAMERROR);
        out.push_back("OBNM");
        return -1;
    }

    // Command
    if (!isQuery && args.size() == 1) {
        k.spectralCfg().observation = args[0];
        std::string pathname = std::string(OBS_ROOT_DIR) + std::string("/") + args[0] + std::string("/");

        boost::system::error_code ec;
        boost::filesystem::path dirpath(pathname);
        boost::filesystem::create_directory(dirpath, ec);
        if (ec.value() != boost::system::errc::success) {
            L_(ldebug) << "OBNM failed to create new directory " << pathname << ", error " << ec.value() << " '" << ec.message() << "'";
            out.push_back(CMD_RET_PARAMERROR);
            out.push_back("OBNM");
            return -1;
        }
        L_(linfo) << "OBNM created new directory " << pathname;
        k.spectralCfg().obsPath = pathname;
    }

    if (isQuery && args.size() != 0) {
        out.push_back(CMD_RET_CMDERROR);
        out.push_back("OBNM");
        return -1;
    }

    // Response
    out.push_back(CMD_RET_DONE);
    out.push_back("OBNM");
    out.push_back(k.spectralCfg().observation);

    return 0;
}

