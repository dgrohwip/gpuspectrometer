#include "commands/common.hxx"

#include <string>
#include <vector>

KVNMC2API_CREATE_AND_REGISTER_CMD(ACQD);

int MCMsgCommandResponse_ACQD::handle(const std::vector<std::string>& args, std::vector<std::string>& out, SpectrometerInterface& k, bool isQuery)
{
    if (args.size() > 1) {
        out.push_back(CMD_RET_PARAMERROR);
        out.push_back("ACQD");
        return -1;
    }

    // Command
    if (!isQuery && args.size() == 1) {
        if (args[0] == "SAVE") {
            k.spectralCfg().writespectra = true;
            k.enableFileOutput(true);
        } else if (args[0] == "NOSAVE") {
            k.spectralCfg().writespectra = false;
            k.enableFileOutput(false);
        } else {
            out.push_back(CMD_RET_PARAMERROR);
            out.push_back("ACQD");
            return -1;
        }
    }

    if (isQuery && args.size() != 0) {
        out.push_back(CMD_RET_CMDERROR);
        out.push_back("ACQD");
        return -1;
    }

    // Response
    out.push_back(CMD_RET_DONE);
    out.push_back("ACQD");
    out.push_back(k.spectralCfg().writespectra ? "SAVE" : "NOSAVE");

    return 0;
}

