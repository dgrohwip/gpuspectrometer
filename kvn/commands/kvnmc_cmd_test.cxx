#include "commands/common.hxx"
#include "core/spectrometer_sim.hxx"

#include <boost/thread.hpp>

#include <iostream>
#include <cassert>
#include <unistd.h>

using namespace std;

static void inject_command(int Ncollisions, const std::string& req, std::string& resp, SpectrometerInterface& k);

static std::string inject_list[] = {
    " ", "garbagecommand",
    "OBNM", "OBNM,test", "OBNM", "OBNM?", "OBNM;", "OBNM,test?", "OBNN,test;",
    "ACQD", "ACQD,illegal", "ACQD,SAVE", "ACQD?", "ACQD;",
    "CONF,SEL_CORIPLEN?", "CONF,SEL_CORIPLEN=250" /*msec*/,  "CONF,SEL_CORIPLEN?",
    "CONF,SEL_CORLEN?", "CONF,SEL_CORLEN=32768" /*channels*/, "CONF,SEL_CORLEN?",
    "CONF,SEL_CORWINDOW?", "CONF,SEL_CORWINDOW=HFT248D", "CONF,SEL_CORWINDOW?",
    "CONF,SEL_COROUTSTREAM=ALL:", "CONF,SEL_COROUTSTREAM=ALL_ON", /* try invalid syntax */
    "CONF,SEL_COROUTSTREAM=ALL:OFF",  // syntax: <ALL|AUTOx|CROSSxy>:<ON|OFF>
    "CONF,SEL_COROUTSTREAM=CROSS14:ON",  // syntax: <ALL|AUTOx|CROSSxy>:<ON|OFF>
    "CONF,SEL_COROUTSTREAM=CROSS12:ON",  // syntax: <ALL|AUTOx|CROSSxy>:<ON|OFF>
    "CONF,SEL_COROUTSTREAM=ALL:ON",  // syntax: <ALL|AUTOx|CROSSxy>:<ON|OFF>
    "CONF,SEL_COROUTSTREAM?",
    "STRT", "STOP",
    "QUIT",
    "" // last entry must be blank
};

int main(void)
{
    SpectrometerSim noOpSpec(TCP_CONTROL_PORT_DEFAULT, TCP_MONITOR_PORT_DEFAULT, TCP_SPECTRAL_PORT_DEFAULT, UDP_VDIF_PORT_DEFAULT);

    try {

        noOpSpec.startMC(/*blocking=*/false);
        noOpSpec.startRX();
        noOpSpec.startGPU();

    } catch (std::exception &e) {

        std::cout << "Error: " << e.what() << "\n";
        return -1;
    }

    int ninjected = 0;
    std::string cmd, response;
    while (1) {

        if (inject_list[ninjected] == "") {
            ninjected = 0;
            break; // if single run-through only of list wanted
        }

        cmd = inject_list[ninjected];
        inject_command(0, cmd, response, noOpSpec);

        ninjected++;
        //sleep(1);
        usleep(100e3);

        if (cmd == "STRT") {
            // let run for a bit
            sleep(10);
        }

    }

    return 0;
}

class CmdInjector {

  private:
    CmdInjector();

  public:
    CmdInjector(const std::string& in, SpectrometerInterface* ptarget)
    {
        target = ptarget;
        input = in;
    }

    void operator()()
    {
        output = target->handleCommand(input);
        boost::this_thread::yield();
    }

    SpectrometerInterface* target;
    std::string input;
    std::string output;

};

void inject_command(int Ncollisions, const std::string& req, std::string& resp, SpectrometerInterface& k)
{
    int nthreads = Ncollisions + 1;
    CmdInjector* injectors[nthreads];
    boost::thread* threads[nthreads];

    for (int n=0; n<nthreads; n++) {
        injectors[n] = new CmdInjector(req, &k);
    }
    for (int n=0; n<nthreads; n++) {
        threads[n] = new boost::thread(*injectors[n]);
    }

    for (int n=0; n<nthreads; n++) {
        threads[n]->join();
        if (n == 0) {
            resp = injectors[0]->output;
        }
        //cout << "Injected command of thread " << n << "/" << nthreads << " "
        //    << injectors[n]->input << " got response " << injectors[n]->output << "\n";
        delete threads[n];
        delete injectors[n];
    }
}

