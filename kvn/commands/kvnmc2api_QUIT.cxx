#include "logger.h"
#include "commands/common.hxx"

#include <string>
#include <vector>
#include <iostream>

KVNMC2API_CREATE_AND_REGISTER_CMD(QUIT);

int MCMsgCommandResponse_QUIT::handle(const std::vector<std::string>& args, std::vector<std::string>& out, SpectrometerInterface& k, bool isQuery)
{
    // Try to stop GPU first, even if not started
    k.stopGPU();

    // Try to stop VDIF RX, even if not started
    k.stopRX();

    // Stop the program
    const std::string msg = "Received 'QUIT' command. Exiting normally.";
    L_(linfo) << msg;
    std::cout << msg << "\n";
    exit(EXIT_SUCCESS);

    return 0;
}

