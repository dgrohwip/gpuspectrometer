#include "commands/common.hxx"

#include <string>
#include <vector>

KVNMC2API_CREATE_AND_REGISTER_CMD(PCMD);

int MCMsgCommandResponse_PCMD::handle(const std::vector<std::string>& args, std::vector<std::string>& out, SpectrometerInterface& k, bool isQuery)
{
    out.push_back(CMD_RET_DONE);
    out.push_back("PCMD");

    // TODO: what exactly to do here?

    return 0;
}

