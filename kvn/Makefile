include Makefile.cuda.inc  # for CUDA parameters

.NOTPARALLEL:

# Enable GPU processing?
# 1: enable GPU (CUDA) code, 0: revert to CPU code (simulator mode)
HAVE_GPU := 1

# Check CUDA compiler really available
ifeq ($(HAVE_GPU),1)
    ifeq (,$(wildcard $(NVCC)))
        $(warning Warning: Cannot find CUDA compiler $(NVCC). Please check the settings in Makefile.top.)
        HAVE_GPU := 0
    endif
endif

##############################################################################################

CDEFS       = -D_FILE_OFFSET_BITS=64 -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE -D_XOPEN_SOURCE=600 -D_GNU_SOURCE
CXXFLAGS    = -O3 -march=native -Wall -g $(CDEFS) -I.
CUFLAGS     = -O3 -m64 -g $(CDEFS) -I. -I./cuda_kernels/ -Xcompiler -Wall
LIBS        = -lm -lboost_system -lboost_thread -lboost_filesystem -lboost_program_options -lpthread
CUDALIBS    = -lnvToolsExt

# NVFLAGS += -DDEBUG # to enable Kernel Timing and other debug output

# Subdirs with Makefile's that build some (self-)test programs
EXTRA_SUBDIRS = network core commands utilities

##############################################################################################

HDR  = logger.h
HDR += version_gittag.hxx
HDR += kvnmc2api/kvnmc_defs.hxx
HDR += kvnmc2api/kvnmc2api_factory.hxx
HDR += kvnmc2api/spectrometermonctrl.hxx
HDR += network/tcpserver.hxx
HDR += network/spectrometertcpserver.hxx

AUX_SRC  = kvnmc2api/spectrometermonctrl.cxx
AUX_SRC += kvnmc2api/kvnmc2api_factory.cxx
AUX_SRC += network/tcpserver.cxx
AUX_SRC += network/spectrometertcpserver.cxx
AUX_SRC += network/udpvdifreceiver.cxx
AUX_SRC += network/udpvdifreceiver_sim.cxx
AUX_SRC += core/spectrometer.cxx
AUX_SRC += core/spectrometer_sim.cxx
AUX_SRC += core/spectrometerconfig.cxx
AUX_SRC += core/spectralavg.cxx
AUX_SRC += settings/spectralsettings.cxx
AUX_SRC += settings/processingsettings.cxx
AUX_SRC += datarecipients/rawdatabuffer.cxx
AUX_SRC += datarecipients/rawdatabuffergroup.cxx
AUX_SRC += datarecipients/spectraloutput.cxx
AUX_SRC += outformats/FormatDSM.cxx
AUX_SRC += outformats/FormatDSMv2.cxx

CUDA_SRC = core/backend_gpu.cu
CPU_SRC  = core/backend_cpu.cxx

CMD_SRC = $(wildcard commands/kvnmc2api_*.cxx)

##############################################################################################

AUX_OBJS  = $(AUX_SRC:.cxx=.o)
CMD_OBJS  = $(CMD_SRC:.cxx=.o)
CUDA_OBJS = $(CUDA_SRC:.cu=.co)
CPU_OBJS  = $(CPU_SRC:.cxx=.o)

KVNMC_OBJS = $(CMD_OBJS) $(AUX_OBJS)

##############################################################################################

ifeq ($(HAVE_GPU),1)
    CXXFLAGS += -DHAVE_GPU
    LIBS += $(NVLDFLAGS) $(NVLDLIBS) $(CUDALIBS)
    KVNMC_OBJS += $(CUDA_OBJS)
else
    $(warning Warning: Compiling without GPU support (will use CPU code, no actual spectral output)!)
    KVNMC_OBJS += $(CPU_OBJS)
endif

##############################################################################################

all: $(EXTRA_SUBDIRS) kfftspec

version_gittag.hxx: version_gittag.sh
	./version_gittag.sh

$(EXTRA_SUBDIRS):
	$(MAKE) -C $@ MAKEFLAGS=$(MAKEFLAGS)

kfftspec: kfftspec.cxx $(KVNMC_OBJS)
	g++ $(CXXFLAGS) $< $(KVNMC_OBJS) -o $@ $(LIBS)

%.o: %.cxx $(HDR)
	g++ $(CXXFLAGS) -c -o $@ $<

%.co: %.cu
	$(NVCC) $(CUFLAGS) $(NV_TARGET_PLATFORMS) -c -o $@ $<

clean:
	rm -f kfftspec $(CMD_OBJS) $(AUX_OBJS) $(CUDA_OBJS) $(CPU_OBJS)
	for EXTRA in $(EXTRA_SUBDIRS); do \
		$(MAKE) -C $$EXTRA clean MAKEFLAGS=$(MAKEFLAGS); \
	done

.PHONY: all $(EXTRA_SUBDIRS) clean install
