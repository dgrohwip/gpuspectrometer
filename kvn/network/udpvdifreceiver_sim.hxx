#ifndef UDFVDIFRECEIVER_SIM_HXX
#define UDFVDIFRECEIVER_SIM_HXX

#include "datarecipients/datarecipient.hxx"
#include "datarecipients/rawdatabuffer.hxx"
#include "datarecipients/rawdatabuffergroup.hxx"
#include "network/udpvdifreceiverinterface.hxx"
#include "network/vdifdetails.hxx"

#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/noncopyable.hpp>

#include <stdlib.h>
#include <sys/time.h>

class DataRecipient;

class UDPVDIFReceiverSim : public UDPVDIFReceiverInterface {

    public:

        UDPVDIFReceiverSim(int verbosity=0);

        /**
         * Open an UDP socket
         * @param frameoffset byte position in the UDP/IP packet at which the actual VDIF frame starts
         */
        bool open(const int port, const size_t frameoffset = 0, const int timeout = DEFAULT_UDP_TIMEOUT_SEC);

        /** Close UDP socket */
        bool close();

        /**
         * Inspect the incoming UDP/IP traffic for a while and determine its properties.
         * Results are stored internally and accessible via getVDIFFramesPerSec() and others.
         * Helpful for deriving values for a later allocate().
         * \return true upon successful determination of frame rate, frame size, bits/sample, num channels per thread
         */
        bool analyze();

        // Accessors

        /** \return VDIF frames per second */
        size_t getVDIFFramesPerSec() const { return m_vdifdetails.framespersec; }

        /** \return VDIF frame size */
        size_t getVDIFFrameSize() const { return m_vdifdetails.framesize; }

        /** \return VDIF payload size */
        size_t getVDIFPayloadSize() const { return m_vdifdetails.payloadsize; }

        /** \return Number of channels in the VDIF stream (once it has been determined by snoopVDIF()) */
        int getVDIFNchan() const { return m_vdifdetails.nbands; }

        /** \return Number of bits/sample in the VDIF stream (once it has been determined by snoopVDIF()) */
        int getVDIFNbits() const { return m_vdifdetails.nbits; }

        /** \return Object containing VDIF details */
        VDIFDetails getVDIFDetails() const { return m_vdifdetails; }

        /// Buffer group

        /** Allocate a set of data receive buffers. The buffer size and excludes VDIF headers. */
        bool allocate(const int Nbuffers, const size_t);

        /** Free up all buffers */
        bool deallocate();

        /** Get pointer array of raw buffers, inteded for externally calling cudaHostRegister() */
        void** getBufferPtrs(int& Nbuffers, size_t& buflen);

    public:
        /**
         * Start data pump in the background. The VDIF payload data received into buffers
         * will be pushed to the registered recipient(s) in round-robin fashion.
         */
        bool startRx();

        /** Stop the data pump thread */
        bool stopRx();

        /**
         * Add a new recipient that shall receive raw buffers upon completion.
         * Call multiple times to add several recipients for round-robin reception.
         */
        bool registerRecipient(DataRecipient*);

        /** Remove a particular recipient, or remove all if argument is NULL */
        bool unregisterRecipient(DataRecipient*);

    public:
        void releaseSlotAsync(int slotnr)
        {
            m_groups->releaseSlot(slotnr);
            m_releasedBufCond.notify_all();
        }

    private:
        int     m_verbosity;

        int     m_udpport;
        int     m_frameoffset;
        VDIFDetails m_vdifdetails;

        boost::condition_variable m_releasedBufCond;

        size_t  m_overflowcount;

        boost::thread *m_rxDatapumpThread;
        boost::mutex m_iomutex;
        boost::mutex m_recipientmutex;
        std::vector<DataRecipient*> m_dataRecipients;

        RawDataBufferGroup *m_groups;

    private:

        /**
         * Worker that receives UDP/IP containing VDIF, strips VDIF headers, and pushes
         * completed buffers into the registered data recipient(s) (round-robin). Takes
         * care to preserve spacing in time appropriate for the integration time (i.e.
         * the buffer size).
         */
        void rxDatapumpWorker();

        /**
         * Detached thread(s) that handle passing completed data to a recipient.
         */
        void rxDatabufferHandover(int slot, boost::thread* self);

        /**
         * Increment the current [start;stop] interval by m_framesperbuf to determine the next data-capture window.
         */
        void nextStartStopRange(int32_t& start_sec, int32_t& start_frame, int32_t& stop_sec, int32_t& stop_frame, const size_t add_frames) const;

};

#endif // UDFVDIFRECEIVER_HXX
