
#include "network/udpvdifreceiver_sim.hxx"
#include "datarecipients/datarecipient.hxx"
#include "logger.h"

#include <iostream>
#include <iomanip>
#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>

#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/mman.h>

#include <errno.h>
#include <malloc.h>
#include <math.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#define PAGE_SZ 4096

#define lockscope // for notation

/////////////////////////////////////////////////////////////////////////////////////////////

/** Return a close power-of-2 to n */
static uint32_t ceil_pow2(uint32_t n)
{
    n--;
    n |= n >> 1;
    n |= n >> 2;
    n |= n >> 4;
    n |= n >> 8;
    n |= n >> 16;
    n++;
    return n;
}

/** Sleep at most T_usec microseconds, or shorter if interrupted */
static void nanosleep_sec(double T_sec)
{
    struct timespec treq;
    treq.tv_sec = (time_t)T_sec;
    T_sec -= treq.tv_sec;
    treq.tv_nsec = T_sec*1e9;
    //L_(linfo) << "nanosleep: " << std::fixed << std::setprecision(6) << T_sec << "sec : tv_ns=" << treq.tv_nsec;
    nanosleep(&treq, NULL); // can be interrupted! don't care to sleep more after interruption
}

/////////////////////////////////////////////////////////////////////////////////////////////
// UDPVDIFReceiverSim Class
/////////////////////////////////////////////////////////////////////////////////////////////

UDPVDIFReceiverSim::UDPVDIFReceiverSim(int verbosity)
{
    m_verbosity = verbosity;
    m_frameoffset = 0;
    m_rxDatapumpThread = NULL;
    m_groups = NULL;
}

/////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Open an UDP socket
 * @param frameoffset byte position in the UDP/IP packet at which the actual VDIF frame starts
 */
bool UDPVDIFReceiverSim::open(const int port, const size_t frameoffset, const int timeout)
{
    boost::mutex::scoped_lock iolock(m_iomutex);
    m_frameoffset = (frameoffset % 128);
    m_udpport = port;
    L_(linfo) << "Listening (simulated) on UDP port " << m_udpport << " for VDIF with " << m_frameoffset << "-byte PSN";
    return true;
}

/** Close UDP socket */
bool UDPVDIFReceiverSim::close()
{
    boost::mutex::scoped_lock iolock(m_iomutex);

    if (m_rxDatapumpThread != NULL) {
        m_rxDatapumpThread->interrupt();
        iolock.unlock();
        m_rxDatapumpThread->join();
        iolock.lock();
        delete m_rxDatapumpThread;
        m_rxDatapumpThread = NULL;
    }

    L_(linfo) << "Stopped (simulated) listening on UDP port " << m_udpport;

    return true;
}

/**
 * Inspect the incoming UDP/IP traffic for a while and determine its properties.
 * Results are stored internally and accessible via getVDIFFramesPerSec() and others.
 * Helpful for deriving values for a later allocate().
 * \return true upon successful determination of frame rate, frame size, bits/sample, num channels per thread
 */
bool UDPVDIFReceiverSim::analyze()
{
    const int tcase = 0;

    m_vdifdetails.clear();
    m_vdifdetails.headersize = 32;
    m_vdifdetails.framesize = 2;

    // Fake VDIF stream info
    if (tcase == 0) {
        // DBBC3 16 ch x 2 bit, 31250 fps, 8224 byte
        m_vdifdetails.nbands = 16;
        m_vdifdetails.framespersec = 31250;
        m_vdifdetails.framesize = 8224;
        m_vdifdetails.payloadsize = m_vdifdetails.framesize - m_vdifdetails.headersize;
    } else if (tcase == 1) {
        // OCTA-D
        m_vdifdetails.nbands = 16;
        m_vdifdetails.framespersec = 62500;
        m_vdifdetails.framesize = 8224;
        m_vdifdetails.payloadsize = m_vdifdetails.framesize - m_vdifdetails.headersize;
    } else {
        L_(lerror) << "UDPVDIFReceiverSim::analyze() test generator case " << tcase << " not implemented!";
        exit(1);
    }

    // Summary
    L_(linfo) << "Autodetect: simulated " << m_vdifdetails.getGoodputMBitPerSec() << " Mbit/s from fps ("
              << m_vdifdetails.geRateMBitPerSec() << " Mbit/s from UDP), "
              << m_vdifdetails.framespersec << " fps, "
              << m_vdifdetails.framesize << "-byte frames, "
              << m_vdifdetails.payloadsize << "-byte payload, "
              << m_vdifdetails.nbands << "-ch " << m_vdifdetails.nbits << "-bit";

    return true;
}

/** Allocate a set of data receive buffers. The buffer size excludes VDIF headers. */
bool UDPVDIFReceiverSim::allocate(const int Nbuffers, const size_t bufsize)
{
    if ((Nbuffers < 1) || (Nbuffers > 256) || (bufsize < (size_t)32*Nbuffers)) {
        return false;
    }

    assert ((bufsize % m_vdifdetails.payloadsize) == 0);
    //assert ((m_vdifdetails.framespersec % (bufsize/m_vdifdetails.payloadsize)) == 0); // integer nr of APs per second?

    if (m_groups != NULL) {
        delete m_groups;
    }
    m_groups = new RawDataBufferGroup(Nbuffers, bufsize, m_vdifdetails.nbands, m_udpport/*or vdif thread id?*/, m_verbosity);

    // Initialize with random data
    for (int n=0; n<m_groups->length(); n++) {
            L_(linfo) << "Filling buffer " << n << " with random data";
#if 0
            RawDataBuffer* buf = m_groups->getSlot(n);
            unsigned char* data = (unsigned char*)(buf->data);
            for (size_t m=0; m < buf->capacity; m++) {
                data[m] = rand() % 256;
            }
#else
            RawDataBuffer* buf = m_groups->getSlot(n);
            if (n==0) {
                unsigned char* data = (unsigned char*)(buf->data);
                for (size_t m=0; m < buf->capacity; m++) {
                    data[m] = rand() % 256;
                }
            } else {
                // Lazy, replicate the first buffer, since rand() is so slow
                mempcpy(buf->data, m_groups->getSlot(0)->data, buf->capacity);
            }
#endif
    }

    return true;
}

/** Release all data receive buffers */
bool UDPVDIFReceiverSim::deallocate()
{
    if (m_groups == NULL) {
        return true;
    }

    delete m_groups;
    m_groups = NULL;
    return true;
}

/** Get pointer array of buffers, inteded for externally calling cudaHostRegister() */
void** UDPVDIFReceiverSim::getBufferPtrs(int& Nbuffers, size_t& alloclen)
{
    void** bufs;
    if (m_groups == NULL) {
        return NULL;
    }

    Nbuffers = m_groups->length();
    alloclen = m_groups->capacity();
    bufs = (void**)calloc(sizeof(void*), Nbuffers);
    for (int n=0; n < Nbuffers; n++) {
        bufs[n] = m_groups->getSlot(n)->data;
    }

    return bufs;
}

/////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Add a new recipient that shall receive raw buffers upon completion.
 * Call multiple times to add several recipients for round-robin reception.
 */
bool UDPVDIFReceiverSim::registerRecipient(DataRecipient* r)
{
    boost::mutex::scoped_lock vlock(m_recipientmutex);
    if (r == NULL) {
        return false;
    }
    m_dataRecipients.push_back(r);
    L_(linfo) << "registered new receiver (ptr=" << (void*)r << ") to receive raw VDIF data blocks.";
    return true;
}

/** Remove a particular recipient, or remove all if argument is NULL */
bool UDPVDIFReceiverSim::unregisterRecipient(DataRecipient* r)
{
    boost::mutex::scoped_lock vlock(m_recipientmutex);
    if (r == NULL) {
        m_dataRecipients.clear();
    } else {
        m_dataRecipients.erase(
            std::remove(m_dataRecipients.begin(), m_dataRecipients.end(), r),
            m_dataRecipients.end()
        );
    }
    return true;
}


/**
 * Start data pump in the background. The VDIF payload data received into buffers
 * will be pushed to the registered recipient(s) in round-robin fashion.
 */
bool UDPVDIFReceiverSim::startRx()
{
    boost::mutex::scoped_lock iolock(m_iomutex);

    if (!m_groups) {
        L_(lerror) << "UDPVDIFReceiverSim::startRx() called but no buffer group allocate()'d yet!";
        return false;
    }

    if (m_rxDatapumpThread != NULL) {
        L_(linfo) << "UDPVDIFReceiverSim::startRx() called while reception already running!";
        return false;
    }

    // Need to determine VDIF UDP/IP properties?
    if (m_vdifdetails.framespersec == 0) {
        L_(linfo) << "UDPVDIFReceiverSim::startRx() will now measure the not yet determined frames/sec rate";
        bool ok = analyze();
        if (!ok) {
            L_(linfo) << "UDPVDIFReceiverSim::startRx() failed to measure frames/sec rate -- no incoming data? stopping...";
            return false;
        }
    }

    // Start thread
    m_overflowcount = 0;
    m_rxDatapumpThread = new boost::thread(&UDPVDIFReceiverSim::rxDatapumpWorker, this);

    return true;
}

/** Stop the data pump thread */
bool UDPVDIFReceiverSim::stopRx()
{
    boost::mutex::scoped_lock iolock(m_iomutex);

    if (m_rxDatapumpThread == NULL) {
        L_(ldebug) << "UDPVDIFReceiverSim::stopRx() called without active data dump!";
        return false;
    }

    L_(ldebug) << "stopRx() interrupting the datapump worker thread...";
    m_rxDatapumpThread->interrupt();
    iolock.unlock();
    m_rxDatapumpThread->join();
    iolock.lock();

    L_(ldebug) << "stopRx() joined datapump worker thread, doing clean-up";
    delete m_rxDatapumpThread;

    m_rxDatapumpThread = NULL;

    int busyslot = m_groups->reserveSlotInv(RawDataBuffer::Free, 0);
    while (busyslot >= 0) {
        m_groups->releaseSlot(busyslot);
        busyslot = m_groups->reserveSlotInv(RawDataBuffer::Free, busyslot);
    }

    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Increment the current [start;stop] interval by m_framesperbuf to determine the next data-capture window.
 * The new start is set to the previous stop +1 frame.
 * The new stop  is set to the new start plus the buffer duration m_framesperbuf.
 */
void UDPVDIFReceiverSim::nextStartStopRange(int32_t& start_sec, int32_t& start_frame, int32_t& stop_sec, int32_t& stop_frame, const size_t add_frames) const
{
    std::stringstream info;
    info << "Capture range : previous=["
         << start_sec << "s/#" << start_frame << ",...,"
         << stop_sec << "s/#" << stop_frame << "]";

    // Determine next start
    start_sec = stop_sec;
    start_frame = stop_frame + 1;
    if (start_frame >= (int32_t)m_vdifdetails.framespersec) {
        start_frame %= m_vdifdetails.framespersec;
        start_sec++;
    }

    // Determine next stop
    stop_sec = start_sec;
    stop_frame = start_frame + add_frames - 1;
    if (stop_frame >= (int32_t)m_vdifdetails.framespersec) {
        stop_frame %= m_vdifdetails.framespersec;
        stop_sec++;
    }

    info << ", next=["
         << start_sec << "s/#" << start_frame << ",...,"
         << stop_sec << "s/#" << stop_frame << "]";
    L_(ldebug) << info.str();
}

/**
 * Worker that receives UDP/IP containing VDIF, strips VDIF headers, and pushes
 * completed buffers into the registered data recipient(s) (round-robin). Takes
 * care to preserve spacing in time appropriate for the integration time (i.e.
 * the buffer size).
 */
void UDPVDIFReceiverSim::rxDatapumpWorker()
{
    int slotnr = 0;
    RawDataBuffer* slot;

    const size_t buf_len = m_groups->utilized();
    const size_t framesperbuf = buf_len / m_vdifdetails.payloadsize;
    const double buf_time = framesperbuf / (double)m_vdifdetails.framespersec;

    int32_t start_at_sec = 0, start_at_frame = 0;
    int32_t stop_at_sec = 0, stop_at_frame = 0; // end marker for current buffer

    L_(linfo) << "Started data pump (simulated) with initially " << m_dataRecipients.size() << " recipients";

    // How many frames we want per buffer
    L_(linfo) << "Data pump buffers will contain " << framesperbuf
              << " frames x " << m_vdifdetails.payloadsize << "-byte payload data, "
              << "corresponding to " << std::fixed << std::setprecision(2) << ((framesperbuf*1e3)/(double)m_vdifdetails.framespersec) << " data-msec";

    // Simulate some start-up / capture start delay
    sleep(1);

    // Produce data blocks in regular intervals
    try {

        start_at_sec = 59000;
        start_at_frame = 0;
        stop_at_sec = start_at_sec + framesperbuf / m_vdifdetails.framespersec;
        stop_at_frame = framesperbuf % m_vdifdetails.framespersec;

        while (1) {

            // Grab next buffer slot that is not used by GPU
            while (1) {

                boost::this_thread::interruption_point();

                int newslot = m_groups->reserveSlot(RawDataBuffer::Free, slotnr);
                if (newslot < 0) {
                    if (1) {
                        // optionally, wait to be woken after a host->GPU transfer completes
                        boost::mutex::scoped_lock iolock(m_iomutex);
                        m_releasedBufCond.wait(iolock);
                    }
                    // No free slot? Try to get one that is at least not 'Transferring'
                    newslot = m_groups->reserveSlotInv(RawDataBuffer::Transferring, slotnr);
                }
                if (newslot < 0) {
                    boost::this_thread::yield();
                    continue;
                }

                slotnr = newslot;
                slot = m_groups->getSlot(slotnr);
                L_(ldebug) << "Reserved slot " << slot << " to receive VDIF into that was " << RawDataBuffer::stateToStr(slot->prevstate);
                break;
            }

            // Sleep for realtime-equivalent duration of the block
            nanosleep_sec(buf_time);

            // Timestamp mid-buffer and other infos
            double t_off = (start_at_frame / (double)m_vdifdetails.framespersec) + (0.5 * framesperbuf) / m_vdifdetails.framespersec;
            slot->tmid.tv_sec = start_at_sec + (time_t)t_off;
            slot->tmid.tv_usec = 1e6*(t_off - floor(t_off));
            slot->weight = 1.0;
            slot->length = buf_len;

            // Mark the current [tstart ; stop] as Full or Overflowed
            m_groups->occupySlot(slotnr);
            if (m_groups->getSlot(slotnr)->state == RawDataBuffer::Overflowed) {
                m_overflowcount++;
            }

            // Push the data to a recipient; the thread marks slot as 'RawDataBuffer::Transferring'
            boost::thread* t = new boost::thread();
            *t = boost::thread(boost::bind(&UDPVDIFReceiverSim::rxDatabufferHandover, this, slotnr, t));
            t->detach();

            L_(ldebug) << "Dispatch simulated slot " << slotnr << " to recipient"
                       << ", nframes " << framesperbuf
                       << ", weight " << std::fixed << std::setprecision(3) << slot->weight
                       << ", midtime " << (slot->tmid.tv_sec + 1e-6*slot->tmid.tv_usec)
                       << ", " << m_overflowcount << " slot overflows accumulated";

            // Determine the end marker of the next buffer
            nextStartStopRange(start_at_sec, start_at_frame, stop_at_sec, stop_at_frame, framesperbuf);

        } //while(receive infinitely)

    } catch (boost::thread_interrupted& e) {
        L_(ldebug) << __func__ << " was interrupted and is exiting";
    }

    L_(linfo) << __func__ << " is exiting";
}

/**
 * Detached thread(s) that handle passing completed data to a recipient.
 */
void UDPVDIFReceiverSim::rxDatabufferHandover(const int slotnr, boost::thread* self)
{
    DataRecipient* recpt = NULL;
    RawDataBuffer* slot = m_groups->getSlot(slotnr);
    int Nrecpt = -1, irecpt = -1;

    // Find a recipient
    lockscope {
        boost::mutex::scoped_lock vlock(m_recipientmutex);
        Nrecpt = m_dataRecipients.size();
        if (Nrecpt > 0) {
            irecpt = slotnr % Nrecpt;
            recpt = m_dataRecipients.at(irecpt);
        }
    }

    // Invoke recipient (hope it wasn't unregistered in the meantime; TODO)
    if (Nrecpt > 0) {
        L_(ldebug2) << "VDIF buffer GPU handover: next recipient " << irecpt << ", transferring slot " << slotnr;
        m_groups->setSlotState(RawDataBuffer::Transferring, slotnr);
        recpt->takeData(slot);
        m_groups->releaseSlot(slotnr);
        m_releasedBufCond.notify_all();
   } else {
        L_(ldebug2) << "VDIF buffer GPU handover: no recipients registered, discarding slot " << slotnr;
        m_groups->setSlotState(RawDataBuffer::Transferring, slotnr);
        // no takeData()
        m_groups->releaseSlot(slotnr);
        m_releasedBufCond.notify_all();
    }


    delete self;
}
