#ifndef SPECTROMETERTCPSERVER_HXX
#define SPECTROMETERTCPSERVER_HXX

#include "network/tcpserver.hxx" // class TCPSimpleServer
#include "datarecipients/commandrecipient.hxx"

class CommandRecipient;

class SpectrometerTCPServer : public TCPSimpleServer
{
    private:
        SpectrometerTCPServer();
    public:
        /** C'stor, link to spectrometer object, listen on port, accept text or binary */
        SpectrometerTCPServer(CommandRecipient&, int, bool binary=false);

    private:
        bool work(boost::asio::ip::tcp::iostream& s);
        void trim(std::string& str, bool safe);

    private:
        CommandRecipient& m_cmdrecipient;
};

#endif

