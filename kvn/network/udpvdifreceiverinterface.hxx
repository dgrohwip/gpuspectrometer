#ifndef UDFVDIFRECEIVERIFACE_HXX
#define UDFVDIFRECEIVERIFACE_HXX

#include "datarecipients/datarecipient.hxx"
#include "network/vdifdetails.hxx"

#include <boost/noncopyable.hpp>

#include <stdlib.h>
#include <sys/time.h>

#define DEFAULT_UDP_TIMEOUT_SEC 5

class DataRecipient;

class UDPVDIFReceiverInterface : private boost::noncopyable {

    public:

        UDPVDIFReceiverInterface(int verbosity=0) { }

        /**
         * Open an UDP socket
         * @param frameoffset byte position in the UDP/IP packet at which the actual VDIF frame starts
         */
        virtual bool open(const int port, const size_t frameoffset = 0, const int timeout = DEFAULT_UDP_TIMEOUT_SEC) = 0;

        /** Close UDP socket */
        virtual bool close() = 0;

        /**
         * Inspect the incoming UDP/IP traffic for a while and determine its properties.
         * Results are stored internally and accessible via getVDIFFramesPerSec() and others.
         * Helpful for deriving values for a later allocate().
         * \return true upon successful determination of frame rate, frame size, bits/sample, num channels per thread
         */
        virtual bool analyze() = 0;

        // Accessors

        /** \return VDIF frames per second */
        virtual size_t getVDIFFramesPerSec() const = 0;

        /** \return VDIF frame size */
        virtual size_t getVDIFFrameSize() const = 0;

        /** \return VDIF payload size */
        virtual size_t getVDIFPayloadSize() const = 0;

        /** \return Number of channels in the VDIF stream (once it has been determined by snoopVDIF()) */
        virtual int getVDIFNchan() const = 0;

        /** \return Number of bits/sample in the VDIF stream (once it has been determined by snoopVDIF()) */
        virtual int getVDIFNbits() const = 0;

        /** \return Object containing VDIF details */
        virtual VDIFDetails getVDIFDetails() const = 0;

        /// Buffer group

        /** Allocate a set of data receive buffers. The buffer size and excludes VDIF headers. */
        virtual bool allocate(const int Nbuffers, const size_t) = 0;

        /** Free up all buffers */
        virtual bool deallocate() = 0;

        /** Get pointer array of raw buffers, inteded for externally calling cudaHostRegister() */
        virtual void** getBufferPtrs(int& Nbuffers, size_t& buflen) = 0;

    public:
        /**
         * Start data pump in the background. The VDIF payload data received into buffers
         * will be pushed to the registered recipient(s) in round-robin fashion.
         */
        virtual bool startRx() = 0;

        /** Stop the data pump thread */
        virtual bool stopRx() = 0;

        /**
         * Add a new recipient that shall receive raw buffers upon completion.
         * Call multiple times to add several recipients for round-robin reception.
         */
        virtual bool registerRecipient(DataRecipient*) = 0;

        /** Remove a particular recipient, or remove all if argument is NULL */
        virtual bool unregisterRecipient(DataRecipient*) = 0;

    public:
        virtual void releaseSlotAsync(int slotnr) = 0;

};

#endif // UDFVDIFRECEIVERIFACE_HXX
