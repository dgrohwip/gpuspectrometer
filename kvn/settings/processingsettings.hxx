#ifndef PROCESSINGSETTINGS_HXX
#define PROCESSINGSETTINGS_HXX

#include "core/defines.hxx" // MAX_SUBBANDS, MAX_GPUS

#include <string>
#include <vector>
#include <cstddef>
#include <ostream>

#include <sys/time.h>

class ProcessingSettings {

    friend std::ostream& operator<<(std::ostream&, const ProcessingSettings&);

    public:

        /** Initialize to default values */
        ProcessingSettings();

        /** \return True if the processing settings look fine */
        bool validate() const;

        /** Recalculate any internal derived settings, call if parameters have been changed */
        void recalculate();

    public:

        /* Time division multiplexing */
        int nGPUs;              // How many GPU to use in parallel
        int nstreams;           // How many CUDA Streams per GPU to create
        int mapGPUs[MAX_GPUS];  // Map the range [0:nGPUs-1] --> [gpu a, gpu b, ..., gpu x]

        /* Spectral processing */
        double Tint_s;         // Integration time in seconds, based on Tint_wish_s
        double dfttime_s;      // Duration of a single DFT in data-time
        int    nfft;           // Number of DFTs per subband that fit into Tint_s
        int    nsubints;       // Number of subintegrations to evenly split 'nfft' into.
        int    nfft_subint;    // Number of DFTs per subintegration
        size_t rawbufsize;     // Total size of VDIF payload in bytes to meet Tint_s
        int    nbuffers;       // Total numer of 'rawbufsize' buffers requested from UDPVDIFReceiver::allocate()
        int    underallocfactor; // What fraction of free memory to use at most (1=100%, 2=50%, etc)

};

std::ostream& operator<<(std::ostream&, const ProcessingSettings&);

#endif // PROCESSINGSETTINGS_HXX
