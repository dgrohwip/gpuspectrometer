#include "settings/spectralsettings.hxx"
#include "kvnmc2api/kvnmc_defs.hxx"
#include "core/defines.hxx"

#include <algorithm>
#include <string>
#include <cassert>
#include <cstring>
#include <iostream>
#include <ostream>
#include <sstream>
#include <iomanip>

/** Initialize to default values */
SpectralSettings::SpectralSettings()
{
    observation = "<none>";
    observer = "<none>";
    telescope = "<none>";

    obsPath = std::string(OBS_ROOT_DIR) + std::string("/noExpt/");

    for (int n=0; n<MAX_SUBBANDS; n++) {
        enableAutocorr[n] = true;
    }
    for (int n=0; n<MAX_SUBBANDS/2; n++) {
        enableCrosscorr[n] = false;
    }

    writespectra = true;
    locallock = false;

    nchan = 128*1024;   // 128k over 512 MHz = 256k-point FFT and 3.90625 kHz/channel
    nchan = 4096;       // KVN DSM default is 4k channels over any bandwidth
    Tint_wish_s = 0.512;

    window_type = WINDOW_FUNCTION_NONE;
    window_overlap = 0.0;

    // derive 'nspectra' and 'nenabledbands'
    recalculate();
}

/** \return True if the spectral settings look fine */
bool SpectralSettings::validate() const
{
    bool ok = true;
    ok = (nchan >= 16);             assert(nchan >= 16);
    ok &= (Tint_wish_s > 10e-3);    assert(Tint_wish_s > 10e-3);
    ok &= (window_type >= WINDOW_FUNCTION_NONE);    assert(window_type >= WINDOW_FUNCTION_NONE);
    ok &= (window_type <= WINDOW_FUNCTION_CUSTOM);  assert(window_type <= WINDOW_FUNCTION_CUSTOM);

    for (int i=0; i<MAX_SUBBANDS/2; i++) {
        if (enableCrosscorr[i]) {
            int b1 = 2*i, b2 = 2*i+1;
            bool cross_members_active = (enableAutocorr[b1] && enableAutocorr[b2]);
            //assert(cross_members_active);
            if (!cross_members_active) {
                std::cout << "CONFIG ERROR: cross-product " << i << " enabled, but corresponding bands " << b1 << ", " << b2 << " not both enabled!\n";
                return false;
            }
        }
    }

    return ok;
}

/** Recalculate any internal derived settings, call if parameters have been changed */
void SpectralSettings::recalculate()
{
    nspectra = 0;
    nenabledbands = 0;
    do_window = (window_type != WINDOW_FUNCTION_NONE);

    for (int i=0; i<MAX_SUBBANDS/2; i++) {
        if (enableCrosscorr[i]) {
            ++nspectra;
        }
    }
    do_cross = (nspectra > 0);

    for (int i=0; i<MAX_SUBBANDS; i++) {
        if (enableAutocorr[i]) {
            ++nspectra;
            ++nenabledbands;
        }
    }
}

int SpectralSettings::windowType(std::string w)
{
    std::transform(w.begin(), w.end(),w.begin(), ::toupper);
    if ((w == "BOXCAR") || (w == "NONE")) {
        return WINDOW_FUNCTION_NONE;
    } else if (w == "HANN") {
        return WINDOW_FUNCTION_HANN;
    } else if (w == "HAMMING") {
        return WINDOW_FUNCTION_HAMMING;
    } else if (w == "HFT248D") {
        return WINDOW_FUNCTION_HFT248D;
    } else if (w == "CUSTOM") {
        return WINDOW_FUNCTION_CUSTOM;
    } else {
        return WINDOW_FUNCTION_NONE;
    }
}

std::string SpectralSettings::windowType(int w)
{
    if (w == WINDOW_FUNCTION_NONE) {
        return "NONE";
    } else if (w == WINDOW_FUNCTION_HANN) {
        return "HANN";
    } else if (w == WINDOW_FUNCTION_HAMMING) {
        return "HAMMING";
    } else if (w == WINDOW_FUNCTION_HFT248D) {
        return "HFT248D";
    } else if (w == WINDOW_FUNCTION_CUSTOM) {
        return "CUSTOM";
    } else {
        return "NONE";
    }
}

std::string SpectralSettings::strEnabledStreams() const
{
    const int Nmaxcross = sizeof(enableCrosscorr)/sizeof(bool);

    std::stringstream ss;
    for (int n=0; n<Nmaxcross; n++) {
        ss << "AUTO" << (2*n+1) << ":" << (enableAutocorr[2*n] ? "ON" : "OFF") << ",";
        ss << "AUTO" << (2*n+2) << ":" << (enableAutocorr[2*n+1] ? "ON" : "OFF") << ",";
        ss << "CROSS" << (2*n+1) << (2*n+2) << ":" << (enableCrosscorr[n] ? "ON" : "OFF");
        if (n != (Nmaxcross-1)) {
            ss << ",";
        }
    }
    return ss.str();
}


std::ostream& operator<<(std::ostream& os, const SpectralSettings& s)
{
    const int Nmaxchan = sizeof(s.enableAutocorr)/sizeof(bool);
    const int Nmaxcross = sizeof(s.enableCrosscorr)/sizeof(bool);

    os << "Settings: observation '" << s.observation << "', path '" << s.obsPath << "'\n";
    os << "Settings: Tint=" << std::setprecision(3) << std::fixed << s.Tint_wish_s << "s requested, "
        << s.nchan << " channels, window " << s.windowType(s.window_type) << ", "
        << s.nenabledbands << " selected bands, "
        << s.nspectra << " spectra per AP\n";
    os << "Settings: auto spectra mask ";
    for (int n=0; n<Nmaxchan; n++) {
        os << " ac[" << (n+1) << "]:" << (s.enableAutocorr[n] ? "on" : "off");
    }
    os << "\nSettings: cross spectra mask";
    for (int n=0; n<Nmaxcross; n++) {
        os << " xc[" << (n+1) << "]:" << (s.enableCrosscorr[n] ? "on" : "off");
    }
    os << "\n";

    return os;
}

