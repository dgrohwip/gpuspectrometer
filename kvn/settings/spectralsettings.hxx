#ifndef SPECTRALSETTINGS_HXX
#define SPECTRALSETTINGS_HXX

#include "core/defines.hxx" // MAX_SUBBANDS, MAX_GPUS

#include <string>
#include <vector>
#include <cstddef>
#include <ostream>

#include <sys/time.h>

class SpectralSettings {

    friend std::ostream& operator<<(std::ostream&, const SpectralSettings&);

    public:

        /** Initialize to default values */
        SpectralSettings();

        /** \return True if the spectral settings look fine */
        bool validate() const;

        /** Recalculate any internal derived settings, call if parameters have been changed */
        void recalculate();

        /** Return streams as a list */
        std::string strEnabledStreams() const;

    public:

        /* Spectral configuration */
        //double Tint_s;         // Integration time in seconds
        double Tint_wish_s;    // Originally requested integration time in seconds, may not fit framing & data rate
        int    nchan;          // Number of output channels (r2c FFT length is <in>=2*nchan, <out>=nchan+1)
        bool   do_cross;       // Form cross-power spectra in addition to power spectra (general setting over enableCrosscorr[])
        bool   do_window;      // Apply window function prior to FFT?
        int    window_type;    // Window function (WINDOW_FUNCTION_NONE, WINDOW_FUNCTION_HANN, WINDOW_FUNCTION_HAMMING, WINDOW_FUNCTION_HFT248D)
        float  window_overlap; // Amount of overlap between FFTs in percent : TODO
        //bool   use_PFB;        // Enable PFB processing (replaces FFT)?

        /* KVN DSM specific */
        std::string observation; // OBNM command, arg <obsname>
        std::string obsPath;     // derived from OBNM command
        std::string configfile;  // CFGF command, arg /home/DAS_LOC/RESOURCE/CFG/<something>
        //std::string pfbcoeffile; // FCDL command, arg /home/DAS_LOC/RESOURCE/COEF/<something>
        bool writespectra;       // ACQD command, arg SAVE | NOSAVE
        bool locallock;          // LGUI command, arg LOCK | UNLOCK, not meaningful for software spectrometer at the moment

        bool enableAutocorr[MAX_SUBBANDS];      // enableAutocorr[n]=true/false, calculate autocorr for n:th subband
        bool enableCrosscorr[MAX_SUBBANDS/2];   // enableCrosscorr[n]=true/false, calculate cross-corr between n:th and n+1:th subband
        int  nspectra;                          // Total number of averaged spectra to be produced, including cross-power if enabled
        int  nenabledbands;                     // Total number of bands with enableAutocorr[n]==True

        /* Extra */
        std::string observer;
        std::string telescope;

   public:
        static int windowType(std::string w);
        static std::string windowType(int w);
};

std::ostream& operator<<(std::ostream&, const SpectralSettings&);

#endif // SPECTRALSETTINGS_HXX
