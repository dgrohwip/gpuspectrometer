#!/usr/bin/python
#
# Similar to KVN Field System helper 'rcv_py' but without KVN FS bindings.
#

import os, numpy, socket, struct, sys, time
#import Objects.Backend as Backend
#import Objects.Tcp as Tcp
import Tcp as Tcp
import dsmfile

DSM_HEADER_SIZE = dsmfile.HEADER_LENGTH
DSM_NPOINT = dsmfile.NCHANNEL
DSM_DATA_SIZE = DSM_NPOINT*4

DSM_DATAPATH = 'dummy' # os.environ['KCS_DATA']+'/%s/'%(backend)
data_addr = ('127.0.0.1',60450) # '192.168.1.30'
verbose = False
dsm_plot_data = numpy.zeros(DSM_NPOINT)
dsm_points = numpy.arange(DSM_NPOINT)

dsm_header = dsmfile.HeaderSection()
cl_dsmdata = Tcp.Client(timeout = 2.0)
cl_dsmdata.connect(data_addr)
_tcp_client = cl_dsmdata

cnt = 0
record = -1
nrec = 0
t_offset = 0. # time offset of DAS_LOC system clock

_bvanes = []
_bskys = []
_bsrcs = []
_bscans = []
_scannums = []
_iareas = []

_is_main = True
_is_hold = True
_is_main_old = _is_main
_is_hold_old = _is_hold

dsm_channels = numpy.arange(DSM_NPOINT)
dsm_channels_crop = 512

do_plot = True
plot_peak_y = [0]*16

if do_plot:
	import matplotlib.pyplot as plt

def checkDIO():
	global _is_main, _is_main_old, _is_hold, _is_hold_old
	_is_main_old = _is_main
	_is_hold_old = _is_hold
	return

def initRecording(dsm):
	dataStacks = []
	_vfc._acc_buf = []
	_vfc._acc_flag = True
	for stream_num,s in enumerate(dsm_header.OutputStreams):
		dataStacks.append('')
	recordstatus = record

def stopRecording(dsm):
	import numpy
	_tpd_buf = numpy.array(_vfc._acc_buf).copy()
	_vfc._acc_buf = []
	_vfc._acc_flag = False
	for stream_num,s in enumerate(dsm_header.OutputStreams):
		databuffer.put_stream(stream_num, dataStacks[stream_num])
	dataStacks = []
	record = -1
	nrec = 0
	recordstatus = record
	seqnum_closed = seq_num
	print '.\n'

def doRecording(dsm):
	import sys
	def putStream(dsm):
		import numpy
		_tpd_buf = numpy.array(_vfc._acc_buf).copy()
		for stream_num,s in enumerate(dsm_header.OutputStreams):
			databuffer.put_stream(stream_num, dataStacks[stream_num])
		dataStacks = []
		print 'v',
		sys.stdout.flush()

	def addNewStream(dsm):
		_vfc._acc_buf = []
		for stream_num,s in enumerate(dsm_header.OutputStreams):
			dataStacks.append(dsm_dbufList[stream_num])
		nrec += 1

	def addStream(dsm):
		global _is_hold
		for stream_num,s in enumerate(dsm_header.OutputStreams):
			dataStacks[stream_num] += dsm_dbufList[stream_num]
		nrec += 1
		if nrec%10 == 1:
			print '.',
			sys.stdout.flush()

	if record > 0 :
		if not _is_hold:
			addStream(dsm)
#			for stream_num,s in enumerate(dsm_header.OutputStreams):
#				dataStacks[stream_num] += dsm_dbufList[stream_num]
#			nrec += 1
#		if nrec%10 == 0: 
#			print '.',
#			sys.stdout.flush()
#
	elif record == 0: # 'Otf Mode'
		if _is_main_old and not _is_main: # Main -> Ref
			print 'Main->Ref'
			mode = 'Main'
			putStream(dsm)
			addNewStream(dsm)
		elif _is_main_old and _is_main: # Main
			addStream(dsm)
#			if nrec%100 == 0:
#				putStream(dsm)
#				addNewStream(dsm)
#			else:
#				addStream(dsm)
		elif not _is_main_old and _is_main: # Ref -> Main
			print 'Ref->Main'
			mode = 'Ref'
			putStream(dsm)
			addNewStream(dsm)
		else:	# Ref
			if not _is_hold:
				addStream(dsm)

def rcvDSMHeader():
	_hbuf = '' 
	rheader_size = 0
	global DSM_HEADER_SIZE
	import socket, time

	while 1: #check_continue():
		try: 
			_hbuf += _tcp_client.recv(size = DSM_HEADER_SIZE-rheader_size)
			rheader_size = len(_hbuf)
			if rheader_size == DSM_HEADER_SIZE:	
				dsm_header.unpack(_hbuf) 
				return True
		except socket.timeout:
			time.sleep(0.05)
			continue
	else:
		return False

def rcvDSMData():
	dsm_dbufList = []
	global DSM_DATA_SIZE, dsm_channels, dsm_channels_crop, DSM_NPOINT, do_plot, plot_peak_y
	import socket,time,numpy

	for strm_num, strm_name in enumerate(dsm_header.OutputStreams):
		dbuf = ''
		rdata_size = 0
		while 1: #check_continue():
			try:
				dbuf += _tcp_client.recv(size = DSM_DATA_SIZE-rdata_size)
				rdata_size = len(dbuf)
				if rdata_size == DSM_DATA_SIZE:	
					dsm_dbufList.append(dsm_header.hbuf+dbuf)
					print ('Got data for %s, len %d byte, now total %d entries' % (strm_name, rdata_size, len(dsm_dbufList)))
					break
			except socket.timeout:
				time.sleep(0.05)
				continue
		else:
			return False

	# Plotting
	i = 0
	creal = numpy.zeros(DSM_NPOINT)
	cimag = numpy.zeros(DSM_NPOINT)
	T_SZ = DSM_DATA_SIZE + len(dsm_header.hbuf)
	N_SQ = numpy.ceil(numpy.sqrt(len(dsm_header.OutputStreams)))
	tstr = time.strftime('%Yy%jd%Hh%Mm%Ss', dsm_header.getStartTime())
	if do_plot:
		plt.clf()
		plt.ion()
	for strm_num, strm_name in enumerate(dsm_header.OutputStreams):
		dd = dsm_dbufList[i][len(dsm_header.hbuf):]
		if (strm_name[:4] == 'Auto'):
			ndd = numpy.fromstring(dd, dtype='float32')
		else:
			# 'CR12' for Real, 'CI12' for Imag, but: how to merge?
			if (strm_name[:2] == 'CR'):
				creal = numpy.fromstring(dd, dtype='float32')
				ndd = creal
			elif (strm_name[:2] == 'CI'):
				cimag = numpy.fromstring(dd, dtype='float32')
				ndd = cimag
			#ndd = numpy.abs(creal + 1j*cimag)  # Mag spectrum
		print ('Spec %s has %d bytes, %d values, x has %d points' % (strm_name, len(dd), len(ndd), len(dsm_points)))
		#print ('Peak in bin %d, value %f' % (numpy.argmax(ndd),numpy.max(ndd)))
		if not(do_plot):
			print ndd[:15], '...'
			continue
		plot_peak_y[i] = max([plot_peak_y[i], numpy.max(numpy.abs(ndd))])
		plt.subplot('%d%d%d' % (N_SQ,N_SQ,i+1))
		plt.plot(dsm_channels, ndd, 'kx')
		plt.axis('tight')
		if  (strm_name[:4] == 'Auto'):
			plt.ylim([0, 1.1*plot_peak_y[i]])
		else:
			plt.ylim([-1.1*plot_peak_y[i], 1.1*plot_peak_y[i]])
		if i==0:
			plt.title(strm_name + ' ' + tstr)
		else:
			plt.title(strm_name)
		i = i + 1
	if do_plot:
		#plt.show()
		plt.draw()
		plt.pause(0.05)
	return True

def dsmQLook():
	import numpy
	global DSM_HEADER_SIZE, dsm_points, dsm_plot_data

	for strm_num, strm_name in enumerate(dsm_header.OutputStreams):
		if not strm_name in qlook_stream: continue
		dbuf = dsm_dbufList[strm_num]
		_data = numpy.fromstring(dbuf[DSM_HEADER_SIZE:],dtype=numpy.float32)
		for i in dsm_points:
			dsm_plot_data[i] = _data[i]
		if strm_num < len(qlooks):
			qlook = qlooks[strm_num]
			s = 'Num=%05d Mode=%s IPNum=%06d Stream=%s'%(getNum(), mode, dsm_header.IPNum,strm_name)
#			qlook('set term x11 title "DSM QLook%d : %s"'%(strm_num+1,strm_name))
			qlook.title(s)
			qlook.plot(dsm_plot_data)
			del s, _data
	
niter = 0
while 1: #check_continue():
#	try:
	if True:
		# Receive DSM Header Part

		#qlook_flag = qlook_flag
		#qlook_period = qlook_period

		if not rcvDSMHeader(): break
		if not rcvDSMData(): break

		checkDIO()

		cnt += 1
		ipnum = dsm_header.IPNum
		iplen = dsm_header.IPLength
		#qlook_timing_flag = (ipnum%(int(1024*qlook_period/iplen))==0)
		uttime = time.mktime(dsm_header.StartTime)+float(iplen*ipnum)/1000
		t1 = uttime+9*3600+t_offset

		# Quik Look
		#if qlook_flag and qlook_timing_flag:
		#	dsmQLook()

		# Data Recording to data buffer
		if record >= 0: 
			if nrec == 0: # When start recording command issued
#				if t1 <= rec_t : 
#					continue # Wait
				if verbose:
					print '\nStart Time : ', rec_t, t1
				initRecording()

			doRecording()
				
			if verbose and nrec%10 == 0:
				print '\tRecord, NRec : ', record, nrec
				print '\tRecord Time : ', t1

			if record == nrec: # End of recording
				stopRecording()
				if verbose: print 'Stop Time : ',rec_t, t1

		elif record < 0: 
			if nrec > 0: # When stop-recording command is issued
#				if t1+float(iplen)/1000 < rec_t : # Waiting for End Time
				if False:
					doRecording()
					continue
				else: 
					stopRecording()
					if verbose: print 'Stop Time : ', rec_t, t1
	else:
		time.sleep(0.05)
		sys.stdout.flush()
		continue

_tcp_client.close()

