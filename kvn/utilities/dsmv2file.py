import struct
import numpy
import time
'''
Wrapper for DSMv2 file format
'''

'''
Currently no documentation, just from FormatDSMv2.hxx code:

  py fld c type      c name
  H      uint16_t    header_length;       // Length of entire header in bytes, includes this field

  I    0   uint32_t    sequence_number;     // Begin at 0, increment +1 in every Integration Period (IP), max IP is 10,000-1 (then new file)
  8B   1   uint8_t     start_time[7];       // Time at acquistion start (not middle of interval); YYYYDDDhhmmss0 with lowest 4 bit of last byte always 0
       8   char        _padding1;

  I    9   uint32_t    bandwidth;           // Bandwidth of the IF channel(s) in hertz, identical to the full bandwidth covered by 'spectral_channels'
  I   10   uint32_t    output_streams;      // Bitmask with bits for presence of 16 auto and 8 cross spectra
  I   11   uint32_t    valid_samples;       // Count of valid samples in each spectrum (identical in all spectra)
  I   12   uint32_t    invalid_samples;     // Count of lost/missing/invalid samples in each spectrum (identical in all spectra)

  1B  13  char        integration_length;  // 1: 10.24ms, 2: 51.2ms, 3:102.4ms, 4:512ms, 5:1024ms, others undefined
  1B  14  char        _padding2;

  f   15   float       actual_integration;  // Actual integration time <msec> as constrained by VDIF and DFT lengths
  I   16   uint32_t    spectral_channels;   // Number of spectral output channels in original DFT
  I   17   uint32_t    start_channel;       // First channel of spectral window taken from original DFT            ; default: 0
  I   18   uint32_t    stop_channel;        // Last channel (inclusive) of spectral window taken from original DFT ; default: spectral_channels-1
  64B 19   uint8_t     sampler_stats[16][4];// Bit distributions in 16 IFs in percentages for 0b00, 0b01, 0b10, 0b11 2-bit sample states

  Length 112 byte.

'''

HEADER_LEN_FORMAT = '<H'
HEADER_FORMAT = 'I8BIIII1B1BfIII64B'
HEADER_LENGTHFIELD_LENGTH = 2
HEADER_LENGTH = 112

								# C format, Python struct format
FLD_IP_NUMBER = 0			# unsigned int[1], I
FLD_START_TIME = 1				# char[7], 7B
FLD_INPUT_MODE = 8				# char[1], B
FLD_OUTPUT_STREAMS = 9			# unsigned int[1], I
								# refer to OUTPUT_STREAMS_INFO
FLD_VALID_SAMPLES = 10			# unsigned int[8], 4Q
FLD_INVALID_SAMPLES = 18		# unsigned int[9], 4Q
FLD_IP_LENGTH  = 26 	# char[1], B
								# 1:10.24, 2:51.2, 3:102.4, 4:512, 5:1024 ms
								# refer to IP_LENGTH_INFO
FLD_OUTPUT_MODE = 27			# char[1], B
								# 1:Channel Select Mode, 2:Channel Bind Mode
								# refer to OUTPUT_MODE_INFO
FLD_START_CHANNEL_INDEX = 28	# 16 bit short[1], h

FLDLEN_START_TIME = 7			
FLDLEN_VALID_SAMPLES = 8		
FLDLEN_INVALID_SAMPLES = 8		

MAX_NR_STREAM = 16 + 2*8

OUTPUT_STREAMS_INFO = [
	'Auto1','Auto2','CR12','CI12',
	'Auto3','Auto4','CR34','CI34',
	'Auto5','Auto6','CR56','CI56',
	'Auto7','Auto8','CR78','CI78',
	'Auto9','Auto10','CR910','CI910',
	'Auto11','Auto12','CR1112','CI1112',
	'Auto13','Auto14','CR1314','CI1314',
	'Auto15','Auto16','CR1516','CI71516']

IP_LENGTH_INFO = [10.24, 51.2, 102.4, 512, 1024] # in ms

DATA_TYPE = numpy.float32
DATA_LENGTH = 4

class HeaderSection:
	def __init__(self):
		self.h = None
		self.HeaderLen = None
		self.IPNum = None
		self.StartTime = None
		self.Bandwidth = None
		self.OutputStreams = []
		self.ValidSamples = None
		self.InvalidSamples = None
		self.IPLength = None
		self.ActualIPLength = None
		self.NumChannelsDFT = None
		self.StartChannelIndex = None
		self.StopChannelIndex = None

	def read(self,repository):
		''' read header section on the repository place.
		The type of repository is either file or string type.
		For file type, the file position must be set to the start position of header section in advance. 
		'''
		if isinstance(repository,file):
			fp = repository
			lenbuf = fp.read(HEADER_LENGTHFIELD_LENGTH)
			self.HeaderLen = struct.unpack(HEADER_LEN_FORMAT, lenbuf)[0]
			buf = fp.read(self.HeaderLen-HEADER_LENGTHFIELD_LENGTH)
		else:
			self.HeaderLen = struct.unpack(HEADER_LEN_FORMAT, repository[:2])[0]
			buf = repository[HEADER_LENGTHFIELD_LENGTH:self.HeaderLen]
		self.hbuf = buf
		self.unpack(self.hbuf)

	def unpack(self,buf):
		ssize = struct.calcsize(HEADER_FORMAT)
		# print 'Format expected len:', ssize, '  Header len:', len(buf)
		self.h = struct.unpack("<" + HEADER_FORMAT, buf)
		shex = ":".join("{:02x}".format(ord(c)) for c in self.hbuf)
		# print 'Raw header:', shex
		# print 'Unpacked:', self.h
		h = self.h

		self.IPNum, self.Bandwidth, self.ValidSamples, self.InvalidSamples = h[0], h[9], h[11], h[12]
		self.ActualIPLength, self.NumChannelsDFT, self.StartChannelIndex, self.StopChannelIndex = h[15:19]

		self.StartTime = self.getStartTime()
		self.OutputStreams = self.getOutputStreams()
		self.IPLength = self.getIPLength()

		self.uttime = time.mktime(self.StartTime)\
			+float(self.IPLength*self.IPNum)/1000
		## self.show()

	def getIPLength(self):
		#print 'getIPLength, self.h=', self.h, ' value=', (int(self.h[13])-1)
		i = int(self.h[13])-1
		if (i >= len(IP_LENGTH_INFO)):
			return 1.00
		return IP_LENGTH_INFO[int(self.h[13])-1]

	def getStartTime(self):
		import time
		st = []
		mask = 0x0f
		bcd = self.h[1:8]
		for i in range(len(bcd)):
			t = (self.h[FLD_START_TIME+i]>>4) & mask
			if t > 0x09: t -= 0x0a
			st.append(t)
			t = (self.h[FLD_START_TIME+i]) & mask
			if t > 0x09: t -= 0x0a
			st.append(t)

		yr = st[0]*1000+st[1]*100+st[2]*10+st[3]
		day = st[4]*100+st[5]*10+st[6]
		hour = st[7]*10+st[8]
		min = st[9]*10+st[10]
		sec = st[11]*10+st[12]

		tfmt = "%Y %j %H %M %S"
		tstr = "%d %d %d %d %d"%(yr,day,hour,min,sec)

		return time.strptime(tstr,tfmt)

	def getOutputStreams(self):
		outputstreams = []
		ostm = self.h[10]
		mask = 0x01
		for i in range(16 + 16):
			if (ostm >> i) & mask:
				outputstreams.append(OUTPUT_STREAMS_INFO[i])
		return outputstreams

	def getChannelRangeLength(self):
		return abs(self.StopChannelIndex - self.StartChannelIndex) + 1

	def show(self):
		if not self.StartTime: 
			print "No header section read."
			return
		print "Sequence Number     :", self.IPNum
		print "Start Time          :", self.StartTime
		print "Bandwidth (Hz)      :", self.Bandwidth
		print "Output Streams      :", self.OutputStreams
		print "Valid Samples       :", self.ValidSamples
		print "Invalid Samples     :", self.InvalidSamples
		print "Integration Length  :", self.IPLength
		print "Integration Actual  :", self.ActualIPLength
		print "Channels from DF    :", self.NumChannelsDFT
		print "Start Channel Index :", self.StartChannelIndex
		print "Stop Channel Index  :", self.StopChannelIndex

class DSMHeader(HeaderSection):
	def __init__(self):
		HeaderSection.__init__(self)
		self.IPNum = []
		self.ValidSamples = []
		self.InvalidSamples = []

	def append(self,header_section):
		self.IPNum.append(header_section.IPNum)
		self.ValidSamples.append(header_section.ValidSamples)
		self.InvalidSamples.append(header_section.InvalidSamples)
		self.StartTime = header_section.StartTime
		self.Bandwidth = header_section.Bandwidth
		self.OutputStreams = header_section.OutputStreams
		self.IPLength = header_section.IPLength
		self.ActualIPLength = header_section.ActualIPLength
		self.NumChannelsDFT = header_section.NumChannelsDFT
		self.StartChannelIndex = header_section.StartChannelIndex
		self.StopChannelIndex = header_section.StopChannelIndex

class DSMData:
	def __init__(self):
		self.header_section = HeaderSection()
		self.header = DSMHeader()
		self.streams = None
		self.fp = None
		self.buf = None
		self.file_size = 0
		self.stream_size = 0
		self.nstreams = 0
		self.num_ip = 0

	def clear(self):
		self.fp.close()
		self.__init__()
	
	def isOpen(self):
		if self.fp == None: return False
		else: return not self.fp.closed

	def open(self,file):
		'''open DSM file and read the first header section to extract 
		StartTime, InputMode, OutputStreams, IPLength, OutputMode, and StartChannelIndex'''

		fp = open(file,'r')
		fp.seek(0,2)
		file_size = fp.tell() 
		fp.seek(0)

		if (file_size < HEADER_LENGTH):
			raise IOError

		self.header_section.read(fp)
		self.header_section.show()
		self.fp = fp
		self.file_size = file_size

		outputstreams = self.header_section.OutputStreams
		self.nstreams = len(outputstreams)
		self.stream_size = self.header_section.HeaderLen
		self.stream_size += 4 * self.nstreams * self.header_section.getChannelRangeLength()
		print 'Stream size: ', self.stream_size
		self.num_ip = file_size / self.stream_size

	def getNStreams(self): return self.nstreams
	def getSequenceSize(self): return self.stream_size
	def getNumIP(self): return self.num_ip
	def getFileSize(self): return self.file_size
	def getFileName(self): return self.fp.name

	def getStartTime(self): return self.header_section.StartTime
	def getBandwidth(self): return self.header_section.Bandwidth
	def getOutputStreams(self): return self.header_section.OutputStreams
	def getIPLength(self): return self.header_section.IPLength
	def getActualIPLength(self): return self.header_section.ActualIPLength
	def getNumChannelsDFT(self): return self.header_section.NumChannelsDFT
	def getStartChannelIndex(self): return self.header_section.StartChannelIndex
	def getStopChannelIndex(self): return self.header_section.StopChannelIndex

	def show(self):
		if not self.fp:
			print "No DSM file opened"
			return
		print "# File Information"
		print "File Name           :", self.getFileName()
		print "File Size           :", self.getFileSize()
		print "File Num of IP      :", self.getNumIP()
		print "File NStreams       :", self.getNStreams()
		print "# Start DSM Header"
		self.header.show()

	def showHeaderSection(self):
		if not self.fp:
			print "No DSM file opened"
			return
		print "# File Information"
		print "File Name           :", self.getFileName()
		print "File Size           :", self.getFileSize()
		print "File Num of IP      :", self.getNumIP()
		print "File NStreams       :", self.getNStreams()
		print "# Start Header Section"
		self.header_section.show()

	def read(self,isequence):
		'''Return header section and data stream in a given sequence number.
		'''
		if isequence > self.num_ip:
			raise IOError

		offset = self.stream_size*isequence
		self.fp.seek(offset)
		header_section = HeaderSection()
		buf = self.fp.read(self.stream_size)
		header_section.read(buf)
		spectraldatastr = buf[header_section.HeaderLen:]

		_streams = numpy.fromstring(spectraldatastr,dtype=DATA_TYPE)
		streams = numpy.resize(_streams,(self.nstreams, header_section.getChannelRangeLength()))

		return header_section, streams

	def readHeader(self):
		'''Read all header sections in the DSM file.
		'''
		fp = self.fp
		header_section = HeaderSection()
		for isequence in range(self.num_ip):
			offset = self.stream_size*isequence
			fp.seek(offset)
			header_section.read(fp)
			self.header.append(header_section)
	
	def readChannel(self,*channels):
		'''Read all data of given IF channel name or IF channel number in DSM file.
		'''
		ichs = []
		output_streams = []
#		streams = numarray.zeros((self.num_ip,len(channels), NCHANNEL),DATA_TYPE)
		streams = numpy.zeros((self.num_ip,len(channels), NCHANNEL),DATA_TYPE)
		header = DSMHeader()

		for ch in channels:
			if isinstance(ch, int):
				if ch < self.nstreams:
					ichs.append(ch)
				else: 
					raise IOError
			elif ch in OUTPUT_STREAMS_INFO:
				ichs.append(OUTPUT_STREAMS_INFO.index(ch))
			else:
				raise IOError
			print ch
			output_streams.append(ichs[-1])

		for isequence in range(self.num_ip):
			h,s = self.read(isequence)
			header.append(h)
			for i, ich in enumerate(ichs):
				streams[isequence][i]=s[ich].copy()

		header.OutputStreams = output_streams

#		return header, streams
		return header, streams
				
	def readChannelAvg(self,*channels):
		'''Read all data of given IF channel name or IF channel number in DSM file.
		'''
		ichs = []
		output_streams = []
#		streams = numarray.zeros((len(channels), NCHANNEL),DATA_TYPE)
		streams = numpy.zeros((len(channels), NCHANNEL),DATA_TYPE)
		header = DSMHeader()

		for ch in channels:
			if isinstance(ch, int):
				if ch < self.nstreams:
					ichs.append(ch)
				else: 
					raise IOError
			elif ch in OUTPUT_STREAMS_INFO:
				ichs.append(OUTPUT_STREAMS_INFO.index(ch))
			else:
				raise IOError
			print ch
			output_streams.append(ichs[-1])

		for isequence in range(self.num_ip):
			h,s = self.read(isequence)
			header.append(h)
			for i, ich in enumerate(ichs):
				streams[i] += s[ich]

		header.OutputStreams = output_streams

		return header, streams

class StreamHeader(HeaderSection):
	def __init__(self):
		HeaderSection.__init__(self)
		self.StreamNum = -1
	
	def read(self,buf,i=0):
		''' read header section on the repository place.
		The type of repository is either file or string type.
		For file type, the file position must be set to the start position of header section in advance. 
		'''
		hlen = HEADER_LENGTH
		if i >= 0:
			self.hbuf = buf[hlen*i:hlen*(i+1)]
		else:
			size = len(buf)
			self.hbuf = buf[size+hlen*i:size+hlen*(i+1)]
			
		self.unpack(self.hbuf)
		
	def unpack(self,buf):
		self.hbuf = buf[:HEADER_LENGTHFIELD_LENGTH]
		self.h = struct.unpack(HEADER_FORMAT, self.hbuf)
		h = self.h
		self.IPNum = h[FLD_IP_NUMBER]
		self.StartTime = self.getStartTime()
		self.OutputStreams = self.getOutputStreams()
		self.ValidSamples = self.getValidSamples()
		self.InvalidSamples = self.getInvalidSamples()
		self.IPLength = self.getIPLength()
		self.OutputMode = self.getOutputMode()
		self.StartChannelIndex = h[FLD_START_CHANNEL_INDEX]
		self.uttime = time.mktime(self.StartTime)\
			+float(self.IPLength*self.IPNum)/1000

	'''
	def show(self):
		if not self.StartTime: 
			print "No header section read."
			return
		print "Integration Period Number:", self.IPNum
		print "Start Time (in UT)       :", self.StartTime
		print "Data Time                :", self.uttime
		print "Input Mode               :", self.InputMode
		print "Output Streams           :", self.OutputStreams
		print "Valid Samples            :", self.ValidSamples
		print "Invalid Samples          :", self.InvalidSamples
		print "Integration Length       :", self.IPLength
		print "Output Mode              :", self.OutputMode
		print "Start Channel Index      :", self.StartChannelIndex
	'''

class DSMStream(DSMData):
	def __init__(self):
		self.header = StreamHeader()
		self.streams = None
		self.fp = None
		self.buf = None
		self.file_size = 0
		self.stream_size = STREAM_SIZE
		self.num_ip = 0

	def open(self,file):
		'''open DSM file and read the first header section to extract 
		StartTime, InputMode, OutputStreams, IPLength, OutputMode, and StartChannelIndex'''

		fp = open(file,'r')
		fp.seek(0,2)
		file_size = fp.tell() 
		fp.seek(0)

		if file_size < STREAM_SIZE:
			raise IOError

		buf = fp.read()
		self.header.read(buf,i=0)
		self.fp = fp
		self.file_size = file_size

		outputstreams = self.header.OutputStreams
		self.stream_size = STREAM_SIZE
		self.num_ip = file_size/self.stream_size

	def show(self):
		if not self.fp:
			print "No DSM file opened"
			return
		print "# File Information"
		print "File Name                 :", self.getFileName()
		print "File Size                 :", self.getFileSize()
		print "Num of Integraiton Period :", self.getNumIP()
		print "# Start Header Section"
		self.header.show()

	def read(self,ipnum=-1):
		'''Return header section and data stream in a given sequence number.
		'''
		if ipnum > self.num_ip:
			raise IOError
		elif ipnum >= 0 :
			offset = self.stream_size*ipnum
		elif ipnum < 0:
			offset = self.stream_size*(self.num_ip+ipnum)
		self.fp.seek(offset)
		header = StreamHeader()
		buf = self.fp.read(self.stream_size)
		header.read(buf,i=0)
		data = numpy.fromstring(buf[HEADER_LENGTH+2:],dtype=DATA_TYPE)
		return header, data

	def readBuf(self,buf):
		header = StreamHeader()
		streams = []
		num_ip = len(buf)/self.self.stream_size
		header.read(buf,i=0)
		for seq in range(num_ip):
			offset = self.stream_size*seq
			_i0 = offset + HEADER_LENGTH
			_i1 = offset + self.stream_size
			_stream = numpy.fromstring(buf[_i0:_i1],dtype=DATA_TYPE)
			streams.append(_stream)
		return header, numpy.array(streams)
