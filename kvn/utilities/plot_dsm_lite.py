#!/usr/bin/python
'''
Usage: plot_dsm_lite.py <spectral file> [<stream_nr>|<stream_name>]

  stream_nr    stream number to plot, 1..N
  stream_name  stream name to plot such as auto1, auto2, cr12, cr34
'''
import sys
from dsmv2file import DSMData

try:
    import gnuplotlib as gp
    gotGnuplot = True
except ImportError as e:
    gotGnuplot = False

try:
    import matplotlib.pyplot as plt
    gotPyplot = True
except ImportError as e:
    gotPyplot = False

if len(sys.argv) < 2:
    print (__doc__)
    sys.exit(0)

filename = sys.argv[1]
plotstream = 1

dsm = DSMData()
dsm.open(filename)
dsm.readHeader()

# Show the first four headers
for n in range(1,4):
    hdr, data = dsm.read(n)
    hdr.show()

if len(sys.argv)>2:
    if sys.argv[2].isdigit():
        plotstream = int(sys.argv[2])
    else:
        plotstream = [ostr.lower() for ostr in hdr.OutputStreams].index(sys.argv[2].lower()) + 1

# Skip first AP since it might be incomplete
hdr, data = dsm.read(1)

# Start plotting
nstream = len(data)
if (plotstream < 1) or (plotstream > nstream):
        plotstream = 1

print ('----- Stream %d [%s] with %d spectral channels' % (plotstream, str(hdr.OutputStreams[plotstream-1]), len(data[plotstream-1])))

bw = hdr.Bandwidth
N = len(data[plotstream-1])
binamps = data[plotstream-1]
binfreqs = [1e-6*bw*(n/float(N)) for n in range(0,N)]

if gotPyplot:
    plt.plot(binfreqs, binamps)
    plt.xlabel('Channel (MHz)')
    plt.show()
elif gotGnuplot:
    gp.plot(binamps,
        _with    = 'lines',
        terminal = 'dumb 80,40',
        unset    = 'grid',
        xlabel   = 'Channel (bin)',
        xrange   = [0,N-1])

# Also write data into text file
fo = open("m5spec.txt", 'w')
for n in range(len(data[plotstream-1])):
    fo.write('%d %.6f\n' % (n, data[plotstream-1][n]))
