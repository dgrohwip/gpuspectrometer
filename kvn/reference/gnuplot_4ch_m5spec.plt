# VDIF has 4 channels : 8 plots : Auto1 Auto2 Auto3 Auto4 Cross12_Mag Cross12_Phase Cross34_Mag Cross34_phase

### Output: PS file

#set term postscript landscape enhanced color dashed "Helvetica" 14
#set output "gnuplot_4ch_m5spec.ps"

### Auto

set multiplot layout 3,3 title "16sk02a_KVNTN_248175459-8Gbps-4channel"

set title 'Auto1'
plot "16sk02a_KVNTN_248175459-8Gbps-4channel.m5spec" using 1:2 with lines title 'Auto1'

set title 'Auto2'
plot "16sk02a_KVNTN_248175459-8Gbps-4channel.m5spec" using 1:3 with lines title 'Auto2'

set title 'Auto3'
plot "16sk02a_KVNTN_248175459-8Gbps-4channel.m5spec" using 1:4 with lines title 'Auto3'

set title 'Auto4'
plot "16sk02a_KVNTN_248175459-8Gbps-4channel.m5spec" using 1:5 with lines title 'Auto4'

#### Cross

set title 'Cross12 Mag'
plot "16sk02a_KVNTN_248175459-8Gbps-4channel.m5spec" using 1:6 with lines title 'Mag'

set title 'Cross12 Phase'
plot "16sk02a_KVNTN_248175459-8Gbps-4channel.m5spec" using 1:7 with points title 'Phase'

#### Cross

set title 'Cross34 Mag'
plot "16sk02a_KVNTN_248175459-8Gbps-4channel.m5spec" using 1:8 with lines title 'Mag'

set title 'Cross34 Phase'
plot "16sk02a_KVNTN_248175459-8Gbps-4channel.m5spec" using 1:9 with points title 'Phase'

# unset multiplot
pause -1

