
# Location of CUDA installation
CUDA_DIR := /usr/local/cuda-9.0/

SUBDIRS = \
	kvn \
	benchmarking \
	alma \
	kernels/cuda/

INSTALLABLES = \
	alma \
	kvn

.PHONY: all clean $(SUBDIRS) dist

all:
	for dir in $(SUBDIRS); do \
		$(MAKE) -C $$dir all CUDA_DIR=$(CUDA_DIR) MAKEFLAGS=$(MAKEFLAGS); \
	done

clean:
	for dir in $(SUBDIRS); do \
		$(MAKE) -C $$dir clean CUDA_DIR=$(CUDA_DIR) MAKEFLAGS=$(MAKEFLAGS); \
	done

install:
	for dir in $(INSTALLABLES); do \
		$(MAKE) -C $$dir install CUDA_DIR=$(CUDA_DIR) MAKEFLAGS=$(MAKEFLAGS); \
	done
