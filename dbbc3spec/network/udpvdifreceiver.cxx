
#include "network/udpvdifreceiver.hxx"
#include "datarecipients/datarecipient.hxx"
#include "logger.h"

#include <iostream>
#include <iomanip>
#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>

#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/mman.h>

#include <errno.h>
#include <malloc.h>
#include <math.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#define PAGE_SZ 4096

#define lockscope // for notation

/////////////////////////////////////////////////////////////////////////////////////////////

/** Lookup table for VDIF Ref.Epoch -> struct tv::tv_sec */
extern const time_t C_vdif_epoch_to_timeval[63];

/** Return a close power-of-2 to n */
static uint32_t ceil_pow2(uint32_t n);

/** Sleep at most T_msec milliseconds, or shorter if interrupted */
static void nanosleep_msec(double T_msec);

typedef struct VDIF_header_tt {
    int32_t  seconds;
    int32_t  framenr;
    int8_t   refepoch;
    int8_t   log2Nch;
    uint16_t tid;
    char     station[2];
    size_t   framesize;
    size_t   headersize;
    size_t   payloadsize;
} VDIF_header_t;
static void buf2VDIFheader_time(const unsigned char*, VDIF_header_t& h);
static void buf2VDIFheader_aux(const unsigned char*, VDIF_header_t& h);

/////////////////////////////////////////////////////////////////////////////////////////////

UDPVDIFReceiver::UDPVDIFReceiver()
{
    m_sd = -1;
    m_frameoffset = 0;
    m_bufs = NULL;
    m_rxDatapumpThread = NULL;
    m_nchan = 0;
    m_bitspersample = 0;
}

/////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Open an UDP socket
 * @param frameoffset byte position in the UDP/IP packet at which the actual VDIF frame starts
 */
bool UDPVDIFReceiver::open(const int port, const size_t frameoffset)
{
    boost::mutex::scoped_lock iolock(m_bufmutex);
    struct addrinfo  hints;
    struct addrinfo *res = 0;
    struct timeval tv_timeout;
    char port_str[9];
    int yes = 1;

    // Settings
    m_frameoffset = (frameoffset % 128);
    snprintf(port_str, sizeof(port_str), "%d", port);

    // Receiver socket
    memset(&hints,0,sizeof(hints));
    hints.ai_family    = AF_UNSPEC;
    hints.ai_socktype  = SOCK_DGRAM;
    hints.ai_protocol  = 0;
    hints.ai_flags     = AI_PASSIVE | AI_ADDRCONFIG;
    getaddrinfo(NULL, port_str, &hints, &res);
    m_sd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);

    // Receiver socket options
    tv_timeout.tv_sec  = UDP_TIMEOUT_SEC;
    tv_timeout.tv_usec = 0;
    if (setsockopt(m_sd, SOL_SOCKET, SO_RCVTIMEO, (char*)&tv_timeout, sizeof(struct timeval)) < 0) { perror("SO_RCVTIMEO"); }
    if (setsockopt(m_sd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) < 0) { perror("SO_REUSEADDR"); }
    if (setsockopt(m_sd, SOL_SOCKET, SO_NO_CHECK, &yes, sizeof(yes)) < 0) { perror("SO_NO_CHECK"); }

    // Start listening
    if (bind(m_sd, res->ai_addr, res->ai_addrlen) == -1) {
        perror("Failed to bind socket");
        free(res);
        return false;
    }

    free(res);
    return true;
}

/** Close UDP socket */
bool UDPVDIFReceiver::close()
{
    boost::mutex::scoped_lock iolock(m_bufmutex);
    if (m_sd < 0) {
        return false;
    }

    if (m_rxDatapumpThread != NULL) {
        m_rxDatapumpThread->interrupt();
        iolock.unlock();
        m_rxDatapumpThread->join();
        iolock.lock();
        delete m_rxDatapumpThread;
        m_rxDatapumpThread = NULL;
    }

    ::close(m_sd);
    m_sd = -1;

    return true;
}

/**
 * Inspect the incoming UDP/IP traffic for a while and determine its properties.
 * Helpful for deriving values for a later allocate().
 */
bool UDPVDIFReceiver::snoopVDIF(size_t& framesize, size_t& payloadsize, size_t& framespersec)
{
    framesize = 0;
    payloadsize = 0;
    framespersec = 0;
    m_framesize = 0;
    m_payloadsize = 0;
    m_framespersec = 0;
    m_nchan = 0;
    m_bitspersample = 0;
    m_headersize = 16;
    if (m_sd < 0) {
        return false;
    }

    void* minibuf = memalign(PAGE_SZ, 65535);
    const unsigned char *hdr = (const unsigned char*)minibuf + m_frameoffset;
    size_t nframes = 0;
    struct timeval tstart, tstop;

    gettimeofday(&tstart, NULL);
    while (1) {

        ssize_t n = recv(m_sd, minibuf, m_frameoffset + 32, MSG_TRUNC);
        if (n < ((ssize_t)m_frameoffset + 32)) {
            L_(lerror) << "UDPVDIFReceiver::snoopVDIF() no VDIF data, stopping";
            free(minibuf);
            return false;
        }
        nframes++;

        // Detect size
        if (framesize == 0) {
            framesize    = 8*(hdr[8] + hdr[9]*256L + hdr[10]*65536L);
            m_headersize = ((hdr[3] & 0x40) != 0) ? 16 : 32;
            payloadsize  = framesize - m_headersize;
        }

        // Detect largest frame# and then stop
        size_t framenr = hdr[4] + hdr[5]*256L + hdr[6]*65536L;
        if (framespersec > (framenr + 1)) {
            break;
        } else {
            framespersec = framenr + 1;
        }
    }
    gettimeofday(&tstop, NULL);

    assert(nframes >= 1);

    // Determine #bits/sample, #channels based on the last frame
    m_bitspersample =  ((hdr[15] >> 2) & 0x1F) + 1;
    m_nchan = 1 << (hdr[11] & 0x1F);

    // Estimate the rates
    double dT = (tstop.tv_sec - tstart.tv_sec) + 1e-6*(tstop.tv_usec - tstart.tv_usec);
    size_t rate_Mbps = (8.0*payloadsize*framespersec) / 1000000.0;
    size_t network_rate_Mbps = ((8.0*payloadsize*nframes) / 1000000.0) / dT;
    L_(linfo) << "Autodetect: measured " << rate_Mbps << " Mbit/s from fps ("
              << network_rate_Mbps << " Mbit/s from UDP), "
              << framespersec << " fps, "
              << framesize << "-byte frames, "
              << payloadsize << "-byte payload, "
              << m_nchan << "-ch " << m_bitspersample << "-bit";

    // Adjust the 'framespersec' to the closest usually expected rate (128/256/512/1024/2048/4096/8192 Mbit/s)
    rate_Mbps = ceil_pow2(rate_Mbps);
    size_t adj_fps = ((1000000.0*rate_Mbps)/8.0) / payloadsize;
    if (framespersec != adj_fps) {
        L_(linfo) << "Autodetect: adjusted measured rate to presumed actual " << rate_Mbps << " Mbit/s, " << adj_fps << " fps";
        framespersec = adj_fps;
    }

    m_framesize = framesize;
    m_payloadsize = payloadsize;
    m_framespersec = framespersec;

    free(minibuf);
    return true;
}

/** Release all data receive buffers */
bool UDPVDIFReceiver::deallocate()
{
    boost::mutex::scoped_lock iolock(m_bufmutex);
    if (m_bufs == NULL) {
        return true;
    }

    for (int n = 0; n < m_Nbufs; n++) {
        if (m_bufs[n] == NULL) {
            continue;
        }
        munlock(m_bufs[n], m_buflen);
        free(m_bufs[n]);
    }
    free(m_bufs);
    free(m_bufstates);
    free(m_bufstates_prev);

    m_bufs = NULL;
    m_bufstates = NULL;
    m_bufstates_prev = NULL;
    m_buflen = 0;
    m_Nbufs = 0;

    return true;
}

/** Allocate a set of data receive buffers. The buffer size excludes VDIF headers. */
bool UDPVDIFReceiver::allocate(const int Nbuffers, const size_t bufsize)
{
    boost::mutex::scoped_lock iolock(m_bufmutex);
    if ((Nbuffers < 1) || (Nbuffers > 256) || (bufsize < (size_t)32*Nbuffers)) {
        return false;
    }

    if (m_bufs != NULL) {
        iolock.unlock();
        deallocate();
        iolock.lock();
    }

    m_buflen = bufsize;
    m_Nbufs = Nbuffers;
    m_bufstates = (BufState*)memalign(PAGE_SZ, sizeof(BufState)*Nbuffers);
    if (!m_bufstates) {
        perror("memalign m_bufstates");
        return false;
    }
    m_bufstates_prev = (BufState*)memalign(PAGE_SZ, sizeof(BufState)*Nbuffers);
    if (!m_bufstates_prev) {
        perror("memalign m_bufstates_prev");
        return false;
    }
    m_bufs = (void**)memalign(PAGE_SZ, sizeof(void*)*Nbuffers);
    if (!m_bufs) {
        perror("memalign m_bufs");
        return false;
    }

    for (int n = 0; n < m_Nbufs; n++) {
        m_bufs[n] = NULL;
        m_bufstates[n] = Free;
        m_bufstates_prev[n] = Free;
    }

    size_t buflen_ceil = m_buflen - (m_buflen%PAGE_SZ) + PAGE_SZ;
    for (int n = 0; n < m_Nbufs; n++) {
        m_bufs[n] = memalign(PAGE_SZ, buflen_ceil);
        if (!m_bufs[n]) {
            perror("memalign m_bufs[n]");
            iolock.unlock();
            deallocate();
            return false;
        }
        if (mlock(m_bufs[n], buflen_ceil) == -1) {
            perror("mlock m_bufs[n]");
        }
        // for async DMA would need this somewhere: cudaHostRegister(m_bufs[n], buflen_ceil, cudaHostRegisterPortable);
    }

    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Add a new recipient that shall receive raw buffers upon completion.
 * Call multiple times to add several recipients for round-robin reception.
 */
bool UDPVDIFReceiver::registerRecipient(DataRecipient* r)
{
    boost::mutex::scoped_lock vlock(m_recipientmutex);
    if (r == NULL) {
        return false;
    }
    m_dataRecipients.push_back(r);
    L_(linfo) << "registered new receiver (ptr=" << (void*)r << ") to receive raw VDIF data blocks.";
    return true;
}

/** Remove a particular recipient, or remove all if argument is NULL */
bool UDPVDIFReceiver::unregisterRecipient(DataRecipient* r)
{
    boost::mutex::scoped_lock vlock(m_recipientmutex);
    if (r == NULL) {
        m_dataRecipients.clear();
    } else {
        m_dataRecipients.erase(
            std::remove(m_dataRecipients.begin(), m_dataRecipients.end(), r),
            m_dataRecipients.end()
        );
    }
    return true;
}


/**
 * Start data pump in the background. The VDIF payload data received into buffers
 * will be pushed to the registered recipient(s) in round-robin fashion.
 */
bool UDPVDIFReceiver::startRx()
{
    boost::mutex::scoped_lock iolock(m_bufmutex);
    if (!m_bufs) {
        L_(lerror) << "UDPVDIFReceiver::startRx() called but no buffer space allocate()'d yet!";
        return false;
    }

    if (m_framespersec == 0) {
        L_(linfo) << "UDPVDIFReceiver::startRx() will now measure the not yet determined frames/sec rate";
        size_t i1, i2;
        iolock.unlock();
        snoopVDIF(i1,i2, m_framespersec);
        iolock.lock();
    }

    if (m_rxDatapumpThread != NULL) {
        L_(linfo) << "UDPVDIFReceiver::startRx() called while reception already running!";
        return false;
    }

    m_overflowcount = 0;
    m_rxDatapumpThread = new boost::thread(&UDPVDIFReceiver::rxDatapumpWorker, this);

    return true;
}

/** Stop the data pump thread */
bool UDPVDIFReceiver::stopRx()
{
    boost::mutex::scoped_lock iolock(m_bufmutex);
    if (m_rxDatapumpThread == NULL) {
        L_(ldebug) << "UDPVDIFReceiver::stopRx() called without active data dump!";
        return false;
    }

    L_(ldebug) << "stopRx() interrupting the datapump worker thread...";
    m_rxDatapumpThread->interrupt();
    iolock.unlock();
    m_rxDatapumpThread->join();
    iolock.lock();

    L_(ldebug) << "stopRx() joined datapump worker thread, doing clean-up";
    delete m_rxDatapumpThread;

    m_rxDatapumpThread = NULL;

    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Increment the current [start;stop] interval by m_framesperbuf to determine the next data-capture window.
 */
void UDPVDIFReceiver::nextStartStopRange(int32_t& start_sec, int32_t& start_frame, int32_t& stop_sec, int32_t& stop_frame) const
{
    std::stringstream info;
    info << "Capture range : previous=["
         << start_sec << "s/#" << start_frame << ",...,"
         << stop_sec << "s/#" << stop_frame << "]";

    // Determine next start
    start_sec = stop_sec;
    start_frame = stop_frame + 1;
    if (start_frame >= (int32_t)m_framespersec) {
        start_frame %= m_framespersec;
        start_sec++;
    }

    // Determine next stop
    stop_sec = start_sec;
    stop_frame = start_frame + m_framesperbuf - 1;
    if (stop_frame >= (int32_t)m_framespersec) {
        stop_frame %= m_framespersec;
        stop_sec++;
    }

    info << ", next=["
         << start_sec << "s/#" << start_frame << ",...,"
         << stop_sec << "s/#" << stop_frame << "]";
    L_(ldebug) << info.str();
}

/**
 * Worker that receives UDP/IP containing VDIF, strips VDIF headers, and pushes
 * completed buffers into the registered data recipient(s) (round-robin). Takes
 * care to preserve spacing in time appropriate for the integration time (i.e.
 * the buffer size).
 */
void UDPVDIFReceiver::rxDatapumpWorker()
{
    int slot = 0;

    VDIF_header_t h;
    void* minibuf = memalign(PAGE_SZ, 65535);
    const unsigned char *hdr = (unsigned char*)minibuf + m_frameoffset;
    const unsigned char *pay = (unsigned char*)minibuf + m_frameoffset + m_headersize;

    int32_t start_at_sec = 0, start_at_frame = 0;
    int32_t stop_at_sec = 0, stop_at_frame = 0; // end marker for current buffer

    L_(linfo) << "Started data pump with initially " << m_dataRecipients.size() << " recipients";

    // How many frames we want per buffer
    m_framesperbuf = m_buflen / m_payloadsize;
    L_(linfo) << "Data pump buffers will contain " << m_framesperbuf
              << " x " << m_payloadsize << "-byte frames, "
              << "corresponding to " << std::fixed << std::setprecision(2) << ((m_framesperbuf*1e3)/(double)m_framespersec) << " data-msec";

    try {

dbgRestartRX:

        // Peek to get active data-second from the VDIF header
        while ( recv(m_sd, minibuf, m_frameoffset + m_headersize, MSG_PEEK|MSG_TRUNC) <= 0 ) {
            boost::this_thread::interruption_point();
            L_(linfo) << "No UDP/IP data in the last " << UDP_TIMEOUT_SEC << " seconds...";
        }

        // Temporary capture-stop at next(+1, +2?) integer sec, to derive actual next [start;stop] interval
        buf2VDIFheader_time(hdr, h);
        stop_at_sec = h.seconds + 1;
        stop_at_frame = m_framespersec - 1;
        nextStartStopRange(start_at_sec, start_at_frame, stop_at_sec, stop_at_frame);

        int32_t prev_sec = h.seconds; //for debug only

        // Receive data until interrupted
        while (1) {

             // Grab next slot that is not used by GPU
             while (1) {
                 boost::this_thread::interruption_point();
                 int newslot = reserveSlot(Free, slot); // try to get a Free slot first
                 if (newslot < 0) {
                     if (1) {
                         // optionally, wait to be woken after a host->GPU transfer completes
                         boost::mutex::scoped_lock iolock(m_bufmutex);
                         releasedbufcond.wait(iolock);
                     }
                     newslot = reserveSlotInv(Transferring, slot); // find any type of slot unused by GPU
                 }
                 if (newslot < 0) {
                     boost::this_thread::yield();
                     //nanosleep_msec(2.0);
                     continue;
                 } else {
                     slot = newslot;
                     L_(ldebug) << "Reserved raw VDIF slot " << slot << " that was " << bufState2str(m_bufstates_prev[slot]);
                     break;
                 }
            }

            // Now receive enough data for the slot
            size_t nframes = 0; // total received, including discarded frames
            bool started = false, stopped = false;
            while (!stopped) {

                // Peek at UDP packet (just the VDIF header portion) to get timestamp
                ssize_t nrd = recv(m_sd, minibuf, m_frameoffset + m_headersize, MSG_PEEK|MSG_TRUNC);
                if (nrd <= 0) {
                    boost::this_thread::interruption_point();
                    L_(linfo) << "No UDP/IP data in the last " << UDP_TIMEOUT_SEC << " seconds...";
                    continue;
                }

                nframes++;
                if ((nframes & 1024) == 1024) { // faster than (nframes%1024)==0 ?
                    boost::this_thread::interruption_point();
                }

                if (h.seconds < prev_sec) {
                    // usually does not happen (except with VDIF-looping sender program)
                    boost::mutex::scoped_lock iolock(m_bufmutex);
                    L_(ldebug2) << "VDIF timestamps jumped back in time! restarting block rx!";
                    m_bufstates[slot] = Free;
                    iolock.unlock();
                    goto dbgRestartRX;
                }
                prev_sec = h.seconds;

                // Receive and discard until started
                buf2VDIFheader_time(hdr, h);
                if (!started) {
                    if (!((h.seconds >= start_at_sec) && (h.framenr >= start_at_frame))) {
                        recv(m_sd, minibuf, 0, MSG_TRUNC);
                        continue;
                    }
                    started = true;
                }

                // Pending stop if at last frame, or already past the last frame
                if ((h.seconds >= stop_at_sec) && (h.framenr >= stop_at_frame)) {
                    stopped = true;
                    if ((h.seconds == stop_at_sec) && (h.framenr == stop_at_frame)) {
                        // exactly at last frame, receive and store (below)
                    } else {
                        break; // keep frame till later
                    }
                }

                //std::cout << " sec " << h.seconds << "/" << start_at_sec << ", frame " << h.framenr << "/" << start_at_frame << "\n";

                // Receive to remove the UDP packet from the buffer
                ssize_t off = m_payloadsize * (((ssize_t)h.seconds - start_at_sec)*m_framespersec + ((ssize_t)h.framenr - start_at_frame));
                if ((off >= 0) && (off < (ssize_t)m_buflen)) {
                    // Store
                    recv(m_sd, minibuf, 65500, 0);
                    memcpy((char*)m_bufs[slot] + off, pay, m_payloadsize);
                } else {
                    // Discard. Note: Negative offset happens on time reversals, excess offset on loss of near-end packets
                    recv(m_sd, minibuf, 0, MSG_TRUNC);
                    //std::cerr << "buf oops @ sec " << h.seconds << "/" << start_at_sec << ", frame " << h.framenr << "/" << start_at_frame << " : off = " << off << "\n";
                }

            }  // while(!stopped)

            // Mark the slot as Full/Overflowed
            lockscope {
                boost::mutex::scoped_lock iolock(m_bufmutex);
                if (m_bufstates_prev[slot] == Free) {
                    m_bufstates[slot] = Full;
                } else if (m_bufstates_prev[slot] == Full) {
                    m_bufstates[slot] = Overflowed;
                } else if (m_bufstates_prev[slot] == Overflowed) {
                    // allow an overflow condition to clear, rather than stick around permanently:
                    m_bufstates[slot] = Full;
                } else if (m_bufstates_prev[slot] == Busy || m_bufstates_prev[slot] == Transferring) {
                    m_bufstates[slot] = Full;
                    L_(lerror) << "Unexpected previous VDIF receiver slot state of 'Busy' or 'Xfer'";
                } else {
                    m_bufstates[slot] = Full;
                    L_(lerror) << "Unexpected previous VDIF receiver slot state";
                }
                if (m_bufstates[slot] == Overflowed) { m_overflowcount++; }
            }

            // Determine data weight and time-tag
            buf2VDIFheader_aux(hdr, h); // just for refepoch
            double weight = ((double)nframes) / m_framesperbuf;
            double t_off = (start_at_frame / (double)m_framespersec) + (0.5 * m_framesperbuf) / m_framespersec;
            struct timeval tmid;
            tmid.tv_sec  =  C_vdif_epoch_to_timeval[h.refepoch] + start_at_sec + (time_t)t_off;
            tmid.tv_usec = 1e6*(t_off - floor(t_off));

            // Push the data to a recipient; the thread marks slot as 'Transferring'
            boost::thread* t = new boost::thread();
            *t = boost::thread(boost::bind(&UDPVDIFReceiver::rxDatabufferHandover, this, slot, weight, tmid, t));
            t->detach();

            L_(ldebug) << "Dispatch slot " << slot << " to recipient"
                       << ", nframes " << nframes
                       << ", weight " << std::fixed << std::setprecision(3) << weight
                       << ", midtime " << (tmid.tv_sec + 1e-6*tmid.tv_usec)
                       << ", " << m_overflowcount << " slot overflows accumulated";

            // Determine the end marker of the next buffer
            nextStartStopRange(start_at_sec, start_at_frame, stop_at_sec, stop_at_frame);

        } //while(1)

    } catch (boost::thread_interrupted& e) {
        L_(ldebug) << __func__ << " was interrupted and is exiting";
    }

    free(minibuf);
}

/**
 * Detached thread(s) that handle passing completed data to a recipient.
 */
void UDPVDIFReceiver::rxDatabufferHandover(int slot, double weight, struct timeval tmid, boost::thread* self)
{
    DataRecipient* r = NULL;
    int Nrecpt = -1, irecpt = -1;

    // Find a recipient
    lockscope {
        boost::mutex::scoped_lock vlock(m_recipientmutex);
        Nrecpt = m_dataRecipients.size();
        if (Nrecpt > 0) {
            irecpt = slot % Nrecpt;
            r = m_dataRecipients.at(irecpt);
        }
    }

    // Invoke recipient (hope it wasn't unregistered in the meantime)
    if (Nrecpt > 0) {
        L_(ldebug2) << "VDIF buffer GPU handover: next recipient " << irecpt << ", transferring slot " << slot;
        lockscope {
            boost::mutex::scoped_lock iolock(m_bufmutex);
            m_bufstates[slot] = Transferring;
        }
        r->takeData(m_bufs[slot], m_buflen, weight, tmid);
    } else {
        L_(ldebug2) << "VDIF buffer GPU handover: no recipients registered, discarding slot " << slot;
    }

    // Release buffer
    lockscope {
        boost::mutex::scoped_lock iolock(m_bufmutex);
        m_bufstates[slot] = Free;
        L_(ldebug) << "VDIF buffer GPU handover done, releasing slot " << slot;
    }
    releasedbufcond.notify_all();

    delete self;
}


/**
 * Find and reserve the first slot with given state, starting from slot n.
 */
int UDPVDIFReceiver::reserveSlot(BufState state, int n)
{
    boost::mutex::scoped_lock iolock(m_bufmutex);

#if 1
    // debug: print status of all slots
    std::stringstream ss;
    ss << "Slot statuses: ";
    for (int i = 0; i < m_Nbufs; i++) {
        ss << i << ":" << bufState2str(m_bufstates[i]) << " ";
    }
    L_(ldebug2) << ss.str();
#endif

    // Now look for first match to 'state'
    for (int i = 0; i < m_Nbufs; i++) {
        n = (n + i) % m_Nbufs;
        if (m_bufstates[n] == state) {
            m_bufstates_prev[n] = m_bufstates[n];
            m_bufstates[n] = Busy;
            return n;
        }
    }
    return -1;
}

/**
 * Find and reserve the first slot *not* in the given state, starting from slot n.
 */
int UDPVDIFReceiver::reserveSlotInv(BufState state, int n)
{
    boost::mutex::scoped_lock iolock(m_bufmutex);

#if 1
    // debug: print status of all slots
    std::stringstream ss;
    ss << "Slot statuses: ";
    for (int i = 0; i < m_Nbufs; i++) {
        ss << i << ":" << bufState2str(m_bufstates[i]) << " ";
    }
    L_(ldebug2) << ss.str();
#endif

    // Now look for first non-match to 'state'
    for (int i = 0; i < m_Nbufs; i++) {
        n = (n + i) % m_Nbufs;
        if (m_bufstates[n] != state) {
            m_bufstates_prev[n] = m_bufstates[n];
            m_bufstates[n] = Busy;
            return n;
        }
    }
    return -1;
}


/////////////////////////////////////////////////////////////////////////////////////////////

/** Return a close power-of-2 to n */
static uint32_t ceil_pow2(uint32_t n)
{
    n--;
    n |= n >> 1;
    n |= n >> 2;
    n |= n >> 4;
    n |= n >> 8;
    n |= n >> 16;
    n++;
    return n;
}

/** Sleep at most T_msec milliseconds, or shorter if interrupted */
static void nanosleep_msec(double T_msec)
{
    struct timespec treq;
    treq.tv_sec = (time_t)(T_msec*1e-3);
    T_msec -= treq.tv_sec*1e3;
    treq.tv_nsec = T_msec*1e6;
    nanosleep(&treq, NULL); // can be interrupted! don't care to sleep more after interruption
}

static void buf2VDIFheader_time(const unsigned char *buf, VDIF_header_t& hdr)
{
    bool aligned = ( ((size_t)buf & (sizeof(uint32_t)-1)) == 0 );
    if (!aligned) {
        hdr.seconds = buf[0] + buf[1]*256L + buf[2]*65536L + (buf[3] & 0x3F)*4294967296L;
        hdr.framenr = buf[4] + buf[5]*256L + buf[6]*65536L;
    } else  {
        const uint32_t *buf32 = (const uint32_t*)buf;
        hdr.seconds = buf32[0] & 0x3FFFFFFF;
        hdr.framenr = buf32[1] & 0x00FFFFFF;
    }
}

static void buf2VDIFheader_aux(const unsigned char *buf, VDIF_header_t& hdr)
{
    bool aligned = ( ((size_t)buf & (sizeof(uint32_t)-1)) == 0 );
    if (!aligned) {
        hdr.framesize = 8*(buf[8] + buf[9]*256L + buf[10]*65536L);
    } else  {
        const uint32_t *buf32 = (const uint32_t*)buf;
        hdr.framesize = 8*(buf32[2] & 0x00FFFFFF);
    }
    hdr.headersize = ((buf[3] & 0x40) != 0) ? 16 : 32;
    hdr.payloadsize = hdr.framesize - hdr.headersize;
    hdr.refepoch = buf[7] & 0x3F;
    hdr.log2Nch = buf[11] & 0x1F;
    hdr.tid = buf[14] + (buf[15] & 3)*256L;
    hdr.station[0] = buf[12];
    hdr.station[1] = buf[13];
}

/////////////////////////////////////////////////////////////////////////////////////////////

const time_t C_vdif_epoch_to_timeval[63] = { // ref.epoch is 6-bit, 0..63
   946684800,  962409600,  978307200,  993945600, 1009843200,
  1025481600, 1041379200, 1057017600, 1072915200, 1088640000,
  1104537600, 1120176000, 1136073600, 1151712000, 1167609600,
  1183248000, 1199145600, 1214870400, 1230768000, 1246406400,
  1262304000, 1277942400, 1293840000, 1309478400, 1325376000,
  1341100800, 1356998400, 1372636800, 1388534400, 1404172800,
  1420070400, 1435708800, 1451606400, 1467331200, 1483228800,
  1498867200, 1514764800, 1530403200, 1546300800, 1561939200,
  1577836800, 1593561600, 1609459200, 1625097600, 1640995200,
  1656633600, 1672531200, 1688169600, 1704067200, 1719792000,
  1735689600, 1751328000, 1767225600, 1782864000, 1798761600,
  1814400000, 1830297600, 1846022400, 1861920000, 1877558400,
  1893456000, 1909094400, 1924992000
};

/////////////////////////////////////////////////////////////////////////////////////////////

#ifdef TEST_UDP_RX

#include <signal.h>

static volatile bool ctrlC_pressed = false;

void sigintHandler(int)
{
    ctrlC_pressed = true;
}

int main(int argc, char** argv)
{
    size_t framesize, payloadsize, framespersec;
    const int Nbufs = 8;
    size_t bufsize;

    DataRecipientNoop rcv;
    UDPVDIFReceiver udpRx;

    if (!udpRx.open(46227, 0)) {
        std::cout << "Error opening UDP port\n";
        return 0;
    }

    if (!udpRx.snoopVDIF(framesize, payloadsize, framespersec)) {
        std::cout << "Error determing UDP VDIF information from UDP stream\n";
        return 0;
    }

    bufsize = (framespersec / 4) * payloadsize;
    if (!udpRx.allocate(Nbufs, bufsize)) {
        std::cout << "Error allocating 250ms receive buffers\n";
        return 0;
    }

    udpRx.startRx();

    std::cout << "Hit Ctrl-C to stop receiver loop\n";
    if (0) {
        signal(SIGINT, sigintHandler);
        while (!ctrlC_pressed) sleep(1);
    } else if(0) {
        while (1) sleep(1);
    } else {
        sleep(20);
    }

    udpRx.stopRx();

    udpRx.deallocate();
    udpRx.close();

    return 0;
}

#endif

