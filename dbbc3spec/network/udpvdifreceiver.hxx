#ifndef UDFVDIFRECEIVER_HXX
#define UDFVDIFRECEIVER_HXX

#include "datarecipients/datarecipient.hxx"

#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/noncopyable.hpp>

#include <stdlib.h>
#include <sys/time.h>

#define UDP_TIMEOUT_SEC 5

class DataRecipient;

class UDPVDIFReceiver : private boost::noncopyable {

    public:

        UDPVDIFReceiver();

        /**
         * Open an UDP socket
         * @param frameoffset byte position in the UDP/IP packet at which the actual VDIF frame starts
         */
        bool open(const int port, const size_t frameoffset = 0);

        /** Close UDP socket */
        bool close();

        /**
         * Inspect the incoming UDP/IP traffic for a while and determine its properties.
         * Helpful for deriving values for a later allocate().
         */
        bool snoopVDIF(size_t& framesize, size_t& payloadsize, size_t& framespersec);

        /** \return Number of channels in the VDIF stream (once it has been determined by snoopVDIF()) */
        int getNchan() const { return m_nchan; }

        /** \return Number of bits/sample in the VDIF stream (once it has been determined by snoopVDIF()) */
        int getNbits() const { return m_bitspersample; }

        /** Allocate a set of data receive buffers. The buffer size and excludes VDIF headers. */
        bool allocate(const int Nbuffers, const size_t);

        /** Free up all buffers */
        bool deallocate();

        /** Get pointer array of buffers, inteded for externally calling cudaHostRegister() */
        void** getBufferPtrs(int& Nbuffers, size_t& buflen) { Nbuffers = m_Nbufs; buflen = m_buflen; return m_bufs; }

    public:
        /**
         * Start data pump in the background. The VDIF payload data received into buffers
         * will be pushed to the registered recipient(s) in round-robin fashion.
         */
        bool startRx();

        /** Stop the data pump thread */
        bool stopRx();

        /**
         * Add a new recipient that shall receive raw buffers upon completion.
         * Call multiple times to add several recipients for round-robin reception.
         */
        bool registerRecipient(DataRecipient*);

        /** Remove a particular recipient, or remove all if argument is NULL */
        bool unregisterRecipient(DataRecipient*);

    public:
        enum BufState { Free=0, Busy=1, Full=2, Overflowed=3, Transferring=4 };
        const std::string bufState2str(BufState s) const {
            switch (s) {
                case Free: return std::string("Free");
                case Busy: return std::string("Busy");
                case Full: return std::string("Full");
                case Overflowed: return std::string("Over");
                case Transferring: return std::string("Xfer");
            }
            return std::string("--illegal--");
        }
        size_t m_overflowcount;

    private:
        int    m_sd;
        size_t m_frameoffset;
        size_t m_framespersec, m_framesize, m_payloadsize, m_headersize;
        int    m_nchan, m_bitspersample;

        int    m_Nbufs;
        size_t m_buflen;
        void **m_bufs;
        BufState *m_bufstates;
        BufState *m_bufstates_prev;
        boost::condition_variable releasedbufcond;

        size_t m_framesperbuf;

        boost::mutex m_bufmutex;
        boost::thread *m_rxDatapumpThread;

        boost::mutex m_recipientmutex;
        std::vector<DataRecipient*> m_dataRecipients;

    private:

        /**
         * Worker that receives UDP/IP containing VDIF, strips VDIF headers, and pushes
         * completed buffers into the registered data recipient(s) (round-robin). Takes
         * care to preserve spacing in time appropriate for the integration time (i.e.
         * the buffer size).
         */
        void rxDatapumpWorker();

        /**
         * Detached thread(s) that handle passing completed data to a recipient.
         */
        void rxDatabufferHandover(int slot, double weight, struct timeval tmid, boost::thread* self);

        /**
         * Find and reserve the first slot with given state, starting from slot n.
         */
        int reserveSlot(BufState state, int n=0);

        /**
         * Find and reserve the first slot *not* in given state, starting from slot n.
         */
        int reserveSlotInv(BufState state, int n=0);

        /**
         * Increment the current [start;stop] interval by m_framesperbuf to determine the next data-capture window.
         */
        void nextStartStopRange(int32_t& start_sec, int32_t& start_frame, int32_t& stop_sec, int32_t& stop_frame) const;

};



#endif // UDFVDIFRECEIVER_HXX
