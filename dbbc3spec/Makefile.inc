
##############################################################################################
## Environment
##############################################################################################

TOPDIR := $(dir $(lastword $(MAKEFILE_LIST)))

# Location where to install and generate
# sub-dirs ./alma ./alma/bin ./alma/conf ./alma/share
INSTALL_PREFIX := /opt/

# Location of CUDA installation
#CUDA_DIR := /usr/local/cuda-8.0/
CUDA_DIR := /usr/local/cuda-9.0/

# Location of DRXP driver sources (only drxpd.h is needed)
DRXP_DIR := /usr/local/include/drxp/

# Enable GPU processing?
# 1: enable GPU (CUDA) code, 0: revert to CPU code (simulator mode)
HAVE_GPU := 1

# Enable DRXP board interface?
# 1: enable access to actual DRXP driver interface, 0: revert to dummy DRXP object
HAVE_DRXP := 1

# Enable Xerces-C XML validation?
# 1: enable, 0: disable
HAVE_XERCES := 1

##############################################################################################
# Check CUDA compiler availability
##############################################################################################

NVCC = $(CUDA_DIR)/bin/nvcc

ifeq ($(HAVE_GPU),1)
    ifeq (,$(wildcard $(NVCC)))
        $(warning Warning: Cannot find CUDA compiler $(NVCC). Please check the settings in Makefile.inc.)
        HAVE_GPU := 0
    endif
endif

##############################################################################################
# Check DRXP driver availability
##############################################################################################

ifneq (,$(wildcard $(DRXP_DIR)/drxpd.h))
    # HAVE_DRXP := 1 (leave unchanged)
else
    HAVE_DRXP := 0
endif

##############################################################################################
## Configuration for C compiler and linker
##############################################################################################

CDEFS   = -D_FILE_OFFSET_BITS=64 -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE
CDEFS  += -D_XOPEN_SOURCE=600 -D_GNU_SOURCE
CFLAGS  = -Wall -O3 -pthread -g $(CDEFS) -m64

LDFLAGS = -lpthread -g
LDLIBS = -lm -lpthread -ldl

ALMA_CXXFLAGS = -O3 -m64 -Wall -g -I. -I$(DRXP_DIR)
ALMA_LIBS = -lboost_system -lboost_thread-mt -lboost_date_time -lpthread

ifeq ($(HAVE_XERCES),1)
    ALMA_LIBS += -lxerces-c
    ALMA_CXXFLAGS += -DHAVE_XERCES=1
endif
        
NVFLAGS = -O3 $(CDEFS) -m64 -g -Xcompiler -Wall
NVFLAGS += --use_fast_math
# NVFLAGS += --ptxas-options=-v
NVFLAGS += --compiler-bindir=/usr/bin/gcc
NVFLAGS += -I$(TOPDIR)kernels
# NVFLAGS += -DGPU_DEBUG

NVLDFLAGS = -L$(CUDA_DIR)/lib64/
NVLDLIBS = -lcuda -lcudart -lcufft

## Configuration for CUDA on a specific multi-GPU system

NV_ADD_KEPLER  := -gencode arch=compute_30,code=sm_30
NV_ADD_KEPLER  += -gencode arch=compute_35,code=sm_35 -gencode arch=compute_37,code=sm_37
NV_ADD_MAXWELL := -gencode arch=compute_50,code=sm_50
NV_ADD_MAXWELL += -gencode=arch=compute_52,code=sm_52 -gencode=arch=compute_52,code=compute_52
NV_ADD_PASCAL  := -gencode=arch=compute_61,code=sm_61 -gencode=arch=compute_61,code=compute_61
NV_ADD_VOLTA   := -gencode=arch=compute_70,code=sm_70 -gencode=arch=compute_70,code=compute_70

# Full list of targets per CUDA version: http://arnon.dk/matching-sm-architectures-arch-and-gencode-for-various-nvidia-cards/
NV_ADD_CUDA_7 = -gencode=arch=compute_50,code=sm_50 -gencode=arch=compute_52,code=sm_52 -gencode=arch=compute_52,code=compute_52
NV_ADD_CUDA_8 = $(NV_ADD_CUDA_7) -gencode=arch=compute_60,code=sm_60 -gencode=arch=compute_61,code=sm_61 -gencode=arch=compute_61,code=compute_61
NV_ADD_CUDA_9 = $(NV_ADD_CUDA_8) -gencode=arch=compute_62,code=sm_62 -gencode=arch=compute_70,code=sm_70 -gencode=arch=compute_70,code=compute_70

NV_ARCHS_MIXED := $(NV_ADD_CUDA_9)
NV_ARCHS_LATEST := $(NV_ADD_CUDA_9)

# TODO: somehow use ./helper/cudaVersion to determine CUDA version, path alone is not reliable enough, and 'nvcc --version' output too long to parse
#ifneq ("$(wildcard /usr/local/cuda-8.0)","")
#       NV_ARCHS_MIXED += $(NV_ADD_PASCAL)
#       NV_ARCHS_LATEST := $(NV_ADD_PASCAL)
#endif
#ifneq ("$(wildcard /usr/local/cuda-9.0)","")
#       NV_ARCHS_MIXED += $(NV_ADD_PASCAL)
#       NV_ARCHS_MIXED += $(NV_ADD_VOLTA)
#       NV_ARCHS_LATEST := $(NV_ADD_VOLTA) $(NV_ADD_PASCAL)
#endif

NVFLAGS_MIXED := $(NVFLAGS) $(NV_ARCHS_MIXED)
NVFLAGS_LATEST := $(NVFLAGS) $(NV_ARCHS_LATEST)
