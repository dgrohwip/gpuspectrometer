
#include "network/udpvdifreceiver.hxx"
#include "dbbc3spectrometer.hxx"

DBBC3Spectrometer::DBBC3Spectrometer(float Tint_sec, int port)
{
    udp_port = port;
    vdif_Tint = Tint_sec;
    if ((vdif_Tint < 0.1) || (vdif_Tint > 2.048)) {
        vdif_Tint = 0.256;
    }
    udpRx = new UDPVDIFReceiver();
}

DBBC3Spectrometer::~DBBC3Spectrometer()
{
    udpRx->stopRx();
    udpRx->deallocate();
}

bool DBBC3Spectrometer::start()
{
    if (!udpRx->open(udp_port, 0)) {
        return false;
    }

    if (!udpRx->snoopVDIF(vdif_framesize, vdif_payloadsize, vdif_framespersec)) {
        return false;
    }

    vdif_Nbufs = 4;
    vdif_bufsize = (vdif_framespersec * vdif_Tint) * vdif_payloadsize;
    if (!udpRx->allocate(4, vdif_bufsize)) {
        return false;
    }

    udpRx->registerRecipient(this);
    udpRx->startRx();
}

bool DBBC3Spectrometer::stop()
{
    udpRx->unregisterRecipient(this);
    udpRx->stopRx();
}

int main(int argc, char** argv)
{
    return 0;
}
