/**
 * \class CalibratorNonLinearity
 *
 * \brief Follows the method by Takeshi Kamazaki (ALMA-CORR-06011-C, pp.219, 2014)
 *        for non-linearity corrections of autocorrelation spectra
 *
 * In Kamazaki's method, mean quantized voltage (D0=sum(x[n])) and mean quantized power (D1=sum(x[n]^2),
 * measured over 1 millisecond intervals, are ussed together with two spline curves (forward m(p) and
 * inverse m^-1(P)) to determine a setpoint for the non-linearity correction. The non-linearity
 * correction itself is linear (AC_out[bin] = const1 * AC_in[bin] + const2).
 *
 * This correction may not actually be identical to the quantization-related van Vleck correction.
 * From the algorithm it seems more like a total power related correction of auto-power spectra
 * and does not touch the cross-power spectra.
 *
 * \author $Author: janw $
 *
 */
#ifndef CALIB_VANVLECK_HXX
#define CALIB_VANVLECK_HXX

#include <exception>
#include <stdexcept>

template<typename Thist_t,typename Tac_t>
class CalibratorNonLinearity {

    public:
        /** Create calibrator for n signal levels (but: just 8 levels tested for now). */
        CalibratorNonLinearity(int nlevels) : m_nlevels(nlevels) {
            m_f3 = 1.0;
            m_a = 1.0;
            m_b = 0.0;
            m_Pout = 0.0f;
            m_Pin = 0.0f;
            if (nlevels > m_nlevelsMax) {
                throw std::runtime_error("Requested more than the maximum supported 16 levels from CalibratorNonLinearity");
            }
        }

        ~CalibratorNonLinearity() { }

    public:
        /** Copy and external histogram and derive some correction factors from it */
        bool setHistogram(const Thist_t* hist);

        /** Apply correction factors to the given auto-power spectrum (out of place), based on factors derived in earlier call to setHistogram() */
        bool calibrateAutocorr(Tac_t* in, Tac_t* out, const int N) const;

        /** Apply correction factors to the given auto-power spectrum (inplace), based on factors derived in earlier call to setHistogram() */
        bool calibrateAutocorr(Tac_t* in, const int N) const;

       void selftest();

    private:
        /** \return Function m() evaluated at x, cf. ALMA-CORR-06011-C mapping Q_{3L}^{2-} */
        Tac_t Q_3L(Tac_t x) const;

        /** \return Function m^-1() evaluated at X, cf. ALMA-CORR-06011-C mapping Q_{3Li}^{2} */
        Tac_t Q_3Li(Tac_t X) const;

    private:
        static const int m_nlevelsMax = 16; /// maximum number of signal levels
        const int m_nlevels;                /// actual number of signal levels

        double m_Pout;                      /// factor 'P_out' derived from histogram, cf. ALMA-CORR-06011-C
        double m_Pin;                       /// factor 'P_in' derived from P_out, cf. "Step 2" in ALMA-CORR-06011-C
        double m_a;                         /// factor 'a' derived from histogram, cf. "Step 2" in ALMA-CORR-06011-C
        double m_b;                         /// factor 'b' derived from histogram, cf. "Step 2" in ALMA-CORR-06011-C
        double m_f3;                        /// efficiency factor
        static const double m_m[85][5];     /// lookup Q_{3L}^{2-} of Table 7.6; entry m[i][0] is the lower edge where the poly starts, m[i][1..4] are 4 coefficients for 3rd order poly
        static const double m_minv[120][5]; /// lookup Q_{3Li}^{2} of Table 7.8
        static const double m_mRef[89][2];  /// "Table 7.5 Parameters of cubic spline interpolation for Q_{3L}^{2-}" : reference values that the spline(m_m[][coeffs]) should closely approximate
        static const double m_minvRef[126][2];  /// "Table 7.7 Parameters of cubic spline interpolation for Q_{3Li}^{2}" : reference values that the spline(m_minv[][coeffs]) should closely approximate

        Tac_t m_const1, m_const2; /// factors for "Step 3" in ALMA-CORR-06011-C

};

#endif
