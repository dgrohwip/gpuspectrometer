// g++ -Wall -O3 countFileBits.cxx -o countFileBits

#include <bitset>
#include <iostream>
#include <fstream>
#include <stdio.h>

int main(int argc, char** argv)
{
    unsigned int block[2500];
    if (argc < 2) {
        return 0;
    }

    for (int n = 1; n < argc; n++) {

        std::ifstream fin(argv[n]);
        size_t nones = 0, nzeros = 0;

        while (fin.good()) {
            fin.read((char*)block, sizeof(block));
            int nrd = fin.gcount()/sizeof(unsigned int);
            for (int i=0; i<nrd; i++) {
                // gcc: int __builtin_popcount (unsigned int x) 
                int ones = __builtin_popcount(block[i]);
                nones += ones;
                nzeros += (8*sizeof(unsigned int) - ones);
            }
        }

	printf("%-30s : total %18zu bits, occurrences '1'=%18zu, ''0'=%18zu\n", argv[n], (nones+nzeros), nones, nzeros);
    }

    return 0;
}
