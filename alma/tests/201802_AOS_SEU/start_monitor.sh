#!/bin/sh

nohup ../../ASM_MC_service -d -c asm_AOStest_SEUcheck.json &

sleep 5

../..//scripts/asmTxCmd.py config-4-AOS.xml

COUNT=5
WAIT_TIME=5
OBS_DURATION=1

for i in $(seq 1 ${COUNT}); do
    OBS_TIME_SINCE_EPOCH=$(expr $(date +%s) + ${WAIT_TIME})
    OBS_TIME=$(date --date="@${OBS_TIME_SINCE_EPOCH}" +%Y-%m-%dT%H:%M:%S.000)
    END_TIME=$(date --date="@$(expr ${OBS_TIME_SINCE_EPOCH} + ${OBS_DURATION})" +%Y-%m-%dT%H:%M:%S.000)
    UID_BASE="$(date --date="@${OBS_TIME_SINCE_EPOCH}" +%m%dT%H%M%S)"
    OBS_UID="uid::${UID_BASE:0:5}/${UID_BASE:5:3}/${UID_BASE:8:3}"

    cat > startObservation.xml <<EOF
<?xml version="1.0"?>
<command>
<commandId>10</commandId>
<startObservation>
    <configId>4</configId>
    <observationId>${OBS_UID}</observationId>
    <time>${OBS_TIME}</time>
</startObservation>
</command>
EOF

    cat > stopObservation.xml <<EOF
<?xml version="1.0"?>
<command>
<commandId>11</commandId>
<stopObservation>
    <observationId>${OBS_UID}</observationId>
    <time>${END_TIME}</time>
</stopObservation>
</command>
EOF

    $HOME/gpuspectrometer/alma/scripts/asmTxCmd.py startObservation.xml
    $HOME/gpuspectrometer/alma/scripts/asmTxCmd.py stopObservation.xml

    sleep $(expr ${WAIT_TIME} + 1)
done


killall ASM_MC_service
