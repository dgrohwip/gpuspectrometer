
infile = 'C:\MatlabData\spectrometer\alma\synthetic.3bit';

Lfft = 128*1024;
Nfft = 4;

fdi = fopen(infile, 'r');

CH1 = zeros(Lfft/2,1);
CH2 = zeros(Lfft/2,1);
for jj=1:Nfft,
    if 1,
        dd = fread(fdi, [2 Lfft], 'ubit3');
        dd = dd - 3.5;
        ch1 = dd(1,:); ch2 = dd(2,:);
    else
        dd = fread(fdi, [Lfft 2], 'ubit3');
        dd = dd - 3.5;
        ch1 = dd(:,1); ch2 = dd(:,2);
    end
    f1 = abs(fft(ch1)); f1 = f1(:);
    f2 = abs(fft(ch2)); f2 = f2(:);
    f1(1) = 0; f2(1) = 0;
    CH1 = CH1 + f1(1:(Lfft/2));
    CH2 = CH2 + f2(1:(Lfft/2));
end

nlev=8;
figure(1), clf,
subplot(2,2,1), plot(CH1);
subplot(2,2,2), plot(CH2);
subplot(2,2,3), hist(ch1,nlev);
subplot(2,2,4), hist(ch2,nlev);
