%% matlab script for the generation of an input data file for 
%  the DTX simulator in the NAOJ.
%  written by Jongsoo Kim, 2017-10-28

% NOTE1: There are two inputs for this script, tint and ofname.
% You could modify the amplitiudes and frequencies of the sign wave.
% If you want to use other functional form, for example, Gaussian function,
% then the normalization of the 3bit quantization part should be modified.

% NOTE2: Once you generate the input data file, for example, sineXY.matlab
% you could use the program written by Takashi Nakamoto to convert 
% the file to a file which could be used for the DTX simulator
% .local/bin/drxpproposed2dtxsim sineXY.matlab sineXY.dtxsim

% NOTE3: You could upload the sineXY.dtxsim using the program again 
% written by Takashi Nakamoto.
% .local/bin/dtxsim upload-file X sineXY.dtxsim -r
% or 
% .local/bin/dtxsim upload-file Y sineXY.dtxsim -r
% where X has nothing to do with polarzation.  A proper name might be
% BB1 (baseband 1) or BB2 (baseband 2)

% Provide a time interval, tint, in units of millisecond as an input 
% of this script. Becuase of the limitation of the memory of the DTX simulator, 
% tint should be less than 1 second (<1000.0).  
% The minimum time interval is 8 um seconds and tint should 
% be a multiple of 8 um seconds
tint = 48; % 8 msecs

% output file name.  The format of the output file is a 3bit bianry format
% with time sequence of a polarization pair, (X,Y)
ofname = '/Users/jskim/Documents/MATLAB/sineXY.matlab';

% sampling rate per baseband per a polarization pair (X,Y)
fs = 2.0e9 * 2.0; % (bandwidth per baseband) * 2 (Nyquest sampliing)
ts = 1.0/fs;      % time interval between samples
 
% number of samples of a polarizaton pair (X,Y) within the time interval
N = tint*1.0e-3 * fs  
n = (0:N-1);            % array with elements from 0 to N-1

fx = 0.5*fs;      % half of the sampling frequency for pol X
fy = 0.25*fs;     % quarter of the smapleing frequency for pol Y
                      
xy(1,:) = sin(2*pi*fx*n*ts);  % sine wave for pol X
xy(2,:) = sin(2*pi*fy*n*ts);  % sine wave for pol Y

%figure(1), clf, 
%subplot(2,1,1), plot(n,xy(1,:)), title('X signal');
%subplot(2,1,2), plot(n,xy(2,:)), title('Y signal');

%% 3 bit quantization
%
% The ALMA ADC (Analog-Digital Converter) digitizes voltage readings
% into 3bit digits using the Gray code.
%
%    Gray   Lin  Gray   Volt
%     000 : 0    0    : -7 
%     001 : 1    1    : -5
%     011 : 2    3    : -3
%     010 : 3    2    : -1
%     110 : 4    6    : +1
%     111 : 5    7    : +3
%     101 : 6    5    : +5
%     100 : 7    4    : +7
%

% First normalize the amplitue of the sine wave whose amplitude becomes 8.0.

nlev = 8.0;                % number of levels
amp = 8.0;                 % amplitude of the sine wave

xy = amp*xy/max(max(xy));  % normalized samples, -8<xy<8
                           % max(max(xy)) find a maximum in the whole xy
                           % array [2,N]

% Second, quantize the samples and assign the Gray code based on 
% the quantized values. 
for j=1:N
    for i=1:2
        if (xy(i,j) < -6.0) 
           xy(i,j)=0;                     
        elseif (xy(i,j)>=-6.0 && xy(i,j)<-4.0)  
           xy(i,j)=1;
        elseif (xy(i,j)>=-4.0 && xy(i,j)<-2.0)
           xy(i,j)=3;
        elseif (xy(i,j)>=-2.0 && xy(i,j)<0.0) 
           xy(i,j)=2;
        elseif (xy(i,j)>=0.0 && xy(i,j)<2.0) 
           xy(i,j)=6;
        elseif (xy(i,j)>=2.0 && xy(i,j)<4.0) 
           xy(i,j)=7;
        elseif (xy(i,j)>=4.0 && xy(i,j)<6.0) 
           xy(i,j)=5;
        else
           xy(i,j)=4;
        end
    end
end


%% write Y and Y polarization data in 3 bit format
% "fwrite" writes to a file in a 3bit binary format in column order.  
% It means that fwrite saves data in time sequence of a polarization pair
% (X,Y). The 'ubit3' option is quite useful.
fid = fopen(ofname,'w');
fwrite(fid,xy,'ubit3');
fclose(fid);


