/* 2017.10.25. Hyunwoo Kang (KASI)					*/
/* It convert 3 bits data (by MATLAB) with DTX format.i			*/
/* 									*/

#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#define _PI_		3.1415926
#define _FRAME_UNIT_	48		//bytes

int main(int argc, char *argv[])
{
	int i, j;
	int tick;
	int d_t=0;

	char data[_FRAME_UNIT_];

	char X_3bits[3][64];
	char Y_3bits[3][64];
	char dtx[48];

	char filename[256];
	FILE *bfp, *dtx_fp;

	if(argc!=2) {
		printf("usage:convert_jskim.exe <data_name>\n");
		return -1;
	}

	sprintf(filename,"%s",argv[1]);

	if((bfp=fopen(filename,"rb"))<0) {
		printf("File does not exist!!\n");
		return -1;
	}

	dtx_fp=fopen("dtx_jskim.bin","wb");
// It reads until end of file (zero)
	while(fread(data,sizeof(char),_FRAME_UNIT_,bfp)!=0) {
		tick=0;
		for(i=0;i<16;i++){
// Three bytes of data contain four sets of HLM each X and Y.
			X_3bits[0][tick+ 0]=data[i*3   ] & 0X01;
			X_3bits[1][tick+ 0]=data[i*3   ] & 0X02;
			X_3bits[2][tick+ 0]=data[i*3   ] & 0X04;
			Y_3bits[0][tick+ 0]=data[i*3   ] & 0X08;
			Y_3bits[1][tick+ 0]=data[i*3   ] & 0X10;
			Y_3bits[2][tick+ 0]=data[i*3   ] & 0X20;

			X_3bits[0][tick+ 1]=data[i*3   ] & 0X40;
			X_3bits[1][tick+ 1]=data[i*3   ] & 0X80;
			X_3bits[2][tick+ 1]=data[i*3 +1] & 0X01;
			Y_3bits[0][tick+ 1]=data[i*3 +1] & 0X02;
			Y_3bits[1][tick+ 1]=data[i*3 +1] & 0X04;
			Y_3bits[2][tick+ 1]=data[i*3 +1] & 0X08;

			X_3bits[0][tick+ 2]=data[i*3 +1] & 0X10;
			X_3bits[1][tick+ 2]=data[i*3 +1] & 0X20;
			X_3bits[2][tick+ 2]=data[i*3 +1] & 0X40;
			Y_3bits[0][tick+ 2]=data[i*3 +1] & 0X80;
			Y_3bits[1][tick+ 2]=data[i*3 +2] & 0X01;
			Y_3bits[2][tick+ 2]=data[i*3 +2] & 0X02;

			X_3bits[0][tick+ 3]=data[i*3 +2] & 0X04;
			X_3bits[1][tick+ 3]=data[i*3 +2] & 0X08;
			X_3bits[2][tick+ 3]=data[i*3 +2] & 0X10;
			Y_3bits[0][tick+ 3]=data[i*3 +2] & 0X20;
			Y_3bits[1][tick+ 3]=data[i*3 +2] & 0X40;
			Y_3bits[2][tick+ 3]=data[i*3 +2] & 0X80;

			tick+=4;
		}
//
// Routine of conversion from 3 bits to DTX format
//
		for(i=0;i<8;i++){
// It points 0, 1, 4, 5, 8, and 9 of X byte positions.
			j = (i/2)*2+i;

			dtx[j]=0X00;
			dtx[j+2]=0X00;
			dtx[j+16]=0X00;
			dtx[j+16+2]=0X00;
			dtx[j+32]=0X00;
			dtx[j+32+2]=0X00;
// X(L) byte
			dtx[j]|=(X_3bits[0][j*8]     >0X00? 0x01 : 0x00);
			dtx[j]|=(X_3bits[0][j*8 + 1] >0X00? 0X02 : 0x00);
			dtx[j]|=(X_3bits[0][j*8 + 2] >0X00? 0X04 : 0x00);
			dtx[j]|=(X_3bits[0][j*8 + 3] >0X00? 0X08 : 0x00);
			dtx[j]|=(X_3bits[0][j*8 + 4] >0X00? 0X10 : 0x00); 
			dtx[j]|=(X_3bits[0][j*8 + 5] >0X00? 0X20 : 0x00); 
			dtx[j]|=(X_3bits[0][j*8 + 6] >0X00? 0X40 : 0x00); 
			dtx[j]|=(X_3bits[0][j*8 + 7] >0X00? 0X80 : 0x00); 
// X(M) byte
			dtx[j+16]|=(X_3bits[1][j*8]     >0X00? 0X01 : 0x00); 
			dtx[j+16]|=(X_3bits[1][j*8 + 1] >0X00? 0X02 : 0x00); 
			dtx[j+16]|=(X_3bits[1][j*8 + 2] >0X00? 0X04 : 0x00); 
			dtx[j+16]|=(X_3bits[1][j*8 + 3] >0X00? 0X08 : 0x00); 
			dtx[j+16]|=(X_3bits[1][j*8 + 4] >0X00? 0X10 : 0x00); 
			dtx[j+16]|=(X_3bits[1][j*8 + 5] >0X00? 0X20 : 0x00); 
			dtx[j+16]|=(X_3bits[1][j*8 + 6] >0X00? 0X40 : 0x00); 
			dtx[j+16]|=(X_3bits[1][j*8 + 7] >0X00? 0X80 : 0x00); 
// X(H) byte
			dtx[j+16*2]|=(X_3bits[2][j*8]     >0X00? 0X01 : 0x00); 
			dtx[j+16*2]|=(X_3bits[2][j*8 + 1] >0X00? 0X02 : 0x00); 
			dtx[j+16*2]|=(X_3bits[2][j*8 + 2] >0X00? 0X04 : 0x00); 
			dtx[j+16*2]|=(X_3bits[2][j*8 + 3] >0X00? 0X08 : 0x00); 
			dtx[j+16*2]|=(X_3bits[2][j*8 + 4] >0X00? 0X10 : 0x00); 
			dtx[j+16*2]|=(X_3bits[2][j*8 + 5] >0X00? 0X20 : 0x00); 
			dtx[j+16+2]|=(X_3bits[1][j*8 + 6] >0X00? 0X40 : 0x00); 
			dtx[j+16+2]|=(X_3bits[1][j*8 + 7] >0X00? 0X80 : 0x00); 
// Y(L) byte	
			dtx[j+2]|=(Y_3bits[0][j*8]     >0X00? 0X01 : 0x00); 
			dtx[j+2]|=(Y_3bits[0][j*8 + 1] >0X00? 0X02 : 0x00); 
			dtx[j+2]|=(Y_3bits[0][j*8 + 2] >0X00? 0X04 : 0x00); 
			dtx[j+2]|=(Y_3bits[0][j*8 + 3] >0X00? 0X08 : 0x00); 
			dtx[j+2]|=(Y_3bits[0][j*8 + 4] >0X00? 0X10 : 0x00); 
			dtx[j+2]|=(Y_3bits[0][j*8 + 5] >0X00? 0X20 : 0x00); 
			dtx[j+2]|=(Y_3bits[0][j*8 + 6] >0X00? 0X40 : 0x00); 
			dtx[j+2]|=(Y_3bits[0][j*8 + 7] >0X00? 0X80 : 0x00); 
// Y(M) byte
			dtx[j+16+2]|=(Y_3bits[1][j*8]     >0X00? 0X01 : 0x00); 
			dtx[j+16+2]|=(Y_3bits[1][j*8 + 1] >0X00? 0X02 : 0x00); 
			dtx[j+16+2]|=(Y_3bits[1][j*8 + 2] >0X00? 0X04 : 0x00); 
			dtx[j+16+2]|=(Y_3bits[1][j*8 + 3] >0X00? 0X08 : 0x00); 
			dtx[j+16+2]|=(Y_3bits[1][j*8 + 4] >0X00? 0X10 : 0x00); 
			dtx[j+16+2]|=(Y_3bits[1][j*8 + 5] >0X00? 0X20 : 0x00); 
			dtx[j+16+2]|=(Y_3bits[1][j*8 + 6] >0X00? 0X40 : 0x00); 
			dtx[j+16+2]|=(Y_3bits[1][j*8 + 7] >0X00? 0X80 : 0x00); 
// Y(H) byte
			dtx[j+16*2+2]|=(Y_3bits[2][j*8]     >0X00? 0X01 : 0x00); 
			dtx[j+16*2+2]|=(Y_3bits[2][j*8 + 1] >0X00? 0X02 : 0x00); 
			dtx[j+16*2+2]|=(Y_3bits[2][j*8 + 2] >0X00? 0X04 : 0x00); 
			dtx[j+16*2+2]|=(Y_3bits[2][j*8 + 3] >0X00? 0X08 : 0x00); 
			dtx[j+16*2+2]|=(Y_3bits[2][j*8 + 4] >0X00? 0X10 : 0x00); 
			dtx[j+16*2+2]|=(Y_3bits[2][j*8 + 5] >0X00? 0X20 : 0x00); 
			dtx[j+16*2+2]|=(Y_3bits[2][j*8 + 6] >0X00? 0X40 : 0x00); 
			dtx[j+16*2+2]|=(Y_3bits[2][j*8 + 7] >0X00? 0X80 : 0x00); 
		}

		for (i=0;i<48;i++) fprintf(dtx_fp,"%c",dtx[i]);
		d_t++;
	}
	fclose(dtx_fp);
	fclose(bfp);

	return 0;
	
}
