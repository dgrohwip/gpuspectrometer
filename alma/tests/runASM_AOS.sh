rm -f asm_drxp0.log /tmp/drxp0/asm_drxp0.log

# numactl -H

# Single CPU core
# numactl --localalloc --physcpubind 0 ../ASM_MC_service -c asm_AOStest.json $@ 

# Multiple CPU cores, on one physical processor
numactl --localalloc --cpunodebind 0 ../ASM_MC_service -c asm_AOStest.json $@ 
# numactl --localalloc --cpunodebind 1 ../ASM_MC_service -c asm_AOStest.json $@ 


