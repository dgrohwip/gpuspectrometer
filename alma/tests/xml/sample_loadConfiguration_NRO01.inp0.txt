
Decoding the key settings

   Integrate in one switch position for 1 second, have only one switch position

   <integrationDuration unit="s">1.000</integrationDuration>
   <channelAverageDuration unit="s">1.000</channelAverageDuration>

   Zero baseband offset: DC bin is loOffset[MHz]/basebandBW[MHz] = 0

   <loOffset unit="MHz">0</loOffset>

   Two spectral windows

        <spectralWindow polnProducts="XX,YY" windowFunction="UNIFORM" windowOverlap="0" simWinFunc="UNIFORM">
          <centerFrequency unit="MHz">3000</centerFrequency>
          <spectralAveragingFactor>128</spectralAveragingFactor>
          <name>SW-1</name>
          <effectiveBandwidth unit="MHz">1000</effectiveBandwidth>
          <effectiveNumberOfChannels>2048</effectiveNumberOfChannels>
          <quantizationCorrection>false</quantizationCorrection>
          <channelAverageRegion>
            <startChannel>0</startChannel>
            <numberChannels>2048</numberChannels>
          </channelAverageRegion>
        </spectralWindow>
        <spectralWindow polnProducts="XX,YY" windowFunction="UNIFORM" windowOverlap="0" simWinFunc="UNIFORM">
          <centerFrequency unit="MHz">3000</centerFrequency>
          <spectralAveragingFactor>64</spectralAveragingFactor>
          <name>SW-2</name>
          <effectiveBandwidth unit="MHz">500</effectiveBandwidth>
          <effectiveNumberOfChannels>2048</effectiveNumberOfChannels>
          <quantizationCorrection>false</quantizationCorrection>
          <channelAverageRegion>
            <startChannel>0</startChannel>
            <numberChannels>2048</numberChannels>
          </channelAverageRegion>
        </spectralWindow>
      </basebandConfig>
    </singleConfiguration>

   Windows have the same

      <centerFrequency unit="MHz">3000</centerFrequency>

   but SW-1 covers

      3000 MHz +- 1000/2 MHz with 2048 channels  from LFFT/128avg
 
      <effectiveBandwidth unit="MHz">1000</effectiveBandwidth>
      <effectiveNumberOfChannels>2048</effectiveNumberOfChannels>
      <spectralAveragingFactor>128</spectralAveragingFactor>

      with total power computed over one channelAverageRegion (ch0 to ch2048)

   while SW-2 covers

      3000 MHz +- 500/2 MHz with 2048 channels  from LFFT/64avg

      <effectiveBandwidth unit="MHz">500</effectiveBandwidth>
      <effectiveNumberOfChannels>2048</effectiveNumberOfChannels>
      <spectralAveragingFactor>64</spectralAveragingFactor>

      with total power computed over one channelAverageRegion (ch0 to ch2048)

  Assumption appears to be 

       basebandBW * (2048 / (LFFT/64)) = basebandBW * (131072/LFFT) = 500 MHz (SW-1)
       basebandBW * (2048 / (LFFT/128)) = basebandBW * (262144/LFFT) = 1000 MHz (SW-2)
       -->  basebandBW == 4000 MHz,  LFFT  == 2^20

       or vice versa,

       2048ch covers 1000 MHz = 1000/2048 MHz spectral resolution, after x128 avg of an original resolution of (1000/(2048*128)) = 3.815 kHz/ch
       2048ch covers 500 MHz = 500/2048 MHz Spectral resolution, after x64 avg of an original resolution of (5000/(2048*64)) = 3.815 kHz/ch
