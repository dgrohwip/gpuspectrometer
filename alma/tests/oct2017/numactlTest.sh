#!/bin/bash
# numactlTest.sh <drxpNr>

if [ "$1" == "0" ]; then
	numactl --physcpubind=0,2 --membind=0 test_drxpCuPinning /dev/drxpd0 0
fi

if [ "$1" == "1" ]; then
	numactl --physcpubind=4,6 --membind=0 test_drxpCuPinning /dev/drxpd1 1
fi

if [ "$1" == "2" ]; then
	numactl --physcpubind=22,24 --membind=1 test_drxpCuPinning /dev/drxpd2 2
fi

if [ "$1" == "3" ]; then
	numactl --physcpubind=26,28 --membind=1 test_drxpCuPinning /dev/drxpd3 3
fi

