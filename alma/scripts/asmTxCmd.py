#!/usr/bin/python
"""
Usage: tsmTxCmd.py [--txt] [--host=<hostname|ip>] [--port=portnr] [--save] <xml1> <xml2> <xml3>

Sends one or more XML command files to the specified host (default: localhost),
in plain text or in text with binary length field format (default: binary).

Each command file must contain only a single XML command; the schema file
that specifies the XML syntax does not allow multiple commands.

Waits about one second for a reply. Does not disconnect between commands.

Can save the response(s) into new files named "<xml1>.response" etc.
"""

import re, socket, struct, sys, time
from datetime import datetime
import xml.etree.ElementTree as ET
import xml.dom.minidom as minidom

binaryMode = True
remotePort = 1337
remoteHost = 'localhost'
doSave     = False

def total_seconds(td):
	# Keep backward compatibility with Python 2.6 which doesn't have this method
	if hasattr(td, 'total_seconds'):
		return td.total_seconds()
	else:
		return (td.microseconds + (td.seconds + td.days * 24 * 3600) * 1e6) / 1e6

def prettifyXML(x):
	x = x.replace('\r', '')
	x = x.replace('\n', '')
	x = re.sub(r'>\s+<', '><', x)
	reparsed = minidom.parseString(x)
	xml = reparsed.toprettyxml(indent='    ',newl='\n')
	return xml.rstrip()

def connect(host):
	try:
		sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		sock.connect(host)
		return sock
	except:
		print ('Could not connect to %s' % (str(host)))
		sys.exit(-1)

def sendXML(s,xml,binary):
	if not(binary):
		s.sendall(xml)
		s.sendall('\0')
	else:
		N = socket.htonl(len(xml))
		msg = struct.Struct('I').pack(N)
		msg += xml
		s.sendall(msg)

def recvXML(s,binary):
	if not(binary):
		xml = s.recv()
	else:
		buf = s.recv(4)
		if len(buf) < 4:
			print ('Receive error: could not read 32-bit response length field!')
			return
		N = struct.unpack('I', buf)
		N = socket.ntohl(N[0])
		xml = ''
		while len(xml) < N:
			d = s.recv(N - len(xml))
			xml = xml + d
	return xml

if len(sys.argv)<=1:
	print __doc__
	sys.exit(-1)

args = sys.argv[1:]
while (len(args)>0) and (args[0][0:2] == '--'):
	if args[0]=='--txt':
		binaryMode = False
	elif args[0]=='--save':
		doSave = True
	elif args[0][0:7]=='--host=':
		remoteHost = args[0][7:]
	elif args[0][0:7]=='--port=':
		remotePort = int(args[0][7:])
	else:
		print ('Unexpected argument: %s' % (args[0]))
	args = args[1:]
cmds = args

if (len(cmds) <= 0):
	print __doc__
	sys.exit(-1)

remoteHostAddr = (remoteHost,remotePort)
sock = connect(remoteHostAddr)
for cmdfile in args:
	with open(cmdfile) as F:
		xml_tx = F.read()
		xml_tx = xml_tx.replace('\n','')
		xml_tx = xml_tx.replace('\r','')
		xml_tx = xml_tx.replace('> <', '><')

		print (50*'=')
		print ('File: %s' % (str(cmdfile)))
		print ('Host: %s' % (str(remoteHostAddr)))
		print (50*'-')
		print ('Request:\n%s' % (prettifyXML(str(xml_tx))))
		print (50*'-')

		t1 = datetime.utcnow()
		sendXML(sock,xml_tx,binaryMode)
		xml_rx = recvXML(sock,binaryMode)
		t2 = datetime.utcnow()

		dT = t2 - t1
		dT_ms = total_seconds(dT) * 1000.0
		xml_rx = prettifyXML(str(xml_rx))
		if doSave:
			fo = open(cmdfile+'.response','w')
			fo.write(xml_rx + '\n')
			fo.close()

		print ('Response:\n%s' % (xml_rx))
		print (50*'-')
		print ('Latency: %.3f msec' % (dT_ms))
		print (50*'=')
		print ('')
