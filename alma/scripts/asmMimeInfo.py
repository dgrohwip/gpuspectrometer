#!/usr/bin/python
"""
Usage: asmMimeInfo.py [-h|--help] [-s|--summary] <asm_file.mime|asc_file.mime> [<asm_file.mime|asc_file.mime> ...]

Displays details on spectra in an ASM MIME file (mixed text and binary),
together with MD5 sums over the binary data.

The MD5s allow a checksum-based comparison of spectra generated e.g. from 3-bit
synthetic data during ASM testing.
"""
import email, hashlib, sys
import numpy as np
import xml.etree.ElementTree as ET

def summarizeAutoData(f32,rawstr):
	h = hashlib.md5()
	h.update(rawstr)
	md5 = h.hexdigest()
	print('%-15s   md5:%s' % ('',md5))
	print('%-15s   min:%f max:%f std:%f' % ('',np.amin(f32),np.amax(f32),np.std(f32)))

def apListStatistics(ap_list,print_dups=False,print_gaps=False):
	# Count un-ordered AP numbers
	# test: ap_list = [ 0, 1, 2, 10, 9, 11, 12, 13, 14, 3, 4, 5, 6, 7, 8 ]
	nunordered = 0
	i = 0; j = 1
	while (j < len(ap_list)):
		if ap_list[i] < ap_list[j]:
			j += 1; i = j - 1
		else:
			nunordered += 1
			j += 1

	# Count missing APs
	ap_list.sort()
	nmissing = len(ap_list) - (max(ap_list) - min(ap_list) + 1)

	# Count duplicate APs
	ap_counts = [ap_list.count(i) for i in set(ap_list)]
	nduplicate = sum(ap_counts) - len(ap_counts)

	# More verbose missings-list:
	for n in range(len(ap_list)-1):
		if print_dups and (ap_list[n+1] == ap_list[n]):
			print('Duplicate AP#%d' % (ap_list[n]))
		elif print_gaps and (ap_list[n+1] != (ap_list[n] + 1)):
			print('Gap AP#%d .. AP#%d' % (ap_list[n],ap_list[n+1]))

	return (nunordered,nmissing,nduplicate)

def processMIME(fn,opt):

	nautodata = 0
	ntext = 0
	ap_list = []

	# Parse file
	f = open(fn,'r')
	msg = email.message_from_file(f)
	for part in msg.walk():

		T = part.get_content_maintype()
		ST = part.get_content_subtype()
		D = part.get_payload(decode=True)
		LOC = part['Content-Location']
		if T == 'multipart':
		        continue

		if "sdmDataHeader.xml" in LOC:
			# Complete header from ASC
			ns = '{http://Alma/sdmDataObject/sdmbin}'
			xml = ET.fromstring(D)
			dinfo = xml.find(ns + 'dataStruct')
			ntext += 1
			if not opt['summary']:
				print ('%-15s : %-21s @%s contents %s' % ('sdmDataHeader.xml','%d byte in header,'%(len(D)),LOC,str(dinfo)))
		elif "dataHeader.xml" in LOC:
			# Lite header from ASM, contains no real info on channels etc
			ntext += 1
			if not opt['summary']:
				print ('%-15s : %-21s @%s' % ('dataHeader.xml','%d byte in header,'%(len(D)),LOC))
		elif "autoData.bin" in LOC:
			nautodata += 1
			if not opt['summary']:
				f32 = np.fromstring(D, dtype=np.float32)
				print ('%-15s : %-21s @%s' % ('autoData.bin','%d float32 values,'%(len(f32)),LOC))
				summarizeAutoData(f32,D)
		elif "desc.xml" in LOC:
			ntext += 1
			if not opt['summary']:
				print ('%-15s : %-21s @%s' % ('desc.xml','%d byte in header,'%(len(D)),LOC))
			fields = LOC.split('/')
			ap = int(fields[-2])
			ap_list.append(ap)
		else:
			if not opt['summary']:
				print ('%-15s : %d byte in entry,  @%s' % ('<other>',len(D),LOC))

	if opt['summary']:
		(nunordered,nmissing,nduplicate) = apListStatistics(ap_list, opt['verbosesummary'],opt['verbosesummary'])
		print ('%s : %d text entries, %d spectra : AP# series had %d missing, %d unordered, %d duplicates' % (fn,ntext,nautodata,nmissing,nunordered,nduplicate))

if __name__ == '__main__':
	opt = {}
	opt['summary'] = False
	opt['verbosesummary'] = False
	args = sys.argv[1:]
	while len(args)>0:
		if args[0] == '-h' or args[0] == '--help':
			print __doc__
			sys.exit(0)
		elif args[0] == '-s' or args[0] == '--summary':
			opt['summary'] = True
			args = args[1:]
		elif args[0] == '-v' or args[0] == '--verbose-summary':
			opt['summary'] = True
			opt['verbosesummary'] = True
			args = args[1:]
		elif args[0][:2] == '--' or args[0][0] == '-':
			print ('Unknown option: %s' % (args[0]))
			sys.exit(0)
		else:
			break

	for fn in args:
		processMIME(fn,opt)
