#!/bin/python
#
# Packages:
# $ yum install libffi-devel openssl-devel lshw lm_sensors lm_sensors-devel
# $ pip install python-hwinfo
# $ chmod u+s /sbin/dmidecode

import xml.etree.ElementTree as ET
import subprocess, sys

#####################################################################
# Non-critical helpers for debug/print

import xml.dom.minidom as minidom

def prettify_xml(x):
	rough_string = ET.tostring(x, 'utf-8')
	reparsed = minidom.parseString(rough_string)
	return reparsed.toprettyxml(indent="\t")

#####################################################################

class NvGPUInfo:

	def __init__(self):
		nvsmi = subprocess.Popen(['/usr/bin/nvidia-smi', '-q', '-x'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		nvsmi_xml, nvsmi_err = nvsmi.communicate()
		print nvsmi_err
		if nvsmi.returncode > 0:
			print 'NVIDIA SMI error ', nvsmi.returncode
			self.xml = None
		self.xml = ET.fromstring(nvsmi_xml)

	def grab(self):
		""" Return GPU information in an XML sub-element that follows the ASC_MC.xsd schema."""
		devnr = 0
		cardinfo = ET.Element('computingCards')
		for gpu in self.xml.iter('gpu'):
			util_elem = gpu.find('utilization')
			temp_elem = gpu.find('temperature')
			powr_elem = gpu.find('power_readings')
			errs_elem = gpu.find('ecc_errors').find('aggregate')

			gpuName = gpu.find('product_name').text
			gpuLoad = float(util_elem.find('gpu_util').text.strip(' %'))
			gpuTemp = float(temp_elem.find('gpu_temp').text.strip(' C'))
			gpuWatt = float(powr_elem.find('power_draw').text.strip(' W'))
			gpuEcc1 = errs_elem.find('single_bit').find('total').text.upper()
			gpuEcc2 = errs_elem.find('double_bit').find('total').text.upper()

			entry = ET.SubElement(cardinfo,'computingCard', {'deviceId':str(devnr)})
			se = ET.SubElement(entry, 'productName')
			se.text = str(gpuName)
			se = ET.SubElement(entry, 'currentTemperature', {'unit':'C'})
			se.text = str(gpuTemp)
			se = ET.SubElement(entry, 'currentPowerConsumption', {'unit':'W'})
			se.text = str(gpuWatt)
			if not((gpuEcc1=='N/A') or (gpuEcc2=='N/A')):
				gpuEcc = int(gpuEcc1) + int(gpuEcc2)
				se = ET.SubElement(entry, 'totalECCErrors')
				se.text = str(gpuEcc)
			devnr = devnr + 1
		return cardinfo

#####################################################################

class RxCardInfo:

	def __init__(self):
		# TODO: what is the "class" of DRXP?
		self.xml = ET.parse('xml_network')
		return
		hw = subprocess.Popen(['/usr/sbin/lshw', '-xml', '-class', 'network'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		hw_xml, hw_err = hw.communicate()
		print hw_err
		if hw.returncode > 0:
			print 'LSHW error ', hw.returncode
			self.xml = None
		self.xml = ET.fromstring(hw_xml)

	def grab(self):
		""" Return receiver card (DRXP, 10G, 40G) information in an XML sub-element that follows the ASC_MC.xsd schema."""
		devnr = 0
		cardinfo = ET.Element('receiverCards')
		for nic in self.xml.iterfind('node'):

			# Determine link speed
			speed = -1
			if nic.find('size')!=None:
				speed = int(nic.find('size').text)
			elif nic.find('capacity')!=None:
				speed = int(nic.find('capacity').text)
			if speed < 10e9:
				continue
			if speed < 40e9:
				mtype = '10GbE'
			else:
				mtype = '40GbE'

			# name = nic.find('product').text
			# iface = nic.find('logicalname').text
			# print iface, name, int(speed*1e-9), mtype

			attr = {'deviceId':str(devnr), 'numInterfaces':'1', 'mediatype':mtype}
			entry = ET.SubElement(cardinfo,'receiverCard', attr)
			devnr = devnr + 1
		return cardinfo

#####################################################################

nv = NvGPUInfo()
info = nv.grab()
print prettify_xml(info)

rx = RxCardInfo()
info = rx.grab()
print prettify_xml(info)


sys.exit(-1)

