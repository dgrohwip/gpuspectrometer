#!/usr/bin/python
import socket, struct, datetime

MCAST_GRP = '224.0.0.1'
MCAST_PORT = 30001 + 1
TERM_CLEAR = '\033[H\033[J'
TIMEOUT_SEC = 5.0

mreq = struct.pack("4sl", socket.inet_aton(MCAST_GRP), socket.INADDR_ANY)

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
sock.bind(('', MCAST_PORT))
sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)
sock.settimeout(TIMEOUT_SEC)

firstUTC = None

while True:
	try:
		(msg,peer) = sock.recvfrom(8192)
		print (TERM_CLEAR)
		nowUTC = datetime.datetime.utcnow()
		if firstUTC == None:
			firstUTC = nowUTC
		dT = (nowUTC - firstUTC).total_seconds()
		print ('ASM multicast from %s at %s UTC at %d sec since first message\n' % (peer[0], nowUTC, dT))
		print (msg)
	except socket.timeout:
		print ('No multicast UDP received in the last %.1d seconds to group %s:%d' % (TIMEOUT_SEC,MCAST_GRP,MCAST_PORT))
	except:
		print ('Done')
		break

