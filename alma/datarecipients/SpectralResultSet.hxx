/**
 *
 * \class SpectralResultSet
 *
 * Container for spectral result details, and wrapper to pointers where spectra are stored in memory.
 *
 */

#ifndef SPECTRALRESULTSET_HXX
#define SPECTRALRESULTSET_HXX

#include <time.h>
#include <sys/time.h>
#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/mutex.hpp>

class MCConfiguration;
class MCObservation;

typedef unsigned int hist_t; // TODO: is def in kernels/histogram_kernels.h

class SpectralResultSet {

    public:
        /** C'stor to initialize as empty */
        SpectralResultSet();

        /** D'stor */
        ~SpectralResultSet();

        /** Copy c'stor */
        SpectralResultSet(const SpectralResultSet&);

        /** Copy c'stor */
        SpectralResultSet(const SpectralResultSet&, bool copyRaw);

        bool user_owned_arrays; /// false if ownership of data arrays is within this object and arrays should be free'd in d'stor

    public:

        /** Store a copy of X, Y histograms */
        void storeHistogram(const hist_t*, const hist_t*);

        /** \return True if histogram has been stored */
        bool hasHistogram() { return histogram_is_set; }

    public:

        /** Allocate */
        void allocate(int nspec, size_t bytesperspec, size_t bytesinfinal);

        /** Check if raw arrays have been allocated
         * \return True if raw arrays have already been allocated
         */
        bool allocated() { return (!user_owned_arrays && spectra!=NULL); }

        /** Deallocate */
        void deallocate();

        /** Deallocate only the data used for raw spectra */
        void deallocateRaw();

        /** Deallocate only the flat storage used for post-processed spectra */
        void deallocatePostprocessed();

        /** \return True if dataset was marked as postprocessed */
        bool isPostprocessed() const { return postprocessed; }

        /** Mark dataset as having been postprocessed */
        void setPostprocessed() { postprocessed = true; }

    public:
        /** Create dummy data */
        void createDummyData(int nspectra, size_t bytesperspectrum);

    public:

        // Protection (lock externally)
        mutable boost::mutex lock;

        // Protection (lock internally)
        mutable boost::recursive_mutex opslock;

        // Metainfo
        int id;         /// a running sequence number for ASM-internal bookkeeping
        struct timespec timestamp; /// the timestamp of the first sample that contributed to data in the SpectralResultSet
        int total_msec;      /// the cumulative integration plus transition time rounded to milliseconds
        int integrated_msec; /// the cumulative integration time rounded to milliseconds (some rounding error since 2^20-pt FFT / 4e9 MSs/s = 262.14 usec per FFT)

        // Raw spectral data
        int nspectra;    /// number of raw spectra = number of switching positions
        float** spectra; /// pointers to raw spectra (nspectra x float*) of N switching positions, each spectrum is {autoX,crossXY,autoY}
        size_t bytesperspectrum; /// length of a raw spectrum in bytes (full-width spectrum, all polarizations)
        double weight;   /// data weight of every raw spectrum (TODO: make this per-spectrum?)

        // Histogram data
        hist_t histogram_X[8]; /// histogram of 3-bit data, X-pol; thought to apply to *all* switching positions
        hist_t histogram_Y[8]; /// histogram of 3-bit data, Y-pol; thought to apply to *all* switching positions
        bool histogram_is_set;

    public:

        // Configuration references
        const MCConfiguration* cfg;
        const MCObservation* obs;

    public:

        // Flat array of calibrated output spectra
        float* finalspectra; /// all spectra (spectral windows, spectrally averaged, calibrated, ...) in a single 1D array
        size_t bytesinfinalspectra; /// length of 'finalspectra' in bytes
        bool postprocessed;

};

#endif // SPECTRALRESULTSET_HXX
