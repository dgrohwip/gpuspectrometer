/**
 *
 * \class ResultRecipient
 *
 * An interface invoked by a spectral processor after the completion of
 * a new set of spectra from a single baseband (1-pol, or 2-pol auto, or 2-pol auto and cross).
 *
 */

#ifndef RESULTRECIPIENT_IFACE_HXX
#define RESULTRECIPIENT_IFACE_HXX

#include <string>

#include <stdlib.h>
#include <sys/time.h>

#include <boost/noncopyable.hpp>

class MCObservation;

class ResultRecipient : private boost::noncopyable {

    private:
        ResultRecipient();

    public:
        ResultRecipient(std::string outputPath) { }

        virtual ~ResultRecipient() { }

    public:
        /** Open the results recipient */
        virtual void open() = 0;

        /** Glean any internally required extra infos from an MCObservation; if used then call this before open() */
        virtual void configureFrom(const MCObservation*) = 0;

        /** Block until all results were written */
        virtual void sync() = 0;

        /** Safely close the results recipient */
        virtual void close() = 0;

    public:

        /** Callback-like function. Accepts a SpectralResultSet or other object. Apply data reordering and calibrations and store the data. */
        virtual void takeResult(const void* resultset) = 0;
};

//ResultRecipient::~ResultRecipient() { }

#endif // RESULTRECIPIENT_IFACE_HXX

