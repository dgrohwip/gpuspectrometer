#include "datarecipients/SpectralResultSet.hxx"
#include "mcwrapping/mc_configuration.hxx"

#include <iostream>
#include <string.h>
#include <sys/time.h>

int main(int argc, char** argv)
{
    SpectralResultSet* rs;
    SpectralResultSet* rs2;
    const int N = 1024;

    // Non-freeable data to test with
    const hist_t histDummy[8] = { 11, 101, 438, 912, 912, 438, 101, 11 };
    float staticData[N] = { 1.0f };

    // Dynamic allocated: by container
    rs = new SpectralResultSet();
    rs->allocate(2, N*sizeof(float), 0);
    rs->storeHistogram(histDummy, histDummy);
    delete rs;

    // Dynamic allocated: by user
    rs = new SpectralResultSet();
    rs->bytesperspectrum = N * sizeof(float);
    rs->nspectra = 2;
    rs->user_owned_arrays = true;
    rs->spectra = new float*[rs->nspectra];
    for (int s=0; s<rs->nspectra; s++) {
        rs->spectra[s] = new float[N];
    }
    rs->storeHistogram(histDummy, histDummy);
    for (int s=0; s<rs->nspectra; s++) {
        delete[] rs->spectra[s];
    }
    delete[] rs->spectra;
    delete rs;

    // Dynamic allocated: by user, then copy
    rs = new SpectralResultSet();
    rs->bytesperspectrum = N * sizeof(float);
    rs->nspectra = 2;
    rs->user_owned_arrays = true;
    rs->spectra = new float*[rs->nspectra];
    for (int s=0; s<rs->nspectra; s++) {
        rs->spectra[s] = new float[N];
    }
    rs->storeHistogram(histDummy, histDummy);

    // Copy cstor
    rs2 = new SpectralResultSet(*rs);
    assert(!rs2->user_owned_arrays);

    // Delete underlying data
    for (int s=0; s<rs->nspectra; s++) {
        delete[] rs->spectra[s];
    }
    delete[] rs->spectra;
    delete rs;

    rs2->deallocateRaw();
    rs2->deallocateRaw();

    // Dstor on copy of data
    delete rs2;

    // Static allocated
    rs = new SpectralResultSet();
    rs->bytesperspectrum = N * sizeof(float);
    rs->nspectra = 1;
    rs->user_owned_arrays = true;
    rs->spectra = new float*[rs->nspectra];
    for (int s=0; s<rs->nspectra; s++) {
        rs->spectra[s] = staticData;
    }
    rs->storeHistogram(histDummy, histDummy);

    rs->deallocateRaw(); // should do nothing
    rs->deallocateRaw();

    // Copy cstor
    rs2 = new SpectralResultSet(*rs);
    delete rs2;

    // Alternate copy cstor
    rs2 = new SpectralResultSet(*rs, true);
    delete rs2;
    rs2 = new SpectralResultSet(*rs, false);
    rs2->deallocate();
    rs2->deallocate();
    delete rs2;

    delete[] rs->spectra;
    delete rs;
}
