/**
 *
 * \class ResultRecipientNoop
 *
 * No-op dummy of ResultRecipient
 *
 */

#ifndef RESULTRECIPIENT_NOOP_HXX
#define RESULTRECIPIENT_NOOP_HXX

#include "ResultRecipient_iface.hxx"

#include <string>

class MCObservation;

class ResultRecipientNoop : public ResultRecipient
{

    public:
        ResultRecipientNoop(std::string outputPath) : ResultRecipient(outputPath) { }
        ~ResultRecipientNoop() {}

    public:
        /** Open the results recipient */
        void open() {}

        /** Glean any internally required extra infos from an MCObservation; if used then call this before open() */
        void configureFrom(const MCObservation*) {}

        /** Safely close the results recipient */
        void close() {}

    public:
        /** Callback-like function. Accepts a SpectralResultSet or other object. Apply data reordering and calibrations and store the data. */
        void takeResult(const void* resultset) { /* discard data */ }
};

#endif // RESULTRECIPIENT_HXX

