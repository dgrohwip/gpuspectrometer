/**
 *
 * \class ResultRecipientMIME
 *
 * Implements the ResultRecipient interface invoked by a spectral processor after the completion of
 * a new set of spectra from a single baseband (1-pol, or 2-pol auto, or 2-pol auto and cross).
 *
 * Output data are written in MIME format as defined in the internal ICD (ALMA-64.00.00.00-70.44.00.00-A-ICD).
 * The MIME message contains a global header, an a 'multipart/mixed' to which new results are appended.
 *
 * The spectral results set in MIME format created by the spectrometer is structured as:
 *
 * - MIME header
 * - MIME multipart level 1 as 'multipart/mixed'
 *   - dataHeader.xml as 'text/xml'
 *   - MIME multipart level 2 as 'multipart/related'
 *     - <ip_name>/desc.xml as 'text/xml'
 *     - <ip_name>/flags.bin as 'application/octet-stream'
 *     - <ip_name>/actualTimes.bin as 'application/octet-stream'
 *     - <ip_name>/actualDurations.bin as 'application/octet-stream'
 *     - <ip_name>/autoData.bin as 'application/octet-stream'
 *   - MIME multipart level 2 as 'multipart/related'
 *     - <ip_name>/desc.xml as 'text/xml'
 *     - <ip_name>/flags.bin as 'application/octet-stream'
 *     ...
 *
 * where <ip_name> consists of <observationId/antennaId/baseBandName/<%05d(ipNr)>
 * in which ipNr starts from 1 and increments by 1 for each added 'multipart/related'.
 *
 * The MIME file(s) from several spectrometer instances are later combined i
 * the ASC server into a final output file in ASM MIME format.
 */

#include "datarecipients/ResultRecipientMIME.hxx"
#include "datarecipients/SpectralResultSet.hxx"
#include "mcwrapping/mc_configuration.hxx" // class MCConfiguration
#include "mcwrapping/mc_observation.hxx"   // class MCObservation (has observation metadata and another copy of MCConfiguration)
#include "mime/ASMMime.hxx"
#include "helper/logger.h"
#include "global_defs.hxx"

#include <boost/noncopyable.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread.hpp>

#include <stdlib.h>
#include <sys/time.h>

#include <algorithm>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <sstream>

#define lockscope  // just a blank define use for nicer syntax of "{ boost::mutex::scoped_lock x(mtx); ... }" scopes

//////////////////////////////////////////////////////////////////////////////////////////////////////

class ResultRecipientMIME_impl;
class ResultRecipientMIME_impl : private boost::noncopyable
{
    friend class ResultRecipientMIME;

    boost::mutex mimemutex; /// a mutex to serialize access to the MIME encapsulator

    ASMMime* mime;    /// MIME encapsulator
    std::string root; /// root for 'Content-Location:' e.g. "xyz987654321/1/BB_1/"
    int seqNo;        /// TODO: replace by a sequence number passed externally into takeResult()
    bool bodyDone;    /// 'true' after MIME header is written

    int nQueued;            /// number of results not yet fully written out
    boost::condition_variable alldoneCond; /// signaled when no results are pending and relatively save to quit

    std::string obs_id;
    std::string obs_antenna;
    std::string obs_baseband;
    std::string obs_resoStr;

    std::string outfilepath;
    std::string outfilename;
    std::ofstream outfile;

    struct timespec first_timestamp;

    /** Detached thread launched by every call of takeResult(). Handles results calibration, reformatting, storage */
    void results_storer(SpectralResultSet*);

    /** C'stor */
    ResultRecipientMIME_impl() {
        seqNo = 1;
        nQueued = 0;
        obs_id = "";
        obs_antenna = "PM01";
        obs_baseband = "BB_1";
        obs_resoStr = "";
        mime = NULL;
        first_timestamp.tv_sec = 0;
        first_timestamp.tv_nsec = 0;
    }
};

//////////////////////////////////////////////////////////////////////////////////////////////////////

/** C'stor */
ResultRecipientMIME::ResultRecipientMIME(std::string outPath) : ResultRecipient(outPath)
{
    ResultRecipientMIME_impl* impl = new ResultRecipientMIME_impl;
    pimpl = (void*)impl;
    impl->outfilepath = outPath;
}

/** D'stor, blocks until reasonably sure all data were written */
ResultRecipientMIME::~ResultRecipientMIME()
{
    this->close();
    delete (ResultRecipientMIME_impl*)pimpl;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

/** Copy certain needed MIME infos from MCObservation; if used then call this before open() */
void ResultRecipientMIME::configureFrom(const MCObservation* obs)
{
    ResultRecipientMIME_impl* impl = (ResultRecipientMIME_impl*)pimpl;
    lockscope {
        boost::mutex::scoped_lock mmlock(impl->mimemutex);
        std::string of = obs->observationId + "_" + obs->antennaId + "_" + obs->baseBandName + ".mime";
        std::replace(of.begin(), of.end(), ':', '_');
        std::replace(of.begin(), of.end(), '*', '_');
        std::replace(of.begin(), of.end(), '?', '_');
        std::replace(of.begin(), of.end(), '/', '_');
        impl->outfilename = impl->outfilepath + of;
        impl->root = obs->observationId + "/" + obs->antennaId + "/" + obs->baseBandName + "/";
        impl->obs_id = obs->observationId;
        impl->obs_antenna = obs->antennaId;
        impl->obs_baseband = obs->baseBandName;
        impl->obs_resoStr = "FULL_RESOLUTION"; // TODO: check obs->configuration for channel averaging
        impl->first_timestamp.tv_sec = 0;
        impl->first_timestamp.tv_nsec = 0;
    }
}

/** Open the MIME file */
void ResultRecipientMIME::open()
{
    ResultRecipientMIME_impl* impl = (ResultRecipientMIME_impl*)pimpl;

    // Prepare MIME encapsulator
    if (impl->mime != NULL) {
        this->close();
    }
    #ifndef DEBUG_MIME_TO_STDOUT
    impl->outfile.open(impl->outfilename.c_str());
    impl->mime = new ASMMime(impl->outfile);
    L_(linfo) << "ResultRecipientMIME initialized MIME output to file " << impl->outfilename;
    #else
    impl->outfile.close();
    impl->mime = new ASMMime(std::cout);
    L_(linfo) << "ResultRecipientMIME initialized MIME output to stdout";
    #endif

    // Start a MIME header
    std::stringstream dhdr;
    dhdr << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
    dhdr << "<dataHeader>\n";
    dhdr << "   <observationId>" << impl->obs_id << "</observationId>\n";
    dhdr << "   <spectrometerInput>" << 0 /* TODO g.configfile.get<int>("hardware.assignedIF") */ << "</spectrometerInput>\n";
    dhdr << "   <spectralResolution>" << impl->obs_resoStr << "</spectralResolution>\n";
    dhdr << "</dataHeader>";
    lockscope {
        boost::mutex::scoped_lock mmlock(impl->mimemutex);
        impl->mime->beginMultipart("", false);
        impl->mime->addXML(dhdr.str().c_str(), "dataHeader.xml");
        impl->bodyDone = true;
    }
    L_(linfo) << "ResultRecipientMIME initialized the MIME body";
}

/** Block until all results were written */
void ResultRecipientMIME::sync()
{
    if (pimpl == NULL) {
        return;
    }

    // Wait for all dispatched writers to finish
    ResultRecipientMIME_impl* impl = (ResultRecipientMIME_impl*)pimpl;
    lockscope {
        boost::mutex::scoped_lock mmlock(impl->mimemutex);
        while (impl->nQueued > 0) {
            impl->alldoneCond.wait(mmlock);
        }
    }
}

/** Safely close the MIME file*/
void ResultRecipientMIME::close()
{
    if (pimpl == NULL) {
        return;
    }

    // Wait for all dispatched writers to finish
    sync();

    // Delete MIME encapsulator, close file
    ResultRecipientMIME_impl* impl = (ResultRecipientMIME_impl*)pimpl;
    if (impl->outfile.is_open()) {
        impl->mime->endMultipart();
        impl->outfile.close();
        L_(linfo) << "ResultRecipientMIME closed MIME output file " << impl->outfilename;
        return;
    }
    if (impl->mime == NULL)  {
        return;
    }
    delete impl->mime;
    impl->mime = NULL;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

void ResultRecipientMIME::takeResult(const void* resultset)
{
    // Increment the queue length, before (slow) copying
    lockscope {
        ResultRecipientMIME_impl* impl = (ResultRecipientMIME_impl*)pimpl;
        boost::mutex::scoped_lock mmlock(impl->mimemutex);
        impl->nQueued++;
    }

    // Copy the results into new temporary object
    const SpectralResultSet* res = (const SpectralResultSet*)resultset;
    SpectralResultSet* workset = new SpectralResultSet(*res, /*copyRaw=*/false);

    // Hand the copy to a worker that carries out post-processing (time consuming?) and final storage */
    lockscope {
        ResultRecipientMIME_impl* impl = (ResultRecipientMIME_impl*)pimpl;
        boost::mutex::scoped_lock mmlock(impl->mimemutex);
        boost::thread store(&ResultRecipientMIME_impl::results_storer, impl, workset);
        store.detach();

        if (impl->first_timestamp.tv_sec == 0) {
            impl->first_timestamp = res->timestamp;
        }
        float dT = (res->timestamp.tv_sec - impl->first_timestamp.tv_sec) + 1e-9*(res->timestamp.tv_nsec - impl->first_timestamp.tv_nsec);
        L_(linfo) << "ResultRecipientMIME dispatched " << (workset->nspectra * workset->bytesperspectrum) << "-byte raw and "
                  << res->bytesinfinalspectra << "-byte postprocessed data of time dT=" << std::fixed << std::setprecision(4) << dT << "s to output writer";
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

/** Detached thread launched by every call of takeResult(). Handles results calibration, reformatting, storage */
void ResultRecipientMIME_impl::results_storer(SpectralResultSet* workset)
{
    // Get our sequence number, increment the count
    int my_seq;
    lockscope {
        boost::mutex::scoped_lock mmlock(mimemutex);
        my_seq = seqNo;
        this->seqNo++;
    }

    // Use sequence number for MIME entry ID? Or use averaging period (switching cycle) number for ID?
    my_seq = workset->id;

    // Current entry base name ("<obsid>/<antnr>/<baseband>/seqno/") such as "uid://A002/xyz98/xf7c6/5fb1/1/BB_1/00001/"
    std::string entryname = this->root;
    entryname += static_cast<std::ostringstream&>(std::ostringstream() << std::dec << std::setw(5) << std::setfill('0') << my_seq).str();

    // Description desc.xml
    std::stringstream desc;
    desc << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
    desc << "<dataSubsetHeader xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:xsi=\"http://www.w3.org.2001/XMLSchema-instance\">\n";
    desc << "   <autoData xlink:href=\"" << entryname << "/autoData.bin\"/>\n";
    desc << "</dataSubsetHeader>";

    // Append data via MIME encapsulator
    lockscope {
        boost::mutex::scoped_lock mmlock(mimemutex);
        mime->beginMultipart("data and metadata subset");
        mime->addXML(desc.str().c_str(), entryname+"/desc.xml");
        if (workset->bytesinfinalspectra > 0) {
            mime->addBinary(workset->finalspectra, workset->bytesinfinalspectra, entryname+"/autoData.bin");
        } else {
            std::cerr << "ResultRecipientMIME : unexpectedly had workset->bytesinfinalspectra=" << workset->bytesinfinalspectra << std::endl;
            for (int s = 0; s < workset->nspectra; s++) {
                mime->addBinary(workset->spectra[0], workset->bytesperspectrum, entryname+"/autoData.bin");
            }
        }
        mime->endMultipart();
    }

    // Delete the work arrays created during dispatch in takeResult()
    delete workset;

    // Finished so can decrement the workers count, notify any listener if this was the last worker
    lockscope {
        boost::mutex::scoped_lock mmlock(mimemutex);
        if (this->nQueued >= 1) {
            this->nQueued--;
            if (this->nQueued == 0) {
                this->alldoneCond.notify_all();
            }
        }
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
