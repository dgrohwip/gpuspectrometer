/**
 * \class DataRecipient
 *
 * An interface invoked by DRXP upon each received 3-bit raw 48ms data chunk.
 * Interface needs to be implemented by CPU or GPU spectral processor class.
 */

#ifndef DATARECIPIENT_HXX
#define DATARECIPIENT_HXX

#include <stdlib.h>
#include <sys/time.h>

#include "drxp/RawDRXPDataGroup.hxx"

class DataRecipient {

    public:

        DataRecipient() { }

        /** Member function called by DRXP data pump (class DRXPIface) when new raw data are available. 
         * \return True to indicate takeData() releases the DRXP group, False if invoker must release group after takeData() returns. */
        virtual bool takeData(RawDRXPDataGroup data) = 0;
};

/** Dummy no-op implementation of DataRecipient */
class DataRecipientNoop : public DataRecipient {

    public:

        DataRecipientNoop() {}

        /** Member function called by DRXP data pump (class DRXPIface) when new raw data are available. 
         * \return True to indicate takeData() releases the DRXP group, False if invoker must release group after takeData() returns. */
        bool takeData(RawDRXPDataGroup data) { return true; /* discard data */ }
};

#endif // DATARECIPIENT_HXX
