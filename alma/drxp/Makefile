
include ../Makefile.inc

SYS_DEFS = -DUTILITY
EXTRA_INCS = -I.. -I../kernels/ -I$(DRXP_DIR)
EXTRA_LIBS = $(BOOSTLIB) -lm -lpthread ../xsd_datatypes/xsd_datatypes.a
DEBUG_FLAGS :=

##############################################################################################
## Sources for test programs
##############################################################################################

DRXP_SRC := DRXPDummy.cxx DRXPInfoFrame.cxx SFF8431_I2C.cxx DRXPPCIeLookup.cxx
ifeq ($(HAVE_DRXP),1)
    DRXP_SRC += DRXP.cxx
endif

DRXP_OBJS := $(DRXP_SRC:.cxx=.o)

AUX_OBJS := ../time/TimeUTC.o 
AUX_OBJS += ../mcwrapping/mc_configuration.o ../mcwrapping/mc_drxp_status.o ../mcwrapping/mc_observation.o
AUX_OBJS += ../datarecipients/ResultRecipientPlain.o ../datarecipients/SpectralResultSetManager.o ../datarecipients/SpectralResultSet.o
AUX_OBJS += ../spectralprocessors/SpectralProcessorGPU.o ../spectralprocessors/SpectralPostprocessor.o
AUX_OBJS += ../calibrations/CalibNonlinearity.o ../spectralprocessors/SpectralAverager.o

##############################################################################################
## Target test programs
##############################################################################################

TRY_TARGETS := try_DRXPInfoFrame try_DRXP_bitfields try_DRXPPCIeLookup try_DRXPDummy
ifeq ($(HAVE_DRXP),1)
    TRY_TARGETS += try_DRXP_cudaHostRegisterIoMemory try_DRXP
endif

all: $(DRXP_OBJS)

try: $(TRY_TARGETS)

try_DRXPInfoFrame: test/DRXPInfoFrame_try.o DRXPInfoFrame.o
	$(CXX) $(CXXFLAGS) -o $@ $^

try_DRXP: test/DRXP_try.o DRXP.o DRXPInfoFrame.o SFF8431_I2C.o
	$(CXX) $(ALMA_CXXFLAGS) -o $@ $^ $(AUX_OBJS) $(ALMA_LIBS)

try_DRXPDummy: test/DRXPDummy_try.o DRXPDummy.o DRXPInfoFrame.o SFF8431_I2C.o
	$(CXX) $(ALMA_CXXFLAGS) -o $@ $^ $(AUX_OBJS) $(ALMA_LIBS)

try_DRXP_bitfields: test/drxp_bitfields_try.o
	$(CXX) $(ALMA_CXXFLAGS) -o $@ $^

try_DRXP_cudaHostRegisterIoMemory: test/try_drxp_cudaHostRegisterIoMemory.cu
	$(NVCC) $(NVFLAGS) $(EXTRA_INCS) $(EXTRA_DEFS) $(DEBUG_FLAGS) -g $^ -o $@ $(EXTRA_LIBS)
	#sudo setcap cap_sys_admin=+ep $@

try_DRXPPCIeLookup: test/DRXPPCIeLookup_try.o DRXPPCIeLookup.o
	$(CXX) $(CXXFLAGS) -o $@ $^ $(EXTRA_LIBS)

clean:
	rm -rf $(TRY_TARGETS) *.o test/*.o *.co

install: $(TRY_TARGETS)
	mkdir -p $(INSTALL_PREFIX)/alma/bin/
	cp -af $(TRY_TARGETS) $(INSTALL_PREFIX)/alma/bin/

.PHONY: clean install all try
