/**
 * DRXP standard constants
 */
#ifndef DRXP_CONST_H
#define DRXP_CONST_H

#define DRXP_BLOCK_SIZE (0x400000)      // 4194304 byte per block, DRXP Driver Specification v1.0.7, section 2.1
#define DRXP_NUM_GROUPS_DEFAULT (16)    // default is 16, DRXP Driver Specification v1.0.7, section 2.1
#define DRXP_NUM_BLOCKS (35)            // always 35, DRXP Driver Specification v1.0.7, section 2.1

#define DRXP_RX_SIZE      (2348810240)  // (16*35*4194304) value that Elecs says to use for RX, for 16 groups
#define DRXP_RX_DATA_SIZE  (144000000)
#define DRXP_RX_IFRAME_SIZE      (48)
#define DRXP_RX_PADDING_SIZE (2800592)
#define DRXP_RX_GROUP_SIZE ((size_t)(DRXP_RX_DATA_SIZE+DRXP_RX_IFRAME_SIZE+DRXP_RX_PADDING_SIZE))

#define DRXP_TX_DATA_SIZE   (144000000) // DRXP Driver Specification v1.0.7, section 5.5.12, to use for DRXPD_SET_SNDSIZ
#define DRXP_TX_PADDING_SIZE  (2800640) // DRXP Driver Specification v1.0.7, section 2.2
#define DRXP_TX_SIZE (DRXP_TX_DATA_SIZE+DRXP_TX_PADDING_SIZE) // to use in mmap(), total 146800640 bytes
#define DRXP_GROUP_SIZE  (DRXP_TX_SIZE) // size of complete group, either receive or transmit

#endif // DRXP_CONST_H

