/**
 * Test code for cudaHostRegisterIoMemory in conjuction with DRXP character device mmap()'ed DRXP DMA memory.
 * Compile with/without 'USE_DRXP_RING_BUFFER' and 'USE_PINNED_MEMORY' defines below to change program behaviour.
 *
 * T. Nakamoto, 11Apr2018
 *
 * Running and checking results in Octave:
 *
 *     $ ./test_drxp_cudaHostRegisterIoMemory.cu > io.log
 *     $ octave
 *     d=dlmread('io.log','',2,1); 1e3*mean(d(:,1)), 1e6*std(d(:,1)), 1e3*mean(d(:,2)), 1e6*std(d(:,2))
 *
 * Transfer times with drxp0 and GPU#0 in milliseconds:
 *
 *                           USE_PINNED_MEMORY          !USE_PINNED_MEMORY
 *   USE_DRXP_RING_BUFFER    avg 12.56 ms, std 9 us     avg 21.80 ms, std 2009 us
 *  !USE_DRXP_RING_BUFFER    avg 12.63 ms, std 14 us    avg 21.43 ms, std 1504 us
 *
 */
#include <drxp/drxpd.h>
#include <fcntl.h>
#include <malloc.h>
#include <stdio.h>
#include <sys/eventfd.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#define GPU_NUM                0                     // Device Id of GPU to use (0..n-1)
#define DRXP_DEV_FILE          "/dev/drxpd0"         // DRXP char dev to use

#define DRXP_GROUP_NUM         (16)
#define DRXP_GROUP_DATA_SIZE   (144000000)           // 48ms data size in bytes
#define DRXP_GROUP_SIZE        ((size_t)0x8C00000u)  // 48ms data size in bytes plus padding to a power-of-2 multiple
#define DRXP_BLOCK_SIZE        (4194304)             // DMA block size that drxpd.ko driver uses internally when allocating memory
#define DRXP_BLOCK_NUM         (35)                  // DMA blocks per DRXP_GROUP_SIZE
#define GROUPS_TO_RCV          (64)

// Compile mode:
#define USE_DRXP_RING_BUFFER   // define to use mmap()'ed memory of DRXP character device, undefine to malloc() allocate an additional working memory area
#define USE_PINNED_MEMORY      // define to pin the memory (cudaHostRegister(cudaHostRegisterIoMemory) for char dev ring buffer, cudaHostRegister(cudaHostRegisterDefault) for malloc)

double timespec_diff(struct timespec *start, struct timespec *end) {
  return (double)(end->tv_sec - start->tv_sec) + (double)(end->tv_nsec - start->tv_nsec) * 1e-9;
}

int main(int argc, char *argv[]) {
  int verbosity = 0;
  int drxpd_fd = -1;
  int rcvevt_fd = -1;
  char *drxp_buffer = (char *)MAP_FAILED;
#ifndef USE_DRXP_RING_BUFFER
  char *user_buffer = NULL;
#endif
  char *gpu_buffer;
  int ret;
  int ioctl_ret;
  uint64_t u;
  int rcvevt_started = 0;
  int rcv_started = 0;
  int received_group_num = 0;
  struct timespec start_time;
  struct timespec end_time;
  cudaError_t cuda_ret;
  cudaEvent_t cu_start_time, cu_end_time;
  float cuda_dt_msec;

  int i;
  int j;

  printf("### %s compiled with: ", argv[0]);
#ifdef USE_DRXP_RING_BUFFER
  printf("USE_DRXP_RING_BUFFER ");
#else
  printf("!USE_DRXP_RING_BUFFER ");
#endif
#ifdef USE_PINNED_MEMORY
  printf("USE_PINNED_MEMORY\n");
#else
  printf("!USE_PINNED_MEMORY\n");
#endif

  /**********************************************
   * Initialize CUDA
   **********************************************/
  cudaSetDevice(GPU_NUM);
  cudaMalloc(&gpu_buffer, DRXP_GROUP_NUM * DRXP_GROUP_SIZE);
  cudaEventCreate(&cu_start_time);
  cudaEventCreate(&cu_end_time);

  /**********************************************
   * Open a DRXP device file and initialize DRXP
   **********************************************/
  drxpd_fd = open(DRXP_DEV_FILE, O_RDONLY);
  if (drxpd_fd == -1) {
    fprintf(stderr, "Failed to open " DRXP_DEV_FILE ".\n");
    goto ERROR;
  }

  rcvevt_fd = eventfd(0, 0);
  if (rcvevt_fd == -1) {
    fprintf(stderr, "Failed to create eventfd.\n");
    goto ERROR;
  }

  ret = ioctl(drxpd_fd, DRXPD_RCVGROUPS, DRXP_GROUP_NUM);
  if (ret == -1) {
    fprintf(stderr, "Failed to execute DRXPD_RCVGROUPS.\n");
    goto ERROR;
  }

  drxp_buffer = (char *)mmap(NULL,
                             DRXP_GROUP_SIZE * DRXP_GROUP_NUM,
                             PROT_READ,
                             MAP_SHARED,
                             drxpd_fd,
                             0);
  if (drxp_buffer == MAP_FAILED) {
    fprintf(stderr, "Failed to call mmap.\n");
    goto ERROR;
  }

  /**********************************************
   * Register DRXP Ring Buffer or separate Work Buf
   **********************************************/
#ifdef USE_DRXP_RING_BUFFER
#ifdef USE_PINNED_MEMORY
  /*
   * Register the memory allocated for DRXP ring buffer (DMA area) in CUDA
   * so that it can be considered as pinned memory. Registration is done
   * block by block (4 MiB) because the DRXP device driver allocates physical
   * memory for DMA transfer in units of 4 MiB.
   */
  for (i=0; i<DRXP_GROUP_NUM; i++) {
    for (j=0; j<DRXP_BLOCK_NUM; j++) {
      cuda_ret = cudaHostRegister(drxp_buffer + i * DRXP_GROUP_SIZE + j * DRXP_BLOCK_SIZE,
                                  DRXP_BLOCK_SIZE,
                                  cudaHostRegisterIoMemory);
      if (cuda_ret != cudaSuccess) {
        printf("Failed to execute cudaHostRegister: %s\n", cudaGetErrorString(cuda_ret));
        goto ERROR;
      } else if (verbosity > 0) {
        printf("cudaHostRegister: grp %d blk %d p=%p len=%d\n", i, j, drxp_buffer + i * DRXP_GROUP_SIZE + j * DRXP_BLOCK_SIZE, DRXP_BLOCK_SIZE);
      }
    }
  }
#endif /* USE_PINNED_MEMORY */
#else  /* USE_DRXP_RING_BUFFER */
  /*
   * Newly acquire the system memory of the same size as the DRXP ring buffer,
   * and register it in CUDA so that it can be considered as pinned memory
   * and allow DMA transfer from the system memory to GPU.
   */
  user_buffer = (char *)memalign(DRXP_BLOCK_SIZE, DRXP_GROUP_SIZE * DRXP_GROUP_NUM);

#ifdef USE_PINNED_MEMORY
  cuda_ret = cudaHostRegister(user_buffer,
                              DRXP_GROUP_SIZE * DRXP_GROUP_NUM,
                              cudaHostRegisterDefault);
#endif /* USE_PINNED_MEMORY */
#endif /* USE_DRXP_RING_BUFFER */

  /**********************************************
   * Prepare DRXP Reception
   **********************************************/
  ret = ioctl(drxpd_fd, DRXPD_SET_RCVEVT, rcvevt_fd);
  if (ret == -1) {
    fprintf(stderr, "Failed to execute DRXPD_SET_RCVEVT.\n");
    goto ERROR;
  }

  ret = ioctl(drxpd_fd, DRXPD_INIT, &ioctl_ret);
  if (ret == -1) {
    fprintf(stderr, "Failed to execute DRXPD_INIT.\n");
    goto ERROR;
  }

  ret = ioctl(drxpd_fd, DRXPD_RCVEVTSTART, &ioctl_ret);
  if (ret == -1) {
    fprintf(stderr, "Failed to execute DRXPD_RCVEVTSTART.\n");
    goto ERROR;
  }
  rcvevt_started = 1;

  /* Wait one event. */
  ret = read(rcvevt_fd, &u, sizeof(uint64_t));
  if (ret == -1) {
    fprintf(stderr, "Failed to get the first reception completion event.\n");
    goto ERROR;
  }

  /*********************************
   * Start receiving data from DRXP
   *********************************/
  ret = ioctl(drxpd_fd, DRXPD_RCVSTART, &ioctl_ret);
  if (ret == -1) {
    fprintf(stderr, "Failed to execute DRXPD_RCVSTART.\n");
    goto ERROR;
  }
  rcv_started = 1;

  for (received_group_num = 0; received_group_num < GROUPS_TO_RCV; received_group_num++) {

    ret = read(rcvevt_fd, &u, sizeof(uint64_t));
    if (ret == -1) {
      fprintf(stderr, "Failed to get the group #%d.\n", received_group_num + 1);
      goto ERROR;
    }

    /* skip checking whether more than 1 group received; assume always 1 */
    /* skip looking for actually received group; assume always incrementing from 0 */
    /* skip executing DRXPD_GET_RCVSTS and ignore OVERRUN status */

    /* Copy data of the received group from DRXP to GPU. */
    clock_gettime(CLOCK_MONOTONIC, &start_time);
    cudaEventRecord(cu_start_time);

#if defined(USE_DRXP_RING_BUFFER) && defined(USE_PINNED_MEMORY)

    #if 0
    // Attempt to copy as a whole 144 MB region --> CUDA Error 11 / invalid argument
    cuda_ret = cudaMemcpy(gpu_buffer + DRXP_GROUP_SIZE * (received_group_num % DRXP_GROUP_NUM),
                          drxp_buffer + DRXP_GROUP_SIZE * (received_group_num % DRXP_GROUP_NUM),
                          DRXP_GROUP_DATA_SIZE,
                          cudaMemcpyHostToDevice);
    if (cuda_ret != cudaSuccess) {
      printf("cudaMemcpy DRXP->GPU [full 144MB] = %d [%s]\n", cuda_ret, cudaGetErrorString(cuda_ret));
      goto ERROR;
    }
    #else
    // Attempt to copy as small 4MB pieces --> works
    for (j=0; j<DRXP_BLOCK_NUM - 1; j++) {
      cuda_ret = cudaMemcpyAsync(gpu_buffer + j * DRXP_BLOCK_SIZE + DRXP_GROUP_SIZE * (received_group_num % DRXP_GROUP_NUM),
                            drxp_buffer + j * DRXP_BLOCK_SIZE + DRXP_GROUP_SIZE * (received_group_num % DRXP_GROUP_NUM),
                            DRXP_BLOCK_SIZE,
                            cudaMemcpyHostToDevice,
                            0);
      if (cuda_ret != cudaSuccess) {
        printf("cudaMemcpy DRXP->GPU [@blk %d] = %d [%s]\n", j, cuda_ret, cudaGetErrorString(cuda_ret));
        goto ERROR;
      }
    }
    cudaStreamSynchronize(0);
    #endif

#else  /* defined(USE_DRXP_RING_BUFFER) && defined(USE_PINNED_MEMORY) */
#ifdef USE_DRXP_RING_BUFFER
    cuda_ret = cudaMemcpy(gpu_buffer + DRXP_GROUP_SIZE * (received_group_num % DRXP_GROUP_NUM),
                          drxp_buffer + DRXP_GROUP_SIZE * (received_group_num % DRXP_GROUP_NUM),
                          DRXP_GROUP_DATA_SIZE,
                          cudaMemcpyHostToDevice);
    if (cuda_ret != cudaSuccess) {
      printf("cudaMemcpy DRXP->GPU : cuda_ret = %d [%s]\n", cuda_ret, cudaGetErrorString(cuda_ret));
      goto ERROR;
    }
#else  /* USE_DRXP_RING_BUFFER */
    cuda_ret = cudaMemcpy(gpu_buffer + DRXP_GROUP_SIZE * (received_group_num % DRXP_GROUP_NUM),
                          user_buffer + DRXP_GROUP_SIZE * (received_group_num % DRXP_GROUP_NUM),
                          DRXP_GROUP_DATA_SIZE,
                          cudaMemcpyHostToDevice);
    if (cuda_ret != cudaSuccess) {
      printf("cudaMemcpy Malloc->GPU cuda_ret = %d [%s]\n", cuda_ret, cudaGetErrorString(cuda_ret));
      goto ERROR;
    }
#endif /* USE_DRXP_RING_BUFFER */
#endif /* defined(USE_DRXP_RING_BUFFER) && defined(USE_PINNED_MEMORY) */

    cudaEventRecord(cu_end_time);
    clock_gettime(CLOCK_MONOTONIC, &end_time);

    /* Print the time that copy operation took. */
    cudaEventSynchronize(cu_end_time);
    cudaEventElapsedTime(&cuda_dt_msec, cu_start_time, cu_end_time);
    printf("%3d %.5f %.5f\n", received_group_num + 1, timespec_diff(&start_time, &end_time), cuda_dt_msec*1e-3);

    ret = ioctl(drxpd_fd, DRXPD_CLR_RCV, received_group_num % DRXP_GROUP_NUM);
    if (ret == -1) {
      fprintf(stderr, "Failed to execute DRXPD_CLR_RCV for the group #%d.\n",
              received_group_num + 1);
      goto ERROR;
    }

  } // for(received_group_num)

ERROR:
  if (rcv_started) {
    ioctl(drxpd_fd, DRXPD_RCVSTOP, &ioctl_ret);
  }

  if (rcvevt_started) {
    ioctl(drxpd_fd, DRXPD_RCVEVTSTOP, &ioctl_ret);
  }

#ifdef USE_DRXP_RING_BUFFER
#ifdef USE_PINNED_MEMORY
  for (i=0; i<DRXP_GROUP_NUM; i++) {
    for (j=0; j<DRXP_BLOCK_NUM; j++) {
      cudaHostUnregister(drxp_buffer + i * DRXP_GROUP_SIZE + j * DRXP_BLOCK_SIZE);
    }
  }
#endif
#else /* USE_DRXP_RING_BUFFER */
#ifdef USE_PINNED_MEMORY
  cudaHostUnregister(user_buffer);
#endif
  free(user_buffer);
#endif /* USE_DRXP_RING_BUFFER */

  if (drxp_buffer != MAP_FAILED) {
    munmap(drxp_buffer, DRXP_GROUP_SIZE * DRXP_GROUP_NUM);
  }

  if (rcvevt_fd != -1) {
    close(rcvevt_fd);
  }

  if (drxpd_fd != -1) {
    close(drxpd_fd);
  }
}

