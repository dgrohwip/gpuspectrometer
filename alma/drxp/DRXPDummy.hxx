/**
 * \class DRXPIface
 *
 * \brief Low-level access to the DRXP via kernel driver and /dev/drxp[n] character device ioctl's
 *
 * \author $Author: janw $
 *
 */
#ifndef DRXP_DUMMY_HXX
#define DRXP_DUMMY_HXX

#include "drxp/DRXPIface.hxx"
#include "drxp/DRXPInfoFrame.hxx"
#include "drxp/SFF8431_infos.hxx"

#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/noncopyable.hpp>

#include <fstream>
#include <string>

class MCDRXPStatus;
class DataRecipient;

class DRXPDummy : public DRXPIface, private boost::noncopyable
{
    friend class MCDRXPStatus;

    public:
        /** Prepare DRXP object, without open any device yet (use selectDevice() for that) */
        DRXPDummy();

        /** Close the DRXP driver interface, without resetting anything */
        ~DRXPDummy();

    public:
        /** Change to another DRXP board */
        bool selectDevice(int iface_nr, bool open_rdonly=true);

        /** Change to another DRXP board */
        bool selectDevice(std::string dev_name, bool open_rdonly=true);

        /** Reset all flags and alarms */
        bool resetDevice();

        /** Reset all flags and alarms */
        bool resetAlarms();

        /** Reset internal synchronization engine on DRXP, forcing a hardware re-sync. */
        bool resetSync();

        /** Reset all checksums */
        bool resetChecksums();

        /** Read out all device information, flags, alarms, and statuses */
        bool deviceReadout(MCDRXPStatus& mc);

        /** Read out board version information */
        bool versionReadout(MCDRXPStatus& mc);

        /** Read out DFR Status */
        bool dcmstatusReadout(MCDRXPStatus& mc);

        /** Read out FPGA related voltage alarms (FPGA, on-FPGA MGT, Vddr) and SFP+ 3.3V voltage warnings/alarms */
        bool voltageReadout(MCDRXPStatus& mc);

        /** Read out all counters */
        bool counterReadout(MCDRXPStatus& mc);

        /** Read out optical power measuremets and warnings/alarms */
        bool opticalReadout(MCDRXPStatus& mc);

        /** Read out ALMA frame decoder sync status and metaframe delay */
        bool syncReadout(MCDRXPStatus& mc);

        /** Read out DRXP Receive Status */
        bool receiveStatusReadout(MCDRXPStatus& mc);

        /** Read out DRXP Send Status */
        bool sendStatusReadout(MCDRXPStatus& mc);

        /** Read out DRXP Temperatures */
        bool temperaturesReadout(MCDRXPStatus& mc);

        /** Read out DRXP FPGA Single Event Upset error counts and status */
        bool fpgaSEUReadout(MCDRXPStatus& mc);

        /** Start periodic monitoring. Periodically updates the referenced MCDRXPStatus and multicasts an ASCII summary string */
        bool startPeriodicMonitoring(const char* mgrp, int mport, MCDRXPStatus& mc, float interval_secs=5.0f);

        /** Stop periodic monitoring. */
        void stopPeriodicMonitoring();

    public:
        /** Start transmission of test pattern (TODO properly) */
        bool startTx();

        /** Stop transmission of test pattern */
        bool stopTx();

    public:
        /** Attach a data processing recipient that gets the received data once/while RX is running.
         * The recipient object must provide a function 'takeData(data_t*)' into which DRXP RX data will be pushed.
         */
        bool attachRxRecipient(DataRecipient* dr);

        /** Detach a data processing recipient */
        bool detachRxRecipient() { return attachRxRecipient(NULL); }

        /** In "polling" mode allow user to check for an available data group */
        bool getGroup(int* group, void** data, DRXPInfoFrame* meta);

        /** In "polling" mode allow user to free a data group back to the DRXP */
        bool releaseGroup(int);

        /** Return the cumulative nr of overruns since start */
        int getOverruns();

        /** Start DRXP RX data pump (thread).
         * The data pump loopRxThread() waits for a new group of 48ms of raw data.
         * The new group is held "reserved" and is passed to the recipient via takeData().
         * The recipient should transfer the data (DMA onto GPU) and block until the transfer completes.
         * Once takeData() returns, the data pump "releases" the group, allowing DRXP DMA to fill it with new data later.
         */
        bool startRx();

        /** Stop data reception */
        bool stopRx();

        /** Change the granularity of software timestamp re-alignment */
        void setTimestampGranularity(int granularity_msec);

    public:
        /** Get pointers and length of all 'groups' in raw DRXP data area. Intended to help (un)pinning in CUDA. */
        bool getBufferPtrs(size_t* Ngroups, void** groupbufs, size_t* len);

    public:
        /** Loads contents of file (48ms data + Info Frame) and serves that as fake rx/tx data.
         *  Call after startRx() or startTx().
         */
        bool loadPattern(const char* filename);

        /** Start debug output into a file */
        bool startDebugLogging(const char* filename);

        /** Stop debug output */
        bool stopDebugLogging();

    private:
        /** Data pump thread. Receives new data blocks from DRXP and pushes them into a DataRecipient object,
         * the object is allowed to be changed during RX, as long as it is NULL (invalid) or a valid object.
         */
        void rxDatapumpThread(DataRecipient* volatile* ppdr);

    private:
        /** Open a DRXP device in receive (rx=true) or transmit (rx=false) mode */
        void deviceOpen(bool rx);

        /** Close a DRXP device */
        void deviceClose();

        /** Background thread that multicasts the contents of given MCDRXPStatus at regular intervals */
        void monitoringThreadFunc(const char* mgrp, int mport, MCDRXPStatus*, float interval_secs);

    public:
        /** Read out internal SFP+ transceiver information of given slot (0, 1, or 2) via the I2C bus. */
        bool deviceGetSFPInfos(const int slot, SFF8431_Xceiver_infos_tt& xi);

        /** Get a copy of SFP+ transceiver information (slot 0) that was read out once in c'stor. */
        const SFF8431_Xceiver_infos_t& sfpInfosSlot0() const { return m_sfpInfos[0]; }

        /** Get a copy of SFP+ transceiver information (slot 2) that was read out once in c'stor. */
        const SFF8431_Xceiver_infos_t& sfpInfosSlot1() const { return m_sfpInfos[1]; }

        /** Get a copy of SFP+ transceiver information (slot 3) that was read out once in c'stor. */
        const SFF8431_Xceiver_infos_t& sfpInfosSlot2() const { return m_sfpInfos[2]; }

    private:
        boost::mutex m_iomutex; /// mutex for multithreaded access to a DRXP

        boost::thread* m_monitoringThread; /// thread that is running monitoringThreadFunc()
        bool m_shutdown_mon_thread;        /// 'true' to signal the monitoring thread to stop

        boost::thread* m_rxDatapumpThread; /// thread that is running rxDatapumpThread()
        bool m_shutdown_rx_thread;         /// 'true' to signal the RX thread to stop

        DRXPIface::UserMode m_usermode; /// PUSH or POLLING
        std::string m_dev;      /// character device path e.g. /dev/drxpd0
        size_t m_maddr_size;    /// size of memory mapped DRXP DMA buffer
        void* m_maddr;          /// memory mapped DRXP DMA buffer
        void* m_simpattern;     /// stores a 48ms long pattern for simulated-Rx mode
        bool m_rx_mode;         /// 'true' if DRXP in receive mode
        bool m_rxtx_started;    /// 'true' if data pump started
        int m_curr_group;
        int m_group_busy[64];
        unsigned int m_naccepts;/// running count of accepted frames
        unsigned int m_ndiscards;/// running count of discarded frames (no recipient)
        unsigned int m_noverruns;/// running count of overrun conditions
        int m_fd;               /// file descriptor of opened m_dev path
        int m_evtfd;            /// event file descriptor to wait for events on m_dev, see DRXP User Manual
        DataRecipient* m_rx_recipient;

        bool m_debugFileEnabled;     /// 'true' if debug output should be written to a file
        std::ofstream m_debugFile;   /// file for debug output

        DRXPInfoFrame infoframe;

        unsigned long m_timestamp_granularity_ns; /// Granularity to use when adjusting software timestamp to closest 48ms/1PPS/other hardware timebase
        struct timespec m_reference_time; /// UTC base time for re-gridding DRXP software timestamps onto 48ms grid based on hardware frame counter
        unsigned long m_reference_tickcount; /// base tick count at m_reference_time
        bool m_reference_set;

        SFF8431_Xceiver_infos_t m_sfpInfos[3]; /// SFP+ transceiver details in the three slots of the DRXP prototype board version

};

#endif // DRXP_DUMMY_HXX
