#ifndef SFF8431_I2C_H
#define SFF8431_I2C_H

// For details on I2C addresses and data fields,
// see https://www.finisar.com/sites/default/files/resources/AN_2030_DDMI_for_SFP_Rev_E2.pdf

/** Structure for SFF-8431 I2C address 0Ah data fields; 256 bytes in total */

typedef struct sff8431_A0h_fields_tt {

    /* Base fields */
    unsigned char identifier;      // 0=unknown, 1=GBIC, 2=soldered on board, 3=SFP/SFP+, ..., 12=QSFP, 13=QSFP+, 14=CXP
    unsigned char extended_identifier;
    unsigned char connector_type;  // 0=unknown, 1=SC, 2=FC Style 1, 3=FC Syle 2, ..., 7=LC, 8=MT-RJ, ...
    unsigned char transceiver[8];  // byte3 : bit4=10GBASE-SR, bit5=10GBASE-LR, bit6=10GBASE-LRM, bit7=10GBASE-ER
    unsigned char encoding;        // 1=8b10b, 2=4b5b, 3=NRZ, 4=Manchester, 5=SONET scrambled, 6=64b66b
    unsigned char bitrate_100M;    // rate in units of 100Mbit/sec
    unsigned char rate_id;         // 1=SFF-8079, 2=SFF-8431 8/4/2G RX, 4=SFF-8431 8/4/2G TX, 6=SFF-8431 8/4/2G RX+TX, ...
    unsigned char max_9um_len_km;  // maximum link length in km, for 9/125um fiber
    unsigned char max_9um_len_m;   // maximum link length in meters, for 9/125um fiber
    unsigned char max_OM2_len_10m; // maximum link length in units of 10m, for 50/125um OM2 fiber
    unsigned char max_OM1_len_10m; // maximum link length in units of 10m, for 62.5/125um OM1 fiber
    unsigned char max_dac_len_m;   // maximum link length in m, for Direct Attach copper
    unsigned char max_50um_len_10m;// maximum link length in units of 10m, for 50/125um fiber
    char vendor_name[16];
    unsigned char trensceiver_opt; // "Code for optical compatibility (TBD)"
    unsigned char oui[3];          // SFP vendor IEEE company ID
    char partnumber[16];           // Part number provided by SFP vendor (ASCII)
    char revision[4];              // Revision level for part number provided by vendor (ASCII)
    unsigned char wavelength[2];
    unsigned char unallocated1;
    unsigned char CC_BASE;         // checksum of raw bytes[0..62] above

    /* Extended fields */
    unsigned char options[2];
    unsigned char bitrate_max_pct; // Upper bit rate margin, units of %
    unsigned char bitrate_min_pct; // Lower bit rate margin, units of %
    char serialnumber[16];
    char datecode[8];              // [0..1]=year since 2000, [2..3]=month since January, [4..5]=day of month, [6..7] = vendor specific
    unsigned char monitoring_type;
    unsigned char enhanced_opts;
    unsigned char compliance;      // "Indicates which revision of SFF-8472 the transceiver complies with."
    unsigned char CC_EXT;

    /* Vendor specific or reserved fields */
    unsigned char vendor_eeprom[32];
    unsigned char reserved_sff8079[128];

} sff8431_A0h_fields_t;

extern const char *g_SFF8431_identifier_names[16];

#endif // SFF8431_I2C_H
