
const char *g_SFF8431_identifier_names[16] = {
   "unknown transceiver form factor",
   "GBIC", "transceiver soldered on board", "SFP/SFP+",
   "300 pin XBI", "XENPAK", "XFP", "XFF", "XFP-E",
   "XPAK", "X2", "DWDM-SFP", "QSFP", "QSFP+", "CXP",
   "unknown transceiver form factor"
};

