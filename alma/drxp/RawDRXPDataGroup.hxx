/**
 * \class RawDRXPDataGroup
 *
 * Container for data pointers and additional information related to one DRXP data group,
 * used for passing a new received DRXP group via ::takeData() to a data recipient.
 *
 */
#ifndef RAWDRXPDATAGROUP_HXX
#define RAWDRXPDATAGROUP_HXX

class DRXPInfoFrame;
class DRXPIface;

struct RawDRXPDataGroup
{
    const DRXPInfoFrame* frameinfo;   /// Meta info; timestamp
    const void* buf;                  /// Pointer to CPU-side buffer containig DRXP group data
    int bufSize;                /// Number of bytes available in the buffer
    double weight;              /// Data weight

    DRXPIface* originator;      /// Source DRXP object
    int group;                  /// Group number; can call "originator->releaseGroup(group)" with this info
};

#endif
