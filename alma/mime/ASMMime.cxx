/**
 * \class ASMMime
 *
 * \brief Creates MIME data in ASM MIME format (tiny subset of MIME) as specified in TP Spectrometer ICD.
 *
 */

#include "mime/ASMMime.hxx"

#include <iostream>
#include <string>
#include <cstring>

/** Create ASM MIME writer and direct its output to the given ostream */
ASMMime::ASMMime(std::ostream& o) : m_out(o), lf("\r\n")
{
    m_level = 0;
    m_multipart_count = 0;
    memset(m_level_ids, 0, sizeof(m_level_ids));
    m_out << "MIME-Version: 1.0" << lf;
}

/** Start a new ASM MIME 'multipart/related' grouping starting with default type 'text/xml' */
void ASMMime::beginMultipart()
{
    beginMultipart("data and metadata subset");
}

/** Start a new ASM MIME 'multipart/related' grouping starting with default type 'text/xml' */
void ASMMime::beginMultipart(const char* contentdescr, bool related)
{
    if (m_level > 0) {
        // Begin with '--MIME_boundary-n'
        m_out << "--MIME_boundary-" << m_level << lf;
    }
    if (m_level < MIME_MAX_LEVELS) {
        m_level++;
    }
    m_multipart_count++;
    m_level_ids[m_level] = m_multipart_count;
    if (related) {
        m_out << "Content-Type: multipart/related; boundary=\"MIME_boundary-" << m_level_ids[m_level] << "\"; type=\"text/xml\";" << lf;
    } else {
        m_out << "Content-Type: multipart/mixed; boundary=\"MIME_boundary-" << m_level_ids[m_level] << "\"; type=\"text/xml\";" << lf;
    }
    if (strlen(contentdescr) > 0) {
        m_out << "Content-Description: " << contentdescr << lf;
    }
    // TODO: RFC1341 says there must be a blank like between the above header and the following data. But ASC MIME example files don't have it.
    m_out << lf;
}

/** End a ASM MIME 'multipart/related' grouping opened earlier with beginMultipart(). */
void ASMMime::endMultipart()
{
    if (m_level > 0) {
        // End with '--MIME_boundary-n--' i.e. with an extra '--' after boundary sig
        m_out << "--MIME_boundary-" << m_level_ids[m_level] << "--" << lf;
        m_level--;
    }
}

/** Add a MIME part that is in XML format */
void ASMMime::addXML(const char* xml, const std::string& loc)
{
    if (m_level > 0) {
        m_out << "--MIME_boundary-" << m_level_ids[m_level] << lf;
    }
    m_out << "Content-Type: text/xml; charset=\"UTF-8\"" << lf;
    m_out << "Content-Transfer-Encoding: 8bit" << lf;
    m_out << "Content-Location: " << loc << lf;
    // TODO: RFC1341 says there must be a blank like between the above header and the following data. But ASC MIME example files don't have it.
    m_out << lf;
    m_out << xml << lf;
}

/** Add a MIME part that is in XML format */
void ASMMime::addBinary(const void* octets, size_t len, const std::string& loc)
{
    if (m_level > 0) {
        m_out << "--MIME_boundary-" << m_level_ids[m_level] << lf;
    }
    m_out << "Content-Type: application/octet-stream" << lf;
    m_out << "Content-Location: " << loc << lf;
    // TODO: RFC1341 says there must be a blank like between the above header and the following data. But ASC MIME example files don't have it.
    m_out << lf;
    m_out.write((const char*)octets, len);
    m_out << lf;
}
