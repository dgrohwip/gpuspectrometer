/////////////////////////////////////////////////////////////////////////////////////
//
// Kernels for 4-baseband cross-correlation, after FFT
// Must have a number of channels that is a multiple of 32
//
// (C) 2016 Jan Wagner
//
/////////////////////////////////////////////////////////////////////////////////////
//
// Data layout for FX
//
// == Single-pol ==
// Input:  [ ch0 ant1 re,im | ch0 ant2 re,im | ch0 ant3 re,im | ch0 ant4 re,im  |  ch1 ant1 re,im | ch1 ant2 re,im ... ]
//           = [ {ant1,ant2,ant3,ant4 <re,im>} x Nch } x Nfft
//
// == Dual-pol ==
// Input:  [ ch0 ant1 X re,im | ch0 ant1 Y re,im | ch0 ant2 X re,im | ch0 ant2 Y re,im ... ]
//           = [ {ant1 X, ant1 Y, ant2 X, ant2 Y, ant3 X, ant3 Y, ant4 X, ant4 Y ; all as <re,im>} x Nch } x Nfft
//
// Output:
//
// GPU_OUT_LAYOUT == 1 : baselines/polpairs first, [ {<re,im> x {XX,XY,YX,YY on bl1,bl2,bl3,bl4,bl5,bl6}} x Nch ]
// GPU_OUT_LAYOUT == 2 : channels and Re-part first then Im-part, [{ch1,ch2,...,chN} x {XX,XY,YX,YY on bl1,bl2,bl3,bl4,bl5,bl6} ] x {re, im}
// GPU_OUT_LAYOUT == 3 : channels first, Re&Im together [<re,im> x {ch1,ch2,...,chN} x {XX,XY,YX,YY on bl1,bl2,bl3,bl4,bl5,bl6} ]
//
// Benchmark of 4-ant 2-pol in full Stokes with Autos on Titan V in Volta arch
// Layout  Channels   Throughput over 5'000 iterations [Ms/s/ant/pol]
//      1        64   2950.6 +- 47.1   min 2665.4 max 2989.6
//      1       128   3718.8 +- 85.7   min 3370.0 max 3790.4
//      1       256   4165.6 +- 27.7   min 4025.8 max 4247.7
//      1       512   4719.5 +- 45.2   min 4543.2 max 4856.7
//      1      1024   4692.3 +- 47.5   min 4572.8 max 4834.2
//      1      2048   4529.3 +- 65.0   min 4340.5 max 4713.9
//      1      4096   4100.6 +- 83.2   min 3930.8 max 4341.7
//      1      8192   3585.1 +- 22.6   min 3512.4 max 3641.6
//      1     16384   2872.6 +- 19.2   min 2817.5 max 2923.2
//
//      2        64   8316.5 +- 98.0  min 7520.2 max 8674.5
//      2       128   8777.2 +- 49.8  min 8314.5 max 8977.3
//      2       256   8922.3 +- 66.9  min 8237.2 max 9132.8
//      2       512   9018.9 +- 57.7  min 8396.6 max 9227.2
//      2      1024   8748.3 +- 53.1  min 8124.8 max 8976.7
//      2      2048   8017.1 +- 56.6  min 7587.9 max 8177.0
//      2      4096   7284.8 +- 37.4  min 7025.5 max 7386.9
//      2      8192   6489.9 +- 41.1  min 6245.2 max 6616.9
//      2     16384   5532.6 +- 34.4  min 5441.1 max 5624.3
//
//      3        64   5865.0 +- 50.4  min 5666.4 max 6003.8
//      3       128   7558.6 +- 72.7  min 7266.6 max 7816.4
//      3       256   8101.5 +- 81.9  min 7672.9 max 8305.1
//      3       512   8203.5 +- 121.8 min 7789.6 max 8509.8
//      3      1024   8288.8 +- 125.5 min 7594.6 max 8628.5
//      3      2048   7534.5 +- 116.6 min 7083.7 max 7944.0
//      3      4096   6707.4 +- 61.2  min 6230.2 max 6891.4
//      3      8192   5740.8 +- 31.1  min 4819.4 max 5849.2
//      3     16384   4572.4 +- 68.5  min  153.7 max 4696.9
//
/////////////////////////////////////////////////////////////////////////////////////
//
// Baseline indices for antenna-pairs (antennas a,b,c,d) including autocorrelations are:
//      a  b  c  d
//    a 0  1  2  3
//    b    4  5  6
//    c       7  8
//    d          9
//
// Baseline indices for antenna-pairs (antennas a,b,c,d) without autocorrelations are:
//      a  b  c  d
//    a -  0  1  2
//    b    -  3  4
//    c       -  5
//    d          -
//
/////////////////////////////////////////////////////////////////////////////////////

#include <assert.h>
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <cuda.h>

/////////////////////////////////////////////////////////////////////////////////////

// Reference
void cpu_fxcorr_1pol(const float2* interleaved_data_in, float2* out, const int Nant, const size_t Nsamp_per_antenna, const int Nchan);
void cpu_fxcorr_1pol_withAuto(const float2* interleaved_data_in, float2* out, const int Nant, const size_t Nsamp_per_antenna, const int Nchan);
void cpu_fxcorr_2pol(const float4* interleaved_data_in, float2* out, const int Nant, const size_t Nsamp_per_antenna, const int Nchan);
void cpu_fxcorr_2pol_withAuto(const float4* interleaved_data_in, float2* out, const int Nant, const size_t Nsamp_per_antenna, const int Nchan);
void cpu_fxcorr_2pol_withAuto(const float2* split_data_in[8], float2* out, const int Nant, const size_t Nsamp_per_antenna, const int Nchan);

// GPU output data layout
static const int GPU_OUT_LAYOUT = 2; // 1 (same as CPU ref), 2 (ideal with split Re&Im), or 3 (compromise with paired Re&Im)

// GPU
void prep_cu_fxcorr_4ant(size_t &nblk, size_t &nthr, const size_t maxphysthreads, size_t Nsamp_per_antenna, size_t Nchan);
__global__ void cu_fxcorr_4ant_1pol(const float2* __restrict__ in, float2* __restrict__ out, const size_t Nsamp_per_antenna, const size_t Nchan);
__global__ void cu_fxcorr_4ant_1pol_withauto(const float2* __restrict__ in, float2* __restrict__ out, const size_t Nsamp_per_antenna, const size_t Nchan);
__global__ void cu_fxcorr_4ant_2pol(const float4* __restrict__ in, float2* __restrict__ out, const size_t Nsamp_per_antenna, const size_t Nchan);
__global__ void cu_fxcorr_4ant_2pol_withAuto(const float4* __restrict__ in, float2* __restrict__ out, const size_t Nsamp_per_antenna, const size_t Nchan);
__global__ void cu_fxcorr_4ant_2pol_withAuto(
    const float2* __restrict__ in1X, const float2* __restrict__ in1Y,
    const float2* __restrict__ in2X, const float2* __restrict__ in2Y,
    const float2* __restrict__ in3X, const float2* __restrict__ in3Y,
    const float2* __restrict__ in4X, const float2* __restrict__ in4Y,
    float2* __restrict__ out, const size_t Nsamp_per_antenna, const size_t Nchan);
    // note: "kernel(float* ptrArray[8])" does not work easly in CUDA
     //      see https://stackoverflow.com/questions/1835537/cuda-allocating-array-of-arrays/1887312#1887312

// Wrapper for cu_fxcorr_4ant_2pol_withAuto() with to support "kernel(float* ptrArray[8])"
void fxcorr_4ant_2pol_withAuto(int numBlocks, int threadsPerBlock, const float2* d_signals[8], float2* d_out, const size_t Nsamp_per_antenna, const size_t Nchan);

// Reshuffling of GPU output layout (also convenient) into the layout expected by CPU reference
void cu_fxcorr_4ant_reshuffle(const float2* vgpu, float2* vcpu_like, size_t Nant, size_t Nchan, size_t Npolprod, bool withAuto);

// CPU vs GPU result comparison
static bool compare(const float2* vcpu, const float2* vgpu, int Nant, int Nchan, bool withAuto);
static bool compare4stokes(const float2* vcpu, const float2* vgpu, int Nant, int Nchan, bool withAuto);

/////////////////////////////////////////////////////////////////////////////////////
// Cross-products
/////////////////////////////////////////////////////////////////////////////////////

// Complex conj prod: (a + ib) * (c + id)' = (a + ib) * (c - id) = ac - iad + ibc + bd = ac+bd +i(bc-ad)
#define D_complexConjMul_re(a,b) (a.x*b.x + a.y*b.y)
#define D_complexConjMul_im(a,b) (a.y*b.x - a.x*b.y)
#define D_complexConjMul(a,b) (float2){ (a.x*b.x + a.y*b.y), (a.y*b.x - a.x*b.y) }
#define D_complexSquare(a)    (float)(a.x*a.x + a.y*a.y)

__device__ __host__ inline void operator+=(float2& lhs, const float2 rhs)
{
    lhs.x += rhs.x;
    lhs.y += rhs.y;
}

/// Complex conjugate cross-product
__device__ __host__ inline float2 complexConjMul(const float2 a, const float2 b)
{
    return (float2){ (a.x*b.x + a.y*b.y), (a.y*b.x - a.x*b.y) };
}

/// Single polarization 2-antenna complex conjugate cross product, one polarization product.
__device__ __host__ inline float2 xstep(const float2 ant1X, const float2 ant2X)
{
    return complexConjMul(ant1X,ant2X);
}

/// Single polarization 2-antenna complex conjugate cross product, with in-place accumulation
__device__ __host__ inline void xstep_I(const float2 ant1X, const float2 ant2X, float2* vizaccu)
{
     *vizaccu += complexConjMul(ant1X,ant2X);
}

/// Half Stokes 2-antenna complex conjugate cross products. Two polarization products.
__device__ __host__ inline void xstepHalfStokes(const float4 ant1XY, const float4 ant2XY, float2* __restrict__ viz)
{
    float2 ant1X = (float2){ant1XY.x,ant1XY.y};
    float2 ant1Y = (float2){ant1XY.z,ant1XY.w};
    float2 ant2X = (float2){ant2XY.x,ant2XY.y};
    float2 ant2Y = (float2){ant2XY.z,ant2XY.w};
    viz[0] = xstep(ant1X,ant2X);
    viz[1] = xstep(ant1Y,ant2Y);
}

/// Half Stokes 2-antenna complex conjugate cross products, with in-place accumulation
__device__ __host__ inline void xstepHalfStokes_I(const float4 ant1XY, const float4 ant2XY, float2* __restrict__ vizaccu)
{
    float2 ant1X = (float2){ant1XY.x,ant1XY.y};
    float2 ant1Y = (float2){ant1XY.z,ant1XY.w};
    float2 ant2X = (float2){ant2XY.x,ant2XY.y};
    float2 ant2Y = (float2){ant2XY.z,ant2XY.w};
    vizaccu[0] += xstep(ant1X,ant2X);
    vizaccu[1] += xstep(ant1Y,ant2Y);
}

/// Full Stokes 2-antenna complex conjugate cross products. Four polarization products.
__device__ __host__ inline void xstepFullStokes(const float4 ant1XY, const float4 ant2XY, float2* __restrict__ viz)
{
    float2 ant1X = (float2){ant1XY.x,ant1XY.y};
    float2 ant1Y = (float2){ant1XY.z,ant1XY.w};
    float2 ant2X = (float2){ant2XY.x,ant2XY.y};
    float2 ant2Y = (float2){ant2XY.z,ant2XY.w};
    viz[0] = xstep(ant1X,ant2X);
    viz[1] = xstep(ant1X,ant2Y);
    viz[2] = xstep(ant1Y,ant2X);
    viz[3] = xstep(ant1Y,ant2Y);
}

/// Full Stokes 2-antenna complex conjugate cross products, with in-place accumulation
__device__ __host__ inline void xstepFullStokes_I(const float4 ant1XY, const float4 ant2XY, float2* __restrict__ vizaccu)
{
    float2 ant1X = (float2){ant1XY.x,ant1XY.y};
    float2 ant1Y = (float2){ant1XY.z,ant1XY.w};
    float2 ant2X = (float2){ant2XY.x,ant2XY.y};
    float2 ant2Y = (float2){ant2XY.z,ant2XY.w};
    vizaccu[0] += xstep(ant1X,ant2X);
    vizaccu[1] += xstep(ant1X,ant2Y);
    vizaccu[2] += xstep(ant1Y,ant2X);
    vizaccu[3] += xstep(ant1Y,ant2Y);
}

static inline __host__ __device__ float4 make_float4(float2 a, float2 b)
{
    return (float4) { a.x, a.y, b.x, b.y };
}


////////////////////////////////////////////////////////////////////////////////////
// Averaging across block for final visibility data, various output data layouts.
////////////////////////////////////////////////////////////////////////////////////

/// Averaging across blocks. Baseline-first layout e.g. if Full Stokes then [ {<re,im> x {XX,XY,YX,YY on bl1,bl2,bl3,bl4,bl5,bl6}} x Nch ].
template<const size_t NVIZ>
__device__ inline void cu_fxcorr_blockReduce_OutFmt1(float2* __restrict__ out, const float2* __restrict__ localviz, const size_t localviz_ch, const size_t Nchan)
{
    // Layout 1: output layout as in the internal compute layout, baseline-first : ~2.5 Gs/s only since writes not coalesced
    for (size_t b=0; b<NVIZ; b++) {
        atomicAdd(&(out[NVIZ*localviz_ch + b].x), localviz[b].x);
        atomicAdd(&(out[NVIZ*localviz_ch + b].y), localviz[b].y); // slow: writes are not coalesced
    }
}

/// Averaging across blocks. Channels-first layout with Re/Im separation e.g. [{ch1,ch2,...,chN} x {XX,XY,YX,YY on bl1,bl2,bl3,bl4,bl5,bl6} ] x {re, im}
template<const size_t NVIZ>
__device__ inline void cu_fxcorr_blockReduce_OutFmt2(float2* __restrict__ out, const float2* __restrict__ localviz, const size_t localviz_ch, const size_t Nchan)
{
    // Layout 2: output layout is channels-first, with Re&Im separated
    for (size_t b=0; b<NVIZ; b++) {
         atomicAdd(&(((float*)out)[localviz_ch + b*Nchan]), localviz[b].x); // fast: writes coalesced, gapless
    }
    for (size_t b=0; b<NVIZ; b++) {
        atomicAdd(&(((float*)out)[localviz_ch + b*Nchan + NVIZ*Nchan]), localviz[b].y);
    }
}

/// Averaging across blocks. Channels-first layout with Re&Im grouped e.g. [<re,im> x {ch1,ch2,...,chN} x {XX,XY,YX,YY on bl1,bl2,bl3,bl4,bl5,bl6} ]
template<const size_t NVIZ>
__device__ inline void cu_fxcorr_blockReduce_OutFmt3(float2* __restrict__ out, const float2* __restrict__ localviz, const size_t localviz_ch, const size_t Nchan)
{
    // Layout 3: output layout is channels-first, with Re&Im packed together
    for (size_t b=0; b<NVIZ; b++) {
        atomicAdd(&out[localviz_ch + b*Nchan].x, localviz[b].x);  // fast'ish: writes coalesced, but 1 x float sized gaps
    }
    for (size_t b=0; b<NVIZ; b++) {
        atomicAdd(&out[localviz_ch + b*Nchan].y, localviz[b].y);
    }
}

/////////////////////////////////////////////////////////////////////////////////////

/// Averaging across blocks
template<const size_t NVIZ>
__device__ inline void cu_fxcorr_blockReduce(float2* __restrict__ out, const float2* __restrict__ localviz, const size_t localviz_ch, const size_t Nchan)
{
    assert(GPU_OUT_LAYOUT>=1 && GPU_OUT_LAYOUT<=3);
    if (GPU_OUT_LAYOUT == 1) {
            cu_fxcorr_blockReduce_OutFmt1<NVIZ>(out, localviz, localviz_ch, Nchan);
    } else if (GPU_OUT_LAYOUT == 2) {
            cu_fxcorr_blockReduce_OutFmt2<NVIZ>(out, localviz, localviz_ch, Nchan);
    } else if (GPU_OUT_LAYOUT == 3) {
            cu_fxcorr_blockReduce_OutFmt3<NVIZ>(out, localviz, localviz_ch, Nchan);
    }
}

/// CPU-side rearranging of GPU output data to the layout used by CPU reference solution
void cu_fxcorr_4ant_reshuffle(const float2* vgpu, float2* vcpu_like, size_t Nant, size_t Nchan, size_t Npolprod, bool withAuto)
{
    const size_t Nbase = (withAuto) ? ((Nant*(Nant-1))/2 + Nant) : (Nant*(Nant-1))/2;
    assert(GPU_OUT_LAYOUT>=1 && GPU_OUT_LAYOUT<=3);
    if (GPU_OUT_LAYOUT == 1) {
        memcpy(vcpu_like, vgpu, sizeof(float2)*Nbase*Npolprod*Nchan);
    } else if (GPU_OUT_LAYOUT == 2) {
       const float* in = (const float*)vgpu;
       const size_t Nviz = Nbase*Npolprod;
       for (size_t n=0; n<Nchan; n++) {
           for (size_t b=0; b<Nviz; b++) {
               vcpu_like[Nviz*n + b] = (float2) { in[n + b*Nchan],in[n + b*Nchan + Nviz*Nchan] };
           }
       }
    } else if (GPU_OUT_LAYOUT == 3) {
       const size_t Nviz = Nbase*Npolprod;
       for (size_t n=0; n<Nchan; n++) {
           for (size_t b=0; b<Nviz; b++) {
               vcpu_like[Nviz*n + b] = vgpu[n + b*Nchan];
           }
       }
    }
}

/////////////////////////////////////////////////////////////////////////////////////

/// Return a CUDA grid layout to use with cu_fxcorr_4ant_***().
void prep_cu_fxcorr_4ant(size_t &nblk, size_t &nthr, const size_t maxphysthreads, size_t Nsamp_per_antenna, size_t Nchan)
{
    size_t Nfft = Nsamp_per_antenna / Nchan;
    int iter = 0;
    nthr = 32;
    nblk = maxphysthreads/nthr;
    printf("  layout initial try: nblk %zu x nthr %zu : %zu averaging steps\n", nblk, nthr, Nfft);
    do {
        size_t stride = nblk * nthr;
        size_t kernel_iters = (Nsamp_per_antenna + stride - 1) / stride;
        printf("  iter %d: fxcorr_4ant_1pol : nblk %zu x nthr %zu = stride %zu : max phys thr %zu : Nfft %zu x Nch %zu : Nsamp %zu : kernel iter %zu\n", iter, nblk, nthr, stride, maxphysthreads, Nfft, Nchan, Nsamp_per_antenna, kernel_iters);
        if ((stride % Nchan) == 0) break;
        nthr--;
        iter++;
    } while(1);
}

/////////////////////////////////////////////////////////////////////////////////////
// Cross-multiply Accumulate
// Single polarization complex cross-product accumlation for 4 antennas (6 baseline)
/////////////////////////////////////////////////////////////////////////////////////

/**
 * Cross-multiply and accumulate 4-antenna single-polariation complex (post-FFT) input data.
 */
__global__ void cu_fxcorr_4ant_1pol(
    const float2* __restrict__ in,
    float2* __restrict__ out,
    const size_t Nsamp_per_antenna,
    const size_t Nchan
)
{
    size_t idx = blockIdx.x * blockDim.x + threadIdx.x; // offset into per-antenna input sample sequence 0...Nch*Nfft-1; ignore 4-antenna re,im data grouping
    const size_t numThreads = blockDim.x * gridDim.x;   // how many input samples (per-antenna) processed in parallel
    const size_t my_ch = (idx % Nchan);

    const size_t NANT = 4;
    const size_t NBASE = 6; // = NANT*(NANT-1)/2

    float2 vizaccu[NBASE] = { (float2){0.0f, 0.0f}, };

    while (idx < Nsamp_per_antenna) {

        float2 singlepolsig[NANT];
        for (int ant=0; ant<NANT; ant++) {
            singlepolsig[ant] = in[NANT*idx + ant];
        }

        int bl = 0;
        for (int ant1=0; ant1<NANT; ant1++) {
            for (int ant2=ant1+1; ant2<NANT; ant2++) {
                xstep_I(singlepolsig[ant1],singlepolsig[ant2],&vizaccu[bl]);
                bl++;
            }
        }

        idx += numThreads;
    }

    cu_fxcorr_blockReduce<NBASE>(out, vizaccu, my_ch, Nchan);
}

/**
 * Cross-multiply and accumulate 4-antenna single-polariation complex (post-FFT) input data, with autocorrelation output.
 */
__global__ void cu_fxcorr_4ant_1pol_withauto(
    const float2* __restrict__ in,
    float2* __restrict__ out,
    const size_t Nsamp_per_antenna,
    const size_t Nchan
)
{
    size_t idx = blockIdx.x * blockDim.x + threadIdx.x; // offset into per-antenna input sample sequence 0...Nch*Nfft-1; ignore 4-antenna re,im data grouping
    const size_t numThreads = blockDim.x * gridDim.x;   // how many input samples (per-antenna) processed in parallel
    const size_t my_ch = (idx % Nchan);

    const size_t NANT = 4;
    const size_t NBASE = 10; // = NANT*(NANT-1)/2 xcorrs + NANT autos

    float2 vizaccu[NBASE] = { (float2){0.0f, 0.0f}, };

    while (idx < Nsamp_per_antenna) {

        float2 sig[NANT];
        for (int ant=0; ant<NANT; ant++) {
            sig[ant] = in[NANT*idx + ant];
        }

        int bl = 0;
        for (int ant1=0; ant1<NANT; ant1++) {
            for (int ant2=ant1; ant2<NANT; ant2++) {
                xstep_I(sig[ant1],sig[ant2],&vizaccu[bl]);
                bl++;
            }
        }

        idx += numThreads;
    }

    cu_fxcorr_blockReduce<NBASE>(out, vizaccu, my_ch, Nchan);
}

/////////////////////////////////////////////////////////////////////////////////////
// Cross-multiply Accumulate
// Dual polarization complex cross-product accumulation
/////////////////////////////////////////////////////////////////////////////////////

/**
 * Cross-multiply and accumulate 4-antenna dual-polariation complex (post-FFT) input data.
 */
__global__ void cu_fxcorr_4ant_2pol(
    const float4* __restrict__ in,
    float2* __restrict__ out,
    const size_t Nsamp_per_antenna,
    const size_t Nchan
)
{
    size_t idx = blockIdx.x * blockDim.x + threadIdx.x; // offset into per-antenna input sample sequence 0...Nch*Nfft-1; ignore 4-antenna re,im X,Y data grouping
    const size_t numThreads = blockDim.x * gridDim.x;   // how many input samples (per-antenna) processed in parallel
    const size_t my_ch = (idx % Nchan);

    const size_t NANT = 4;           // 4 antennas in dual pol
    const size_t NPOLPROD = 4;       // {XX,YX,XY,YY}
    const size_t NVIZ = 6*NPOLPROD;  // = (NANT*(NANT-1)/2 baseline) * pol products = 24

    float2 vizaccu[NVIZ] = { (float2){0.0f, 0.0f}, };

    while (idx < Nsamp_per_antenna) {

        float4 dualpolsig[NANT];
        for (int ant=0; ant<NANT; ant++) {
            dualpolsig[ant] = in[NANT*idx + ant];
        }

        int bl = 0;
        for (int ant1=0; ant1<NANT; ant1++) {
          for (int ant2=ant1+1; ant2<NANT; ant2++) {
              xstepFullStokes_I(dualpolsig[ant1], dualpolsig[ant2], &vizaccu[bl]);
              bl += NPOLPROD;
          }
        }

        idx += numThreads;
    }

    cu_fxcorr_blockReduce<NVIZ>(out, vizaccu, my_ch, Nchan);
}


/**
 * Cross-multiply and accumulate 4-antenna dual-polariation complex (post-FFT) input data, with autocorrelation output.
 */
__global__ void cu_fxcorr_4ant_2pol_withAuto(
    float4* __restrict__ in,
    float2* __restrict__ out,
    const size_t Nsamp_per_antenna,
    const size_t Nchan
)
{
    size_t idx = blockIdx.x * blockDim.x + threadIdx.x; // offset into per-antenna input sample sequence 0...Nch*Nfft-1; ignore 4-antenna re,im X,Y data grouping
    const size_t numThreads = blockDim.x * gridDim.x;   // how many input samples (per-antenna) processed in parallel
    const size_t my_ch = (idx % Nchan);

    const size_t NANT = 4;           // 4 antennas in dual pol
    const size_t NPOLPROD = 4;       // {XX,YX,XY,YY}
    const size_t NVIZ = (6 + NANT)*NPOLPROD;  // = (NANT*(NANT-1)/2 baselines + Nant auto) * pol products = 40

    float2 vizaccu[NVIZ] = { (float2){0.0f, 0.0f}, };

    while (idx < Nsamp_per_antenna) {

        float4 dualpolsig[NANT];
        for (int ant=0; ant<NANT; ant++) {
            dualpolsig[ant] = in[NANT*idx + ant];
        }

        int bl = 0;
        for (int ant1=0; ant1<NANT; ant1++) {
          for (int ant2=ant1; ant2<NANT; ant2++) {
              xstepFullStokes_I(dualpolsig[ant1], dualpolsig[ant2], &vizaccu[bl]);
              bl += NPOLPROD;
          }
        }

        idx += numThreads;
    }

    cu_fxcorr_blockReduce<NVIZ>(out, vizaccu, my_ch, Nchan);
}

/**
 * Cross-multiply and accumulate 4-antenna dual-polariation complex (post-FFT) input data, with autocorrelation output.
 * The input data are not interlaved, but rather are given as 8 separate vectors (in[0]=ant1X, in[1]=ant1Y, in[2]=ant2X, ...)
 */
__global__ void cu_fxcorr_4ant_2pol_withAuto(
    const float2* __restrict__ in1X, const float2* __restrict__ in1Y,
    const float2* __restrict__ in2X, const float2* __restrict__ in2Y,
    const float2* __restrict__ in3X, const float2* __restrict__ in3Y,
    const float2* __restrict__ in4X, const float2* __restrict__ in4Y,
    float2* __restrict__ out,
    const size_t Nsamp_per_antenna,
    const size_t Nchan
)
{
    size_t idx = blockIdx.x * blockDim.x + threadIdx.x; // offset into per-antenna input sample sequence 0...Nch*Nfft-1; ignore 4-antenna re,im X,Y data grouping
    const size_t numThreads = blockDim.x * gridDim.x;   // how many input samples (per-antenna) processed in parallel
    const size_t my_ch = (idx % Nchan);

    const size_t NANT = 4;           // 4 antennas in dual pol
    const size_t NPOLPROD = 4;       // {XX,YX,XY,YY}
    const size_t NVIZ = (6 + NANT)*NPOLPROD;  // = (NANT*(NANT-1)/2 baselines + Nant auto) * pol products = 40

    float2 vizaccu[NVIZ] = { (float2){0.0f, 0.0f}, };

    while (idx < Nsamp_per_antenna) {

        float4 dualpolsig[NANT];
        dualpolsig[0] = make_float4(in1X[idx], in1Y[idx]);
        dualpolsig[1] = make_float4(in2X[idx], in2Y[idx]);
        dualpolsig[2] = make_float4(in3X[idx], in3Y[idx]);
        dualpolsig[3] = make_float4(in4X[idx], in4Y[idx]);

        int bl = 0;
        for (int ant1=0; ant1<NANT; ant1++) {
          for (int ant2=ant1; ant2<NANT; ant2++) {
              xstepFullStokes_I(dualpolsig[ant1], dualpolsig[ant2], &vizaccu[bl]);
              bl += NPOLPROD;
          }
        }

        idx += numThreads;
    }

    cu_fxcorr_blockReduce<NVIZ>(out, vizaccu, my_ch, Nchan);
}

/// Wrapper to invoke cu_fxcorr_4ant_2pol_withAuto with "float* signals[8]" which is not supported directlyt by CUDA kernel calls
void fxcorr_4ant_2pol_withAuto(int numBlocks, int threadsPerBlock,
    const float2* d_signals[8], float2* d_out,
    const size_t Nsamp_per_antenna, const size_t Nchan
)
{
    cu_fxcorr_4ant_2pol_withAuto <<<numBlocks, threadsPerBlock>>> (
        d_signals[0], d_signals[1], /* ant 1 X, Y complex spectral data */
        d_signals[2], d_signals[3], /* ... */
        d_signals[4], d_signals[5], /* ... */
        d_signals[6], d_signals[7], /* ant 4 X, Y complex spectral data */
        d_out,
        Nsamp_per_antenna, Nchan );
}

/////////////////////////////////////////////////////////////////////////////////////
// CPU Reference code
/////////////////////////////////////////////////////////////////////////////////////

/** CPU reference FX 4-ant 1-pol
 * Input:  [ {ant1,ant2,ant3,ant4 re im} x Nsamples ]
 * Output: [ {bl1,bl2,bl3,bl4,bl5,bl6} re im} x Nch ]
 */
void cpu_fxcorr_1pol(const float2* interleaved_data_in, float2* out, const int Nant, const size_t Nsamp_per_antenna, const int Nchan)
{
    const size_t Nbase = (Nant*(Nant-1))/2;
    const size_t Nsamp = Nsamp_per_antenna * (size_t)Nant;
    size_t offset = 0;
    size_t curr_ch = 0;
    memset(out, 0x00, sizeof(float2)*Nbase*Nchan);
    while (offset < Nsamp) {
        size_t out_bl_idx = curr_ch*Nbase;
        for (int ant1=0; ant1<Nant; ant1++) {
            for (int ant2=ant1+1; ant2<Nant; ant2++) {
                const float2 a = interleaved_data_in[offset + ant1];
                const float2 b = interleaved_data_in[offset + ant2];
                out[out_bl_idx].x += D_complexConjMul_re(a,b);
                out[out_bl_idx].y += D_complexConjMul_im(a,b);
                out_bl_idx++;
            }
        }
        offset += Nant;
        curr_ch = (curr_ch + 1) % Nchan;
    }
}

/** CPU reference FX 4-ant 1-pol visibilities and autocorrelations
 * Input:  [ {ant1,ant2,ant3,ant4 re im} x Nsamples ]
 * Output: [ {bl1,bl2,bl3,bl4,bl5,bl6,bl7,bl8,bl9,bl10} re im} x Nch ]
 */
void cpu_fxcorr_1pol_withAuto(const float2* interleaved_data_in, float2* out, const int Nant, const size_t Nsamp_per_antenna, const int Nchan)
{
    const size_t Nbase = (Nant*(Nant-1))/2 + Nant;
    const size_t Nsamp = Nsamp_per_antenna * (size_t)Nant;
    size_t offset = 0;
    size_t curr_ch = 0;
    memset(out, 0x00, sizeof(float2)*Nbase*Nchan);
    while (offset < Nsamp) {
        size_t out_bl_idx = curr_ch*Nbase;
        for (int ant1=0; ant1<Nant; ant1++) {
            for (int ant2=ant1; ant2<Nant; ant2++) {
                const float2 a = interleaved_data_in[offset + ant1];
                const float2 b = interleaved_data_in[offset + ant2];
                out[out_bl_idx].x += D_complexConjMul_re(a,b);
                out[out_bl_idx].y += D_complexConjMul_im(a,b);
                out_bl_idx++;
            }
        }
        offset += Nant;
        curr_ch = (curr_ch + 1) % Nchan;
    }
}

/** CPU reference FX 4-ant 2-pol visibilities with cross-pols
 * Input:  [ {ant1,ant2,ant3,ant4 <X,Y> x <re,im>} x Nsamples ]
 * Output: [ {bl1,bl2,bl3,bl4,bl5,bl6} <XX,XY,YX,YY> <re,im>} x Nch ]
 */
void cpu_fxcorr_2pol(const float4* interleaved_data_in, float2* out, const int Nant, const size_t Nsamp_per_antenna, const int Nchan)
{
    const size_t Nbase = (Nant*(Nant-1))/2;
    const size_t Npolprod = 4;
    const size_t Nsamp = Nsamp_per_antenna * (size_t)Nant;
    size_t offset = 0;
    size_t curr_ch = 0;
    memset(out, 0x00, sizeof(float2)*Nbase*Npolprod*Nchan);

    while (offset < Nsamp) {

        size_t out_bl_idx = curr_ch*(Nbase*Npolprod);
        for (int ant1=0; ant1<Nant; ant1++) {
            for (int ant2=ant1+1; ant2<Nant; ant2++) {

                const float4 a = interleaved_data_in[offset + ant1];
                const float4 b = interleaved_data_in[offset + ant2];

                const float2 aX = (float2){a.x,a.y};
                const float2 aY = (float2){a.z,a.w};
                const float2 bX = (float2){b.x,b.y};
                const float2 bY = (float2){b.z,b.w};

                out[out_bl_idx+0].x += D_complexConjMul_re(aX,bX);
                out[out_bl_idx+0].y += D_complexConjMul_im(aX,bX);
                out[out_bl_idx+1].x += D_complexConjMul_re(aX,bY);
                out[out_bl_idx+1].y += D_complexConjMul_im(aX,bY);
                out[out_bl_idx+2].x += D_complexConjMul_re(aY,bX);
                out[out_bl_idx+2].y += D_complexConjMul_im(aY,bX);
                out[out_bl_idx+3].x += D_complexConjMul_re(aY,bY);
                out[out_bl_idx+3].y += D_complexConjMul_im(aY,bY);
                out_bl_idx += Npolprod;
            }
        }

        offset += Nant;
        curr_ch = (curr_ch + 1) % Nchan;
    }
}

/** CPU reference FX 4-ant 2-pol visibilities with cross-pols and autocorrelations
 * Input:  [ {ant1,ant2,ant3,ant4 <X,Y> x <re,im>} x Nsamples ]
 * Output: [ {bl1,bl2,bl3,bl4,bl5,bl6,bl7,bl8,bl9,bl10} <XX,XY,YX,YY> <re,im>} x Nch ]
 */
void cpu_fxcorr_2pol_withAuto(const float4* interleaved_data_in, float2* out, const int Nant, const size_t Nsamp_per_antenna, const int Nchan)
{
    const size_t Nbase = (Nant*(Nant-1))/2 + Nant;
    const size_t Npolprod = 4;
    const size_t Nsamp = Nsamp_per_antenna * (size_t)Nant;
    size_t offset = 0;
    size_t curr_ch = 0;
    memset(out, 0x00, sizeof(float2)*Nbase*Npolprod*Nchan);

    while (offset < Nsamp) {

        size_t out_bl_idx = curr_ch*(Nbase*Npolprod);
        for (int ant1=0; ant1<Nant; ant1++) {
            for (int ant2=ant1; ant2<Nant; ant2++) {

                const float4 a = interleaved_data_in[offset + ant1];
                const float4 b = interleaved_data_in[offset + ant2];

                const float2 aX = (float2){a.x,a.y};
                const float2 aY = (float2){a.z,a.w};
                const float2 bX = (float2){b.x,b.y};
                const float2 bY = (float2){b.z,b.w};

                out[out_bl_idx+0].x += D_complexConjMul_re(aX,bX);
                out[out_bl_idx+0].y += D_complexConjMul_im(aX,bX);
                out[out_bl_idx+1].x += D_complexConjMul_re(aX,bY);
                out[out_bl_idx+1].y += D_complexConjMul_im(aX,bY);
                out[out_bl_idx+2].x += D_complexConjMul_re(aY,bX);
                out[out_bl_idx+2].y += D_complexConjMul_im(aY,bX);
                out[out_bl_idx+3].x += D_complexConjMul_re(aY,bY);
                out[out_bl_idx+3].y += D_complexConjMul_im(aY,bY);
                out_bl_idx += Npolprod;
            }
        }

        offset += Nant;
        curr_ch = (curr_ch + 1) % Nchan;
    }
}


/** CPU reference FX 4-ant 2-pol visibilities with cross-pols and autocorrelations
 * Input:  [ <re,im>} x Nsamples ] x 2 pol x 4 ant
 * Output: [ {bl1,bl2,bl3,bl4,bl5,bl6,bl7,bl8,bl9,bl10} <XX,XY,YX,YY> <re,im>} x Nch ]
 */
void cpu_fxcorr_2pol_withAuto(const float2* split_data_in[8], float2* out, const int Nant, const size_t Nsamp_per_antenna, const int Nchan)
{
    const size_t Nbase = (Nant*(Nant-1))/2 + Nant;
    const size_t Npolprod = 4;
    size_t offset = 0;
    size_t curr_ch = 0;
    memset(out, 0x00, sizeof(float2)*Nbase*Npolprod*Nchan);

    while (offset < Nsamp_per_antenna) {

        size_t out_bl_idx = curr_ch*(Nbase*Npolprod);
        for (int ant1=0; ant1<Nant; ant1++) {
            for (int ant2=ant1; ant2<Nant; ant2++) {

                const float2 aX = split_data_in[2*ant1+0][offset];
                const float2 aY = split_data_in[2*ant1+1][offset];
                const float2 bX = split_data_in[2*ant2+0][offset];
                const float2 bY = split_data_in[2*ant2+1][offset];

                out[out_bl_idx+0].x += D_complexConjMul_re(aX,bX);
                out[out_bl_idx+0].y += D_complexConjMul_im(aX,bX);
                out[out_bl_idx+1].x += D_complexConjMul_re(aX,bY);
                out[out_bl_idx+1].y += D_complexConjMul_im(aX,bY);
                out[out_bl_idx+2].x += D_complexConjMul_re(aY,bX);
                out[out_bl_idx+2].y += D_complexConjMul_im(aY,bX);
                out[out_bl_idx+3].x += D_complexConjMul_re(aY,bY);
                out[out_bl_idx+3].y += D_complexConjMul_im(aY,bY);
                out_bl_idx += Npolprod;
            }
        }

        offset += 1;
        curr_ch = (curr_ch + 1) % Nchan;
    }
}


/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////


#ifdef BENCH // standalone compile mode for testing

//#undef CUDA_DEVICE_NR
#ifndef CUDA_DEVICE_NR
    #define CUDA_DEVICE_NR 0
#endif

#define CHECK_TIMING 1
#include "cuda_utils.cu"
#include "statistics.h"

static bool compare(const float2* vcpu, const float2* vgpu, int Nant, int Nchan, bool withAuto)
{
    bool verbose = false;
    const double max_err = 1e-3;

    // Data layout: [ {bl1,bl2,bl3,bl4,bl5,bl6} <re,im> x Nch ]
    bool consistent = true;
    int nbase = (Nant*(Nant-1))/2;
    double peak_rel_error = 0;
    for (int ch=0; ch<Nchan; ch++) {
        int b = 0;
        if (verbose) printf("Channel %3d\n", ch);
        for (int a1=0; a1<Nant; a1++) {
           int a2 = withAuto ? a1 : a1+1;
           for (; a2<Nant; a2++, b++) {
               double2 v1 = (double2){vcpu[ch*nbase+b].x, vcpu[ch*nbase+b].y};
               double2 v2 = (double2){vgpu[ch*nbase+b].x, vgpu[ch*nbase+b].y};
               double mag1 =  sqrt( (v1.x*v1.x) + (v1.y*v1.y) );
               double mag2 =  sqrt( (v2.x*v2.x) + (v2.y*v2.y) );
               double diff = sqrt( (v1.x-v2.x)*(v1.x-v2.x) + (v1.y-v2.y)*(v1.y-v2.y));
               double relative_err = fabs(mag2-mag1) / mag1;
               bool pass = (mag1 == 0 && diff < max_err) || (relative_err < max_err);
               consistent = consistent & pass;
               peak_rel_error = (relative_err > peak_rel_error) ? relative_err : peak_rel_error;
               if (verbose || !consistent) {
                   printf("   baseline %1d: ant %d x %d : %+.1f%+.1fi / %+.1f%+.1fi : diff %f %.1e %s\n",
                       b, a1, a2, v1.x, v1.y, v2.x, v2.y,
                       diff, relative_err, pass ? "PASS" : "FAIL"
                   );
               }
           }
        }
    }
    printf("Relative err: %.3e\n", peak_rel_error);
    return consistent;
}

static bool compare4stokes(const float2* vcpu, const float2* vgpu, int Nant, int Nchan, bool withAuto)
{
    bool verbose = false;

    // Data layout: [ {bl1,bl2,bl3,bl4,bl5,bl6} <re,im> x Nch ]
    const int Npolprod = 4;
    const char* polpairNames[Npolprod] = {"XX","XY","YX","YY"};
    bool consistent = true;
    int nbase = (Nant*(Nant-1))/2;

    double peak_rel_error = 0;
    for (int ch=0; ch<Nchan; ch++) {
        int b = 0;
        if (verbose) printf("Channel %3d\n", ch);
        for (int a1=0; a1<Nant; a1++) {
           int a2 = withAuto ? a1 : a1+1;
           for (; a2<Nant; a2++) {
               for (int p=0; p<Npolprod; p++, b++) {
                   double2 v1 = (double2){vcpu[ch*nbase+b].x, vcpu[ch*nbase+b].y};
                   double2 v2 = (double2){vgpu[ch*nbase+b].x, vgpu[ch*nbase+b].y};
                   double mag1 =  sqrt( (v1.x*v1.x) + (v1.y*v1.y) );
                   double mag2 =  sqrt( (v2.x*v2.x) + (v2.y*v2.y) );
                   double diff = sqrt( (v1.x-v2.x)*(v1.x-v2.x) + (v1.y-v2.y)*(v1.y-v2.y));
                   double relative_err = fabs(mag2-mag1) / mag1;
                   bool pass = (mag1 == 0 && diff < 1e-2) || (relative_err < 1e-2);
                   consistent = consistent & pass;
                   peak_rel_error = (relative_err > peak_rel_error) ? relative_err : peak_rel_error;
                   if (verbose || !consistent) {
                       printf("   baseline %1d: ant %d x %d %s : %+.1f%+.1fi / %+.1f%+.1fi : diff %f %.1e %s\n",
                          b/Npolprod, a1, a2, polpairNames[p], v1.x, v1.y, v2.x, v2.y,
                           diff, relative_err, pass ? "PASS" : "FAIL"
                       );
                   }
               }
           }
        }
    }
    printf("Relative err: %.3e\n", peak_rel_error);
    return consistent;
}

int main(int argc, char** argv)
{
    cudaDeviceProp cudaDevProp;
    cudaEvent_t tstart, tstop;
    size_t maxphysthreads;
    size_t nsamples, nchan, nbyte, nbyteviz;
    size_t nant, nbase;
    size_t threadsPerBlock;
    size_t numBlocks;
    float2 *d_data;
    float2 *h_data;
    float2 *d_viz;
    float2 *h_viz;
    float2 *h_viz_reshuffled;
    float2 *ref_viz;
    float2 *ref_viz_withAuto;
    bool passed;

    const int Niter = 5000;
    float rates[Niter];
    float mean, std, vmin, vmax;

    CUDA_CALL( cudaSetDevice(CUDA_DEVICE_NR) );
    CUDA_CALL( cudaGetDeviceProperties(&cudaDevProp, CUDA_DEVICE_NR) );
    maxphysthreads = cudaDevProp.multiProcessorCount * cudaDevProp.maxThreadsPerMultiProcessor;

    CUDA_CALL( cudaEventCreate( &tstart ) );
    CUDA_CALL( cudaEventCreate( &tstop ) );

    // Input: single pol, 4 antennas, 128 channels after 256-point r2c FFT
    // Assuming 4 Gs/s then 1 msec = 4000000 real = 2000000 complex samples
    nant = 4;
    nchan = 256; //256;
    if (argc >= 2) {
        nchan = atoi(argv[1]);
    }

    // Samples per antenna
    // 48 ms @ 2 GHz 1-pol = 96000000 complex values
    //  4 ms @ 2 GHz 1-pol =  8000000 complex values
    //  2 ms @ 2 GHz 1-pol =  4000000 complex values
    //  1 ms @ 2 GHz 1-pol =  2000000 complex values
    nsamples = 2000000;
    nsamples *= 2; // dual-pol
    nsamples -= (nsamples % (2*nchan)); // make nsamples an integer multiple of 'nchan' in 2-pol
    nbyte = sizeof(float2) * nant * nsamples;
    printf("Data size : ~%.2f GByte total\n", nbyte*1e-9);
    if (nbyte > 8ULL*1024*1024*1024) {
        printf("Built-in memory limit (8G) reached\n");
        return -1;
    }
    CUDA_CALL( cudaMalloc( (void **)&d_data, nbyte ) );
    CUDA_CALL( cudaHostAlloc( (void **)&h_data, nbyte, cudaHostAllocDefault ) );

    printf("Generating data... ");
    if (1) {
        size_t n = 0, nspec = 0;
        float2* d = h_data;
        while (n < nsamples) {
            for (size_t c = 0; c < nchan && n < nsamples; c++, n++) {
                for (size_t a = 0; a < nant; a++, d++) {
                    // Real,imag parts of antennas FFT bin 'c'
                    d->x = (c%8) * a;
                    d->y = (c%8) * (a+1);
                }
            }
            nspec++;
        }
        printf("done, last ptr=%p, end of array=%p.\n", (void*)d, (void*)(h_data+nant*nsamples));
        CUDA_CALL( cudaMemcpy(d_data, h_data, nbyte, cudaMemcpyHostToDevice) );
    }

    // Output: 4 antennas -> 6 baselines with 256 channels x  4 pol-products
    nbase = (nant*(nant-1))/2;
    nbyteviz = sizeof(float2) * nchan * nbase;  // visibilities
    nbyteviz += sizeof(float2) * nchan * nant;  // autocorrelations
    nbyteviz *= 4;                              // full-stokes
    CUDA_CALL( cudaHostAlloc( (void **)&ref_viz, nbyteviz, cudaHostAllocDefault ) );
    CUDA_CALL( cudaHostAlloc( (void **)&ref_viz_withAuto, nbyteviz, cudaHostAllocDefault ) );
    CUDA_CALL( cudaMalloc( (void **)&d_viz, nbyteviz) );
    CUDA_CALL( cudaHostAlloc( (void **)&h_viz, nbyteviz, cudaHostAllocDefault ) );
    CUDA_CALL( cudaHostAlloc( (void **)&h_viz_reshuffled, nbyteviz, cudaHostAllocDefault ) );
    CUDA_CALL( cudaMemset(d_viz, 0x00, nbyteviz) );
    memset(ref_viz, 0x00, nbyteviz);
    memset(ref_viz_withAuto, 0x00, nbyteviz);

    // Some extra info
    printf("CUDA Device #%d : %s, Compute Capability %d.%d, %d threads/block, warpsize %d, %zu threads max.\n",
        CUDA_DEVICE_NR, cudaDevProp.name, cudaDevProp.major, cudaDevProp.minor,
        cudaDevProp.maxThreadsPerBlock, cudaDevProp.warpSize, maxphysthreads
    );
    printf("Data: %zu samples/antenna (half this when dual-pol), %zu antennas, %zu spectral channels\n", nsamples, nant, nchan);
    printf("Kernel speed is reported in units of megasamples/sec per antenna\n");

    // Computing layout
    printf("Preparing computing layout for GPU\n");
    prep_cu_fxcorr_4ant(numBlocks, threadsPerBlock, maxphysthreads, nsamples, nchan);
    printf("  Layout: %zu blocks x %zu threads\n\n", numBlocks, threadsPerBlock);

    printf("Kernel speed benchmark reports the X-step throughput in units of: million samples/second / antenna / polarization\n");

    /////////////////////////////////////////////////////////////////////////////////

    printf("=== Single-pol\n");

    printf("Calculating reference result on CPU\n");
    cpu_fxcorr_1pol(h_data, ref_viz, nant, nsamples, nchan);

    CUDA_CALL( cudaMemset(d_viz, 0x00, nbyteviz) );
    CUDA_TIMING_START(tstart, 0);
    cu_fxcorr_4ant_1pol <<<numBlocks, threadsPerBlock>>> ( d_data, d_viz, nsamples, nchan );
    CUDA_TIMING_STOP(tstop, tstart, 0, "fx4ant1p", nsamples);

    CUDA_CALL( cudaMemcpy(h_viz, d_viz, nbyteviz, cudaMemcpyDeviceToHost) );
    cu_fxcorr_4ant_reshuffle(h_viz, h_viz_reshuffled, nant, nchan, 1, false);
    passed = compare(ref_viz, h_viz_reshuffled, nant, nchan, false);
    printf("Comparison against reference: %s\n", passed ? "PASS" : "FAIL");
    printf("\n");

    /////////////////////////////////////////////////////////////////////////////////

    printf("=== Single-pol with autocorrs\n");

    printf("Calculating reference result on CPU\n");
    cpu_fxcorr_1pol_withAuto(h_data, ref_viz_withAuto, nant, nsamples, nchan);

    CUDA_CALL( cudaMemset(d_viz, 0x00, nbyteviz) );
    CUDA_TIMING_START(tstart, 0);
    cu_fxcorr_4ant_1pol_withauto <<<numBlocks, threadsPerBlock>>> ( d_data, d_viz, nsamples, nchan );
    CUDA_TIMING_STOP(tstop, tstart, 0, "fx4ant1p_auto", nsamples);

    CUDA_CALL( cudaMemcpy(h_viz, d_viz, nbyteviz, cudaMemcpyDeviceToHost) );
    cu_fxcorr_4ant_reshuffle(h_viz, h_viz_reshuffled, nant, nchan, 1, true);
    passed = compare(ref_viz_withAuto, h_viz_reshuffled, nant, nchan, true);
    printf("Comparison against reference: %s\n", passed ? "PASS" : "FAIL");
    printf("\n");

    /////////////////////////////////////////////////////////////////////////////////

    printf("=== Dual-pol\n");

    printf("Calculating reference result on CPU\n");
    cpu_fxcorr_2pol((float4*)h_data, ref_viz, nant, nsamples/2, nchan);

    CUDA_CALL( cudaMemset(d_viz, 0x00, nbyteviz) );
    CUDA_TIMING_START(tstart, 0);
    cu_fxcorr_4ant_2pol <<<numBlocks, threadsPerBlock>>> ( (float4*)d_data, d_viz, nsamples/2, nchan );
    CUDA_TIMING_STOP(tstop, tstart, 0, "fx4ant2p", nsamples/2);

    CUDA_CALL( cudaMemcpy(h_viz, d_viz, nbyteviz, cudaMemcpyDeviceToHost) );
    cu_fxcorr_4ant_reshuffle(h_viz, h_viz_reshuffled, nant, nchan, 4, false);
    passed = compare4stokes(ref_viz, h_viz_reshuffled, nant, nchan, false);
    printf("Comparison against reference: %s\n", passed ? "PASS" : "FAIL");
    printf("\n");

    /////////////////////////////////////////////////////////////////////////////////

    printf("=== Dual-pol with autocorrs\n");

    printf("Calculating reference result on CPU\n");
    cpu_fxcorr_2pol_withAuto((float4*)h_data, ref_viz, nant, nsamples/2, nchan);

    CUDA_CALL( cudaMemset(d_viz, 0x00, nbyteviz) );
    CUDA_TIMING_START(tstart, 0);
    cu_fxcorr_4ant_2pol_withAuto <<<numBlocks, threadsPerBlock>>> ( (float4*)d_data, d_viz, nsamples/2, nchan );
    CUDA_TIMING_STOP(tstop, tstart, 0, "fx4ant2p_auto", nsamples/2);

    CUDA_CALL( cudaMemcpy(h_viz, d_viz, nbyteviz, cudaMemcpyDeviceToHost) );
    cu_fxcorr_4ant_reshuffle(h_viz, h_viz_reshuffled, nant, nchan, 4, true);
    passed = compare4stokes(ref_viz, h_viz_reshuffled, nant, nchan, true);
    printf("Comparison against reference: %s\n", passed ? "PASS" : "FAIL");
    printf("\n");

    /////////////////////////////////////////////////////////////////////////////////

    printf("=== Dual-pol with autocorrs, input antenna signals not interleaved\n");

    const float2* h_data_split[8];
    const float2* d_data_split[8];
    for (int n=0; n<8; n++) {
        h_data_split[n] = ((float2*)h_data) + n * (nsamples/2);
        d_data_split[n] = ((float2*)d_data) + n * (nsamples/2);
    }

    printf("Calculating reference result on CPU\n");
    cpu_fxcorr_2pol_withAuto(h_data_split, ref_viz, nant, nsamples/2, nchan);

    CUDA_CALL( cudaMemset(d_viz, 0x00, nbyteviz) );
    CUDA_TIMING_START(tstart, 0);
    fxcorr_4ant_2pol_withAuto(numBlocks, threadsPerBlock, d_data_split, d_viz, nsamples/2, nchan );
    CUDA_TIMING_STOP(tstop, tstart, 0, "fx4ant2p_split", nsamples/2);

    CUDA_CALL( cudaMemcpy(h_viz, d_viz, nbyteviz, cudaMemcpyDeviceToHost) );
    cu_fxcorr_4ant_reshuffle(h_viz, h_viz_reshuffled, nant, nchan, 4, true);
    passed = compare4stokes(ref_viz, h_viz_reshuffled, nant, nchan, true);
    printf("Comparison against reference: %s\n", passed ? "PASS" : "FAIL");
    printf("\n");

    /////////////////////////////////////////////////////////////////////////////////

    if (passed) {
        printf("=== Multi-iteration throughput for: Dual-pol with autocorrs, input antenna signals not interleaved\n");

        const size_t nsamp = nsamples/2; // single pol signals treated as dual pol
        memset(rates, 0, sizeof(rates));
        for (int iter=0; iter<Niter; iter++) {
            float dt_ms;
            CUDA_CALL( cudaMemset(d_viz, 0x00, nbyteviz) );
            CUDA_CALL( cudaEventRecord(tstart, 0) );
            fxcorr_4ant_2pol_withAuto(numBlocks, threadsPerBlock, d_data_split, d_viz, nsamp, nchan);
            CUDA_CALL( cudaEventRecord(tstop, 0) );
            CUDA_CALL( cudaEventSynchronize(tstop) );
            CUDA_CALL( cudaEventElapsedTime(&dt_ms, tstart, tstop) );
            rates[iter] = (1e-6*nsamp)/(1e-3*dt_ms);
        }

        computeStats(rates, Niter, mean, std, vmin, vmax);
        printf("Throughput statistics after %d iterations: %6.1f +- %4.1f  min %6.1f max %6.1f Ms/s/ant/pol\n",
            Niter, mean, std, vmin, vmax);
    }


    return 0;
}

#endif
