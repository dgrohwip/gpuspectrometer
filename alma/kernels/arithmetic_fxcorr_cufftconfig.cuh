/////////////////////////////////////////////////////////////////////////////////////
//
// Assisting funcs to prepare CUDA FFT for data input to 4-baseband cross-correlation
// Must have a number of channels that is a multiple of 32. Must have 2-pol data.
//
// The FFT input stage performs pre-FFT time domain fringe stopping via phase
// rotation of the real-valued input samples. Two methods for phase rotation
// are supported:
// 1) supply 4-antenna X,Y signals and short vectors containing phasor values to
//    apply to stretches of >100 samples of each of signal (constant-phase stretches)
// 2) supply 4-antenna X,Y signals and phase polynomial coefficients for each antenna
//    signal, coefficients 'c' as in phase[n]/pi = c[0] + c[1]*n + c[2]*n^2 + c[3]*n^3
//
// The FFT output of 4-ant 2-pol spectra are interleaved in the way
// that arithmetic_fxcorr_kernels expect.
//
// (C) 2019 Jan Wagner
//
/////////////////////////////////////////////////////////////////////////////////////
#ifndef ARITHMETIC_FXCORR_CUFFTCONFIG_CUH
#define ARITHMETIC_FXCORR_CUFFTCONFIG_CUH

#include <cufft.h>
#include <cstddef>

class FXCorrFFT4 {

    enum CallbackType { NONE=0, PHASOR_LUT, SINCOS_LINEAR, SINCOS_CUBIC };

    public:
        /** Construct FX FFT object for fringe stopped 4-antenna 2-pol FFT */
        FXCorrFFT4(size_t Nsamp_per_antenna, size_t Nchan, size_t Nphasors, size_t NsampPerPhasor)
         : Nant_(4), Nantpol_(4*2), Lbatch_(Nsamp_per_antenna/Nchan),
           Nsamp_per_antenna_(Nsamp_per_antenna), Nchan_(Nchan), Nphasors_(Nphasors), NsampPerPhasor_(NsampPerPhasor)
        {
            init();
        }

        ~FXCorrFFT4() { deinit(); }

    private:
        void init();
        void deinit();

    public:
        /** Set value of phasor for antenna and phasor nr */
        void setPhasor(int ant, int phasor, cufftComplex valuePolX, cufftComplex valuePolY);

        /** Fill out phasor list for antenna from data in given host-side arrays */
        void setPhasors(int ant, const cufftComplex* valuesPolX, cufftComplex* valuesPolY);

    public:
        /**
         * Execute 4-antenna 2-pol FFT transforms (8 FFT) with intrinsic fringe rotation.
         * Utilizes complex phasors previously loaded with setPhasors() or setPhasor()
         * to apply the fringe rotation. Phasors are constant for stretches of 100+ samples.
         *
         * Input: X,Y pol signals from antennas stored in 8 independent arrays
         * Output: single array with complex antenna/pol-interleaved frequency domain data
         */
        void transform(const float* d_X[4], const float* d_Y[4], cufftComplex* d_fftsOut);

        /**
         * Execute 4-antenna 2-pol FFT transforms (8 FFT) with intrinsic fringe rotation.
         * Utilizes linear phase slopes (2 coeff) or 3rd order (4 coeff) for each of the 8 signals.
         *
         * Input: X,Y pol signals of 4 antennas and their phase slopes
         * Output: single array with complex antenna/pol-interleaved frequency domain data
         */
        void transform(int phasePolyOrder,
            const float* d_X[4], float* phaseXCoeffs[4],
            const float* d_Y[4], float* phaseYCoeffs[4],
            cufftComplex* d_fftsOut);

    private:
        /**
         * Execute c2c FFT on real-valued samples of one antenna in two polarizations.
         * Samples are converted from real to complex (with phase rotation) at the time of FFT,
         * utilizing phasor lookup tables.
         */
        void transformAntenna(int antenna, const float* __restrict__ d_X, const float* __restrict__ d_Y,
            cufftComplex* __restrict__ d_fftsOut);

        /**
         * Execute c2c FFT on real-valued samples of one antenna in two polarizations.
         * Samples are converted from real to complex (with phase rotation) at the time of FFT,
         * utilizing continous linear poly with sincos() for every single sample.
         */
        void transformAntennaSinCos(int antenna, int polyOrder,
            const float* __restrict__ d_X, const float* phasePolyX,
            const float* __restrict__ d_Y, const float* phasePolyY,
            cufftComplex* __restrict__ d_fftsOut);

        /** Switch CUFFT Callback to the specified type of internal callback function (none, phasor, sin/cos) */
        void registerCallback(CallbackType ct);

    public:

         typedef struct cu_fxcorr_FFT_cb_params_tt {
             // Phasor lookup related
             const cufftComplex* d_phasors; // array with one new value for every 'NsampPerPhasor' samples
             size_t Nphasors;               // total nr of phasors
             size_t NsampPerPhasor;         // each phasor value valid for this many samples
             size_t sampleOffset;           // value to *add* to sample index before looking up d_phasors[(sampleIdx + sampleOffset)/NsampPerPhasor]
             // Continuous rotation related
             float phasePoly[4];            // 0: phase of sample#0, 1: rate in phase/sample, 2: accel in phase/sample^2, 3: phase/sample^3; all in units suitable for sincospif()
         } cu_fxcorr_FFT_cb_params_t;

    private:

         const size_t Nant_;
         const size_t Nantpol_;
         const size_t Nsamp_per_antenna_;
         const size_t Nchan_;
         const size_t Lbatch_;

         const size_t Nphasors_;
         const size_t NsampPerPhasor_;

         cufftHandle c2cPlan;

         cu_fxcorr_FFT_cb_params_t* rotatorParams_;

         cufftComplex* m_phasors_X_[4];
         cufftComplex* m_phasors_Y_[4];

         CallbackType currentRegisteredCallback;

};


#endif // ARITHMETIC_FXCORR_CUFFTCONFIG_CUH

