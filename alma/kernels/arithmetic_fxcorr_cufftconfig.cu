/////////////////////////////////////////////////////////////////////////////////////
//
// Assisting funcs to prepare CUDA FFT for data input to 4-baseband cross-correlation
// Must have a number of channels that is a multiple of 32. Must have 2-pol data.
//
// The FFT input stage performs pre-FFT time domain fringe stopping via phase
// rotation of the real-valued input samples. Two methods for phase rotation
// are supported:
// 1) supply 4-antenna X,Y signals and short vectors containing phasor values to
//    apply to stretches of >100 samples of each of signal (constant-phase stretches)
// 2) supply 4-antenna X,Y signals and phase polynomial coefficients for each antenna
//    signal, coefficients 'c' as in phase[n]/pi = c[0] + c[1]*n + c[2]*n^2 + c[3]*n^3
//
// The FFT output of 4-ant 2-pol spectra are interleaved in the way
// that arithmetic_fxcorr_kernels expect.
//
// (C) 2019 Jan Wagner
//
/////////////////////////////////////////////////////////////////////////////////////

#include <cassert>
#include <cuda.h>
#include <cufft.h>
#include <math.h>

#include "cuda_utils.cu"
#include "arithmetic_fxcorr_cufftconfig.cuh" // wrapper class FXCorrFFT4

#define FX_FFT_INTERLEAVE_OUTPUT 0  // 0 to write each signal to different output (turned out fast @4.5GS/s), 1 to interleave 8 signals (slower @2.5GS/s)

/// Callback routine for CUFFT, no-op load of real to comples, without fringe rotation
static __device__ cufftComplex CB_loadReal(void *dataIn, size_t index, void *cb_info, void *shmem)
{
    // No fringe rotation
    cufftReal sample = ((cufftReal*)dataIn)[index];
    return (cufftComplex) { sample, 0.0f };
}


/// Callback routine for CUFFT to produce fringe-rotated complex values from samples via phasor lookup table
static __device__ cufftComplex CB_loadRotateLUTReal(void *dataIn, size_t index, void *cb_info, void *shmem)
{
    // Apply pre-FFT fringe rotation
    FXCorrFFT4::cu_fxcorr_FFT_cb_params_t* params = (FXCorrFFT4::cu_fxcorr_FFT_cb_params_t*)cb_info;
    cufftReal sample = ((cufftReal*)dataIn)[index];
    size_t indexLUT = (index + params->sampleOffset) / params->NsampPerPhasor;
    cufftComplex phasor = params->d_phasors[indexLUT];
    return (cufftComplex) { sample*phasor.x, sample*phasor.y };
}

/// Callback routine for CUFFT to produce fringe-rotated complex values from samples via continousphase rotation
template<int order>
static inline __device__ cufftComplex CB_loadRotateSinCosReal_T(void *dataIn, size_t index, void *cb_info, void *shmem)
{
    // Evaluate phase polynomial
    FXCorrFFT4::cu_fxcorr_FFT_cb_params_t* params = (FXCorrFFT4::cu_fxcorr_FFT_cb_params_t*)cb_info;
    float theta = params->phasePoly[order];
    for (int n=order; n>=1; n--) {
        theta = params->phasePoly[n-1] + theta*(float)index;
    }

    // Phase to complex phasor
    // NOTE: sincos() in CUDA library
    //       could also use this one: https://devtalk.nvidia.com/default/topic/957538/sincospif-implementation-with-improved-performance-and-accuracy/
    cufftComplex phasor;
    sincospif(theta, &phasor.x, &phasor.y);

    // Get sample
    unsigned int phasewrapsamples = __float2uint_rd(theta) / 2;
    cufftReal sample = ((cufftReal*)dataIn)[index + phasewrapsamples];

    // Apply pre-FFT fringe rotation
    return (cufftComplex) { sample*phasor.x, sample*phasor.y };
}

static __device__ cufftComplex CB_loadRotateSinCosLinearReal(void *dataIn, size_t index, void *cb_info, void *shmem)
{
    return CB_loadRotateSinCosReal_T<1>(dataIn, index, cb_info, shmem);
}

static __device__ cufftComplex CB_loadRotateSinCosCubicReal(void *dataIn, size_t index, void *cb_info, void *shmem)
{
    return CB_loadRotateSinCosReal_T<3>(dataIn, index, cb_info, shmem);
}

/// Device pointer to the callback routine. Needed by cufftXtSetCallback(), cannot be __managed__ (CUFFT limitation).
__device__ cufftCallbackLoadC d_CB_loadReal_ptr = CB_loadReal;
__device__ cufftCallbackLoadC d_CB_loadRotateLUTReal_ptr = CB_loadRotateLUTReal;
__device__ cufftCallbackLoadC d_CB_loadRotateSinCosLinearReal_ptr = CB_loadRotateSinCosLinearReal;
__device__ cufftCallbackLoadC d_CB_loadRotateSinCosCubicReal_ptr = CB_loadRotateSinCosCubicReal;

/////////////////////////////////////////////////////////////////////////////////////

/**
 * Prepare for 4-antenna 2-pol FFTs (8 FFT) with fringe rotation.
 */
void FXCorrFFT4::init()
{
    // CUFFT Plan c2c FFT
    int dimn[1] = {(int)Nchan_};   // DFT size
    int inembed[1] = {0};          // ignored for 1D xform
    int onembed[1] = {0};          // ignored for 1D xform
    int istride = 1;               // step between successive input elems
#if FX_FFT_INTERLEAVE_OUTPUT
    int ostride = Nantpol_;        // step between successive output elems; interleave outputs
#else
    int ostride = 1;               // step between successive output elems; do not interleave
#endif
    int idist = Nchan_;
    int odist = Nchan_;
    int batch = Lbatch_;

    // ostride = 1; // 4.5 GS/s
    // ostride = Nantpol_; // 2.6 GS/s

    CUFFT_CALL( cufftPlanMany(&c2cPlan,
        1, dimn,
        inembed, istride, idist,
        onembed, ostride, odist,
        CUFFT_C2C,
        batch)
    );

    // Phase storage
    for (size_t n=0; n<Nant_; n++) {
        CUDA_CALL( cudaMallocManaged((void**)&m_phasors_X_[n], Nphasors_*sizeof(cufftComplex)) );
        CUDA_CALL( cudaMallocManaged((void**)&m_phasors_Y_[n], Nphasors_*sizeof(cufftComplex)) );
    }

    // Storage for CUFFT callback aux parameters
    CUDA_CALL( cudaMallocManaged((void**)&rotatorParams_, sizeof(rotatorParams_)) );
    rotatorParams_->d_phasors = NULL;
    rotatorParams_->Nphasors = Nphasors_;
    rotatorParams_->NsampPerPhasor = NsampPerPhasor_;
    rotatorParams_->sampleOffset = 0;

    // Default callback handler
    currentRegisteredCallback = FXCorrFFT4::NONE;
    registerCallback(FXCorrFFT4::PHASOR_LUT);
}

/**
 * Set CUFFT Callback to the specified type of internal callback function (none, phasor, sin/cos)
 */
void FXCorrFFT4::registerCallback(CallbackType ct)
{
    if (currentRegisteredCallback != ct) {

        // Unregister callback first, required for changing it later!
        CUFFT_CALL( cufftXtClearCallback(c2cPlan, CUFFT_CB_LD_COMPLEX) );

        // Get device address of callback function and register it
        cufftCallbackLoadC h_cb_loadPtr;
        switch (ct) {
            case PHASOR_LUT:
                CUDA_CALL( cudaMemcpyFromSymbol(&h_cb_loadPtr, d_CB_loadRotateLUTReal_ptr, sizeof(h_cb_loadPtr)) );
                break;
            case SINCOS_LINEAR:
                CUDA_CALL( cudaMemcpyFromSymbol(&h_cb_loadPtr, d_CB_loadRotateSinCosLinearReal_ptr, sizeof(h_cb_loadPtr)) );
                break;
            case SINCOS_CUBIC:
                CUDA_CALL( cudaMemcpyFromSymbol(&h_cb_loadPtr, d_CB_loadRotateSinCosCubicReal_ptr, sizeof(h_cb_loadPtr)) );
                break;
            default:
                CUDA_CALL( cudaMemcpyFromSymbol(&h_cb_loadPtr, d_CB_loadReal_ptr, sizeof(h_cb_loadPtr)) );
                break;
        }

        // Register
        CUFFT_CALL( cufftXtSetCallback(c2cPlan, (void **)&h_cb_loadPtr, CUFFT_CB_LD_COMPLEX, (void**)&rotatorParams_) );
        currentRegisteredCallback = ct;
    }
}


/**
 * Clean up all allocations used by FX FFT setup.
 */
void FXCorrFFT4::deinit()
{
    CUDA_CALL( cudaFree(rotatorParams_) );
    for (size_t n=0; n<Nant_; n++) {
        CUDA_CALL( cudaFree(m_phasors_X_[n]) );
        CUDA_CALL( cudaFree(m_phasors_Y_[n]) );
    }
    CUFFT_CALL( cufftDestroy(c2cPlan) );
}

/////////////////////////////////////////////////////////////////////////////////////

/** Set value of phasor for antenna and phasor nr */
void FXCorrFFT4::setPhasor(int ant, int phasor, cufftComplex valuePolX, cufftComplex valuePolY)
{
    m_phasors_X_[ant][phasor] = valuePolX;
    m_phasors_Y_[ant][phasor] = valuePolY;
}

/** Fill out phasor list for antenna from data in given host-side array */
void FXCorrFFT4::setPhasors(int ant, const cufftComplex* valuesPolX, cufftComplex* valuesPolY)
{
    memcpy(m_phasors_X_[ant], valuesPolX, sizeof(cufftComplex)*Nphasors_);
    memcpy(m_phasors_Y_[ant], valuesPolY, sizeof(cufftComplex)*Nphasors_);
}

/////////////////////////////////////////////////////////////////////////////////////

/**
 * Execute c2c FFT on real-valued samples of one antenna in two polarizations.
 * Samples are converted from real to complex (with phase rotation) at the time of FFT,
 * utilizing phasor lookup tables.
 */
void FXCorrFFT4::transformAntenna(
    int antenna,
    const float* __restrict__ d_X, const float* __restrict__ d_Y,
    cufftComplex* __restrict__ d_fftsOut
)
{
#if FX_FFT_INTERLEAVE_OUTPUT
    size_t odist = 1;
#else
    size_t odist = Nchan_*Lbatch_;
#endif

    rotatorParams_->d_phasors = m_phasors_X_[antenna];
    rotatorParams_->sampleOffset = 0; // TODO? m_phasorOffsets_X[antenna];
    CUFFT_CALL( cufftExecC2C(c2cPlan, (cufftComplex*)d_X, d_fftsOut + odist*(2*antenna+0), CUFFT_FORWARD) );

    rotatorParams_->d_phasors = m_phasors_Y_[antenna];
    rotatorParams_->sampleOffset = 0; // TODO? m_phasorOffsets_Y[antenna];
    CUFFT_CALL( cufftExecC2C(c2cPlan, (cufftComplex*)d_Y, d_fftsOut + odist*(2*antenna+1), CUFFT_FORWARD) );
}

/**
 * Execute 4-antenna 2-pol FFT transforms (8 FFT) with intrinsic fringe rotation.
 * Utilizes complex phasors previously loaded with setPhasors() or setPhasor()
 * to apply the fringe rotation. Phasors are constant for stretches of 100+ samples.
 *
 * Input: X,Y pol signals from antennas stored in 8 independent arrays
 * Output: single array with complex antenna/pol-interleaved frequency domain data
 */
void FXCorrFFT4::transform(
    const float*  d_X[4],
    const float*  d_Y[4],
    cufftComplex*  d_fftsOut
)
{
    registerCallback(FXCorrFFT4::PHASOR_LUT);

    for (size_t ant=0; ant<Nant_; ant++) {
        transformAntenna(ant, d_X[ant], d_Y[ant], d_fftsOut);
    }
}

/////////////////////////////////////////////////////////////////////////////////////

/**
 * Execute c2c FFT on real-valued samples of one antenna in two polarizations.
 * Samples are converted from real to complex (with phase rotation) at the time of FFT,
 * utilizing continous linear poly with sincos() for every single sample.
 */
void FXCorrFFT4::transformAntennaSinCos(int antenna, int polyOrder,
    const float* __restrict__ d_X, const float* phasePolyX,
    const float* __restrict__ d_Y, const float* phasePolyY,
    cufftComplex* __restrict__ d_fftsOut)
{
    assert(polyOrder==1 || polyOrder==3);

#if FX_FFT_INTERLEAVE_OUTPUT
    size_t odist = 1;
#else
    size_t odist = Nchan_*Lbatch_;
#endif

    memcpy(rotatorParams_->phasePoly, phasePolyX, (polyOrder+1)*sizeof(float));
    CUFFT_CALL( cufftExecC2C(c2cPlan, (cufftComplex*)d_X, d_fftsOut + odist*(2*antenna+0), CUFFT_FORWARD) );

    memcpy(rotatorParams_->phasePoly, phasePolyY, (polyOrder+1)*sizeof(float));
    CUFFT_CALL( cufftExecC2C(c2cPlan, (cufftComplex*)d_Y, d_fftsOut + odist*(2*antenna+1), CUFFT_FORWARD) );
}

/**
 * Execute 4-antenna 2-pol FFT transforms (8 FFT) with intrinsic fringe rotation.
 * Utilizes linear phase slopes (2 coeff) or 3rd order (4 coeff) for each of the 8 signals.
 *
 * Input: X,Y pol signals of 4 antennas and their phase slopes
 * Output: single array with complex antenna/pol-interleaved frequency domain data
 */
void FXCorrFFT4::transform(int phasePolyOrder,
    const float* d_X[4], float* phasePolysX[4],
    const float* d_Y[4], float* phasePolysY[4],
    cufftComplex*  d_fftsOut
)
{
    assert(phasePolyOrder==1 || phasePolyOrder==3);
    if (phasePolyOrder==1) {
        registerCallback(FXCorrFFT4::SINCOS_LINEAR);
    } else {
        registerCallback(FXCorrFFT4::SINCOS_CUBIC);
    }

    for (size_t ant=0; ant<Nant_; ant++) {
        transformAntennaSinCos(ant, phasePolyOrder,
            d_X[ant], phasePolysX[ant],
            d_Y[ant], phasePolysY[ant],
            d_fftsOut);
    }
}


/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

#ifdef BENCH_FXFFT // standalone compile mode for testing

#define CUDA_DEVICE_NR 0

int main(int argc, char** argv)
{
    const size_t Nant = 4;
    const size_t Nchan = 128;
    const size_t Nsamp_per_antenna = 4e-3 * 4e9; // 4 millisec at 4 GS/s
    const size_t NsampPerPhasor = 16 * 100;
    const size_t Nphasors = (Nsamp_per_antenna / NsampPerPhasor) + 1;

    cudaDeviceProp cudaDevProp;
    cudaEvent_t tstart, tstop;
    float dT_msec;

    CUDA_CALL( cudaGetDeviceProperties(&cudaDevProp, CUDA_DEVICE_NR) );
    CUDA_CALL( cudaSetDevice(CUDA_DEVICE_NR) );
    CUDA_CALL( cudaDeviceReset() );
    CUDA_CALL( cudaEventCreate(&tstart) );
    CUDA_CALL( cudaEventCreate(&tstop) );

    float* d_signals[2*Nant];
    cufftComplex* d_fftOut;

    // Input and output arrays
    printf("Preparing arrays and data for 4-antenna 2-pol %zu-channel FX FFT processing with pre-FFT fringe stopping...\n", Nchan);
    for (size_t n=0; n<2*Nant; n++) {
        CUDA_CALL( cudaMalloc((void **)&d_signals[n], Nsamp_per_antenna*sizeof(float)) );
    }
    CUDA_CALL( cudaMalloc((void **)&d_fftOut, Nsamp_per_antenna*sizeof(cufftComplex)*2*Nant) );

    // Synthetic data
    if (1) {
        printf("Generating synthetic data...\n");
        float* h_signal;
        CUDA_CALL( cudaMallocHost((void**)&h_signal, Nsamp_per_antenna*sizeof(float)) );
        for (size_t n=0; n<2*Nant; n++) {
            for(size_t s=0; s<Nsamp_per_antenna; s++) {
                h_signal[s] = ((s + n) % 1237) * (n + 1);
            }
            CUDA_CALL( cudaMemcpy(d_signals[n], h_signal, Nsamp_per_antenna*sizeof(float), cudaMemcpyHostToDevice) );
        }
        CUDA_CALL( cudaFreeHost(h_signal) );
    }

    // Prepare FX FFT
    FXCorrFFT4* fxcorrFFT = new FXCorrFFT4(Nsamp_per_antenna, Nchan, Nphasors, NsampPerPhasor);

    // Invent some phasing info
    printf("Generating pre-FFT fringe stop phase data with %zu points, each valid for %zu samples...\n", Nphasors, NsampPerPhasor);
    for (size_t n=0; n<Nant; n++) {
        for (size_t p=0; p<Nphasors; p++) {
            float theta = M_PI / (n+4.0f) + (M_PI/8.0f)*p;
            float dtheta = M_PI*(n+1)/10.0f;
            cufftComplex px = (cufftComplex) { cosf(theta), sinf(theta) };
            cufftComplex py = (cufftComplex) { cosf(theta+dtheta), sinf(theta+dtheta) };
            fxcorrFFT->setPhasor(n, p, px,py);
        }
    }

    printf("Generating phase polynomial coeffs for pre-FFT continous per-sample sin/cos fringe stopping...\n");
    float* polyX[Nant];
    float* polyY[Nant];
    for (size_t n=0; n<Nant; n++) {
        polyX[n] = new float[4]();
        polyY[n] = new float[4]();
        polyX[n][0] = 0.010f + 0.002f*n;
        polyX[n][1] = -1.7e-3f + 0.002f*n;
        polyX[n][2] = -3.1e-5f + 0.003f*n;
        polyX[n][3] = -2.7e-7f + 0.005f*n;
        polyY[n][0] = 0.010f + 0.002f*n;
        // [1..3] = 0.0f
    }


    // Phasor LUT based fringe-stopping FFT
    printf("Executing fringe-stopping FFT transform set with LUT-based phasors...\n");
    CUDA_CALL( cudaEventRecord(tstart, 0) );
    fxcorrFFT->transform((const float**)&d_signals[0], (const float**)&d_signals[4], d_fftOut);
    CUDA_CALL( cudaEventRecord(tstop, 0) );

    CUDA_CALL( cudaEventSynchronize(tstop) );
    CUDA_CALL( cudaEventElapsedTime(&dT_msec, tstart, tstop) );
    printf("Elapsed time: %.3f ms, rate %.3f GS/s/ant/pol\n\n", dT_msec, ((float)Nsamp_per_antenna / (1e6*dT_msec)));

    // Phase polynomial based fringe-stopping FFT: linear, 1st order poly
    printf("Executing sin/cos linear (order=1) phase fringe-stopping FFT transform set...\n");
    CUDA_CALL( cudaEventRecord(tstart, 0) );
    fxcorrFFT->transform(1,
        (const float**)&d_signals[0], polyX,
        (const float**)&d_signals[4], polyY,
        d_fftOut);
    CUDA_CALL( cudaEventRecord(tstop, 0) );

    CUDA_CALL( cudaEventSynchronize(tstop) );
    CUDA_CALL( cudaEventElapsedTime(&dT_msec, tstart, tstop) );
    printf("Elapsed time: %.3f ms, rate %.3f GS/s/ant/pol\n\n", dT_msec, ((float)Nsamp_per_antenna / (1e6*dT_msec)));

    // Phase polynomial based fringe-stopping FFT: cubic, 3rd order poly
    printf("Executing sin/cos cubic (order=3) phase fringe-stopping FFT transform set...\n");
    CUDA_CALL( cudaEventRecord(tstart, 0) );
    fxcorrFFT->transform(3,
        (const float**)&d_signals[0], polyX,
        (const float**)&d_signals[4], polyY,
        d_fftOut);
    CUDA_CALL( cudaEventRecord(tstop, 0) );

    CUDA_CALL( cudaEventSynchronize(tstop) );
    CUDA_CALL( cudaEventElapsedTime(&dT_msec, tstart, tstop) );
    printf("Elapsed time: %.3f ms, rate %.3f GS/s/ant/pol\n", dT_msec, ((float)Nsamp_per_antenna / (1e6*dT_msec)));

    // Cleanup ad device reset for 'cuda-memcheck'
    delete fxcorrFFT;
    CUDA_CALL( cudaDeviceReset() );

    return 0;
}

/////////////////////////////////////////////////////////////////////////////////////

#endif
