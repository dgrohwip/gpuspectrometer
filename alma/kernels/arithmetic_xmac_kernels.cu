/////////////////////////////////////////////////////////////////////////////////////
//
// Cross-power spectrum kernels (accumulation of |X|^2 after FFT of signal x).
//
// (C) 2015 Jan Wagner
//
// Performance on Titan X is about 17.0 Gs/s complex per polarization (34 Gs/s total)
// for non-interleaved output, and ~17.1 Gs/s complex per polarization for interleaved.
//
/////////////////////////////////////////////////////////////////////////////////////
// $ nvcc -DBENCH=1 -gencode=arch=compute_52,code=sm_52 arithmetic_xmac_kernels.cu -o arithmetic_xmac_kernel
/////////////////////////////////////////////////////////////////////////////////////

#ifndef ARITHMETIC_XMAC_KERNELS_CU
#define ARITHMETIC_XMAC_KERNELS_CU

#define XMAC_OUTPUT_INTERLEAVED 0   // 1 to store [XX Re{XY} Im{XY} YY],
                                    // 0 to store [XX XX ... YY YY ... Re Im Re Im ...]

#include "index_mapping.cu"

/////////////////////////////////////////////////////////////////////////////////////

__global__ void cu_accumulate_2pol(const float2* __restrict__ x, const float2* __restrict__ y, float4* __restrict__ acc, size_t Lfft, size_t Nfft);
__global__ void cu_accumulate_2pol_skipNyquist(const float2* __restrict__ x, const float2* __restrict__ y, float4* __restrict__ acc, size_t Lfft, size_t Nfft);

__global__ void cu_accumulate_2pol_packed(const float4* __restrict__ xy, float4* __restrict__ acc, size_t Lfft, size_t Nfft);
__global__ void cu_accumulate_2pol_autoOnly(const float2* __restrict__ x, const float2* __restrict__ y,
                                   float2* __restrict__ acc, size_t Lfft, size_t Nfft);
__global__ void cu_accumulate_2pol_autoOnly_packed(const float4* __restrict__ xy, float2* __restrict__ acc, size_t Lfft, size_t Nfft);

/////////////////////////////////////////////////////////////////////////////////////

__device__ float4& operator/=(float4 &a, const float f)
{
        a.x /= f; a.y /= f; a.z /= f; a.w /= f;
        return a;
}

__device__ float2& operator/=(float2 &a, const float f)
{
        a.x /= f; a.y /= f;
        return a;
}

/////////////////////////////////////////////////////////////////////////////////////

__global__ void cu_accumulate_2pol(const float2* __restrict__ x, const float2* __restrict__ y,
                                   float4* __restrict__ acc, size_t Lfft, size_t Nfft)
{
        size_t nthreads = blockDim.x * gridDim.x;
        size_t idx = blockIdx.x * blockDim.x + threadIdx.x;
        size_t last_idx = Lfft * Nfft;
        size_t bin = idx % Lfft;

        // Note: X*conj(Y) = (re1 + i im1) (re2 - i im2) = re1*re2 + im1*im2 - i re1*im2 + i re2*im1
        //                 ^= two float2 in              ^= one float4 out

        float4 a = {0.0f, 0.0f, 0.0f, 0.0f};

        while (idx < last_idx) {

             float2 P1 = x[idx]; // {Re,Im} from "X-pol"
             float2 P2 = y[idx]; // {Re,Im} from "Y-pol"

             // TODO: use Kahan summation?

             a.x +=  P1.x*P1.x + P1.y*P1.y; // .x = 1st <XX'>
             a.y +=  P1.x*P2.x + P1.y*P2.y; // .y = 1st Re{<XY'>}
             a.z += -P1.x*P2.y + P1.y*P2.x; // .z = 1st Im{<XY'>}
             a.w +=  P2.x*P2.x + P2.y*P2.y; // .w = 1st <YY'>

             idx += nthreads;
        }

        //a /= (float)Nfft;

#if XMAC_OUTPUT_INTERLEAVED
        // Layout : [XX Re{XY} Im{XY} YY]
        atomicAdd(&(acc[bin].x), a.x);
        atomicAdd(&(acc[bin].y), a.y);
        atomicAdd(&(acc[bin].z), a.z);
        atomicAdd(&(acc[bin].w), a.w);
#else
        // Layout : [XX XX XX ... YY YY YY ... Re{XY} Im{XY} Re{XY} Im{XY} Re{XY} Im{XY} ...]
        float* acc1f = (float*)acc;
        atomicAdd(&(acc1f[bin]),            a.x);
        atomicAdd(&(acc1f[Lfft+bin]),       a.w);
        atomicAdd(&(acc1f[2*Lfft+2*bin+0]), a.y);
        atomicAdd(&(acc1f[2*Lfft+2*bin+1]), a.z);
#endif

        return;
}

__global__ void cu_accumulate_2pol_skipNyquist(const float2* __restrict__ x, const float2* __restrict__ y,
                                   float4* __restrict__ acc, size_t Nchout, size_t Nfft)
{
        size_t nthreads = blockDim.x * gridDim.x;
        size_t idx = blockIdx.x * blockDim.x + threadIdx.x;
        size_t last_idx = Nchout * Nfft;
        size_t bin = idx % Nchout;

        // Note: X*conj(Y) = (re1 + i im1) (re2 - i im2) = re1*re2 + im1*im2 - i re1*im2 + i re2*im1
        //                 ^= two float2 in              ^= one float4 out

        float4 a = {0.0f, 0.0f, 0.0f, 0.0f};

        while (idx < last_idx) {

             size_t memidx = reindex_skip_Nyquist(Nchout, idx);
             float2 P1 = x[memidx]; // {Re,Im} from "X-pol"
             float2 P2 = y[memidx]; // {Re,Im} from "Y-pol"

             // TODO: use Kahan summation?

             a.x +=  P1.x*P1.x + P1.y*P1.y; // .x = 1st <XX'>
             a.y +=  P1.x*P2.x + P1.y*P2.y; // .y = 1st Re{<XY'>}
             a.z += -P1.x*P2.y + P1.y*P2.x; // .z = 1st Im{<XY'>}
             a.w +=  P2.x*P2.x + P2.y*P2.y; // .w = 1st <YY'>

             idx += nthreads;
        }

        //a /= (float)Nfft;

#if XMAC_OUTPUT_INTERLEAVED
        // Layout : [XX Re{XY} Im{XY} YY]
        atomicAdd(&(acc[bin].x), a.x);
        atomicAdd(&(acc[bin].y), a.y);
        atomicAdd(&(acc[bin].z), a.z);
        atomicAdd(&(acc[bin].w), a.w);
#else
        // Layout : [XX XX XX ... YY YY YY ... Re{XY} Im{XY} Re{XY} Im{XY} Re{XY} Im{XY} ...]
        float* acc1f = (float*)acc;
        atomicAdd(&(acc1f[bin]),            a.x);
        atomicAdd(&(acc1f[Nchout+bin]),       a.w);
        atomicAdd(&(acc1f[2*Nchout+2*bin+0]), a.y);
        atomicAdd(&(acc1f[2*Nchout+2*bin+1]), a.z);
#endif

        return;
}

__global__ void cu_accumulate_2pol_packed(const float4* __restrict__ xy, float4* __restrict__ acc, size_t Lfft, size_t Nfft)
{
        size_t nthreads = blockDim.x * gridDim.x;
        size_t idx = blockIdx.x * blockDim.x + threadIdx.x;
        size_t last_idx = Lfft * Nfft;
        size_t bin = idx % Lfft;

        // Note: X*conj(Y) = (re1 + i im1) (re2 - i im2) = re1*re2 + im1*im2 - i re1*im2 + i re2*im1
        //                 ^= two float2 in              ^= one float4 out

        float4 a = {0.0f, 0.0f, 0.0f, 0.0f};

        while (idx < last_idx) {

             float2 P1 = { xy[idx].x, xy[idx].y }; // {Re,Im} from "X-pol"
             float2 P2 = { xy[idx].z, xy[idx].w }; // {Re,Im} from "Y-pol"

             // TODO: use Kahan summation?

             a.x +=  P1.x*P1.x + P1.y*P1.y; // .x = 1st <XX'>
             a.y +=  P1.x*P2.x + P1.y*P2.y; // .y = 1st Re{<XY'>}
             a.z += -P1.x*P2.y + P1.y*P2.x; // .z = 1st Im{<XY'>}
             a.w +=  P2.x*P2.x + P2.y*P2.y; // .w = 1st <YY'>

             idx += nthreads;
        }

        //a /= (float)Nfft;

#if XMAC_OUTPUT_INTERLEAVED
        // Layout : [XX Re{XY} Im{XY} YY]
        atomicAdd(&(acc[bin].x), a.x);
        atomicAdd(&(acc[bin].y), a.y);
        atomicAdd(&(acc[bin].z), a.z);
        atomicAdd(&(acc[bin].w), a.w);
#else
        // Layout : [XX XX XX ... YY YY YY ... Re{XY} Im{XY} Re{XY} Im{XY} Re{XY} Im{XY} ...]
        float* acc1f = (float*)acc;
        atomicAdd(&(acc1f[bin]),            a.x);
        atomicAdd(&(acc1f[Lfft+bin]),       a.w);
        atomicAdd(&(acc1f[2*Lfft+2*bin+0]), a.y);
        atomicAdd(&(acc1f[2*Lfft+2*bin+1]), a.z);
#endif

        return;

}

///////////////////////////////////////////////////////////////////////

__global__ void cu_accumulate_2pol_autoOnly(const float2* __restrict__ x, const float2* __restrict__ y,
                                   float2* __restrict__ acc, size_t Lfft, size_t Nfft)
{
        size_t nthreads = blockDim.x * gridDim.x;
        size_t idx = blockIdx.x * blockDim.x + threadIdx.x;
        size_t last_idx = Lfft * Nfft;
        size_t bin = idx % Lfft;

        float2 a = {0.0f, 0.0f};

        while (idx < last_idx) {

             float2 P1 = x[idx]; // {Re,Im} from "X-pol"
             float2 P2 = y[idx]; // {Re,Im} from "Y-pol"

             // TODO: use Kahan summation?
             // TODO: potentially bad performance when input data are not interleaved

             a.x +=  P1.x*P1.x + P1.y*P1.y; // .x = 1st <XX'>
             a.y +=  P2.x*P2.x + P2.y*P2.y; // .w = 1st <YY'>

             idx += nthreads;
        }

        //a /= (float)Nfft;

#if XMAC_OUTPUT_INTERLEAVED
        // Layout : [XX YY XX YY XX YY]
        atomicAdd(&(acc[bin].x), a.x);
        atomicAdd(&(acc[bin].y), a.y);
#else
        // Layout : [XX XX XX ... YY YY YY ... ]
        float* acc1f = (float*)acc;
        atomicAdd(&(acc1f[bin]),      a.x);
        atomicAdd(&(acc1f[Lfft+bin]), a.y);
#endif

        return;
}

__global__ void cu_accumulate_2pol_autoOnly_packed(const float4* __restrict__ xy, float2* __restrict__ acc, size_t Lfft, size_t Nfft)
{
        size_t nthreads = blockDim.x * gridDim.x;
        size_t idx = blockIdx.x * blockDim.x + threadIdx.x;
        size_t last_idx = Lfft * Nfft;
        size_t bin = idx % Lfft;

        float2 a = {0.0f, 0.0f};

        while (idx < last_idx) {

             float2 P1 = { xy[idx].x, xy[idx].y }; // {Re,Im} from "X-pol"
             float2 P2 = { xy[idx].z, xy[idx].w }; // {Re,Im} from "Y-pol"

             // TODO: use Kahan summation?

             a.x +=  P1.x*P1.x + P1.y*P1.y; // .x = 1st <XX'>
             a.y +=  P2.x*P2.x + P2.y*P2.y; // .w = 1st <YY'>

             idx += nthreads;
        }

        //a /= (float)Nfft;

#if XMAC_OUTPUT_INTERLEAVED
        // Layout : [XX Re{XY} Im{XY} YY]
        atomicAdd(&(acc[bin].x), a.x);
        atomicAdd(&(acc[bin].y), a.y);
#else
        // Layout : [XX XX XX ... YY YY YY ... ]
        float* acc1f = (float*)acc;
        atomicAdd(&(acc1f[bin]),      a.x);
        atomicAdd(&(acc1f[Lfft+bin]), a.y);
#endif

        return;

}

/////////////////////////////////////////////////////////////////////////////////////

#ifdef BENCH // standalone compile mode for testing

#ifndef CUDA_DEVICE_NR
    #define CUDA_DEVICE_NR 0
#endif
#define CHECK_TIMING 1
#include "cuda_utils.cu"

__global__ void cu_accu_2pol_testdata(float2* x, float2* y, const size_t N, const size_t nchan)
{
        size_t i = blockIdx.x * blockDim.x + threadIdx.x;
        if (i >= N) { return; }
#if 0
        float2 vx = {(float)(i%nchan + 1), 1.0f};
        float2 vy = {-1.0f, -(float)(i%nchan + 1)};
#else
        float2 vx = {+1.0f, +1.0f};
        float2 vy = {-1.0f, -1.0f};
#endif
        x[i] = vx;
        y[i] = vy;
}

int main(int argc, char **argv)
{
    cudaDeviceProp cudaDevProp;
    cudaEvent_t tstart, tstop;
    size_t threadsPerBlock, numBlocks;
    size_t maxphysthreads;
    size_t nsamples, nchan, nffts;
    size_t nfree, ntotal, memreq;
    float *d_xx_xy_yy;
    float *d_x;
    float *d_y;
    float *h_d;

    CUDA_CALL( cudaSetDevice(CUDA_DEVICE_NR) );
    CUDA_CALL( cudaDeviceReset() );
    CUDA_CALL( cudaMemGetInfo(&nfree, &ntotal) );
    CUDA_CALL( cudaGetDeviceProperties(&cudaDevProp, CUDA_DEVICE_NR) );
    maxphysthreads = cudaDevProp.multiProcessorCount * cudaDevProp.maxThreadsPerMultiProcessor;
    CUDA_CALL( cudaEventCreate( &tstart ) );
    CUDA_CALL( cudaEventCreate( &tstop ) );

    // Input
    nchan = 1024*1024;  // 1024k complex values per spectrum
    nffts = 625;  // 625 spectra
    if (argc == 3) {
        nchan = atoi(argv[1]);
        nffts = atoi(argv[2]);
    }
    nsamples = nchan * nffts;
    memreq = 2*nsamples*sizeof(float2) + nchan*sizeof(float4);
    if ( memreq > nfree ) {
        while ( memreq > nfree ) {
            nffts -= 32;
            nsamples = nchan * nffts;
            memreq = 2*nsamples*sizeof(float2) + nchan*sizeof(float4);
        }
        printf("Reduced number of FFTs to %zu fit GPU, now %.2f GB needed, fits into %.2f GB free.\n", nffts, 1e-9*memreq, 1e-9*nfree);
    }
    printf("Data size : %zu complex samples for %zu channels and %zu FFTs\n", nsamples, nchan, nffts);
    CUDA_CALL( cudaMalloc( (void **)&d_xx_xy_yy, nchan*sizeof(float4) ) );
    CUDA_CALL( cudaMalloc( (void **)&d_x, 2*nsamples*sizeof(float2) ) );
    d_y = d_x + nsamples*2; //CUDA_CALL( cudaMalloc( (void **)&d_y, nsamples*sizeof(float2) ) );
    CUDA_CALL( cudaHostAlloc( (void **)&h_d, 2*nsamples*sizeof(float), cudaHostAllocDefault ) );
    printf("Pointers: d_xx_xy_yy=%p, d_x=%p, d_y=%p, h_d=%p\n", d_xx_xy_yy, d_x, d_y, h_d);
    CUDA_CALL( cudaMemset( d_xx_xy_yy, 0x00, nchan*sizeof(float4) ) );
    CUDA_CALL( cudaMemset( d_x, 0xFF, nsamples*sizeof(float2) ) );
    CUDA_CALL( cudaMemset( d_y, 0xFF, nsamples*sizeof(float2) ) );
    /////////////////////////////////////////////////////////////////////////////////

    //const int lst_threadsPerBlock[11] = { 32, 8, 16, 24, 32, 56, 64, 96, 128, 256, 512 };
    const int lst_threadsPerBlock[3] = {16, 32, 64};
    //const int lst_threadsPerBlock[1] = {2*cudaDevProp.warpSize};
    for (size_t i = 0; i < sizeof(lst_threadsPerBlock)/sizeof(int); i++) {
        printf("Speed in complex samples per second per polarization with %d threads/block:\n", lst_threadsPerBlock[i]);

        // Data generator kernel
        threadsPerBlock = lst_threadsPerBlock[i];
        numBlocks = div2ceil(nsamples,threadsPerBlock);
        CUDA_TIMING_START(tstart, 0);
        cu_accu_2pol_testdata <<< numBlocks, threadsPerBlock>>> ( (float2*)d_x, (float2*)d_y, nsamples, nchan );
        CUDA_CHECK_ERRORS("cu_accu_2pol_testdata");
        CUDA_TIMING_STOP(tstop, tstart, 0, "testdata", nsamples); // complex samples per polarization

        // Clear before cross-accumulate
        CUDA_CALL( cudaMemset( d_xx_xy_yy, 0x00, 4*nchan*sizeof(float) ) );

        // Cross-accumulate
        //numBlocks = div2ceil(32*nchan,threadsPerBlock);
        //numBlocks = div2ceil(nchan*threadsPerBlock,threadsPerBlock);
        numBlocks = div2ceil(maxphysthreads,threadsPerBlock);
        CUDA_TIMING_START(tstart, 0);
        cu_accumulate_2pol <<< numBlocks, threadsPerBlock>>> ((float2*)d_x, (float2*)d_y, (float4*)d_xx_xy_yy, nchan, nffts);
        CUDA_CHECK_ERRORS("cu_accumulate_2pol");
        CUDA_TIMING_STOP(tstop, tstart, 0, "xmac", nsamples); // complex samples per polarization

        CUDA_TIMING_START(tstart, 0);
        cu_accumulate_2pol_packed <<< numBlocks, threadsPerBlock>>> ((float4*)d_x, (float4*)d_xx_xy_yy, nchan, nffts);
        CUDA_CHECK_ERRORS("cu_accumulate_2pol_packed");
        CUDA_TIMING_STOP(tstop, tstart, 0, "xmac_pack", nsamples); // complex samples per polarization

        // Clear before cross-accumulate
        CUDA_CALL( cudaMemset( d_xx_xy_yy, 0x00, 4*nchan*sizeof(float) ) );

        // Auto only accumulate
        CUDA_TIMING_START(tstart, 0);
        cu_accumulate_2pol_autoOnly <<< numBlocks, threadsPerBlock>>> ((float2*)d_x, (float2*)d_y, (float2*)d_xx_xy_yy, nchan, nffts);
        CUDA_CHECK_ERRORS("cu_accumulate_2pol_autoOnly");
        CUDA_TIMING_STOP(tstop, tstart, 0, "amac", nsamples); // complex samples per polarization

        CUDA_TIMING_START(tstart, 0);
        cu_accumulate_2pol_autoOnly_packed <<< numBlocks, threadsPerBlock>>> ((float4*)d_x, (float2*)d_xx_xy_yy, nchan, nffts);
        CUDA_CHECK_ERRORS("cu_accumulate_2pol_autoOnly_packed");
        CUDA_TIMING_STOP(tstop, tstart, 0, "amac_packed", nsamples); // complex samples per polarization

    }

    /////////////////////////////////////////////////////////////////////////////////

    CUDA_CALL( cudaMemset( d_xx_xy_yy, 0x00, 4*nchan*sizeof(float) ) );
    cu_accumulate_2pol <<< numBlocks, threadsPerBlock>>> ((float2*)d_x, (float2*)d_y, (float4*)d_xx_xy_yy, nchan, nffts);

    // Some extra info
    printf("Layout: %zd blocks x %zd threads\n", numBlocks, threadsPerBlock);
    printf("CUDA Device #%d : %s, Compute Capability %d.%d, %d threads/block, warpsize %d, %zu threads max.\n",
        CUDA_DEVICE_NR, cudaDevProp.name, cudaDevProp.major, cudaDevProp.minor,
        cudaDevProp.maxThreadsPerBlock, cudaDevProp.warpSize, maxphysthreads
    );
    printf("nsamples = %zd (in each pol.), nffts = %zd, nchan = %zd\n", nsamples, nffts, nchan);

    CUDA_CALL( cudaMemcpy(h_d, d_x, 2*nsamples*sizeof(float), cudaMemcpyDeviceToHost) );
    printf("x  : {%.1f %.1f} {%.1f %.1f} ... ", h_d[0],h_d[1],h_d[2],h_d[3]);
    printf("{%.1f %.1f} {%.1f %.1f}\n", h_d[2*nsamples-4],h_d[2*nsamples-3],h_d[2*nsamples-2],h_d[2*nsamples-1]);

    CUDA_CALL( cudaMemcpy(h_d, d_y, 2*nsamples*sizeof(float), cudaMemcpyDeviceToHost) );
    printf("y  : {%.1f %.1f} {%.1f %.1f} ... ", h_d[0],h_d[1],h_d[2],h_d[3]);
    printf("{%.1f %.1f} {%.1f %.1f}\n", h_d[2*nsamples-4],h_d[2*nsamples-3],h_d[2*nsamples-2],h_d[2*nsamples-1]);

    CUDA_CALL( cudaMemcpy(h_d, d_xx_xy_yy, 4*nchan*sizeof(float), cudaMemcpyDeviceToHost) );
    printf("cross xy : {%.1f | %.1f %.1f | %.1f} ... ", h_d[0],h_d[1],h_d[2],h_d[3]);
    printf("{%.1f | %.1f %.1f | %.1f} ",    h_d[4*nchan/2-4],h_d[4*nchan/2-3],h_d[4*nchan/2-2],h_d[4*nchan/2-1]);
    printf("{%.1f | %.1f %.1f | %.1f} ...", h_d[4*nchan/2],h_d[4*nchan/2+1],h_d[4*nchan/2+2],h_d[4*nchan/2+3]);
    printf("{%.1f | %.1f %.1f | %.1f}\n" ,  h_d[4*nchan-4],h_d[4*nchan-3],h_d[4*nchan-2],h_d[4*nchan-1]);

    return 0;
}

#endif // BENCH

#endif // ARITHMETIC_XMAC_KERNELS_CU
