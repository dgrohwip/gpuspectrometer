/////////////////////////////////////////////////////////////////////////////////////
//
// Kernels for decoding 3-bit ALMA sample data into 32-bit float.
//
// (C) 2015 Jan Wagner
//
// ALMA memo 532 : 
//   - samples are in 3-bit Gray code, upper bit is "sign" (0=negative?),
//   - quantization is 8-level linear (?) with a dV = (Vref_hi - Vref_lo) / 2^3
//
// Custom 4 Gs/s ADC Datasheet https://safe.nrao.edu/wiki/pub/Main/ALMADigitizerTargetLevels/Datasheet_Converter.pdf
//
// Wiki https://safe.nrao.edu/wiki/bin/view/Main/ALMADigitizerTargetLevels
//    Gray   Lin  Gray   Volt
//     000 : 0    0    : -7 
//     001 : 1    1    : -5
//     011 : 2    3    : -3
//     010 : 3    2    : -1
//     110 : 4    6    : +1
//     111 : 5    7    : +3
//     101 : 6    5    : +5
//     100 : 7    4    : +7
//     => linear_LUT[8] = {-7.0f, -5.0f, -1.0f, -3.0f, +7.0f, +5.0f, +1.0f, +3.0f};
//
// How are 3-bit samples stored in natively N*8-bit segmented memory/streams/...?
//
/////////////////////////////////////////////////////////////////////////////////////
//
// The ALMA antenna data "networking" i.e. "Digital Transmission System" is using
// a SONET OC-192'ish network but with a standard-breaking high clock rate (see ALMA
// memo 420), with 96-bit parallel input to TX and 192-bit parallel output from RX.
//
// The samplers output 2 pol x 4 subband x 2 GHz. These are grouped into 8 x 48-bit
// words total where each 48-bit word (6 byte) contains 16 x 3-bit samples. Pairs
// of 48-bit words would be 96-bit like the TX input width.
// Hence a ADC sample decoder might work best on 6-byte (48-bit) granularity.
// Time and bit order of samples packed into 48-bit words are unspecified... (?)
//
// Data are framed into 160-bit frames of ideally
//   [10-bit sync word | 5-bit PSN | 1-bit index | 16-bit CRC | 128-bit payload]
// except this is rearranged at 16-bit granularity such that the sync word is split,
// and the 160-bit frame should actually look like shown in Fig. 4 of ALMA memo 420.
// The payload are 4 x 32-bit input words. How exactly the input words relate to
// sample data is apparently unspecified. According to Fig. 2, framing might involve
// a splitting the 2 x 48-bit of sample data into 3 x 32-bit of sample data plus 
// 3 x 8-bit filler, producing 3 parallel 40-bit (120-bit) data carried over three 
// 1-bit 10.00 Gbps channels each with 160-bit framing.
//
/////////////////////////////////////////////////////////////////////////////////////
// $ nvcc -DBENCH=1 -gencode=arch=compute_52,code=sm_52 decoder_3b32f_kernels.cu # test
/////////////////////////////////////////////////////////////////////////////////////

#include <stdint.h>
#include <inttypes.h>

// Single polarization (or dual but polarizations not copied into own target arrays)
__global__ void cu_decode_3bit1ch(const uint8_t* __restrict__ src, float4* __restrict__ dst, const size_t Nbytes);
__global__ void cu_decode_3bit1ch_Hann(const uint8_t* __restrict__ src, float4* __restrict__ dst, const size_t Nbytes, const size_t Lfft);
__global__ void cu_decode_3bit1ch_Hamming(const uint8_t* __restrict__ src, float4* __restrict__ dst, const size_t Nbytes, const size_t Lfft);

// Dual polarization
__global__ void cu_decode_3bit2ch_split(const uint8_t* __restrict__ src, float4* __restrict__ dstX, float4* __restrict__ dstY, const size_t Nbytes);
__global__ void cu_decode_3bit2ch_Hann_split(const uint8_t* __restrict__ src, float4* __restrict__ dstX, float4* __restrict__ dstY, const size_t Nbytes, const size_t Lfft);
__global__ void cu_decode_3bit2ch_Hamming_split(const uint8_t* __restrict__ src, float4* __restrict__ dstX, float4* __restrict__ dstY, const size_t Nbytes, const size_t Lfft);

// Single pol, DRXP data format
__global__ void cu_decode_drxp_3bit1ch(const uint8_t* __restrict__ src, float4* __restrict__ dst, const size_t Nbytes);

// Dual pol, DRXP data format
__global__ void cu_decode_drxp_3bit2ch_split(const uint8_t* __restrict__ src, float4* __restrict__ dstX, float4* __restrict__ dstY, const size_t Nbytes);

/////////////////////////////////////////////////////////////////////////////////////

__constant__ float c_lut3bit[8] = {-7.0f, -5.0f, -1.0f, -3.0f, +7.0f, +5.0f, +1.0f, +3.0f};

//#define TO_VOLTS(v) ( -7.0f + 2.0f*((float)(v)) )   // assumes [-7,-5,-3,-1,+1,+3,+5,+7] encoding
//#define TO_VOLTS(v) s_lut3bit[v]                    // arbitrary encoding, const local lookup
#define TO_VOLTS(v)   c_lut3bit[v]                    // arbitrary encoding, const global lookup

/////////////////////////////////////////////////////////////////////////////////////

/**
 * Unpack ALMA 3-bit sample data into 32-bit float.
 * Orthogonal polarizaions (if any) are unpacked into the same output array.
 *
 * The best input granularity is LCM(3,8)=24-bit which means 8 samples
 * are unpacked for every 3 bytes of input data. The kernel processes
 * one 3-byte input group. It outputs 8 x float samples.
 *
 * Assumptions:
 * - first byte contains oldest samples
 * - oldest sample is in MSBs, newest in LSBs
 *
 * Performance:
 *  ~26.4 Gs/s with local const LUT
 *  ~51.4 Gs/s with global const LUT
 */
__global__ void cu_decode_3bit1ch(const uint8_t* __restrict__ src, float4* __restrict__ dst, const size_t Nbytes)
{
    const size_t i = blockIdx.x * blockDim.x + threadIdx.x;
    const size_t rawpos = 3*i;

    if ((rawpos+2) >= Nbytes) { return; } // warp divergence...

    // Load pieces of 24-bit word (unaligned!)
    uint8_t b0 = src[rawpos+0];
    uint8_t b1 = src[rawpos+1];
    uint8_t b2 = src[rawpos+2];

    // Generate 24-bit word
    uint32_t w = (((uint32_t)b2) << 16) | (((uint32_t)b1) << 8) | ((uint32_t)b0);

    // Decode with some arithmetics and small lookup
    float4 out0 = {
        TO_VOLTS((w >> 0) & 7),
        TO_VOLTS((w >> 3) & 7),
        TO_VOLTS((w >> 6) & 7),
        TO_VOLTS((w >> 9) & 7)
    };
    float4 out1 = {
        TO_VOLTS((w >> 12) & 7),
        TO_VOLTS((w >> 15) & 7),
        TO_VOLTS((w >> 18) & 7),
        TO_VOLTS((w >> 21) & 7)
    };

    // Store
    dst[2*i+0] = out0;
    dst[2*i+1] = out1;

    return;
}

/**
 * Unpack ALMA 3-bit sample data into 32-bit float.
 * Orthogonal polarizations are unpacked into different output arrays.
 *
 * The best input granularity is LCM(3,8)=24-bit which means 8 samples
 * are unpacked for every 3 bytes of input data. The kernel processes
 * one 3-byte input group. It outputs 4 x float for X and 4 x float for Y.
 *
 * Assumptions:
 * - first byte contains oldest samples
 * - oldest sample is in MSBs, newest in LSBs
 */
__global__ void cu_decode_3bit2ch_split(const uint8_t* __restrict__ src, float4* __restrict__ dstX, float4* __restrict__ dstY, const size_t Nbytes)
{
    const size_t i = blockIdx.x * blockDim.x + threadIdx.x;
    const size_t rawpos = 3*i;

    if ((rawpos+2) >= Nbytes) { return; } // warp divergence...

    // Load pieces of 24-bit word (unaligned!)
    uint8_t b0 = src[rawpos+0];
    uint8_t b1 = src[rawpos+1];
    uint8_t b2 = src[rawpos+2];

    // Generate 24-bit word
    uint32_t w = (((uint32_t)b2) << 16) | (((uint32_t)b1) << 8) | ((uint32_t)b0);

    // Decode with some arithmetics and small lookup
    float4 X = {
        TO_VOLTS((w >>  0) & 7),
        TO_VOLTS((w >>  6) & 7),
        TO_VOLTS((w >> 12) & 7),
        TO_VOLTS((w >> 18) & 7)
    };
    float4 Y = {
        TO_VOLTS((w >>  3) & 7),
        TO_VOLTS((w >>  9) & 7),
        TO_VOLTS((w >> 15) & 7),
        TO_VOLTS((w >> 21) & 7)
    };

    // Store
    dstX[i] = X;
    dstY[i] = Y;

    return;
}

/////////////////////////////////////////////////////////////////////////////////////
// Unpacking with DRXP format
/////////////////////////////////////////////////////////////////////////////////////

/**
 * Unpack DRXP 3-bit sample data into 32-bit float.
 * Orthogonal polarizaions (if any) are unpacked into the same output array.
 *
 * DRXP format has 12 byte with 16 samples X-pol (6 byte) then 16 samples Y-pol (6 byte),
 * with the 8-bit contents of bytes sort of bit-reversed (samples shifted in from the low
 * end i.e. highest bits contain oldest sample). Here they are decoded as originating
 * from a single polarization.
 *
 * Outputs 32 x float samples.
 *
 */
__global__ void cu_decode_drxp_3bit1ch(const uint8_t* __restrict__ src, float4* __restrict__ dst, const size_t Nbytes)
{
    const size_t i = blockIdx.x * blockDim.x + threadIdx.x;
    const size_t rawpos = 12*i;

    if ((rawpos+11) >= Nbytes) { return; } // warp divergence...

    // Format of DRXP data (prototype board)
    //         bit7   6   5   4   3   2    1  0
    // byte #0   h0  m0  l0  h1  m1  l1  h2  m2  X-pol odds
    // byte #1   l2  h3  m3  l3  h4  m4  l4  h5  X-pol odds
    // byte #2   m5  l5  h6  m6  l6  h7  m7  l7  X-pol odds
    //         bit7   6   5   4   3   2    1  0
    // byte #3   h8  m8  l8  h9  m9  l9 h10 m10  X-pol odds
    // byte #4  l10 h11 m11 l11 h12 m12 l12 h13  X-pol odds
    // byte #5  m13 l13 h14 m14 l14 h15 m15 l15  X-pol odds
    //         bit7   6   5   4   3   2    1  0
    // byte #6   H0  M0  L0  H1  M1  L1  H2  M2  X-pol evens
    // byte #7   L2  H3  M3  L3  H4  M4  L4  H5  X-pol evens
    // byte #8   M5  L5  H6  M6  L6  H7  M7  L7  X-pol evens
    //         bit7   6   5   4   3   2    1  0
    // byte #9   H8  M8  L8  H9  M9  L9 H10 M10  X-pol evens
    // byte #10 L10 H11 M11 L11 H12 M12 L12 H13  X-pol evens
    // byte #11 M13 L13 H14 M14 L14 H15 M15 L15  X-pol evens

    uint8_t b0o = src[rawpos+0];
    uint8_t b1o = src[rawpos+1];
    uint8_t b2o = src[rawpos+2];
    uint8_t b3o = src[rawpos+3];
    uint8_t b4o = src[rawpos+4];
    uint8_t b5o = src[rawpos+5];
    uint8_t b0e = src[rawpos+6];
    uint8_t b1e = src[rawpos+7];
    uint8_t b2e = src[rawpos+8];
    uint8_t b3e = src[rawpos+9];
    uint8_t b4e = src[rawpos+10];
    uint8_t b5e = src[rawpos+11];

    uint32_t w0o = (((uint32_t)b0o) << 16) | (((uint32_t)b1o) << 8) | ((uint32_t)b2o);
    uint32_t w1o = (((uint32_t)b3o) << 16) | (((uint32_t)b4o) << 8) | ((uint32_t)b5o);

    uint32_t w0e = (((uint32_t)b0e) << 16) | (((uint32_t)b1e) << 8) | ((uint32_t)b2e);
    uint32_t w1e = (((uint32_t)b3e) << 16) | (((uint32_t)b4e) << 8) | ((uint32_t)b5e);

    // Decode with some arithmetics and small lookup
    float4 X0 = {
        TO_VOLTS((w0o >> 21) & 7),
        TO_VOLTS((w0e >> 21) & 7),
        TO_VOLTS((w0o >> 18) & 7),
        TO_VOLTS((w0e >> 18) & 7)
    };
    float4 X1 = {
        TO_VOLTS((w0o >> 15) & 7),
        TO_VOLTS((w0e >> 15) & 7),
        TO_VOLTS((w0o >> 12) & 7),
        TO_VOLTS((w0e >> 12) & 7)
    };
    float4 X2 = {
        TO_VOLTS((w0o >>  9) & 7),
        TO_VOLTS((w0e >>  9) & 7),
        TO_VOLTS((w0o >>  6) & 7),
        TO_VOLTS((w0e >>  6) & 7)
    };
    float4 X3 = {
        TO_VOLTS((w0o >>  3) & 7),
        TO_VOLTS((w0e >>  3) & 7),
        TO_VOLTS((w0o      ) & 7),
        TO_VOLTS((w0e      ) & 7)
    };

    float4 X4 = {
        TO_VOLTS((w1o >> 21) & 7),
        TO_VOLTS((w1e >> 21) & 7),
        TO_VOLTS((w1o >> 18) & 7),
        TO_VOLTS((w1e >> 18) & 7)
    };
    float4 X5 = {
        TO_VOLTS((w1o >> 15) & 7),
        TO_VOLTS((w1e >> 15) & 7),
        TO_VOLTS((w1o >> 12) & 7),
        TO_VOLTS((w1e >> 12) & 7)
    };
    float4 X6 = {
        TO_VOLTS((w1o >>  9) & 7),
        TO_VOLTS((w1e >>  9) & 7),
        TO_VOLTS((w1o >>  6) & 7),
        TO_VOLTS((w1e >>  6) & 7)
    };
    float4 X7 = {
        TO_VOLTS((w1o >>  3) & 7),
        TO_VOLTS((w1e >>  3) & 7),
        TO_VOLTS((w1o      ) & 7),
        TO_VOLTS((w1e      ) & 7)
    };

    // Store
    const size_t j = 8*i;
    dst[j+0] = X0;
    dst[j+1] = X1;
    dst[j+2] = X2;
    dst[j+3] = X3;
    dst[j+4] = X4;
    dst[j+5] = X5;
    dst[j+6] = X6;
    dst[j+7] = X7;

    return;
}

/**
 * Unpack DRXP 3-bit sample data into 32-bit float.
 * Orthogonal polarizations are unpacked into different output arrays.
 *
 * The best input granularity is LCM(3,8)=24-bit which means 8 samples
 * are unpacked for every 3 bytes of input data. The kernel processes
 * four 3-byte input groups. It outputs 4 x float for X and 4 x float for Y.
 *
 * DRXP format has 12 byte with 16 samples X-pol (6 byte) then 16 samples Y-pol (6 byte),
 * with the 8-bit contents of bytes sort of bit-reversed (samples shifted in from the low
 * end i.e. highest bits contain oldest sample).
 */
__global__ void cu_decode_drxp_3bit2ch_split(const uint8_t* __restrict__ src, float4* __restrict__ dstX, float4* __restrict__ dstY, const size_t Nbytes)
{
    const size_t i = blockIdx.x * blockDim.x + threadIdx.x;
    const size_t rawpos = 12*i;

    if ((rawpos+11) >= Nbytes) { return; } // warp divergence...

    // Format of DRXP data (prototype board)
    //         bit7   6   5   4   3   2    1  0
    // byte #0   h0  m0  l0  h1  m1  l1  h2  m2  X-pol
    // byte #1   l2  h3  m3  l3  h4  m4  l4  h5  X-pol
    // byte #2   m5  l5  h6  m6  l6  h7  m7  l7  X-pol
    //         bit7   6   5   4   3   2    1  0
    // byte #3   h8  m8  l8  h9  m9  l9 h10 m10  X-pol
    // byte #4  l10 h11 m11 l11 h12 m12 l12 h13  X-pol
    // byte #5  m13 l13 h14 m14 l14 h15 m15 l15  X-pol
    //         bit7   6   5   4   3   2    1  0
    // byte #6   H0  M0  L0  H1  M1  L1  H2  M2  Y-pol
    // byte #7   L2  H3  M3  L3  H4  M4  L4  H5  Y-pol
    // byte #8   M5  L5  H6  M6  L6  H7  M7  L7  Y-pol
    //         bit7   6   5   4   3   2    1  0
    // byte #9   H8  M8  L8  H9  M9  L9 H10 M10  Y-pol
    // byte #10 L10 H11 M11 L11 H12 M12 L12 H13  Y-pol
    // byte #11 M13 L13 H14 M14 L14 H15 M15 L15  Y-pol

    uint8_t b0x = src[rawpos+0];
    uint8_t b1x = src[rawpos+1];
    uint8_t b2x = src[rawpos+2];
    uint8_t b3x = src[rawpos+3];
    uint8_t b4x = src[rawpos+4];
    uint8_t b5x = src[rawpos+5];

    uint8_t b0y = src[rawpos+6];
    uint8_t b1y = src[rawpos+7];
    uint8_t b2y = src[rawpos+8];
    uint8_t b3y = src[rawpos+9];
    uint8_t b4y = src[rawpos+10];
    uint8_t b5y = src[rawpos+11];

    uint32_t w0x = (((uint32_t)b0x) << 16) | (((uint32_t)b1x) << 8) | ((uint32_t)b2x);
    uint32_t w1x = (((uint32_t)b3x) << 16) | (((uint32_t)b4x) << 8) | ((uint32_t)b5x);

    uint32_t w0y = (((uint32_t)b0y) << 16) | (((uint32_t)b1y) << 8) | ((uint32_t)b2y);
    uint32_t w1y = (((uint32_t)b3y) << 16) | (((uint32_t)b4y) << 8) | ((uint32_t)b5y);

    // Decode with some arithmetics and small lookup
    float4 X0 = {
        TO_VOLTS((w0x >> 21) & 7),
        TO_VOLTS((w0x >> 18) & 7),
        TO_VOLTS((w0x >> 15) & 7),
        TO_VOLTS((w0x >> 12) & 7)
    };
    float4 X1 = {
        TO_VOLTS((w0x >>  9) & 7),
        TO_VOLTS((w0x >>  6) & 7),
        TO_VOLTS((w0x >>  3) & 7),
        TO_VOLTS( w0x        & 7)
    };
    float4 X2 = {
        TO_VOLTS((w1x >> 21) & 7),
        TO_VOLTS((w1x >> 18) & 7),
        TO_VOLTS((w1x >> 15) & 7),
        TO_VOLTS((w1x >> 12) & 7)
    };
    float4 X3 = {
        TO_VOLTS((w1x >>  9) & 7),
        TO_VOLTS((w1x >>  6) & 7),
        TO_VOLTS((w1x >>  3) & 7),
        TO_VOLTS( w1x        & 7)
    };

    float4 Y0 = {
        TO_VOLTS((w0y >> 21) & 7),
        TO_VOLTS((w0y >> 18) & 7),
        TO_VOLTS((w0y >> 15) & 7),
        TO_VOLTS((w0y >> 12) & 7)
    };
    float4 Y1 = {
        TO_VOLTS((w0y >>  9) & 7),
        TO_VOLTS((w0y >>  6) & 7),
        TO_VOLTS((w0y >>  3) & 7),
        TO_VOLTS( w0y        & 7)
    };
    float4 Y2 = {
        TO_VOLTS((w1y >> 21) & 7),
        TO_VOLTS((w1y >> 18) & 7),
        TO_VOLTS((w1y >> 15) & 7),
        TO_VOLTS((w1y >> 12) & 7)
    };
    float4 Y3 = {
        TO_VOLTS((w1y >>  9) & 7),
        TO_VOLTS((w1y >>  6) & 7),
        TO_VOLTS((w1y >>  3) & 7),
        TO_VOLTS( w1y        & 7)
    };

    // Store
    const size_t j = 4*i;
    dstX[j+0] = X0;
    dstX[j+1] = X1;
    dstX[j+2] = X2;
    dstX[j+3] = X3;
    dstY[j+0] = Y0;
    dstY[j+1] = Y1;
    dstY[j+2] = Y2;
    dstY[j+3] = Y3;

    return;
}

/////////////////////////////////////////////////////////////////////////////////////
// Unpacking with window function applied simultaneously
/////////////////////////////////////////////////////////////////////////////////////

#ifdef M_2PI_f32
    #undef M_2PI_f32    
#endif
#ifdef m_cosf
    #undef m_cosf
#endif

#define M_2PI_f32 6.283185307179586f
#define m_cosf(x) __cosf(x)  // use cosf(x) or __cosf(x)

/**
 * Unpack ALMA 3-bit sample data into 32-bit float.
 * Outputs 8 float samples for every 3-byte input data group.
 *
 * Applies the Hann window function w[n] = 0.5*(1 - cos(2pi*n/(N-1))) for n=0..N-1
 * as part of the sample unpacking step.
 */
__global__ void cu_decode_3bit1ch_Hann(const uint8_t* __restrict__ src, float4* __restrict__ dst, const size_t Nbytes, const size_t Lfft)
{
    const size_t i = blockIdx.x * blockDim.x + threadIdx.x;
    const size_t rawpos = 3*i;

    if ((rawpos+2) >= Nbytes) { return; } // warp divergence...

    float omega = M_2PI_f32 / (Lfft - 1);
    float n_win = (8*i) % Lfft;
    float4 wf0 = {
        (1.0f - m_cosf(omega*(n_win+0))) / 2.0f,
        (1.0f - m_cosf(omega*(n_win+1))) / 2.0f,
        (1.0f - m_cosf(omega*(n_win+2))) / 2.0f,
        (1.0f - m_cosf(omega*(n_win+3))) / 2.0f
    };
    float4 wf1 = {
        (1.0f - m_cosf(omega*(n_win+4))) / 2.0f,
        (1.0f - m_cosf(omega*(n_win+5))) / 2.0f,
        (1.0f - m_cosf(omega*(n_win+6))) / 2.0f,
        (1.0f - m_cosf(omega*(n_win+7))) / 2.0f
    };

    // Load pieces of 24-bit word (unaligned!)
    uint8_t b0 = src[rawpos+0];
    uint8_t b1 = src[rawpos+1];
    uint8_t b2 = src[rawpos+2];

    // Generate 24-bit word
    uint32_t w = (((uint32_t)b2) << 16) | (((uint32_t)b1) << 8) | ((uint32_t)b0);

    // Decode with some arithmetics and small lookup
    float4 out0 = {
        TO_VOLTS((w >> 0) & 7) * wf0.x,
        TO_VOLTS((w >> 3) & 7) * wf0.y,
        TO_VOLTS((w >> 6) & 7) * wf0.z,
        TO_VOLTS((w >> 9) & 7) * wf0.w
    };
    float4 out1 = {
        TO_VOLTS((w >> 12) & 7) * wf1.x,
        TO_VOLTS((w >> 15) & 7) * wf1.y,
        TO_VOLTS((w >> 18) & 7) * wf1.z,
        TO_VOLTS((w >> 21) & 7) * wf1.w
    };

    // Store
    dst[2*i+0] = out0;
    dst[2*i+1] = out1;

    return;
}

/**
 * Unpack ALMA 3-bit sample data into 32-bit float.
 * Outputs 8 float samples for every 3-byte input data group.
 *
 * Applies the Hamming window function w[n] = 0.54 - 0.46*cos(2pi*n/(N-1))) for n=0..N-1
 * as part of the sample unpacking step.
 */
__global__ void cu_decode_3bit1ch_Hamming(const uint8_t* __restrict__ src, float4* __restrict__ dst, const size_t Nbytes, const size_t Lfft)
{
    const size_t i = blockIdx.x * blockDim.x + threadIdx.x;
    const size_t rawpos = 3*i;

    if ((rawpos+2) >= Nbytes) { return; } // warp divergence...

    float omega = M_2PI_f32 / (Lfft - 1);
    float n_win = (8*i) % Lfft;
    float4 wf0 = {
        (0.54f - 0.46f*m_cosf(omega*(n_win+0))),
        (0.54f - 0.46f*m_cosf(omega*(n_win+1))),
        (0.54f - 0.46f*m_cosf(omega*(n_win+2))),
        (0.54f - 0.46f*m_cosf(omega*(n_win+3)))
    };
    float4 wf1 = {
        (0.54f - 0.46f*m_cosf(omega*(n_win+4))),
        (0.54f - 0.46f*m_cosf(omega*(n_win+5))),
        (0.54f - 0.46f*m_cosf(omega*(n_win+6))),
        (0.54f - 0.46f*m_cosf(omega*(n_win+7)))
    };

    // Load pieces of 24-bit word (unaligned!)
    uint8_t b0 = src[rawpos+0];
    uint8_t b1 = src[rawpos+1];
    uint8_t b2 = src[rawpos+2];

    // Generate 24-bit word
    uint32_t w = (((uint32_t)b2) << 16) | (((uint32_t)b1) << 8) | ((uint32_t)b0);

    // Decode with some arithmetics and small lookup
    float4 out0 = {
        TO_VOLTS((w >> 0) & 7) * wf0.x,
        TO_VOLTS((w >> 3) & 7) * wf0.y,
        TO_VOLTS((w >> 6) & 7) * wf0.z,
        TO_VOLTS((w >> 9) & 7) * wf0.w
    };
    float4 out1 = {
        TO_VOLTS((w >> 12) & 7) * wf1.x,
        TO_VOLTS((w >> 15) & 7) * wf1.y,
        TO_VOLTS((w >> 18) & 7) * wf1.z,
        TO_VOLTS((w >> 21) & 7) * wf1.w
    };

    // Store
    dst[2*i+0] = out0;
    dst[2*i+1] = out1;

    return;
}

/**
 * Unpack ALMA 3-bit sample data into 32-bit float.
 * Orthogonal polarizations are unpacked into different output arrays.
 *
 * Applies the Hann window function w[n] = 0.5*(1 - cos(2pi*n/(N-1))) for n=0..N-1
 * as part of the sample unpacking step.
 *
 */
__global__ void cu_decode_3bit2ch_Hann_split(const uint8_t* __restrict__ src, float4* __restrict__ dstX, float4* __restrict__ dstY, const size_t Nbytes, const size_t Lfft)
{
    const size_t i = blockIdx.x * blockDim.x + threadIdx.x; // index of current 3-byte group (gives 4 X and 4 Y samples)
    const size_t rawpos = 3*i;

    if ((rawpos+2) >= Nbytes) { return; } // warp divergence...

    float omega = M_2PI_f32 / (Lfft - 1);
    float n_win = (4*i) % Lfft;
    float4 wf = {
        (1.0f - m_cosf(omega*(n_win+0))) / 2.0f,
        (1.0f - m_cosf(omega*(n_win+1))) / 2.0f,
        (1.0f - m_cosf(omega*(n_win+2))) / 2.0f,
        (1.0f - m_cosf(omega*(n_win+3))) / 2.0f
    };

    // Load pieces of 24-bit word (unaligned!)
    uint8_t b0 = src[rawpos+0];
    uint8_t b1 = src[rawpos+1];
    uint8_t b2 = src[rawpos+2];

    // Generate 24-bit word
    uint32_t w = (((uint32_t)b2) << 16) | (((uint32_t)b1) << 8) | ((uint32_t)b0);

    // Decode with some arithmetics and small lookup
    float4 X = {
        TO_VOLTS((w >>  0) & 7) * wf.x,
        TO_VOLTS((w >>  6) & 7) * wf.y,
        TO_VOLTS((w >> 12) & 7) * wf.z,
        TO_VOLTS((w >> 18) & 7) * wf.w
    };
    float4 Y = {
        TO_VOLTS((w >>  3) & 7) * wf.x,
        TO_VOLTS((w >>  9) & 7) * wf.y,
        TO_VOLTS((w >> 15) & 7) * wf.z,
        TO_VOLTS((w >> 21) & 7) * wf.w
    };

    // Store
    dstX[i] = X;
    dstY[i] = Y;

    return;
}

/**
 * Unpack ALMA 3-bit sample data into 32-bit float.
 * Orthogonal polarizations are unpacked into different output arrays.
 *
 * Applies the Hamming window function w[n] = 0.54 - 0.46*cos(2pi*n/(N-1))) for n=0..N-1
 * as part of the sample unpacking step.
 *
 */
__global__ void cu_decode_3bit2ch_Hamming_split(const uint8_t* __restrict__ src, float4* __restrict__ dstX, float4* __restrict__ dstY, const size_t Nbytes, const size_t Lfft)
{
    const size_t i = blockIdx.x * blockDim.x + threadIdx.x; // index of current 3-byte group (gives 4 X and 4 Y samples)
    const size_t rawpos = 3*i;

    if ((rawpos+2) >= Nbytes) { return; } // warp divergence...

    float omega = M_2PI_f32 / (Lfft - 1);
    float n_win = (4*i) % Lfft;
    float4 wf = {
        (0.54f - 0.46f*m_cosf(omega*(n_win+0))),
        (0.54f - 0.46f*m_cosf(omega*(n_win+1))),
        (0.54f - 0.46f*m_cosf(omega*(n_win+2))),
        (0.54f - 0.46f*m_cosf(omega*(n_win+3)))
    };

    // Load pieces of 24-bit word (unaligned!)
    uint8_t b0 = src[rawpos+0];
    uint8_t b1 = src[rawpos+1];
    uint8_t b2 = src[rawpos+2];

    // Generate 24-bit word
    uint32_t w = (((uint32_t)b2) << 16) | (((uint32_t)b1) << 8) | ((uint32_t)b0);

    // Decode with some arithmetics and small lookup
    float4 X = {
        TO_VOLTS((w >>  0) & 7) * wf.x,
        TO_VOLTS((w >>  6) & 7) * wf.y,
        TO_VOLTS((w >> 12) & 7) * wf.z,
        TO_VOLTS((w >> 18) & 7) * wf.w
    };
    float4 Y = {
        TO_VOLTS((w >>  3) & 7) * wf.x,
        TO_VOLTS((w >>  9) & 7) * wf.y,
        TO_VOLTS((w >> 15) & 7) * wf.z,
        TO_VOLTS((w >> 21) & 7) * wf.w
    };

    // Store
    dstX[i] = X;
    dstY[i] = Y;

    return;
}



/////////////////////////////////////////////////////////////////////////////////////

#ifdef BENCH // standalone compile mode for testing

#define USE_MAP_HOSTMEM      1 // 1 to map host memory and not use explicit memcpy's
#define MEASURE_MEMCPY       1 // if not USE_MAP_HOSTMEM: 1 to include h2d memCpy in timing measurement, 0 to measure just the kernel
#define USE_PINNABLE_HOSTMEM 0 // type of source buffer, 1 to use cudaHostAlloc(), 0 to use memalign() like a buffer from DRXP
#define USE_MEMCPY_TO_PINNED 1 // if USE_PINNABLE_HOSTMEM, add an extra memalign()'ed buffer like DRXP and a memcpy() into the mapped/pinned memory

#ifndef CUDA_DEVICE_NR
    #define CUDA_DEVICE_NR 0
#endif
#define CHECK_TIMING 1
#include "cuda_utils.cu"

#include <malloc.h>

int main(void)
{
    cudaDeviceProp cudaDevProp;
    cudaEvent_t tstart, tstop;
    size_t maxphysthreads;
    size_t nsamples, nrawbytes, i;
    unsigned char *d_data_uint8;
    unsigned char *h_data_uint8;
    unsigned char *drxp_ptr_uint8;
    float *d_data_f32;
    float *h_data_f32;

#if USE_MAP_HOSTMEM
    printf("Using memory from CPU-side mapped to GPU for decoder access, memcpy is implicit\n");
#else
    #if MEASURE_MEMCPY
    printf("Using memory copied from CPU-side to GPU for decoder\n");
    #else
    printf("Using memory on GPU without any copying from CPU-side\n");
    #endif
#endif

#if USE_MAP_HOSTMEM
    CUDA_CALL( cudaSetDeviceFlags(cudaDeviceMapHost) );
#endif
    CUDA_CALL( cudaSetDevice(CUDA_DEVICE_NR) );
    CUDA_CALL( cudaGetDeviceProperties(&cudaDevProp, CUDA_DEVICE_NR) );
    maxphysthreads = cudaDevProp.multiProcessorCount * cudaDevProp.maxThreadsPerMultiProcessor;

    CUDA_CALL( cudaEventCreate( &tstart ) );
    CUDA_CALL( cudaEventCreate( &tstop ) );

    // Input: 8M x 24-bit words with 3-bit samples = 64M samples = 24M raw byte
    nsamples  = (24/3) * 8000000;
    nrawbytes = (nsamples*3)/8;
    drxp_ptr_uint8 = (unsigned char*)memalign(4096, nrawbytes);
#if USE_MAP_HOSTMEM
    CUDA_CALL( cudaHostAlloc( (void **)&h_data_uint8, nrawbytes, cudaHostAllocMapped|cudaHostAllocWriteCombined) );
    CUDA_CALL( cudaHostGetDevicePointer( (void **)&d_data_uint8, h_data_uint8, 0) );
#endif
#if (!USE_MAP_HOSTMEM) && USE_PINNABLE_HOSTMEM
    CUDA_CALL( cudaHostAlloc( (void **)&h_data_uint8, nrawbytes, cudaHostAllocWriteCombined) );
    CUDA_CALL( cudaMalloc( (void **)&d_data_uint8, nrawbytes ) );
#endif
#if (!USE_MAP_HOSTMEM) && (!USE_PINNABLE_HOSTMEM)
    h_data_uint8 = (unsigned char*)memalign(4096, nrawbytes);
    CUDA_CALL( cudaMalloc( (void **)&d_data_uint8, nrawbytes ) );
    printf("Using host memory that is not pinned\n");
#endif
#if USE_MEMCPY_TO_PINNED
    printf("Using additional memcpy() from unaligned to pinned/mapped host memory\n");
#endif

    // Output: nsamples of float32
    CUDA_CALL( cudaMalloc( (void **)&d_data_f32,   nsamples*sizeof(float) ) );
    CUDA_CALL( cudaHostAlloc( (void **)&h_data_f32, nsamples*sizeof(float), cudaHostAllocDefault ) );

    // Fill input with 3-byte 0x191919 = 0b000.110.010.001.100.100.011.001 = {-7, +1, -1, -5, +7, +7, -3, -5}
    // pattern is 0x191919 0b000 110 01 = 0x19 = {-7.0, +1.0, ??})
    //CUDA_CALL( cudaMemset( d_data_uint8, 0xFF, nrawbytes ) );

    // Or fill input with 3-byte 0xFFFFFF = {+3, +3, ..., +3}
    CUDA_CALL( cudaMemset( d_data_uint8, 0xFF, nrawbytes ) );
#if !USE_MAP_HOSTMEM
    CUDA_CALL( cudaMemcpy( h_data_uint8, d_data_uint8, nrawbytes, cudaMemcpyDeviceToHost ) );
#endif

    /////////////////////////////////////////////////////////////////////////////////

    // Distribution
    size_t M = cudaDevProp.warpSize;
    size_t N = ((nsamples*3)/24);  // nr# of 24-bit words containing 3-bit samples
    dim3 numBlocks(div2ceil(N,M));
    dim3 threadsPerBlock(M);

    M = cudaDevProp.warpSize;
    N = ((nsamples*3)/(4*24)); // nr# of quad 24-bit words containing 3-bit samples from dual pol and in DRXP layout
    dim3 numBlocks_drxpFmt(div2ceil(N,M));
    dim3 threadsPerBlock_drxpFmt(M);

    float4* d_XX = (float4*)d_data_f32;
    float4* d_X = (float4*)d_data_f32;
    float4* d_Y = d_X + (nsamples/2)/4;
    size_t Lfft = 64; // fake length for presumed FFT

#define DO_MEMCPY \
    if (MEASURE_MEMCPY && !USE_MAP_HOSTMEM) { \
        if (USE_PINNABLE_HOSTMEM && USE_MEMCPY_TO_PINNED) { \
            CUDA_CALL( cudaMemcpy( h_data_uint8, drxp_ptr_uint8, nrawbytes, cudaMemcpyHostToHost ) ); \
        } \
        CUDA_CALL( cudaMemcpy( d_data_uint8, h_data_uint8, nrawbytes, cudaMemcpyHostToDevice ) ); \
    } else if (USE_MAP_HOSTMEM && USE_MEMCPY_TO_PINNED) { \
        CUDA_CALL( cudaMemcpy( h_data_uint8, drxp_ptr_uint8, nrawbytes, cudaMemcpyHostToHost ) );  \
        /* memcpy(h_data_uint8, drxp_ptr_uint8, nrawbytes); */ \
    }

    // Decoder kernel timing : 1-polarization
    CUDA_TIMING_START(tstart, 0);
    DO_MEMCPY;
    cu_decode_3bit1ch <<<numBlocks, threadsPerBlock>>> ( d_data_uint8, d_XX, nrawbytes );
    CUDA_CHECK_ERRORS("3bit1ch");
    CUDA_TIMING_STOP(tstop, tstart, 0, "3bit_1ch", nsamples);

    // Decoder kernel timing : DRXP 1-polarization
    CUDA_TIMING_START(tstart, 0);
    DO_MEMCPY;
    cu_decode_drxp_3bit1ch <<<numBlocks_drxpFmt, threadsPerBlock_drxpFmt>>> ( d_data_uint8, d_XX, nrawbytes );
    CUDA_CHECK_ERRORS("drxp3b1c");
    CUDA_TIMING_STOP(tstop, tstart, 0, "drxp_3bit_1ch", nsamples);

    // Decoder kernel timing : 1-polarization + Hann windowing
    CUDA_TIMING_START(tstart, 0);
    DO_MEMCPY;
    cu_decode_3bit1ch_Hann <<<numBlocks, threadsPerBlock>>> ( d_data_uint8, d_XX, nrawbytes, Lfft );
    CUDA_CHECK_ERRORS("3bit1ch_Hann");
    CUDA_TIMING_STOP(tstop, tstart, 0, "3bit_1ch_Hann", nsamples);

    // Decoder kernel timing : 1-polarization + Hamming windowing
    CUDA_TIMING_START(tstart, 0);
    DO_MEMCPY;
    cu_decode_3bit1ch_Hamming <<<numBlocks, threadsPerBlock>>> ( d_data_uint8, d_XX, nrawbytes, Lfft );
    CUDA_CHECK_ERRORS("3bit1ch_Hamming");
    CUDA_TIMING_STOP(tstop, tstart, 0, "3bit_1ch_Hamming", nsamples);

    // Decoder kernel timing : 2-polarization
    CUDA_TIMING_START(tstart, 0);
    DO_MEMCPY;
    cu_decode_3bit2ch_split <<<numBlocks, threadsPerBlock>>> ( d_data_uint8, d_X, d_Y, nrawbytes );
    CUDA_CHECK_ERRORS("3bit2ch");
    CUDA_TIMING_STOP(tstop, tstart, 0, "3bit_2ch", nsamples);

    // Decoder kernel timing : DRXP 2-polarization
    CUDA_TIMING_START(tstart, 0);
    DO_MEMCPY;
    cu_decode_drxp_3bit2ch_split <<<numBlocks_drxpFmt, threadsPerBlock_drxpFmt>>> ( d_data_uint8, d_X, d_Y, nrawbytes );
    CUDA_CHECK_ERRORS("drxp3b2c");
    CUDA_TIMING_STOP(tstop, tstart, 0, "drxp_3bit_2ch", nsamples);

    // Decoder kernel timing : 2-polarization + Hann windowing
    CUDA_TIMING_START(tstart, 0);
    DO_MEMCPY;
    cu_decode_3bit2ch_Hann_split <<<numBlocks, threadsPerBlock>>> ( d_data_uint8, d_X, d_Y, nrawbytes, Lfft );
    CUDA_CHECK_ERRORS("3bit2ch_Hann");
    CUDA_TIMING_STOP(tstop, tstart, 0, "3bit_2ch_Hann", nsamples);

    // Decoder kernel timing : 2-polarization + Hamming windowing
    CUDA_TIMING_START(tstart, 0);
    DO_MEMCPY;
    cu_decode_3bit2ch_Hamming_split <<<numBlocks, threadsPerBlock>>> ( d_data_uint8, d_X, d_Y, nrawbytes, Lfft );
    CUDA_CHECK_ERRORS("3bit2ch_Hamming");
    CUDA_TIMING_STOP(tstop, tstart, 0, "3bit_2ch_Hamming", nsamples);

    /////////////////////////////////////////////////////////////////////////////////

    // Some extra info
    printf("Layout: %u blocks x %u threads ideal-format 3-bit\n", numBlocks.x, threadsPerBlock.x);
    printf("Layout: %u blocks x %u threads DRXP-format 3-bit\n", numBlocks_drxpFmt.x, threadsPerBlock_drxpFmt.x);
    printf("CUDA Device #%d : %s, Compute Capability %d.%d, %d threads/block, warpsize %d, %zu threads max.\n",
        CUDA_DEVICE_NR, cudaDevProp.name, cudaDevProp.major, cudaDevProp.minor,
        cudaDevProp.maxThreadsPerBlock, cudaDevProp.warpSize, maxphysthreads
    );

    CUDA_CALL( cudaMemcpy(h_data_f32, d_data_f32, nsamples*sizeof(float), cudaMemcpyDeviceToHost) );
    for (i = 0; i < 8; i++) {
        printf("kernel out: [%2zd..%2zd] %5.3f %5.3f %5.3f %5.3f  %5.3f %5.3f %5.3f %5.3f\n",
                8*i, 8*i+7, 
                h_data_f32[8*i+0], h_data_f32[8*i+1], h_data_f32[8*i+2], h_data_f32[8*i+3],
                h_data_f32[8*i+4], h_data_f32[8*i+5], h_data_f32[8*i+6], h_data_f32[8*i+7]
        );
    }

    return 0;
}

#endif

#undef TO_VOLTS
