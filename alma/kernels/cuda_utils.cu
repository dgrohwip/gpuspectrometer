/////////////////////////////////////////////////////////////////////////////////////
//
// Some common helper functions for calls to CUDA
//
// (C) 2015 Jan Wagner, based on examples from CUDA
//
/////////////////////////////////////////////////////////////////////////////////////

#ifndef CUDA_UTILS_CU
#define CUDA_UTILS_CU

#include <stdlib.h>
#include <stdio.h>
#include <cufft.h>
#include <cufftXt.h>

/////////////////////////////////////////////////////////////////////////////////////
// Integer divide with round-up to ceil
/////////////////////////////////////////////////////////////////////////////////////

// Example: divisions 3/4 and 4/4 produce div2ceil(3,4) == div2ceil(4,4) == 1
#ifndef div2ceil
    #define div2ceil(x,y) ((x+y-1)/y)
#endif

// Generic helper
#ifndef m_min
    #define m_min(x,y) ((x < y) ? x : y)   // note: don't use it with function calls...
#endif

/////////////////////////////////////////////////////////////////////////////////////
// Vector element wise operators
/////////////////////////////////////////////////////////////////////////////////////

__device__ __host__ float4  operator+( const float4 lhs, const float4 rhs)
{
    float4 res = { lhs.x + rhs.x , lhs.y + rhs.y , lhs.z + rhs.z , lhs.w + rhs.w };
    return res;
}

__device__ __host__ float4  operator-( const float4 lhs, const float4 rhs)
{
    float4 res = { lhs.x - rhs.x , lhs.y - rhs.y , lhs.z - rhs.z , lhs.w - rhs.w };
    return res;
}

__device__ __host__ float4  operator*( const float4 lhs, const float4 rhs)
{
    float4 res = { lhs.x * rhs.x , lhs.y * rhs.y , lhs.z * rhs.z , lhs.w * rhs.w };
    return res;
}

/////////////////////////////////////////////////////////////////////////////////////
// CUFFT call wrapper and error printout
/////////////////////////////////////////////////////////////////////////////////////

// List of CUFFT errors, copied from http://stackoverflow.com/questions/16267149/cufft-error-handling
static const char *cufftGetErrorStr(cufftResult error)
{
    switch (error)
    {
        case CUFFT_SUCCESS:
            return "CUFFT_SUCCESS";

        case CUFFT_INVALID_PLAN:
            return "CUFFT_INVALID_PLAN";

        case CUFFT_ALLOC_FAILED:
            return "CUFFT_ALLOC_FAILED";

        case CUFFT_INVALID_TYPE:
            return "CUFFT_INVALID_TYPE";

        case CUFFT_INVALID_VALUE:
            return "CUFFT_INVALID_VALUE";

        case CUFFT_INTERNAL_ERROR:
            return "CUFFT_INTERNAL_ERROR";

        case CUFFT_EXEC_FAILED:
            return "CUFFT_EXEC_FAILED";

        case CUFFT_SETUP_FAILED:
            return "CUFFT_SETUP_FAILED";

        case CUFFT_INVALID_SIZE:
            return "CUFFT_INVALID_SIZE";

        case CUFFT_UNALIGNED_DATA:
            return "CUFFT_UNALIGNED_DATA";

        case CUFFT_LICENSE_ERROR:
            return "CUFFT_LICENSE_ERROR : likely a cuFFT Callback error, need valid license file!";

        default:
            return "CUFFT_<undecoded error>";
    }

    //return "<unknown>";
}

#define CUFFT_CALL(x) m_CUFFT_CALL(x,__FILE__,__LINE__)
void m_CUFFT_CALL(cufftResult_t iRet, const char* pcFile, const int iLine)
{
        if (iRet != CUFFT_SUCCESS) {
                (void) fprintf(stderr, "CuFFT ERROR: File <%s>, Line %d: %s (%d)\n",
                               pcFile, iLine, cufftGetErrorStr(iRet), iRet);
                cudaDeviceReset();
                exit(EXIT_FAILURE);
        }
        return;
}

/////////////////////////////////////////////////////////////////////////////////////
// CUDA call wrapper and error printout
/////////////////////////////////////////////////////////////////////////////////////

// Direct wrapper of CUDA API calls (except kernel invocation),
// based on checkCudaErrors() from CUDA Examples helper_cuda.h
void m_CUDA_CALL(cudaError_t iRet, const char* pcFile, const int iLine)
{
        if (iRet != cudaSuccess) {
                (void) fprintf(stderr, "ERROR: File <%s>, Line %d: %s\n",
                               pcFile, iLine, cudaGetErrorString(iRet));
                cudaDeviceReset();
                exit(EXIT_FAILURE);
        }
        return;
}
#define CUDA_CALL(x) m_CUDA_CALL(x,__FILE__,__LINE__)

// Check for errors after a kernel invocation
void CUDA_CHECK_ERRORS(const char* kernel_name)
{
       cudaError_t e = cudaGetLastError();
       if (e == cudaSuccess) {
              // fprintf(stderr, "  %s<<<x,y>>>: OK\n", kernel_name);
       } else {
              fprintf(stderr, "  %s<<<x,y>>>: Error: %s\n", kernel_name, cudaGetErrorString(e));
       }
}

// Check for errors at any time, does not reset error state back to cudaSuccess.
cudaError_t CUDA_PEEK_ERRORS(void)
{
       cudaError_t e = cudaPeekAtLastError();
       if (e != cudaSuccess) {
              fprintf(stderr, "  CUDA Error: %s: %s\n", cudaGetErrorName(e), cudaGetErrorString(e));
       }
       return e;
}

// Print out memory status
inline void CUDA_PRINT_MEMORY_INFO(const char* msg)
{
      size_t free, total;
      int device;
      CUDA_CALL(cudaGetDevice(&device));
      CUDA_CALL(cudaMemGetInfo(&free,&total));
      fprintf(stderr, "CUDA device %d memory: %s: %.2f GB free out of %.2f GB total\n",
              device,msg,free*(1/1073741824.0),total*(1/1073741824.0)
      );
}

/////////////////////////////////////////////////////////////////////////////////////
// CUDA kernel timing helpers
/////////////////////////////////////////////////////////////////////////////////////

#define CUDA_TIMING_START(event,stream) \
   do { if (CHECK_TIMING) CUDA_CALL( cudaEventRecord(event, stream) ); } while(0)

#define CUDA_TIMING_STOP_ACC(stop,start,total_time,stream,msg,Nsamples) \
   do { if (CHECK_TIMING) { \
            float dt_msec; \
            CUDA_CALL( cudaEventRecord(stop, stream) ); \
            CUDA_CALL( cudaEventSynchronize(stop) ); \
            CUDA_CALL( cudaEventElapsedTime(&dt_msec, start, stop) ); \
            fprintf(stderr, "  %-18s : %7.5f s : %7.3f Ms/s\n", msg, 1e-3*dt_msec, \
                1e-6*Nsamples/(1e-3*dt_msec)); \
            total_time += dt_msec; \
        } \
   } while(0)

#define CUDA_TIMING_STOP_ACC_M(stop,start,total_time,stream,mtx,msg,Nsamples) \
   do { if (CHECK_TIMING) { \
            float dt_msec; \
            CUDA_CALL( cudaEventRecord(stop, stream) ); \
            mtx.unlock(); \
            CUDA_CALL( cudaEventSynchronize(stop) ); \
            CUDA_CALL( cudaEventElapsedTime(&dt_msec, start, stop) ); \
            fprintf(stderr, "  %-18s : %7.5f s : %7.3f Ms/s\n", msg, 1e-3*dt_msec, \
                1e-6*Nsamples/(1e-3*dt_msec)); \
            total_time += dt_msec; \
        } \
   } while(0)

#define CUDA_TIMING_STOP(stop,start,stream,msg,Nsamples) \
   do { if (CHECK_TIMING) { \
            float dt_msec; \
            CUDA_CALL( cudaEventRecord(stop, stream) ); \
            CUDA_CALL( cudaEventSynchronize(stop) ); \
            CUDA_CALL( cudaEventElapsedTime(&dt_msec, start, stop) ); \
            fprintf(stderr, "  %-18s : %7.5f s : %7.3f Ms/s\n", msg, 1e-3*dt_msec, \
                1e-6*Nsamples/(1e-3*dt_msec)); \
        } \
   } while(0)

#define CUDA_TIMING_STOP_M(stop,start,stream,mtx,msg,Nsamples) \
   do { if (CHECK_TIMING) { \
            float dt_msec; \
            CUDA_CALL( cudaEventRecord(stop, stream) ); \
            mtx.unlock(); \
            CUDA_CALL( cudaEventSynchronize(stop) ); \
            CUDA_CALL( cudaEventElapsedTime(&dt_msec, start, stop) ); \
            fprintf(stderr, "  %-18s : %7.5f s : %7.3f Ms/s\n", msg, 1e-3*dt_msec, \
                1e-6*Nsamples/(1e-3*dt_msec)); \
        } \
   } while(0)


class cuScopeTiming {
    public:
        cuScopeTiming(cudaStream_t stream=0) : m_stream(stream) {
            CUDA_CALL( cudaEventCreate(&m_tstart) );
            CUDA_CALL( cudaEventCreate(&m_tstop) );
            CUDA_CALL( cudaEventRecord(m_tstart, stream) );
        }
        ~cuScopeTiming() {
            float dt_msec;
            CUDA_CALL( cudaEventRecord(m_tstop, m_stream) );
            CUDA_CALL( cudaEventSynchronize(m_tstop) );
            CUDA_CALL( cudaEventElapsedTime(&dt_msec, m_tstart, m_tstop) );
            fprintf(stderr, "CUDA dT = %7.5f sec", 1e-3*dt_msec);
            CUDA_CALL( cudaEventDestroy(m_tstart) );
            CUDA_CALL( cudaEventDestroy(m_tstop) );
        }
    private:
        cudaEvent_t m_tstart, m_tstop;
        cudaStream_t m_stream;
};

#endif // CUDA_UTILS_CU
