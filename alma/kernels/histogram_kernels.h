/////////////////////////////////////////////////////////////////////////////////////
//
// Kernels for histogramming 2-bit or 3-bit data with 1 or 2 channels
//
// (C) 2015 Jan Wagner
//
/////////////////////////////////////////////////////////////////////////////////////

#ifndef HISTOGRAM_KERNELS_H
#define HISTOGRAM_KERNELS_H

#include <stdint.h>

// Histogram bin data type:
typedef unsigned int hist_t;
// Note 1: 32-bit 'unsigned int' is much faster on Maxwell than 64-bit 'unsigned long long int',
// Note 2: maximum 32-bit unsigned int is 2^32-1 = 4,294,967,295 ~= 4.2 gigasamples before any overflow risk

#define HIST_MAX_LEVS8        256 // 8-bit data
#define HIST_MAX_LEVS24  16777216 // 24-bit data

/////////////////////////////////////////////////////////////////////////////////////

//static const unsigned int PARTIAL_HISTOGRAM256_COUNT = 240;
#define PARTIAL_HISTOGRAM256_COUNT 240
#define WARP_COUNT 6 //Warps ==subhistograms per threadblock
#define HISTOGRAM256_BIN_COUNT 256
#define HISTOGRAM256_THREADBLOCK_SIZE (WARP_COUNT * WARP_SIZE) //Threadblock size
#define HISTOGRAM256_THREADBLOCK_MEMORY (WARP_COUNT * HISTOGRAM256_BIN_COUNT) //Shared memory per threadblock

/////////////////////////////////////////////////////////////////////////////////////

/** Histogramming wrapper that uses the most efficient CPU implementation for the given number of sample bits and channels */
void cpu_histogram(int nbits, int nch, bool drxpFormat, uint8_t* raw, size_t Nbytes, hist_t* hist_ch1, hist_t* hist_ch2=NULL, hist_t* hist_ch3=NULL, hist_t* hist_ch4=NULL);

/** The CUDA implementation for a 8-bit histogram on GPU; this CPU code invokes internally certain GPU kernels. */
void gpu_histogram256(void *d_Data, size_t byteCount, hist_t *d_Histogram);

/** Transform function to convert an 8-bit sample histogram (gpu_histogram256()) into a 2-bit 1-channel sample histogram */
void cpu_histogramtransform_8bit_to_2bit_1ch(hist_t* hist8bit, hist_t* hist);

/** Transform function to convert an 8-bit sample histogram (gpu_histogram256()) into a 2-bit 2-channel sample histogram */
void cpu_histogramtransform_8bit_to_2bit_2ch(hist_t* hist8bit, hist_t* hist_ch1, hist_t* hist_ch2);

/** Transform function to convert an 8-bit sample histogram (gpu_histogram256()) into a 2-bit 4-channel sample histogram */
void cpu_histogramtransform_8bit_to_2bit_4ch(hist_t* hist8bit, hist_t* hist_ch1, hist_t* hist_ch2, hist_t* hist_ch3, hist_t* hist_ch4);

/** CUDA kernel for histogram of 3-bit 1-channel data (now 108 Gs/s/1channel; tough to optimize the speed any further?) */
//__global__ void cu_histogram_3bit_1ch(const uint8_t* __restrict__ src, const size_t Nbyte, hist_t* __restrict__ hist);

/** CUDA kernel for histogram of 3-bit 2-channel data (now 108 G/s/2channel; tough to optimize the speed any further?) */
//__global__ void cu_histogram_3bit_2ch(const uint8_t* __restrict__ src, const size_t Nbyte, hist_t* __restrict__ hist_ch1, hist_t* __restrict__ hist_ch2);

/////////////////////////////////////////////////////////////////////////////////////

#endif // HISTOGRAM_KERNELS_H

