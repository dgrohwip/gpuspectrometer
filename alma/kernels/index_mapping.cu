/////////////////////////////////////////////////////////////////////////////////////
//
// GPU device functions to re-map post-FFT indexing
//
// reindex_skip_Nyquist : take bin index that assumes no Nyquist was produced by FFT,
//                        return memory index that hops over Nyquist bin actually
//                        produced by FFT
//
// (C) 2018 Jan Wagner
//
/////////////////////////////////////////////////////////////////////////////////////

#ifndef INDEX_MAPPING_KERNELS_CU
#define INDEX_MAPPING_KERNELS_CU

#include <stdint.h>
#include <inttypes.h>

/** Map linear virtual index to underlying memory index, avoiding the Nyquist bin */
__host__ __device__ size_t reindex_skip_Nyquist(const size_t nch_with_Nyquist, const size_t nch_without_Nyquist, const size_t indx_without_Nyquist)
{
    size_t bin = indx_without_Nyquist % nch_without_Nyquist;
    size_t fft_nr = indx_without_Nyquist / nch_without_Nyquist;
    size_t mem_index_withNyquist = fft_nr*nch_with_Nyquist + bin;
    return mem_index_withNyquist;
}

/** Map linear virtual index to underlying memory index, avoiding the Nyquist bin */
__host__ __device__ size_t reindex_skip_Nyquist(const size_t nch_without_Nyquist, const size_t indx_without_Nyquist)
{
    const size_t nch_with_Nyquist = nch_without_Nyquist + 1;
    size_t bin = indx_without_Nyquist % nch_without_Nyquist;
    size_t fft_nr = indx_without_Nyquist / nch_without_Nyquist;
    size_t mem_index_withNyquist = fft_nr*nch_with_Nyquist + bin;
    return mem_index_withNyquist;
}

#endif // INDEX_MAPPING_KERNELS_CU
