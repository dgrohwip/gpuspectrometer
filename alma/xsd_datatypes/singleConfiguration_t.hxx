/**
 * \class singleConfiguration_t
 *
 * Container for XSD SingleConfigurationType
 */
#ifndef SINGLECONFIGURATION_T_HXX
#define SINGLECONFIGURATION_T_HXX

#include "xsd_datatypes/subArray_t.hxx"
#include "xsd_datatypes/timeSpec_t.hxx"
#include "xsd_datatypes/basebandConfig_t.hxx"
#include "xsd_ptree_iface.hxx"

namespace XSD
{

class singleConfiguration_t : public XSDPtreeInterface
{
    public:
        subArray_t subArray;
        timeSpec_t timeSpec;
        basebandConfig_t basebandConfig;

    public:
        singleConfiguration_t() : subArray(), timeSpec(), basebandConfig() { }

    public:
        bool validate() const {
            bool valid = true;
            valid &= subArray.validate();
            valid &= timeSpec.validate();
            valid &= basebandConfig.validate();
            return valid;
        }
        bool get(const boost::property_tree::ptree&);
        bool put(boost::property_tree::ptree&) const;

};

}

#endif
