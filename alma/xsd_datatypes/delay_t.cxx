#include "delay_t.hxx"

namespace XSD {

template<typename T> void delay_t::parse_coefficients(std::string raw, T* out, const int Nmax, const char separator /* default: ' ' */)
{
    std::istringstream iss(raw);
    std::string coeff;
    int n = 0;
    while (std::getline(iss, coeff, separator) && (n < Nmax)) {
        out[n] = (T)atof(coeff.c_str());
        n++;
    }
}

template<typename T> std::string delay_t::generate_coefficients(const T* in, const int Nmax, const char* separator) const
{
    std::ostringstream oss;
    for (int n=0; n<Nmax-1; n++) {
        oss << in[n] << separator;
    }
    if (Nmax > 0) {
        oss << in[Nmax-1];
    }
    return oss.str();
}

bool delay_t::get(const boost::property_tree::ptree& in)
{
    const int Nmax = sizeof(delayCoefficients)/sizeof(delayCoefficients[0]);
    for (int n=0; n<Nmax; n++) {
        delayCoefficients[n] = 0;
    }

    startTime = in.get("startTime", "");
    stopTime = in.get("stopTime", "");
    parse_coefficients<double>(in.get("delayCoefficients","0.0"), delayCoefficients, Nmax);
    instrumentalDelayX = in.get("instrumentalDelayX", 0.0);
    instrumentalDelayY = in.get("instrumentalDelayY", 0.0);

    return validate();
}

bool delay_t::put(boost::property_tree::ptree& out) const
{
    const int Nmax = sizeof(delayCoefficients)/sizeof(delayCoefficients[0]);
    std::string coeffs = generate_coefficients<double>(delayCoefficients, Nmax);

    out.put("startTime", startTime);
    out.put("stopTime", stopTime);
    out.put("delayCoefficients", coeffs);
    out.put("instrumentalDelayX", instrumentalDelayX);
    out.put("instrumentalDelayY", instrumentalDelayY);

    return validate();
}

}
