/**
 * \class fpgaSEUInfo_t
 *
 * Container for SEU Status and Error Count and Elapsed time
 */
#ifndef FPGASEUINFO_T_HXX
#define FPGASEUINFO_T_HXX

#include <stdint.h>
//#include <cstdint> // c++x11

#include "xsd_ptree_iface.hxx"

namespace XSD {

class fpgaSEUInfo_t : public XSDPtreeInterface
{
    public:
        uint32_t status;
        uint32_t error;
        uint32_t corErrCnt;
        uint32_t uncorErrCnt;
        uint32_t elapsedObs;
        uint32_t elapsedTot;

    public:
        fpgaSEUInfo_t() { }

    public:
        bool validate() const { return true; }
        bool get(const boost::property_tree::ptree&);
        bool put(boost::property_tree::ptree&) const;

};

}

#endif
