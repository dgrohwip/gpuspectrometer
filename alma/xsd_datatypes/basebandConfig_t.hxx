/**
 * \class basebandConfig_t
 *
 * Container for XSD BasebandConfigType
 */
#ifndef BASEBANDCONFIG_T_HXX
#define BASEBANDCONFIG_T_HXX

#include "xsd_ptree_iface.hxx"
#include "spectralWindow_t.hxx"

#include <vector>

namespace XSD {
 
/*** Observation baseband and spectral window specifications, see ICD */
class basebandConfig_t : public XSDPtreeInterface
{
    public:
        // double loOffset_Hz; // TODO: unclear in 19aug2019 schema: sidebandSuppression = string, can be "None"|"LoOffset"|"PhaseSwitching" but has no float value
        std::string sidebandSuppression;
        std::vector<spectralWindow_t> spectralWindow;

    public:
        basebandConfig_t() : sidebandSuppression("None") {
            spectralWindow.clear();
        }

    public:
        bool validate() const {
            bool valid = true;
            for (std::vector<spectralWindow_t>::const_iterator it=spectralWindow.begin(); it != spectralWindow.end(); it++) {
                valid &= it->validate();
            }
            valid &= (
                (sidebandSuppression=="None")
                || (sidebandSuppression=="LoOffset")
                || (sidebandSuppression=="PhaseSwitching")
            );
            return valid;
        }
        bool get(const boost::property_tree::ptree&);
        bool put(boost::property_tree::ptree&) const;

};

}

#endif

