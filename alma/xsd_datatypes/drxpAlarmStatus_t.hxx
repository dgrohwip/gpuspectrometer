/**
 * \class drxpAlarmStatus_t
 *
 * Container for XSD DrxpAlarmStatusType
 */
#ifndef DRXPALARMSTATUS_T_HXX
#define DRXPALARMSTATUS_T_HXX

#include "xsd_ptree_iface.hxx"
#include "drxpBitAlarmStatus_t.hxx"

namespace XSD {

class drxpAlarmStatus_t : public XSDPtreeInterface
{
    public:
        drxpBitAlarmStatus_t ch1BitD;
        drxpBitAlarmStatus_t ch2BitC;
        drxpBitAlarmStatus_t ch3BitB;

    public:
        drxpAlarmStatus_t() : ch1BitD(), ch2BitC(), ch3BitB() { }

    public:
        bool validate() const { return true; }
        bool get(const boost::property_tree::ptree&);
        bool put(boost::property_tree::ptree&) const;

};

}

#endif
