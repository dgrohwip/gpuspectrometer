/**
 * \class subArray_t
 *
 * Container for XSD SubArrayType and to-string of CorrelationModeType enum (INTERFEROMETRY, SINGLE_DISH)
 */
#ifndef SUBARRAY_T_HXX
#define SUBARRAY_T_HXX

#include "xsd_enums.hxx"
#include "xsd_ptree_iface.hxx"

#include <string>

namespace XSD  {

class subArray_t : public XSDPtreeInterface
{
    public:
        std::string name;
        correlationMode_t corrMode;

    public:
        subArray_t() : name("Array0001"), corrMode(SINGLE_DISH) { }

        subArray_t(const std::string& saname, correlationMode_t corrmode) : name(saname), corrMode(corrmode) { }

        subArray_t(const std::string& saname, int corrmode) : name(saname), corrMode((correlationMode_t)corrmode) { }

        bool operator==(const subArray_t& rhs) const {
            return (name == rhs.name) && (corrMode == rhs.corrMode);
        }

        bool operator<(const subArray_t& rhs) const {
            return name < rhs.name;
        }

        bool operator>(const subArray_t& rhs) const {
            return name > rhs.name;
        }

        std::string getCorrMode() const {
            if (corrMode == INTERFEROMETRY) {
                return std::string("INTERFEROMETRY");
            } else {
                return std::string("SINGLE_DISH");
            }
        }

        correlationMode_t setCorrMode(const std::string& corrModeStr) {
            if (corrModeStr == "INTERFEROMETRY") {
                corrMode = INTERFEROMETRY;
            } else {
                corrMode = SINGLE_DISH;
            }
            return corrMode;
        }

    public:
        bool validate() const {
            bool valid = true;
            valid &= name.length() >= 1;
            valid &= name.length() <= 128;
            return valid;
        }
        bool get(const boost::property_tree::ptree&);
        bool put(boost::property_tree::ptree&) const;

};

}

#endif
