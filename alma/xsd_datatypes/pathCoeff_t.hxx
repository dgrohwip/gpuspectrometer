/**
 * \class pathCoeff_t
 *
 * Container for XSD PathCoeffType
 */
#ifndef PATHCOEFF_T_HXX
#define PATHCOEFF_T_HXX

#include "inputCoeff_t.hxx"
#include "xsd_ptree_iface.hxx"

#include <vector>

namespace XSD
{

class pathCoeff_t : public XSDPtreeInterface
{
    public:
        unsigned int index;
        std::vector<inputCoeff_t> inputCoeff;

    public:
        pathCoeff_t() : index(0) {
            inputCoeff.clear();
        }

    public:
        bool validate() const {
            bool valid = true;
            valid &= (inputCoeff.size() >= 1);
            for (std::vector<inputCoeff_t>::const_iterator it = inputCoeff.begin(); it != inputCoeff.end(); it++) {
                valid &= it->validate();
            }
            return valid;
        }
        bool get(const boost::property_tree::ptree&);
        bool put(boost::property_tree::ptree&) const;

};

}

#endif
