/**
 * \class drxpBitSyncLockStatus_t
 *
 * Container for XSD DrxpBitSyncLockStatusType
 */
#ifndef DRXPBITSYNCLOCKSTATUS_T_HXX
#define DRXPBITSYNCLOCKSTATUS_T_HXX

#include "xsd_ptree_iface.hxx"

namespace XSD {

class drxpBitSyncLockStatus_t : public XSDPtreeInterface
{
    public:
        bool syncUnlock;
        int syncLockOffset;

    public:
        drxpBitSyncLockStatus_t() : syncUnlock(false), syncLockOffset(0) { }

    public:
        bool validate() const { return true; }
        bool get(const boost::property_tree::ptree&);
        bool put(boost::property_tree::ptree&) const;

};

}

#endif
