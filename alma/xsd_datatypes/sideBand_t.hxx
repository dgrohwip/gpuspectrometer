/**
 * \class sideBand_t
 *
 * Container for XSD sideBand enum (None, Lsb, Usb)
 */
#ifndef SIDEBAND_T_HXX
#define SIDEBAND_T_HXX

#include "xsd_enums.hxx"
#include "xsd_ptree_iface.hxx"

#include <string>

namespace XSD {

class sideBand_t : public XSDPtreeInterface
{
    public:
        sideBandType_t sideband;

    public:
        sideBand_t() : sideband(SIDEBAND_None) { }

        std::string str() {
            if (sideband == SIDEBAND_LSB) {
                return std::string("Lsb");
            } else if (sideband == SIDEBAND_USB) {
                return std::string("Usb");
            } else {
                return std::string("None");
            }
        }

        sideBandType_t operator=(sideBandType_t sb) {
            sideband = sb;
            return sideband;
        }

        bool operator==(const sideBandType_t& rhs) const {
            return sideband==rhs;
        }

        bool operator!=(const sideBandType_t& rhs) const {
            return sideband!=rhs;
        }

    public:
        bool validate() const {
            bool valid = true;
            valid &= (sideband == SIDEBAND_None) || (sideband == SIDEBAND_LSB) || (sideband == SIDEBAND_USB);
            return valid;
        }
        bool get(const boost::property_tree::ptree&);
        bool put(boost::property_tree::ptree&) const;

};

}

#endif
