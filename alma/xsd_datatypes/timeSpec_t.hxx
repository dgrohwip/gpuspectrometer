/**
 * \class timeSpec_t
 *
 * Container for XSD TimeSpecType
 */
#ifndef TIMESPEC_T_HXX
#define TIMESPEC_T_HXX

#include "switching_t.hxx"
#include "xsd_ptree_iface.hxx"

#include <vector>

namespace XSD {

class timeSpec_t : public XSDPtreeInterface
{
    public:
        std::vector<switching_t> switching;
        double integrationDuration; // sec
        double channelAverageDuration; // sec

    public:
        timeSpec_t() {
            switching.clear();
            integrationDuration = 0;
            channelAverageDuration = 0;
        }

    public:
        bool validate() const {
            bool valid = true;
            valid &= (integrationDuration >= 0);
            valid &= (channelAverageDuration >= 0);
            for (std::vector<switching_t>::const_iterator it=switching.begin(); it != switching.end(); it++) {
                valid &= it->validate();
            }
            return valid;
        }
        bool get(const boost::property_tree::ptree&);
        bool put(boost::property_tree::ptree&) const;

};

}

#endif
