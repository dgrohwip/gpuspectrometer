/**
 * \class inputCoeff_t
 *
 * Container for XSD InputCoeffType
 */
#ifndef INPUTCOEFF_T_HXX
#define INPUTCOEFF_T_HXX

#include "chanCoeff_t.hxx"
#include "xsd_ptree_iface.hxx"

#include <vector>

namespace XSD
{

class inputCoeff_t : public XSDPtreeInterface
{
    public:
        unsigned int index;
        std::vector<chanCoeff_t> chanCoeff;

    public:
        inputCoeff_t() : index(0) {
            chanCoeff.clear();
        }

    public:
        bool validate() const {
            bool valid = true;
            valid &= (chanCoeff.size() >= 1);
            for (std::vector<chanCoeff_t>::const_iterator it = chanCoeff.begin(); it != chanCoeff.end(); it++) {
                valid &= it->validate();
            }
            return valid;
        }
        bool get(const boost::property_tree::ptree&);
        bool put(boost::property_tree::ptree&) const;

};

}

#endif
