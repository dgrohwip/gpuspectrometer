#include "inputCoeff_t.hxx"

namespace XSD {

bool inputCoeff_t::get(const boost::property_tree::ptree& in)
{
    return validate();
}

bool inputCoeff_t::put(boost::property_tree::ptree& out) const
{
    return validate();
}

}
