#include "basebandConfig_t.hxx"

namespace XSD {

bool basebandConfig_t::get(const boost::property_tree::ptree& in)
{
    return validate();
}

bool basebandConfig_t::put(boost::property_tree::ptree& out) const
{
    return validate();
}

}
