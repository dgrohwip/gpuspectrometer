/**
 * \class XSDPtreeInterface
 *
 * Interface for converting XSD-declared data types to/from a BOOST PropertyTree (siginficantly
 * more lightweight than Xerces-C & XML / xmlrpc).
 *
 */
#ifndef XSD_PTREE_IFACE
#define XSD_PTREE_IFACE

#include <boost/property_tree/ptree.hpp>

namespace XSD {

class XSDPtreeInterface
{
    public:

        XSDPtreeInterface() { }

        /// Store XSD datatype content into the referenced Ptree
        virtual bool put(boost::property_tree::ptree& out) const = 0;

        /// Retreive Ptree content and store into respective fields of XSD datatype
        virtual bool get(const boost::property_tree::ptree& in) = 0;

        /// Validate XSD datatype content
        virtual bool validate() const = 0;

};

}

#endif
