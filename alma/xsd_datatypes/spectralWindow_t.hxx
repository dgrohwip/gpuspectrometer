/**
 * \class spectralWindow_t
 *
 * Container for XSD SpectralWindowType
 */
#ifndef SPECTRALWINDOW_T_HXX
#define SPECTRALWINDOW_T_HXX

#include "channelAverageRegion_t.hxx"
#include "xsd_enums.hxx"
#include "xsd_ptree_iface.hxx"

#include <vector>
#include <string>

namespace XSD {

class spectralWindow_t : public XSDPtreeInterface
{
    public:
        double centerFrequency; /// 2000..4000 MHz
        unsigned int spectralAveragingFactor; /// 1, 2, 4, 8, ..., 4096
        std::string name;
        double effectiveBandwidth;
        unsigned int effectiveNumberOfChannels; // "less than 8192 for now" (ICD, Section 7.4.1)
        bool quantizationCorrection;
        std::vector<channelAverageRegion_t> channelAverageRegion;
        windowFunction_t windowFunction;
        double windowOverlap;
        windowFunction_t simulatedWindowFunction;
        polnProduct_t polnProducts;

    public:
        bool windowFunction_chosen;
        bool simulatedWindowFunction_chosen;

    public:
        spectralWindow_t() {
            centerFrequency = 0;
            spectralAveragingFactor = 1;
            name = "uninitialized";
            effectiveBandwidth = 0;
            effectiveNumberOfChannels = 0;
            quantizationCorrection = 0;
            channelAverageRegion.clear();
            windowFunction = UNIFORM;
            windowOverlap = 0;
            simulatedWindowFunction = UNIFORM;
            polnProducts = POLN_UNDEFINED;
            windowFunction_chosen = false;
            simulatedWindowFunction_chosen = false;
        }

    public:
        bool validate() const {
            bool valid = true;
            valid &= (2000.0 <= centerFrequency) && (centerFrequency <= 40000.0);
            valid &= (spectralAveragingFactor > 0);
            valid &= (effectiveBandwidth > 0);
            // valid &= (effectiveNumberOfChannels <= 8192);
            valid &= (0 <= windowOverlap) && (windowOverlap <= 0.95);
            for (std::vector<channelAverageRegion_t>::const_iterator it=channelAverageRegion.begin(); it != channelAverageRegion.end(); it++) {
                valid &= it->validate();
            }
            return valid;
        }
        bool get(const boost::property_tree::ptree&);
        bool put(boost::property_tree::ptree&) const;

};

}

#endif
