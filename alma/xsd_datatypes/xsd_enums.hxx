#ifndef XSD_ENUMS_HXX
#define XSD_ENUMS_HXX

namespace XSD {

enum polnProduct_t { POLN_UNDEFINED=0, POLN_XX_YY_XY, POLN_XX_YY, POLN_XX, POLN_YY }; /// Types of polarization products desired as final spectral output
enum windowFunction_t { UNIFORM=0, HANNING, HAMMING, BARTLETT, BLACKMAN, BLACKMAN_HARRIS, WELCH }; /// Window function types
enum sideBandType_t { SIDEBAND_None=0, SIDEBAND_LSB, SIDEBAND_USB }; /// Sideband types, see also class sideBand_t

enum drxpTestMode_Type_t {
    DRXP_TestMode_No=0,
    DRXP_TestMode_Random=0b010000,   // PRBS31: Pseudo Random Bit Sequence. The pattern length is 2^31 - 1
    DRXP_TestMode_Serial=0b100000,   // 0x00000000,0x00000001,0x00000002,0x00000003,0x00000004,....
    DRXP_TestMode_Periodic=0b110000, // PRBS31: initialized every 48ms
    DRXP_TestMode_Fixed_0x3C=0x01,
    DRXP_TestMode_Fixed_0xC3=0x02,
    DRXP_TestMode_Fixed_0x3=0x03,
    DRXP_TestMode_Fixed_0xC=0x04,
    DRXP_TestMode_Fixed_0x5=0x05,
    DRXP_TestMode_Fixed_0x5555AAAA=0x06,
    DRXP_TestMode_Fixed_0x0=0x08,
    DRXP_TestMode_Fixed_0x1=0x09,
    DRXP_TestMode_Fixed_0xA=0x0A,
    DRXP_TestMode_Fixed_0xAAAA5555=0x0B,
    DRXP_TestMode_Fixed_0xFEDCBA98=0x0D, // represent a repetition of "0xFEDCBA98_01234567_BA987654_456789AB
    DRXP_TestMode_Fixed_0x01234567=0x0E  // represent a repetition of "0x01234567_89ABCDEF_FEDCBA98_76543210
};

enum correlationMode_t { SINGLE_DISH=0, INTERFEROMETRY };

enum sidebandSuppression_t { SB_SUPPRESSION_None=0, SB_SUPPRESSION_LoOffset, SB_SUPPRESSION_PhaseSwitching  };
//enum sidebandSuppression_t { SB_SUPPRESSION_None="None", SB_SUPPRESSION_LoOffset="LoOffset", SB_SUPPRESSION_PhaseSwitching="PhaseSwitching" };

enum WVRMethod_t { ATM_MODEL=0, EMPIRICAL };

}

#endif
