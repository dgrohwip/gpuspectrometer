#include "subArray_t.hxx"

namespace XSD {

bool subArray_t::get(const boost::property_tree::ptree& in)
{
    name = in.get("name", "Array0001");
    setCorrMode(in.get("corrMode", "SINGLE_DISH"));

    return validate();
}

bool subArray_t::put(boost::property_tree::ptree& out) const
{
    out.put("name", name);
    out.put("corrMode", getCorrMode());
    return validate();
}

}
