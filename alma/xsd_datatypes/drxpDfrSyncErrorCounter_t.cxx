#include "drxpDfrSyncErrorCounter_t.hxx"

namespace XSD {

bool drxpDfrSyncErrorCounter_t::get(const boost::property_tree::ptree& in)
{
    // NOTE: there is currently no use for get() since values arrive read-only from DRXP hardware

    return true;
}

bool drxpDfrSyncErrorCounter_t::put(boost::property_tree::ptree& out) const
{
    out.put("ch1BitD", ch1BitD);
    out.put("ch2BitC", ch2BitC);
    out.put("ch3BitB", ch3BitB);

    return true;
}

}
