#include "antennaParameters_t.hxx"

namespace XSD {

bool antennaParameters_t::get(const boost::property_tree::ptree& in)
{
    spectrometerInput = in.get("spectrometerInput", 1);
    loOffsetIndex = in.get("loOffsetIndex", 1);
    walshIndex = in.get("walshIndex", 1);
    return validate();
}

bool antennaParameters_t::put(boost::property_tree::ptree& out) const
{
    out.put("spectrometerInput", spectrometerInput.input);
    out.put("loOffsetIndex", loOffsetIndex);
    out.put("walshIndex", walshIndex);
    return validate();
}

}
