#include "spectrometerInput_t.hxx"

namespace XSD {

bool spectrometerInput_t::get(const boost::property_tree::ptree& in)
{
    return validate();
}

bool spectrometerInput_t::put(boost::property_tree::ptree& out) const
{
    return validate();
}

}
