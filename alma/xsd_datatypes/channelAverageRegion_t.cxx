#include "channelAverageRegion_t.hxx"

namespace XSD {

bool channelAverageRegion_t::get(const boost::property_tree::ptree& in)
{
    return validate();
}

bool channelAverageRegion_t::put(boost::property_tree::ptree& out) const
{
    return validate();
}

}
