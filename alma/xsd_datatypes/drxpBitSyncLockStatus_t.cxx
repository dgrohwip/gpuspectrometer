#include "drxpBitSyncLockStatus_t.hxx"

namespace XSD {

bool drxpBitSyncLockStatus_t::get(const boost::property_tree::ptree& in)
{
    // NOTE: there is currently no use for get() since values arrive read-only from DRXP hardware

    return true;
}

bool drxpBitSyncLockStatus_t::put(boost::property_tree::ptree& out) const
{
    out.put("syncUnlock", syncUnlock);
    out.put("syncLockOffset", syncLockOffset);

    return true;
}

}
