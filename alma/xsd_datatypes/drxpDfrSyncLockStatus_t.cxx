#include "drxpDfrSyncLockStatus_t.hxx"

namespace XSD {

bool drxpDfrSyncLockStatus_t::get(const boost::property_tree::ptree& in)
{
    // NOTE: there is currently no use for get() since values arrive read-only from DRXP hardware

    return true;
}

bool drxpDfrSyncLockStatus_t::put(boost::property_tree::ptree& out) const
{
    boost::property_tree::ptree pt_D;
    boost::property_tree::ptree pt_C;
    boost::property_tree::ptree pt_B;

    ch1BitD.put(pt_D);
    ch2BitC.put(pt_C);
    ch3BitB.put(pt_B);

    out.add_child("ch1BitD", pt_D);
    out.add_child("ch2BitC", pt_C);
    out.add_child("ch3BitB", pt_B);

    return true;
}

}
