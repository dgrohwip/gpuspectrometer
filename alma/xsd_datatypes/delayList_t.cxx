#include "delayList_t.hxx"
#include "global_defs.hxx"

#include <boost/foreach.hpp>

namespace XSD {

bool delayList_t::get(const boost::property_tree::ptree& in)
{
    using boost::property_tree::ptree;

    delayList.clear();

    spectrometerInput.input = in.get("spectrometerInput", 1);
    BOOST_FOREACH(const ptree::value_type &v, in.get_child("")) {
        if (v.first != "delay") { continue; }
        delay_t delay;
        delay.get(v.second);
        push_back(delay);
    }

    return validate();
}

bool delayList_t::put(boost::property_tree::ptree& out) const
{
    using boost::property_tree::ptree;
    std::deque<delay_t>::const_iterator it;

    out.put("spectrometerInput", spectrometerInput.input);
    for (it = delayList.begin(); it != delayList.end(); it++) {
        ptree dout;
        it->put(dout);
        out.add_child("delay", dout);
    }

    return validate();
}

void delayList_t::push_back(delay_t& delays)
{
    if (delayList.size() >= MAX_DELAYLIST_LENGTH) {
        delayList.pop_front();
    }
    delayList.push_back(delays);
}

}
