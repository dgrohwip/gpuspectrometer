#include "drxpBitAlarmStatus_t.hxx"

namespace XSD {

bool drxpBitAlarmStatus_t::get(const boost::property_tree::ptree& in)
{
    // NOTE: there is currently no use for get() since values arrive read-only from DRXP hardware

    return true;
}

bool drxpBitAlarmStatus_t::put(boost::property_tree::ptree& out) const
{
    out.put("temperatureWarning", temperatureWarning);
    out.put("outputBiasCurrentWarning", outputBiasCurrentWarning);
    out.put("outputPowerWarning", outputPowerWarning);
    out.put("rxInputPowerWarning", rxInputPowerWarning);
    out.put("temperatureAlarm", temperatureAlarm);
    out.put("outputBiasCurrentAlarm", outputBiasCurrentAlarm);
    out.put("outputPowerAlarm", outputPowerAlarm);
    out.put("rxInputPowerAlarm", rxInputPowerAlarm);
    out.put("i2cAccessError", i2cAccessError);

    return true;
}

}
