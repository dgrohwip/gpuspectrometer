/**
 * \class ASMInterface
 *
 * \brief Interface definition for container of ASM functions
 *
 * \author $Author: janw $
 *
 */
#ifndef ASMINTERFACE_HXX
#define ASMINTERFACE_HXX

#include <string>
#include <map>

#include <boost/noncopyable.hpp>
#include <boost/property_tree/ptree.hpp>

#include "xsd_datatypes/xsd_datatypes.hxx"
#include "mcwrapping/mc_configuration.hxx"
#include "mcwrapping/mc_observationqueue.hxx"
#include "mcwrapping/mc_spectralbackend.hxx"
#include "mcwrapping/mc_spectrometerstate.hxx"
#include "mcwrapping/mc_drxp_status.hxx"
#include "mcwrapping/mc_subarrays.hxx"
#include "network/XMLIO_iface.hxx"
#include "drxp/DRXPPCIeLookup.hxx"
#include "asm/CorrelationSettings.hxx"

class DRXPIface;

typedef std::map<unsigned int,MCConfiguration> MCConfigurationsMap_t;

class ASMInterface : private boost::noncopyable, public XMLIOIface
{
    public:

        ASMInterface() { }
        virtual ~ASMInterface() { }

    public:

        /** Load settings from a configuration file (JSON) */
        virtual bool loadConfigurationFile(const std::string&) = 0;

        /** Start DRXP(s) */
        virtual bool startDRXPs(void) = 0;

        /** Stop DRXP(s) */
        virtual bool stopDRXPs(void) = 0;

        /** Start observing queue handlers */
        virtual bool startObservingQueue() = 0;

        /** Stop observing queue handlers */
        virtual bool stopObservingQueue() = 0;

        /** Start M&C service */
        virtual bool startMC() = 0;

        /** Stop M&C service */
        virtual bool stopMC() = 0;

    public:
        /// Act on an XML-containing string and return an XML response
        virtual std::string xmlHandover(std::string xmlIn) = 0;

    public:

        /// Accessors
        virtual MCSpectrometerState& getStateContainer(void) = 0;
        virtual MCSpectralBackend& getBackend(void) = 0;
        virtual DRXPIface* getDRXP(int ifaceindex=0) = 0;
        virtual MCObservationQueue& getObservationQueue(void) = 0;
        virtual MCConfigurationsMap_t& getConfigsMap(void) = 0;
        virtual boost::property_tree::ptree& getUserConfig(void) = 0;
        virtual MCSubarrays& getSubarrays(void) = 0;
        virtual DRXPPCIeLookup& getDRXPLookup(void) = 0;
        virtual CorrelationSettings& getCorrelationSettings(void) = 0;

};

#endif // ASMINTERFACE_HXX
