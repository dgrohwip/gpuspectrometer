#ifndef CORRELATIONSETTINGS_HXX
#define CORRELATIONSETTINGS_HXX

#include "global_defs.hxx"
#include "xsd_datatypes/xsd_datatypes.hxx"

/// Storage for all settings not relevant to Spectrometer mode
class CorrelationSettings {

    public:

        XSD::Apc_t apc;
        XSD::antennaParameters_t antennaparameters[MAX_SPECTROMETER_INPUTS];
        XSD::antennaParameters_t antennaparametersRecent;
        XSD::wvrCoeff_t wvrCoeffs;
        XSD::delayListList_t delayLists;

};


#endif
