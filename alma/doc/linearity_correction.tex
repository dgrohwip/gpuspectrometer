%&latex
\documentclass{article}
\usepackage{mathtools}
\usepackage{graphicx}

\begin{document}

%+Title
\title{GPU Spectrometer\\Summary of Sampler Non-Linearity Correction}
\author{Jan Wagner}
\date{\today}
\maketitle
%-Title

%+Abstract
\begin{abstract}
    This document summarizes my understanding and implementation of the 3-bit digitizer linearity correction method devised by Takeshi Kamazaki\footnote{{kamazaki.takeshi@nao.ac.jp}} (cf. \cite{Kamazaki2014}). The correction scales and level-shifts auto power spectra by using signal statistics derived from digital sample histograms of the ALMA 3-bit quantized data and spline curves with ideal coefficients tabulated in Mazakis document. The correction is for auto spectra only, is not intended for cross-polarization auto spectra, and presumably is not applicable to visibility data.
\end{abstract}
%-Abstract

%+Contents
\tableofcontents
%-Contents

\pagebreak

\section{Implementation details}

Below are the processing steps that are carried out at various stages of the spectral processing.

\subsection{Processing of raw 3-bit sample data}

The goal of the method in \cite{Kamazaki2014} is to derive auto-power spectral correction curves at much finer  intervals ($< 48$~ms) than the long interval of 960~ms used in the previous 3-bit Digitizer Linearity Correction. The corrections require the analog power level to remain stable over the time period that the spectral corrections are applied to; 960~ms is too long. 

We assume samples are 3-bit quantized ({\em omitting a division by 2 here}) into
%
\begin{equation}
x[n] \in Q \textrm{~~where~~} Q \coloneqq \{-7, -5, -3, -1, +1, +3, +5, +7 \}
\end{equation}

Two parameters are derived from these quantized data: the direct sum, $D_0$, and  sum of squares, $D_1$, over a time period of 1~ms, given as (cf. \S~7.21.2 in \cite{Kamazaki2014})
%
\begin{eqnarray}
D_0 \coloneqq \sum^{N}{x[n]} \label{eq:def_D0} \mathrm{~~ and ~~}
D_1 \coloneqq \sum^{N}{(x[n])^2} \label{eq:def_D1}
\end{eqnarray}

The memo \cite{Kamazaki2014} defines the above for $N=4 \times 2^{20}$ samples  coming from a "Digital Power Meter", apparently implemented inside a 3-bit data receiver card. We  assume we process 3-bit samples from a 2~GHz wide analog voltage signal sampled at 4~Gsamples/s, rather than sampled power. To be compatible with the existing power estimation system we process $N = 4 \times 2^{20}$ samples total that contribute to $D_0$ and $D_1$. This amounts to 1.0486~ms rather than the more "native" 1~ms. By suggestion of M. Watanabe we follow the way this discrepancy is handled in ACA Correlator, i.e., consecutive 1.0486~ms data segments are overlapped with the previous segment by 0.0486~ms (194400000 samples). \\

In the CUDA implementation we do not actually calculate $D_0$ and $D_1$ directly. Instead, we form sample histograms in each polarization. Histograms allow an easy estimation of several signal statistics.

Histogram $h$ bins $h[q]$ count  occurrences of each of the eight possible sample values $q \in Q$ over a 1.0486~ms long signal $x[n]$. Bin counts are given by 
%
\begin{eqnarray}
h[q]= | \left\{ n \in \left\{1 \dotsc N\right\}: x[n] =q \right\}| 
\textrm{~~and with~~}
\sum_{q \in Q}{h[q]} \equiv N
\end{eqnarray}
%
An example histogram is illustrated in Figure~\ref{fig:hist} showing a close to ideal case with good analog signal level and low signal offset prior to Analog/Digital Conversion.

From the histogram one can determine mean voltage and mean power
%
\begin{eqnarray}
D_0 = \sum_{q \in Q}{(q\cdot h[q])} \label{eq:def2_D0} \mathrm{~~ and ~~}
D_1 = \sum_{q \in Q}{(q\cdot h[q])^2} \label{eq:def2_D0}
\end{eqnarray}
%
\begin{figure*}[tb]\centering
\includegraphics[width=9cm]{histogramexample.pdf}
\caption{\centering Example 3-bit sample histogram and derived parameters $D_0$ and $D_1$.\label{fig:hist}}
\end{figure*}

Next we derive values of four additional parameters of Step~2 in \cite{Kamazaki2014}, rewritten here in condensed form, that parameterize the correction curves that are subsequently used to correct the auto-power spectra:%
\begin{eqnarray}
P_{\rm out} \coloneqq \frac{D_1 - D_0^2/N}{4(N-1)} \\
a \coloneqq m^{-1}(P_{\rm out}) \\
b \coloneqq  m(P_{\rm out} \cdot m^{-1}(P_{\rm out})) = m(a\,P_{\rm out}) \\
c \coloneqq (1 - b)\cdot P_{\rm out} \cdot m^{-1}(P_{\rm out}) = (1 - b)\cdot a\,P_{\rm out}
\end{eqnarray}
%
The value of $a/b$ is the scaling factor for spectral bin amplitudes, $c/b$ is related to a spectral bin amplitude offset adjustment,. The map $m$ and its "inverse" map $m^{-1}$ that provide values for $a,b,c$ are given by cubic splines,
%
\begin{eqnarray}
m(p) \coloneqq \alpha_{i,0} (p-p_i)^3 + \alpha_{i,1} (p-p_i)^2 + \alpha_{i,2} (p-p_i) + \alpha_{i,3} \\
m^{-1}(P) \coloneqq A_{i,0} (P-P_i)^3 + A_{i,1} (P-P_i)^2 + A_{i,2} (P-P_i) + A_{i,3}
\end{eqnarray}
%
The centers, $p_i$, and  polynomial coefficients, $\alpha_{i,0 \dotsc 3}$, of each cubic spline segment, $i$, are given in Table~7.6 and respectively $P_i, A_{i,0 \dotsc 3}$ in Table~7.7 of \cite{Kamazaki2014}. \\

To correct a set of auto spectra we first determine $D_0$ and $D_1$ via raw 3-bit sample data from a short time slice. Sample data of 1~ms is sufficient as per\cite{Kamazaki2014}. From histogrammed sample data we derive $P_{\rm out}$. Based on $P_{\rm out}$ and cubic spline interpolation tables we determine $a$, $b$, and $c$.

\subsection{Processing of auto-power spectra}

First we form an auto-power spectrum, e.g., $X(\omega) \sim |\mathcal{DFT}(x[n])|^2$, of the sample data $x[n]$ within the same (or longer) time interval as used for determining $D_0$ and $D_1$. Then parameters $a$, $b$, and $c$, are used to correct each channel in a time averaged
auto-power spectrum using the linear map,
%
\begin{eqnarray}
X'[n] = \frac{a\,X[n] - f_3\,c}{b} \label{eq:linCorrAC}
\end{eqnarray}
%
where $f_3$ is a near-unity constant ($\simeq 1$) that depends on the ripple in receiver passband. Actual values are tabulated in ([no reference document known]). 

The resultant corrected auto-power spectrum can  be written out, or spectral windows can be extracted and/or spectra can be further spectrally averaged.

%\newpage
%+Bibliography
\begin{thebibliography}{99}
\bibitem{Kamazaki2014} T. Kamazaki, "3-bit Digitizer Linearity Correction", ACA-FX Memo, ALMA-CORR-06011-C, pp.219, 2014
%\bibitem{Label2} ...
\end{thebibliography}
%-Bibliography

\end{document}


