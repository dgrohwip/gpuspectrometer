/**
 * \class ASMConfigfileReader
 *
 * \brief Helper class that reads the contents on a JSON-format ASM configuration file
 *
 * The class parses a ASM configuration file in JSON format.
 * The expected structure of the JSON file is given below.
 *
 * {
 *     "network":{
 *         "tcpPortMC":"1337",                         The TCP port that ASM should listen on for M&C commands
 *         "hostNameASC":"localhost",                  The hostname of the remote ASC
 *         "tcpPortASC":"1234",                        The TCP port that the remote ASC is listening on
 *         "udpPortASC":"1234",                        The UDP port (not used) that the remote ASC is listening on
 *         "drxpMonMulticastGroup":"224.0.0.1",        The multicast group to use for DRXP status info multicasts
 *         "drxpMonMulticastPort":"30001"              The multicast UDP port to use for DRXP status info multicasts
 *     },
 *     "mc_xml":{
 *         "schema":"/opt/alma/conf/ASC_MC.xsd",       The XML schema file to use to validate M&C commands
 *         "encapsulation":"binxml"                    The type of M&C XML over TCP encapsulation to use (default: binxml)
 *                                                     none = plain text/xml, binxml = binary length field precedes text/xml
 *     },
 *     "hardware":{
 *         "assignedIF":"1",                           The IF assigned to this ASM; for bookkeeping only, no effect
 *         "assignedDRXPDevice":"/dev/drxpd0",         The DRXP character device to use (e.g. /dev/drxpd0)
 *         "assignedGPUIds":"0,1,2,3",                 Comma-separated list of one or more GPU's to use for this ASM instance
 *         "timeAlignGranularityMsec":"48"             Rounding granularity in milliseconds for converting DRXP software timestamps
 *                                                     into probable hardware timestamps. With 1PPS use 8 (8ms), with 48ms TE use 48 (48ms).
 *     },
 *     "operation":{
 *         "mode":"normal",                            Operation mode of the ASM (default: normal)
 *                                                     normal = normal spectrometer operation
 *                                                     simulateDTS = get input 3-bit data from a file
 *                                                     simulateMIME = provide ASM MIME output from a file
 *         "logfile":"ASM_MC_service.log",             The log file path and name
 *         "loglevel":"info",                          The logging detail level (debug2, debug1, info, warning)
 *         "startObsMinLeadtime":"5.0",                The minimum time that the requested start time in startObservation must lie in the future
 *         "mimeOutputPath":"/tmp/"                    Output directory where ASM should write MIME files
 *     },
 *     "simulation":{
 *         "inputFileDTS":"/opt/alma/share/fourlines_synthetic_48ms_2pol.3bit",    Used in operation.mode=simulateDTS
 *         "inputFileMIME":"/opt/alma/share/uid__X0_X1_X4.mime"                    Used in operation.mode=simulateMIME
 *     }
 * }
 *
 */

#include "ASMConfigfileReader.hxx" // class ASMConfigfileReader
#include "global_defs.hxx"         // some defaults for ASM configuration

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <string>
#include <iostream>

bool ASMConfigfileReader::readConfigFile(std::string jsonfilepath, boost::property_tree::ptree& config)
{
    /* Load JSON */
    bool success = false;
    try {
        boost::property_tree::json_parser::read_json(jsonfilepath, config);
        success = true;
    } catch (boost::property_tree::json_parser::json_parser_error &e) {
        std::cout << "Warning: failed to load configuration (" << jsonfilepath << " line " << e.line() << " " << e.message() << "), using defaults." << std::endl;
    }

    /* Assign defaults to any missing JSON entries */
    config.put("hardware.assignedIF", config.get<int>("hardware.assignedIF",DEFAULT_IF_NUMBER));
    config.put("hardware.assignedDRXPDevice", config.get<std::string>("hardware.assignedDRXPDevice",DEFAULT_DRXP_DEVICE));
    config.put("hardware.assignedGPUIds", config.get<std::string>("hardware.assignedGPUIds",""));
    config.put("hardware.timeAlignGranularityMsec", config.get<int>("hardware.timeAlignGranularityMsec",DEFAULT_DRXP_TIMEALIGN_GRANULARITY_MS));
    config.put("mc_xml.schema", config.get<std::string>("mc_xml.schema",DEFAULT_SCHEMA_FILE_PATH));
    config.put("mc_xml.encapsulation", config.get<std::string>("mc_xml.encapsulation","binxml"));
    config.put("network.tcpPortMC", config.get<int>("network.tcpPortMC",DEFAULT_MC_TCP_PORT));
    config.put("network.tcpPortASC", config.get<int>("network.tcpPortASC",DEFAULT_ASC_TCP_PORT));
    config.put("network.udpPortASC", config.get<int>("network.udpPortASC",DEFAULT_ASC_UDP_PORT));
    config.put("network.drxpMonMulticastGroup", config.get<std::string>("network.drxpMonMulticastGroup",DEFAULT_DRXP_MCAST_GROUP));
    config.put("network.drxpMonMulticastPort", config.get<int>("network.drxpMonMulticastPort",DEFAULT_DRXP_MCAST_PORT));
    config.put("operation.mode", config.get<std::string>("operation.mode","normal"));
    config.put("operation.logfile", config.get<std::string>("operation.logfile",DEFAULT_LOG_FILE));
    config.put("operation.loglevel", config.get<std::string>("operation.loglevel",DEFAULT_LOG_LEVEL));
    config.put("operation.startObsMinLeadtime", config.get<double>("operation.startObsMinLeadtime",DEFAULT_STARTOBS_MINIMUM_LEADTIME_SECS));
    config.put("operation.mimeOutputPath", config.get<std::string>("operation.mimeOutputPath",DEFAULT_MIME_OUTPUT_PATH));
    //config.put("operation.internalDFTLength", config.get<int>("operation.internalDFTLength",DEFAULT_INTERNAL_DFT_LENGTH)); // unused for now, hardcoded
    config.put("simulation.inputFileDTS", config.get<std::string>("simulation.inputFileDTS",DEFAULT_SIMULATION_DTS_INPUT));
    config.put("simulation.inputFileMIME", config.get<std::string>("simulation.inputFileMIME",DEFAULT_SIMULATION_MIME_INPUT));

    /* Print out final parsed result for debug/verify */
    //boost::property_tree::json_parser::write_json(std::cout, config);

    return success;
}
