/**
 * \class NTPDStatusQuery
 *
 * \brief Functionality to check NTP Daemon (local) for synchronization and quality
 *
 * Contains a comparison of NTP statistics against the requirements stated under
 * Section 6.5 of the document "ACA Spectrometer Module Timestamping Accuracy" are met.
 *
 * The local NTP daemon on ASM is queried over NTP Mode 6 commands (UDP/IP).
 * Reference documentation cf. https://docs.ntpsec.org/latest/mode6.html
 *
 * Requirements for proper NTP sync based on Section 6.5 are:
 *   #1 Leap field in the system status is not 3 (leap_alarm).
 *   #2 Source field in the system status is 6 (sync_ntp).
 *   #3 Stratum of the system variable is 2.
 *   #4 refid of the system variable is the IP address of the NTP server: ntp.osf.alma.cl with IP via gethostbyname()
 *   #5 Select field in the peer status is 6 (sel_sys.peer).
 *   #6 Synchronization distance is <= 21 milliseconds
 *   #7 Only one association (peer) is allowed; 2+ NTP servers caused large time steps in the ACA Correlator computers
 *
 * \author $Author: janw $
 *
 */
#ifndef NTPDSTATUSQUERY_HXX
#define NTPDSTATUSQUERY_HXX

#include <string>
#include <map>
#include <stdint.h>

//////////////////////////////////////////////////////////////////////////////////////////////////////////
/// NTP definitions adopted from NTP server ntp_control.h cf. https://github.com/ntp-project/ntp/
//////////////////////////////////////////////////////////////////////////////////////////////////////////

#define CTL_OP_READSTAT     1               // from NTP Daemon source code; ntp_control.h
#define CTL_OP_READVAR      2               // -"-
#define MAX_MAC_LEN (6 * sizeof(uint32_t))  // -"_
#define CTL_MAX_DATA_LEN    468             // -"-
#define SOURCE_ID_NTP       6               // "sync_ntp" cf. http://doc.ntp.org/latest/decode.html#sys
#define CTL_PST_SEL_SYSPEER 6               // "sel_sys.peer" cf. https://docs.ntpsec.org/latest/decode.html#peer
#define LEAP_IND_NO_SYNC    3               // leap indicator 0b11 means no sync
#define LEAP_IND_GOOD       0               // leap indicator 0b00 means synched and the minute has no leap second
#define ASSOCIATION_ID_HOST 0               // special association ID of 0 is always the system (system variables)
#define FLAG_RESPONSE       0x80
#define FLAG_ERROR          0x40
#define FLAG_MORE           0x20

#pragma pack(1)
/// NTP mode 6 op 2 packet data area, adopted from NTP Daemon source code ntp_control.h
typedef union {
    unsigned char data[480 + MAX_MAC_LEN]; /* data + auth */
    uint32_t u32[(480 + MAX_MAC_LEN) / sizeof(uint32_t)];
} NTPMode6PacketData_t;
#pragma pack()

#pragma pack(1)
/// NTP mode 6 packet structure, adopted from NTP Daemon source code ntp_control.h
typedef struct {
    uint8_t li_vn_mode; // 2-bit Leap Indicator, 3-bit Version, 3-bit Mode
    uint8_t r_m_e_op;   // 1-bit Response, 1-bit Error, 1-bit More, 5-bit Opcode
    uint16_t sequence;
    uint16_t status;    // 2-bit Leap Indicator, 6-bit sync source, 4-bit event count, 4-bit event id
    uint16_t associd;
    uint16_t offset;
    uint16_t count;
    NTPMode6PacketData_t udata;

    // -- li_vn_mode --
    // Leap Indicator:  2-bit   b11: alarm (no sync), b10: 59s in last minute, b01: 61s in last minute, b00: ok
    // Version:         3-bit   1 in requests, server typically 4 ver4
    // Mode:            3-bit   3: client, 4: server, 5: broadcast, 6: NTP control message

    // -- r_m_e_op --
    // Response flag    1-bit   1: response, 0: request
    // Error flag       1-bit   1: error response
    // More flag        1-bit   1: payload continues to next UDP packet
    // Opcode           5-bit   multiple values cf. https://docs.ntpsec.org/latest/mode6.html

    // -- status --
    // type 1 -- System/Peer Status (CTL_OP_READSTAT)
    // Status Field:    5-bit   broacast assoc, reachable, authentication info, persistent assoc, ...
    // Select:          3-bit   0: sel_reject, 6: sel_sys.peer, and others
    // Count:           4-bit
    // Code:            4-bit
    // type 2 -- System/Peer Variable (CTL_OP_READVAR)
    // Leap Indicator:  2-bit   same as in li_vn_mode
    // Source:          6-bit   0: not yet synched, 1: pps sync, 6: sync_ntp
    // Count:           4-bit   number of events since the last time the code changed
    // Event:           4-bit   various event ids possible

} NTPMode6Packet_t;
#pragma pack()

//////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Actual class NTPDStatusQuery
//////////////////////////////////////////////////////////////////////////////////////////////////////////

class NTPDStatusQuery
{
    public:

        /** C'stor. Reads out NTP server data */
        NTPDStatusQuery(bool isProductionEnv=false);

    public:

        /**
         * Query the NTP server (NTP_CLIENT_HOST, NTP_PORT) via an UDP/IP NTP v4 Mode 6 request,
         * and check whether all requirements placed by ASM on the NTP time sync quality are met.
         * \return True if NTPd server could be reached and ASM timing requirements are met
         */
        bool requirementsMet();

    private:
        bool readoutNTP();
        bool validateConditions();

    public:

        // Accessors

        /** \return Synchronization distance in milliseconds (NTP mode 6 query) */
        float getSynchronizationDistance_msec() const { return m_synchronizationdistance; }

        /** \return True if no errors and computer seems NTP-synchronized properly */
        bool isSynched() const { return !m_error && m_synced; }

        /** \return Root delay */
        double getRootDelay_msec() const { return m_rootdelay; }

        /** \return Root dispersion */
        double getRootDispersion_msec() const { return m_rootdispersion; }

        /** \return Stratum of (lcoal)NTP server */
        int getStratum() const { return m_stratum; }

        /** \return Address (IP) of associated single NTP peer */
        std::string getPeer() const { return m_refId; }

        /** \return Jitter of associated single NTP peer */
        double getPeerJitter_msec() const { return m_peer_jitter; }

    public:

        // Helpers

        /**
         * Send a NTP mode 6 op 2 query with a comma-separated list of items to server,
         * and fill in the response into the call argument NTPMode6Packet_t&.
         * Use Association ID of 0 to check system variables, or ID of peer to check peer variables.
         */
        bool queryMode6(std::string request_vars, int opCode, int associationId, NTPMode6Packet_t& response) const;

        /** Tokenize a comma-separated response into string,string pairs */
        std::map<std::string,std::string> tokenizeNtpResponse(std::string response) const;

        /** \return True if IP matches against at least one out of all IP&aliases of a hostname */
        bool compareIPv4Addr(std::string ip, std::string hostname) const;

    private:
        const std::string m_ntpd_hostname;
        const int m_ntpd_port;
        const bool m_productionEnvironment;
        std::string m_requiredPeer;

        bool m_error;
        bool m_synced;

        float m_synchronizationdistance;

    private:
        // Variables read out from NTP server
        int m_leapindicator;
        int m_syncsource;
        int m_stratum;
        std::string m_refId;
        bool m_refidIsCorrect;
        int m_peerselectflag;
        double m_rootdelay;
        double m_rootdispersion;

        int m_numpeers;
        int m_peer;
        double m_peer_jitter;

};

#endif // NTPDSTATUSQUERY_HXX
