#include "time/NTPDStatusQuery.hxx"

#include <iostream>

int main(int argc, char** argv)
{
    NTPDStatusQuery info_testenv(false);
    NTPDStatusQuery info_production(true);

    std::cout << "TEST ENV -- Checking NTP via UDP/IP NTP v4 Mode 6 request\n";
    std::cout << "timing requirements met=" << info_testenv.requirementsMet() << ", "
        << "peer=" << info_testenv.getPeer() << ", "
        << "peerjitter=" << info_testenv.getPeerJitter_msec() << " ms, "
        << "stratum=" << info_testenv.getStratum() << ", "
        << "rootdispersion=" << info_testenv.getRootDispersion_msec() << " ms, "
        << "sync=" << info_testenv.isSynched() << "\n\n";

    std::cout << "PRODUCTION ENV -- Checking NTP via UDP/IP NTP v4 Mode 6 request\n";
    std::cout << "timing requirements met=" << info_production.requirementsMet() << ", "
        << "peer=" << info_production.getPeer() << ", "
        << "peerjitter=" << info_production.getPeerJitter_msec() << " ms, "
        << "stratum=" << info_production.getStratum() << ", "
        << "rootdispersion=" << info_production.getRootDispersion_msec() << " ms, "
        << "sync=" << info_production.isSynched() << "\n";

    return 0;
}

