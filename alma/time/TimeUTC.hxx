/**
 * \class TimeUTC
 *
 * \brief Similar to TimeTAI class except no conversion from system clock (UTC) to TAI time.
 *
 * The purpose is to make a switch between UTC or TAI timestamp definitions easier.
 * Intially ALMA was thought to use TAI timestamps. Later it turned out UTC is used.
 *
 * \author $Author: janw $
 *
 */
#ifndef MC_TIME_UTC_HXX
#define MC_TIME_UTC_HXX

#include <sys/time.h>
#include <time.h>
#include <string>

#define C_UTC_refepstr  "1972-01-01 00:00:00.0"
#define C_UTC_refsec    2272060800ULL // 1972-01-01 as seconds since 1900-01-01 (used by the IETF leap sec table)

class TimeUTC
{
    public:

        TimeUTC() { }

    public:

        /** \return Current time as UTC time string. */
        std::string getCurrentTime();

        /** \return Current time as UTC time in seconds since reference epoch 1972. */
        static void getCurrentTime(double* utc_sec_1972);

        /** \return Seconds since reference epoch 1972 for the time in given UTC string */
        static bool stringToSeconds(const std::string& utc, double* utc_sec);

        /** \return Struct timeval for the time in given UTC string */
        static bool stringToTimeval(const std::string& utc, struct timeval* tv);

        /** \return Struct timespec for the time in given UTC string */
        static bool stringToTimespec(const std::string& utc, struct timespec* ts);

        /** \return String representation of the time given in struct timeval */
        static bool timevalToString(const struct timeval& tv, std::string& utc);

        /** \return String representation of the time given in struct timespec */
        static bool timespecToString(const struct timespec& ts, std::string& utc);

        /** \return Time difference in seconds between the current UTC/wallclock time and a specified UTC time. */
        static double getSecondsSince(const std::string& utc);

};

#endif // MC_TIME_UTC_HXX
