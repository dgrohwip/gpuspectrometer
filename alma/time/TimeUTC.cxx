/**
 * \class TimeUTC
 *
 * \brief Similar to TimeTAI class except no conversion from system clock (UTC) to TAI time.
 *
 * The purpose is to make a switch between UTC or TAI timestamp definitions easier.
 * Intially ALMA was thought to use TAI timestamps. Later it turned out UTC is used.
 *
 * \author $Author: janw $
 *
 */

#include "helper/logger.h"
#include "time/TimeUTC.hxx"

#include <boost/date_time/local_time/local_time.hpp>

#include <algorithm>
#include <string>
#include <iostream>
#include <cmath>

namespace pt = boost::posix_time;

static const std::string C_time_format("2017-01-01T23:59:59.999");
static const pt::ptime   C_Unix_refep(pt::time_from_string("1970-01-01 00:00:00.0"));
static const pt::ptime   C_UTC_refep(pt::time_from_string(C_UTC_refepstr));

///////////////////////////////////////////////////////////////////////////////////////

/** \return Current time in UTC as seconds since reference epoch 1972-01-01 00:00:00 */
void TimeUTC::getCurrentTime(double* utc_sec)
{
    pt::ptime now_utc = pt::microsec_clock::universal_time();
    *utc_sec = (now_utc - C_UTC_refep).total_milliseconds() * 1e-3;
}

/** \return Seconds since reference epoch 1972 for the time in given UTC string. True on success. */
bool TimeUTC::stringToSeconds(const std::string& utc, double* utc_sec)
{
    if (utc_sec == NULL) { return false; }

    const std::string::size_type nexpect = C_time_format.size();
    if (utc.size() != nexpect) {
        L_(lerror) << "TimeUTC::stringToSeconds() time '" << utc << "' does not match expected format of '" << C_time_format << "'";
        return false;
    }

    try {
        std::string s(utc);
        std::replace(s.begin(), s.end(), 'T', ' ');
        *utc_sec = (pt::ptime(pt::time_from_string(s)) - C_UTC_refep).total_milliseconds() * 1e-3;
    } catch (...) {
        L_(lerror) << "TimeUTC::stringToSeconds() failed to parse '" << utc << "'! Use a format like '" << C_time_format << "'!";
        return false;
    }
    return true;
}

/** \return Struct timeval for the time in given UTC string */
bool TimeUTC::stringToTimeval(const std::string& utc, struct timeval* tv)
{
    if (tv == NULL) { return false; }

    // Defaults in case of blank time string, or later errors
    tv->tv_sec = 0;
    tv->tv_usec = 0;
    if (utc == "") {
        return true;
    }

    // Check the format
    const std::string::size_type nexpect = C_time_format.size();
    if (utc.size() != nexpect) {
        L_(lerror) << "TimeUTC::stringToTimeval() time '" << utc << "' does not match expected format of '" << C_time_format << "'";
        return false;
    }

    // Decode
    try {
        std::string s(utc);
        std::replace(s.begin(), s.end(), 'T', ' ');
        pt::time_duration dt = pt::ptime(pt::time_from_string(s)) - C_Unix_refep;
        tv->tv_sec = dt.total_seconds();
        tv->tv_usec = dt.fractional_seconds();
    } catch (...) {
        L_(lerror) << "TimeUTC::stringToTimeval() failed to parse '" << utc << "'! Use a format like '" << C_time_format << "'!";
        return false;
    }
    return true;
}

/** \return Struct timeval for the time in given UTC string */
bool TimeUTC::stringToTimespec(const std::string& utc, struct timespec* ts)
{
    if (ts == NULL) { return false; }
    ts->tv_sec = 0;
    ts->tv_nsec = 0;
    if (utc == "") {
        return true;
    }

    // Alternatively could use:
    //struct timeval tv;
    //bool rc = stringToTimeval(utc, &tv);
    //TIMEVAL_TO_TIMESPEC(&tv, ts);
    //return rc;

    const std::string::size_type nexpect = C_time_format.size();
    if (utc.size() != nexpect) {
        L_(lerror) << "TimeUTC::stringToTimespec() time '" << utc << "' does not match expected format of '" << C_time_format << "'";
        return false;
    }

    try {
        std::string s(utc);
        std::replace(s.begin(), s.end(), 'T', ' ');
        pt::time_duration dt = pt::ptime(pt::time_from_string(s)) - C_Unix_refep;
        ts->tv_sec = dt.total_seconds();
        ts->tv_nsec = dt.fractional_seconds()*1000;
    } catch (...) {
        L_(lerror) << "TimeUTC::stringToTimespec() failed to parse '" << utc << "'! Use a format like '" << C_time_format << "'!";
        return false;
    }
    return true;
}

/** \return String representation of the time given in struct timeval */
bool TimeUTC::timevalToString(const struct timeval& tv, std::string& utc)
{
    pt::ptime ptv = pt::from_time_t(tv.tv_sec);
    ptv += pt::microseconds(tv.tv_usec);
    utc = pt::to_iso_extended_string(ptv);
    utc = utc.substr(0, C_time_format.size());
    return true;
}

/** \return String representation of the time given in struct timespec */
bool TimeUTC::timespecToString(const struct timespec& tv, std::string& utc)
{
    pt::ptime ptv = pt::from_time_t(tv.tv_sec);
    ptv += pt::microseconds(tv.tv_nsec * 1.0e-3f);
    utc = pt::to_iso_extended_string(ptv);
    utc = utc.substr(0, C_time_format.size());
    return true;
}

/** \return Difference in seconds from current time and given time, or NaN on error. */
double TimeUTC::getSecondsSince(const std::string& utc)
{
    double dT;
    double tgiven, tnow;
    getCurrentTime(&tnow);
    if (!stringToSeconds(utc, &tgiven)) {
        return NAN;
    }
    dT = tnow - tgiven;
    //std::cout << "getSecsSince: curr=" << tnow << ", other=" << tgiven << " (str=" << tai << "), dT=" << dT << "\n";
    return dT;
}

///////////////////////////////////////////////////////////////////////////////////////
