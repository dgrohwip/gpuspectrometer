/**
 * \class MCSystemInformation
 *
 * \brief Functionality to pull informatin about GPU, CPU/system, receiver boards.
 *
 * \author $Author: janw $
 *
 */
#ifndef MC_SYSTEMINFORMATION_HXX
#define MC_SYSTEMINFORMATION_HXX

#include <boost/property_tree/ptree.hpp>
#include <string>

class MCSystemInformation
{
    public:

        /** C'stor. Sets program paths to their defaults (lshw, nvidia-smi, others) and reads system infos */
        MCSystemInformation();

    public:

        /** Read out system information and current temperature and power consumption */
        void refresh();

        /** \return True if all functions pass testing */
        bool selfTestOK();

        /** \return Tree with entries as in ASC_MC.xsd schema response to getSystemInformation */
        const boost::property_tree::ptree& get() const { return m_infos; };

        /** Fill out the given tree with entries as in ASC_MC.xsd schema response to getSystemInformation */
        void fill(boost::property_tree::ptree& out) const;

    public:

        /** Change paths to external programs from their defaults to custom paths */
        void setProgramPaths(std::string lshw, std::string nvidiasmi, std::string lmsensors, std::string grep);

    private:
        bool file_is_readable(const std::string& fname, bool warn=false) const;

    private:
        float get_system_temperatureC();
        float get_system_powerW();
        void refresh_ifaces_network(boost::property_tree::ptree& out);
        void refresh_ifaces_DRXP(boost::property_tree::ptree& out);
        void refresh_NVIDIA(boost::property_tree::ptree& out);

    private:
        boost::property_tree::ptree m_infos;
        bool m_error;

    private:
        std::string m_path_lshw;
        std::string m_path_nvidiasmi;
        std::string m_path_lmsensors;
        std::string m_path_grep;
};


#endif // MC_SYSTEMINFORMATION_HXX

