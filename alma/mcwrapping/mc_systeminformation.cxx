/**
 * \class MCSystemInformation
 *
 * \brief Functionality to pull informatin about GPU, CPU/system, receiver boards.
 *
 * \author $Author: janw $
 *
 */

#include "mc_systeminformation.hxx"
#include "../drxp/DRXPPCIeLookup.hxx"
#include "../helper/pstream.hxx"    // for subprocess STDOUT as stream

#include <boost/foreach.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/** C'stor. Sets program paths to their defaults (lshw, nvidia-smi, others) and reads system infos */
MCSystemInformation::MCSystemInformation()
{
    setProgramPaths("/usr/sbin/lshw", "/usr/bin/nvidia-smi", "/usr/bin/sensors", "/bin/grep");

    // Auto-detect binary file paths if default locations don't seem correct
    std::string roots[4] = { "/usr/sbin/", "/usr/bin/", "/bin/", "/sbin/" };
    for (int n=0; n<4; n++) {
        if (!file_is_readable(m_path_lshw)) {
            if (file_is_readable(roots[n] + "lshw")) {
                m_path_lshw = roots[n] + "lshw";
            }
        }
        if (!file_is_readable(m_path_nvidiasmi)) {
            if (file_is_readable(roots[n] + "nvidia-smi")) {
                m_path_nvidiasmi = roots[n] + "nvidia-smi";
            }
        }
        if (!file_is_readable(m_path_lmsensors)) {
            if (file_is_readable(roots[n] + "sensors")) {
                m_path_lmsensors = roots[n] + "sensors";
            }
        }
        if (!file_is_readable(m_path_grep)) {
            if (file_is_readable(roots[n] + "grep")) {
                m_path_grep = roots[n] + "grep";
            }
        }
    }

    // Load system infos
    refresh();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/** Change paths to external programs from their defaults to custom paths */
void MCSystemInformation::setProgramPaths(std::string lshw, std::string nvidiasmi, std::string lmsensors, std::string grep)
{
    m_path_lshw = lshw;
    m_path_nvidiasmi = nvidiasmi;
    m_path_lmsensors = lmsensors;
    m_path_grep = grep;
}

/** Helper. Check if file exists and is readable (not necessarily executable...) */
bool MCSystemInformation::file_is_readable(const std::string& fname, bool warn) const
{
    std::ifstream infile(fname.c_str());
    if (!infile.good() && warn) {
        std::cerr << "Error: MCSystemInformation: cannot access " << fname << "\n";
    }
    return infile.good();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

void MCSystemInformation::refresh_ifaces_network(boost::property_tree::ptree& out)
{
    using namespace boost::property_tree;
    ptree ifaces;

    /* Put output of 'lshw' into a file */
	// TODO: replace std::tmpnam() somehow, alas all methods for mkstemp-->fstream are hacks (https://stackoverflow.com/questions/499636/how-to-create-a-stdofstream-to-a-temp-file)
    std::string fname(std::tmpnam(NULL));
    try {
        std::string cmd = m_path_lshw + std::string(" -quiet -sanitize -xml -class network 2>/dev/null > ") + fname.c_str();
        redi::ipstream in(cmd.c_str());
        std::stringstream() << in.rdbuf();
    } catch (...) {
        std::cerr << "Error: MCSystemInformation::ifaces() failed to get lshw to output XML file '" << fname << "'!\n";
        out.put("receiverCards", "");
        m_error = true;
        return;
    }

    /* Load the XML file */
    try {
        read_xml(fname.c_str(), ifaces, xml_parser::trim_whitespace);
        remove(fname.c_str());
    } catch (...) {
        std::cerr << "Error: MCSystemInformation::ifaces() failed to parse the lshw XML file '" << fname << "'!\n";
        out.put("receiverCards", "");
        remove(fname.c_str());
        m_error = true;
        return;
    }

    /* Traverse the XML document */
    try {
        int id = 0;
        BOOST_FOREACH(ptree::value_type &v, ifaces.get_child("list")) {

            if (v.first != "node") { continue; }

            float speed = v.second.get("size", -1.0f);
            float capacity = v.second.get("capacity", -1.0f);
            if (speed < 0) { speed = capacity; }
            if (speed < 10e9) { continue; }
            std::string mediatype = (speed < 40e9) ? "10GbE" : "40GbE";

            ptree card;
            card.put("<xmlattr>.deviceId", id);
            card.put("<xmlattr>.numInterfaces", 1);
            card.put("<xmlattr>.mediatype", mediatype);

            out.add_child("receiverCards.receiverCard", card);
            id++;
        }
        if (id <= 0) {
            // Blank element if no cards were found
            out.put("receiverCards", "");
        }
    } catch (...) {
        std::cerr << "Error: MCSystemInformation::ifaces() failed to parse the nvidia-smi XML document!\n";
        m_error = true;
        return;
    }
}

void MCSystemInformation::refresh_ifaces_DRXP(boost::property_tree::ptree& out)
{
    using namespace boost::property_tree;

    DRXPPCIeLookup lookup;
    const std::set<std::string>& devices = lookup.getInstalledDevices();

    out.put("totalBasebands", lookup.getInstalledInterfacesCount());

    std::set<std::string>::const_iterator dev;
    for (dev = devices.begin(); dev != devices.end(); dev++) {
        ptree card;
        card.put("<xmlattr>.deviceId", *dev);
        card.put("<xmlattr>.numInterfaces", lookup.getDeviceInterfaceCount(*dev));
        card.put("<xmlattr>.mediatype", "DTS");

        out.add_child("receiverCards.receiverCard", card);
    }

    return;
}

void MCSystemInformation::refresh_NVIDIA(boost::property_tree::ptree& out)
{
    using namespace boost::property_tree;
    std::string fname(std::tmpnam(NULL));
    ptree nvidia;

    /* Use 'nvidia-smi' in XML output format mode to get NVIDIA card details */
    // note, the option [-f outfile] gets stuck so we grab STDOUT first...
    try {
        redi::ipstream in(m_path_nvidiasmi + " -q -x 2>/dev/null");
        std::ofstream ftmp(fname.c_str());
        if (!ftmp) {
            std::cerr << "Error: MCSystemInformation::NVIDIA() failed to open temp file!\n";
            m_error = true;
            return;
        }
        ftmp << in.rdbuf();
        ftmp.flush();
    } catch (...) {
        std::cerr << "Error: MCSystemInformation::NVIDIA() failed to get nvidia-smi to output XML file '" << fname << "'!\n";
        out.put("computingCards", "");
        m_error = true;
        return;
    }

    /* Load the XML file */
    try {
        read_xml(fname.c_str(), nvidia, xml_parser::trim_whitespace);
        remove(fname.c_str());
    } catch (...) {
        std::cerr << "Error: MCSystemInformation::NVIDIA() failed to parse the nvidia-smi XML file '" << fname << "'!\n";
        out.put("computingCards", "");
        remove(fname.c_str());
        m_error = true;
        return;
    }

    /* Traverse the XML document */
    try {
        int id = 0;
        BOOST_FOREACH(ptree::value_type &v, nvidia.get_child("nvidia_smi_log")) {

            if (v.first != "gpu") continue;

            std::string name = v.second.get("product_name", "unknown");
            std::string T = v.second.get("temperature.gpu_temp", "0 C");
            std::string P = v.second.get("power_readings.power_draw", "0 W");
            T.erase(T.end()-1); // erase unit C
            P.erase(P.end()-1); // erase unit C

            ptree card;
            card.put("<xmlattr>.deviceId", id);
            card.put("productName", name);
            card.put("currentTemperature", atof(T.c_str()));
            card.put("currentTemperature.<xmlattr>.unit", "C");
            card.put("currentPowerConsumption", atof(P.c_str()));
            card.put("currentPowerConsumption.<xmlattr>.unit", "W");

            out.add_child("computingCards.computingCard", card);
            id++;
        }
        if (id <= 0) {
            // Blank element if no cards were found
            out.put("computingCards", "");
        }
    } catch (...) {
        std::cerr << "Error: MCSystemInformation::NVIDIA() failed to parse the nvidia-smi XML document!\n";
        m_error = true;
        return;
    }

}

float MCSystemInformation::get_system_temperatureC()
{
    float T = -273.0f;
    try {
        redi::ipstream in(m_path_lmsensors + " | " + m_path_grep + " -i physical");
        std::string s;
        while (std::getline(in, s)) {
            // Fixed line format from 'sensors' is: "Physical id 0:  +45.0°C  (high = +80.0°C, crit = +98.0°C)"
            if (s.length() < 25) { continue; }
            float Tsubunit = atof(s.substr(16,5).c_str());
            if (Tsubunit > T) { T = Tsubunit; }
        }
    } catch (...) {
        std::cerr << "Error: MCSystemInformation::Temperature() failed to parse output from " << m_path_lmsensors << "!\n";
        m_error = true;
    }
    return T;
}

float MCSystemInformation::get_system_powerW()
{
    float P = 0.0f;
    try {
        redi::ipstream in(m_path_lmsensors + " | " + m_path_grep + " -i power1");
        std::string s;
        while (std::getline(in, s)) {
            // Fixed line format from 'sensors' is: "power1:      153.00 W  (interval =   1.00 s)"
            if (s.length() < 25) { continue; }
            float Psubunit = atof(s.substr(10,10).c_str());
            if (Psubunit > P) { P = Psubunit; }
        }
    } catch (...) {
        std::cerr << "Error: MCSystemInformation::Power() failed to parse output from " << m_path_grep << "!\n";
        m_error = true;
    }
    return P;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

void MCSystemInformation::refresh()
{
    m_error = false;
    m_infos.clear();

    float T = get_system_temperatureC();
    float P = get_system_powerW();

    m_infos.put("systemTemperature", T);
    m_infos.put("systemTemperature.<xmlattr>.unit", "C");
    if (P > 0.0f) {
        m_infos.put("systemPower", P);
        m_infos.put("systemPower.<xmlattr>.unit", "W");
    }
    //m_infos.put("totalBasebands", 1);
    //refresh_ifaces_network(m_infos);
    refresh_ifaces_DRXP(m_infos);
    refresh_NVIDIA(m_infos);

    // write_xml(std::cout, m_infos);
}

/** Fill out the given tree with entries as in ASC_MC.xsd schema response to getSystemInformation */
void MCSystemInformation::fill(boost::property_tree::ptree& out) const
{
    out.put_child("systemTemperature", m_infos.get_child("systemTemperature"));
    out.put_child("totalBasebands", m_infos.get_child("totalBasebands"));
    out.put_child("receiverCards", m_infos.get_child("receiverCards"));
    out.put_child("computingCards", m_infos.get_child("computingCards"));
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

bool MCSystemInformation::selfTestOK()
{
    if (!file_is_readable(m_path_lshw, true)) {
        return false;
    }
    if (!file_is_readable(m_path_nvidiasmi, true)) {
        return false;
    }
    if (!file_is_readable(m_path_lmsensors, true)) {
        return false;
    }
    if (!file_is_readable(m_path_grep)) {
        return false;
    }

    try {
        refresh();
    } catch (...) {
        m_error = true;
    }

    return !m_error;
}
