/**
 * \class MCObservationQueue
 *
 * \brief Manages a list of MCObservation items.
 *
 * \author $Author: janw $
 *
 */
#ifndef MC_OBSERVATIONQUEUE_HXX
#define MC_OBSERVATIONQUEUE_HXX

#include "mcwrapping/mc_observation.hxx"

#include <boost/property_tree/ptree.hpp>
#include <boost/thread/mutex.hpp>
// #include <boost/thread.hpp>

#include <string>
#include <deque>
#include <exception>
#include <iostream>

class MCObservationQueue
{
    public:
        MCObservationQueue();
        ~MCObservationQueue();

    public:
        /* Start monitoring and processing the observation queue */
        void start();

        /* Stop monitoring and processing the observation queue */
        void stop();

        /* Clear the observation queue, and stopmonitoring and processing the queue */
        void reset();

    public:

        /** \return True if at least one observation was found. Also fills out Boost PropertyTree with infos of observing queue entries */
        bool getCurrentObservations(boost::property_tree::ptree& p, const int nmax=1);

        /** \return True if observation was successfully added to the queue, False if not (e.g. duplicate ID) */
        bool enqueueObservation(MCObservation& obs);

       /** Stop an observation. Has no effect on an already stopped observation.
        *  Removes the observation if its stop time was reached, or if it had no assigned stop time.
        *  \return True if observation stopped at the return from this function.
        */
        bool stopObservation(const std::string& obsId);

        /** \return True if the given observation ID does not yet exist (i.e. if it is still available) */
        bool isObservationIdUnique(const std::string& id);

        /** \return True if the (optional) observation stop time field was successfully set */
        bool setObservationStopTime(const std::string& id, const std::string& utc);

        /** Check for a soon-upcoming observation in the queue.
         * Chooses the next PENDING observation that has a future start time close to current computer UTC time.
         * Optionally also considers any PENDING observation whose start time (but not stop time) is already in the past.
         * \return True if a suitable observation is found.
         */
        bool getUpcomingObservation(int* index, MCObservation** pobs, double* dT_to_next,
            bool allow_past=true, const double min_leadtime_sec=DEFAULT_STARTOBS_MINIMUM_LEADTIME_SECS
        );

        /** Remove old observations from queue */
        int removeExpiredObservations();

        /** Return length of queue */
        int getQueueLength(void) const { return m_observationQueue.size(); }

    private:
        boost::mutex m_obsQueue_mutex;
        std::deque<MCObservation> m_observationQueue;
};

#endif // MC_OBSERVATIONQUEUE_HXX
