
#include "mcwrapping/mc_drxp_status.hxx"
#include "drxp/DRXPIface.hxx"
#include "xsd_datatypes/xsd_enums.hxx"
#include "xsd_datatypes/xsd_datatypes.hxx"

#include <boost/property_tree/ptree.hpp>
#include <stdio.h> // just for summarizeToConsole() here

void MCDRXPStatus::clear()
{
    memset(&alarmStatus, 0, sizeof(alarmStatus));
    memset(&powerAlarm, 0, sizeof(powerAlarm));
    memset(&dfrStatus, 0, sizeof(dfrStatus));
    memset(&dfrVoltageStatus, 0, sizeof(dfrVoltageStatus));
    memset(&syncLockStatus, 0, sizeof(syncLockStatus));
    drxpTestMode.ch1BitD = XSD::DRXP_TestMode_No;
    drxpTestMode.ch2BitC = XSD::DRXP_TestMode_No;
    drxpTestMode.ch3BitB = XSD::DRXP_TestMode_No;
    memset(&checksumCounter, 0, sizeof(checksumCounter));
    memset(&syncErrorCounter, 0, sizeof(syncErrorCounter));
    memset(&metaframeDelay, 0, sizeof(metaframeDelay));
    memset(&drxpTemperatures, 0, sizeof(drxpTemperatures));
    memset(&fpgaSEUInfo, 0, sizeof(fpgaSEUInfo));
}

////////////////////////////////////////////////////////////////////////////////////////

bool MCDRXPStatus::deviceReadout(DRXPIface* drxp)
{
    // Tell DRXP API to fill us out
    if (!drxp->deviceReadout(*this)) {
        return false;
    }
    invariant();
    return true;
}

////////////////////////////////////////////////////////////////////////////////////////

bool MCDRXPStatus::getChecksumCounters(boost::property_tree::ptree& pt) const
{
    if (!is_valid) {
        return false;
    }

    boost::property_tree::ptree psub;
    checksumCounter.put(psub);
    pt.add_child("checksumCounter", psub);

    return true;
}

bool MCDRXPStatus::getSyncErrorCounters(boost::property_tree::ptree& pt) const
{
    if (!is_valid) {
        return false;
    }

    boost::property_tree::ptree psub;
    syncErrorCounter.put(psub);
    pt.add_child("syncErrorCounter", psub);

    return true;
}

bool MCDRXPStatus::getDrxpMetaframeDelay(boost::property_tree::ptree& pt) const
{
    // TODO
    if (!is_valid) {
        return false;
    }

    boost::property_tree::ptree psub;
    metaframeDelay.put(psub);
    pt.add_child("metaframeDelay", psub);

    return true;
}

////////////////////////////////////////////////////////////////////////////////////////

bool MCDRXPStatus::getAlarmStatus(boost::property_tree::ptree& pt) const
{
    if (!is_valid) {
        return false;
    }

    boost::property_tree::ptree psub;
    alarmStatus.put(psub);
    pt.add_child("alarmStatus", psub);

    return true;
}

bool MCDRXPStatus::getPowerAlarms(boost::property_tree::ptree& pt) const
{
    using boost::property_tree::ptree;
    if (!is_valid) {
        return false;
    }

    boost::property_tree::ptree psub;
    powerAlarm.put(psub);
    pt.add_child("powerAlarm", psub);

    return true;
}

bool MCDRXPStatus::getDfrStatus(boost::property_tree::ptree& pt) const
{
    if (!is_valid) {
        return false;
    }

    boost::property_tree::ptree psub;
    dfrStatus.put(psub);
    pt.add_child("dfrStatus", psub);

    return true;
}

bool MCDRXPStatus::getDfrVoltages(boost::property_tree::ptree& pt) const
{
    using boost::property_tree::ptree;
    if (!is_valid) {
        return false;
    }

    boost::property_tree::ptree psub;
    dfrVoltageStatus.put(psub);
    pt.add_child("dfrVoltageStatus", psub);

    return true;
}

bool MCDRXPStatus::getSyncStatus(boost::property_tree::ptree& pt) const
{
    if (!is_valid) {
        return false;
    }

    boost::property_tree::ptree psub;
    syncLockStatus.put(psub);
    pt.add_child("syncLockStatus", psub);

    return true;
}

bool MCDRXPStatus::getDrxpTestmode(boost::property_tree::ptree& pt) const
{
    if (!is_valid) {
        return false;
    }

    boost::property_tree::ptree psub;
    drxpTestMode.put(psub);
    pt.add_child("drxpTestMode", psub);

    return true;
}

bool MCDRXPStatus::getDrxpStatus(boost::property_tree::ptree& pt) const
{
    if (!is_valid) {
        return false;
    }
    getAlarmStatus(pt);
    getPowerAlarms(pt);
    getDfrStatus(pt);
    getDfrVoltages(pt);
    getSyncStatus(pt);
    getDrxpTestmode(pt);
    return true;
}

////////////////////////////////////////////////////////////////////////////////////////

bool MCDRXPStatus::getDrxpSerialNumber(boost::property_tree::ptree& pt) const
{
    if (!is_valid) {
        return false;
    }
    pt.put("serialNumber", serialNumber);
    pt.put("product", product);
    return true;
}

bool MCDRXPStatus::getDrxpVersion(boost::property_tree::ptree& pt) const
{
    if (!is_valid) {
        return false;
    }

    // Decode ::driver_version it contains [<0x00>, maj, min, micro]
    pt.put("driverMajor", (driver_version >> 16) & 0xFF);
    pt.put("driverMinor", (driver_version >> 8) & 0xFF);
    pt.put("driverMicro", driver_version & 0xFF);
    pt.put("fpga", fpgaMainLogic_version);
    return true;
}

////////////////////////////////////////////////////////////////////////////////////////

// TODO: these are not yet part of the XML Schema

bool MCDRXPStatus::getOpticalPower(boost::property_tree::ptree& pt, bool jsonCompatible) const
{
    if (!is_valid) {
        return false;
    }
    pt.put("power.ch1BitD", drxpOpticalPower.ch1BitD_power_nW);
    pt.put("power.ch2BitC", drxpOpticalPower.ch2BitC_power_nW);
    pt.put("power.ch3BitB", drxpOpticalPower.ch3BitB_power_nW);
    if (!jsonCompatible) {
        pt.put("power.ch1BitD.<xmlattr>.unit", "nW");
        pt.put("power.ch2BitC.<xmlattr>.unit", "nW");
        pt.put("power.ch3BitB.<xmlattr>.unit", "nW");
    }
    return true;
}

bool MCDRXPStatus::getDrxpTemperatures(boost::property_tree::ptree& pt, bool jsonCompatible) const
{
    if (!is_valid) {
        return false;
    }
    pt.put("temperatures.fpga", drxpTemperatures.fpga);
    if (!jsonCompatible) {
        pt.put("temperatures.fpga.<xmlattr>.unit", "C");
    }
    return true;
}

////////////////////////////////////////////////////////////////////////////////////////

/** Return a summary. Uses VT-100 Terminal escape codes to color the alarms or warnings (green/yellow/red). */
std::string MCDRXPStatus::toString() const
{
    //const char* TERM_CLEAR = "\033[H\033[J";
    //const char* TERM_CLRLINE = "\033[K";
    //const char* TERM_XY = "\033[%d;%dH";
    const char* GREEN = "\033[32m";
    const char* RED = "\033[31m";
    const char* YELLOW = "\033[33m";

    #define b2color(b)      ((b) ? GREEN : RED)                  // error yes/no
    #define tri2color(w,a)  ((a) ? RED : ((w) ? YELLOW : GREEN)) // alarm(a), warning(w), or fine (!a & !w)
    #define DEFCOLOR "\033[39m"

    std::string s;
    char tmp[2048];

    snprintf(tmp, sizeof(tmp),
        DEFCOLOR "Device %s with serial number %s\n\n",
        deviceName.c_str(), 
        serialNumber.c_str()
    );
    s += tmp;

    snprintf(tmp, sizeof(tmp),
        DEFCOLOR "   FPGA supply: %s0.95V  %s1.8V  %s1.0V  %s1.2V  %s1.8V  %s3.3V  %sVddr"
        DEFCOLOR "\n",
        b2color(!dfrVoltageStatus.p0_95VForFpgaCore),
        b2color(!dfrVoltageStatus.p1_8VForFpga),
        b2color(!dfrVoltageStatus.p1_0VmgtavccForFpga),
        b2color(!dfrVoltageStatus.p1_2VmgtavttForFpga),
        b2color(!dfrVoltageStatus.p1_8VmgtvccauxForFpga),
        b2color(!dfrVoltageStatus.p3_3VForFpgaIo),
        b2color(!dfrVoltageStatus.p1_35VDdrForDdr)
    );
    s += tmp;

    snprintf(tmp, sizeof(tmp),
        DEFCOLOR "   DDR 200 MHz: %sPLL 1 %sPLL 2"
        DEFCOLOR "   REF 125 MHz: %sPLL  "
        DEFCOLOR "   Data PLL: %sCH1 %sCH2 %sCH3"
        DEFCOLOR "\n",
        b2color(!dfrStatus.ddrClock200MhzPll1Unlock),
        b2color(!dfrStatus.ddrClock200MhzPll2Unlock),
        b2color(!dfrStatus.referenceClock125MhzPllUnlock),
        b2color(!dfrStatus.dataInputClockPllUnlockCh1BitD),
        b2color(!dfrStatus.dataInputClockPllUnlockCh2BitC),
        b2color(!dfrStatus.dataInputClockPllUnlockCh3BitB)
    );
    s += tmp;

    snprintf(tmp, sizeof(tmp),
        DEFCOLOR "   Pwr 3.3V   : %sCH1 %sCH2 %sCH3"
        DEFCOLOR "   Ibias:  %sCH1 %sCH2 %sCH3"
        DEFCOLOR "  Temp.   : %sCH1 %sCH2 %sCH3 \n"
        DEFCOLOR "   RX Pwr     : %sCH1 %sCH2 %sCH3"
        DEFCOLOR "   TX Pwr: %sCH1 %sCH2 %sCH3"
        DEFCOLOR "\n",
        tri2color(powerAlarm.ch1BitD.p3_3VWarning, powerAlarm.ch1BitD.p3_3VAlarm),
        tri2color(powerAlarm.ch2BitC.p3_3VWarning, powerAlarm.ch2BitC.p3_3VAlarm),
        tri2color(powerAlarm.ch3BitB.p3_3VWarning, powerAlarm.ch3BitB.p3_3VAlarm),
        tri2color(alarmStatus.ch1BitD.outputBiasCurrentWarning, alarmStatus.ch1BitD.outputBiasCurrentAlarm),
        tri2color(alarmStatus.ch2BitC.outputBiasCurrentWarning, alarmStatus.ch2BitC.outputBiasCurrentAlarm),
        tri2color(alarmStatus.ch3BitB.outputBiasCurrentWarning, alarmStatus.ch3BitB.outputBiasCurrentAlarm),
        tri2color(alarmStatus.ch1BitD.temperatureWarning, alarmStatus.ch1BitD.temperatureAlarm),
        tri2color(alarmStatus.ch2BitC.temperatureWarning, alarmStatus.ch2BitC.temperatureAlarm),
        tri2color(alarmStatus.ch3BitB.temperatureWarning, alarmStatus.ch3BitB.temperatureAlarm),
        tri2color(alarmStatus.ch1BitD.rxInputPowerWarning, alarmStatus.ch1BitD.rxInputPowerAlarm),
        tri2color(alarmStatus.ch2BitC.rxInputPowerWarning, alarmStatus.ch2BitC.rxInputPowerAlarm),
        tri2color(alarmStatus.ch3BitB.rxInputPowerWarning, alarmStatus.ch3BitB.rxInputPowerAlarm),
        tri2color(alarmStatus.ch1BitD.outputPowerWarning, alarmStatus.ch1BitD.outputPowerAlarm),
        tri2color(alarmStatus.ch2BitC.outputPowerWarning, alarmStatus.ch2BitC.outputPowerAlarm),
        tri2color(alarmStatus.ch3BitB.outputPowerWarning, alarmStatus.ch3BitB.outputPowerAlarm)
    );
    s += tmp;

    snprintf(tmp, sizeof(tmp),
        "\n"
        DEFCOLOR "   RX Optical : %s%+.2f dBm  %s%+.2f dBm  %s%+.2f dBm"
        DEFCOLOR "\n",
        tri2color(alarmStatus.ch1BitD.rxInputPowerWarning, alarmStatus.ch1BitD.rxInputPowerAlarm),
        drxpOpticalPower.ch1BitD_power_dBm,
        tri2color(alarmStatus.ch2BitC.rxInputPowerWarning, alarmStatus.ch2BitC.rxInputPowerAlarm),
        drxpOpticalPower.ch2BitC_power_dBm,
        tri2color(alarmStatus.ch3BitB.rxInputPowerWarning, alarmStatus.ch3BitB.rxInputPowerAlarm),
        drxpOpticalPower.ch3BitB_power_dBm
    );
    s += tmp;

    snprintf(tmp, sizeof(tmp),
        DEFCOLOR "   Frame CH1 : %ssync " DEFCOLOR " offset %-6d  sync.err. %-6lu  %5.1f usec dly  csum.err. %-6lu "
        DEFCOLOR "\n",
        b2color(syncLockStatus.ch1BitD.syncUnlock),
        syncLockStatus.ch1BitD.syncLockOffset,
        syncErrorCounter.ch1BitD,
        1e-6 * metaframeDelay.ch1BitD / 200e6, // delay in counts of 200 MHz clock --> usec
        checksumCounter.ch1BitD
    );
    s += tmp;

    snprintf(tmp, sizeof(tmp),
        DEFCOLOR "   Frame CH2 : %ssync " DEFCOLOR " offset %-6d  sync.err. %-6lu  %5.1f usec dly  csum.err. %-6lu "
        DEFCOLOR "\n",
        b2color(syncLockStatus.ch2BitC.syncUnlock),
        syncLockStatus.ch2BitC.syncLockOffset,
        syncErrorCounter.ch2BitC,
        1e-6 * metaframeDelay.ch2BitC / 200e6, // delay in counts of 200 MHz clock --> usec
        checksumCounter.ch2BitC
    );
    s += tmp;

    snprintf(tmp, sizeof(tmp),
        DEFCOLOR "   Frame CH3 : %ssync " DEFCOLOR " offset %-6d  sync.err. %-6lu  %5.1f usec dly  csum.err. %-6lu "
        DEFCOLOR "\n",
        b2color(syncLockStatus.ch3BitB.syncUnlock),
        syncLockStatus.ch3BitB.syncLockOffset,
        syncErrorCounter.ch3BitB,
        1e-6 * metaframeDelay.ch3BitB / 200e6, // delay in counts of 200 MHz clock --> usec
        checksumCounter.ch3BitB
    );
    s += tmp;

    snprintf(tmp, sizeof(tmp),
        "\n"
        DEFCOLOR "   Receive %s%s" DEFCOLOR "   Transmit %s%s\n" DEFCOLOR,
        b2color(drxpStarted==0),
        (drxpStarted==0) ? "off" : "on",
        b2color(drxpSending==0),
        (drxpSending==0) ? "off" : "on"
    );
    s += tmp;

    snprintf(tmp, sizeof(tmp),
        "\n"
        DEFCOLOR "   RX %u accepts %u discards %u overruns\n",
        naccepts, ndiscards, noverruns
    );
    s += tmp;

    snprintf(tmp, sizeof(tmp),
        "\n"
        DEFCOLOR "   SEU detection: %3d err  %3d corrected  %3d uncorrected   status %d\n"
                 "   SEU time     : %d s elapsed, %d s total\n",
        fpgaSEUInfo.error, fpgaSEUInfo.corErrCnt, fpgaSEUInfo.uncorErrCnt, fpgaSEUInfo.status,
        fpgaSEUInfo.elapsedObs, fpgaSEUInfo.elapsedTot
    );
    s += tmp;

    return s;
}

////////////////////////////////////////////////////////////////////////////////////////

/** Start periodic monitoring of DRXP. This call simply wraps the call to the
    initiation of periodic monitoring via DRXPIface::startPeriodicMonitoring().
    When periodic monitoring is started this MCDRXPStatus object will auto-refresh
    its contents (alarms, counters, etc) at regular intervals. */
bool MCDRXPStatus::startPeriodicMonitoring(DRXPIface& drxp, float interval_secs, const char* multicastgroup, int multicastport)
{
    bool success = drxp.startPeriodicMonitoring(multicastgroup, multicastport, *this, interval_secs);
    return success;
}

/** Stop periodic monitoring of DRXP. This call simply wraps DRXPIface::stopPeriodicMonitoring().
    Once stopped, the this MCDRXPStatus object no longer auto-refreshes its contents.
*/
void MCDRXPStatus::stopPeriodicMonitoring(DRXPIface& drxp) const
{
    drxp.stopPeriodicMonitoring();
}
