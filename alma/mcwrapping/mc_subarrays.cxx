/**
 * \class MCSubarrays
 *
 * \brief Manages a list of subArray_t items and their spectrometerInput_t's.
 *
 * \author $Author: janw $
 *
 */

#include "mcwrapping/mc_subarrays.hxx"

#include <boost/thread/mutex.hpp>

#include <map>
#include <ostream>
#include <string>
#include <vector>

MCSubarrays::MCSubarrays()
{
    subarrays.clear();
}

MCSubarrays::~MCSubarrays()
{
}

bool MCSubarrays::hasSubarray(const XSD::subArray_t& array) const
{
    boost::unique_lock<boost::mutex> scoped_lock(mutex);
    return (subarrays.count(array) > 0);
}

bool MCSubarrays::addSubarray(const XSD::subArray_t& array)
{
    boost::unique_lock<boost::mutex> scoped_lock(mutex);
    if (subarrays.count(array) == 0) {
        subarrays.insert(std::pair<XSD::subArray_t, SpecinputSet>(array, SpecinputSet()));
        return true;
    } else {
        return false;
    }
}

bool MCSubarrays::delSubarray(const XSD::subArray_t& array)
{
    boost::unique_lock<boost::mutex> scoped_lock(mutex);
    SubarraySet::iterator it = subarrays.find(array);
    if (it != subarrays.end()) {
        subarrays.erase(it);
        return true;
    } else {
        return false;
    }
}

bool MCSubarrays::addSubarrayInputs(const XSD::subArray_t& array, std::vector<XSD::spectrometerInput_t>& newinputs)
{
    boost::unique_lock<boost::mutex> scoped_lock(mutex);
    SubarraySet::iterator it = subarrays.find(array);
    if (it != subarrays.end()) {
        it->second.insert(it->second.end(), newinputs.begin(), newinputs.end());
        return true;
    } else {
        return false;
    }
}

bool MCSubarrays::getSubarrayInputs(const XSD::subArray_t& array, std::vector<XSD::spectrometerInput_t>& inputs)
{
    boost::unique_lock<boost::mutex> scoped_lock(mutex);
    SubarraySet::iterator it = subarrays.find(array);
    if (it != subarrays.end()) {
        inputs.insert(inputs.end(), it->second.begin(), it->second.end());
        return true;
    } else {
        return false;
    }
}

std::ostream& operator<<(std::ostream &os, const MCSubarrays &a)
{
    os << "MCSubarrays: " << a.subarrays.size() << " total\n";
    MCSubarrays::SubarraySet::const_iterator it;
    for (it = a.subarrays.begin(); it != a.subarrays.end(); it++) {
        os << "   '" << it->first.name << "', inputs : ";
        MCSubarrays::SpecinputSet::const_iterator it2;
        for (it2 = it->second.begin(); it2 != it->second.end(); it2++) {
            os << it2->input << " ";
        }
        os << "\n";
    }
    return os;
}
