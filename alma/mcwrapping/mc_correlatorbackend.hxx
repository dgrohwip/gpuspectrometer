/**
 * \class MCCorrelatorBackend
 *
 * \brief Wraps an implementation of a CPU or GPU spectral correlator.
 *
 * \author $Author: janw $
 *
 */
#ifndef MC_CORRELATORBACKEND_HXX
#define MC_CORRELATORBACKEND_HXX

#include <string>
#include "datarecipients/DataRecipient_iface.hxx"
#include "spectralprocessors/SpectralProcessor_iface.hxx"
#include "xsd_datatypes/xsd_datatypes.hxx"

class ResultRecipient;
class SpectralProcessorIFace;
class DRXPInfoFrame;

class MCCorrelatorBackend : public DataRecipient
{
    public:
        MCCorrelatorBackend(bool useGPU=true);
        ~MCCorrelatorBackend();

    public:

        /** Replace delays list in case of correlation mode */
        bool replaceDelayLists(const XSD::delayListList_t& lists) {
            if (spu != NULL) {
                return spu->replaceDelayLists(lists);
            }
            return false;
        }

        /** \return Current spectral processor object */
        SpectralProcessorIFace* getCurrentProcessor() { return spu; }

        // void setCurrentProcessor(SpectralProcessorIFace*);

        /** Pass a device selection to current/future processors */
        void setAffinity(int ndevices, const int* deviceIds);

        /** Pass a device selection to current/future processors */
        void setAffinity(const std::string& devList_commaseparated);

    public:
        /** Interface inherited by SpectralProcessor from DataRecipient */
        bool takeData(RawDRXPDataGroup data);

    public:
        /** Set the recipient to be invoked by SpectralProcessor to store spectral results */
        void setSpectralRecipient(ResultRecipient* r);

    private:
        SpectralProcessorIFace* spu;
        bool m_useGPU;
        bool m_isObserving;
        int m_ndevices;
        int m_deviceIds[32];
};

#endif // MC_CORRELATORBACKEND_HXX
