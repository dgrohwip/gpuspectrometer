#include "mcwrapping/mc_subarrays.hxx"

#include <iostream>

int main(int argc, char** argv)
{
    MCSubarrays s;

    XSD::subArray_t a1("Array0001", XSD::SINGLE_DISH);
    XSD::subArray_t a2("Array0002", XSD::SINGLE_DISH);

    std::vector<XSD::spectrometerInput_t> inputs;
    inputs.push_back(XSD::spectrometerInput_t(1));
    inputs.push_back(XSD::spectrometerInput_t(4));

    std::cout << "Empty has a1 : returned " << s.hasSubarray(a1) << "\n";
    std::cout << "Add a1       : returned " << s.addSubarray(a1) << "\n";
    std::cout << "Now has a1?  : returned " << s.hasSubarray(a1) << "\n";
    std::cout << "Re-add a1    : returned " << s.addSubarray(a1) << "\n";

    std::cout << "Add a1 inputs: returned " << s.addSubarrayInputs(a1, inputs) << "\n";

    std::vector<XSD::spectrometerInput_t> foundinputs;
    std::cout << "Get a1 inputs: returned " << s.getSubarrayInputs(a1, foundinputs) << "\n";

    std::cout << "Add a2       : returned " << s.addSubarray(a2) << "\n";

    std::cout << "\nSummary of subarrays:\n" << s << "\n";

    std::cout << "Del a1       : returned " << s.delSubarray(a1) << "\n";

    std::cout << "\nSummary of subarrays:\n" << s << "\n";

    return 0;
}
