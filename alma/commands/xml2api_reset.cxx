
#include "xml2api/xml2api_factory.hxx"
#include "mcwrapping/mc_observationqueue.hxx"

#include <boost/property_tree/ptree.hpp>

XML2API_CREATE_AND_REGISTER_CMD(reset);

int MCMsgCommandResponse_reset::handle(const boost::property_tree::ptree& in, boost::property_tree::ptree& out, ASMInterface& asm_)
{
    asm_.getObservationQueue().reset();
    asm_.getConfigsMap().clear();

    out.put("response.commandId", this->getId());
    out.put("response.reset.completionCode", "success");
    out.put("response.reset.status", "initialized");

    return 0;
}
