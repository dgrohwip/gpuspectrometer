
#include "xml2api/xml2api_factory.hxx"
#include "mcwrapping/mc_observationqueue.hxx"
#include "mcwrapping/mc_spectrometerstate.hxx"

#include <string>
#include <boost/property_tree/ptree.hpp>

XML2API_CREATE_AND_REGISTER_CMD(runSelfTest);

int MCMsgCommandResponse_runSelfTest::handle(const boost::property_tree::ptree& in, boost::property_tree::ptree& out, ASMInterface& asm_)
{
    boost::property_tree::ptree dsp;

    MCSpectrometerState::OperatingMode_t level = MCSpectrometerState::NormalDataMode;
    std::string levelstr = in.get("level", "");

    if (levelstr == "simple") { level = MCSpectrometerState::SelfTestMode_Simple; }
    else if (levelstr == "thorough") { level = MCSpectrometerState::SelfTestMode_Thorough; }
    else if (levelstr == "endurance") { level = MCSpectrometerState::SelfTestMode_Endurance; }

    out.put("response.commandId", this->getId());
    if (level == MCSpectrometerState::NormalDataMode) {
        out.put("response.runSelfTest.completionCode", "invalid-xml");
        return -1;
    }

// TODO: dispatch the self-test!

    //bool rc = g.gMC---.dispatchSelfTest(level);
    //if (!rc) {
    //    out.put("response.runSelfTest.completionCode", "multiple-activation");
    //    return -1;
    //}

    if (level == MCSpectrometerState::SelfTestMode_Simple) {
        //rc = g.gMC---.joinSelfTest();
        // TODO: grab the short-term self test results and return them
        out.put("response.runSelfTest.completionCode", "success");
        out.put("response.results", "TBD in the XSD / Schema");
    } else {
        out.put("response.runSelfTest.completionCode", "success");
    }

    return 0;
}
