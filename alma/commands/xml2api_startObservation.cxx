/**
 * \class MCMsgCommandResponse_startObservation
 *
 * \brief Translates an XML observation task received by M&C command.
 *
 * \author $Author: janw $
 *
 */

#include "time/TimeUTC.hxx"
#include "xml2api/xml2api_factory.hxx"
#include "mcwrapping/mc_observation.hxx"
#include "mcwrapping/mc_observationqueue.hxx"

#include <boost/property_tree/ptree.hpp>

XML2API_CREATE_AND_REGISTER_CMD(startObservation);

int MCMsgCommandResponse_startObservation::handle(const boost::property_tree::ptree& in, boost::property_tree::ptree& out, ASMInterface& asm_)
{
    out.put("response.commandId", this->getId());

    // Check config
    unsigned int configId = in.get("configId", 0);
    if (asm_.getConfigsMap().find(configId) == asm_.getConfigsMap().end()) {
        out.put("response.startObservation.completionCode", "non-existing-configId");
        return -1;
    }

    // Assemble an observation entry
    MCObservation obs;
    obs.cfg = asm_.getConfigsMap().at(configId); // stores a copy into 'cfg'
    obs.state = MCObservation::ObsUninitialized;
    obs.startTimeUTC = in.get("time", "");
    obs.stopTimeUTC = ""; // unkown at this stage
    obs.observationId = in.get("observationId", "");
    obs.updateDerivedParams();

    // Check the obs is unique and valid
    if (!asm_.getObservationQueue().isObservationIdUnique(obs.observationId)) {
        out.put("response.startObservation.completionCode", "duplicated-observationId");
        return -1;
    }
    if (!obs.cfg.valid()) {
        out.put("response.startObservation.completionCode", "non-existing-configId");
        std::cerr << "Error: startObservation() configuration with Id " << configId << " exists but is marked invalid.\n";
        return -1;
    }

    // Check if too late to start
    double min_leadtime = asm_.getUserConfig().get<double>("operation.startObsMinLeadtime");
    double actual_leadtime = TimeUTC::getSecondsSince(obs.startTimeUTC);
    if (actual_leadtime > -min_leadtime) {
        // std::cerr << "DEBUG: startObs has actual_leadtime=" << actual_leadtime << " vs min_leadtime=" << min_leadtime << std::endl;
        out.put("response.startObservation.completionCode", "insufficient-lead-time");
        return -1;
    }

    // Enqueue
    if (!asm_.getObservationQueue().enqueueObservation(obs)) {
        // TODO: when adding fails, what to return over XML?
        std::cerr << "Warning: could not enqueueObservation()\n";
        out.put("response.startObservation.completionCode", "duplicated-observationId");
        return -1;
    }

    out.put("response.startObservation.completionCode", "success");

    return 0;
}
