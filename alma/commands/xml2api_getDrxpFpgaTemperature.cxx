
#include "xml2api/xml2api_factory.hxx"
#include "mcwrapping/mc_drxp_status.hxx"
#include "xsd_datatypes/xsd_datatypes.hxx"

#include <boost/property_tree/ptree.hpp>

XML2API_CREATE_AND_REGISTER_CMD(getDrxpFpgaTemperature);

int MCMsgCommandResponse_getDrxpFpgaTemperature::handle(const boost::property_tree::ptree& in, boost::property_tree::ptree& out, ASMInterface& asm_)
{
    MCDRXPStatus ds;
    boost::property_tree::ptree dsp;

    XSD::drxpInterfaceId_t iface;
    iface.deviceId = in.get("deviceId", "00:00");
    iface.InterfaceNumber = in.get("InterfaceNumber", 1);
    if (!asm_.getDRXPLookup().isPresent(iface)) {
        L_(lerror) << "getDrxpSerialNumber for non-existend device " << iface.deviceId << " but M&C XSD defines no error code for this case.";
    }
    std::string charDev = asm_.getDRXPLookup().getCharDevice(iface);

    // TODO: actually use charDev!

    //asm_.getDRXP()->deviceReadout(ds);
    asm_.getDRXP()->temperaturesReadout(ds);
    ds.getDrxpTemperatures(dsp);

    out.put("response.commandId", this->getId());
    out.put("response.getDrxpFpgaTemperature.completionCode", "success");
    out.add_child("response.getDrxpFpgaTemperature.temperature", dsp.get_child("temperatures.fpga"));

    return 0;
}
