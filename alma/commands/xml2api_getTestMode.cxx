
#include "xml2api/xml2api_factory.hxx"

#include <boost/property_tree/ptree.hpp>

XML2API_CREATE_AND_REGISTER_CMD(getTestMode);

int MCMsgCommandResponse_getTestMode::handle(const boost::property_tree::ptree& in, boost::property_tree::ptree& out, ASMInterface& asm_)
{
    bool testMode = false;

    // TODO: invoke spectrometer API

    out.put("response.commandId", this->getId());
    out.put("response.getTestMode.completionCode", "success");
    out.put("response.getTestMode.testMode", testMode ? "true" : "false");

    return 0;
}
