
#include "xml2api/xml2api_factory.hxx"
#include "mcwrapping/mc_drxp_status.hxx"
#include "xsd_datatypes/xsd_datatypes.hxx"

#include <boost/property_tree/ptree.hpp>

XML2API_CREATE_AND_REGISTER_CMD(getDrxpSerialNumber);

int MCMsgCommandResponse_getDrxpSerialNumber::handle(const boost::property_tree::ptree& in, boost::property_tree::ptree& out, ASMInterface& asm_)
{
    MCDRXPStatus ds;
    boost::property_tree::ptree dsp;

    XSD::drxpInterfaceId_t iface;
    iface.deviceId = in.get("", "00:00");
    iface.InterfaceNumber = 1; // unspecified by M&C XSD!
    if (!asm_.getDRXPLookup().isPresent(iface)) {
        L_(lerror) << "getDrxpSerialNumber for non-existend device " << iface.deviceId << " but M&C XSD defines no error code for this case.";
    }
    std::string charDev = asm_.getDRXPLookup().getCharDevice(iface);

    // TODO: actually use charDev!

    asm_.getDRXP()->versionReadout(ds);
    ds.getDrxpSerialNumber(dsp);

    out.put("response.commandId", this->getId());
    out.put("response.getDrxpSerialNumber.completionCode", "success");
    out.add_child("response.getDrxpSerialNumber.drxpSerialNumber", dsp);

    return 0;
}
