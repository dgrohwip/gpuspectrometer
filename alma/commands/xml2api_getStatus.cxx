/**
 * \class MCMsgCommandResponse_getStatus
 *
 * \brief Translates an XML configuration received by M&C command.
 *
 * \author $Author: janw $
 *
 */

#include "xml2api/xml2api_factory.hxx"
#include "mcwrapping/mc_spectrometerstate.hxx"

#include <boost/property_tree/ptree.hpp>

XML2API_CREATE_AND_REGISTER_CMD(getStatus);

int MCMsgCommandResponse_getStatus::handle(const boost::property_tree::ptree& in, boost::property_tree::ptree& out, ASMInterface& asm_)
{
    out.put("response.commandId", this->getId());
    out.put("response.getStatus.completionCode", "success");
    asm_.getStateContainer().getStatusAll(out.get_child("response.getStatus"));
    return 0;
}
