/**
 * \class MCMsgCommandResponse_setWvrCoeff
 *
 * \brief Translates an XML configuration received by M&C command.
 *
 * \author $Author: janw $
 *
 */

#include "xml2api/xml2api_factory.hxx"
#include "xsd_datatypes/xsd_datatypes.hxx"

#include <boost/property_tree/ptree.hpp>

XML2API_CREATE_AND_REGISTER_CMD(setWvrCoeff);

int MCMsgCommandResponse_setWvrCoeff::handle(const boost::property_tree::ptree& in, boost::property_tree::ptree& out, ASMInterface& asm_)
{
    using boost::property_tree::ptree;

    XSD::wvrCoeff_t coeff;
    coeff.get(in);

    // TODO: stick the resulting wvrCoeff_t somewhere (asm_.getBackend().setWVR....)
    asm_.getCorrelationSettings().wvrCoeffs = coeff;

    out.put("response.commandId", this->getId());
    out.put("response.setWvrCoeff.completionCode", "success");

    return 0;
}
