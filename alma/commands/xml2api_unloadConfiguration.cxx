/**
 * \class MCMsgCommandResponse_loadConfiguration
 *
 * \brief Translates an XML configuration received by M&C command.
 *
 * \author $Author: janw $
 *
 */

#include "xml2api/xml2api_factory.hxx"
#include "mcwrapping/mc_configuration.hxx"

#include <boost/property_tree/ptree.hpp>

XML2API_CREATE_AND_REGISTER_CMD(unloadConfiguration);

int MCMsgCommandResponse_unloadConfiguration::handle(const boost::property_tree::ptree& in, boost::property_tree::ptree& out, ASMInterface& asm_)
{
    out.put("response.commandId", this->getId());

    unsigned int configId = in.get("configId", -1);
    MCConfigurationsMap_t::iterator it = asm_.getConfigsMap().find(configId);

    if (it == asm_.getConfigsMap().end()) {
        out.put("response.unloadConfiguration.completionCode", "non-existing-configId");
    } else {
        asm_.getConfigsMap().erase(it);
        out.put("response.unloadConfiguration.completionCode", "success");
    }

    return 0;
}
