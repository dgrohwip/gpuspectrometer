/**
 * \class MCMsgCommandResponse_setDelayList
 *
 * \brief Translates an XML configuration received by M&C command.
 *
 * \author $Author: janw $
 *
 */

#include "xml2api/xml2api_factory.hxx"
#include "xsd_datatypes/xsd_datatypes.hxx"

#include <cstdlib>
#include <sstream>
#include <string>
#include <vector>

#include <boost/foreach.hpp>
#include <boost/property_tree/ptree.hpp>

XML2API_CREATE_AND_REGISTER_CMD(setDelayList);

int MCMsgCommandResponse_setDelayList::handle(const boost::property_tree::ptree& in, boost::property_tree::ptree& out, ASMInterface& asm_)
{
    using boost::property_tree::ptree;

    XSD::delayList_t delaylist;
    delaylist.get(in);

    // Append list to already stored lists
    asm_.getCorrelationSettings().delayLists.push_back(delaylist);

    // Update backend
    asm_.getBackend().replaceDelayLists(asm_.getCorrelationSettings().delayLists);

    // TODO:
    // temporal-overlap
    // insufficient-lead-time

    out.put("response.commandId", this->getId());
    out.put("response.setDelayList.completionCode", "success");

    return 0;
}
