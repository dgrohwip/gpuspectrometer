
#include "xml2api/xml2api_factory.hxx"
#include <boost/property_tree/ptree.hpp>

// Register new M&C command handler for ICD command name 'newCommand'
XML2API_CREATE_AND_REGISTER_CMD(newCommand);

// Implement hander for the M&C command
int MCMsgCommandResponse_newCommand::handle(const boost::property_tree::ptree& in, boost::property_tree::ptree& out, ASMInterface& asm_)
{
    // Do something with args in 'in'
    // Store results 'out'
    out.put("response.commandId", this->getId());
    out.put("response.getStatus.completionCode", "success");
    return 0;
}
