/**
 * \class MCMsgCommandResponse_getAntennaParameters
 *
 * \brief Translates an XML configuration received by M&C command.
 *
 * \author $Author: janw $
 *
 */

#include "xml2api/xml2api_factory.hxx"

#include <boost/property_tree/ptree.hpp>

XML2API_CREATE_AND_REGISTER_CMD(getAntennaParameters);

int MCMsgCommandResponse_getAntennaParameters::handle(const boost::property_tree::ptree& in, boost::property_tree::ptree& out, ASMInterface& asm_)
{
    using boost::property_tree::ptree;

    out.put("response.commandId", this->getId());
    out.put("response.getAntennaParameters.completionCode", "success");

    // Note: unclear which spectrometerInput is meant.
    // For now just return the settings of the previous setAntennaParameters.

    ptree resp;
    asm_.getCorrelationSettings().antennaparametersRecent.put(resp);
    out.add_child("response.getAntennaParameters.antennaParameters", resp);

    return 0;
}
