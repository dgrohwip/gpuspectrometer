/**
 * \class MCMsgCommandResponse_setSubArray
 *
 * \brief Translates an XML configuration received by M&C command.
 *
 * \author $Author: janw $
 *
 */

#include "xml2api/xml2api_factory.hxx"
#include "xsd_datatypes/xsd_datatypes.hxx"

#include <vector>
#include <boost/foreach.hpp>
#include <boost/property_tree/ptree.hpp>

XML2API_CREATE_AND_REGISTER_CMD(setSubArray);

int MCMsgCommandResponse_setSubArray::handle(const boost::property_tree::ptree& in, boost::property_tree::ptree& out, ASMInterface& asm_)
{
    using boost::property_tree::ptree;
    bool success = true;

    // subArray field
    ptree p = in.get_child("subArray");
    XSD::subArray_t subarray;
    subarray.get(p);

    if (asm_.getSubarrays().hasSubarray(subarray)) {
        out.put("response.commandId", this->getId());
        out.put("response.setSubArray.completionCode", "duplicated-subArrayId");
        return 0;
    }

    // SI field(s): minOccurs="1" maxOccurs="4"
    std::vector<XSD::spectrometerInput_t> inputs;
    BOOST_FOREACH(const ptree::value_type &v, in.get_child("")) {
        if (v.first != "SI") { continue; }
        XSD::spectrometerInput_t input(v.second.get_value((int)1));
        inputs.push_back(input);
    }

    // Wrong number of args or incorrect field values
    success &= subarray.validate();
    success &= (inputs.size() >= 1 && inputs.size() <= 4);
    if (!success) {
        out.put("response.commandId", this->getId());
        out.put("response.setSubArray.completionCode", "not-supported");
        return 0;
    }

    // Finally, add
    asm_.getSubarrays().addSubarray(subarray);
    asm_.getSubarrays().addSubarrayInputs(subarray, inputs);
    out.put("response.commandId", this->getId());
    out.put("response.setSubArray.completionCode", "success");

    return 0;
}
