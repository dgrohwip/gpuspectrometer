
#include "xml2api/xml2api_factory.hxx"

#include <boost/property_tree/ptree.hpp>

XML2API_CREATE_AND_REGISTER_CMD(getCurrentObservation);

int MCMsgCommandResponse_getCurrentObservation::handle(const boost::property_tree::ptree& in, boost::property_tree::ptree& out, ASMInterface& asm_)
{
    out.put("response.commandId", this->getId());
    out.put("response.getCurrentObservation.completionCode", "success");

    asm_.getObservationQueue().getCurrentObservations(out.get_child("response.getCurrentObservation"), 1); // adds one or more .observation entries

    return 0;
}
