/**
 * \class MCMsgCommandResponse_setAntennaParameters
 *
 * \brief Translates an XML configuration received by M&C command.
 *
 * \author $Author: janw $
 *
 */

#include "xml2api/xml2api_factory.hxx"
#include "xsd_datatypes/xsd_datatypes.hxx"

#include <boost/property_tree/ptree.hpp>

XML2API_CREATE_AND_REGISTER_CMD(setAntennaParameters);

int MCMsgCommandResponse_setAntennaParameters::handle(const boost::property_tree::ptree& in, boost::property_tree::ptree& out, ASMInterface& asm_)
{
    boost::property_tree::ptree pt;

    XSD::antennaParameters_t ap;
    ap.get(in);

    // TODO: better storage place...
    asm_.getCorrelationSettings().antennaparameters[ap.spectrometerInput.input] = ap;
    asm_.getCorrelationSettings().antennaparametersRecent = ap;

    out.put("response.commandId", this->getId());
    out.put("response.setAntennaParameters.completionCode", "success");

    return 0;
}
