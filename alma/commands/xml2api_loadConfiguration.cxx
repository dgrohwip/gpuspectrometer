/**
 * \class MCMsgCommandResponse_loadConfiguration
 *
 * \brief Translates an XML configuration received by M&C command.
 *
 * \author $Author: janw $
 *
 */

#include "xml2api/xml2api_factory.hxx"
#include "mcwrapping/mc_configuration.hxx"

#include <boost/property_tree/ptree.hpp>

XML2API_CREATE_AND_REGISTER_CMD(loadConfiguration);

int MCMsgCommandResponse_loadConfiguration::handle(const boost::property_tree::ptree& in, boost::property_tree::ptree& out, ASMInterface& asm_)
{
    MCConfiguration cfg(in.get_child("singleConfiguration"));
    cfg.setId(in.get("configId", -1));

    // config ID is duplicate? --> duplicated-configId
    out.put("response.commandId", this->getId());
    if (asm_.getConfigsMap().find(cfg.getId()) != asm_.getConfigsMap().end()) {
        out.put("response.loadConfiguration.completionCode", "duplicated-configId");
        return -1;
    } else if (!cfg.valid()) {
        out.put("response.loadConfiguration.completionCode", "invalid-xml");
        return -1;
    } else {
        asm_.getConfigsMap().insert( std::pair<unsigned int,MCConfiguration>(cfg.getId(),cfg) ); // TODO: copy operator?
        out.put("response.loadConfiguration.completionCode", "success");
    }

    return 0;
}
