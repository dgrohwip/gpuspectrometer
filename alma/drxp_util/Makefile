
include ../Makefile.inc

DEBUG_FLAGS :=

##############################################################################################
## Sources for test programs
##############################################################################################

AUX_SRC  = ../mcwrapping/mc_drxp_status.cxx
AUX_SRC += ../mcwrapping/mc_observation.cxx
AUX_SRC += ../mcwrapping/mc_configuration.cxx 
AUX_SRC += ../time/TimeUTC.cxx
AUX_SRC += ../datarecipients/SpectralResultSet.cxx
AUX_SRC += ../datarecipients/SpectralResultSetManager.cxx
AUX_SRC += ../datarecipients/ResultRecipientPlain.cxx
AUX_SRC += ../spectralprocessors/SpectralProcessorCPU.cxx ../spectralprocessors/SpectralPostprocessor.cxx
AUX_SRC += ../spectralprocessors/SpectralAverager.cxx ../calibrations/CalibNonlinearity.cxx

AUX_OBJS := $(AUX_SRC:.cxx=.o)

DRXP_SRC = ../drxp/DRXPDummy.cxx ../drxp/DRXPInfoFrame.cxx ../drxp/SFF8431_I2C.cxx ../mcwrapping/mc_drxp_status.cxx

DRXP_OBJS := $(DRXP_SRC:.cxx=.o)

XSDLIB = ../xsd_datatypes/xsd_datatypes.a

##############################################################################################
## Target test programs
##############################################################################################

TARGETS =

ifeq ($(HAVE_DRXP),1)
	TARGETS += drxpTransmit
	DRXP_OBJS += ../drxp/DRXP.o
endif

ifeq ($(HAVE_GPU),1)
	AUX_OBJS += ../spectralprocessors/SpectralProcessorGPU.o
endif

ifeq ($(HAVE_DRXP)$(HAVE_GPU),11)
	TARGETS += drxpMiniCorr
	TARGETS += drxpDataChecker
	TARGETS += test_drxpCuPinning
#	DEBUG_FLAGS += -DGPU_DEBUG
endif

all: $(TARGETS)

try: $(TRY_TARGETS)

test_drxp_cudaHostRegisterIoMemory: test_drxp_cudaHostRegisterIoMemory.cu
	$(NVCC) $(NVFLAGS) $(DEBUG_FLAGS) -g $^ -o $@ $(ALMA_LIBS)
	#sudo setcap cap_sys_admin=+ep $@

test_drxpCuPinning: test_drxpCuPinning.cu ../drxp/DRXPInfoFrame.cxx
	$(NVCC) $(NVFLAGS) -g $^ -o $@ $(ALMA_LIBS)

drxpTransmit: drxpTransmit.cxx $(DRXP_OBJS)
	$(CXX) $(ALMA_CXXFLAGS) -g $^ -o $@ $(BOOSTLIB) $(XSDLIB)

drxpDataChecker: drxpDataChecker.cu $(DRXP_OBJS)
	$(NVCC) $(NVFLAGS) -g $^ -o $@ $(BOOSTLIB) $(XSDLIB)

drxpMiniCorr: drxpMiniCorr.cxx $(AUX_OBJS) $(DRXP_OBJS)
	$(NVCC) $(NVFLAGS) $(DEBUG_FLAGS) -g $^ -o $@ $(ALMA_LIBS)

clean:
	rm -rf $(TARGETS) *.o *.co

install: $(TARGETS)
	mkdir -p $(INSTALL_PREFIX)/alma/bin/
	cp -af $(TARGETS) $(INSTALL_PREFIX)/alma/bin/

.PHONY: clean install all try

