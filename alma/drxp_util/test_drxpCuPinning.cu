
#include "drxpd.h" // DRXP kernel module user-space API; header is part of Elecs' source code package
#undef MAX_SIZE
#include "drxp/drxp_const.hxx"
#include "drxp/drxp_bitfields.hxx"
#include "drxp/drxp_ioctl.hxx"

#include "drxp/DRXPInfoFrame.hxx"
#include "drxp/SFF8431_infos.hxx"
#include "kernels/cuda_utils.cu"

//#define DRXP_REG_DEBUGPRINT
const int C_num_groups = 16; // 16, 32, or 64 are allowed

#include <iomanip>
#include <cmath>
#include <cstring>
#include <sstream>
#include <string>
#include <iostream>
#include <fstream>

#include <sys/ioctl.h>
#include <sys/eventfd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <errno.h>
#include <fcntl.h>
#include <malloc.h>
#include <stdint.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

//////////////////////////////////////////////////////////////////////////////////////////////////////

// Helper macros, somewhat ugly

#ifdef DRXP_REG_DEBUGPRINT
    static void print_mreg(const ST_DRXPD_MONDAT& mon);
    static void print_creg(const ST_DRXPD_CONTDAT& ctl);
    #define READ_MON_REG_DbgPrint(x) print_mreg(x)
    #define WRITE_CTL_REG_DbgPrint(x) print_creg(x)
#else
    #define READ_MON_REG_DbgPrint(x)
    #define WRITE_CTL_REG_DbgPrint(x)
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////////

/** Convert integer to string */
static std::string int_to_stdstr(int64_t x)
{
    return dynamic_cast<std::ostringstream &>((std::ostringstream() << std::dec << x)).str();
}

/** Convert a byte array into 32-bit unsigned int, MSB first at index 0 of the array. */
static uint64_t byteArr4_to_uint32(const unsigned char* msb_first_raw)
{
    uint32_t out = msb_first_raw[0];
    for (int i=1; i<4; i++) {
        out = 256UL*out + msb_first_raw[i];
    }
    return out;
}

#ifdef DRXP_REG_DEBUGPRINT

/** Print the contents of ST_DRXPD_MONDAT */
static void print_mreg(const ST_DRXPD_MONDAT& mon)
{
    printf("mreg: id=%04X, bytes[0..7]=[%02X %02X %02X %02X | %02X %02X %02X %02X]\n",
        mon.ItemID,
        mon.RdValue[0], mon.RdValue[1], mon.RdValue[2], mon.RdValue[3],
        mon.RdValue[4], mon.RdValue[5], mon.RdValue[6], mon.RdValue[7]
    );
}

/** Print the contents of ST_DRXPD_CONTDAT */
static void print_creg(const ST_DRXPD_CONTDAT& ctl)
{
    printf("creg: id=%04X, bytes[0..7]=[%02X %02X %02X %02X | %02X %02X %02X %02X]\n",
        ctl.ItemID,
        ctl.WrValue[0], ctl.WrValue[1], ctl.WrValue[2], ctl.WrValue[3],
        ctl.WrValue[4], ctl.WrValue[5], ctl.WrValue[6], ctl.WrValue[7]
    );
}

#endif

//////////////////////////////////////////////////////////////////////////////////////////////////////

int m_fd;
int m_evtfd;
int m_noverruns = 0;
int m_naccepts = 0;

char* m_simpattern = NULL;
char* m_maddr = NULL;
size_t m_maddr_size = 0;

cudaEvent_t m_kstart;
cudaEvent_t m_kstop;
cudaStream_t m_streams[2];

void* d_data;
void* h_data_pinned;

void* d_spectra;
void* h_spectra;
const size_t C_spectra_length = 512*1024*4 * 4;

//////////////////////////////////////////////////////////////////////////////////////////////////////

void clrscreen()
{
    std::cout << "\033[2J" << std::endl;
    //std::cout << "\033[0;0";
}

int openDrxp(std::string m_dev)
{
    m_fd = open(m_dev.c_str(), O_RDONLY);
    //m_fd = open(m_dev.c_str(), O_RDWR); // fails -- would be needed though for CUDA!
    if (m_fd < 0) {
        std::cout << "Failed to open " << m_dev << " for DRXP readout\n";
        return -1;
    }

    m_simpattern = (char*)memalign(4096, DRXP_RX_DATA_SIZE+DRXP_RX_IFRAME_SIZE);

    // Set up groups
    DRXP_IOCTL(m_fd, DRXPD_RCVGROUPS, C_num_groups); // TODO: always DRXP_NUM_GROUPS_DEFAULT gives errno=1 'Operation not permitted' !?

    // Prepare memory mapping in advance of any use
    m_maddr_size = DRXP_RX_GROUP_SIZE * DRXP_NUM_GROUPS_DEFAULT;
    //m_maddr = (char*)mmap(NULL, m_maddr_size, PROT_READ|PROT_WRITE, MAP_SHARED, m_fd, 0); // would need O_RDWR
    m_maddr = (char*)mmap(NULL, m_maddr_size, PROT_READ, MAP_SHARED, m_fd, 0);
    if (m_maddr == (void*)-1) {
        std::cout << __func__ << " mmap error on " << m_dev << ", errno=" << errno << ", " << strerror(errno) << std::dec << "\n";
        return -1;
    }

    // Master reset
    ST_DRXPD_CONTDAT ctl;
    memset(&ctl, 0x00, sizeof(ctl));
    ctl.WrValue[0] = 1<<7;  // DFR_RST : bit7: Master Reset, bit0: clear Status and Voltage Status registers
    WRITE_CTL_REG(DFR_RST_ID_ALL, ctl);
    std::cout << "Requested a full reset of " << m_dev << "\n";

    // Elecs recommends a ~1 second wait after master reset. Sleep 1.5 seconds.
    if (1) {
        struct timespec req={1,(long)0.5e9}, rem={0,0};
        do {
            if (nanosleep(&req, &rem) != 0) { break; }
            req = rem;
        } while ((rem.tv_sec + 1e-9*rem.tv_nsec) > 10e-3);
    }

    // Reset individual: checksums, sync statuses, status and voltage warnings/alarms
    memset(&ctl, 0x00, sizeof(ctl));
    WRITE_CTL_REG(RST_DFR_CHK_ID_ALL, ctl);
    memset(&ctl, 0x00, sizeof(ctl));
    WRITE_CTL_REG(RST_DFR_SYNC_ID_ALL, ctl);
    memset(&ctl, 0x00, sizeof(ctl));
    ctl.WrValue[0] = 0x01; // DFR_RST : bit7: Master Reset, bit0: clear Status and Voltage Status registers
    WRITE_CTL_REG(DFR_RST_ID_ALL, ctl);

    // Do an additional but shorter wait after clearing status registers
    if (1) {
        struct timespec req={0,(long)0.5e9}, rem={0,0};
        do {
            if (nanosleep(&req, &rem) != 0) { break; }
            req = rem;
        } while ((rem.tv_sec + 1e-9*rem.tv_nsec) > 10e-3);
    }

    // Check version
    dfr_mon_reg_t mreg;
    mreg.mon.ItemID = GET_DFR_SN_ID;
    ioctl(m_fd, DRXPD_GET_MONSTS, &mreg.mon);
    std::string serialNumber = int_to_stdstr(byteArr4_to_uint32(mreg.mon.RdValue) >> 8);
    std::cout << "Device " << m_dev << " has serial nr " << serialNumber << ", product=" << ((mreg.mon.RdValue[3] & 1) != 0) << "\n";

    mreg.mon.ItemID = GET_DFR_VER_ID;
    ioctl(m_fd, DRXPD_GET_MONSTS, &mreg.mon);
    unsigned int driver_version = byteArr4_to_uint32(mreg.mon.RdValue) >> 8; // maj, min, micro, <reserved 0x00>
    unsigned int fpgaMainLogic_version = byteArr4_to_uint32(mreg.mon.RdValue + 4);
    std::cout << "Device " << m_dev << " has driver ver=" << driver_version << ", logic ver=" << fpgaMainLogic_version << "\n";

    return 0;
}

int startDrxpRx()
{
    int ioctl_result = 0;

    // Prepare EventFD
    m_evtfd = eventfd(0, 0);
    DRXP_IOCTL(m_fd, DRXPD_SET_RCVEVT, m_evtfd);

    // Set up reception
    DRXP_IOCTL_IPTR(m_fd, DRXPD_INIT, &ioctl_result);
    DRXP_IOCTL_IPTR(m_fd, DRXPD_RCVEVTSTART, &ioctl_result);
    uint64_t evt;
    if (read(m_evtfd, &evt, sizeof(uint64_t)) < 0) {
        std::cout << __func__ << "() failed to read from event-fd (errno=" << errno << "), cannot start data reception";
        return -1;
    }

    // Start reception
    DRXP_IOCTL_IPTR(m_fd, DRXPD_RCVSTART, &ioctl_result);

    return 0;
}

/** Signal the RX data pump to stop and wait until it finishes */
int closeDrxp()
{
    int ioctl_result = 0;
    DRXP_IOCTL_IPTR(m_fd, DRXPD_RCVSTOP, &ioctl_result);
    DRXP_IOCTL_IPTR(m_fd, DRXPD_RCVEVTSTOP, &ioctl_result);
    // note: the v1.0.7 driver documentation provides this order (stop first, then evtstop last)

    close(m_evtfd);
    close(m_fd);
    return 0;
}

int getDrxpSlot(void** slotData_ptr)
{
    DRXPInfoFrame info;
    ST_DRXPD_RCVSTS rxstat;
    uint64_t evt;

    while (1) {

        int ready_grp = 0;

        // Blocking wait until new data available (or other event)
        if (read(m_evtfd, &evt, sizeof(uint64_t)) < 0) {
            std::cout << __func__ << " event-fd read failed, stopping datapump\n";
            break;
        }

        // Get info about groups
        DRXP_IOCTL_PTR(m_fd, DRXPD_GET_RCVSTS, &rxstat);
        if (rxstat.State == DRXPD_STOP) {
            std::cout << __func__ << " found DRXP unexpectedly in state DRXPD_STOP!\n";
            continue;
        }

        // Locate the next ready group
        for (int n = 0; n < rxstat.GroupNum; n++) {
            if ((rxstat.Group[ready_grp] == DRXPD_COMPLETE) || (rxstat.Group[ready_grp] == DRXPD_OVERRUN)) {
                break;
            }
            ready_grp = (ready_grp + 1) %  rxstat.GroupNum;
        }
        if (!(rxstat.Group[ready_grp] == DRXPD_COMPLETE) && !(rxstat.Group[ready_grp] == DRXPD_OVERRUN)) {
            //std::cout << "Unexpectedly no ready data yet according to DRXPD_GET_RCVSTS\n";
            continue;
        }

        // Keep count of overruns
        if (rxstat.Group[ready_grp] == DRXPD_OVERRUN) {
            m_noverruns++;
        }

        // Get Info Element field (48byte) trailing the data group payload
        size_t off = ready_grp*DRXP_RX_GROUP_SIZE + DRXP_RX_DATA_SIZE;
        if ((off + DRXP_RX_IFRAME_SIZE) >= m_maddr_size) {
            std::cout << __func__ << " oops, group " << ready_grp << " info frame offset " << off << " plus 48B falls outsize size of " << m_maddr_size << "\n";
        } else {
            info.fromByteArray((unsigned char*)m_maddr + off);
        }

        //clrscreen();

        std::cout   << "Found group " << std::dec << std::setw(2) << ready_grp << " with data; "
                    << "48ms tick " << info.hwTickCount_48ms << ", " << std::hex << info.frameStatus << std::dec
                    << ", tstamp " << std::fixed << std::setprecision(6) << (info.swRxTime_UTC.tv_sec + info.swRxTime_UTC.tv_nsec*1e-9) << "\n";

        if (0) {
            // Print out group statuses
            std::stringstream ss;
            ss << "DRXPD_GET_RCVSTS: ";
            ss << "rx is " << ((rxstat.State==DRXPD_RUN) ? "running" : "stopped");
            ss << ", ngroups=" << rxstat.GroupNum << ", statuses are:";
            for (int n = 0; n < rxstat.GroupNum; n++) { // GroupNum means really "NumGroups"...
                if ((n % 4) == 0) {
                    ss << "\n   ";
                }
                ss << " n[" << n << "]:" << rxGroupState2Str(rxstat.Group[n]);
            }
            std::cout << ss.str() << "\n";
        } else {
            int nover=0, nready=0, nfree=0, nbusy=0;
            for (int n = 0; n < rxstat.GroupNum; n++) {
                if (rxstat.Group[n] == DRXPD_OVERRUN) { nover++; }
                else if (rxstat.Group[n] == DRXPD_COMPLETE) { nready++; }
                else if (rxstat.Group[n] == DRXPD_IDLE) { nfree++; }
                else if (rxstat.Group[n] == DRXPD_IDLE_WR) { nbusy++; }
                else if (rxstat.Group[n] == DRXPD_OVERRUN_WR) { nbusy++; }
            }
            std::cout << "DRXPD_GET_RCVSTS: nfree=" << nfree << " navail=" << nready << " nbusy=" << nbusy << " noverf=" << nover << " : total accepted=" << m_naccepts << "\n";
        }

        // Return to caller
        if (slotData_ptr != NULL) {
            *slotData_ptr = ((char*)m_maddr) + ((size_t)ready_grp)*DRXP_RX_GROUP_SIZE;
        }
        m_naccepts++;
        return ready_grp;
    }

    return -1;
}

int releaseDrxpSlot(int ready_grp)
{
    if (ready_grp >= 0) {
        DRXP_IOCTL(m_fd, DRXPD_CLR_RCV, ready_grp);
    }
    std::cout << "Released group " << std::setw(2) << ready_grp << "\n";
    return 0;
}

int doGpu(void* h_src, size_t h_src_len, bool copyToPinned=false)
{
    float dT_msec = 0.0f;
    float dT_wallclock_msec = 0.0f;
    struct timeval t1, t2;

    gettimeofday(&t1, NULL);
    CUDA_CALL ( cudaEventRecord(m_kstart) );
#if 0
    if (copyToPinned) {
        CUDA_CALL ( cudaMemcpy(h_data_pinned, h_src, h_src_len, cudaMemcpyHostToHost) );
        h_src = h_data_pinned;
    }
    CUDA_CALL ( cudaMemcpy(d_data, h_src, h_src_len, cudaMemcpyHostToDevice) );
#else
    if (copyToPinned) {
        CUDA_CALL ( cudaMemcpyAsync(h_data_pinned, h_src, h_src_len, cudaMemcpyHostToHost, m_streams[0]) );
        h_src = h_data_pinned;
    }
    CUDA_CALL ( cudaMemcpyAsync(d_data, h_src, h_src_len, cudaMemcpyHostToDevice, m_streams[0]) );
    CUDA_CALL ( cudaMemcpyAsync(h_spectra, d_spectra, C_spectra_length, cudaMemcpyDeviceToHost, m_streams[1]) );
#endif
    CUDA_CALL ( cudaEventRecord(m_kstop) );
    CUDA_CALL ( cudaEventSynchronize(m_kstop) );
    CUDA_CALL ( cudaEventElapsedTime(&dT_msec, m_kstart, m_kstop) );
    gettimeofday(&t2, NULL);

    dT_wallclock_msec = 1e3*( (t2.tv_sec - t1.tv_sec) + 1e-6*(t2.tv_usec - t1.tv_usec) );

    float R_GBps = (double(h_src_len) / (dT_msec*1e-3)) / 1073741824.0;
    float R_Gsps = ((R_GBps*8)/3) / 2;
    std::cout << "doGpu() : " << std::fixed << std::setprecision(2) <<  dT_msec << " msec GPU, " << dT_wallclock_msec << " msec CPU, "
              << std::setw(6) << R_GBps << " GByte/sec, "
              << std::setw(6) << R_Gsps << " Gsamp/sec/dualpol\n";

    return 0;
}

int main(int argc, char** argv)
{
    cudaDeviceProp cudaDevProp;
    cudaError_t rc;
    int gpu;

    // Command line args
    if (argc != 3) {
        std::cout << "Usage: " << argv[0] << " </dev/drxpdN> <gpuNr>\n";
        exit(EXIT_FAILURE);
    }

    // Init DRXP
    if (openDrxp(argv[1]) != 0) {
        exit(EXIT_FAILURE);
    }

    // Init GPU
    gpu = atoi(argv[2]);
    CUDA_CALL( cudaGetDeviceProperties(&cudaDevProp, gpu) );
    CUDA_CALL( cudaSetDevice(gpu) );
    CUDA_CALL( cudaMalloc( (void **)&d_data, DRXP_RX_DATA_SIZE ) );
    CUDA_CALL( cudaHostAlloc( (void **)&h_data_pinned, DRXP_RX_DATA_SIZE, cudaHostAllocWriteCombined ) ); // cudaHostAllocWriteCombined, cudaHostAllocDefault, ...
    CUDA_CALL( cudaMalloc( (void **)&d_spectra, C_spectra_length ) );
    CUDA_CALL( cudaHostAlloc( (void **)&h_spectra, C_spectra_length, cudaHostAllocDefault ) );
    CUDA_CALL( cudaEventCreate(&m_kstart) );
    CUDA_CALL( cudaEventCreate(&m_kstop) );
    CUDA_CALL( cudaStreamCreate(&m_streams[0]) );
    CUDA_CALL( cudaStreamCreate(&m_streams[1]) );

    // Might have to change memory mapping to read-write to allow CUDA Pinning
    // conclusion: needs O_RDWR! but that doesn't work with DRXP driver model!
    //if (mprotect(m_maddr, m_maddr_size, PROT_READ|PROT_WRITE) < 0) {
    //    std::cout << "Error: mprotect() failed, errno=" << errno << " : " << strerror(errno) << "\n";
    //} else {
    //    std::cout << "MProtect: Memory map switch from read-only to read-write succeeded\n";
    //}

    // Try to "pin" DRXP buffer into main memory
    // --> results in "ERROR: File <test_drxpCuPinning.cu>, Line ***: invalid argument"
    rc = cudaHostRegister(m_maddr, m_maddr_size, cudaHostRegisterPortable);
    if (rc != cudaSuccess) {
        std::cout << "Error: cudaHostRegister() failed, cudaErr=" << rc << " : " << cudaGetErrorString(rc) << "\n";
    } else {
        std::cout << "CUDA: Memory pinning succeeded\n";
    }

    // Try to "mlock"
    if (1) {
        if (mlock(m_maddr, m_maddr_size) != 0) {
            std::cout << "Error: mlock() failed, errno=" << errno << " : " << strerror(errno) << "\n";
        } else {
            std::cout << "MLock: Memory locking succeeded\n";
        }
    }

    // Read, transfer
    startDrxpRx();
    int run = 1;
    while (run) {
        void* buf;
        int slot = getDrxpSlot(&buf);
        if (slot < 0) {
            continue;
        }
        doGpu(buf, DRXP_RX_DATA_SIZE); // actual DRXP buffer
        doGpu(buf, DRXP_RX_DATA_SIZE, true); // actual DRXP buffer, copied to pinned first, then to GPU
        doGpu(h_data_pinned, DRXP_RX_DATA_SIZE); // fake, but pinned host-side memory
        releaseDrxpSlot(slot);
        std::cout << std::endl;
    }

    closeDrxp();

    return 0;
}

