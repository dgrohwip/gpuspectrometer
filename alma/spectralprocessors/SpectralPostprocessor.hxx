/**
 * \class SpectralPostprocessor
 *
 * Helper class that applies post-processing to full-resolution spectra
 * from the spectrometer, combined with histograms, and produces pre-calibrated
 * spectra of all configured spectral windows x switching positions.
 */
#ifndef SPECTRALPOSTPROCESSOR_HXX
#define SPECTRALPOSTPROCESSOR_HXX

class SpectralResultSet;
class MCConfiguration;

class SpectralPostprocessor {

    public:
        /** C'stor */
        SpectralPostprocessor();
        /** D'stor */
        ~SpectralPostprocessor();

    public:
        /** Take raw spectra and histograms in a results set, transform them
         * into final output spectra, and store them into a flat array in the
         * same result set container
         */
        bool process(SpectralResultSet* rs, const MCConfiguration* cfg);

        /** Rearrange flat array from an order of "POL-CH" into "CH-POL" order.
         * In a 2-polproduct case : (Nch x XX, Nch x YY) converts into Nch x (XX,YY)
         * In a 4-polproduct case : (Nch x XX, Nch x YY, Nch x ReIm XY) converts into Nch x (XX,YY,ReIm XY)
         */
        void reshapeInterleave(float* inout, int nchan, int npolprod);

    private:
        float* m_interleaveworkbuf;
        int m_interleaveworkbuf_size;
};

#endif // SPECTRALPOSTPROCESSOR_HXX
