/**
 * \class SpectralProcessorIFace
 *
 * Interface definition for a class that carries out computing-intensive
 * spectral processing of raw 3-bit sample data from one baseband.
 * Raw data are pushed into the class from class DRXPIface.
 *
 */
#ifndef SPECTRALPROCESSOR_IFACE_HXX
#define SPECTRALPROCESSOR_IFACE_HXX

#include "datarecipients/DataRecipient_iface.hxx"
#include "xsd_datatypes/delayListList_t.hxx"

#include <cstddef>

class MCObservation;
class MCConfiguration;
class DRXPIface;
class DRXPInfoFrame;
class ResultRecipient;

class SpectralProcessorIFace : public DataRecipient {

    public:

        SpectralProcessorIFace() { };
        virtual ~SpectralProcessorIFace() { };

    public:

        /** Prepare spectral processing based on given observation details (configuration, time, metadata) */
        virtual bool initialize(MCObservation* obs) = 0;

        /** Reset the spectral processing*/
        virtual bool reset() = 0;

        /** Shut down spectral processing of current observation */
        virtual bool deinitialize() = 0;

        /** Replace delay lists in case of correlation mode */
        virtual bool replaceDelayLists(const XSD::delayListList_t&) = 0;

    public:

        /** Interface inherited by SpectralProcessor from DataRecipient
         * \return True if data will be used and group freed later, False if group can be freed immediately
         */
        virtual bool takeData(RawDRXPDataGroup data) = 0;

    public:

        /** Performance optimization related: function to lock pages into memory */
        virtual void pinUserBuffers(int Nbuffers, void** buffers, size_t buflen) = 0;

        /** Performance optimization related: function to unlock pages in memory */
        virtual void unpinUserBuffers(int Nbuffers, void** buffers, size_t buflen) = 0;

    public:

        /** Set the recipient for spectral results */
        virtual void setSpectralRecipient(ResultRecipient* r) = 0;

        /** Assign to a subset of devices (CPU, GPU, any) */
        virtual void setAffinity(int ndevices, const int* deviceIds) = 0;

    public:

        /** Start periodic monitoring. Sends out status messages onto the network. */
        virtual bool startPeriodicMonitoring(const char*, const int, float=5.0f) = 0;

        /** Stop periodic monitoring */
        virtual void stopPeriodicMonitoring() = 0;

};

//typedef SpectralProcessorIFace SpectralProcessor;

#endif // SPECTRALPROCESSOR_IFACE_HXX
