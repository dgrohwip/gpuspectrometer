/**
 * \class VisibilityProcessor
 *
 * Provisional class for future expansion to an actual correlator (i.e. cross-antenna spectra).
 * Currently does nothing.
 *
 */
#ifndef VISIBILITYPROCESSOR_H
#define VISIBILITYPROCESSOR_H

#include "drxp/DRXPIface.hxx"
#include "drxp/DRXPInfoFrame.hxx"
//#include "drxp/drxp_mpi_xbar.hxx"

class VisibilityProcessor {

    public:
        VisibilityProcessor(int Nantennas) { }
        /** Do something to newly completed visibility data. Called by spectral processor. */
        virtual void takeData(void* src, int n, int antNr, const DRXPInfoFrame_t& frameinfo) = 0;

    private:
        void* pimpl;
};

#endif // VISIBILITYPROCESSOR_H
