#include "spectralprocessors/SpectralAccumulator.hxx"
#include <cassert>
#include <cstring>

#include <malloc.h>

//////////////////////////////////////////////////////////////////////////////////////////

/** C'stor. Initializes internal spectral data to zero. */
SpectralAccumulator::SpectralAccumulator(int nchan, int npol, bool fullStokes)
{
    init(nchan, npol, fullStokes);
    if (m_pol1_spec) {
        memset(m_pol1_spec, 0, m_nchan*sizeof(float));
    }
    if (m_pol2_spec) {
        memset(m_pol2_spec, 0, m_nchan*sizeof(float));
    }
    if (m_crosspol_spec) {
        memset(m_crosspol_spec, 0, m_nchan*2*sizeof(float));
    }
}

/** C'stor. Initializes internal spectral data to the given power spectrum */
SpectralAccumulator::SpectralAccumulator(int nchan, const float* pol1)
{
    init(nchan, 1, false);
    memcpy(m_pol1_spec, pol1, m_nchan*sizeof(float));
}

/** C'stor. Initializes internal spectral data to the given half-Stokes power spectra */
SpectralAccumulator::SpectralAccumulator(int nchan, float* pol1, float* pol2)
{
    init(nchan, 2, false);
    memcpy(m_pol1_spec, pol1, m_nchan*sizeof(float));
    memcpy(m_pol2_spec, pol2, m_nchan*sizeof(float));
}

/** C'stor. Initializes internal spectral data to the given full-Stokes power spectra */
SpectralAccumulator::SpectralAccumulator(int nchan, float* pol1, float* pol2, float* crosspol)
{
    init(nchan, 2, true);
    memcpy(m_pol1_spec, pol1, m_nchan*sizeof(float));
    memcpy(m_pol2_spec, pol2, m_nchan*sizeof(float));
    memcpy(m_crosspol_spec, crosspol, m_nchan*2*sizeof(float));
}

//////////////////////////////////////////////////////////////////////////////////////////

/** Reset all accumulated spectra to zero, reset weight(s) to zero */
void SpectralAccumulator::reset()
{
    if (m_pol1_spec) {
        memset(m_pol1_spec, 0, m_nchan*sizeof(float));
        m_sum_weights[0] = 0.0f;
        m_num_avgd[0] = 0;
    }
    if (m_pol2_spec) {
        memset(m_pol2_spec, 0, m_nchan*sizeof(float));
        m_sum_weights[1] = 0.0f;
        m_num_avgd[1] = 0;
    }
    if (m_crosspol_spec) {
        memset(m_crosspol_spec, 0, m_nchan*2*sizeof(float));
        m_sum_weights[2] = 0.0f;
        m_num_avgd[2] = 0;
    }
}

//////////////////////////////////////////////////////////////////////////////////////////

/** D'stor. Free all allocations */
SpectralAccumulator::~SpectralAccumulator()
{
    deinit();
}

//////////////////////////////////////////////////////////////////////////////////////////

/** Reset internal state, allocate arrays */
void SpectralAccumulator::init(const int nchan, const int npol, const bool fullStokes)
{
    m_nchan = nchan;
    m_npol = npol;
    m_fullStokes = fullStokes;

    m_pol1_spec = NULL;
    m_pol2_spec = NULL;
    m_crosspol_spec = NULL;

    m_num_avgd[0] = 0;
    m_num_avgd[1] = 0;
    m_num_avgd[2] = 0;

    m_sum_weights[0] = 0.0f;
    m_sum_weights[1] = 0.0f;
    m_sum_weights[2] = 0.0f;

    m_pol1_spec = (float*)memalign(128, m_nchan*sizeof(float));
    if (m_npol >= 2) {
        m_pol2_spec = (float*)memalign(128, m_nchan*sizeof(float));
    }
    if (m_fullStokes) {
        m_crosspol_spec = (float*)memalign(128, m_nchan*2*sizeof(float));
    }
}

/** Deallocate arrays */
void SpectralAccumulator::deinit()
{
    if (m_pol1_spec) {
        free(m_pol1_spec);
        m_pol1_spec = NULL;
    }
    if (m_pol2_spec) {
        free(m_pol2_spec);
        m_pol2_spec = NULL;
    }
    if (m_crosspol_spec) {
        free(m_crosspol_spec);
        m_crosspol_spec = NULL;
    }
}

//////////////////////////////////////////////////////////////////////////////////////////

int SpectralAccumulator::getNumAveraged() const
{
    if ((m_num_avgd[0] > m_num_avgd[1]) && (m_num_avgd[0] > m_num_avgd[2])) {
        return m_num_avgd[0];
    } else if (m_num_avgd[1] > m_num_avgd[2]) {
        return m_num_avgd[1];
    } else {
        return m_num_avgd[2];
    }
}

//////////////////////////////////////////////////////////////////////////////////////////

/** Accumulate single-pol data */
void SpectralAccumulator::accumulate(const float* in)
{
    accumulator(in, m_pol1_spec, m_nchan);
    m_sum_weights[0] += 1.0f;
    m_num_avgd[0]++;
}

/** Accumulate single-pol data, weighted (0 to 1.0) */
void SpectralAccumulator::accumulate(const float* in, const float weight)
{
    accumulator(in, m_pol1_spec, m_nchan, weight);
    m_sum_weights[0] += weight;
    m_num_avgd[0]++;
}

//////////////////////////////////////////////////////////////////////////////////////////

/** Accumulate dual-pol half-Stokes data */
void SpectralAccumulator::accumulate(const float* pol1, const float* pol2)
{
    accumulator(pol1, m_pol1_spec, m_nchan);
    accumulator(pol2, m_pol2_spec, m_nchan);
    m_sum_weights[0] += 1.0f;
    m_sum_weights[1] += 1.0f;
    m_num_avgd[0]++;
    m_num_avgd[1]++;
}

/** Accumulate dual-pol half-Stokes data, weighted (0 to 1.0) */
void SpectralAccumulator::accumulate(const float* pol1, const float* pol2, const float weight)
{
    accumulator(pol1, m_pol1_spec, m_nchan, weight);
    accumulator(pol2, m_pol2_spec, m_nchan, weight);
    m_sum_weights[0] += weight;
    m_sum_weights[1] += weight;
    m_num_avgd[0]++;
    m_num_avgd[1]++;
}

//////////////////////////////////////////////////////////////////////////////////////////

/** Accumulate dual-pol full-Stokes data */
void SpectralAccumulator::accumulate(const float* pol1, const float* pol2, const float* crosspol)
{
    accumulator(pol1, m_pol1_spec, m_nchan);
    accumulator(pol2, m_pol2_spec, m_nchan);
    accumulator(crosspol, m_crosspol_spec, 2*m_nchan);
    m_sum_weights[0] += 1.0f;
    m_sum_weights[1] += 1.0f;
    m_sum_weights[2] += 1.0f;
    m_num_avgd[0]++;
    m_num_avgd[1]++;
    m_num_avgd[2]++;
}

/** Accumulate dual-pol full-Stokes data, weighted (0 to 1.0) */
void SpectralAccumulator::accumulate(const float* pol1, const float* pol2, const float* crosspol, const float weight)
{
    accumulator(pol1, m_pol1_spec, m_nchan, weight);
    accumulator(pol2, m_pol2_spec, m_nchan, weight);
    accumulator(crosspol, m_crosspol_spec, 2*m_nchan, weight);
    m_sum_weights[0] += weight;
    m_sum_weights[1] += weight;
    m_sum_weights[2] += weight;
    m_num_avgd[0]++;
    m_num_avgd[1]++;
    m_num_avgd[2]++;
}

//////////////////////////////////////////////////////////////////////////////////////////

/** Normalize the spectrum/spectra by the total weight or total number of unweighted accumulations */
void SpectralAccumulator::normalize()
{
    if (m_pol1_spec && (m_sum_weights[0] > 0)) {
        normalizer(m_pol1_spec, m_nchan, 1.0f/m_sum_weights[0]);
        m_sum_weights[0] = 1.0f;
    }
    if (m_pol2_spec && (m_sum_weights[1] > 0)) {
        normalizer(m_pol2_spec, m_nchan, 1.0f/m_sum_weights[1]);
        m_sum_weights[1] = 1.0f;
    }
    if (m_crosspol_spec && (m_sum_weights[2] > 0)) {
        normalizer(m_crosspol_spec, 2*m_nchan, 1.0f/m_sum_weights[2]);
        m_sum_weights[2] = 1.0f;
    }
}

//////////////////////////////////////////////////////////////////////////////////////////

/** Accumulate arbitrary */
void SpectralAccumulator::accumulator(const float* src, float* dst, const int nfloats)
{
    int n = 0;
    for ( ; n <= (nfloats-4); n += 4) {
        dst[n+0] += src[n+0];
        dst[n+1] += src[n+1];
        dst[n+2] += src[n+2];
        dst[n+3] += src[n+3];
    }
    for ( ; n < nfloats; n++) {
        dst[n] += src[n];
    }
}

/** Accumulate arbitrary, weighted (0 to 1.0) */
void SpectralAccumulator::accumulator(const float* src, float* dst, const int nfloats, const float weight)
{
    int n = 0;
    for ( ; n <= (nfloats-4); n += 4) {
        dst[n+0] += src[n+0]*weight;
        dst[n+1] += src[n+1]*weight;
        dst[n+2] += src[n+2]*weight;
        dst[n+3] += src[n+3]*weight;
    }
    for ( ; n < nfloats; n++) {
        dst[n] += src[n]*weight;
    }
}

/** Normalize arbitrary (multiply spectrum by a scalar) */
void SpectralAccumulator::normalizer(float *srcdst, const int nfloats, const float scale)
{
    int n = 0;
    for ( ; n <= (nfloats-4); n += 4) {
        srcdst[n+0] *= scale;
        srcdst[n+1] *= scale;
        srcdst[n+2] *= scale;
        srcdst[n+3] *= scale;
    }
    for ( ; n < nfloats; n++) {
        srcdst[n] *= scale;
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
