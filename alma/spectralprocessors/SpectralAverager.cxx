#include "spectralprocessors/SpectralAverager.hxx"
#include <cstring>

/** Average 'nchan'-long auto-power data across every 'step' channels and write the result into 'out'. */
__attribute__((optimize("unroll-loops")))
void SpectralAverager::spectralAvgAuto(const float* in, float* out, int nchan, int step)
{
    if (step <= 1) {
        // Direct copy
        memcpy(out, in, nchan*sizeof(float));
    } else if(step == 2) {
        // Reduce to half
        for (int n=0, k=0; n<nchan; n+=2, k++) {
            out[k] = (in[n] + in[n+1]) / 2.0f;
        }
    } else if(step == 4) {
        // Reduce to fourth
        for (int n=0, k=0; n<nchan; n+=4, k++) {
            out[k] = (in[n] + in[n+1] + in[n+2] + in[n+3]) / 4.0f;
        }
    } else if(step == 8) {
        // Reduce to eighth
        for (int n=0, k=0; n<nchan; n+=8, k++) {
            out[k] = (in[n] + in[n+1] + in[n+2] + in[n+3] + in[n+4] + in[n+5] + in[n+6] + in[n+7]) / 8.0f;
        }
    } else {
        // Reduce arbitrary (slowest method)
        memset(out, 0, (sizeof(float)*nchan)/step);
        for (int n=0, k=0; n<nchan; n+=step, k++) {
            for (int m=0; m<step; m++) {
                out[k] += in[n+m];
            }
            out[k] /= (float)step;
        }
    }
}

/** Average 'nchan'-long cross-power data across every 'step' channels (Re and Im are averaged independently) and write the result into 'out' */
__attribute__((optimize("unroll-loops")))
void SpectralAverager::spectralAvgCross(const float* in, float* out, int nchan, int step)
{
    if (step <= 1) {
        // Direct copy
        memcpy(out, in, 2*nchan*sizeof(float));
    } else if(step == 2) {
        // Reduce to half
        for (int n=0, k=0; n<nchan; n+=2, k+=2) {
            out[k+0] = (in[2*n+0] + in[2*n+2]) / 2.0f;
            out[k+1] = (in[2*n+1] + in[2*n+3]) / 2.0f;
        }
    } else if(step == 4) {
        // Reduce to fourth
        for (int n=0, k=0; n<nchan; n+=4, k+=2) {
            out[k+0] = (in[2*n+0] + in[2*n+2] + in[2*n+4] + in[2*n+6]) / 4.0f;
            out[k+1] = (in[2*n+1] + in[2*n+3] + in[2*n+5] + in[2*n+7]) / 4.0f;
        }
    } else if(step == 8) {
        // Reduce to eighth
        for (int n=0, k=0; n<nchan; n+=8, k+=2) {
            out[k+0] = (in[2*n+0] + in[2*n+2] + in[2*n+4] + in[2*n+6] + in[2*n+8] + in[2*n+10] + in[2*n+12] + in[2*n+14]) / 8.0f;
            out[k+1] = (in[2*n+1] + in[2*n+3] + in[2*n+5] + in[2*n+7] + in[2*n+9] + in[2*n+11] + in[2*n+13] + in[2*n+15]) / 8.0f;
        }
    } else {
        // Reduce arbitrary (slowest method)
        memset(out, 0, (2*sizeof(float)*nchan)/step);
        for (int n=0, k=0; n<nchan; n+=step, k+=2) {
            for (int m=0; m<step; m++) {
                out[k+0] += in[2*(n+m)+0];
                out[k+1] += in[2*(n+m)+1];
            }
            out[k+0] /= (float)step;
            out[k+1] /= (float)step;
        }
    }
}

/** Multiply 'nchan'-long auto-power data (use 2*nchan for cross-power data) inplace by a constant factor */
__attribute__((optimize("unroll-loops")))
void SpectralAverager::spectralRescale(float* d, int nchan, const float factor)
{
    if ((nchan % 128) == 0) {
        for (int n=0; n<nchan; n+=128) {
            for (int k=0; k<128; k++) {
                d[n+k] *= factor;
            }
        }
    } else if ((nchan % 100) == 0) {
        for (int n=0; n<nchan; n+=8) {
            for (int k=0; k<8; k++) {
                d[n+k] *= factor;
            }
        }
    } else {
        for (int n=0; n<nchan; n++) {
            d[n] *= factor;
        }
    }
}

