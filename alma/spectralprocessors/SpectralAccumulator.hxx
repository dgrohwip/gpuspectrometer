/**
 * \class SpectralAccumulator
 *
 * Helper class that reserves (CPU-side) storage for a power spectrum,
 * and allows accumulation into that spectrum. Supports 1 polarization,
 * 2 polarizations in half-stokes, and 2 polarizations in full-stokes.
 *
 * The class is intended to assist long-term averaging of short-term spectra.
 *
 * One usage example is during a position switching type of observation .
 * In this case one can make two SpectralAccumulator objects, and
 * accumulate sub-integration (from GPU) that are on-source into the
 * first object, and accumulate sub-integrations that are off-source
 * into the second object.
 */

#ifndef SPECTRALACCUMULATOR_HXX
#define SPECTRALACCUMULATOR_HXX

class SpectralAccumulator {

    public:

        /** C'stor. Initializes internal spectral data to zero. */
        SpectralAccumulator(int nchan, int npol, bool fullStokes);

        /** C'stor. Initializes internal spectral data to the given power spectrum */
        SpectralAccumulator(int nchan, const float* pol1);

        /** C'stor. Initializes internal spectral data to the given half-Stokes power spectra */
        SpectralAccumulator(int nchan, float* pol1, float* pol2);

        /** C'stor. Initializes internal spectral data to the given full-Stokes power spectra */
        SpectralAccumulator(int nchan, float* pol1, float* pol2, float* crosspol);

        /** D'stor. Free all allocations */
        ~SpectralAccumulator();

    public:

        int getNchannels() const { return m_nchan; }
        int getNpols() const { return m_npol; }
        bool isFullStokes() const { return m_fullStokes; }

        const float* getSpectrumPol1() const { return m_pol1_spec; }
        const float* getSpectrumPol2() const { return m_pol2_spec; }
        const float* getSpectrumCrosspol() const { return m_crosspol_spec; }

        int getNumAveraged() const;

    private:

        /** Reset internal state, allocate arrays */
        void init(const int nchan, const int npol, const bool fullStokes);

        /** Deallocate arrays */
        void deinit();

    private:

        /** Accumulate arbitrary */
        void accumulator(const float* src, float* dst, const int nfloats);

        /** Accumulate arbitrary, weighted (0 to 1.0) */
        void accumulator(const float* src, float* dst, const int nfloats, const float weight);

        /** Normalize arbitrary (multiply spectrum by a scalar) */
        void normalizer(float *srcdst, const int nfloats, const float scale);

    public:

        /** Accumulate single-pol data */
        void accumulate(const float* in);

        /** Accumulate single-pol data, weighted (0 to 1.0) */
        void accumulate(const float* in, const float weight);

        /** Accumulate dual-pol half-Stokes data */
        void accumulate(const float* pol1, const float* pol2);

        /** Accumulate dual-pol half-Stokes data, weighted (0 to 1.0) */
        void accumulate(const float* pol1, const float* pol2, const float weight);

        /** Accumulate dual-pol full-Stokes data */
        void accumulate(const float* pol1, const float* pol2, const float* crosspol);

        /** Accumulate dual-pol full-Stokes data, weighted (0 to 1.0) */
        void accumulate(const float* pol1, const float* pol2, const float* crosspol, const float weight);

    public:

        /** Normalize the spectrum/spectra by the total weight or total number of unweighted accumulations */
        void normalize();

    public:

        /** Reset all accumulated spectra to zero, reset weight(s) to zero */
        void reset();

    private:

        int m_nchan;
        int m_npol;
        bool m_fullStokes;

        float* m_pol1_spec;
        float* m_pol2_spec;
        float* m_crosspol_spec;

        int m_num_avgd[3]; /// counts the number of spectra averaged in pol 1, pol 2, and crosspol
        float m_sum_weights[3]; /// total sum of weights, if weighted averaging was used

};

#endif // SPECTRALACCUMULATOR_HXX
