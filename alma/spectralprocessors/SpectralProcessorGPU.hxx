/**
 * \class SpectralProcessorGPU
 *
 * Complete spectral processing of raw data implemented on CUDA GPU device(s).
 * Some less realtime critical or less computing intensive calculations are done on CPU.
 *
 */
#ifndef SPECTRALPROCESSOR_GPU_HXX
#define SPECTRALPROCESSOR_GPU_HXX

class MCObservation;
class DRXPIface;
class DRXPInfoFrame;
class ResultRecipient;

#include "spectralprocessors/SpectralProcessor_iface.hxx"

#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/noncopyable.hpp>

class SpectralProcessorGPU : public SpectralProcessorIFace, private boost::noncopyable {

    public:
        SpectralProcessorGPU();
        ~SpectralProcessorGPU();

    public:
        /** Prepare spectral processing based on given observation details (configuration, time, metadata) */
        bool initialize(MCObservation* obs);

        /** Reset the spectral processing*/
        bool reset();

        /** Shut down spectral processing of current observation */
        bool deinitialize();

        /** Replace delay lists in case of correlation mode */
        bool replaceDelayLists(const XSD::delayListList_t&) { return true; }

    public:

        /** Interface inherited by SpectralProcessor from DataRecipient
         * \return True if data will be used and group free later, False if group can be freed immediately
         */
        bool takeData(RawDRXPDataGroup data);

    public:

        /** Performance optimization related: function to lock pages into memory */
        void pinUserBuffers(int Nbuffers, void** buffers, size_t buflen);

        /** Performance optimization related: function to unlock pages in memory */
        void unpinUserBuffers(int Nbuffers, void** buffers, size_t buflen);

    public:

        /** Set the recipient for spectral results */
        void setSpectralRecipient(ResultRecipient* r);

        /** Assign to a subset of devices (GPU) */
        void setAffinity(int ndevices, const int* deviceIds);

    public:

        /** Start periodic monitoring. Sends out status messages onto the network. */
        bool startPeriodicMonitoring(const char* mgroup, int mport, float interval_secs=5.0f);

        /** Stop periodic monitoring */
        void stopPeriodicMonitoring();

    private:

        void monitoringThreadFunc(const char* mgroup, int mport, float interval_secs);

    private:
        /**
         * Reserve a processing slot and return a handle to it
         * \return Slot handle >=0 on success, -1 on error
         */
        int reserveSlot();

        /**
         * Carry out raw spectral processing of data in a slot
         */
        int processSlot(const int slothandle);

    private:
        /** Allocate GPU worker threads */
        bool allocate();

        /** Deallocate GPU worker threads */
        bool deallocate();

    private:
        /** Allocate resources of a single GPU worker thread (CPU objects and on-GPU objects) */
        bool allocateWorker(int slot);

        /** Deallocate resources of a GPU worker thread */
        bool deallocateWorker(int slot);

        /** Worker thread that waits for and processes slices of data on GPU+CPU */
        void workerThread(void* slotcontext);

    private:
        /** Start background spectral results writeout */
        bool startSpectralWriteout();

        /** Stop background spectral results writeout */
        bool stopSpectralWriteout();

        /** Thread that regularly checks for completion of new spectra */
        void handleCompletedSpectraThread();

        /** Get all currently completed spectra from internal SpectralResultManager and write them out to the results handler */
        void handleCompletedSpectra();

    private:
        boost::mutex m_implMutex;  /// Mutex to 'pimpl'
        boost::mutex m_cuCtxMutex; /// Mutex to 'cudaSetDevice()' because a process (all threads) can direct commands to only one selected GPU at a time

        bool m_init_done;
        bool m_shutdown_monitoringThread;   /// 'true' to signal the monitoring thread to stop
        boost::thread* m_monitoringThread;  /// thread that is running monitoringThreadFunc()
        bool m_shutdown_specHandlerThread;  /// 'true' to signal the handleCompletedSpectraThread thread to stop
        boost::thread* m_specHandlerThread; /// thread that is running handleCompletedSpectraThread()

        /// Implementation specific data structures
        void* pimpl;


};

#endif
