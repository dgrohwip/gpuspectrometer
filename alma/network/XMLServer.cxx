#include "helper/logger.h"
#include "network/XMLServer.hxx"

#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/version.hpp>
#if (BOOST_VERSION < 105300)
    #include <boost/algorithm/string/trim.hpp>
#else
    #include <boost/algorithm/string/trim_all.hpp>
    // trim.hpp (before v1.53): left&right trim, without altering middle of string
    // trim_all.hpp (v1.53 and later): left&right trimg, and additionally replaces whitespace inside of string
#endif

#include <algorithm>
#include <iostream>
#include <string>

using namespace boost::asio::ip;

/////////////////////////////////////////////////////////////////////////////////////////////////////////
/// XMLServer class
/////////////////////////////////////////////////////////////////////////////////////////////////////////

XMLServer::XMLServer(XMLIOIface& callbackObj, int port)
    : xmlcallback_(callbackObj), port_(port), endpoint_(tcp::v4(), port), acceptor_(service_,endpoint_)
{
    acceptor_.set_option(boost::asio::socket_base::reuse_address(true));
}

void XMLServer::acceptClients()
{
    // Create XML session for the first client
    XMLServerSession* first_session = new XMLServerSession(service_, xmlcallback_);

    // Wait for first client to connect and use that session
    acceptor_.async_accept(
        first_session->getSocket(),
        boost::bind(&XMLServer::handle_accept, this, first_session, boost::asio::placeholders::error)
    );

    // Start asio TCP service, blocking!
    service_.run();
}

void XMLServer::handle_accept(XMLServerSession* session, const boost::system::error_code& error)
{
    if (!error) {

        L_(linfo) << "A client connected from " << session->getSocket().remote_endpoint();

        // Start asynchronously receiving command(s) from the new connected client session
        session->start();

        // Create next session
        XMLServerSession* future_session = new XMLServerSession(service_, xmlcallback_);

        // Wait for next client to connect and use that session
        acceptor_.async_accept(
            future_session->getSocket(),
            boost::bind(&XMLServer::handle_accept, this, future_session, boost::asio::placeholders::error)
        );

    } else {
        std::cout << "handle_accept() boost error " << error << "\n";
        delete session;
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
// XMLServerSession asynchronous I/O for one connected client
/////////////////////////////////////////////////////////////////////////////////////////////////////////

XMLServerSession::XMLServerSession(boost::asio::io_service& service, XMLIOIface& callback)
    : clientsocket_(service), xmlcallback_(callback)
{
    nbyte_in_ = 0;
    memset(data_in_, 0, sizeof(data_in_));
    memset(data_out_, 0, sizeof(data_out_));
}

boost::asio::ip::tcp::socket& XMLServerSession::getSocket()
{
    return clientsocket_;
}

void XMLServerSession::start()
{
    // Sequence:
    //  1) async_read(4 byte) --> calls back handle_read_header()
    //  2) handle_read_header() --> async_read(msg len) --> calls back handle_read_body()
    //  3) handle_read_header() --> reply=xmlcallback_.xmlHandover(msg) --> async_write(reply len + reply) --> calls back handle_write_response()
    //  4) handle_write_response() --> restart from [1] by async_read(4 byte) --> calls back handle_read_header()

    boost::asio::async_read(
        clientsocket_,
        boost::asio::buffer(&nbyte_in_, sizeof(nbyte_in_)),
        boost::asio::transfer_exactly(sizeof(nbyte_in_)),
        boost::bind(&XMLServerSession::handle_read_header, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred)
    );
}

void XMLServerSession::handle_read_header(const boost::system::error_code& error, size_t nbytes)
{
    if (!error) {
        assert(nbytes == sizeof(nbyte_in_));
        size_t nchars = ntohl(nbyte_in_);
        // L_(ldebug) << "handle_read() got nbytes=" << nbytes << " into length field, data=" << nchars;
        if (nchars >= sizeof(data_in_)) {
            L_(lerror) << "Client at " << clientsocket_.remote_endpoint() << " sent " << nbyte_in_ << " bytes, exceeds allowed length of " << sizeof(data_in_) << "+1 bytes";
            delete this;
        } else {
            boost::asio::async_read(
                clientsocket_,
                boost::asio::buffer(data_in_, nchars),
                boost::asio::transfer_exactly(nchars),
                boost::bind(&XMLServerSession::handle_read_body, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred)
            );
        }
    } else {
        delete this;
    }
}

void XMLServerSession::handle_read_body(const boost::system::error_code& error, size_t nbytes)
{
    if (!error) {

        // Tidy up the received XML message
        data_in_[nbytes] = '\0';
        std::string xml_in(data_in_);
        trim_xml(xml_in);
        L_(ldebug) << "Received " << xml_in;

        // Hand XML to the message handler class
        std::string xml_out = xmlcallback_.xmlHandover(xml_in);

        // Transmit the response
        size_t ntransmit = sizeof(uint32_t) + xml_out.size();
        data_out_[0] = htonl(xml_out.size());
        std::copy(xml_out.begin(), xml_out.end(), reinterpret_cast<char*>(&data_out_[1]));
        boost::asio::async_write(
            clientsocket_,
            boost::asio::buffer(reinterpret_cast<char*>(data_out_), ntransmit),
            boost::bind(&XMLServerSession::handle_write_response, this, boost::asio::placeholders::error)
        );
    } else {
        delete this;
    }
}

void XMLServerSession::trim_xml(std::string& xml) const
{
    xml.erase(std::remove(xml.begin(), xml.end(), '\t'), xml.end());
    xml.erase(std::remove(xml.begin(), xml.end(), '\r'), xml.end());
    xml.erase(std::remove(xml.begin(), xml.end(), '\n'), xml.end());
#if (BOOST_VERSION < 105300)
    boost::algorithm::trim(xml);
#else
    boost::algorithm::trim_all(xml);
#endif
}

void XMLServerSession::handle_write_response(const boost::system::error_code& error)
{
    if (!error) {
        // Read next message, if any
        boost::asio::async_read(
            clientsocket_,
            boost::asio::buffer(&nbyte_in_, sizeof(nbyte_in_)),
            boost::asio::transfer_exactly(sizeof(nbyte_in_)),
            boost::bind(&XMLServerSession::handle_read_header, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred)
        );
    } else {
        delete this;
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////

