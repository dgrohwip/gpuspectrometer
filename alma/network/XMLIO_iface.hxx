#ifndef XMLIO_IFACE_HXX
#define XMLIO_IFACE_HXX

#include <string>

class XMLIOIface
{
    public:
        XMLIOIface() { }
        virtual ~XMLIOIface() { }

    public:
        virtual std::string xmlHandover(std::string xmlIn) = 0;
};

#endif
