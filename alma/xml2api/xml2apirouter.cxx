/**
 * \brief The top level class to handle XML commands, and an implementation
 *        of the command class factory.
 *
 * The top level class uses Xerces-C C++ library (if available) to validate XML
 * against a schema file. It then uses the (more robust) Boost PropertyTree to
 * parse the contents of the XML and determine the type of M&C command.
 * The items in the PropertyTree are passed onwards to some handler class
 * that is responsible for that command. The handler class must parse
 * the remainder of the tree into C++ internal structures and invoke
 * the respective functions in the GPU spectrometer.
 *
 * \author $Author: janw $
 *
 */

#include "xml2api/xml2apirouter.hxx"
#include "xml2api/xml2api_factory.hxx"
#include "helper/logger.h"

#include <boost/algorithm/string.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/foreach.hpp>

#include <string>
#include <cstring>
#include <set>
#include <map>
#include <exception>
#include <stdexcept>
#include <iostream>
#include <fstream>
#include <sstream>
#include <memory>

#include <limits.h>
#include <stdlib.h>  // realpath()

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

std::string XML2ApiRouter::generateBasicResponse(boost::property_tree::ptree& pt_out, unsigned int Id, const char* cmd, const char* code, const char* status)
{
    std::ostringstream os;
    std::string _cmd(cmd);
    pt_out.put("response.commandId", Id);
    pt_out.put("response." + _cmd + ".completionCode", code);
    if (strlen(status) > 0) {
        pt_out.put("response." + _cmd + ".status", status);
    }
    write_xml(/*dest*/os, /*src*/pt_out);
    return os.str();
}

/**
 * Dynamic handler: dispach command in 'in' to a suitable handler that then provides output into 'out'.
 */
int XML2ApiRouter::handle(const boost::property_tree::ptree& in, boost::property_tree::ptree& out, ASMInterface& asm_)
{
    using boost::property_tree::ptree;
    int rc;

    // Determine command ID & name from input
    const int commandId = in.get("command.commandId", 0);
    std::string commandName;
    BOOST_FOREACH(const ptree::value_type &v, in.get_child("command")) {
        std::string elem(v.first.data());
        boost::trim(elem);
        if (elem != "commandId") {
            commandName = elem;
            break;
        }
    }

    // Handle
    MCMsgCommandResponse *impl = MCMsgCommandResponseFactory::createObject(commandName);
    if (impl == NULL) {
        std::cout << "MCMsgCommandResponse: did not find representative handler for command " << commandName << ".\n";
        generateBasicResponse(out, commandId, commandName.c_str(), "not-implemented", "initialized");
        rc = -1;
    } else {
        const ptree& in_subtree = in.get_child(std::string("command.") + commandName, empty_ptree<ptree>());
        impl->setId(commandId);
        rc = impl->handle(in_subtree, out, asm_);
        delete impl;
    }

    return rc;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/** \brief Parses the XML Schema to prepare future message handling.
 *
 * Makes a rudimentary parse of the XML Schema file and extracts the
 * allowed command and response names (but not their allowed arguments).
 */
XML2ApiRouter::XML2ApiRouter(const std::string &schemafilename)
{
    using boost::property_tree::ptree;

    m_schemaFilename = schemafilename;

    ptree xsd;
    try {
        L_(linfo) << "Loading XML Schema file " << schemafilename;
        read_xml(schemafilename, xsd);
    } catch(boost::property_tree::xml_parser::xml_parser_error& e) {
        throw std::invalid_argument("Error with ASC M&C schema file " + schemafilename + " (line " + boost::lexical_cast<std::string>(e.line()) + " " + e.message() + ")");
    }

    L_(linfo) << "Extracting M&C commands from XML Schema file " << schemafilename;
    BOOST_FOREACH(ptree::value_type const& v, xsd.get_child("xsd:schema")) {

        if (v.first == "xsd:element") {

            const ptree &atr = v.second.get_child("<xmlattr>", empty_ptree<ptree>());
            const ptree &lst = v.second.get_child("xsd:complexType.xsd:sequence", empty_ptree<ptree>());
            std::string type(atr.get("name","invalid"));

            BOOST_FOREACH(const ptree::value_type &vs, lst.get_child("xsd:choice")) {

                const ptree &satr = vs.second.get_child("<xmlattr>", empty_ptree<ptree>());
                std::string cmd(satr.get("name",""));
                boost::trim(cmd);

                if ((type == "command") && !cmd.empty()) {
                    m_schemaCommands.insert(cmd);
                    if (PT_DEBUG) std::cout << "added " << type << " " << cmd << "\n";
                    // std::cout << "Found M&C schema command '" << cmd << "'\n";
                } else if ((type == "response") && !cmd.empty()) {
                    m_schemaResponses.insert(cmd);
                    if (PT_DEBUG) std::cout << "added " << type << " " << cmd << "\n";
                } else {
                    std::cout << "Schema init failed while looking at xsd:element '" << type << "', xsd:choice '" << cmd << "'\n";
                }

            }
        }
    }
}

XML2ApiRouter::~XML2ApiRouter()
{
}

/** \brief Returns true if the specified command name was found in the XML schema.
 */
bool XML2ApiRouter::isNominallySupported(const std::string &command) const
{
    bool isSupportedCmd = (m_schemaCommands.find(command) != m_schemaCommands.end());
    bool isSupportedResp = (m_schemaResponses.find(command) != m_schemaResponses.end());
    return isSupportedCmd | isSupportedResp;
}

/** \brief Returns true if the specified command was found in the registered command handlers.
 */
bool XML2ApiRouter::isCommandImplemented(const std::string &command) const
{
    MCMsgCommandResponse *impl = MCMsgCommandResponseFactory::createObject(command);
    if (impl == NULL) {
        return false;
    }
    delete impl;
    return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

std::string XML2ApiRouter::dbg_ListAllCommands(bool do_color) const
{
    std::ostringstream ss;
    std::set<std::string>::iterator it;
    const std::string green("\033[32m"), red("\033[31m"), def("\033[39m");

    ss << "Commands found in XML Schema and their status in ASM:\n";
    for (it = m_schemaCommands.begin(); it != m_schemaCommands.end(); it++) {
        bool implemented = isCommandImplemented(*it);
        if (!do_color) {
            ss << std::left << std::setw(25) << (*it) << (implemented ? " : implemented\n" : " : not yet implemented\n");
        } else {
            ss << std::left << std::setw(25) << (*it) << " : ";
            if (implemented) {
                ss << green << "implemented" << def << " \n";
            } else {
                ss << red << "not yet implemented" << def << " \n";
            }
        }
    }
    return ss.str();
}

