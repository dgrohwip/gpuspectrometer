/**
 * \class XML2ApiRouter
 *
 * \brief The top level class to handle XML commands.
 *
 * The class uses Xerces-C C++ library (if available) to validate XML against
 * a schema file. It then uses the (more robust) Boost PropertyTree to
 * parse the contents of the XML and determine the type of M&C command.
 * The items in the PropertyTree are passed onwards to some handler class
 * that is responsible for that command. The handler class must parse
 * the remainder of the tree into C++ internal structures and invoke
 * the respective functions in the GPU spectrometer.
 *
 * \author $Author: janw $
 *
 */
#ifndef XML2API_HXX
#define XML2API_HXX

#include <boost/property_tree/ptree.hpp>
#include <iostream>
#include <map>
#include <set>
#include <string>

class ASMInterface;
#include "global_defs.hxx"
#include "asm/ASMInterface.hxx"
#include "xml2api/xml2api_factory.hxx"

#ifndef PT_DEBUG
    #define PT_DEBUG 0
#endif


template<class Ptree>
inline const Ptree &empty_ptree()
{
    static Ptree pt;
    return pt;
}

struct XML2ApiRouter {

    private:
        XML2ApiRouter() {};

    public:
        XML2ApiRouter(const std::string &schemafilename);
        ~XML2ApiRouter();

    public:
        bool isNominallySupported(const std::string &command) const;
        bool isCommandImplemented(const std::string &command) const;
        const std::string& getSchemaFileName() const { return m_schemaFilename; }

    public:
        int handle(const boost::property_tree::ptree& in, boost::property_tree::ptree& out, ASMInterface& asm_);

    public:
        std::string generateBasicResponse(boost::property_tree::ptree& pt_out, unsigned int Id, const char* cmd, const char* code, const char* status);

    public:

    public:
        std::string dbg_ListAllCommands(bool do_color=true) const;

    private:
        std::string m_schemaFilename;
        std::set<std::string> m_schemaCommands;
        std::set<std::string> m_schemaResponses;
};

#endif // XML2API_HXX
