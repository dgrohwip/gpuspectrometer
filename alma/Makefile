include Makefile.inc

.NOTPARALLEL:

##############################################################################################
## Sources
##############################################################################################

# Subdirs with Makefile's that build some (self-)test programs
EXTRA_SUBDIRS = xsd_datatypes xml2api commands asm datarecipients helper time drxp \
    spectralprocessors calibrations mcwrapping drxp_util

HDR  = $(wildcard calibrations/*.hxx)
HDR += $(wildcard datarecipients/*.hxx)
HDR += $(wildcard drxp/*.hxx)
HDR += $(wildcard helper/*.h*)
HDR += $(wildcard mcwrapping/*.hxx)
HDR += $(wildcard mime/*.hxx)
HDR += $(wildcard network/*.hxx)
HDR += $(wildcard spectralprocessors/*.hxx)
HDR += $(wildcard time/*.hxx)
HDR += $(wildcard xml2api/*.hxx)

CMD_SRC = $(wildcard commands/xml2api_*.cxx)

AUX_SRC  = $(wildcard asm/*.cxx)
AUX_SRC += $(wildcard calibrations/*.cxx)
AUX_SRC += $(wildcard datarecipients/*.cxx)
AUX_SRC += $(wildcard helper/*.cxx)
AUX_SRC += $(wildcard mcwrapping/mc_*.cxx)
AUX_SRC += $(wildcard mime/*.cxx)
AUX_SRC += $(wildcard network/*.cxx)
AUX_SRC += $(wildcard spectralprocessors/*.cxx)
AUX_SRC += $(wildcard time/*.cxx)
AUX_SRC += $(wildcard xsd_datatypes/*.cxx)
AUX_SRC += $(wildcard xml2api/*.cxx)

CUDA_SRC = spectralprocessors/SpectralProcessorGPU.cu

##############################################################################################
## Object files
##############################################################################################

AUX_OBJS  = $(AUX_SRC:.cxx=.o)
CUDA_OBJS = $(CUDA_SRC:.cu=.co)
CMD_OBJS  = $(CMD_SRC:.cxx=.o)
ALMA_OBJS = $(CMD_OBJS) $(AUX_OBJS)

ifeq ($(HAVE_DRXP),1)
    AUX_SRC += drxp/DRXP.cxx
    AUX_SRC += drxp/DRXPDummy.cxx
    AUX_SRC += drxp/DRXPInfoFrame.cxx drxp/SFF8431_I2C.cxx drxp/DRXPPCIeLookup.cxx
else
    AUX_SRC += drxp/DRXPDummy.cxx
    AUX_SRC += drxp/DRXPInfoFrame.cxx drxp/SFF8431_I2C.cxx drxp/DRXPPCIeLookup.cxx
    $(warning Warning: Compiling without DRXP support (will use built-in dummy DRXP)!)
endif

ifeq ($(HAVE_GPU),1)
    ALMA_OBJS += $(CUDA_OBJS)
    EXTRA_SUBDIRS += kernels
else
    $(warning Warning: Compiling without GPU support (will use CPU code, no actual spectral output)!)
endif

##############################################################################################
## Targets
##############################################################################################

all: $(EXTRA_SUBDIRS) ASM_MC_service

gitVersion: FORCE
	# Auto-update of version_gittag.hxx
	@./version_gittag.sh

$(EXTRA_SUBDIRS):
	$(MAKE) -C $@ CUDA_DIR=$(CUDA_DIR) MAKEFLAGS=$(MAKEFLAGS)

doc: FORCE
	$(MAKE) -C ./doc/ MAKEFLAGS=$(MAKEFLAGS)

xsd_datatypes/xsd_datatypes.hxx.gch: xsd_datatypes/xsd_datatypes.hxx
	$(MAKE) -C xsd_datatypes xsd_datatypes.hxx.gch

PRECOMPILED_HEADER: xsd_datatypes/xsd_datatypes.hxx.gch

ASM_MC_service: ASM_MC_service.cxx $(HDR) $(ALMA_OBJS) gitVersion
	$(CXX) $(ALMA_CXXFLAGS) $< $(ALMA_OBJS) -o $@ $(ALMA_LIBS)
	# Optionally, add CAP_SYS_ADMIN priviledges that help to optimize CUDA access to DRXP DMA Ring Buffer memory
	# sudo setcap cap_sys_admin=+ep $@

try:
	for EXTRA in $(EXTRA_SUBDIRS); do \
		$(MAKE) -C $$EXTRA try CUDA_DIR=$(CUDA_DIR) MAKEFLAGS=$(MAKEFLAGS); \
	done

clean:
	rm -f ASM_MC_service $(CMD_OBJS) $(AUX_OBJS) $(CUDA_OBJS) version_gittag.hxx
	for EXTRA in $(EXTRA_SUBDIRS); do \
		$(MAKE) -C $$EXTRA clean CUDA_DIR=$(CUDA_DIR) MAKEFLAGS=$(MAKEFLAGS); \
	done

tests/raw3bit/fourlines_synthetic_48ms_2pol.3bit: tests/raw3bit/synthetic.zip
	cd tests/raw3bit/ ; unzip -u synthetic.zip

install: all tests/raw3bit/fourlines_synthetic_48ms_2pol.3bit
	mkdir -p $(INSTALL_PREFIX)/alma/bin/
	mkdir -p $(INSTALL_PREFIX)/alma/conf/
	mkdir -p $(INSTALL_PREFIX)/alma/share/
	#ln -f -s $(INSTALL_PREFIX)/alma /usr/local/alma
	cp -af ASM_MC_service $(INSTALL_PREFIX)/alma/bin/
	cp -af scripts/asmMimeUploader.py $(INSTALL_PREFIX)/alma/bin/
	cp -af scripts/asmMimeInfo.py $(INSTALL_PREFIX)/alma/bin/
	cp -af scripts/asmPlotMime.py $(INSTALL_PREFIX)/alma/bin/
	cp -af scripts/asmTxCmd.py $(INSTALL_PREFIX)/alma/bin/
	cp -af scripts/drxpMonRx.py $(INSTALL_PREFIX)/alma/bin/
	cp -af scripts/asmMon.py $(INSTALL_PREFIX)/alma/bin/
	for EXTRA in $(EXTRA_SUBDIRS); do \
		$(MAKE) -C $$EXTRA install; \
	done
	cp -af ASC_MC.xsd $(INSTALL_PREFIX)/alma/conf/
	cp -af ASM_MC_config.json $(INSTALL_PREFIX)/alma/conf/
	cp -af tests/spectra/* $(INSTALL_PREFIX)/alma/share/
	cp -af tests/raw3bit/fourlines_synthetic_48ms_2pol.3bit $(INSTALL_PREFIX)/alma/share/
	chmod a+r $(INSTALL_PREFIX)/alma/share/*
	chmod a+r $(INSTALL_PREFIX)/alma/conf/*

.PHONY: all $(EXTRA_SUBDIRS) clean install try gitVersion FORCE

FORCE:
