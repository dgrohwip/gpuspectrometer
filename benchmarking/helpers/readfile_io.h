#ifndef READFILE_IO_H
#define READFILE_IO_H

void readData_file_Mk5B(const char* fname, unsigned char* buf, size_t nframes);

#endif
