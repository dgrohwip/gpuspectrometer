#ifndef READFILE_IO_C // in case file is "include"d
#define READFILE_IO_C

////////////////////////////////////////////////////////////////////////////////////////////////
// Helper for reading Mark5B file, with headers stripped during reading.
////////////////////////////////////////////////////////////////////////////////////////////////

#include <stdio.h>

void readData_file_Mk5B(const char* fname, unsigned char* buf, size_t nframes)
{
    FILE* fin;
    unsigned char *header;
    const size_t MK5_payloadsize = 10000;
    const size_t MK5_headersize  = 16;

    fin = fopen(fname, "rb");
    if (fin == NULL) {
        perror("Input file error");
        exit(-1);
    }
    fseek(fin, 0, SEEK_SET);

    // Read one-second data from a file in Mark5B format, strip the 5B headers
    header = (unsigned char*)malloc(sizeof(char) * MK5_headersize);
    for (size_t k = 0; k < nframes; k++) {
        size_t nrd;
        nrd = fread(header, MK5_headersize, 1, fin);
        if (nrd != 1) {
            perror("readData_file_Mk5B short read");
        }
        nrd = fread(buf + k*MK5_payloadsize, MK5_payloadsize, 1, fin);
        if (nrd != 1) {
            perror("readData_file_Mk5B short read");
        }
    }
    fclose(fin);

    free(header);
}

#endif // READFILE_IO_C
