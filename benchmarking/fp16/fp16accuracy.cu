////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Compare sample representations in fp16 and fp32.
// Compare result of large FFTs with fp16 input only (load stage is fp16), and fp16 input with fp16 processing and fp16 output.
//
// Findings so far:
//   2-bit : noise : low error
//   2-bit : sawtooth : low error only until 32k-point FFT, then unusable
//   2-bit : DC level : low error only until 16k-point FFT, then unusable
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <cuda.h>
#include <cuda_fp16.h>
#include <cufft.h>
#include <cufftXt.h>

#ifndef CUDA_DEVICE_NR
#define CUDA_DEVICE_NR 0
#endif

const float lut3bit_alma[8] = {-7.0f, -5.0f, -1.0f, -3.0f, +7.0f, +5.0f, +1.0f, +3.0f };
const float lut2bit_vdif[4] = {-3.3359f, -1.0f, +1.0f, +3.3359f };

int m_DataGen = 0; // signal type to make (0=noise, 1=sawtooth, 2=DC level), can be set from cmd line

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CUDA wrappers
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define CUFFT_CALL(x) m_CUFFT_CALL(x,__FILE__,__LINE__)
void m_CUFFT_CALL(cufftResult_t iRet, const char* pcFile, const int iLine)
{
        if (iRet != CUFFT_SUCCESS) {
                (void) fprintf(stderr, "CuFFT ERROR: File <%s>, Line %d: error %d\n",
                               pcFile, iLine, iRet);
                cudaDeviceReset();
                exit(EXIT_FAILURE);
        }
        return;
}

#define CUDA_CALL(x) m_CUDA_CALL(x,__FILE__,__LINE__)
void m_CUDA_CALL(cudaError_t iRet, const char* pcFile, const int iLine)
{
        if (iRet != cudaSuccess) {
                (void) fprintf(stderr, "ERROR: File <%s>, Line %d: %s\n",
                               pcFile, iLine, cudaGetErrorString(iRet));
                cudaDeviceReset();
                exit(EXIT_FAILURE);
        }
        return;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Datatype conversion loss
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void checkConversion()
{
    printf("\n=== Conversion accuracy ===\n\n");

    printf("Conversion of 3-bit data to float32 and float16:\n");
    for (int n=0; n<8; n++) {
        const float value = lut3bit_alma[n];
        const float reconverted = __half2float( __float2half(value) );
        printf("   q:%d voltage:%+7.5f : fp16 voltage:%+7.5f : delta=%.3e\n", n, value, reconverted, value-reconverted);
    }
    printf("\n");

    printf("Conversion of 2-bit data to float32 and float16:\n");
    for (int n=0; n<4; n++) {
        const float value = lut2bit_vdif[n];
        const float reconverted = __half2float( __float2half(value) );
        printf("   q:%d voltage:%+7.5f : fp16 voltage:%+7.5f : delta=%.3e\n", n, value, reconverted, value-reconverted);
    }
    printf("\n");

}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Transform conversion lsos
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int indexGen(int n, int N)
{
    switch (m_DataGen) {
       case 1: return n % N; // sawtooth'ish
       case 2: return N-1; // DC
       default: return rand() % N; // randomized data
    }
}

void generateData4Level_f16(half* out, int length)
{
    time_t t;
    srand((unsigned) time(&t));
    for (int n=0; n<length; n++) {
        out[n] = lut2bit_vdif[indexGen(n,4)];
    }
}

void generateData4Level_f32(float* out, int length)
{
    time_t t;
    srand((unsigned) time(&t));
    for (int n=0; n<length; n++) {
        out[n] = lut2bit_vdif[indexGen(n,4)];
    }
}

void generateData8Level_f16(half* out, int length)
{
    time_t t;
    srand((unsigned) time(&t));
    for (int n=0; n<length; n++) {
        out[n] = lut3bit_alma[indexGen(n, 8)];
    }
}

void generateData8Level_f32(float* out, int length)
{
    time_t t;
    srand((unsigned) time(&t));
    for (int n=0; n<length; n++) {
        out[n] = lut3bit_alma[indexGen(n, 8)];
    }
}

void compareVisually(const float2* fft32, const half2* fft16full, const int Npoints)
{
    const int values_per_line = 8;
    int n = 0;
    while (n < Npoints) {
        for (int k=0; k<values_per_line && (n+k)<Npoints; k++) {
            printf("bin %-8d %11s", n+k, " ");
        }
        printf("\n");
        for (int k=0; k<values_per_line && (n+k)<Npoints; k++) {
            float2 v = fft32[n+k];
            printf("%+10.6f %+10.6fi  ", v.x, v.y);
        }
        printf("\n");
        for (int k=0; k<values_per_line && (n+k)<Npoints; k++) {
            float2 v = __half22float2(fft16full[n+k]);
            printf("%+10.6f %+10.6fi  ", v.x, v.y);
        }
        printf("\n\n");
        n += values_per_line;
    }

}

void compare(const float* fft32, const half* fft16full, const int Npoints,
    double* mean_of_errors, double* error_of_mean
)
{
    double mean_err = 0, mean32 = 0, mean16 = 0;
    for (int n=0; n<Npoints; n++) {
        float err = fabs(fft32[n] - __half2float(fft16full[n]));
        mean_err += err;
        mean32 += fft32[n];
        mean16 += __half2float(fft16full[n]);
    }
    mean_err /= Npoints;
    mean32 /= Npoints;
    mean16 /= Npoints;
    *mean_of_errors = mean_err;
    *error_of_mean = fabs(mean32-mean16);
}


void checkTransform()
{
    const int MAX_FFT_POINTS = 8388608;
    const int MAX_FFT_POINTS_OUT = MAX_FFT_POINTS/2 + 1;

    cufftHandle fftplan_r2c;
    cufftHandle fftplan_r2c_fp16input;
    cufftHandle fftplan_r2c_fp16full;

    void *data16, *data32;
    void *fft32, *fft16input, *fft16full;

    CUDA_CALL( cudaSetDevice(CUDA_DEVICE_NR) );

    printf("Alloc... ");
    CUDA_CALL( cudaMallocManaged(&data16, MAX_FFT_POINTS*sizeof(half)) );
    CUDA_CALL( cudaMallocManaged(&data32, MAX_FFT_POINTS*sizeof(float)) );
    CUDA_CALL( cudaMallocManaged(&fft32, MAX_FFT_POINTS_OUT*sizeof(cufftComplex)) );
    CUDA_CALL( cudaMallocManaged(&fft16input, MAX_FFT_POINTS_OUT*sizeof(cufftComplex)) );
    CUDA_CALL( cudaMallocManaged(&fft16full, MAX_FFT_POINTS_OUT*sizeof(cufftReal)) );
    printf("done\n");

    for (int dtype=0; dtype<2; dtype++) {

        if (dtype == 0) {

            printf("\n=== Transform accuracy for 2-bit equivalent raw data ===\n\n");

            printf("Raw data gen...");
            generateData4Level_f16((half*)data16, MAX_FFT_POINTS);
            generateData4Level_f32((float*)data32, MAX_FFT_POINTS);

        } else {

            printf("\n=== Transform accuracy for 3-bit equivalent raw data ===\n\n");

            printf("Raw data generation gen...");
            generateData8Level_f16((half*)data16, MAX_FFT_POINTS);
            generateData8Level_f32((float*)data32, MAX_FFT_POINTS);

        }
        cudaMemPrefetchAsync(data16, MAX_FFT_POINTS*sizeof(half), CUDA_DEVICE_NR, NULL);
        cudaMemPrefetchAsync(data32, MAX_FFT_POINTS*sizeof(float), CUDA_DEVICE_NR, NULL);
        printf("done\n");

        printf("FFT size  Batch size Mean(errs)  Err(means)\n");

        for (int fftlen=128; fftlen <= MAX_FFT_POINTS; fftlen *= 2) {

            // Layout
            long long dimnXt[1] = { fftlen };
            int dimn[1] = {fftlen};         // DFT size
            int inembed[1] = {0};           // ignored for 1D xform
            int onembed[1] = {0};           // ignored for 1D xform
            int istride = 1, ostride = 1;   // step between successive in(out) elements
            int idist = fftlen;             // step between batches (R2C input)
            int odist = fftlen/2+1;         // step between batches (R2C output)
            size_t worksize[10];
    
            // Reference 32-bit -> 32-bit FFT
            CUFFT_CALL( cufftPlanMany(&fftplan_r2c, 1, dimn,
                inembed, istride, idist,
                onembed, ostride, odist,
                CUFFT_R2C,
                1)
            );
            CUFFT_CALL( cufftExecR2C(fftplan_r2c, (cufftReal*)data32, (cufftComplex*)fft32) );

            // Fully 16-bit -> 16-bit FFT
            CUFFT_CALL( cufftCreate(&fftplan_r2c_fp16full) );
            CUFFT_CALL( cufftXtMakePlanMany(fftplan_r2c_fp16full,
                1, dimnXt,
                NULL, 1, fftlen, /* inembed=NULL, ignored istride, ignored idist, */
                CUDA_R_16F,
                NULL, 1, fftlen/2+1, /* onembed=NULL, ignored ostride, ignored odist, */
                CUDA_C_16F,
                1,
                worksize,
                CUDA_C_16F)
            );
            CUFFT_CALL( cufftXtExec(fftplan_r2c_fp16full, (float*)data16, (float*)fft16full, CUFFT_FORWARD) );


            // Compare
            CUDA_CALL( cudaDeviceSynchronize() );

            //compareVisually((float2*)fft32, (half2*)fft16full, 8);
            //compareVisually((float2*)fft32, (half2*)fft16full, odist);

            double mean_of_errors, error_of_mean;
            compare((float*)fft32, (half*)fft16full, odist, &mean_of_errors, &error_of_mean);
            printf("%8d %8d %9.5f %9.5f\n", fftlen, odist, mean_of_errors, error_of_mean);

            CUFFT_CALL( cufftDestroy(fftplan_r2c) );
            CUFFT_CALL( cufftDestroy(fftplan_r2c_fp16full) );

        }
    }

    CUDA_CALL( cudaFree(data16) );
    CUDA_CALL( cudaFree(data32) );
    CUDA_CALL( cudaFree(fft32) );
    CUDA_CALL( cudaFree(fft16input) );
    CUDA_CALL( cudaFree(fft16full) );

}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// MAIN
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int main(int argc, char** argv)
{
    if (argc >= 2) {
        m_DataGen = atoi(argv[1]);
    }
    checkConversion();
    checkTransform();
    return 0;
}
