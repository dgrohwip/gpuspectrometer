////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// A test of CUDA 7.x CuFFT with Callbacks to convert FFT input Half2 data into Float2
//
// Compile time options:
// STATIC_CUFFT  static link CUFFT in order to use Load callback and fp16 data into fp32 for normal f32 r2c FFT
// NATIVE_FP16   use native fp16 and cufftXtMakePlanMany() with input:CUDA_R_16F output:CUDA_C_16F
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// http://docs.nvidia.com/cuda/cuda-math-api/group__CUDA__MATH____HALF2__ARITHMETIC.html#group__CUDA__MATH____HALF2__ARITHMETIC
// http://docs.nvidia.com/cuda/cuda-math-api/group__CUDA__MATH____HALF__MISC.html#group__CUDA__MATH____HALF__MISC
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <math.h>

#include <cuda.h>
#include <cuda_fp16.h>
#include <cufft.h>
#include <cufftXt.h>

#ifndef CUDA_DEVICE_NR
#define CUDA_DEVICE_NR 0
#endif

static const size_t FFT_LENS[] = {
    // 2^N : 64-point to 8M-point
    64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768, 65536, 131072, 262144, 524288, 1048576, 2097152, 4194304, 8388608,
    // decimal : 500-point to ~8M-point
    100, 200, 500, 1000, 2000, 4000, 8000, 10000, 16000, 20000, 32000, 50000, 64000, 100000, 128000, 200000, 250000, 256000,
    500000, 512000, 1000000, 1024000, 2000000, 2048000, 4000000, 4096000, 5000000, 8000000, 8192000
};

#define NITER      10  // iterations to use for determining average execution time
#define MAX_LFFT   8192000ULL

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Statistic
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

float mean(float* s, int n)
{
    float m = 0.0f;
    for (int i=0; i<n; i++) {
        m += s[i];
    }
    return m/float(n);
}

float stddev(float* s, int n)
{
    float m = mean(s, n);
    float v = 0.0f;
    for (int i=0; i<n; i++) {
        v += (s[i]-m)*(s[i]-m);
    }
    return sqrtf(v/float(n));
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CUDA wrappers
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define CUFFT_CALL(x) m_CUFFT_CALL(x,__FILE__,__LINE__)
void m_CUFFT_CALL(cufftResult_t iRet, const char* pcFile, const int iLine)
{
        if (iRet != CUFFT_SUCCESS) {
                (void) fprintf(stderr, "CuFFT ERROR: File <%s>, Line %d: error %d\n",
                               pcFile, iLine, iRet);
                cudaDeviceReset();
                exit(EXIT_FAILURE);
        }
        return;
}

#define CUDA_CALL(x) m_CUDA_CALL(x,__FILE__,__LINE__)
void m_CUDA_CALL(cudaError_t iRet, const char* pcFile, const int iLine)
{
        if (iRet != cudaSuccess) {
                (void) fprintf(stderr, "ERROR: File <%s>, Line %d: %s\n",
                               pcFile, iLine, cudaGetErrorString(iRet));
                cudaDeviceReset();
                exit(EXIT_FAILURE);
        }
        return;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Callbacks when using statically linked CUFFT
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef STATIC_CUFFT

__device__ cufftComplex cu_loadC(void *dataIn, size_t offset, void *callerInfo, void *sharedPointer)
{
    half2 h = ((half2*)dataIn)[offset];
    float2 f = __half22float2(h);
    return (cufftComplex)f;
}

__device__ cufftReal cu_loadR(void *dataIn, size_t offset, void *callerInfo, void *sharedPointer)
{
    const size_t word = offset/2;
    const size_t halfword = offset - 2*word; // mod(offset,2)  or   offset & 0x01
    const uint32_t ui32 = ((uint32_t*)dataIn)[word];
    const short i16 = ui32 >> (16*halfword);
    float f = __half2float(__short_as_half(i16));
    return f;
}

__device__ void cu_storeC(void *dataOut, size_t offset, cufftComplex element, void *callerInfo, void *sharedPointer)
{
    half2 h = __floats2half2_rn(element.x,element.y);
    ((half2*)dataOut)[offset] = h;
}

__device__ cufftCallbackLoadC cu_loadC_ptr = cu_loadC;
__device__ cufftCallbackLoadR cu_loadR_ptr = cu_loadR;
__device__ cufftCallbackStoreC cu_storeC_ptr = cu_storeC;

#endif // STATIC_CUFFT


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// BENCHMARKING
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int main(int argc, char **argv)
{
    cudaDeviceProp cudaDevProp;
    size_t max_batch, memfree, memtotal;

    float *d_idata;
    float *h_idata;
    cufftComplex *d_odata;
#ifdef NATIVE_FP32
    cufftHandle fftplan_r2c;
#endif
#ifdef STATIC_CUFFT
    cufftHandle fftplan_r2c_fp16;
    cufftHandle fftplan_c2c_fp16;
#endif
#ifdef NATIVE_FP16
    cufftHandle fftplan_r2cXt_fp16;
#endif

    cudaEvent_t tstart, tstop;
    float dt[NITER], dt_msec, dt_sigma, R_gsps, R_sigma;

    // Select device and do some preparations
    CUDA_CALL( cudaSetDevice(CUDA_DEVICE_NR) );
    CUDA_CALL( cudaGetDeviceProperties(&cudaDevProp, CUDA_DEVICE_NR) );
    printf("CUDA Device #%d : %s, Compute Capability %d.%d, %d threads/block, warpsize %d\n",
        CUDA_DEVICE_NR, cudaDevProp.name, cudaDevProp.major, cudaDevProp.minor,
        cudaDevProp.maxThreadsPerBlock, cudaDevProp.warpSize
    );

    CUDA_CALL( cudaEventCreate( &tstart ) );
    CUDA_CALL( cudaEventCreate( &tstop ) );

    CUDA_CALL( cudaMemGetInfo(&memfree, &memtotal) );
    max_batch = memfree / (2 * 2 * sizeof(cufftComplex) * MAX_LFFT);
    printf("Free memory of %.3f GB allows max. %zu-batch x %zu-pt FFT\n", memfree/1073741824.0, max_batch, (size_t)MAX_LFFT);

    CUDA_CALL( cudaHostAlloc( (void **)&h_idata, sizeof(cufftComplex)*MAX_LFFT*max_batch, cudaHostAllocDefault ) );
    CUDA_CALL( cudaMalloc( (void **)&d_idata, sizeof(cufftComplex)*MAX_LFFT*max_batch ) );
    CUDA_CALL( cudaMalloc( (void **)&d_odata, sizeof(cufftComplex)*MAX_LFFT*max_batch ) );
    for (size_t n=0; n<MAX_LFFT*max_batch*sizeof(cufftComplex)/sizeof(float); n++) {
        h_idata[n] = n % 1234;
    }

    printf("# Type             Lfft     Lbatch   T[ms]+-dT   R[Gs/s]+-dR\n");

    for (size_t fi = 0; fi < sizeof(FFT_LENS)/sizeof(size_t); fi++) {

        const size_t fftlen = FFT_LENS[fi];
        const size_t batch = (MAX_LFFT*max_batch)/fftlen;

#if defined(NATIVE_FP32) || defined(STATIC_CUFFT)
        // Layout
        int dimn[1] = {fftlen};         // DFT size
        int inembed[1] = {0};           // ignored for 1D xform
        int onembed[1] = {0};           // ignored for 1D xform
        int istride = 1, ostride = 1;   // step between successive in(out) elements
        int idist = fftlen;             // step between batches (R2C input)
        int odist = fftlen/2+1;         // step between batches (R2C output)
#endif

#ifdef NATIVE_FP32
        // Run a normal R2C FFT
        CUFFT_CALL( cufftPlanMany(&fftplan_r2c, 1, dimn,
            inembed, istride, idist,
            onembed, ostride, odist,
            CUFFT_R2C,
            batch)
        );
        for (size_t i=0; i<NITER; i++) {
            CUDA_CALL( cudaMemcpy( d_idata, h_idata, fftlen*sizeof(cufftComplex)*batch, cudaMemcpyHostToDevice) );
            CUDA_CALL( cudaMemset( d_odata, 0x00, fftlen*sizeof(cufftComplex)*batch ) );
            CUDA_CALL( cudaEventRecord(tstart, 0) );
            CUFFT_CALL( cufftExecR2C(fftplan_r2c, d_idata, d_odata) );
            CUDA_CALL( cudaEventRecord(tstop, 0) );
            CUDA_CALL( cudaEventSynchronize(tstop) );
            CUDA_CALL( cudaEventElapsedTime( &dt[i], tstart, tstop ) );
        }
        dt_msec = mean(dt, NITER);
        dt_sigma = stddev(dt, NITER);
        R_gsps = (1e-9*fftlen*batch)/(dt_msec*1e-3);
        R_sigma =  (R_gsps/(dt_msec*1e-3)) * dt_sigma*1e-3; // |dR/dt|*sigmat ; diff(1/x)=-1/x^2
        printf("%-12s %10zu %10zu %7.3f %5.3f %5.2f %3.2f\n", "r2c fp32", fftlen, batch, dt_msec, dt_sigma, R_gsps, R_sigma);
        CUFFT_CALL( cufftDestroy(fftplan_r2c) );
#endif

#ifdef STATIC_CUFFT
        cufftCallbackLoadR h_cuRbLoad;
        cufftCallbackLoadC h_cuCbLoad;
        cufftCallbackStoreC h_cuCbStore;
        CUDA_CALL( cudaMemcpyFromSymbol(&h_cuRbLoad, cu_loadR_ptr, sizeof(h_cuRbLoad)) );
        CUDA_CALL( cudaMemcpyFromSymbol(&h_cuCbLoad, cu_loadC_ptr, sizeof(h_cuCbLoad)) );
        CUDA_CALL( cudaMemcpyFromSymbol(&h_cuCbStore, cu_storeC_ptr, sizeof(h_cuCbStore)) );

        // Run a R2C FFT with callback on half2 data
        CUFFT_CALL( cufftPlanMany(&fftplan_r2c_fp16, 1, dimn,
            inembed, istride, idist,
            onembed, ostride, odist,
            CUFFT_R2C,
            batch)
        );
        CUFFT_CALL( cufftXtSetCallback(fftplan_r2c_fp16, (void **)&h_cuRbLoad, CUFFT_CB_LD_REAL, (void**)NULL) );
        CUFFT_CALL( cufftXtSetCallback(fftplan_r2c_fp16, (void **)&h_cuCbStore, CUFFT_CB_ST_COMPLEX, (void**)NULL) );
        for (size_t i=0; i<NITER; i++) {
            CUDA_CALL( cudaMemcpy( d_idata, h_idata, fftlen*sizeof(cufftComplex)*batch, cudaMemcpyHostToDevice) );
            CUDA_CALL( cudaMemset( d_odata, 0x00, fftlen*sizeof(cufftComplex)*batch ) );
            CUDA_CALL( cudaEventRecord(tstart, 0) );
            CUFFT_CALL( cufftExecR2C(fftplan_r2c_fp16, d_idata, d_odata) );
            CUDA_CALL( cudaEventRecord(tstop, 0) );
            CUDA_CALL( cudaEventSynchronize(tstop) );
            CUDA_CALL( cudaEventElapsedTime( &dt[i], tstart, tstop ) );
        }
        dt_msec = mean(dt, NITER);
        dt_sigma = stddev(dt, NITER);
        R_gsps = (1e-9*fftlen*batch)/(dt_msec*1e-3);
        R_sigma =  (R_gsps/(dt_msec*1e-3)) * dt_sigma*1e-3;
        printf("%-12s %10zu %10zu %7.3f %5.3f %5.2f %3.2f\n", "r2c fp16/32", fftlen, batch, dt_msec, dt_sigma, R_gsps, R_sigma);
        CUFFT_CALL( cufftDestroy(fftplan_r2c_fp16) );

        // Run a C2C FFT with callback on half2 data
        CUFFT_CALL( cufftPlanMany(&fftplan_c2c_fp16, 1, dimn,
            inembed, istride, idist,
            onembed, ostride, odist,
            CUFFT_C2C,
            batch)
        );
        CUDA_CALL( cudaMemcpyFromSymbol(&h_cuCbLoad, cu_loadC_ptr, sizeof(h_cuCbLoad)) );
        CUDA_CALL( cudaMemcpyFromSymbol(&h_cuCbStore, cu_storeC_ptr, sizeof(h_cuCbStore)) );
        CUFFT_CALL( cufftXtSetCallback(fftplan_c2c_fp16, (void **)&h_cuCbLoad, CUFFT_CB_LD_COMPLEX, (void**)NULL) );
        CUFFT_CALL( cufftXtSetCallback(fftplan_c2c_fp16, (void **)&h_cuCbStore, CUFFT_CB_ST_COMPLEX, (void**)NULL) );
        for (size_t i=0; i<NITER; i++) {
            CUDA_CALL( cudaMemcpy( d_idata, h_idata, fftlen*sizeof(cufftComplex)*batch, cudaMemcpyHostToDevice) );
            CUDA_CALL( cudaMemset( d_odata, 0x00, fftlen*sizeof(cufftComplex)*batch ) );
            CUDA_CALL( cudaEventRecord(tstart, 0) );
            CUFFT_CALL( cufftExecC2C(fftplan_c2c_fp16, (cufftComplex*)d_idata, d_odata, CUFFT_FORWARD) );
            CUDA_CALL( cudaEventRecord(tstop, 0) );
            CUDA_CALL( cudaEventSynchronize(tstop) );
            CUDA_CALL( cudaEventElapsedTime( &dt[i], tstart, tstop ) );
        }
        dt_msec = mean(dt, NITER);
        dt_sigma = stddev(dt, NITER);
        R_gsps = (1e-9*fftlen*batch)/(dt_msec*1e-3);
        R_sigma =  (R_gsps/(dt_msec*1e-3)) * dt_sigma*1e-3;
        printf("%-12s %10zu %10zu %7.3f %5.3f %5.2f %3.2f\n", "c2c fp16/32", fftlen, batch, dt_msec, dt_sigma, R_gsps, R_sigma);
        CUFFT_CALL( cufftDestroy(fftplan_c2c_fp16) );
#endif

#ifdef NATIVE_FP16
        // A CuFFT XT R2C plan with 16-bit data
        size_t worksizesXt[10];
        long long dimnXt[1] = { fftlen };
        CUFFT_CALL( cufftCreate(&fftplan_r2cXt_fp16) );
        CUFFT_CALL( cufftXtMakePlanMany(fftplan_r2cXt_fp16,
            1, dimnXt,
            NULL, 1, fftlen, /* inembed=NULL, ignored istride, ignored idist, */
            CUDA_R_16F,
            NULL, 1, fftlen/2+1, /* onembed=NULL, ignored ostride, ignored odist, */
            CUDA_C_16F,
            batch,
            worksizesXt,
            CUDA_C_16F)
        );
        for (size_t i=0; i<NITER; i++) {
            CUDA_CALL( cudaMemcpy( d_idata, h_idata, fftlen*sizeof(cufftComplex)*batch, cudaMemcpyHostToDevice) );
            CUDA_CALL( cudaMemset( d_odata, 0x00, fftlen*sizeof(cufftComplex)*batch ) );
            CUDA_CALL( cudaEventRecord(tstart, 0) );
            CUFFT_CALL( cufftXtExec(fftplan_r2cXt_fp16, d_idata, d_odata, CUFFT_FORWARD) );
            CUDA_CALL( cudaEventRecord(tstop, 0) );
            CUDA_CALL( cudaEventSynchronize(tstop) );
            CUDA_CALL( cudaEventElapsedTime( &dt[i], tstart, tstop ) );
        }
        dt_msec = mean(dt, NITER);
        dt_sigma = stddev(dt, NITER);
        R_gsps = (1e-9*fftlen*batch)/(dt_msec*1e-3);
        R_sigma =  (R_gsps/(dt_msec*1e-3)) * dt_sigma*1e-3;
        printf("%-12s %10zu %10zu %7.3f %5.3f %5.2f %3.2f\n", "r2c nat fp16", fftlen, batch, dt_msec, dt_sigma, R_gsps, R_sigma);
        CUFFT_CALL( cufftDestroy(fftplan_r2cXt_fp16) );
#endif

    }

    return 0;
}
