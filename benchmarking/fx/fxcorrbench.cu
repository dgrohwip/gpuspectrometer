////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Usage: fxcorrbench [gpu_id] [fx_nchan]
//
// Synthetic real-valued time domain data of 4 antennas x 2 pol are generated first.
//
// Next the FXCorrFFT4 class is used to transform time domain into frequency domain.
//
// The transform consists of loading sample data, rotating them by a per-antenna/pol
// 3rd order (cubic) fringe phase stopping polynomial (8 coefficient sets),
// and then CUFFT Fourier transforming the now complex valued time domain sample data
// into the frequency domain.
//
// Lastly, frequency domain data are cross-multiplied between antennas to form visibilities.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <cuda.h>
#include <cufft.h>

#define CHECK_TIMING 0
#include "cuda_utils.cu"

#define FX_FFT_INTERLEAVE_OUTPUT 0 // modifier for class FXCorrFFT4; 0 == keep the 4ant*2pol post-FFT signals separate

#include "decoder_3b32f_kernels.cu"
#include "arithmetic_fxcorr_cufftconfig.cu" // Class FXCorrFFT4 for phase-stopping FFT (F-step)
#include "arithmetic_fxcorr_kernels.cu"     // Kernels for X-step

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

size_t nchans_to_test[] = { 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768 };

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void usage(void)
{
    printf("Usage: fxcorrbench [gpu_id] [fx_nchan]\n");
}

void dbg_print_device_complexArr(void* d_data, size_t N)
{
    float2* h;
    CUDA_CALL( cudaHostAlloc((void **)&h, N*sizeof(float2), cudaHostAllocDefault) );
    CUDA_CALL( cudaMemcpy(h, d_data, N*sizeof(float2), cudaMemcpyDeviceToHost) );
    printf("dbg_print_device_complexArr(%p, %zu):\n", d_data, N);
    for (size_t n=0; n<N; n++) {
        printf(" %.3f%+.3fi ", h[n].x, h[n].y);
        if ((n+1) % 10 == 0) { printf("\n"); }
    }
    printf("\n");
    CUDA_CALL( cudaFreeHost(h) );
}

void dbg_print_device_realArr(void* d_data, size_t N)
{
    float* h;
    CUDA_CALL( cudaHostAlloc((void **)&h, N*sizeof(float), cudaHostAllocDefault) );
    CUDA_CALL( cudaMemcpy(h, d_data, N*sizeof(float), cudaMemcpyDeviceToHost) );
    printf("dbg_print_device_complexArr(%p, %zu):\n", d_data, N);
    for (size_t n=0; n<N; n++) {
        printf(" %.3f ", h[n]);
        if ((n+1) % 10 == 0) { printf("\n"); }
    }
    printf("\n");
    CUDA_CALL( cudaFreeHost(h) );
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int main(int argc, char** argv)
{
    const size_t PhasepolyOrder = 3;
    const size_t Nant = 4;
    const size_t Npol = 2;
    const size_t Nbase = (Nant*(Nant-1))/2;
    int nr_nchans_to_test = sizeof(nchans_to_test)/sizeof(nchans_to_test[0]);

    int device = 0;
    cudaDeviceProp cudaDevProp;
    size_t maxphysthreads;
    cudaEvent_t tstart, tstop, gstart, gstop;
    float dT_msec;

    float* d_td_signals[Nant*Npol];
    const float2* d_fd_antspols[Nant*Npol];

    FXCorrFFT4* fxcorrFFT; // CUDA FFT helper for fringe-stopping F-step
    float* polyX[Nant];    // X-polarization phase polynomial for one time segment
    float* polyY[Nant];    // Y-polarization phase polynomial for one time segment

    cufftComplex* d_fftOut;
    float2* d_viz;

    // Command line options
    if (argc > 1) {
        device = atoi(argv[1]);
    }
    if (argc > 2) {
        nchans_to_test[0] = atoi(argv[2]);
        nr_nchans_to_test = 1;
    }
    if ((argc > 3) || (device < 0) || (device > 8) || (nchans_to_test[0] < 32) || (argc > 1 && argv[1][0]=='-')) {
        usage();
        return -1;
    }

    // GPU setup
    CUDA_CALL( cudaSetDevice(device) );
    CUDA_CALL( cudaGetDeviceProperties(&cudaDevProp, device) );
    CUDA_CALL( cudaEventCreate( &tstart ) );
    CUDA_CALL( cudaEventCreate( &tstop ) );
    CUDA_CALL( cudaEventCreate( &gstart ) );
    CUDA_CALL( cudaEventCreate( &gstop ) );
    maxphysthreads = cudaDevProp.multiProcessorCount * cudaDevProp.maxThreadsPerMultiProcessor;
    printf("CUDA Device #%d : %s, Compute Capability %d.%d, %d threads/block, warpsize %d, %zu threads max.\n",
        device, cudaDevProp.name, cudaDevProp.major, cudaDevProp.minor,
        cudaDevProp.maxThreadsPerBlock, cudaDevProp.warpSize, maxphysthreads
    );

    // Fringe stopping phase polyomial
    //printf("Generating phase polynomial coeffs for pre-FFT continous per-sample sin/cos fringe stopping...\n");
    for (size_t n=0; n<Nant; n++) {
        polyX[n] = new float[PhasepolyOrder+1]();
        polyY[n] = new float[PhasepolyOrder+1]();
        assert(PhasepolyOrder >= 3);
        polyX[n][0] = 0.010f + 0.002f*n;
        polyX[n][1] = -1.7e-3f + 0.002f*n;
        polyX[n][2] = -3.1e-5f + 0.003f*n;
        polyX[n][3] = -2.7e-7f + 0.005f*n;
        polyY[n][0] = 0.010f + 0.002f*n;
        // [1..3] = 0.0f
    }

    // Benchmark various FFT lengths
    for (int test_idx=0; test_idx<nr_nchans_to_test; test_idx++) {

        // Number of FFT channels
        const size_t Nchan = nchans_to_test[test_idx];

        // Samples per antenna
        // 48 ms @ 2 GHz 1-pol = 96000000 complex values
        //  4 ms @ 2 GHz 1-pol =  8000000 complex values
        //  2 ms @ 2 GHz 1-pol =  4000000 complex values
        //  1 ms @ 2 GHz 1-pol =  2000000 complex values
        const size_t nsamplesperantennapol = 8000000;
        size_t nsamples = Nant * Npol * nsamplesperantennapol;
        nsamples -= (nsamples % (Npol*Nchan)); // make nsamples an integer multiple of 'nchan' in 2-pol

        // FFT details
        const size_t nfft = nsamples / Nchan;   // input data are float32 -> complex32 due to fringe stopping

        // Visibility datails
        size_t nbyteviz;
        nbyteviz = sizeof(float2) * Nchan * Nbase;  // visibilities
        nbyteviz += sizeof(float2) * Nchan * Nant;  // autocorrelations
        nbyteviz *= 4;                              // full-stokes

        // Time-domain data
        size_t nbyte = nsamples * sizeof(float2);
        //printf("Generating %zu-antenna %zu-pol time domain signals (float32; ~%.2f GByte) for %zu-channel FX...\n", Nant, Npol, nbyte/1073741824.0, Nchan);
        if (nbyte > 8ULL*1024*1024*1024) {
            printf("Built-in memory limit (8G) reached\n");
            return -1;
        }
        float* h_signal;
        CUDA_CALL( cudaMallocHost((void**)&h_signal, nsamplesperantennapol*sizeof(float)) );
        for (size_t n=0; n<Nant*Npol; n++) {
            for(size_t s=0; s<nsamplesperantennapol; s++) {
                h_signal[s] = ((s + n) % 1237) * (n + 1);
            }
            CUDA_CALL( cudaMalloc((void **)&d_td_signals[n], nsamplesperantennapol*sizeof(float)) );
            CUDA_CALL( cudaMemcpy(d_td_signals[n], h_signal, nsamplesperantennapol*sizeof(float), cudaMemcpyHostToDevice) );
        }
        CUDA_CALL( cudaFreeHost(h_signal) );
    
        // Initialize F-step with polynomial based fringe stopping
        //printf("Preparing F-step computing layout...\n");
        fxcorrFFT = new FXCorrFFT4(Nant, Npol, nsamplesperantennapol, Nchan, 0, 0);
        CUDA_CALL( cudaMalloc((void **)&d_fftOut, nsamplesperantennapol*Nant*Npol*sizeof(cufftComplex)) );
        for (size_t n=0; n<Nant*Npol; n++) {
            d_fd_antspols[n] = d_fftOut + n * fxcorrFFT->getTransformOdist();
        }

        // Initialize X-step
        size_t numBlocks, threadsPerBlock;
        prep_cu_fxcorr_4ant(numBlocks, threadsPerBlock, maxphysthreads, nsamples, Nchan);
        //printf("Prepared X-step computing layout:\n");
        //printf("    %zu blocks x %zu threads\n\n", numBlocks, threadsPerBlock);

        CUDA_CALL( cudaMalloc( (void **)&d_viz, nbyteviz) );
        CUDA_CALL( cudaMemset(d_viz, 0x00, nbyteviz) );

        // Call F-step once and discard; priming for benchmark
        fxcorrFFT->transform(
            PhasepolyOrder,
            (const float**)&d_td_signals[0], polyX,
            (const float**)&d_td_signals[Nant], polyY,
            d_fftOut
        );
        CUDA_CALL( cudaDeviceSynchronize() );

        ////////////////////////////////////////////////////////////

        // Start overall timing
        CUDA_CALL( cudaEventRecord(gstart) );
    
        // F-step
        printf("Executing: %zu-channel sin/cos poly order=%zu fringe stopped FX with autocorrelations\n", Nchan, PhasepolyOrder);
        CUDA_CALL( cudaEventRecord(tstart, 0) );
        fxcorrFFT->transform(
            PhasepolyOrder,
            (const float**)&d_td_signals[0], polyX,
            (const float**)&d_td_signals[Nant], polyY,
            d_fftOut
        );
        CUDA_CALL( cudaEventRecord(tstop, 0) );
        CUDA_CALL( cudaEventSynchronize(tstop) );
        CUDA_CALL( cudaEventElapsedTime(&dT_msec, tstart, tstop) );
        printf("    F:  %.3f ms   %.3f GS/s/ant/pol\n", dT_msec, ((float)nsamplesperantennapol / (1e6*dT_msec)));

        // X-step
        CUDA_CALL( cudaEventRecord(tstart, 0) );
        fxcorr_4ant_2pol_withAuto(numBlocks, threadsPerBlock, d_fd_antspols, d_viz, nsamplesperantennapol, Nchan );
        CUDA_CALL( cudaEventRecord(tstop, 0) );
        CUDA_CALL( cudaEventSynchronize(tstop) );
        CUDA_CALL( cudaEventElapsedTime(&dT_msec, tstart, tstop) );
        printf("    X:  %.3f ms   %.3f GS/s/ant/pol\n", dT_msec, ((float)nsamplesperantennapol / (1e6*dT_msec)));

        // Stop overall timing
        CUDA_CALL( cudaEventRecord(gstop) );

        ////////////////////////////////////////////////////////////

        CUDA_CALL( cudaDeviceSynchronize() );
        CUDA_CALL( cudaEventElapsedTime(&dT_msec, gstart, gstop) );
        printf("    FX: %.3f ms   %.3f GS/s/ant/pol\n\n", dT_msec, ((float)nsamplesperantennapol / (1e6*dT_msec)));

        // Clean up
        for (size_t n=0; n<Nant*Npol; n++) {
            CUDA_CALL( cudaFree(d_td_signals[n]) );
        }
        delete fxcorrFFT;
        CUDA_CALL( cudaFree(d_fftOut) );
        CUDA_CALL( cudaFree(d_viz) );

    }

    CUDA_CALL( cudaDeviceReset() );

    return 0;
}
