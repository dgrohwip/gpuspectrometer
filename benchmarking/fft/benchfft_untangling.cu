//
// Usage: ./benchfft_untangling [<deviceNr>]
//
// Benchmarks real-to-complex (N real -> N/2 complex without Nyquist)
// and complex-to-complex     (N/2 complex -> N/2 complex)
// and dual r2c via packed complex-to-complex (2*N/2 real -> packed 2*N/2 complex -> FFT -> N/2 complex -> untangle)
// discrete Fourier transforms of various sizes.
//
// TODO: is the indexing while untangling correct, we stop in the middle of the c2c length; maybe should untangle past the midpoint? then cannot do this in-place...

#include <assert.h>
#include <stdio.h>
#include <math.h>
#include <cuda.h>
#include <cufft.h>
#include <ctype.h>

#include <thrust/fill.h>
#include <thrust/device_ptr.h>

#define DO_WRITE_XMAC_FILES 0 // 1 to write post-accumulation spectrum into benchfft_untangling_xmac_<DFTlen>.out for each benchmarked DFT length

#define MIN_LFFT      64
#define MAX_LFFT      (1ULL*1024*1024)
#define MIN_BATCH     256ULL
#define MAX_SAMPLES   (MAX_LFFT*MIN_BATCH)
#define MAX_BATCH     (MAX_SAMPLES/MIN_LFFT)
#define MAX_CPLX_OUT  ((MIN_LFFT*MAX_BATCH)/2 + MAX_BATCH) // r2c fft
#define MAX(a,b)      (((a) > (b)) ? (a) : (b))

static const size_t DFT_LENS[] = {
    // 2^N : 64-point to 8M-point
    64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768, 65536, 131072, 262144, 524288, 1048576, 2097152, 4194304, 8388608,
    // decimal : 500-point to ~8M-point
    100, 200, 500, 1000, 2000, 4000, 8000, 10000, 16000, 20000, 32000, 50000, 64000, 100000, 128000, 200000, 250000, 256000,
    500000, 512000, 1000000, 1024000, 2000000, 2048000, 4000000, 4096000, 5000000, 8000000, 8192000
};

// Untangle the result of a c2c DFT that was used to perform a dual r2c DFT
__global__ void cu_untangle(float2* inout, size_t dftlen)
{
    size_t idx  = blockIdx.x * blockDim.x + threadIdx.x;
    size_t u    = idx % dftlen; // bin 0 ... dftlen-1
    size_t base = idx - u;      // memory offset of bin 0 in the series of DFTs
    if (u < dftlen/2) {
        // http://www.kennethmoreland.com/fftgpu/fftgpu.pdf Eq. 7 and Eq. 8
        float2 Hu   = inout[idx];                 // H(u)
        float2 HNu  = inout[base + (dftlen-1)-u]; // H(N-u)
        float F_r =  (Hu.x + HNu.x)/2;            // F(u)_re =  (H(u)_re + H(N-u)_re)/2
        float F_i =  (Hu.y - HNu.y)/2;            // F(u)_im =  (H(u)_im - H(N-u)_im)/2
        float G_r =  (Hu.y + HNu.y)/2;            // G(u)_re =  (H(u)_im + H(N-u)_im)/2
        float G_i = -(Hu.x - HNu.x)/2;            // G(u)_im = -(H(u)_re - H(N-u)_re)/2

        // Write back the untangled data
        // both spectra F, G are 0...(dftlen/2)-1 bins long
        // store the first  (F) into input area bins 0...(dftlen/2)-1          in-order
        // store the second (G) into input area bins (dftlen/2)-1...(dftlen/2) in reverse-order to avoid data overwrite
        inout[idx] = (float2){F_r, F_i};
        inout[base + (dftlen-1)-u] = (float2){G_r, G_i};
    }
}

// Untangle and form auto&cross power spectra from the result of a c2c DFT that was used to perform a dual r2c DFT
__global__ void cu_untangled_crosspow(float2* __restrict__ inout, const size_t dftlen)
{
    size_t idx  = blockIdx.x * blockDim.x + threadIdx.x;
    size_t u    = idx % dftlen; // bin 0 ... dftlen-1
    size_t base = idx - u;      // memory offset of bin 0 in the series of DFTs
    if (u < dftlen/2) {
        // http://www.kennethmoreland.com/fftgpu/fftgpu.pdf Eq. 7 and Eq. 8
        float2 Hu   = inout[idx];                 // H(u)
        float2 HNu  = inout[base + (dftlen-1)-u]; // H(N-u)
        float F_r =  (Hu.x + HNu.x);            // F(u)_re =  (H(u)_re + H(N-u)_re)/2 * 2
        float F_i =  (Hu.y - HNu.y);            // F(u)_im =  (H(u)_im - H(N-u)_im)/2 * 2
        float G_r =  (Hu.y + HNu.y);            // G(u)_re =  (H(u)_im + H(N-u)_im)/2 * 2
        float G_i = -(Hu.x - HNu.x);            // G(u)_im = -(H(u)_re - H(N-u)_re)/2 * 2

        // |F|^2, |G|^2
        float FF = (F_r*F_r + F_i*F_i)/4;
        float GG = (G_r*G_r + G_i*G_i)/4;

        // F*conj(G) = (F_r + i*F_i)(G_r - i*G_i) = F_r*G_r+F_i*G_i + i(F_i*G_r - G_i*F_r)
        float FG_r = F_r*G_r + F_i*G_i;
        float FG_i = F_i*G_r - F_r*G_i;

        // Write back the untangled and cross-multiplied data
        inout[idx] = (float2){FF, GG};
        inout[base + (dftlen-1)-u] = (float2){FG_i, FG_r}; // swapped on purpose, so not only float2[] array is reverse-order but float2.x,.y elements are too

        // Accumulate, slow, would need reduction tree
        //atomicAdd(&specaccu[u].x, FF);
        //atomicAdd(&specaccu[u].y, GG);
        //atomicAdd(&specaccu[u].z, FG_r);
        //atomicAdd(&specaccu[u].w, FG_i);
    }
}

// Return a best-fit grid layout ('nBlk' and 'nThrd') to use when invoking kernel cu_untangled_crosspow_reduce().
void prep_untangled_crosspow_reduce(size_t &nblk, size_t &nthr, const size_t maxphysthreads, const size_t veclen, const size_t nvecs)
{
    size_t end = veclen*nvecs;
    nthr = 64;
    while ((veclen % nthr) > 0) {
        nthr--;
    }
    if (veclen < maxphysthreads) {
        nblk = (maxphysthreads - (maxphysthreads%veclen)) / nthr;
    } else {
        nblk = veclen / nthr;
    }
    size_t stride = nblk * nthr;
    assert((stride % veclen) == 0);
}

// Accumulate spectra
__global__ void cu_untangled_crosspow_reduce(float* __restrict__ in, const size_t veclen, const size_t nvecs, float* accu)
{
    __shared__ float tmp[64];
    const int end = veclen * nvecs;
    int N = blockDim.x * gridDim.x;
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    int bin = idx % veclen;

    // bin content: [ff0 gg0 ff1 gg1 ff2 gg2 .. ff<dftlen/2-1> gg<dftlen/2-1> fg_re<dftlen/2-1> fg_im<dftlen/2-1> .. fg_re1 fg_im1 fg_re0 fg_im0]
    // size       : 4 x dftlen floats
    tmp[threadIdx.x] = 0.0f;
    __syncthreads();

    while (idx < end) {
        float v = in[idx];
        tmp[threadIdx.x] += v;
        idx += N; // N must be a multiple of 'veclen' otherwise we land in the wrong bin
    }
    __syncthreads();

#if 0
    atomicAdd(accu+bin, tmp[threadIdx.x]);
#else
    if (bin < veclen/2) {
        atomicAdd(accu+bin, tmp[threadIdx.x]);
    } else {
        // Un-reverse the second half of the vector
        atomicAdd(accu + veclen/2+(veclen-1)-bin, tmp[threadIdx.x]);
    }
#endif
}



///////////////////////////////////////////////////////////////////////////////////////////////

#define CUFFT_CALL(x) m_CUFFT_CALL(x,__FILE__,__LINE__)
void m_CUFFT_CALL(cufftResult_t iRet, const char* pcFile, const int iLine)
{
        if (iRet != CUFFT_SUCCESS) {
                (void) fprintf(stderr, "CuFFT ERROR: File <%s>, Line %d: error %d\n",
                               pcFile, iLine, iRet);
                cudaDeviceReset();
                exit(EXIT_FAILURE);
        }
        return;
}

#define CUDA_CALL(x) m_CUDA_CALL(x,__FILE__,__LINE__)
void m_CUDA_CALL(cudaError_t iRet, const char* pcFile, const int iLine)
{
        if (iRet != cudaSuccess) {
                (void) fprintf(stderr, "ERROR: File <%s>, Line %d: %s\n",
                               pcFile, iLine, cudaGetErrorString(iRet));
                cudaDeviceReset();
                exit(EXIT_FAILURE);
        }
        return;
}

///////////////////////////////////////////////////////////////////////////////////////////////

int main(int argc, char **argv)
{
    cudaDeviceProp cudaDevProp;
    cudaEvent_t tstart, tstop;
    cudaError_t err;
    size_t maxphysthreads;

    float dt_msec, *d_idata, *h_idata;
    cufftComplex *d_odata;
    float *d_oaccu, *h_oaccu;

    // Select device and do some preparations
    int device = 0;
    if ((argc >= 2) && isdigit(argv[1][0])) {
        device = argv[1][0] - '0';
    }
    CUDA_CALL( cudaSetDevice(device) );
    CUDA_CALL( cudaGetDeviceProperties(&cudaDevProp, device) );
    maxphysthreads = cudaDevProp.multiProcessorCount * cudaDevProp.maxThreadsPerMultiProcessor;
    printf("CUDA Device #%d : %s, Compute Capability %d.%d, %d threads/block, warpsize %d\n",
        device, cudaDevProp.name, cudaDevProp.major, cudaDevProp.minor,
        cudaDevProp.maxThreadsPerBlock, cudaDevProp.warpSize
    );

    CUDA_CALL( cudaEventCreate( &tstart ) );
    CUDA_CALL( cudaEventCreate( &tstop ) );

    CUDA_CALL( cudaHostAlloc( (void **)&h_idata, sizeof(float)*MAX_SAMPLES, cudaHostAllocDefault ) );
    CUDA_CALL( cudaMalloc( (void **)&d_idata, sizeof(cufftReal)*MAX_SAMPLES ) );
    CUDA_CALL( cudaMalloc( (void **)&d_odata, sizeof(cufftComplex)*MAX_CPLX_OUT ) );
    for (size_t n=0; n<MAX_SAMPLES; n++) {
        h_idata[n] = n % 1234;
    }

    printf("                -- N-point r2c DFT --         -- N/2-point c2c DFT --     --N/2-pt dual r2c via c2c--    --w/ xmac--\n");
    printf("    Lbatch     Nreals T_r2c[ms]  R[Gs/s]     Ncplex T_c2c[ms]  R[Gs/s]     Nreals T_c2c[ms]  R[Gs/s]  T_all[ms]  R[Gs/s]\n");

    for (size_t fi = 0; fi < sizeof(DFT_LENS)/sizeof(size_t); fi++) {

        const size_t r2c_dftlen = DFT_LENS[fi];
        const size_t c2c_dftlen = DFT_LENS[fi]/2;
        size_t batch = (MAX_SAMPLES)/r2c_dftlen;

        // DFT Plans
        cufftHandle r2c_fftplan;
        cufftHandle c2c_fftplan;
        int dimn[1] = {(int)r2c_dftlen};    // DFT size
        int inembed[1] = {0};               // ignored for 1D xform
        int onembed[1] = {0};               // ignored for 1D xform
        int istride = 1, ostride = 1;       // step between successive in(out) elements
        int r2c_odist = r2c_dftlen/2 + 0;   // step between batches (x2C output = complex); N/2+0 to discard R2C Nyquist, N/2+1 to keep R2C Nyq
        CUFFT_CALL( cufftPlanMany(&r2c_fftplan, 1, dimn,
            inembed, istride, r2c_dftlen,
            onembed, ostride, r2c_odist,
            CUFFT_R2C,
            batch)
        );
        dimn[0] = c2c_dftlen;
        CUFFT_CALL( cufftPlanMany(&c2c_fftplan, 1, dimn,
            inembed, istride, c2c_dftlen,
            onembed, ostride, c2c_dftlen,
            CUFFT_C2C,
            batch)
        );
        #if defined(CUDA_VERSION) && (CUDA_VERSION < 8000)
        CUFFT_CALL( cufftSetCompatibilityMode(r2c_fftplan, CUFFT_COMPATIBILITY_NATIVE) );
        CUFFT_CALL( cufftSetCompatibilityMode(c2c_fftplan, CUFFT_COMPATIBILITY_NATIVE) );
        #endif

        // Real-to-complex FFT : input N reals, output N/2(+1) complex
        CUDA_CALL( cudaMemcpy( d_idata, h_idata, r2c_dftlen*sizeof(cufftReal)*batch, cudaMemcpyHostToDevice) );
        CUDA_CALL( cudaMemset( d_odata, 0x00, r2c_odist*sizeof(cufftComplex)*batch ) );

        CUDA_CALL( cudaEventRecord(tstart) );
        CUFFT_CALL( cufftExecR2C(r2c_fftplan, (cufftReal*)d_idata, d_odata) );
        CUDA_CALL( cudaEventRecord(tstop) );

        CUDA_CALL( cudaEventSynchronize(tstop) );
        CUDA_CALL( cudaEventElapsedTime( &dt_msec, tstart, tstop ) );
        printf("%10zu %10zu %9.3f %8.1f ", batch, r2c_dftlen, dt_msec, (1e-6*r2c_dftlen*batch)/dt_msec);

        // Complex-to-complex : input M complex, output M complex
        //                      M=N/2 i.e. pairs of real treated as complex
        CUDA_CALL( cudaMemcpy( d_idata, h_idata, c2c_dftlen*sizeof(cufftComplex)*batch, cudaMemcpyHostToDevice) );
        CUDA_CALL( cudaMemset( d_odata, 0x00, c2c_dftlen*sizeof(cufftComplex)*batch ) );

        CUDA_CALL( cudaEventRecord(tstart) );
        CUFFT_CALL( cufftExecC2C(c2c_fftplan, (cufftComplex*)d_idata, d_odata, CUFFT_FORWARD) );
        CUDA_CALL( cudaEventRecord(tstop) );

        CUDA_CALL( cudaEventSynchronize(tstop) );
        CUDA_CALL( cudaEventElapsedTime( &dt_msec, tstart, tstop ) );
        printf("%10zu %9.3f %8.1f ", c2c_dftlen, dt_msec, (1e-6*c2c_dftlen*batch)/dt_msec);

        // Dual r2c via one c2c : input M complex (c = x + iy), output M complex (C=DFT(x+iy)),
        //                        untangle to get X=DFT(x) and Y=DFT(y) that are M-point r2c DFTs
        //                        M=N/2 i.e. pairs of real treated as complex
        CUDA_CALL( cudaMemcpy( d_idata, h_idata, c2c_dftlen*sizeof(cufftComplex)*batch, cudaMemcpyHostToDevice) );
        CUDA_CALL( cudaMemset( d_odata, 0x00, c2c_dftlen*sizeof(cufftComplex)*batch ) );

        CUDA_CALL( cudaEventRecord(tstart) );
        CUFFT_CALL( cufftExecC2C(c2c_fftplan, (cufftComplex*)d_idata, d_odata, CUFFT_FORWARD) );

        size_t nThr = 64;
        size_t nBlks = (c2c_dftlen*batch)/nThr;
        cu_untangle <<<nBlks,nThr>>> ((float2*)d_odata, c2c_dftlen); // M-point c2c results in, 2 x M-point r2c results out
        err = cudaGetLastError();
        if (err != cudaSuccess) {
            fprintf(stderr, "  %s<<<x,y>>>: Error: %s\n", "cu_untangle", cudaGetErrorString(err));
        }

        CUDA_CALL( cudaEventRecord(tstop) );
        CUDA_CALL( cudaEventSynchronize(tstop) );
        CUDA_CALL( cudaEventElapsedTime( &dt_msec, tstart, tstop ) );

        printf("%10zu %9.3f %8.1f", c2c_dftlen, dt_msec, (1e-6*(2*c2c_dftlen)*batch)/dt_msec);

        // Dual r2c via one c2c : as above, but untangle step includes spectral cross-product
        CUDA_CALL( cudaHostAlloc( (void **)&h_oaccu, sizeof(float)*c2c_dftlen*4, cudaHostAllocDefault ) );
        CUDA_CALL( cudaMalloc( (void **)&d_oaccu, sizeof(float)*c2c_dftlen*4 ) );
        CUDA_CALL( cudaMemset( d_oaccu, 0x00, sizeof(float)*c2c_dftlen*4 ) );
        CUDA_CALL( cudaMemset( d_odata, 0x00, c2c_dftlen*sizeof(cufftComplex)*batch ) );

        CUDA_CALL( cudaEventRecord(tstart) );
        CUFFT_CALL( cufftExecC2C(c2c_fftplan, (cufftComplex*)d_idata, d_odata, CUFFT_FORWARD) );

        thrust::device_ptr<float> dev_ptr((float*)d_odata);
        thrust::fill(dev_ptr, dev_ptr + batch*c2c_dftlen*2, 1.0f);
        cu_untangled_crosspow <<<nBlks,nThr>>> ((float2*)d_odata, c2c_dftlen); // M-point c2c results in, 2 x M-point r2c temp, results out with cross-pols
        err = cudaGetLastError();
        if (err != cudaSuccess) {
            fprintf(stderr, "  %s<<<x,y>>>: Error: %s\n", "cu_untangled_crosspow", cudaGetErrorString(err));
        }

        size_t veclen = c2c_dftlen * 2, nvecs = batch; // 2 of M-point r2c out
        prep_untangled_crosspow_reduce(/*out:*/nBlks,nThr, /*derived from:*/maxphysthreads,veclen,nvecs);
        //thrust::device_ptr<float> dev_ptr((float*)d_odata);
        //thrust::fill(dev_ptr, dev_ptr + veclen*nvecs, 1.0f);
        cu_untangled_crosspow_reduce <<<nBlks,nThr>>> ((float*)d_odata, veclen, nvecs, (float*)d_oaccu);
        err = cudaGetLastError();
        if (err != cudaSuccess) {
            fprintf(stderr, "  %s<<<x,y>>>: Error: %s\n", "cu_untangled_crosspow_reduce", cudaGetErrorString(err));
        }

        CUDA_CALL( cudaEventRecord(tstop) );
        CUDA_CALL( cudaMemcpy(h_oaccu, d_oaccu, sizeof(float)*c2c_dftlen*4, cudaMemcpyDeviceToHost) );
        CUDA_CALL( cudaEventSynchronize(tstop) );
        CUDA_CALL( cudaEventElapsedTime( &dt_msec, tstart, tstop ) );
        if (DO_WRITE_XMAC_FILES) {
            char fname[120];
            snprintf(fname, sizeof(fname)-1, "benchfft_untangling_xmac_%zu.out", c2c_dftlen);
            FILE* f = fopen(fname, "w");
            for (size_t bin=0; bin<veclen/4; bin++) {
                float XX = h_oaccu[2*bin+0], YY = h_oaccu[2*bin+1];
                float XYre = h_oaccu[veclen/2 + 2*bin+0], XYim = h_oaccu[veclen/2 + 2*bin+1];
                fprintf(f, "%7zu %8.5f %8.5f %8.5f %8.5f\n", bin, XX, YY, XYre, XYim);
            }
            fclose(f);
        }
        CUDA_CALL( cudaFreeHost(h_oaccu) );
        CUDA_CALL( cudaFree(d_oaccu) );

        printf("  %9.3f %8.1f", dt_msec, (1e-6*(2*c2c_dftlen)*batch)/dt_msec);

        // Cleanup
        printf("\n");
        CUFFT_CALL( cufftDestroy(r2c_fftplan) );
        CUFFT_CALL( cufftDestroy(c2c_fftplan) );
    }

    return 0;
}
