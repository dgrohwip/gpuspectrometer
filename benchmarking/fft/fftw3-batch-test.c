//
// Simple FFTW3 benchmarking of "batched" 1D transforms.
//
// (C) 2015 Jan Wagner
//
// Compares three approaches:
//   1) Simple FFTW3 call, input and output data are copied from/to larger array
//   2) Simple FFTW3 call that allows specifying new pointers for input and output in larger array
//   3) Advanced FFTW3 for planning many i.e. "batched" transforms
//
// Derived from FFTW3 examples found on the internet.
//
// Usage:   ./fftw3-batch-test <FFT-length> <iterations>
//

/* Makefile :

CFLAGS = -Wall -O3 -g
LDFLAGS = -lfftw3f -lgsl -lgslcblas

all: fftw3-batch-test

fftw3-batch-test: fftw3-batch-test.o

clean:
        rm fftw3-batch-test fftw3-batch-test.o
*/

#define HAVE_GSL 1              // 1 if GSL is available, used for faster random number generation

#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <sys/time.h>
#include <time.h>               // for RNG seed
#include <fftw3.h>              // for Fourier Transforms
#if HAVE_GSL
#include <gsl/gsl_rng.h>        // for random number generation
#endif

#define FFT_PREP_MODE   FFTW_PATIENT      // can be FFTW_ESTIMATE, FFTW_PATIENT, FFTW_MEASURE
#define FFT_FLAG        0                 // can be 0 or FFTW_DESTROY_INPUT

void test_FFTW_1D_simple(size_t, size_t);            // same plan, repeated on large array by copying input, copying output to new locations
void test_FFTW_1D_simple_newarray(size_t, size_t);   // same plan, repeated -"- via "New-Array Execute Functions"
void test_FFTW_1D_advanced(size_t, size_t);          // same plan, repeated via FFTW internal batch mode

void initialiseData(float*, size_t);
int prng_seed;

#ifdef HAVE_THREADS
int nthreads = 8;
#endif

int main(int argc, char** argv)
{
    size_t L = 128;
    size_t N = 8192;

    prng_seed = 0;

    if (argc >= 2) {
        L = atoi(argv[1]);
    }
    if (argc >= 3) {
        N = atoi(argv[2]);
    }

#ifdef HAVE_THREADS
    int err = fftwf_init_threads();
    if (err == 0) {
        printf("fftw3 thread creation error : %d\n", err);
        return -1;
    }
    if (argc >= 4) {
        nthreads = atoi(argv[3]);
    }
#endif

    printf("Benchmark for %zu-point r2c FFT over %zu vectors\n", L,N);

    test_FFTW_1D_simple(L,N);
    test_FFTW_1D_simple_newarray(L,N);
    test_FFTW_1D_advanced(L,N);

    return 0;
}

void test_FFTW_1D_simple(const size_t L, const size_t N)
{
    struct timeval tstart, tstop;
    double dT = 0;
    size_t k;

    float* in = fftwf_malloc(N*L*sizeof(float));
    fftwf_complex* out = fftwf_malloc(N*L*sizeof(fftwf_complex));
    initialiseData(in, N*L);

    fftwf_plan fp = fftwf_plan_dft_r2c_1d(L, in, out, FFT_PREP_MODE|FFT_FLAG);

    gettimeofday(&tstart, NULL);
    for (k=0; k<N; k++)
    {
        // Simple FFTW Interface:
        // need to move input data into input area,
        // execute the FFT,
        // then move results from output area to final destination
        memmove(in+k*L, in, L*sizeof(float));
        fftwf_execute(fp);
        memmove(out+k*L, out, L*sizeof(float));
    }

    gettimeofday(&tstop, NULL);
    dT = (tstop.tv_sec - tstart.tv_sec) + 1e-6*(tstop.tv_usec - tstart.tv_usec);
    printf("test_FFTW_1D_simple   : %.5f ms, %6.2f Ms/s : mempcy in&out\n", 1e3*dT, 1e-6*L*N/dT);

    gettimeofday(&tstart, NULL);
    for (k=0; k<N; k++)
    {
        // now assume FFT output does not need to be moved elsewhere
        memmove(in+k*L, in, L*sizeof(float));
        fftwf_execute(fp);
    }
    gettimeofday(&tstop, NULL);
    dT = (tstop.tv_sec - tstart.tv_sec) + 1e-6*(tstop.tv_usec - tstart.tv_usec);
    printf("test_FFTW_1D_simple   : %.5f ms, %6.2f Ms/s : memcpy in only\n", 1e3*dT, 1e-6*L*N/dT);

    gettimeofday(&tstart, NULL);
    for (k=0; k<N; k++)
    {
        // now assume FFT input and output do not need to be moved
        fftwf_execute(fp);
    }
    gettimeofday(&tstop, NULL);
    dT = (tstop.tv_sec - tstart.tv_sec) + 1e-6*(tstop.tv_usec - tstart.tv_usec);
    printf("test_FFTW_1D_simple   : %.5f ms, %6.2f Ms/s : no memcpy\n", 1e3*dT, 1e-6*L*N/dT);

    fftwf_destroy_plan(fp);
    fftwf_free(in);
    fftwf_free(out);
    fftwf_cleanup();
}


void test_FFTW_1D_simple_newarray(size_t L, size_t N)
{
    struct timeval tstart, tstop;
    double dT = 0;
    size_t k;

    float* in = fftwf_malloc(N*L*sizeof(float));
    fftwf_complex* out = fftwf_malloc(N*L*sizeof(fftwf_complex));
    initialiseData(in, N*L);

#ifdef HAVE_THREADS
    fftwf_plan_with_nthreads(nthreads);
    printf("Using %d threads\n", nthreads);
#endif

    fftwf_plan fp = fftwf_plan_dft_r2c_1d(L, in, out, FFT_PREP_MODE|FFT_FLAG);
    if (fp == NULL)
    {
        printf("Warning: test_FFTW_1D_simple_newarray() FFT planning failed!\n");
    }

    gettimeofday(&tstart, NULL);
    for (k=0; k<N; k++)
    {
        // FFTW Advanced Interface:
        // execute FFT with input&output array locations that
        // are different from locations used at the planning stage
        fftwf_execute_dft_r2c(fp, in + L*k, out + L*k);
    }

    gettimeofday(&tstop, NULL);
    dT = (tstop.tv_sec - tstart.tv_sec) + 1e-6*(tstop.tv_usec - tstart.tv_usec);
    printf("test_FFTW_1D_newarray : %.5f ms, %6.2f Ms/s\n", 1e3*dT, 1e-6*L*N/dT);

    fftwf_destroy_plan(fp);
    fftwf_free(in);
    fftwf_free(out);
    fftwf_cleanup();
}


void test_FFTW_1D_advanced(size_t L, size_t N)
{
    struct timeval tstart, tstop;
    double dT = 0;

    float* in = fftwf_malloc(N*L*sizeof(float));
    fftwf_complex* out = fftwf_malloc(N*L*sizeof(fftwf_complex));
    initialiseData(in, N*L);

#ifdef HAVE_THREADS
    fftwf_plan_with_nthreads(nthreads);
    printf("Using %d threads\n", nthreads);
#endif

    const int dims[1] = {L};
    fftwf_plan fp = fftwf_plan_many_dft_r2c(1, dims, N,
            in,  NULL, N, 1,
            out, NULL, N, 1,
            FFT_PREP_MODE|FFT_FLAG);
    if (fp == NULL)
    {
        printf("Warning: test_FFTW_1D_advanced() FFT planning failed!\n");
    }

    gettimeofday(&tstart, NULL);
    // FFTW Advanced Interface: execute all FFTs at once
    fftwf_execute(fp);
    gettimeofday(&tstop, NULL);

    dT = (tstop.tv_sec - tstart.tv_sec) + 1e-6*(tstop.tv_usec - tstart.tv_usec);
    printf("test_FFTW_1D_advanced : %.5f ms, %6.2f Ms/s\n", 1e3*dT, 1e-6*L*N/dT);

    fftwf_destroy_plan(fp);
    fftwf_free(in);
    fftwf_free(out);
    fftwf_cleanup();
}

void initialiseData(float* input, size_t len)
{
    size_t i;
#if HAVE_GSL
    gsl_rng* rng = gsl_rng_alloc(gsl_rng_mt19937);
    gsl_rng_set(rng, prng_seed);
    for (i=0; i<len; i++) {
        input[i] = gsl_rng_uniform(rng);
    }
#else
    for (i=0; i<len; i++) {
        input[i] = (float)rand()/(float)(RAND_MAX/5.0);
    }
#endif

}

