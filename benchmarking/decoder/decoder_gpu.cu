/////////////////////////////////////////////////////////////////////////////////////
//
// Basic CUDA implementation of 2-bit decoding (and perhaps other things),
// most of the overhead is for CUDA setup...
//
/////////////////////////////////////////////////////////////////////////////////////
//
// TODO:
// 1) make sure user buffers were allocated with cudaMallocHost() to get pinned memory,
//    also see http://www.cs.virginia.edu/~mwb7w/cuda_support/pinned_tradeoff.html
// 2) now device memory is allocated here in init() once, is re-used on each 
//    call to submit(), but it might be tidier to allow "deinit()" or similar for
//    clean-up invoked by user...
// 3) allow user selection of card
//
// Helpful:
// - Grid#, block#, thread#:
//     http://cs.calvin.edu/curriculum/cs/374/CUDA/CUDA-Thread-Indexing-Cheatsheet.pdf 
//  -Multi-stream async DMA and computation:
//     http://devblogs.nvidia.com/parallelforall/how-overlap-data-transfers-cuda-cc 
/////////////////////////////////////////////////////////////////////////////////////

// Note: 
// nvcc -DCUDA_DEVICE_NR=x   should be used to select card (0=Titan X, 1=Tesla K40m)

#include "cuda_utils.cu"
#include "decoder_2b32f_kernels.cu"
#include "decoder_8b32f_kernels.cu"
#include "arithmetic_xmac_kernels.cu"

#include "decoder.h"
#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>
#include <cufft.h>
#include <malloc.h>

// Generic CUDA related
static cudaDeviceProp cudaDevProp;  // device properties, used to grid the data
#define CHECK_TIMING  1             // 1 to check time for commands, 0 to run without checks
#define ASYNC_MODE    0             // 1 for interleaved memcpy and processing, 0 for synchronous
#define DISABLE_MEMORY_IN 0         // 1 to benchmark without any memory transfers Host->GPU
#define DISABLE_MEMORY_OUT 0        // 1 to benchmark without any memory transfers GPU->Host

#define SAMPLES_PER_BYTE 4          // 4 for 2-bit packed, 1 for 2-bit zero-padded, 1 for 3-bit zero-padded
#define BITS_PER_SAMPLE  2

// Threading/streaming related
struct decode_context_gpu_tt;
struct decode_descriptor_gpu_tt;
#define MAX_STREAMS 16

typedef struct decode_descriptor_gpu_tt {
        size_t       Nstreams;
        cudaStream_t sid[MAX_STREAMS];
        cudaEvent_t  start;
        cudaEvent_t  stop;
        struct decode_context_gpu_tt* ctx[MAX_STREAMS];
} decode_descriptor_gpu_t;

typedef struct decode_context_gpu_tt {
        cudaEvent_t     start;
        cudaEvent_t     stop;
        uint64_t        max_physical_threads;
        size_t          src_bufsize;
        size_t          dst_bufsize;
        const uint32_t* h_src;
        const uint8_t*  d_src;
        float*          h_dst;
        float*          d_dst;
        size_t          Nsamples;
        int             rank;
        int             rc;
#ifdef EXTRA_PROCESSING
        // Related to additional processing
        size_t          fftlen;   // number of real (float1) input samples
        size_t          numfft;   // number of FFTs per input buffer
        cufftHandle     fftplan;  // CUFFT R2C, in-place transform (1st Nyuist of fftlen real = fftlen/2+1 complex samples)
        float*          d_fftout;
        float4*         d_xmac;   // dual-pol accumulation, each float4 = { <X*X>, Re{<X*Y'>}, Im{<X*Y'>}, <Y*Y> } for one float2 pair
        float*          h_xmac;
        size_t          xmaclen;  // xmac length in real (float1) samples = 2*fftlen
#endif
} decode_context_gpu_t;


/////////////////////////////////////////////////////////////////////////////////////
// Memory alloc wrapper
/////////////////////////////////////////////////////////////////////////////////////
void* decode_malloc_gpu(size_t bytes)
{
        void* p;
        CUDA_CALL( cudaMallocHost(&p, bytes) );
        //  CUDA_CALL( cudaHostAlloc(&p, bytes, cudaHostAllocPortable) );
        return p;
}

/////////////////////////////////////////////////////////////////////////////////////
// Generic threading
//	
// decode_2bit_1channel_gpu_init   : prepares a parallel decoding task
// decode_2bit_1channel_gpu_submit : starts parallel execution of decoding subtasks
// decode_2bit_1channel_gpu_join   : blocks until all subtasks have completed
//
/////////////////////////////////////////////////////////////////////////////////////
void* decode_2bit_1channel_gpu_init(const uint32_t** src, float** dst, const size_t Nsamp, const size_t Nstreams)
{
        decode_descriptor_gpu_t* d;
        int cdevs, dd;
        size_t maxphysthreads;
        size_t i;

        // Generic CUDA init
        CUDA_CALL( cudaGetDeviceCount(&cdevs) );
        if (0 == cdevs) {
                (void) fprintf(stderr, "ERROR: No CUDA-capable device found!\n");
        }
        for (dd = 0; dd < cdevs; dd++) {
                CUDA_CALL( cudaGetDeviceProperties(&cudaDevProp, dd) );
                printf("CUDA Device #%d : %s, Compute Capability %d.%d, %d threads/block  %s\n",
                       dd, cudaDevProp.name, cudaDevProp.major, cudaDevProp.minor, cudaDevProp.maxThreadsPerBlock,
                       (dd == CUDA_DEVICE_NR) ? "(selected)" : ""
                );
        }

        CUDA_CALL( cudaSetDevice(CUDA_DEVICE_NR) );
        CUDA_CALL( cudaGetDeviceProperties(&cudaDevProp, CUDA_DEVICE_NR) );
        maxphysthreads = cudaDevProp.multiProcessorCount * cudaDevProp.maxThreadsPerMultiProcessor;
        printf("CUDA device %d has %d multiprocessors, max. %zu physical threads.\n", 
                CUDA_DEVICE_NR, cudaDevProp.multiProcessorCount, maxphysthreads
        );
        //CUDA_CALL( cudaFuncSetCacheConfig(cu_decode_2bit1ch_1D_v3, cudaFuncCachePreferShared) );
        //CUDA_CALL( cudaDeviceSetSharedMemConfig(cudaSharedMemBankSizeEightByte) );

        // Overall descriptor of parallel decoding task
        d = (decode_descriptor_gpu_t*)malloc(sizeof(decode_descriptor_gpu_t));
        memset((void*)d, 0, sizeof(decode_descriptor_gpu_t));
        CUDA_CALL( cudaEventCreate(&d->start) );
        CUDA_CALL( cudaEventCreate(&d->stop) );

        CUDA_TIMING_START( d->start, 0 );

        // Task contexts for individual decoding subtasks
        d->Nstreams = Nstreams;
        for (i = 0; i < Nstreams; i++) {

                decode_context_gpu_t* ctx = (decode_context_gpu_t*)malloc(sizeof(decode_context_gpu_t));
                memset((void*)ctx, 0, sizeof(decode_context_gpu_t));

                ctx->max_physical_threads = maxphysthreads;
                ctx->Nsamples = Nsamp;
                ctx->h_src = src[i];
                ctx->h_dst = dst[i];
                ctx->rc = -1;
                ctx->rank = i;
                ctx->src_bufsize = sizeof(char)*Nsamp/SAMPLES_PER_BYTE;
                ctx->dst_bufsize = sizeof(float)*Nsamp;
                CUDA_CALL( cudaEventCreate(&ctx->start) );
                CUDA_CALL( cudaEventCreate(&ctx->stop) );

                // Allocate device memory: memory is reused in every submit(), no need for cudaFree()!
                CUDA_CALL( cudaMalloc((void**)&(ctx->d_src), ctx->src_bufsize + 16) );
                CUDA_CALL( cudaMalloc((void**)&(ctx->d_dst), ctx->dst_bufsize + 16) );
                CUDA_CALL( cudaMemset((void*)(ctx->d_src), 0x11, ctx->src_bufsize) );
                CUDA_CALL( cudaMemset((void*)(ctx->d_dst), 0x22, ctx->dst_bufsize) );

                d->ctx[i] = ctx;
                printf("Created context %p : Nsamp=%zu, src_bufsize=%.3f dst_bufsize=%.3f Mbyte\n", 
                      d->ctx[i], d->ctx[i]->Nsamples, ctx->src_bufsize/1048576.0, ctx->dst_bufsize/1048576.0);
        }

        for (i = 0; i < d->Nstreams; i++) {
                CUDA_CALL( cudaStreamCreate(&(d->sid[i])) );
        }

#ifdef EXTRA_PROCESSING
        for (i = 0; i < d->Nstreams; i++) {

                // Plan out-of-place R2C FFT 
                // Note 1: d->ctx[i]->Nsamples e.g. 512M-point is too large for CuFFT
                // Note 2: cufftPlan1d(..., batch_size) is deprecated, should use cufftPlanMany()
                // Note 3: in-place R2C FFT does not really work since output requires +1 more complex (1st bin Nyquist)

                d->ctx[i]->fftlen = 32768;
                d->ctx[i]->numfft = d->ctx[i]->Nsamples / d->ctx[i]->fftlen; 
                printf("Planning CUFFT for %zu of %zu-point FFTs\n", d->ctx[i]->numfft, d->ctx[i]->fftlen);

                CUDA_CALL( cudaMalloc((void**)&(d->ctx[i]->d_fftout), d->ctx[i]->Nsamples*sizeof(float) + 16) );

                int dimn[1] = {d->ctx[i]->fftlen}; // DFT size
                int inembed[1] = {0};              // ignored for 1D xform
                int onembed[1] = {0};              // ignored for 1D xform
                int istride = 1, ostride = 1;      // step between successive in(out) elements
                int idist = d->ctx[i]->fftlen;     // step between batches (R2C input = real)
                int odist = d->ctx[i]->fftlen/2+0; // step between batches (R2C output = 1st Nyquist only; let overwrite N/2+1 point!)
                CUFFT_CALL( cufftPlanMany(&(d->ctx[i]->fftplan), 1, dimn,
                        inembed, istride, idist,
                        onembed, ostride, odist,
                        CUFFT_R2C, 
                        d->ctx[i]->numfft) 
                );
                #if (CUDA_VERSION < 8000)
                CUFFT_CALL( cufftSetCompatibilityMode(d->ctx[i]->fftplan, CUFFT_COMPATIBILITY_NATIVE) );
                #endif
                CUFFT_CALL( cufftSetStream(d->ctx[i]->fftplan, d->sid[i]) );

                // Prepare XMAC
                // Note 1: even streams are considered X-pol, odd streams are considered Y-pol
                // Note 2: we store results in a float4 = { <X*X>, Re{<X*Y'>}, Im{<X*Y'>}, <Y*Y> }
                // Note 3: two real input samples make one complex output sample, makes one float4
                d->ctx[i]->xmaclen = 2 * d->ctx[i]->fftlen;
                CUDA_CALL( cudaMalloc((void**)&(d->ctx[i]->d_xmac), d->ctx[i]->xmaclen * sizeof(float) + 16) );
                CUDA_CALL( cudaMallocHost((void**)&(d->ctx[i]->h_xmac), d->ctx[i]->xmaclen * sizeof(float) + 16) );
        }
#endif

        CUDA_TIMING_STOP( d->stop, d->start, 0, "setup of stream(s)", Nstreams*Nsamp );

        return (void*)d;
}

int decode_2bit_1channel_gpu_submit(void* descriptor)
{
        decode_descriptor_gpu_t* d = (decode_descriptor_gpu_t*)descriptor;
        size_t i;

        CUDA_TIMING_START( d->start, 0 );

        // Copy data onto device
        for (i = 0; i < d->Nstreams; i++) {
#if (!DISABLE_MEMORY_IN)
            #if ASYNC_MODE
                CUDA_CALL( cudaMemcpyAsync(
                             (void*)(d->ctx[i]->d_src),
                             (const void*)(d->ctx[i]->h_src),
                             d->ctx[i]->src_bufsize,
                             cudaMemcpyHostToDevice,
                             d->sid[i])
                );
           #else
                CUDA_TIMING_START( d->ctx[i]->start, d->sid[i] );
                CUDA_CALL( cudaMemcpy(
                             (void*)(d->ctx[i]->d_src),
                             (const void*)(d->ctx[i]->h_src),
                             d->ctx[i]->src_bufsize,
                             cudaMemcpyHostToDevice)
                );
           #endif
#endif
        }

#if (!DISABLE_MEMORY_IN) && (!ASYNC_MODE)
        for (i = 0; i < d->Nstreams; i++) {
	           CUDA_TIMING_STOP( d->ctx[i]->stop, d->ctx[i]->start, d->sid[i], "Host-->GPU", SAMPLES_PER_BYTE*d->ctx[i]->src_bufsize );
        }
#endif

        // Process the data
        for (i = 0; i < d->Nstreams; i++) {

                // Step 1: unpack single-pol 2-bit data into 32-bit,
                //         each thread processes ctx->src_bufsize/d->max_physical_threads input bytes
                CUDA_TIMING_START( d->ctx[i]->start, d->sid[i] );
                if (0) {
                        size_t N = d->ctx[i]->max_physical_threads;
                        size_t M = cudaDevProp.warpSize;
                        size_t bytes_per_thread = div2ceil(d->ctx[i]->src_bufsize, N);
                        dim3 numBlocks(div2ceil(N,M));
                        dim3 threadsPerBlock(M);
                        cu_decode_2bit1ch_1D_v3 
                           <<< numBlocks, threadsPerBlock, 0, d->sid[i] >>> (
                                d->ctx[i]->d_src,
                                (float4*)d->ctx[i]->d_dst,
                                d->ctx[i]->src_bufsize,
                                bytes_per_thread
                        );
                        CUDA_CHECK_ERRORS("cu_decode_2bit1ch_1D_v3");
                        printf("   CUDA launch kernel cu_decode_2bit1ch_1D_v3<<<blocks=%d,threads=%d>>>(%zu byte/thread) in stream %zu\n", 
                                numBlocks.x, threadsPerBlock.x,
                                bytes_per_thread, i);
                        printf("        bufsize %zu : %u blocks * %u threads/block * %zu byte/thread = %zu\n",
                                d->ctx[i]->src_bufsize, numBlocks.x, threadsPerBlock.x, 
                                bytes_per_thread, numBlocks.x*threadsPerBlock.x*bytes_per_thread
                        );

                } else if (1) {
                        size_t M = cudaDevProp.warpSize;
                        size_t N = d->ctx[i]->src_bufsize;
                        dim3   numBlocks(div2ceil(N,M));
                        dim3   threadsPerBlock(M);
#if (BITS_PER_SAMPLE==2)
                        cu_decode_2bit1ch_1D_v2 <<< numBlocks, threadsPerBlock, 0, d->sid[i]>>> (
                                d->ctx[i]->d_src,
                                (float4*)d->ctx[i]->d_dst,
                                d->ctx[i]->src_bufsize, 0
                        );
                        CUDA_CHECK_ERRORS("cu_decode_2bit1ch_1D_v2");
                        printf("   CUDA launch kernel cu_decode_2bit1ch_1D_v2<<<blocks=%d,threads=%d>>> in stream %zu\n", numBlocks.x, threadsPerBlock.x, i);
#elif (BITS_PER_SAMPLE==3)
                        cu_decode_8bit1ch_ALMA_1D <<< numBlocks, threadsPerBlock, 0, d->sid[i]>>> (
                                (uchar4*)d->ctx[i]->d_src,
                                (float4*)d->ctx[i]->d_dst,
                                d->ctx[i]->src_bufsize
                        );
                        CUDA_CHECK_ERRORS("cu_decode_8bit1ch_ALMA_1D");
                        printf("   CUDA launch kernel cu_decode_8bit1ch_ALMA_1D<<<blocks=%d,threads=%d>>> in stream %zu\n", numBlocks.x, threadsPerBlock.x, i);
#endif
                        printf("        bufsize %zu : %u blocks * %u threads/block * 1 byte/thread = %u\n",
                                d->ctx[i]->src_bufsize, numBlocks.x, threadsPerBlock.x, 
                                numBlocks.x*threadsPerBlock.x
                        );
                }
                CUDA_TIMING_STOP( d->ctx[i]->stop, d->ctx[i]->start, d->sid[i], "2b32f", d->ctx[i]->Nsamples);

#ifdef EXTRA_PROCESSING
                // Step 2: perform FFT
                if (1) {
                       CUDA_TIMING_START( d->ctx[i]->start, d->sid[i] );
                       CUFFT_CALL( cufftExecR2C(
                                d->ctx[i]->fftplan, 
                                (cufftReal*)d->ctx[i]->d_dst, 
                                // (cufftComplex*)d->ctx[i]->d_dst  // in-place fft
                                (cufftComplex*)d->ctx[i]->d_fftout // out-of-place fft
                            )
                        );
                        printf("   CUDA launch CuFFT R2C in stream %zu\n", i);
                        CUDA_TIMING_STOP( d->ctx[i]->stop, d->ctx[i]->start, d->sid[i], "r2c oop FFT", d->ctx[i]->Nsamples);
                }

                // Step 3: accumulate, each thread sums its own bin out of 0..fftlen-1 across all numfft chunks
                //         odd streams  = X-pol
                //         even streams = Y-pol
                if (1) {
                        CUDA_TIMING_START( d->ctx[i]->start, d->sid[i] );
                        size_t N = d->ctx[i]->fftlen / 4; // in units of float4
                        size_t M = cudaDevProp.warpSize;
                        dim3   numBlocks((N+M-1)/M);
                        dim3   threadsPerBlock(M);
                        if ((i % 2) == 0 && (i + 1) < d->Nstreams) {
// TODO: synchronize with other stream!
                                cu_accumulate_2pol <<< numBlocks, threadsPerBlock, 0, d->sid[i] >>> (
                                    // (const float4*)d->ctx[i+0]->d_dst,  // in-place fft
                                    // (const float4*)d->ctx[i+1]->d_dst, 
                                    (const float4*)d->ctx[i+0]->d_fftout,  // out-of-place fft
                                    (const float4*)d->ctx[i+1]->d_fftout, 
                                    d->ctx[i]->d_xmac,
                                    d->ctx[i]->fftlen/4, // float4 units
                                    d->ctx[i]->numfft
                                );
                        }
                        printf("   CUDA launch XMAC in stream %zu\n", i);
                        CUDA_TIMING_STOP( d->ctx[i]->stop, d->ctx[i]->start, d->sid[i], "dual-pol xmac", d->ctx[i]->Nsamples*2);
                }
#endif

        }

        return 0;
}

int decode_2bit_1channel_gpu_join(void* descriptor)
{
        decode_descriptor_gpu_t* d = (decode_descriptor_gpu_t*)descriptor;
        size_t i;

        // Copy results into host memory
#if (DISABLE_MEMORY_OUT==0)
        for (i = 0; i < d->Nstreams; i++) {
            CUDA_TIMING_START( d->ctx[i]->start, d->sid[i] );
            #if ASYNC_MODE
                CUDA_CALL( cudaMemcpyAsync(
                             (void*)(d->ctx[i]->h_dst), // dest
                             (const void*)(d->ctx[i]->d_dst), // source
                             d->ctx[i]->dst_bufsize,
                             cudaMemcpyDeviceToHost,
                             d->sid[i]) 
                );
            #else
                CUDA_CALL( cudaMemcpy(
                             (void*)(d->ctx[i]->h_dst),
                             (const void*)(d->ctx[i]->d_dst),
                             d->ctx[i]->dst_bufsize,
                             cudaMemcpyDeviceToHost)
                );
            #endif
        }
#endif

#ifdef EXTRA_PROCESSING 
#if (DISABLE_MEMORY_OUT==0)
        for (i = 0; i < d->Nstreams; i+=2) {
                if ((i + 1) < d->Nstreams) {
                        CUDA_CALL( cudaMemcpyAsync(
                             (void*)(d->ctx[i]->h_xmac),
                             (const void*)(d->ctx[i]->d_xmac),
                             d->ctx[i]->xmaclen * sizeof(float),
                             cudaMemcpyDeviceToHost,
                             d->sid[i]) );
                }
        }
#endif
#endif

        for (i = 0; i < d->Nstreams; i++) {
                CUDA_CALL( cudaStreamSynchronize(d->sid[i]) );
        }

#if (DISABLE_MEMORY_OUT==0)
        for (i = 0; i < d->Nstreams; i++) {
                CUDA_TIMING_STOP( d->ctx[i]->stop, d->ctx[i]->start, d->sid[i], "GPU-->host", d->ctx[i]->Nsamples);
        }
#endif

#ifdef EXTRA_PROCESSING
        if (0) {
                static int first_pass = 1;
                const char* mode = first_pass ? "w" : "a"; // "a"ppend or over"w"rite
                first_pass = 0;
                FILE* xx = fopen("/scratch/jwagner/gpu/xmac_xx_f32.bin", mode);
                FILE* yy = fopen("/scratch/jwagner/gpu/xmac_yy_f32.bin", mode);
                FILE* xy = fopen("/scratch/jwagner/gpu/xmac_xy_cplx32.bin", mode);
                for (i = 0; i < d->ctx[0]->xmaclen; i += 4) {
                    fwrite((const void*)&(d->ctx[0]->h_xmac[i+0]), sizeof(float), 1, xx);
                    fwrite((const void*)&(d->ctx[0]->h_xmac[i+1]), sizeof(float), 2, xy);
                    fwrite((const void*)&(d->ctx[0]->h_xmac[i+3]), sizeof(float), 1, yy);
                }
                fclose(xx);
                fclose(yy);
                fclose(xy);

                FILE* fx = fopen("/scratch/jwagner/gpu/xmac_FFTx_f32.bin", mode);
                CUDA_CALL( cudaMemcpy((void*)d->ctx[0]->h_dst, 
                                      (void*)&(d->ctx[0]->d_fftout[1 * d->ctx[0]->fftlen]),
                                      sizeof(float)*d->ctx[0]->fftlen, cudaMemcpyDeviceToHost) 
                );
                fwrite((const void*)d->ctx[0]->h_dst, sizeof(float), d->ctx[0]->fftlen, fx);
                fclose(fx);

        }
#endif

        // single overall on-GPU timer; must disable stream timing for this to work
        CUDA_TIMING_STOP( d->stop, d->start, 0, "total", d->Nstreams * d->ctx[0]->Nsamples);

        return 0;
}
