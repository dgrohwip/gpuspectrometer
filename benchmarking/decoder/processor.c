///////////////////////////////////////////////////////////////////////
//
// Simple VDIF 2-bit to float decoder using the multi-threaded
// unpack library for CPU (not yet using GPU)
//
// S. Trippe SiO line data is 1-channel and in Mark5B format,
//   /scratch/jwagner/st/s14db02c_KVNYS_No0006
// converted to 10032-byte/frame VDIF data
//  /home/jwagner/rawdata/sascha/s14db02c_KVNYS_No0006.short.vdif
//
// R. Dodson pulsar data is 16-channel and in VDIF format,
//   /scratch/jwagner/jw2/r14018a/YS/r14018a_KYS_No0001_No0010
// but was modified (modifyVDIF) to fix VOA transfer errors and
// relabel data as 1-channel (thus spectrum will look strange,
// but is usable for comparing the decoder against m5d or m5spec).
//
// Compile with 
// $ gcc -Wall -O3 cdecoder.c -o cdecoder     to use CPU
// $ nvcc -DUSE_GPU cdecoder.c                to use GPU
// 
///////////////////////////////////////////////////////////////////////

#include "decoder.h"

#include <sched.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <unistd.h>
#include <memory.h>

#define VDIF_HEADER_LEN 32
#define MK5B_HEADER_LEN 16

#define OUT_FILE "/scratch/jwagner/gpu/processed.bin"
//#define WRITE_TO_DISK  yes // comment out to disable writing results to a file
#define READ_FROM_DISK yes // comment out to disable reading VDIF file


#ifdef USE_GPU

   #define THREADED                 // define to use GPU streams
   #define N_THREADS  2ULL          // number of streams, need 2 to simulate dual-pol
   #define L_DFT (32ULL*10000ULL*800ULL) // number of samples to process per segment, choose something matching VDIF frame size,
                                    // and make larger than ~64MB for Host<->GPU DMA efficiency

   #define decode_2bit_1channel_init     decode_2bit_1channel_gpu_init
   #define decode_2bit_1channel_submit   decode_2bit_1channel_gpu_submit
   #define decode_2bit_1channel_join     decode_2bit_1channel_gpu_join
   #define decode_2bit_1channel_blocking decode_2bit_1channel_gpu_blocking
   #define decode_malloc                 decode_malloc_gpu

#else

   #define THREADED                // define to use threads
   #define N_THREADS  4UL          // number of threads (or just single CPU "iterations" on next data): 16 on polaris, mds0, <8 on mars
   #define L_DFT (2*10000UL*800UL) // number of samples to process per segment, choose something matching VDIF frame size

   #define decode_2bit_1channel_init     decode_2bit_1channel_cpu_init
   #define decode_2bit_1channel_submit   decode_2bit_1channel_cpu_submit
   #define decode_2bit_1channel_join     decode_2bit_1channel_cpu_join
   #define decode_2bit_1channel_blocking decode_2bit_1channel_cpu_blocking
   #define decode_malloc                 decode_malloc_cpu

#endif



// Helper
void realtime_init(int cpu)
{
	int rc;
	cpu_set_t set;
	CPU_ZERO(&set);
	CPU_SET(cpu, &set);
	rc = sched_setaffinity(0, sizeof(set), &set);
	if (rc < 0) {
		printf("sched_setaffinity: could not set CPU affinity (maybe must run as root)?\n");
	} else {
		printf("Bound to CPU#%d\n", cpu);
	}
	return;
}


int main(int argc, char** argv)
{
	FILE* fin;
	FILE* fout;

	const size_t nbitspersample = 2;
	uint32_t**   decoder_inputs;
	float**      decoder_outputs;
#ifdef THREADED
	void*        dthreads;
#endif
	double	     mem_total;

	struct timeval tstart;
	struct timeval tstop;
	size_t iter = 0;
	double dT, Msps, Msps_mean = 0.0;
	int i, j, gotEOF = 0;

	uint32_t     vdif_header[VDIF_HEADER_LEN/sizeof(uint32_t)];
	unsigned int vdif_bitpersample, vdif_nchannels;
	size_t       vdif_framebytes = 0;
	size_t       vdif_payloadbytes = 0;
	size_t       vdif_samples_per_frame = 0;
	double       vdif_frames_per_DFT;

	/* Arguments */
	if (argc != 2) {
		printf(
			"\nUsage: cdecoder <inputfile.vdif>\n\n"
			"Decodes 2-bit 1-channel VDIF input file into 32-bit floating point\n"
			"output file called %s using CPU (or later, GPU)\n\n", OUT_FILE
		);
		exit(-1);
	}
	fin = fopen(argv[1], "rb");
	if (fin == NULL) {
		perror("Input file error");
		exit(-1);
	}
	fout = fopen(OUT_FILE, "wb");
	if (fin == NULL) {
		perror("Output file error");
		exit(-1);
	}

	/* Tie to some CPU core */
#ifndef THREADED
	realtime_init(1);
#endif

	/* Look at first VDIF header */
	fread((void*)vdif_header, VDIF_HEADER_LEN, 1, fin);
	fseek(fin, 0, SEEK_SET);
	// refer to http://vlbi.org/vdif/docs/VDIF_specification_Release_1.1.1.pdf
	vdif_framebytes   = (vdif_header[2] & ~0xFF000000) * 8;
	vdif_nchannels    = 1UL << ((vdif_header[2] >> 24) & 0x1F);
	vdif_bitpersample = 1 + ((vdif_header[3] >> 26) & 0x1F);
	if ((vdif_bitpersample != nbitspersample) || (vdif_nchannels != 1)) {
		printf("Error: input VDIF not suitable (%u-bit %u-channel rather than %ld-bit 1-channel)\n",
			vdif_bitpersample, vdif_nchannels, nbitspersample);
		exit(-1);
	}
	vdif_payloadbytes      = vdif_framebytes - VDIF_HEADER_LEN;
	vdif_samples_per_frame = vdif_payloadbytes * 8 / nbitspersample;
	vdif_frames_per_DFT    = ((double)L_DFT) / vdif_samples_per_frame;
	printf("VDIF file :\n"
		"   %ld samples per frame\n"
		"   %f frames per %ld-point DFT.\n",
		vdif_samples_per_frame, vdif_frames_per_DFT, L_DFT
	);


	/* Space */
	mem_total  = N_THREADS * (vdif_frames_per_DFT + 0.5) * vdif_framebytes;
	printf("Input memory: %.2f MByte\n", mem_total/1048576.0);
	mem_total += N_THREADS * sizeof(float) * L_DFT;
	printf("Total memory: %.2f MByte\n", mem_total/1048576.0);
	if (mem_total > 8589934592*2) {
		printf("Memory exceeds hard-coded safety limit of 16 GB!\n");
		exit(-1);
	}
	decoder_inputs = decode_malloc(sizeof(const uint32_t*) * N_THREADS);
	decoder_outputs = decode_malloc(sizeof(float*) * N_THREADS);
	for (i=0; i<N_THREADS; i++) {
		decoder_inputs[i]  = decode_malloc((vdif_frames_per_DFT + 0.5) * vdif_framebytes);
		decoder_outputs[i] = decode_malloc(sizeof(float) * L_DFT);
	}


	/* Decoding loop */
#ifdef THREADED
	dthreads = decode_2bit_1channel_init((const uint32_t**)decoder_inputs, decoder_outputs, L_DFT, N_THREADS);
#endif
	while (!gotEOF) {

		printf("\n--- Chunk %lu\n", iter++);

#ifdef READ_FROM_DISK
		/* Get data */
		printf("File   -> memory : Reading\n");
		for (i=0; i<N_THREADS; i++) {
			size_t nr = fread((void*)decoder_inputs[i], vdif_framebytes, vdif_frames_per_DFT, fin);
			if (nr < vdif_frames_per_DFT) {
				gotEOF = 1;
				printf("EOF\n");
				break;
			}
		}
#endif
		if (gotEOF) {
			break;
		}

		/* Discard headers (NOTE: TODO: must also check for lost frames!) */
		printf("Memory -> memory : Discardig VDIF headers\n");
		for (i=0; i<N_THREADS; i++) {
			char* src = ((char*)decoder_inputs[i]) + VDIF_HEADER_LEN;
			char* dst = ((char*)decoder_inputs[i]);
			for (j=0; j<vdif_frames_per_DFT; j++) {
				memmove(dst, src, vdif_payloadbytes);
				src += vdif_framebytes;
				dst += vdif_payloadbytes;
			}
		}

		/* Decode 2-bit => float */
#ifdef THREADED
		printf("Memory -> memory : Decoding with %lu threads\n", N_THREADS);
		gettimeofday(&tstart, NULL);
		decode_2bit_1channel_submit(dthreads);
		decode_2bit_1channel_join(dthreads);
		gettimeofday(&tstop, NULL);
#else
		printf("Memory -> memory : Decoding non-threaded\n");
		gettimeofday(&tstart, NULL);
		for (i=0; i<N_THREADS; i++) {
			decode_2bit_1channel_blocking(decoder_inputs[i], decoder_outputs[i], L_DFT);
		}
		gettimeofday(&tstop, NULL);
#endif

		/* Report */
		dT = (tstop.tv_sec - tstart.tv_sec) + 1e-6*(tstop.tv_usec - tstart.tv_usec);
		Msps = 1e-6 * ((double)N_THREADS * (double)L_DFT) / dT;
		Msps_mean = (Msps_mean * (iter-1) + Msps) / iter;
		printf("    Time:       %.3f sec   Output samples: %.3f Ms\n", dT,  1e-6 * ((double)N_THREADS * (double)L_DFT));
		printf("    Throughput: %.3f Ms/s\n", Msps);
		printf("    Mean      : %.3f Ms/s over %ld iterations\n", Msps_mean, iter);

#ifdef WRITE_TO_DISK
		/* Store results */
		printf("Memory -> file   : Appending to %s\n", OUT_FILE);
		for (i=0; i<N_THREADS; i++) {
			fwrite((void*)decoder_outputs[i], L_DFT*sizeof(float), 1, fout);
		}
#endif
	}

	fclose(fin);
	fclose(fout);

	printf("\n\nDone.");

	return 0;
}
