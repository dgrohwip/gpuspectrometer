%
% Matlab / GNU Octave script
% decoder_check.m
%
% Reads a decoded 2-bit --> 32-bit float sample data file 
% produced by decode_cpu or decode_gpu run on a VDIF file.
%
% Also makes a time interated spectrum of that raw
% decoded sample data. This allows to compare the spectrum
% against one produced from the VDIF file using mark5access
% utilities m5spec or m5spec.py.
%

% Settings for spectrum
Lfft = 8192; Nint = 20000;
% Lfft = 32768; Nint = 100;  % for more detail

fn = '/scratch/jwagner/gpu/decoded.bin'; % the output from last 'cdecoder' run
% fn = '/scratch/jwagner/gpu/h2o-maser.float32'; % backup of 'cdecoder' output used for testing other spectrometer code
% fn = '/scratch/jwagner/gpu/h2o-maser-subband.float32'; % 2 MHz subband extracted with 'm5subband.py' (Hilbert filter transform) from Mark5B 512 MHz data

% Read data from cdecoder output file:
spec = zeros(1,Lfft);
fid = fopen(fn, 'r');
for ii=1:Nint,
    dd = fread(fid, [1 Lfft], 'float32');
    if (size(dd) ~= [1 Lfft]),
        fprintf(1, 'EOF\n');
        break;
    end
    spec = spec + abs(fft(dd));
    fprintf(1, 'Averaged spectrum %d of %d\n', ii, Nint);
end
fclose(fid);

% Show 1st Nyquist zone of averaged spectrum
spec = spec(1:round(Lfft/2+1)) / ii;
plot(spec);
xlabel('FFT bin');
ylabel('Averaged FFT magnitude');
axis tight;
x = input('Any key to exit >');

