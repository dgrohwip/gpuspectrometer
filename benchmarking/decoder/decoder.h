#ifndef GPU_CPU_DECODER_H
#define GPU_CPU_DECODER_H

#include <stdint.h>
#include <inttypes.h>
#include <stddef.h>

#ifdef __cplusplus
#define _EXPORT extern "C"
#else
#define _EXPORT
#endif

// Wrapper for threaded CPU decoding
_EXPORT void* decode_2bit_1channel_cpu_init(const uint32_t** src, float** dst, const size_t Nsamp, const size_t Nth);
_EXPORT int decode_2bit_1channel_cpu_submit(void* descriptor);
_EXPORT int decode_2bit_1channel_cpu_join(void* descriptor);
_EXPORT void* decode_malloc_cpu(size_t bytes);

// Kernel for CPU decoding
_EXPORT int decode_2bit_1channel_cpu_blocking(const uint32_t* src, float* dst, size_t N);

// Wrapper for GPU decoding
_EXPORT void* decode_2bit_1channel_gpu_init(const uint32_t** src, float** dst, const size_t Nsamp, const size_t Nth);
_EXPORT int decode_2bit_1channel_gpu_submit(void* descriptor);
_EXPORT int decode_2bit_1channel_gpu_join(void* descriptor);
_EXPORT void* decode_malloc_gpu(size_t bytes);

// Kernel for GPU decoding (wraps actual CUDA/OpenCL kernel(s))
_EXPORT int decode_2bit_1channel_gpu_blocking(const uint32_t* src, float* dst, size_t N);

#endif // GPU_CPU_DECODER_H

