#!/usr/bin/python
import numpy, sys
import pylab

Lfft = 8192
Niter = 2000
Nskip = 512*200*16  * 4

fn = '/scratch/jwagner/gpu/decoded.pulsar.bin'
# fn = '/scratch/jwagner/gpu/decoded.SiO.bin'
fin = open(fn, 'r')
fin.seek(4*Nskip)

S = numpy.zeros(Lfft)
for ii in xrange(Niter):
	buf = fin.read(Lfft*4)
	x = numpy.frombuffer(buf, dtype='float32')
	X = numpy.fft.fft(x)
	S = numpy.add(numpy.abs(X),S)
S /= Niter
pylab.plot(S[0:int(Lfft/2)])
pylab.show()
