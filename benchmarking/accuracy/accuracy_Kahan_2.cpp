// $ g++ -Wall -O3 -m64 -std=c++11 accuracy_Kahan_2.cpp -o accuracy_Kahan_2

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <random>
#include <cmath>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#define NITER 256*1024*1024 // Must be power-of-2 to test binary pair summation
#define PI 3.1415926535897932384626

//typedef double fp_t;
typedef float fp_t;

/** Returns sum = sum + y, and stores the running value of compensation. */
template<typename T> inline T kahanSumOf(T sum, const T x, T* c)
{
   volatile T e = x - *c;  // new input minus current compensation
   volatile T t = sum + e; // add "large" sum and new "small" number, looses low-order digits
   *c = (t - sum) - e;     // new compensation to recover lost low-order digits
   return t;
}

/** Timing funcs, a quick hack implementation */
static struct timeval tv_start;
void timing_start()
{
   gettimeofday(&tv_start, NULL);
}
double timing_stop()
{
   struct timeval tv_stop;
   gettimeofday(&tv_stop, NULL);
   double msec = 1e3*( (tv_stop.tv_sec-tv_start.tv_sec) + (tv_stop.tv_usec-tv_start.tv_usec)*1e-6 );
   return msec;
}

int main(void)
{
   fp_t s = 0.0, sk = 0.0, sk_c = 0.0;
   double sd = 0.0;
   double s_expect, err;
   double dT_k, dT_21;
   int i;

   std::random_device rd;
   std::mt19937 generator(rd()); // or std::mt19937_64 for 64-bit integers
   std::uniform_real_distribution<double> distribution(0, 1);

   /* Summation of constant data */
   printf("\n1) Calculating s = sum(x_i, i=0..N-1) where x_i=pi, N=%u\n", NITER);
   s_expect = NITER*PI;
   for (i=0; i < NITER; i++) {
      s += (fp_t)PI;
      sd += PI;
      sk = kahanSumOf<fp_t>(sk, (fp_t)PI, &sk_c);
   }
   printf ("  expected value = %.18g\n", s_expect);
   printf ("  64-bit additions of pi = %.18g, error = %.6g\n", sd, s_expect-sd);
   if (sizeof(fp_t) == 4) {
      printf ("  %zu-bit additions of pi = %.18g, error = %.6g (%.6g:1)\n", 8*sizeof(fp_t), s, s_expect-s, fabs((s_expect-s)/s_expect));
   }
   printf ("  %zu-bit Kahan additions of pi = %.18g, error = %.6g (%.6g:1)\n", 8*sizeof(fp_t), sk, s_expect-sk, fabs((s_expect-sk)/s_expect));

   /* Summation of random data */
   printf("\n2) Calculating s = sum(x_i, i=0..N-1) where x_i uniform random [0;1.0], N=%u\n", NITER);

   // 32-bit and 64-bit summation
   generator.seed(); // restart, to give same values
   s_expect = 0.5*NITER;
   s = 0.0; sd = 0.0;
   for (i=0; i < NITER; i++) {
      fp_t v = distribution(generator);
      s += v;
      sd += (double)v;
   }

   // Compensated summation
   generator.seed(); // restart, to give same values
   sk = 0.0; sk_c = 0.0;
   timing_start();
   for (i=0; i < NITER; i++) {
      fp_t v = distribution(generator);
      sk = kahanSumOf<fp_t>(sk, v, &sk_c);
   }
   dT_k = timing_stop();


   // 2:1 adder tree (binary pairwise summation?)
   // code is a direct copy out of an email from Manabu Watanabe
   #define getNew(x) (fp_t)distribution(generator) // dirty way to have a getNew() "function"
   generator.seed();                               // restart, to give same values
   int K = std::floor(std::log2(NITER)) - 1;
   int L = 2 << K;
   if (L != NITER) {
       printf ("  binary-sum additions of urand[0;1] : iters not power-of-2, results might not be comparable!\n");
   }
   int p, p2;
   fp_t wk[K+1];
   int  dep[K+1];
   timing_start();
   wk[0]  = getNew() + getNew();
   wk[1]  = getNew() + getNew();
   wk[0] += wk[1];
   dep[0] = 2;
   p = 4;
   i = 0;
   while (p < L) {
     p2 = p * 2;
     while (p < p2) {
       i += 2;
       p += 4;
       wk[i-1]  = getNew() + getNew();
       wk[i]    = getNew() + getNew();
       dep[i-1] = 1;
       dep[i]   = 1;
       for (;i > 0; i--) {
         if (dep[i-1] != dep[i]) {
           break;
         }
         wk[i-1]  += wk[i];
         dep[i-1]++;
       }
     }
   }
   fp_t s21 = wk[0];
   dT_21 = timing_stop();

   printf ("  expected value of <urand[0;1]>*N = %.18g +- %.18g\n", s_expect, NITER*sqrt(1/12.0)/sqrt(NITER));
   printf ("  64-bit additions of urand[0;1] = %.18g, delta = %.6g\n", sd, s_expect-sd);
   if (sizeof(fp_t) == 4) {
      err = fabs(sd-s);
      printf ("  32-bit additions of urand[0;1] = %.18g, delta = %.6g, error vs 64-bit = %.6g (%.6g:1)\n", s, s_expect-s, err, err/sd);
   }
   err = fabs(sd-sk);
   printf ("  %zu-bit Kahan additions of urand[0;1] = %.18g, delta = %.6g, error vs 64-bit = %.6g (%.6g:1), time = %.3f msec\n",
      8*sizeof(fp_t), sk, s_expect-sk, err, err/sd, dT_k
   );
   err = fabs(sd-s21);
   printf ("  %zu-bit 2:1 binary-sum additions of urand[0;1] = %.18g, delta = %.6g, error vs 64-bit = %.6g (%.6g:1), time = %.3f msec\n\n",
      8*sizeof(fp_t), s21, s_expect-s21, err, err/sd, dT_21
   );

   return 0;
}
