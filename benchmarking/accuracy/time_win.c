/*==============================================================================================*
	Radix2_FFT.c
 *==============================================================================================*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

//----------------------------------------------------------------------------------------------
#define m_PI	3.14159265358979323846	//��

#define FFT_N	16384	// FFT points 2^14
#define SSB_N	(FFT_N / 2)
#define ACC_N	100

#define F_SMPL	1.0e+3
#define F_BIN	(F_SMPL / FFT_N)

//----------------------------------------------------------------------------------------------
double	Data[ACC_N][FFT_N];
double	Rslt[SSB_N];

double	HannW[FFT_N];

//****************************************************
// for FFT
typedef struct {
	double	re;
	double	im;
} complex;

int		RvTbl[FFT_N];
complex	TwTbl[FFT_N];
complex	wrk[FFT_N];

//****************************************************
// processing time measurement
clock_t	st_0, st_1;
/*
	st_0 = clock();
	st_1 = clock();
	printf("%f�b \n", (double)(st_1 - st_0) / CLOCKS_PER_SEC);
*/

//----------------------------------------------------------------------------------------------
int  rd_data(char fname[]);
void make_win(void);
void paw_acc(void);
int  wr_rslt(char fname[]);

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// FFT modules
void FFT(int length, complex buf[], complex tw_tbl[], int rev_tbl[]);
static void mk_twiddle(int length, complex tw_tbl[]);
static void mk_revtble(int length, int rev_tbl[]);
void revers_order(int length, complex buf[], int rev_tbl[]);
//----------------------------------------------------------------------------------------------
main(int argc,char *argv[])
{
	// argment check
	if(argc < 3)
	{
		fprintf(stderr, "Usage : %s <data file> <result file>\n", argv[0]);
		return EXIT_SUCCESS;
	}

	// source data read
	if(rd_data(argv[1]) != 0)
		return EXIT_SUCCESS;

	make_win();

	paw_acc();

	// result out
	wr_rslt(argv[2]);

	return EXIT_SUCCESS;
}

//----------------------------------------------------------------------------------------------
int rd_data(char fname[])
{
	FILE	*fp;

	fp = fopen(fname, "rb");
	if(fp == NULL)
	{
		fprintf(stderr, "file open error, %s\n", fname);
		return -1;
	}

	fread(Data, sizeof(Data), 1, fp);
	fclose(fp);

	return 0;
}

//----------------------------------------------------------------------------------------------
void make_win(void)
{
	int		dc;
	double	phs;

	memset(HannW, 0, sizeof(HannW));

	for(dc = 0; dc < FFT_N; dc++)
	{
		phs = (2 * m_PI * dc) / FFT_N;
		HannW[dc] = 0.5 - 0.5 * cos(phs);	// Hann window
	}
}

//----------------------------------------------------------------------------------------------
void paw_acc(void)
{
	int		ac, dc;
	double	re, im, nml;

	nml = 1.0 / (double)ACC_N;

	// setup FFT
	mk_twiddle(FFT_N, TwTbl);
	mk_revtble(FFT_N, RvTbl);

	// result buffer clear;
	memset(Rslt, 0, sizeof(Rslt));

	for(ac = 0; ac < ACC_N; ac++)
	{
		// FFT work buffer set;
		for(dc = 0; dc < FFT_N; dc++)
		{
			wrk[dc].re = Data[ac][dc] * HannW[dc];
			wrk[dc].im = 0.0;
		}

		// FFT
		FFT(FFT_N, wrk, TwTbl, RvTbl);

		// accumelation
		for(dc = 0; dc < SSB_N; dc++)
		{
			re = wrk[dc].re;
			im = wrk[dc].im;

			Rslt[dc] += ((re * re) + (im * im));
		}
	}

	for(dc = 0; dc < SSB_N; dc++)
		Rslt[dc] *= nml;
}

//----------------------------------------------------------------------------------------------
int wr_rslt(char fname[])
{
	FILE	*fp;
	int		dc;
	double	fr, val;

	fp = fopen(fname, "w");
	if(fp == NULL)
	{
		fprintf(stderr, "file open error, %s\n", fname);
		return -1;
	}

	for(dc = 0; dc < SSB_N; dc++)
	{
		fr = F_BIN * dc;
		val = Rslt[dc];

		fprintf(fp, "%4d\t%20.14e\t%20.14e\t%20.14e\n", dc, fr, val, sqrt(val));
	}

	fclose(fp);

	return 0;
}

//==============================================================================================
//	radix-2 FFT modules
//==============================================================================================
void FFT(int length, complex buf[], complex tw_tbl[], int rev_tbl[])
{
	complex	d, twf;
	int		step, range, blkp, kfct, btfp;
	int		pbse, pstp;
	int		i, n, k;

	// order reversing
	revers_order(length, buf, rev_tbl);


	for(step = 1; step < length; step = range)
	{
		pbse  = 0;
		range = step * 2;
		pstp  = length / range;

		for(kfct = 0; kfct < step; kfct++)
		{
			twf = tw_tbl[pbse];

			for(blkp = kfct; blkp < length; blkp += range)
			{
				btfp = blkp + step;

				d.re = (twf.im * buf[btfp].im) + (twf.re * buf[btfp].re);
				d.im = (twf.re * buf[btfp].im) - (twf.im * buf[btfp].re);

				buf[btfp].re = buf[blkp].re - d.re;	buf[blkp].re += d.re;
				buf[btfp].im = buf[blkp].im - d.im;	buf[blkp].im += d.im;
			}

			pbse += pstp;
		}
	}

	// normalize
	for(i = 0; i < length; i++)
	{
		buf[i].re /= (double)length;
		buf[i].im /= (double)length;
	}
}

//----------------------------------------------------------------------------------------------
static void mk_twiddle(int length, complex tw_tbl[])
{
	int		i;
	double	dp, ph;

	dp = 2.0 * m_PI / (double)length;

	for(i = 0; i < length; i++)
	{
		ph = dp * i;

		tw_tbl[i].re =  cos(ph);
		tw_tbl[i].im = -sin(ph);
	}
}

//----------------------------------------------------------------------------------------------
static void mk_revtble(int length, int rev_tbl[])
{
	int		i, j, k, n2;

	memset(rev_tbl, 0, sizeof(int) * length);

	n2 = length / 2;
	i = j = 0;

	for(;;)
	{
		rev_tbl[i] = j;

		if(++i >= length)
			break;

		k = n2;

		while(k <= j)
		{
			j -= k;
			k /= 2;
		}

		j += k;
	}
}

//----------------------------------------------------------------------------------------------
void revers_order(int length, complex buf[], int rev_tbl[])
{
	int		i, k;
	complex	tmp;

	for(i = 0; i < length; i++)
	{
		k = rev_tbl[i];

		if(i < k)
		{
			tmp    = buf[i];
			buf[i] = buf[k];
			buf[k] = tmp;
		}
	}

}

//----------------------------------------------------------------------------------------------
