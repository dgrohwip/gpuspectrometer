/*==============================================================================================
	intg_1M.c

	Program to integrate 1-million times of power spectrum of 16,384-points of FFT.
	The argument is a start number of the integration.
	The case without the argument, random number generation and the integration are initialized.

 ===============================================================================================*/
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>
#include<time.h>

//----------------------------------------------------------------------------------------------
#define m_PI	3.14159265358979323846	//ƒÎ

#define FFT_N	16384	// FFT points 2^14
#define SSB_N	(FFT_N / 2)

#ifndef ACC_N
#define ACC_N	10000	// power acuumlation number: 1-million (deviation around 3M? 10M?)
#endif

#define RND_N	(FFT_N / 4)	// random data size

//----------------------------------------------------------------------------------------------
typedef struct {
	double	avg;
	double	sgm;
} prof;

prof	Rslt_Acc[ACC_N];
prof	Rslt_Ech[ACC_N];

/*
// for double precision
double	MesBuf[SSB_N];
double	FltPac[SSB_N];
double	FltPaw[SSB_N];
*/
// for single precision
float	MesBuf[SSB_N];
float	FltPac[SSB_N];
float	FltPaw[SSB_N];

unsigned char	Rnd_Src[FFT_N];
unsigned int	Gen_Rnd[RND_N];

int 	Init_Flg;
int 	Strt_Cnt;

//****************************************************/
// for FFT
static int		brev_tbl[FFT_N];
static double	tw_fct[FFT_N + (FFT_N / 4)];
double			flt_wkx[FFT_N], flt_wky[FFT_N];		// work buffer for double precision
//double			flt_wkx[FFT_N], flt_wky[FFT_N];		// work buffer for single precision

//****************************************************/
// distribution table
#define Lv_1	0.125	// 1/8
#define Lv_2	0.375	// 3/8
#define Lv_3	0.625	// 5/8
#define Lv_4	0.875	// 7/8

static double	cmb_tbl[128] = {
	-Lv_4, -Lv_3, -Lv_3, -Lv_3, -Lv_3, -Lv_3, -Lv_3, -Lv_3,
	-Lv_2, -Lv_2, -Lv_2, -Lv_2, -Lv_2, -Lv_2, -Lv_2, -Lv_2,
	-Lv_2, -Lv_2, -Lv_2, -Lv_2, -Lv_2, -Lv_2, -Lv_2, -Lv_2,
	-Lv_2, -Lv_2, -Lv_2, -Lv_2, -Lv_2, -Lv_1, -Lv_1, -Lv_1,
	-Lv_1, -Lv_1, -Lv_1, -Lv_1, -Lv_1, -Lv_1, -Lv_1, -Lv_1,
	-Lv_1, -Lv_1, -Lv_1, -Lv_1, -Lv_1, -Lv_1, -Lv_1, -Lv_1,
	-Lv_1, -Lv_1, -Lv_1, -Lv_1, -Lv_1, -Lv_1, -Lv_1, -Lv_1,
	-Lv_1, -Lv_1, -Lv_1, -Lv_1, -Lv_1, -Lv_1, -Lv_1, -Lv_1,
	 Lv_1,  Lv_1,  Lv_1,  Lv_1,  Lv_1,  Lv_1,  Lv_1,  Lv_1,
	 Lv_1,  Lv_1,  Lv_1,  Lv_1,  Lv_1,  Lv_1,  Lv_1,  Lv_1,
	 Lv_1,  Lv_1,  Lv_1,  Lv_1,  Lv_1,  Lv_1,  Lv_1,  Lv_1,
	 Lv_1,  Lv_1,  Lv_1,  Lv_1,  Lv_1,  Lv_1,  Lv_1,  Lv_1,
	 Lv_1,  Lv_1,  Lv_1,  Lv_2,  Lv_2,  Lv_2,  Lv_2,  Lv_2,
	 Lv_2,  Lv_2,  Lv_2,  Lv_2,  Lv_2,  Lv_2,  Lv_2,  Lv_2,
	 Lv_2,  Lv_2,  Lv_2,  Lv_2,  Lv_2,  Lv_2,  Lv_2,  Lv_2,
	 Lv_3,  Lv_3,  Lv_3,  Lv_3,  Lv_3,  Lv_3,  Lv_3,  Lv_4
};

//****************************************************/
// for random noise generate
static int SR_P;
static unsigned int SR[521];

//----------------------------------------------------------------------------------------------
void src_gen(void);

void flt_proc(int cnt);

void pwr_chk(int cnt);
prof analysis(void);

int  res_out(char fname[]);
int  res_out2(char fname[]);
int  plt_out(char fname[]);

int load_acc(char fname[]);
int save_acc(char fname[]);

//++++++ FFT modules +++++++++++++++++++++++++++++++++++
static void make_sintbl(int n, double sintbl[]);
static void make_bitrev(int n, int bitrev[]);
void        FFT_FLT(int n, double x[], double y[]);	// with double precision
//void        FFT_FLT(int n, double x[], double y[]);	// with singl precision

//++++++ M-series random modules +++++++++++++++++++++++
static void  lfsr521(void);
void         init_rnd(unsigned int seed);
unsigned int irnd(void);
int          load_lfsr(char fname[]);
int          save_lfsr(char fname[]);
//----------------------------------------------------------------------------------------------
main(int argc,char *argv[])
{
	int		acn;
	char	fname[32];

	unsigned int	rnd_sd;

	if(argc < 2)
	{
		Init_Flg = 1;
		Strt_Cnt = 0;
	}else{
		Init_Flg = 0;
		Strt_Cnt = atoi(argv[1]);
	}

	if(Init_Flg == 1)
	{
	// initial start
		// initialise random sequence
		rnd_sd = 3141592653; // default value, any
		init_rnd(rnd_sd);

		// accumlation buffer clear
		memset(FltPac, 0, sizeof(FltPac));

	}else{
	// continue start -> load last status
		load_lfsr("lfsr.dat");
		load_acc("acc.dat");
	}
	// setup for FFT
	make_sintbl(FFT_N, tw_fct);
	make_bitrev(FFT_N, brev_tbl);

	for(acn = 0; acn < ACC_N; acn++)
	{
		src_gen();

		flt_proc(acn);

		pwr_chk(acn);

		//indicate
		if((acn % 1000) == 0) fprintf(stderr, "%5dk / %5dk\r", acn / 1000, ACC_N / 1000);

	}

	res_out("acc_res_float32.dat");
	res_out2("each_res_float32.dat");

	plt_out("intg_1M_float32.txt");

	// end status saving
	save_lfsr("lfsr_float32.dat");
	save_acc("acc_float32.dat");

	return EXIT_SUCCESS;
}

//----------------------------------------------------------------------------------------------
// random data generation for each fft
void src_gen(void)
{
	int		dc;

	for(dc = 0; dc < RND_N; dc++)
		Gen_Rnd[dc] = irnd();

	memcpy(Rnd_Src, Gen_Rnd, sizeof(Rnd_Src));	// excheng to 8bits-array from 32bits-array
}

//----------------------------------------------------------------------------------------------
// fft and power-spectrum accumulation
void flt_proc(int cnt)
{
	int		i, k, ac;
//	double	re, im, pw;
	float	re, im, pw;

	// data set
	for(i = 0; i < FFT_N; i++)
	{
		k = (int)(Rnd_Src[i] & 0x7f);

		flt_wkx[i] = cmb_tbl[k];
		flt_wky[i] = 0.0;
	}

	// FFT
	FFT_FLT(FFT_N, flt_wkx, flt_wky);

	// power accumlation
	for(i = 0; i < SSB_N; i++)
	{
		re = flt_wkx[i];
		im = flt_wky[i];
		pw = (re * re) + (im * im);

		FltPac[i] += pw;
		FltPaw[i] =  pw;
	}
}

//----------------------------------------------------------------------------------------------
// sigma and anerage caluculation
void pwr_chk(int cnt)
{
	int		i, ac;

	// for accumlated result
	for(i = 0; i < SSB_N; i++)
		MesBuf[i] = FltPac[i] / (cnt + 1);

	Rslt_Acc[cnt] = analysis();

	// for each fft result
	for(i = 0; i < SSB_N; i++)
		MesBuf[i] = FltPaw[i];

	Rslt_Ech[cnt] = analysis();
}

//----------------------------------------------------------------------------------------------
// sigma and anerage caluculation for onece power spectrum
prof analysis(void)
{
	int		i, bin;
	double	val;
	double	avg, max;
	double	dif, sgm;
	prof	res;

	// check maximun  calculate average
	avg = 0.0;
	for(i = 0; i < SSB_N; i++)
	{
		val = MesBuf[i];

		avg += val;
	}
	avg /= (double)SSB_N;

	// calculate sigunma
	sgm = 0.0;
	for(i = 0; i < SSB_N; i++)
	{
		dif = avg - MesBuf[i];

		sgm += (dif * dif);
	}
	sgm /= (double)SSB_N;

	res.avg = avg;
	res.sgm = sqrt(sgm);

	return res;
}

//----------------------------------------------------------------------------------------------
// accumulated result output
int res_out(char fname[])
{
	FILE	*fp;

	if(Init_Flg == 1)
		fp = fopen(fname, "wb");
	else
		fp = fopen(fname, "ab");

	if(fp == NULL)
	{
		fprintf(stderr, "file open error, %s\n", fname);
		return -1;
	}

	fwrite(Rslt_Acc, sizeof(Rslt_Acc), 1, fp);
	fclose(fp);

	return 0;
}

// each fft result output
int res_out2(char fname[])
{
	FILE	*fp;

	if(Init_Flg == 1)
		fp = fopen(fname, "wb");
	else
		fp = fopen(fname, "ab");

	if(fp == NULL)
	{
		fprintf(stderr, "file open error, %s\n", fname);
		return -1;
	}

	fwrite(Rslt_Ech, sizeof(Rslt_Ech), 1, fp);
	fclose(fp);

	return 0;
}

//----------------------------------------------------------------------------------------------
// result write for check the middle of process
int  plt_out(char fname[])
{
	FILE	*fp;
	int		ac, bin;

	if(Init_Flg == 1)
		fp = fopen(fname, "w");
	else
		fp = fopen(fname, "a");

	if(fp == NULL)
	{
		fprintf(stderr, "file open error, %s\n", fname);
		return -1;
	}

	for(ac = 0; ac < ACC_N; ac += 100)
	{
		fprintf(fp, "%6d\t", ac + Strt_Cnt);
		fprintf(fp, "%20.14e\t", Rslt_Acc[ac].avg);
		fprintf(fp, "%20.14e\t", Rslt_Acc[ac].sgm);
		fprintf(fp, "%20.14e\n", Rslt_Acc[ac].sgm / Rslt_Acc[ac].avg);
	}

	fclose(fp);

	return 0;
}

//----------------------------------------------------------------------------------------------
// last accumulated data loading
int load_acc(char fname[])
{
	FILE	*fp;

	fp = fopen(fname, "rb");
	if(fp == NULL)
	{
		fprintf(stderr, "file open error, %s\n", fname);
		return -1;
	}

	fread(FltPac, sizeof(FltPac), 1, fp);
	fclose(fp);

	return 0;
}

//----------------------------------------------------------------------------------------------
// accumulated data saving for next
int save_acc(char fname[])
{
	FILE	*fp;

	fp = fopen(fname, "wb");
	if(fp == NULL)
	{
		fprintf(stderr, "file open error, %s\n", fname);
		return -1;
	}

	fwrite(FltPac, sizeof(FltPac), 1, fp);
	fclose(fp);

	return 0;
}

//==============================================================================================
//	FFT modules
//==============================================================================================
// twiddle factor table making
static void make_sintbl(int n, double sintbl[])
{
	int		i;
	int		n2, n4, n8;
	double	c, s, dc, ds, t;

	memset(sintbl, 0, sizeof(double) * n);

	n2 = n / 2;	n4 = n / 4;	n8 = n / 8;

	t = sin(m_PI / (double)n);
	dc = 2 * t * t;
	ds = sqrt(dc * (2 - dc));

	t = 2 * dc;
	c = sintbl[n4] = 1;
	s = sintbl[0]  = 0;

	for(i = 1; i < n8; i++)
	{
		c -= dc;	dc += t * c;
		s += ds;	ds -= t * s;

		sintbl[i] = s;
		sintbl[n4-i] = c;
	}

	if(n8 != 0)
		sintbl[n8] = sqrt(0.5);

	for(i = 0; i < n4; i++)
		sintbl[n2-i] = sintbl[i];

	for(i = 0; i < (n2 + n4); i++)
		sintbl[n2 + i] = -sintbl[i];
}

//----------------------------------------------------------------------------------------------
// bit-reverse table making
static void make_bitrev(int n, int bitrev[])
{
	int		i, j, k, n2;

	memset(bitrev, 0, sizeof(int) * n);

	n2 = n / 2;
	i = j = 0;

	for(;;)
	{
		bitrev[i] = j;

		if(++i >= n)
			break;

		k = n2;

		while(k <= j)
		{
			j -= k;
			k /= 2;
		}

		j += k;
	}
}

//----------------------------------------------------------------------------------------------
// fft with double precision
void FFT_FLT(int n, double  x[], double  y[])
{
	int		i, j, k, ik, h, d, k2, n4;
	int		inv;
	double	t, s, c, dx, dy;

	n4 = n / 4;

	for(i = 0; i < n; i++)
	{
		j = brev_tbl[i];

		if(i < j)
		{
			t = x[i];	x[i] = x[j];	x[j] = t;
			t = y[i];	y[i] = y[j];	y[j] = t;
		}
	}

	for(k = 1; k < n; k = k2)
	{
		h  = 0;
		k2 = k + k;
		d  = n / k2;

		for(j = 0; j < k; j++)
		{
			c = tw_fct[h + n4];
			s = tw_fct[h];

			for(i = j; i < n; i += k2)
			{
				ik = i + k;

				dx = (s * y[ik]) + (c * x[ik]);
				dy = (c * y[ik]) - (s * x[ik]);

				x[ik] = x[i] - dx;	x[i] += dx;
				y[ik] = y[i] - dy;	y[i] += dy;
			}

			h += d;
		}
	}

	for(i=0; i<n; i++)
	{
		x[i] /= (double)n;
		y[i] /= (double)n;
	}
}

/*
//----------------------------------------------------------------------------------------------
// fft with single precision
void FFT_FLT(int n, float x[], float y[])
{
	int		i, j, k, ik, h, d, k2, n4;
	int		inv;
	float	t, s, c, dx, dy;

	n4 = n / 4;

	for(i = 0; i < n; i++)
	{
		j = brev_tbl[i];

		if(i < j)
		{
			t = x[i];	x[i] = x[j];	x[j] = t;
			t = y[i];	y[i] = y[j];	y[j] = t;
		}
	}

	for(k = 1; k < n; k = k2)
	{
		h  = 0;
		k2 = k + k;
		d  = n / k2;

		for(j = 0; j < k; j++)
		{
			c = (float)tw_fct[h + n4];
			s = (float)tw_fct[h];

			for(i = j; i < n; i += k2)
			{
				ik = i + k;

				dx = (s * y[ik]) + (c * x[ik]);
				dy = (c * y[ik]) - (s * x[ik]);

				x[ik] = x[i] - dx;	x[i] += dx;
				y[ik] = y[i] - dy;	y[i] += dy;
			}

			h += d;
		}
	}

	for(i=0; i<n; i++)
	{
		x[i] /= (float)n;
		y[i] /= (float)n;
	}
}
*/

//==============================================================================================
//	M-series random modules
//==============================================================================================
// update LFSR
static void lfsr521(void)
{
	int i;

	for(i =  0; i <  32; i++)
		SR[i] ^= SR[i + 489];

	for(i = 32; i < 521; i++)
		SR[i] ^= SR[i -  32];
}

//----------------------------------------------------------------------------------------------
// initialise random sequence
void init_rnd(unsigned int seed)
{
	int i, j;
	unsigned int u;

	u = 0;

	for(i = 0; i <= 16; i++)
	{
		for(j = 0; j < 32; j++)
		{
			seed = seed * 1566083941UL + 1;

			u = (u >> 1) | (seed & (1UL << 31));
		}
		SR[i] = u;
	}

	SR[16] = (SR[16] << 23) ^ (SR[0] >> 9) ^ SR[15];

	for(i = 17; i <= 520; i++)
		SR[i] = (SR[i-17] << 23) ^ (SR[i-16] >> 9) ^ SR[i-1];

	lfsr521();  lfsr521();  lfsr521();  /* warm up */

	SR_P = 520;
}

//----------------------------------------------------------------------------------------------
unsigned int irnd(void)
{
	if(++SR_P >= 521)
	{
		lfsr521();  SR_P = 0;
	}

	return SR[SR_P];
}

//----------------------------------------------------------------------------------------------
// last LFSR status loading
int load_lfsr(char fname[])
{
	FILE	*fp;

	fp = fopen(fname, "rb");
	if(fp == NULL)
	{
		fprintf(stderr, "file open error, %s\n", fname);
		return -1;
	}

	fread(SR, sizeof(SR), 1, fp);
	fclose(fp);

	SR_P = 520;

	return 0;
}

//----------------------------------------------------------------------------------------------
// LFSR status saving next
int save_lfsr(char fname[])
{
	FILE	*fp;

	fp = fopen(fname, "wb");
	if(fp == NULL)
	{
		fprintf(stderr, "file open error, %s\n", fname);
		return -1;
	}

	fwrite(SR, sizeof(SR), 1, fp);
	fclose(fp);

	return 0;
}

//----------------------------------------------------------------------------------------------

