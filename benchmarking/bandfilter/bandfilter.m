
%% Octave script to make noise plus sinusoid, write to float32 binary file
clear;
pkg load signal;

%% Filter properties
R = 16;      % decimation ratio
Ntaps = 64;  % taps in FIR filter

%% Test signal properties
L_desired_MB = 512;
N = floor(L_desired_MB * 2^20 / 4);
w_tone = 2*pi * (128/8192);
w_spur = 2*pi * (3000/8192);

%% Make test signal
t = 0:(N-1);
x = randn(1,N);
x = x + sin(w_tone*t);
x = x + sin(w_spur*t);
fd = fopen('bandfilter.in.f32', 'w');
fwrite(fd, x, 'float32');
fclose(fd);

%% Filter coefficients
if 0,
   % Low-pass
   wbw = 1/R;
   wcut = wbw;
   b = fir1(Ntaps, wcut);
else
   % Bandpass
   wbw = 1/R;
   m = 1; % choose an integer value m (1<=m<R) to select the desired 1/R-wide region
          % out of the R uniformly spaced 1/R wide regions of input wideband signal spectrum
   wstart = m * wbw;
   wcut = [wstart, wstart+wbw];
   if (wcut(1) < 1e-4), wcut = 1/R; end  % revert to lowpass if region is 1st region
   b = single( fir1(Ntaps, wcut) );
end
b = single(b);
b = flipud(b(:)); % time reversal for easy indexing on GPU
fd = fopen('bandfilter.coeff.f32', 'w');
fwrite(fd, b, 'float32');
fclose(fd);
b

%clear;

fd = fopen('bandfilter.in.f32', 'r');
x = fread(fd, [1 32768], 'float32');
fclose(fd);
plot(abs(fft(x)));

%% Filter coefficients for higher quality filter, based on frequency shifting
% Method:
% 1) Upsample by factor 2,
% 2) Lowpass to remove images not centered at zero,
% 3) Frequency shift,
% 4) Lowpass to desired bandwidth 
% 5) Take real part (thanks to upsampling earlier the spectral images do not fold now)
%
% Shortcut:
% 1) frequency shift : upsample and multiply every 2nd sample by cosine (imag part never needed, and every 2nd sample is 0.0f due to upsampling)
% 2) N-tap lowpass + N-tap bandpass : convolution of individual impolse responses, a new FIR of length N+N-1
Ru = 2; % upsampling factor
aa_lp = fir1(Ru*Ntaps,1/Ru);

