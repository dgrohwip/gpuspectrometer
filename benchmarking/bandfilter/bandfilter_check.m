
%% Octave script to load output of GPU

clear;

L = 32768;
R = 8;

fd = fopen('bandfilter.in.f32', 'r');
xi = fread(fd, [1 L*R], 'float32');
fclose(fd);

fd = fopen('bandfilter.out.f32', 'r');
xo = fread(fd, [1 L], 'float32'); % read and discard start of filter response
xo = fread(fd, [1 L], 'float32'); % keep
fclose(fd);

Xi = abs(fft(xi)) / numel(xi);
Xo = abs(fft(xo)) / numel(xo);

clf;
semilogy(0:(R*L/2-1), Xi(1:(R*L/2)), 'r:');
hold on;
semilogy((0:(L/2-1)), Xo(1:(L/2)), 'kx:');

