////////////////////////////////////////////////////////////////////////////////////////////////
//
// Polyphase Filter Bank test with settings like KVN DFB (512 MHz into >= 8MHz <128-ch, 1024-coeff)
// (C) 2016 Jan Wagner
//
// Input data are 2-bit VDIF read from an external file (#define INFILE further below).
//
// The CUDA PFB number of channels and taps is defined at compile time (compiler -D flags),
// rather than dynamically at runtime. This allows the NVCC compiler to apply optimizations.
// Some suitable PFB coefficients can be generated with pfb_coeffs.py.
//
// The CUDA PFB output is a time series of complex-valued time-domain samples from each channel.
//
// - KVN case: 512 MHz bandwidth, extract >= 4 MHz, complex PFB with <=128 channels suffices.
//   Coefficients are calculated in kvnuser@KVNTN:/home/kvnuser/KVN/DAS/DFB/coef_tools/,
//   The KVN DFB filter system has a fixed number of N=1024 points over 512 MHz.
//   The 1024 (511) coefficients are stored in *.freq files.
//   The coeffs are cosine-shifted Hamming-windowed sinc().
//      KVN PFB case       BW/ch     GTX Titan X
//      128 ch x 8 tap/ch  4 MHz     pfb_v1c:20.1Gs/s   decimFIR_csm2(R=128):10.4Gs/s
//      64 ch x 16 tap/ch  8 MHz     pfb_v1c:11.8Gs/s   decimFIR_csm2(R=64):5.1Gs/s
//      32 ch x 32 tap/ch  16 MHz    pfb_v1c:5.0Gs/s    decimFIR_csm2(R=32):2.3Gs/s
//      16 ch x 64 tap/ch  32 MHz    pfb_v1c:(slow,2.7) decimFIR_csm2(R=16):1.2Gs/s
//      8 ch x 128 tap/ch  64 MHz    pfb_v1c:(slow,0.8) decimFIR_csm2(R=8):0.6Gs/s
//      4 ch x 256 tap/ch  128 MHz   pfb_v1c:(no mem)
//      2 ch x 512 tap/ch  256 MHz   pfb_v1c:(no mem)
//
////////////////////////////////////////////////////////////////////////////////////////////////

/* Std Headers */
#include <stdio.h>
#include <math.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdint.h>
#include <inttypes.h>
#include <cufft.h>

#define KVN_PFB_NCOEFFS 1024 // KVN DFB has hard-coded array space for 1024 coeffs, emulate this here
#ifndef PFB_HC_NCHAN
    #define PFB_HC_NCHAN 128
#endif
#define PFB_HC_TAPS  (KVN_PFB_NCOEFFS/PFB_HC_NCHAN) // choose #taps to match KVN DFB total 1024 coeffs

#include "pfb_fir.h"

/* Hardcoded run mode options */
#define CHECK_TIMING 1   // 1 to activate CUDA_TIMING_START()/_STOP() timing, 0 otherwise
#define DO_COMPLEX_PFB 0 // 1 to let PFB produce (real,0.0f) pairs to feed into C2C IFFT (R2C is always FFT)
#define DUMP_PFBOUT  0   // 1 to write post-PFB IFFT 32-bit complex data to files

/* CUDA kernels */
// Note: some cu files rely on pre-#define'd things (PFB_HC_NCHAN, PFB_HC_TAPS)!
#include "cuda_utils.cu"
#include "decoder_2b32f_kernels.cu"
#include "pfb_fir.cu"
#include "pfb_setup.cu"
#include "decim_fir.cu" // Decimating FIR without PFB, when only 1 channel desired; always 1024 taps, adjustable decimation
#include "arithmetic_stat_kernels.cu"

/* C kernels and helpers */ // nvcc: easier to include .c rather than .h and link with .o...
#include "pfb_fir.c"
#include "dumpfile_io.c"
#include "readfile_io.c"

#ifndef m_min
#define m_min(x,y) ((x < y) ? x : y)
#endif

////////////////////////////////////////////////////////////////////////////////////////////////

// Polyphase filterbank structure
#define M_FIR_PER_BLOCK 32   // Shared Memory cache of FIR coeffs, set equal to 1x or 2x warpsize

// Input data format description
#define INFILE "/home/jwagner/rawdata/sascha/s14db02c_KVNYS_No0006.short.vdif"
#define FRAMESIZE   10032UL
#define PAYLOADSIZE 10000UL
#define SAMP_PER_BYTE 4

// Input data amount
#define INPUT_SAMPS   (2*32768*64*64) // (2*32768*PFB_HC_NCHAN*PFB_HC_TAPS)
#define INPUT_BYTES   (INPUT_SAMPS/SAMP_PER_BYTE)
#define NUMFRAMES     (16 + (INPUT_BYTES/PAYLOADSIZE))

////////////////////////////////////////////////////////////////////////////////////////////////
// Coeff generator
// KVN PFB mkh.c creates Hamming-windowed sinc()
////////////////////////////////////////////////////////////////////////////////////////////////

static double sinc(double x)
{
    return (x ==0) ? 1.0 : sin(x)/x;
}

static void make_pfb_coeffs(float *w, const double low_Hz, const double high_Hz, int Ncoeff)
{
    const int N = Ncoeff, L = (Ncoeff/2)-1;
    double fc_lo = low_Hz/N, fc_hi = high_Hz/N;
    double wc_lo = 2*M_PI*fc_lo, wc_hi = 2*M_PI*fc_hi;
    double nm = 1.0 / (fc_hi - fc_lo);
    for (int n=0; n<N; n++) {
       double wn = nm;
       wn *= fc_hi*sinc((n-L)*wc_hi) - fc_lo*sinc((n-L)*wc_lo); // sinc weights
       wn *= 0.54 - 0.46*cos(2*M_PI*n/(N-1)); // Hamming window
       w[n] = (float)wn;
    }
    w[N-1] = 0.0f;
}

////////////////////////////////////////////////////////////////////////////////////////////////
// MAIN
////////////////////////////////////////////////////////////////////////////////////////////////

int main(int argc, char **argv)
{
    cudaDeviceProp cudaDevProp;

    cudaEvent_t globalstart, globalstop;
    cudaEvent_t substart, substop;
    float etime, globaltime;

    double low_Hz = 0.0, high_Hz = 8.0e6;
    const double total_bw_Hz = 512.0e6;

    // Create PFB job descriptor
    pfb_job_t J;
    memset((void*)&J, 0x00, sizeof(J));
    J.do_complex_pfb = DO_COMPLEX_PFB;
    J.nrawbytes      = INPUT_BYTES;
    J.nsamples       = INPUT_SAMPS;

    // Command line
    if (argc == 3) {
        low_Hz = atof(argv[1]);
        high_Hz = atof(argv[2]);
        int min_ch = total_bw_Hz / abs(high_Hz - low_Hz);
        J.Nch = min_ch;
        J.Ntaps = KVN_PFB_NCOEFFS / min_ch;
    } else {
        J.Nch = PFB_HC_NCHAN;
        J.Ntaps = PFB_HC_TAPS;
    }
    printf("Using %d-ch %d-tap PFB\n", J.Nch, J.Ntaps);

    // Select device and do some preparations
    CUDA_CALL( cudaSetDevice(CUDA_DEVICE_NR) );
    CUDA_CALL( cudaGetDeviceProperties(&cudaDevProp, CUDA_DEVICE_NR) );
    J.maxphysthreads = cudaDevProp.multiProcessorCount * cudaDevProp.maxThreadsPerMultiProcessor;
    printf("CUDA Device #%d : %s, Compute Capability %d.%d, %d threads/block, warpsize %d, %zu threads max.\n",
        CUDA_DEVICE_NR, cudaDevProp.name, cudaDevProp.major, cudaDevProp.minor,
        cudaDevProp.maxThreadsPerBlock, cudaDevProp.warpSize, J.maxphysthreads
    );

    // Prepare PFB coeffs
    float* w = (float*)malloc(J.Nch * J.Ntaps * sizeof(float));
    make_pfb_coeffs(w, low_Hz, high_Hz, J.Nch * J.Ntaps);

    J.h_pfb_coeff = (float**)malloc(J.Nch*sizeof(float*));
    for (int c=0; c<J.Nch; c++) {
        J.h_pfb_coeff[c] = (float*)malloc(J.Ntaps * sizeof(float));
        for (int t=0; t<J.Ntaps; t++) {
            J.h_pfb_coeff[c][t] = w[c*J.Ntaps + t];
        }
    }

    // Allocate memory for the input and output data, rearrange coefficients for GPU (and CPU)
    allocatePFBJob(&J);

    // Read one-second data from a file that has Mark5B format.
    readData_file_Mk5B(INFILE, J.h_samples, NUMFRAMES);

    // Initialize post-PFB IFFT
    preparePFBJob_FFT(&J);

    // Prepare timing events
    CUDA_CALL( cudaEventCreate( &globalstart ) );
    CUDA_CALL( cudaEventCreate( &globalstop ) );
    CUDA_CALL( cudaEventCreate( &substart ) );
    CUDA_CALL( cudaEventCreate( &substop ) );

    // Copy raw data from host to device
    CUDA_TIMING_START(substart, 0);
    CUDA_CALL( cudaMemcpy(J.d_samples, J.h_samples, INPUT_BYTES, cudaMemcpyHostToDevice) );
    CUDA_TIMING_STOP_ACC(substop, substart, etime, 0, "CPU-->GPU", INPUT_SAMPS);

    // Convert 2-bit samples to 32-bit float
    if (1) {
        size_t M = cudaDevProp.maxThreadsPerBlock;
        size_t N = INPUT_BYTES;
        dim3 numBlocks(div2ceil(N,M));
        dim3 threadsPerBlock(M);
        CUDA_TIMING_START(substart, 0);
        cu_decode_2bit1ch <<<numBlocks, threadsPerBlock>>> (
            J.d_samples, (float4 *)J.d_idata, INPUT_BYTES
        );
        CUDA_CHECK_ERRORS("cu_decode_2bit1ch");
        CUDA_TIMING_STOP_ACC(substop, substart, etime, 0, "decode", INPUT_SAMPS);
    }

    // Run 1-channel Decimating FIR for comparison
    if (1) {
        size_t threadsPerBlock = N_DECFIR_BLOCKSZ;
        size_t numBlocks = div2ceil(J.maxphysthreads,threadsPerBlock);
        //size_t shm = N_DECFIR_BLOCKSZ*sizeof(float) + N_DECFIR_MAXTAP*sizeof(float); // kernel<<grid,block,dynamic shm,stream>>
        int Ncoeff = N_DECFIR_MAXTAP;
        int R = PFB_HC_NCHAN;
        size_t nsamp = INPUT_SAMPS;
        nsamp = nsamp - (nsamp % N_DECFIR_MAXTAP);
        printf("decimating FIR (for speed ref.) with %d coeffs, decimation R=%d, %zu samples\n", Ncoeff, R, nsamp);
        CUDA_TIMING_START(substart, 0);
        cu_decim_FIR_csm2 <<<numBlocks, threadsPerBlock>>> (  J.d_idata, J.d_pfb_coeff_1D, J.d_pfbout, nsamp, Ncoeff, R);
        CUDA_CHECK_ERRORS("decimFIR_csm2");
        CUDA_TIMING_STOP_ACC(substop, substart, etime, 0, "decimFIR_csm2", nsamp);
    }

    // Begin timing key performance
    CUDA_CALL( cudaEventRecord(globalstart, 0) );

    // Run PFB
    if (1) {

        // Version 1b : several PFB running in parallel, subsets of x[n] to calculate y[n]

        size_t M = M_FIR_PER_BLOCK;
        size_t N = J.Nch * J.nPFB;
        size_t len = INPUT_SAMPS-J.Nch*J.Ntaps;

        dim3 numBlocks(div2ceil(N,M));
        dim3 threadsPerBlock(M);
#if DO_COMPLEX_PFB
        CUDA_TIMING_START(substart, 0);
        cu_pfb_complexout_v1c <<<numBlocks, threadsPerBlock>>> (
          (float*)J.d_idata, (float*)J.d_pfbout, J.d_pfb_coeff_1D, len, J.nPFB
        );
        CUDA_CHECK_ERRORS("cu_pfb_c_v1c");
        CUDA_TIMING_STOP_ACC(substop, substart, etime, 0, "pfb_c_v1c", INPUT_SAMPS);
#else
        CUDA_TIMING_START(substart, 0);
        cu_pfb_v1c <<<numBlocks, threadsPerBlock>>> (
          (float*)J.d_idata, (float*)J.d_pfbout, J.d_pfb_coeff_1D, len, J.nPFB
        );
        CUDA_CHECK_ERRORS("cu_pfb_v1c");
        CUDA_TIMING_STOP_ACC(substop, substart, etime, 0, "pfb_v1c", INPUT_SAMPS);
#endif

    } else if (0) {

        // Version 2 : every thread processes just one PFB tap, results summed across threads.

        size_t Nsamp_out = INPUT_SAMPS - J.Nch*J.Ntaps;
        size_t y_n_fanout = J.maxphysthreads / (J.Nch*J.Ntaps);
        dim3   numBlocks(J.Nch * y_n_fanout);
        dim3   threadsPerBlock(J.Ntaps);
        printf("   launching cu_pfb_v2<<<%zu,%zu>>> fanout=%zu to get %zu channelized samples\n", 
            (size_t)numBlocks.x, (size_t)threadsPerBlock.x, y_n_fanout, Nsamp_out
        );
        CUDA_TIMING_START(substart, 0);
        cu_pfb_v2 <<<numBlocks, threadsPerBlock>>> (
            (float*)J.d_idata, (float*)J.d_pfbout, 
            J.d_pfb_coeff_1D, Nsamp_out
        );
        CUDA_CHECK_ERRORS("cu_pfb_v2");
        CUDA_TIMING_STOP_ACC(substop, substart, etime, 0, "pfb_v2", INPUT_SAMPS);
    }

    // Step 2b -- run 1D r2c IFFT with Nch points on the PFB output data
    //            to get the actual and final PFB time-domain output
    //            (Or should it be 1D r2c/c2c FFT i.e. forward transform..??)
    if (1) {
#if DO_COMPLEX_PFB
        CUDA_TIMING_START(substart, 0);
        CUFFT_CALL( cufftExecC2C(J.ifftplan, (cufftComplex*)J.d_pfbout, (cufftComplex*)J.d_pfbout_td, CUFFT_FORWARD /*inverse?*/) );
        CUDA_TIMING_STOP_ACC(substop, substart, etime, 0, "c2c fft", INPUT_SAMPS);
#else
        // Problem: CUFFT has no R2C Inverse!? Only R2C Forward!? Inverse possible by conjugating FFT output?
        CUDA_TIMING_START(substart, 0);
        CUFFT_CALL( cufftExecR2C(J.ifftplan, (cufftReal*)J.d_pfbout, (cufftComplex*)J.d_pfbout_td) );
        CUDA_TIMING_STOP_ACC(substop, substart, etime, 0, "r2c fft", INPUT_SAMPS);
#endif
    }

    // Stop total elapsed time checker, report throughput
    CUDA_CALL( cudaEventRecord(globalstop, 0) );
    CUDA_CALL( cudaEventSynchronize(globalstop) );
    CUDA_CALL( cudaEventElapsedTime( &globaltime, globalstart, globalstop ) );
    float rate_total = 1e-9*(INPUT_SAMPS)/(globaltime*1e-3);
    printf("%10d %6d %10.5f %10.3f\n",J.Nch,J.Ntaps,globaltime*1e-3,rate_total); // samples | seconds | Gs/s

    // Report the mean and std deviation
    // note: for real-valued raw input to PFB, half the PFB IFFT output bins are redundant, ignore them.
    if (1) {
        size_t M = M_FIR_PER_BLOCK;
        size_t N = J.Nch*J.nPFB;
        size_t istride = J.Nch*J.nPFB;

        dim3 numBlocks(div2ceil(N,M));
        dim3 threadsPerBlock(M);

        CUDA_TIMING_START(substart, 0);
        CUDA_CALL( cudaMemset( J.d_stats, 0x00, sizeof(float)*J.Nch*(2+1) ) );
        cu_stats_complex_v1 <<<numBlocks, threadsPerBlock>>> (
           (float*)J.d_pfbout_td, // output from FFT : real and complex expected to be mid-channel-symmetric
           // (float*)J.d_pfbout, // output from PFB : real part expected to be zero, similar variance
           (float*)J.d_stats_chMeans,
           (float*)J.d_stats_chVars,
           J.ifft_output_samples,
           istride
        );
        CUDA_CHECK_ERRORS("stats_c_v1");
        CUDA_TIMING_STOP_ACC(substop, substart, etime, 0, "stats_c_v1", J.ifft_output_samples);

        // Report the results
        CUDA_CALL( cudaMemcpy(J.h_stats, J.d_stats, sizeof(float)*J.Nch*(2+1), cudaMemcpyDeviceToHost) );
        for (int ch=0; ch<J.Nch; ch++) {
            printf(" Channel %2d : complex mean = {%8.5f,%8.5f} : var = %8.5f\n",
                ch,
                J.h_stats_chMeans[2*ch+0],
                J.h_stats_chMeans[2*ch+1],
                J.h_stats_chVars[ch]
            );
        }
    }

    // Get post-FFT data
    if (DUMP_PFBOUT) {
        const size_t N = INPUT_SAMPS/(2*J.Nch);
        float* arr = (float*)malloc(2*N*sizeof(float));
        CUDA_CALL( cudaMemcpy(J.h_pfbout /* ok to reuse */, J.d_pfbout_td, 2*sizeof(float)*INPUT_SAMPS, cudaMemcpyDeviceToHost) );
        for (int ch=0; ch<J.Nch/2; ch++) {
            char filename[128];
            for (size_t n=0; n<N; n++) {
               arr[2*n+0] = J.h_pfbout[2*ch + 2*n*J.Nch + 0]; // Real
               arr[2*n+1] = J.h_pfbout[2*ch + 2*n*J.Nch + 1]; // Imag
            }
            sprintf(filename, "out/pfbkvn_timedomain_complex32b_%d_%d_ch%03d.bin", J.Nch, J.Ntaps, ch);
            write_to_binary_file(filename, (void*)arr, 2*N*sizeof(float));
        }
    }

    // Grab the PFB pre-FFT result for checking in Octave/Matlab
    if (DUMP_PFBOUT) {
        char filename[1024];
        CUDA_TIMING_START(substart, 0);
        CUDA_CALL( cudaMemcpy(J.h_pfbout, J.d_pfbout, sizeof(float)*INPUT_SAMPS, cudaMemcpyDeviceToHost) );
        CUDA_TIMING_STOP_ACC(substop, substart, etime, 0, "GPU-->Host (PFB)", INPUT_SAMPS);
        sprintf(filename, "out/pfbkvn_%d_%d.pfbout_gpu", J.Nch, J.Ntaps);
#if DO_COMPLEX_PFB
        // Remove the zero-valued Imag part to make Octave/Matlab reading more consistent for CPU,GPU
        for (size_t j=0; j<INPUT_SAMPS; j++) {
            J.h_pfbout[j] = J.h_pfbout[2*j+0];
        }
#endif
        write_float32_to_binary_file(filename, J.h_pfbout, INPUT_SAMPS);
    }

    // Grab the PFB post-FFT result for checking in Octave/Matlab
    if (DUMP_PFBOUT) {
        char filename[1024];
        CUDA_CALL( cudaMemcpy(J.h_pfbout, J.d_pfbout_td, sizeof(float)*INPUT_SAMPS, cudaMemcpyDeviceToHost) );
        sprintf(filename, "out/pfbkvn_%d_%d.pfbout_td_gpu", J.Nch, J.Ntaps);
        write_float32_to_binary_file(filename, J.h_pfbout, INPUT_SAMPS);
    }

    freePFBJob(&J);
}

