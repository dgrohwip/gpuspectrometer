clear; fclose('all');

% Hard-coded pfb.cu settings copied here:
Nch = 32;
Ntaps = 32;
fs = 256e6;

% Derived
fn = sprintf('pfb_%d_%d.pfbout_gpu', Nch, Ntaps); device = 'GPU';
% fn = sprintf('pfb_%d_%d.pfbout_cpu', Nch, Ntaps); device = 'CPU';

Navg = 64;
Lchfft = 8192;
Nsamp = Nch * Lchfft;
fn_TeX = strrep(fn, '_', '\_');

N = Nsamp / Nch;
x_units = fs / (N*Nch);

% Read data
ddim = [Nch N];
Fd_avg = zeros(ddim);

fd = fopen(fn, 'rb');
Nadded = 0;
for iter=1:Navg,
    din = fread(fd, ddim, 'float32');
    dd = ifft(din,[],1);    % last step of PFB, channel direction
    if size(dd) ~= ddim, break; end
    Fd = abs(fft(dd,[],2)); % spectra in channels, time direction
    Fd_avg = Fd_avg + Fd;
    Nadded = Nadded + 1;
end
fclose(fd);
Fd_avg = Fd_avg / Nadded;

Fd = Fd_avg; % choose averaged spectrum, comment out to use recently read spectrum

% Glue spectra side by side into a plot
figure(1), clf;

Nch = round(Nch/2 + 1); % optional, throw away the redudant channels (if real-valued sample data input)
Fd = Fd(1:Nch, :);

Fd_max = max(max(Fd));
cmap = jet(Nch);
for ch=1:Nch,
    yy = Fd(ch,:); 
    N = numel(yy);
    xx = (1:N) - 1 + N*(ch-1);
    %semilogy(xx*x_units, yy,'Color',cmap(ch,:)); 
    plot(xx*x_units, yy,'Color',cmap(ch,:)); 
    text(min(xx)*x_units, 0.1*Fd_max, sprintf('Ch#%02d',ch), 'Color',cmap(ch,:));
    hold on;
end

xlabel('Frequency (MHz)');
%ylabel('Fourier Amplitude (log|F(x)|)')
ylabel('Fourier Amplitude (|F(x)|)')
title([device ' output ' fn_TeX]);
axis tight;
