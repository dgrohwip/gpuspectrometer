function pfb_test()
    close all;
    
    %% Design a PFB
    Nch  = 32;   % Number of channels in the FIR filter bank
    Ntap = 32;   % Number of taps in each FIR filter (total: Nch*Ntap coeffs)

    [H,Hq] = pfb_coeffs(Nch, Ntap);

    %% Settings for test data
    Np = 100;
    Nsamp = Nch*Ntap*Np;
    t = 1:Nsamp;
    use_complex = 0;  % 1 to use complex input, 0 to use real-valued input
    
    %% Test the channel responses of the designed PFB

    % Angular input frequencies to sweep
    Nppch = 10;
    Nfreq = Nppch*Nch;
    if use_complex,
        w = linspace(0,2*pi,Nfreq);
    else
        w = linspace(0,1*pi,Nfreq);
    end
    
    % Output power in each PFB channel for certain input frequency
    HH = zeros(Nch,Nfreq); 

    % Calculate PFB result on sine test signal at each angular frequency
    for ii=1:Nfreq,
        fprintf(1, 'Filtering tone %d of %d\n', ii,Nfreq);
        x = exp(-1i*w(ii)*t);
        if ~use_complex,
            x = real(x);
        end
        
        % add slope to amplitude to trace LSB/USB
        x = x * (1 + (ii/Nfreq)); 
        
        [Y,~] = pfb_filtering(Hq, x);
        HH(:,ii) = var(Y.')'; % remember the output power
    end

    if use_complex,
        title_str = sprintf('%d-Channel %d-Tap PFB Response for Complex signal', Nch, Ntap);
    else
        title_str = sprintf('%d-Channel %d-Tap PFB Response for Real-valued signal', Nch, Ntap);
    end
    
    % Show
    figure(), set(gca,'Color','w');
    plot(w, 10*log10(HH));
    set(gca,'XTick', (1:Nch/2)*w(end)/Nch*2);
    set(gca,'XTickLabel', (1:Nch/2));
    title(title_str);
    xlabel('Frequency (normalized to channel center)');
    ylabel('Magnitude Response (dB)');
    axis tight;
    
    % Show
    figure(), set(gca,'Color','w');
    strips(HH'), grid off;
    set(gca,'YTickLabel',1:Nch);
    set(gca,'XTick',(1:Nch/2)*Nfreq/Nch*2);
    set(gca,'XTickLabel',(1:Nch/2));
    title(title_str);
    xlabel('Frequency (normalized to channel center)');
    ylabel('Channel, Amplitude in Each Channel');
    axis tight;
    
end

