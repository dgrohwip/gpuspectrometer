clear; fclose('all');

% Hard-coded pfb.cu settings copied here:
Nch = 32; 
Ntaps = 32;
fs = 256e6;

% Derived
fn = sprintf('pfb_%d_%d.pfbout_td_gpu', Nch, Ntaps); device = 'GPU Post-IFFT';

Lchfft = 8192;
Nsamp = Nch * Lchfft * 2;

N = Nsamp/Nch;
x_units = fs/(N*Nch);

% Read data
ddim = [Nch N];
fd = fopen(fn, 'rb');
din = fread(fd, ddim, 'float32');
fclose(fd);

% convert Nch real values into Nch/2 complex values (1st Nyquist of PFB IFFT output)
dd = din(1:2:Nch,:) + 1i*din(2:2:Nch,:); 
Nch = floor(Nch / 2);
clear din;

Fd = fft(dd,[],2); % spectra in channels, time direction
Fd = abs(Fd);
% Nnyq = size(dd,2);
Nnyq = round(size(dd,2)/2 + 1);

Fd = Fd(:,1:Nnyq);
Fdmax = max(max(Fd));

% Glue spectra side by side into a plot
figure(1), clf;
cmap = jet(Nch);
for ch=1:Nch,
    yy = Fd(ch,:); 
    N = numel(yy);
    xx = (1:N) - 1 + N*(ch-1);
    semilogy(xx*x_units, yy,'Color',cmap(ch,:)); 
    text(min(xx)*x_units,0.1*Fdmax, sprintf('Ch#%02d',ch), 'Color',cmap(ch,:));
    hold on;
end
xlabel('Frequency (MHz)');
ylabel('Fourier Amplitude (log|F(x)|)')
title([device ' output ' fn]);
axis tight;
